---
title: (38) Sad - ص
date: 2021-10-27T03:57:24.010Z
ayat: 38
description: "Jumlah Ayat: 88 / Arti: Sad"
---
<!--StartFragment-->

38:1

# صۤ ۗوَالْقُرْاٰنِ ذِى الذِّكْرِۗ

*shaa*d wa**a**lqur-*aa*ni *dz*ii **al***dzdz*ikr**i**

Shad, demi Al-Qur'an yang mengandung peringatan.

38:2

# بَلِ الَّذِيْنَ كَفَرُوْا فِيْ عِزَّةٍ وَّشِقَاقٍ

bali **al**la*dz*iina kafaruu fii 'izzatin wasyiq*aa*q**in**

Tetapi orang-orang yang kafir (berada) dalam kesombongan dan permusuhan.

38:3

# كَمْ اَهْلَكْنَا مِنْ قَبْلِهِمْ مِّنْ قَرْنٍ فَنَادَوْا وَّلَاتَ حِيْنَ مَنَاصٍ

kam ahlakn*aa* min qablihim min qarnin fan*aa*daw wal*aa*ta *h*iina man*aas***in**

Betapa banyak umat sebelum mereka yang telah Kami binasakan, lalu mereka meminta tolong padahal (waktu itu) bukanlah saat untuk lari melepaskan diri.

38:4

# وَعَجِبُوْٓا اَنْ جَاۤءَهُمْ مُّنْذِرٌ مِّنْهُمْ ۖوَقَالَ الْكٰفِرُوْنَ هٰذَا سٰحِرٌ كَذَّابٌۚ

wa'ajibuu an j*aa*-ahum mun*dz*irun minhum waq*aa*la **a**lk*aa*firuuna h*aatsa* s*aah*irun ka*dzdzaa*b**un**

Dan mereka heran karena mereka kedatangan seorang pemberi peringatan (rasul) dari kalangan mereka; dan orang-orang kafir berkata, “Orang ini adalah pesihir yang banyak berdusta.”

38:5

# اَجَعَلَ الْاٰلِهَةَ اِلٰهًا وَّاحِدًا ۖاِنَّ هٰذَا لَشَيْءٌ عُجَابٌ

aja'ala **a**l-*aa*lihata il*aa*han w*aah*idan inna h*aadzaa* lasyay-un 'uj*aa*b**un**

Apakah dia menjadikan tuhan-tuhan itu Tuhan yang satu saja? Sungguh, ini benar-benar sesuatu yang sangat mengherankan.

38:6

# وَانْطَلَقَ الْمَلَاُ مِنْهُمْ اَنِ امْشُوْا وَاصْبِرُوْا عَلٰٓى اٰلِهَتِكُمْ ۖاِنَّ هٰذَا لَشَيْءٌ يُّرَادُ ۖ

wa**i**n*th*alaqa **a**lmalau minhum ani imsyuu wa**i***sh*biruu 'al*aa* *aa*lihatikum inna h*aadzaa* lasyay-un yur*aa*d**u**

Lalu pergilah pemimpin-pemimpin mereka (seraya berkata), “Pergilah kamu dan tetaplah (menyembah) tuhan-tuhanmu, sesungguhnya ini benar-benar suatu hal yang dikehendaki.

38:7

# مَا سَمِعْنَا بِهٰذَا فِى الْمِلَّةِ الْاٰخِرَةِ ۖاِنْ هٰذَآ اِلَّا اخْتِلَاقٌۚ

m*aa* sami'n*aa* bih*aadzaa* fii **a**lmillati **a**l-*aa*khirati in h*aadzaa* ill*aa* ikhtil*aa*q**un**

Kami tidak pernah mendengar hal ini dalam agama yang terakhir; ini (mengesakan Allah), tidak lain hanyalah (dusta) yang diada-adakan,

38:8

# اَؤُنْزِلَ عَلَيْهِ الذِّكْرُ مِنْۢ بَيْنِنَا ۗبَلْ هُمْ فِيْ شَكٍّ مِّنْ ذِكْرِيْۚ بَلْ لَّمَّا يَذُوْقُوْا عَذَابِ ۗ

aunzila 'alayhi **al***dzdz*ikru min baynin*aa* bal hum fii syakkin min *dz*ikrii bal lamm*aa* ya*dz*uuquu 'a*dzaa*b**i**

mengapa Al-Qur'an itu diturunkan kepada dia di antara kita?” Sebenarnya mereka ragu-ragu terhadap Al-Qur'an-Ku, tetapi mereka belum merasakan azab(-Ku).

38:9

# اَمْ عِنْدَهُمْ خَزَاۤىِٕنُ رَحْمَةِ رَبِّكَ الْعَزِيْزِ الْوَهَّابِۚ

am 'indahum khaz*aa*-inu ra*h*mati rabbika **a**l'aziizi **a**lwahh*aa*b**i**

Atau apakah mereka itu mempunyai perbendaharaan rahmat Tuhanmu Yang Mahaperkasa, Maha Pemberi?]

38:10

# اَمْ لَهُمْ مُّلْكُ السَّمٰوٰتِ وَالْاَرْضِ وَمَا بَيْنَهُمَا ۗفَلْيَرْتَقُوْا فِى الْاَسْبَابِ

am lahum mulku **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i wam*aa* baynahum*aa* falyartaquu fii **a**l-asb*aa*b**i**

Atau apakah mereka mempunyai kerajaan langit dan bumi dan apa yang ada di antara keduanya? (Jika ada), maka biarlah mereka menaiki tangga-tangga (ke langit).

38:11

# جُنْدٌ مَّا هُنَالِكَ مَهْزُوْمٌ مِّنَ الْاَحْزَابِ

jundun m*aa* hun*aa*lika mahzuumun mina **a**l-a*h*z*aa*b**i**

(Mereka itu) kelompok besar bala tentara yang berada di sana yang akan dikalahkan.

38:12

# كَذَّبَتْ قَبْلَهُمْ قَوْمُ نُوْحٍ وَّعَادٌ وَّفِرْعَوْنُ ذُو الْاَوْتَادِۙ

ka*dzdz*abat qablahum qawmu nuu*h*in wa'*aa*dun wafir'awnu *dz*uu **a**l-awt*aa*d**i**

Sebelum mereka itu, kaum Nuh, ‘Ad dan Fir’aun yang mempunyai bala tentara yang banyak, juga telah mendustakan (rasul-rasul),

38:13

# وَثَمُوْدُ وَقَوْمُ لُوْطٍ وَّاَصْحٰبُ لْـَٔيْكَةِ ۗ اُولٰۤىِٕكَ الْاَحْزَابُ

watsamuudu waqawmu luu*th*in wa-a*sh*-*haa*bu **a**l-aykati ul*aa*-ika **a**l-a*h*z*aa*b**u**

dan (begitu juga) Samud, kaum Lut dan penduduk Aikah. Mereka itulah golongan-golongan yang bersekutu (menentang rasul-rasul).

38:14

# اِنْ كُلٌّ اِلَّا كَذَّبَ الرُّسُلَ فَحَقَّ عِقَابِ ࣖ

in kullun ill*aa* ka*dzdz*aba **al**rrusula fa*h*aqqa 'iq*aa*b**i**

Semua mereka itu mendustakan rasul-rasul, maka pantas mereka merasakan azab-Ku.

38:15

# وَمَا يَنْظُرُ هٰٓؤُلَاۤءِ اِلَّا صَيْحَةً وَّاحِدَةً مَّا لَهَا مِنْ فَوَاقٍ

wam*aa* yan*zh*uru h*aa*ul*aa*-i ill*aa* *sh*ay*h*atan w*aah*idatan m*aa* lah*aa* min faw*aa*q**in**

Dan sebenarnya yang mereka tunggu adalah satu teriakan saja, yang tidak ada selanya.

38:16

# وَقَالُوْا رَبَّنَا عَجِّلْ لَّنَا قِطَّنَا قَبْلَ يَوْمِ الْحِسَابِ

waq*aa*luu rabban*aa* 'ajjil lan*aa* qi*ththh*an*aa* qabla yawmi **a**l*h*is*aa*b**i**

Dan mereka berkata, “Ya Tuhan kami, segerakanlah azab yang diperuntukkan bagi kami sebelum hari perhitungan.”

38:17

# اِصْبِرْ عَلٰى مَا يَقُوْلُوْنَ وَاذْكُرْ عَبْدَنَا دَاوٗدَ ذَا الْاَيْدِۚ اِنَّهٗٓ اَوَّابٌ

i*sh*bir 'al*aa* m*aa* yaquuluuna wa**u***dz*kur 'abdan*aa* d*aa*wuuda *dzaa* **a**l-aydi innahu aww*aa*b**un**

Bersabarlah atas apa yang mereka katakan; dan ingatlah akan hamba Kami Dawud yang mempunyai kekuatan; sungguh dia sangat taat (kepada Allah).

38:18

# اِنَّا سَخَّرْنَا الْجِبَالَ مَعَهٗ يُسَبِّحْنَ بِالْعَشِيِّ وَالْاِشْرَاقِۙ

inn*aa* sakhkharn*aa* **a**ljib*aa*la ma'ahu yusabbi*h*na bi**a**l'asyiyyi wa**a**l-isyr*aa*q**i**

Sungguh, Kamilah yang menundukkan gunung-gunung untuk bertasbih bersama dia (Dawud) pada waktu petang dan pagi,

38:19

# وَالطَّيْرَمَحْشُوْرَةً ۗ كُلٌّ لَهٗٓ اَوَّابٌ

wa**al***ththh*ayra ma*h*syuuratan kullun lahu aww*aa*b**un**

dan (Kami tundukkan pula) burung-burung dalam keadaan terkumpul. Masing-masing sangat taat (kepada Allah).

38:20

# وَشَدَدْنَا مُلْكَهٗ وَاٰتَيْنٰهُ الْحِكْمَةَ وَفَصْلَ الْخِطَابِ

wasyadadn*aa* mulkahu wa*aa*tayn*aa*hu **a**l*h*ikmata wafa*sh*la **a**lkhi*thaa*b**i**

Dan Kami kuatkan kerajaannya dan Kami berikan hikmah kepadanya serta kebijaksanaan dalam memutuskan perkara.

38:21

# وَهَلْ اَتٰىكَ نَبَؤُ الْخَصْمِۘ اِذْ تَسَوَّرُوا الْمِحْرَابَۙ

wahal at*aa*ka nabau **a**lkha*sh*mi i*dz* tasawwaruu **a**lmi*h*r*aa*b**a**

Dan apakah telah sampai kepadamu berita orang-orang yang berselisih ketika mereka memanjat dinding mihrab?

38:22

# اِذْ دَخَلُوْا عَلٰى دَاوٗدَ فَفَزِعَ مِنْهُمْ قَالُوْا لَا تَخَفْۚ خَصْمٰنِ بَغٰى بَعْضُنَا عَلٰى بَعْضٍ فَاحْكُمْ بَيْنَنَا بِالْحَقِّ وَلَا تُشْطِطْ وَاهْدِنَآ اِلٰى سَوَاۤءِ الصِّرَاطِ

its dakhaluu 'al*aa* d*aa*wuuda fafazi'a minhum q*aa*luu l*aa* takhaf kha*sh*m*aa*ni bagh*aa* ba'*dh*un*aa* 'al*aa* ba'*dh*in fa**u***h*kum baynan*aa* bi**a**l*h<*

ketika mereka masuk menemui Dawud lalu dia terkejut karena (kedatangan) mereka. Mereka berkata, “Janganlah takut! (Kami) berdua sedang berselisih, sebagian dari kami berbuat zalim kepada yang lain; maka berilah keputusan di antara kami secara adil dan jan







38:23

# اِنَّ هٰذَآ اَخِيْ ۗ لَهٗ تِسْعٌ وَّتِسْعُوْنَ نَعْجَةً وَّلِيَ نَعْجَةٌ وَّاحِدَةٌ ۗفَقَالَ اَكْفِلْنِيْهَا وَعَزَّنِيْ فِى الْخِطَابِ

inna h*aadzaa* akhii lahu tis'un watis'uuna na'jatan waliya na'jatun w*aah*idatun faq*aa*la akfilniih*aa* wa'azzanii fii **a**lkhi*thaa*b**i**

Sesungguhnya saudaraku ini mempunyai sembilan puluh sembilan ekor kambing betina dan aku mempunyai seekor saja, lalu dia berkata, “Serahkanlah (kambingmu) itu kepadaku! Dan dia mengalahkan aku dalam perdebatan.”

38:24

# قَالَ لَقَدْ ظَلَمَكَ بِسُؤَالِ نَعْجَتِكَ اِلٰى نِعَاجِهٖۗ وَاِنَّ كَثِيْرًا مِّنَ الْخُلَطَاۤءِ لَيَبْغِيْ بَعْضُهُمْ عَلٰى بَعْضٍ اِلَّا الَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِ وَقَلِيْلٌ مَّا هُمْۗ وَظَنَّ دَاوٗدُ اَنَّمَا فَتَنّٰهُ فَاسْتَغْفَرَ

q*aa*la laqad *zh*alamaka bisu-*aa*li na'jatika il*aa* ni'*aa*jihi wa-inna katsiiran mina **a**lkhula*thaa*-i layabghii ba'*dh*uhum 'al*aa* ba'*dh*in ill*aa* **al**la*dz<*

Dia (Dawud) berkata, “Sungguh, dia telah berbuat zalim kepadamu dengan meminta kambingmu itu untuk (ditambahkan) kepada kambingnya. Memang banyak di antara orang-orang yang bersekutu itu berbuat zalim kepada yang lain, kecuali orang-orang yang beriman dan







38:25

# فَغَفَرْنَا لَهٗ ذٰلِكَۗ وَاِنَّ لَهٗ عِنْدَنَا لَزُلْفٰى وَحُسْنَ مَاٰبٍ

faghafarn*aa* lahu *dzaa*lika wa-inna lahu 'indan*aa* lazulf*aa* wa*h*usna ma*aa*b**in**

Lalu Kami mengampuni (kesalahannya) itu. Dan sungguh, dia mempunyai kedudukan yang benar-benar dekat di sisi Kami dan tempat kembali yang baik.

38:26

# يٰدَاوٗدُ اِنَّا جَعَلْنٰكَ خَلِيْفَةً فِى الْاَرْضِ فَاحْكُمْ بَيْنَ النَّاسِ بِالْحَقِّ وَلَا تَتَّبِعِ الْهَوٰى فَيُضِلَّكَ عَنْ سَبِيْلِ اللّٰهِ ۗاِنَّ الَّذِيْنَ يَضِلُّوْنَ عَنْ سَبِيْلِ اللّٰهِ لَهُمْ عَذَابٌ شَدِيْدٌ ۢبِمَا نَسُوْا يَوْمَ الْحِسَا

y*aa* d*aa*wuudu inn*aa* ja'aln*aa*ka khaliifatan fii **a**l-ar*dh*i fa**u***h*kum bayna **al**nn*aa*si bi**a**l*h*aqqi wal*aa* tattabi'i **a**lh

(Allah berfirman), “Wahai Dawud! Sesungguhnya engkau Kami jadikan khalifah (penguasa) di bumi, maka berilah keputusan (perkara) di antara manusia dengan adil dan janganlah engkau mengikuti hawa nafsu, karena akan menyesatkan engkau dari jalan Allah. Sungg

38:27

# وَمَا خَلَقْنَا السَّمَاۤءَ وَالْاَرْضَ وَمَا بَيْنَهُمَا بَاطِلًا ۗذٰلِكَ ظَنُّ الَّذِيْنَ كَفَرُوْا فَوَيْلٌ لِّلَّذِيْنَ كَفَرُوْا مِنَ النَّارِۗ

wam*aa* khalaqn*aa* **al**ssam*aa*-a wa**a**l-ar*dh*a wam*aa* baynahum*aa* b*aath*ilan *dzaa*lika *zh*annu **al**la*dz*iina kafaruu fawaylun lilla*dz*iina kafaru

Dan Kami tidak menciptakan langit dan bumi dan apa yang ada di antara keduanya dengan sia-sia. Itu anggapan orang-orang kafir, maka celakalah orang-orang yang kafir itu karena mereka akan masuk neraka.

38:28

# اَمْ نَجْعَلُ الَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِ كَالْمُفْسِدِيْنَ فِى الْاَرْضِۖ اَمْ نَجْعَلُ الْمُتَّقِيْنَ كَالْفُجَّارِ

am naj'alu **al**la*dz*iina *aa*manuu wa'amiluu **al***shshaa*li*haa*ti ka**a**lmufsidiina fii **a**l-ar*dh*i am naj'alu **a**lmuttaqiina ka**a**lfujj

Pantaskah Kami memperlakukan orang-orang yang beriman dan mengerjakan kebajikan sama dengan orang-orang yang berbuat kerusakan di bumi? Atau pantaskah Kami menganggap orang-orang yang bertakwa sama dengan orang-orang yang jahat?

38:29

# كِتٰبٌ اَنْزَلْنٰهُ اِلَيْكَ مُبٰرَكٌ لِّيَدَّبَّرُوْٓا اٰيٰتِهٖ وَلِيَتَذَكَّرَ اُولُوا الْاَلْبَابِ

kit*aa*bun anzaln*aa*hu ilayka mub*aa*rakun liyaddabbaruu *aa*y*aa*tihi waliyata*dz*akkara uluu **a**l-alb*aa*b**i**

Kitab (Al-Qur'an) yang Kami turunkan kepadamu penuh berkah agar mereka menghayati ayat-ayatnya dan agar orang-orang yang berakal sehat mendapat pelajaran.

38:30

# وَوَهَبْنَا لِدَاوٗدَ سُلَيْمٰنَۗ نِعْمَ الْعَبْدُ ۗاِنَّهٗٓ اَوَّابٌۗ

wawahabn*aa* lid*aa*wuuda sulaym*aa*na ni'ma **a**l'abdu innahu aww*aa*b**un**

Dan kepada Dawud Kami karuniakan (anak bernama) Sulaiman; dia adalah sebaik-baik hamba. Sungguh, dia sangat taat (kepada Allah).

38:31

# اِذْ عُرِضَ عَلَيْهِ بِالْعَشِيِّ الصّٰفِنٰتُ الْجِيَادُۙ

i*dz* 'uri*dh*a 'alayhi bi**a**l'asyiyyi **al***shshaa*fin*aa*tu **a**ljiy*aa*d**u**

(Ingatlah) ketika pada suatu sore dipertunjukkan kepadanya (kuda-kuda) yang jinak, (tetapi) sangat cepat larinya,

38:32

# فَقَالَ اِنِّيْٓ اَحْبَبْتُ حُبَّ الْخَيْرِ عَنْ ذِكْرِ رَبِّيْۚ حَتّٰى تَوَارَتْ بِالْحِجَابِۗ

faq*aa*la innii a*h*babtu *h*ubba **a**lkhayri 'an *dz*ikri rabbii *h*att*aa* taw*aa*rat bi**a**l*h*ij*aa*b**i**

maka dia berkata, “Sesungguhnya aku menyukai segala yang baik (kuda), yang membuat aku ingat akan (kebesaran) Tuhanku, sampai matahari terbenam.”

38:33

# رُدُّوْهَا عَلَيَّ ۚفَطَفِقَ مَسْحًا ۢبِالسُّوْقِ وَالْاَعْنَاقِ

rudduuh*aa* 'alayya fa*th*afiqa mas*h*an bi**al**ssuuqi wa**a**l-a'n*aa*q**i**

”Bawalah semua kuda itu kembali kepadaku.” Lalu dia mengusap-usap kaki dan leher kuda itu.

38:34

# وَلَقَدْ فَتَنَّا سُلَيْمٰنَ وَاَلْقَيْنَا عَلٰى كُرْسِيِّهٖ جَسَدًا ثُمَّ اَنَابَ

walaqad fatann*aa* sulaym*aa*na wa-alqayn*aa* 'al*aa* kursiyyihi jasadan tsumma an*aa*b**a**

Dan sungguh, Kami telah menguji Sulaiman dan Kami jadikan (dia) tergeletak di atas kursinya sebagai tubuh (yang lemah karena sakit), kemudian dia bertobat.

38:35

# قَالَ رَبِّ اغْفِرْ لِيْ وَهَبْ لِيْ مُلْكًا لَّا يَنْۢبَغِيْ لِاَحَدٍ مِّنْۢ بَعْدِيْۚ اِنَّكَ اَنْتَ الْوَهَّابُ

q*aa*la rabbi ighfir lii wahab lii mulkan l*aa* yanbaghii li-a*h*adin min ba'dii innaka anta **a**lwahh*aa*b**u**

Dia berkata, “Ya Tuhanku, ampunilah aku dan anugerahkanlah kepadaku kerajaan yang tidak dimiliki oleh siapa pun setelahku. Sungguh, Engkaulah Yang Maha Pemberi.”

38:36

# فَسَخَّرْنَا لَهُ الرِّيْحَ تَجْرِيْ بِاَمْرِهٖ رُخَاۤءً حَيْثُ اَصَابَۙ

fasakhkharn*aa* lahu **al**rrii*h*a tajrii bi-amrihi rukh*aa*-an *h*aytsu a*shaa*b**a**

Kemudian Kami tundukkan kepadanya angin yang berhembus dengan baik menurut perintahnya ke mana saja yang dikehendakinya,

38:37

# وَالشَّيٰطِيْنَ كُلَّ بَنَّاۤءٍ وَّغَوَّاصٍۙ

wa**al**sysyay*aath*iina kulla bann*aa*-in waghaww*aas***in**

dan (Kami tundukkan pula kepadanya) setan-setan, semuanya ahli bangunan dan penyelam,

38:38

# وَّاٰخَرِيْنَ مُقَرَّنِيْنَ فِى الْاَصْفَادِ

wa*aa*khariina muqarraniina fii **a**l-a*sh*f*aa*d**i**

dan (setan) yang lain yang terikat dalam belenggu.

38:39

# هٰذَا عَطَاۤؤُنَا فَامْنُنْ اَوْ اَمْسِكْ بِغَيْرِ حِسَابٍ

h*aadzaa* 'a*thaa*un*aa* fa**u**mnun aw amsik bighayri *h*is*aa*b**in**

Inilah anugerah Kami; maka berikanlah (kepada orang lain) atau tahanlah (untuk dirimu sendiri) tanpa perhitungan.

38:40

# وَاِنَّ لَهٗ عِنْدَنَا لَزُلْفٰى وَحُسْنَ مَاٰبٍ ࣖ

wa-inna lahu 'indan*aa* lazulf*aa* wa*h*usna ma*aa*b**in**

Dan sungguh, dia mempunyai kedudukan yang dekat pada sisi Kami dan tempat kembali yang baik.

38:41

# وَاذْكُرْ عَبْدَنَآ اَيُّوْبَۘ اِذْ نَادٰى رَبَّهٗٓ اَنِّيْ مَسَّنِيَ الشَّيْطٰنُ بِنُصْبٍ وَّعَذَابٍۗ

wa**u***dz*kur 'abdan*aa* ayyuuba i*dz* n*aa*d*aa* rabbahu annii massaniya **al**sysyay*thaa*nu binu*sh*bin wa'a*dzaa*b**in**

Dan ingatlah akan hamba Kami Ayyub ketika dia menyeru Tuhannya, “Sesungguhnya aku diganggu setan dengan penderitaan dan bencana.”

38:42

# اُرْكُضْ بِرِجْلِكَۚ هٰذَا مُغْتَسَلٌۢ بَارِدٌ وَّشَرَابٌ

urku*dh* birijlika h*aadzaa* mughtasalun b*aa*ridun wasyar*aa*b**un**

Allah berfirman), “Hentakkanlah kakimu; inilah air yang sejuk untuk mandi dan untuk minum.”

38:43

# وَوَهَبْنَا لَهٗٓ اَهْلَهٗ وَمِثْلَهُمْ مَّعَهُمْ رَحْمَةً مِّنَّا وَذِكْرٰى لِاُولِى الْاَلْبَابِ

wawahabn*aa* lahu ahlahu wamitslahum ma'ahum ra*h*matan minn*aa* wa*dz*ikr*aa* li-ulii **a**l-alb*aa*b**i**

Dan Kami anugerahi dia (dengan mengumpulkan kembali) keluarganya dan Kami lipatgandakan jumlah mereka, sebagai rahmat dari Kami dan pelajaran bagi orang-orang yang berpikiran sehat.

38:44

# وَخُذْ بِيَدِكَ ضِغْثًا فَاضْرِبْ بِّهٖ وَلَا تَحْنَثْ ۗاِنَّا وَجَدْنٰهُ صَابِرًا ۗنِعْمَ الْعَبْدُ ۗاِنَّهٗٓ اَوَّابٌ

wakhu*dz* biyadika *dh*ightsan fa**i***dh*rib bihi wal*aa* ta*h*nats inn*aa* wajadn*aa*hu *shaa*biran ni'ma **a**l'abdu innahu aww*aa*b**un**

Dan ambillah seikat (rumput) dengan tanganmu, lalu pukullah dengan itu dan janganlah engkau melanggar sumpah. Sesungguhnya Kami dapati dia (Ayyub) seorang yang sabar. Dialah sebaik-baik hamba. Sungguh, dia sangat taat (kepada Allah).

38:45

# وَاذْكُرْ عِبٰدَنَآ اِبْرٰهِيْمَ وَاِسْحٰقَ وَيَعْقُوْبَ اُولِى الْاَيْدِيْ وَالْاَبْصَارِ

wa**u***dz*kur 'ib*aa*dan*aa* ibr*aa*hiima wa-is*haa*qa waya'quuba ulii **a**l-aydii wa**a**l-ab*shaa*r**i**

Dan ingatlah hamba-hamba Kami: Ibrahim, Ishak dan Yakub yang mempunyai kekuatan-kekuatan yang besar dan ilmu-ilmu (yang tinggi).

38:46

# اِنَّآ اَخْلَصْنٰهُمْ بِخَالِصَةٍ ذِكْرَى الدَّارِۚ

inn*aa* akhla*sh*n*aa*hum bikh*aa*li*sh*atin *dz*ikr*aa* **al**dd*aa*r**i**

Sungguh, Kami telah menyucikan mereka dengan (menganugerahkan) akhlak yang tinggi kepadanya yaitu selalu mengingatkan (manusia) kepada negeri akhirat.

38:47

# وَاِنَّهُمْ عِنْدَنَا لَمِنَ الْمُصْطَفَيْنَ الْاَخْيَارِۗ

wa-innahum 'indan*aa* lamina **a**lmu*sth*afayna **a**l-akhy*aa*r**i**

Dan sungguh, di sisi Kami mereka termasuk orang-orang pilihan yang paling baik.

38:48

# وَاذْكُرْ اِسْمٰعِيْلَ وَالْيَسَعَ وَذَا الْكِفْلِ ۗوَكُلٌّ مِّنَ الْاَخْيَارِۗ

wa**u***dz*kur ism*aa*'iila wa-ilyasa'a wa*dzaa* **a**lkifli wakullun mina **a**l-akhy*aa*r**i**

Dan ingatlah Ismail, Ilyasa‘ dan Zulkifli. Semuanya termasuk orang-orang yang paling baik.

38:49

# هٰذَا ذِكْرٌ ۗوَاِنَّ لِلْمُتَّقِيْنَ لَحُسْنَ مَاٰبٍۙ

h*aadzaa* *dz*ikrun wa-inna lilmuttaqiina la*h*usna ma*aa*b**in**

Ini adalah kehormatan (bagi mereka). Dan sungguh, bagi orang-orang yang bertakwa (disediakan) tempat kembali yang terbaik,

38:50

# جَنّٰتِ عَدْنٍ مُّفَتَّحَةً لَّهُمُ الْاَبْوَابُۚ

jann*aa*ti 'adnin mufatta*h*atan lahumu **a**l-abw*aa*b**u**

(yaitu) surga ’Adn yang pintu-pintunya terbuka bagi mereka,

38:51

# مُتَّكِـِٕيْنَ فِيْهَا يَدْعُوْنَ فِيْهَا بِفَاكِهَةٍ كَثِيْرَةٍ وَّشَرَابٍ

muttaki-iina fiih*aa* yad'uuna fiih*aa* bif*aa*kihatin katsiiratin wasyar*aa*b**in**

di dalamnya mereka bersandar (di atas dipan-dipan) sambil meminta buah-buahan yang banyak dan minuman (di surga itu).

38:52

# وَعِنْدَهُمْ قٰصِرٰتُ الطَّرْفِ اَتْرَابٌ

wa'indahum q*aas*ir*aa*tu **al***ththh*arfi atr*aa*b**un**

dan di samping mereka (ada bidadari-bidadari) yang redup pandangannya dan sebaya umurnya.

38:53

# هٰذَا مَا تُوْعَدُوْنَ لِيَوْمِ الْحِسَابِ

h*aadzaa* m*aa* tuu'aduuna liyawmi **a**l*h*is*aa*b**i**

Inilah apa yang dijanjikan kepadamu pada hari perhitungan.

38:54

# اِنَّ هٰذَا لَرِزْقُنَا مَا لَهٗ مِنْ نَّفَادٍۚ

inna h*aadzaa* larizqun*aa* m*aa* lahu min naf*aa*d**in**

Sungguh, inilah rezeki dari Kami yang tidak ada habis-habisnya.

38:55

# هٰذَا ۗوَاِنَّ لِلطّٰغِيْنَ لَشَرَّ مَاٰبٍۙ

h*aadzaa* wa-inna li**l***ththaa*ghiina lasyarra ma*aa*b**in**

Beginilah (keadaan mereka). Dan sungguh, bagi orang-orang yang durhaka pasti (disediakan) tempat kembali yang buruk,

38:56

# جَهَنَّمَۚ يَصْلَوْنَهَاۚ فَبِئْسَ الْمِهَادُ

jahannama ya*sh*lawnah*aa* fabi/sa **a**lmih*aa*d**u**

(yaitu) neraka Jahanam yang mereka masuki; maka itulah seburuk-buruk tempat tinggal.

38:57

# هٰذَاۙ فَلْيَذُوْقُوْهُ حَمِيْمٌ وَّغَسَّاقٌۙ

h*aadzaa* falya*dz*uuquuhu *h*amiimun waghass*aa*q**un**

Inilah (azab neraka), maka biarlah mereka merasakannya, (minuman mereka) air yang sangat panas dan air yang sangat dingin,

38:58

# وَّاٰخَرُ مِنْ شَكْلِهٖٓ اَزْوَاجٌۗ

wa*aa*kharu min syaklihi azw*aa*j**un**

dan berbagai macam (azab) yang lain yang serupa itu.

38:59

# هٰذَا فَوْجٌ مُّقْتَحِمٌ مَّعَكُمْۚ لَا مَرْحَبًا ۢبِهِمْ ۗ اِنَّهُمْ صَالُوا النَّارِ

h*aadzaa* fawjun muqta*h*imun ma'akum l*aa* mar*h*aban bihim innahum *shaa*luu **al**nn*aa*r**i**

(Dikatakan kepada mereka), “Ini rombongan besar (pengikut-pengikutmu) yang masuk berdesak-desak bersama kamu (ke neraka).” Tidak ada ucapan selamat datang bagi mereka karena sesungguhnya mereka akan masuk neraka (kata pemimpin-pemimpin mereka).

38:60

# قَالُوْا بَلْ اَنْتُمْ لَا مَرْحَبًاۢ بِكُمْ ۗ اَنْتُمْ قَدَّمْتُمُوْهُ لَنَاۚ فَبِئْسَ الْقَرَارُ

q*aa*luu bal antum l*aa* mar*h*aban bikum antum qaddamtumuuhu lan*aa* fabi/sa **a**lqar*aa*r**u**

(Para pengikut mereka menjawab), “Sebenarnya kamulah yang (lebih pan-tas) tidak menerima ucapan selamat datang, karena kamulah yang menjerumuskan kami ke dalam azab, maka itulah seburuk-buruk tempat menetap.”

38:61

# قَالُوْا رَبَّنَا مَنْ قَدَّمَ لَنَا هٰذَا فَزِدْهُ عَذَابًا ضِعْفًا فِى النَّارِ

q*aa*luu rabban*aa* man qaddama lan*aa* h*aadzaa* fazidhu 'a*dzaa*ban *dh*i'fan fii **al**nn*aa*r**i**

Mereka berkata (lagi), “Ya Tuhan kami, barangsiapa menjerumuskan kami ke dalam (azab) ini, maka tambahkanlah azab kepadanya dua kali lipat di dalam neraka.”

38:62

# وَقَالُوْا مَا لَنَا لَا نَرٰى رِجَالًا كُنَّا نَعُدُّهُمْ مِّنَ الْاَشْرَارِ

waq*aa*luu m*aa* lan*aa* l*aa* nar*aa* rij*aa*lan kunn*aa* na'udduhum mina **a**l-asyr*aa*r**i**

Dan (orang-orang durhaka) berkata, “Mengapa kami tidak melihat orang-orang yang dahulu (di dunia) kami anggap sebagai orang-orang yang jahat (hina).

38:63

# اَتَّخَذْنٰهُمْ سِخْرِيًّا اَمْ زَاغَتْ عَنْهُمُ الْاَبْصَارُ

attakha*dz*n*aa*hum sikhriyyan am z*aa*ghat 'anhumu **a**l-ab*shaa*r**u**

Dahulu kami menjadikan mereka olok-olokan, ataukah karena penglihatan kami yang tidak melihat mereka?”

38:64

# اِنَّ ذٰلِكَ لَحَقٌّ تَخَاصُمُ اَهْلِ النَّارِ ࣖ

inna *dzaa*lika la*h*aqqun takh*aas*umu ahli **al**nn*aa*r**i**

Sungguh, yang demikian benar-benar terjadi, (yaitu) pertengkaran di antara penghuni neraka.

38:65

# قُلْ اِنَّمَآ اَنَا۠ مُنْذِرٌ ۖوَّمَا مِنْ اِلٰهٍ اِلَّا اللّٰهُ الْوَاحِدُ الْقَهَّارُ

qul innam*aa* an*aa* mun*dz*irun wam*aa* min il*aa*hin ill*aa* **al**l*aa*hu **a**lw*aah*idu **a**lqahh*aa*r**u**

Katakanlah (Muhammad), “Sesungguhnya aku hanya seorang pemberi peringatan, tidak ada tuhan selain Allah Yang Maha Esa, Mahaperkasa,

38:66

# رَبُّ السَّمٰوٰتِ وَالْاَرْضِ وَمَا بَيْنَهُمَا الْعَزِيْزُ الْغَفَّارُ

rabbu **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i wam*aa* baynahum*aa* **a**l'aziizu **a**lghaff*aa*r**u**

(yaitu) Tuhan langit dan bumi dan apa yang ada di antara keduanya, Yang Mahaperkasa, Maha Pengampun.”

38:67

# قُلْ هُوَ نَبَؤٌا عَظِيْمٌۙ

qul huwa nabaun 'a*zh*iim**un**

Katakanlah, “Itu (Al-Qur'an) adalah berita besar,

38:68

# اَنْتُمْ عَنْهُ مُعْرِضُوْنَ

antum 'anhu mu'ri*dh*uun**a**

yang kamu berpaling darinya.

38:69

# مَا كَانَ لِيَ مِنْ عِلْمٍۢ بِالْمَلَاِ الْاَعْلٰٓى اِذْ يَخْتَصِمُوْنَ

m*aa* k*aa*na liya min 'ilmin bi**a**lmala-i **a**l-a'l*aa* i*dz* yakhta*sh*imuun**a**

Aku tidak mempunyai pengetahuan sedikit pun tentang al-mala’ul a’la (malaikat) itu ketika mereka berbantah-bantahan.

38:70

# اِنْ يُّوْحٰىٓ اِلَيَّ اِلَّآ اَنَّمَآ اَنَا۠ نَذِيْرٌ مُّبِيْنٌ

in yuu*haa* ilayya ill*aa* annam*aa* an*aa* na*dz*iirun mubiin**un**

Yang diwahyukan kepadaku, bahwa aku hanyalah seorang pemberi peringatan yang nyata.”

38:71

# اِذْ قَالَ رَبُّكَ لِلْمَلٰۤىِٕكَةِ اِنِّيْ خَالِقٌۢ بَشَرًا مِّنْ طِيْنٍ

i*dz* q*aa*la rabbuka lilmal*aa*-ikati innii kh*aa*liqun basyaran min *th*iin**in**

(Ingatlah) ketika Tuhanmu berfirman kepada malaikat, “Sesungguhnya Aku akan menciptakan manusia dari tanah.

38:72

# فَاِذَا سَوَّيْتُهٗ وَنَفَخْتُ فِيْهِ مِنْ رُّوْحِيْ فَقَعُوْا لَهٗ سٰجِدِيْنَ

fa-i*dzaa* sawwaytuhu wanafakhtu fiihi min ruu*h*ii faqa'uu lahu s*aa*jidiin**a**

Kemudian apabila telah Aku sempurnakan kejadiannya dan Aku tiupkan roh (ciptaan)-Ku kepadanya; maka tunduklah kamu dengan bersujud kepadanya.”

38:73

# فَسَجَدَ الْمَلٰۤىِٕكَةُ كُلُّهُمْ اَجْمَعُوْنَۙ

fasajada **a**lmal*aa*-ikatu kulluhum ajma'uun**a**

Lalu para malaikat itu bersujud semuanya,

38:74

# اِلَّآ اِبْلِيْسَۗ اِسْتَكْبَرَ وَكَانَ مِنَ الْكٰفِرِيْنَ

ill*aa* ibliisa istakbara wak*aa*na mina **a**lk*aa*firiin**a**

kecuali Iblis; ia menyombongkan diri dan ia termasuk golongan yang kafir.

38:75

# قَالَ يٰٓاِبْلِيْسُ مَا مَنَعَكَ اَنْ تَسْجُدَ لِمَا خَلَقْتُ بِيَدَيَّ ۗ اَسْتَكْبَرْتَ اَمْ كُنْتَ مِنَ الْعَالِيْنَ

q*aa*la y*aa* ibliisu m*aa* mana'aka an tasjuda lim*aa* khalaqtu biyadayya astakbarta am kunta mina **a**l'*aa*liin**a**

(Allah) berfirman, “Wahai Iblis, apakah yang menghalangi kamu sujud kepada yang telah Aku ciptakan dengan kekuasaan-Ku. Apakah kamu menyombongkan diri atau kamu (merasa) termasuk golongan yang (lebih) tinggi?”

38:76

# قَالَ اَنَا۠ خَيْرٌ مِّنْهُ خَلَقْتَنِيْ مِنْ نَّارٍ وَّخَلَقْتَهٗ مِنْ طِيْنٍ

q*aa*la an*aa* khayrun minhu khalaqtanii min n*aa*rin wakhalaqtahu min *th*iin**in**

(Iblis) berkata, “Aku lebih baik daripadanya, karena Engkau ciptakan aku dari api, sedangkan dia Engkau ciptakan dari tanah.”

38:77

# قَالَ فَاخْرُجْ مِنْهَا فَاِنَّكَ رَجِيْمٌۖ

q*aa*la fa**u**khruj minh*aa* fa-innaka rajiim**un**

(Allah) berfirman, “Kalau begitu keluarlah kamu dari surga! Sesungguhnya kamu adalah makhluk yang terkutuk.

38:78

# وَّاِنَّ عَلَيْكَ لَعْنَتِيْٓ اِلٰى يَوْمِ الدِّيْنِ

wa-inna 'alayka la'natii il*aa* yawmi **al**ddiin**i**

Dan sungguh, kutukan-Ku tetap atasmu sampai hari pembalasan.”

38:79

# قَالَ رَبِّ فَاَنْظِرْنِيْٓ اِلٰى يَوْمِ يُبْعَثُوْنَ

q*aa*la rabbi fa-an*zh*irnii il*aa* yawmi yub'atsuun**a**

(Iblis) berkata, “Ya Tuhanku, tangguhkanlah aku sampai pada hari mereka dibangkitkan.”

38:80

# قَالَ فَاِنَّكَ مِنَ الْمُنْظَرِيْنَۙ

q*aa*la fa-innaka mina **a**lmun*zh*ariin**a**

(Allah) berfirman, “Maka sesungguhnya kamu termasuk golongan yang diberi penangguhan,

38:81

# اِلٰى يَوْمِ الْوَقْتِ الْمَعْلُوْمِ

il*aa* yawmi **a**lwaqti **a**lma'luum**i**

sampai pada hari yang telah ditentukan waktunya (hari Kiamat).”

38:82

# قَالَ فَبِعِزَّتِكَ لَاُغْوِيَنَّهُمْ اَجْمَعِيْنَۙ

q*aa*la fabi'izzatika laughwiyannahum ajma'iin**a**

(Iblis) menjawab, “Demi kemuliaan-Mu, pasti aku akan menyesatkan mereka semuanya,

38:83

# اِلَّا عِبَادَكَ مِنْهُمُ الْمُخْلَصِيْنَ

ill*aa* 'ib*aa*daka minhumu **a**lmukhla*sh*iin**a**

kecuali hamba-hamba-Mu yang terpilih di antara mereka.”

38:84

# قَالَ فَالْحَقُّۖ وَالْحَقَّ اَقُوْلُۚ

q*aa*la fa**a**l*h*aqqu wa**a**l*h*aqqa aquul**u**

(Allah) berfirman, “Maka yang benar (adalah sumpahku), dan hanya kebenaran itulah yang Aku katakan.

38:85

# لَاَمْلَئَنَّ جَهَنَّمَ مِنْكَ وَمِمَّنْ تَبِعَكَ مِنْهُمْ اَجْمَعِيْنَ

la-amla-anna jahannama minka wamimman tabi'aka minhum ajma'iin**a**

Sungguh, Aku akan memenuhi neraka Jahanam dengan kamu dan dengan orang-orang yang mengikutimu di antara mereka semuanya.”

38:86

# قُلْ مَآ اَسْـَٔلُكُمْ عَلَيْهِ مِنْ اَجْرٍ وَّمَآ اَنَا۠ مِنَ الْمُتَكَلِّفِيْنَ

qul m*aa* as-alukum 'alayhi min ajrin wam*aa* an*aa* mina **a**lmutakallifiin**a**

Katakanlah (Muhammad), “Aku tidak meminta imbalan sedikit pun kepadamu atasnya (dakwahku); dan aku bukanlah termasuk orang yang mengada-ada.

38:87

# اِنْ هُوَ اِلَّا ذِكْرٌ لِّلْعٰلَمِيْنَ

in huwa ill*aa* *dz*ikrun lil'*aa*lamiin**a**

(Al-Qur'an) ini tidak lain hanyalah peringatan bagi seluruh alam.

38:88

# وَلَتَعْلَمُنَّ نَبَاَهٗ بَعْدَ حِيْنٍ ࣖ

walata'lamunna naba-ahu ba'da *h*iin**in**

Dan sungguh, kamu akan mengetahui (kebenaran) beritanya (Al-Qur'an) setelah beberapa waktu lagi.”

<!--EndFragment-->