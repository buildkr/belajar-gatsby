---
title: (6) Al-An'am - الانعام
date: 2021-10-27T03:16:13.108Z
ayat: 6
description: "Jumlah Ayat: 165 / Arti: Binatang Ternak"
---
<!--StartFragment-->

6:1

# اَلْحَمْدُ لِلّٰهِ الَّذِيْ خَلَقَ السَّمٰوٰتِ وَالْاَرْضَ وَجَعَلَ الظُّلُمٰتِ وَالنُّوْرَ ەۗ ثُمَّ الَّذِيْنَ كَفَرُوْا بِرَبِّهِمْ يَعْدِلُوْنَ

al*h*amdu lill*aa*hi **al**la*dz*ii khalaqa **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*a waja'ala **al***zhzh*ulum*aa*ti wa**al**nnuura tsumma **al**

**Segala puji bagi Allah yang telah menciptakan langit dan bumi, dan menjadikan gelap dan terang, namun demikian orang-orang kafir masih mempersekutukan Tuhan mereka dengan sesuatu.**

6:2

# هُوَ الَّذِيْ خَلَقَكُمْ مِّنْ طِيْنٍ ثُمَّ قَضٰٓى اَجَلًا ۗوَاَجَلٌ مُّسَمًّى عِنْدَهٗ ثُمَّ اَنْتُمْ تَمْتَرُوْنَ

huwa **al**la*dz*ii khalaqakum min *th*iinin tsumma qa*daa* ajalan wa-ajalun musamman 'indahu tsumma antum tamtaruun**a**

Dialah yang menciptakan kamu dari tanah, kemudian Dia menetapkan ajal (kematianmu), dan batas waktu tertentu yang hanya diketahui oleh-Nya. Namun demikian kamu masih meragukannya.

6:3

# وَهُوَ اللّٰهُ فِى السَّمٰوٰتِ وَفِى الْاَرْضِۗ يَعْلَمُ سِرَّكُمْ وَجَهْرَكُمْ وَيَعْلَمُ مَا تَكْسِبُوْنَ

wahuwa **al**l*aa*hu fii **al**ssam*aa*w*aa*ti wafii **a**l-ar*dh*i ya'lamu sirrakum wajahrakum waya'lamu m*aa* taksibuun**a**

Dan Dialah Allah (yang disembah), di langit maupun di bumi; Dia mengetahui apa yang kamu rahasiakan dan apa yang kamu nyatakan dan mengetahui (pula) apa yang kamu kerjakan.

6:4

# وَمَا تَأْتِيْهِمْ مِّنْ اٰيَةٍ مِّنْ اٰيٰتِ رَبِّهِمْ اِلَّا كَانُوْا عَنْهَا مُعْرِضِيْنَ

wam*aa* ta/tiihim min *aa*yatin min *aa*y*aa*ti rabbihim ill*aa* k*aa*nuu 'anh*aa* mu'ri*dh*iin**a**

Dan setiap ayat dari ayat-ayat Tuhan yang sampai kepada mereka (orang kafir), semuanya selalu diingkarinya.

6:5

# فَقَدْ كَذَّبُوْا بِالْحَقِّ لَمَّا جَاۤءَهُمْۗ فَسَوْفَ يَأْتِيْهِمْ اَنْۢبـٰۤؤُا مَا كَانُوْا بِهٖ يَسْتَهْزِءُوْنَ

faqad ka*dzdz*abuu bi**a**l*h*aqqi lamm*aa* j*aa*-ahum fasawfa ya/tiihim anb*aa*u m*aa* k*aa*nuu bihi yastahzi-uun**a**

Sungguh, mereka telah mendustakan kebenaran (Al-Qur'an) ketika sampai kepada mereka, maka kelak akan sampai kepada mereka (kenyataan dari) berita-berita yang selalu mereka perolok-olokkan.

6:6

# اَلَمْ يَرَوْا كَمْ اَهْلَكْنَا مِنْ قَبْلِهِمْ مِّنْ قَرْنٍ مَّكَّنّٰهُمْ فِى الْاَرْضِ مَا لَمْ نُمَكِّنْ لَّكُمْ وَاَرْسَلْنَا السَّمَاۤءَ عَلَيْهِمْ مِّدْرَارًا ۖوَّجَعَلْنَا الْاَنْهٰرَ تَجْرِيْ مِنْ تَحْتِهِمْ فَاَهْلَكْنٰهُمْ بِذُنُوْبِهِمْ وَاَنْش

alam yaraw kam ahlakn*aa* min qablihim min qarnin makkann*aa*hum fii **a**l-ar*dh*i m*aa* lam numakkin lakum wa-arsaln*aa* **al**ssam*aa*-a 'alayhim midr*aa*ran waja'aln*aa* **a**

**Tidakkah mereka memperhatikan berapa banyak generasi sebelum mereka yang telah Kami binasakan, padahal (generasi itu) telah Kami teguhkan kedudukannya di bumi, yaitu keteguhan yang belum pernah Kami berikan kepadamu. Kami curahkan hujan yang lebat untuk m**

6:7

# وَلَوْ نَزَّلْنَا عَلَيْكَ كِتٰبًا فِيْ قِرْطَاسٍ فَلَمَسُوْهُ بِاَيْدِيْهِمْ لَقَالَ الَّذِيْنَ كَفَرُوْٓا اِنْ هٰذَآ اِلَّا سِحْرٌ مُّبِيْنٌ

walaw nazzaln*aa* 'alayka kit*aa*ban fii qir*thaa*sin falamasuuhu bi-aydiihim laq*aa*la **al**la*dz*iina kafaruu in h*aadzaa* ill*aa* si*h*run mubiin**un**

Dan sekiranya Kami turunkan kepadamu (Muhammad) tulisan di atas kertas, sehingga mereka dapat memegangnya dengan tangan mereka sendiri, niscaya orang-orang kafir itu akan berkata, “Ini tidak lain hanyalah sihir yang nyata.”

6:8

# وَقَالُوْا لَوْلَآ اُنْزِلَ عَلَيْهِ مَلَكٌ ۗوَلَوْ اَنْزَلْنَا مَلَكًا لَّقُضِيَ الْاَمْرُ ثُمَّ لَا يُنْظَرُوْنَ

waq*aa*luu lawl*aa* unzila 'alayhi malakun walaw anzaln*aa* malakan laqu*dh*iya **a**l-amru tsumma l*aa* yun*zh*aruun**a**

Dan mereka berkata, “Mengapa tidak diturunkan malaikat kepadanya (Muhammad)?” Jika Kami turunkan malaikat (kepadanya), tentu selesailah urusan itu, tetapi mereka tidak diberi penangguhan (sedikit pun).

6:9

# وَلَوْ جَعَلْنٰهُ مَلَكًا لَّجَعَلْنٰهُ رَجُلًا وَّلَلَبَسْنَا عَلَيْهِمْ مَّا يَلْبِسُوْنَ

walaw ja'aln*aa*hu malakan laja'aln*aa*hu rajulan walalabasn*aa* 'alayhim m*aa* yalbisuun**a**

Dan sekiranya rasul itu Kami jadikan (dari) malaikat, pastilah Kami jadikan dia (berwujud) laki-laki, dan (dengan demikian) pasti Kami akan menjadikan mereka tetap ragu sebagaimana kini mereka ragu.

6:10

# وَلَقَدِ اسْتُهْزِئَ بِرُسُلٍ مِّنْ قَبْلِكَ فَحَاقَ بِالَّذِيْنَ سَخِرُوْا مِنْهُمْ مَّا كَانُوْا بِهٖ يَسْتَهْزِءُوْنَ ࣖ

walaqadi istuhzi-a birusulin min qablika fa*haa*qa bi**a**lla*dz*iina sakhiruu minhum m*aa* k*aa*nuu bihi yastahzi-uun**a**

Dan sungguh, beberapa rasul sebelum engkau (Muhammad) telah diperolok-olokkan, sehingga turunlah azab kepada orang-orang yang mencemoohkan itu sebagai balasan olok-olokan mereka.

6:11

# قُلْ سِيْرُوْا فِى الْاَرْضِ ثُمَّ انْظُرُوْا كَيْفَ كَانَ عَاقِبَةُ الْمُكَذِّبِيْنَ

qul siiruu fii **a**l-ar*dh*i tsumma un*zh*uruu kayfa k*aa*na '*aa*qibatu **a**lmuka*dzdz*ibiin**a**

Katakanlah (Muhammad), “Jelajahilah bumi, kemudian perhatikanlah bagai-mana kesudahan orang-orang yang mendustakan itu.”

6:12

# قُلْ لِّمَنْ مَّا فِى السَّمٰوٰتِ وَالْاَرْضِۗ قُلْ لِّلّٰهِ ۗ كَتَبَ عَلٰى نَفْسِهِ الرَّحْمَةَ ۗ لَيَجْمَعَنَّكُمْ اِلٰى يَوْمِ الْقِيٰمَةِ لَا رَيْبَ فِيْهِۗ اَلَّذِيْنَ خَسِرُوْٓا اَنْفُسَهُمْ فَهُمْ لَا يُؤْمِنُوْنَ

qul liman m*aa* fii **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i qul lill*aa*hi kataba 'al*aa* nafsihi **al**rra*h*mata layajma'annakum il*aa* yawmi **a**lqiy*aa*ma

Katakanlah (Muhammad), “Milik siapakah apa yang di langit dan di bumi?” Katakanlah, “Milik Allah.” Dia telah menetapkan (sifat) kasih sayang pada diri-Nya. Dia sungguh akan mengumpulkan kamu pada hari Kiamat yang tidak diragukan lagi. Orang-orang yang mer

6:13

# ۞ وَلَهٗ مَا سَكَنَ فِى الَّيْلِ وَالنَّهَارِ ۗوَهُوَ السَّمِيْعُ الْعَلِيْمُ

walahu m*aa* sakana fii **al**layli wa**al**nnah*aa*ri wahuwa **al**ssamii'u **a**l'aliim**u**

Dan milik-Nyalah segala apa yang ada pada malam dan siang hari. Dan Dialah Yang Maha Mendengar, Maha Mengetahui.

6:14

# قُلْ اَغَيْرَ اللّٰهِ اَتَّخِذُ وَلِيًّا فَاطِرِ السَّمٰوٰتِ وَالْاَرْضِ وَهُوَ يُطْعِمُ وَلَا يُطْعَمُ ۗ قُلْ اِنِّيْٓ اُمِرْتُ اَنْ اَكُوْنَ اَوَّلَ مَنْ اَسْلَمَ وَلَا تَكُوْنَنَّ مِنَ الْمُشْرِكِيْنَ

qul aghayra **al**l*aa*hi attakhi*dz*u waliyyan f*aath*iri **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i wahuwa yu*th*'imu wal*aa* yu*th*'amu qul innii umirtu an akuuna awwala ma

Katakanlah (Muhammad), “Apakah aku akan menjadikan pelindung selain Allah yang menjadikan langit dan bumi, padahal Dia memberi makan dan tidak diberi makan?” Katakanlah, “Sesungguhnya aku diperintahkan agar aku menjadi orang yang pertama berserah diri (ke

6:15

# قُلْ اِنِّيْٓ اَخَافُ اِنْ عَصَيْتُ رَبِّيْ عَذَابَ يَوْمٍ عَظِيْمٍ

qul innii akh*aa*fu in 'a*sh*aytu rabbii 'a*dzaa*ba yawmin 'a*zh*iim**in**

Katakanlah (Muhammad), “Aku benar-benar takut akan azab hari yang besar (hari Kiamat), jika aku mendurhakai Tuhanku.”

6:16

# مَنْ يُّصْرَفْ عَنْهُ يَوْمَىِٕذٍ فَقَدْ رَحِمَهٗ ۗوَذٰلِكَ الْفَوْزُ الْمُبِيْنُ

man yu*sh*raf 'anhu yawma-i*dz*in faqad ra*h*imahu wa*dzaa*lika **a**lfawzu **a**lmubiin**u**

Barangsiapa dijauhkan dari azab atas dirinya pada hari itu, maka sungguh, Allah telah memberikan rahmat kepadanya. Dan itulah kemenangan yang nyata.

6:17

# وَاِنْ يَّمْسَسْكَ اللّٰهُ بِضُرٍّ فَلَا كَاشِفَ لَهٗٓ اِلَّا هُوَ ۗوَاِنْ يَّمْسَسْكَ بِخَيْرٍ فَهُوَ عَلٰى كُلِّ شَيْءٍ قَدِيْرٌ

wa-in yamsaska **al**l*aa*hu bi*dh*urrin fal*aa* k*aa*syifa lahu ill*aa* huwa wa-in yamsaska bikhayrin fahuwa 'al*aa* kulli syay-in qadiir**un**

Dan jika Allah menimpakan suatu bencana kepadamu, tidak ada yang dapat menghilangkannya selain Dia. Dan jika Dia mendatangkan kebaikan kepadamu, maka Dia Mahakuasa atas segala sesuatu.

6:18

# وَهُوَ الْقَاهِرُ فَوْقَ عِبَادِهٖۗ وَهُوَ الْحَكِيْمُ الْخَبِيْرُ

wahuwa **a**lq*aa*hiru fawqa 'ib*aa*dihi wahuwa **a**l*h*akiimu **a**lkhabiir**u**

Dan Dialah yang berkuasa atas hamba-hamba-Nya. Dan Dia Mahabijaksana, Maha Mengetahui.

6:19

# قُلْ اَيُّ شَيْءٍ اَكْبَرُ شَهَادَةً ۗ قُلِ اللّٰهُ ۗشَهِيْدٌۢ بَيْنِيْ وَبَيْنَكُمْ ۗوَاُوْحِيَ اِلَيَّ هٰذَا الْقُرْاٰنُ لِاُنْذِرَكُمْ بِهٖ وَمَنْۢ بَلَغَ ۗ اَىِٕنَّكُمْ لَتَشْهَدُوْنَ اَنَّ مَعَ اللّٰهِ اٰلِهَةً اُخْرٰىۗ قُلْ لَّآ اَشْهَدُ ۚ قُلْ اِ

qul ayyu syay-in akbaru syah*aa*datan quli **al**l*aa*hu syahiidun baynii wabaynakum wauu*h*iya ilayya h*aadzaa* **a**lqur-*aa*nu li-un*dz*irakum bihi waman balagha a-innakum latasyhaduuna anna ma'a

Katakanlah (Muhammad), “Siapakah yang lebih kuat kesaksiannya?” Katakanlah, “Allah, Dia menjadi saksi antara aku dan kamu. Al-Qur'an ini diwahyukan kepadaku agar dengan itu aku memberi peringatan kepadamu dan kepada orang yang sampai (Al-Qur'an kepadanya)

6:20

# اَلَّذِيْنَ اٰتَيْنٰهُمُ الْكِتٰبَ يَعْرِفُوْنَهٗ كَمَا يَعْرِفُوْنَ اَبْنَاۤءَهُمْۘ اَلَّذِيْنَ خَسِرُوْٓا اَنْفُسَهُمْ فَهُمْ لَا يُؤْمِنُوْنَ ࣖ

**al**la*dz*iina *aa*tayn*aa*humu **a**lkit*aa*ba ya'rifuunahu kam*aa* ya'rifuuna abn*aa*-ahum **al**la*dz*iina khasiruu anfusahum fahum l*aa* yu/minuun**a**

Orang-orang yang telah Kami berikan Kitab kepadanya, mereka mengenalnya (Muhammad) seperti mereka mengenal anak-anaknya sendiri. Orang-orang yang merugikan dirinya, mereka itu tidak beriman (kepada Allah).

6:21

# وَمَنْ اَظْلَمُ مِمَّنِ افْتَرٰى عَلَى اللّٰهِ كَذِبًا اَوْ كَذَّبَ بِاٰيٰتِهٖۗ اِنَّهٗ لَا يُفْلِحُ الظّٰلِمُوْنَ

waman a*zh*lamu mimmani iftar*aa* 'al*aa* **al**l*aa*hi ka*dz*iban aw ka*dzdz*aba bi-*aa*y*aa*tihi innahu l*aa* yufli*h*u **al***zhzhaa*limuun**a**

Dan siapakah yang lebih zalim daripada orang yang mengada-adakan suatu kebohongan terhadap Allah, atau yang mendustakan ayat-ayat-Nya? Sesungguhnya orang-orang yang zalim itu tidak beruntung.

6:22

# وَيَوْمَ نَحْشُرُهُمْ جَمِيْعًا ثُمَّ نَقُوْلُ لِلَّذِيْنَ اَشْرَكُوْٓا اَيْنَ شُرَكَاۤؤُكُمُ الَّذِيْنَ كُنْتُمْ تَزْعُمُوْنَ

wayawma na*h*syuruhum jamii'an tsumma naquulu lilla*dz*iina asyrakuu ayna syurak*aa*ukumu **al**la*dz*iina kuntum taz'umuun**a**

Dan (ingatlah), pada hari ketika Kami mengumpulkan mereka semua kemudian Kami berfirman kepada orang-orang yang menyekutukan Allah, “Di manakah sembahan-sembahanmu yang dahulu kamu sangka (sekutu-sekutu Kami)?”

6:23

# ثُمَّ لَمْ تَكُنْ فِتْنَتُهُمْ اِلَّآ اَنْ قَالُوْا وَاللّٰهِ رَبِّنَا مَا كُنَّا مُشْرِكِيْنَ

tsumma lam takun fitnatuhum ill*aa* an q*aa*luu wa**al**l*aa*hi rabbin*aa* m*aa* kunn*aa* musyrikiin**a**

Kemudian tidaklah ada jawaban bohong mereka, kecuali mengatakan, “Demi Allah, ya Tuhan kami, tidaklah kami mempersekutukan Allah.”

6:24

# اُنْظُرْ كَيْفَ كَذَبُوْا عَلٰٓى اَنْفُسِهِمْ وَضَلَّ عَنْهُمْ مَّا كَانُوْا يَفْتَرُوْنَ

tsumma lam takun fitnatuhum ill*aa* an q*aa*luu wa**al**l*aa*hi rabbin*aa* m*aa* kunn*aa* musyrikiin**a**

Lihatlah, bagaimana mereka berbohong terhadap diri mereka sendiri. Dan sesembahan yang mereka ada-adakan dahulu akan hilang dari mereka.

6:25

# وَمِنْهُمْ مَّنْ يَّسْتَمِعُ اِلَيْكَ ۚوَجَعَلْنَا عَلٰى قُلُوْبِهِمْ اَكِنَّةً اَنْ يَّفْقَهُوْهُ وَفِيْٓ اٰذَانِهِمْ وَقْرًا ۗوَاِنْ يَّرَوْا كُلَّ اٰيَةٍ لَّا يُؤْمِنُوْا بِهَا ۗحَتّٰٓى اِذَا جَاۤءُوْكَ يُجَادِلُوْنَكَ يَقُوْلُ الَّذِيْنَ كَفَرُوْٓا اِ

waminhum man yastami'u ilayka waja'aln*aa* 'al*aa* quluubihim akinnatan an yafqahuuhu wafii *aatsaa*nihim waqran wa-in yaraw kulla *aa*yatin l*aa* yu/minuu bih*aa* *h*att*aa* i*dzaa* j*aa*uuka yuj*aa*

Dan di antara mereka ada yang mendengarkan bacaanmu (Muhammad), dan Kami telah menjadikan hati mereka tertutup (sehingga mereka tidak) memahaminya, dan telinganya tersumbat. Dan kalaupun mereka melihat segala tanda (kebenaran), mereka tetap tidak mau beri

6:26

# وَهُمْ يَنْهَوْنَ عَنْهُ وَيَنْـَٔوْنَ عَنْهُ ۚوَاِنْ يُّهْلِكُوْنَ اِلَّآ اَنْفُسَهُمْ وَمَا يَشْعُرُوْنَ

wahum yanhawna 'anhu wayan-awna 'anhu wa-in yuhlikuuna ill*aa* anfusahum wam*aa* yasy'uruun**a**

Dan mereka melarang (orang lain) mendengarkan (Al-Qur'an) dan mereka sendiri menjauhkan diri daripadanya, dan mereka hanyalah membinasakan diri mereka sendiri, sedang mereka tidak menyadari.

6:27

# وَلَوْ تَرٰٓى اِذْ وُقِفُوْا عَلَى النَّارِ فَقَالُوْا يٰلَيْتَنَا نُرَدُّ وَلَا نُكَذِّبَ بِاٰيٰتِ رَبِّنَا وَنَكُوْنَ مِنَ الْمُؤْمِنِيْنَ

walaw tar*aa* i*dz* wuqifuu 'al*aa* **al**nn*aa*ri faq*aa*luu y*aa* laytan*aa* nuraddu wal*aa* nuka*dzdz*iba bi-*aa*y*aa*ti rabbin*aa* wanakuuna mina **a**lmu/miniin

Dan seandainya engkau (Muhammad) melihat ketika mereka dihadapkan ke neraka, mereka berkata, “Seandainya kami dikembalikan (ke dunia), tentu kami tidak akan mendustakan ayat-ayat Tuhan kami, serta menjadi orang-orang yang beriman.”

6:28

# بَلْ بَدَا لَهُمْ مَّا كَانُوْا يُخْفُوْنَ مِنْ قَبْلُ ۗوَلَوْ رُدُّوْا لَعَادُوْا لِمَا نُهُوْا عَنْهُ وَاِنَّهُمْ لَكٰذِبُوْنَ

bal bad*aa* lahum m*aa* k*aa*nuu yukhfuuna min qablu walaw rudduu la'*aa*duu lim*aa* nuhuu 'anhu wa-innahum lak*aadz*ibuun**a**

Tetapi (sebenarnya) bagi mereka telah nyata kejahatan yang mereka sembunyikan dahulu. Seandainya mereka dikembalikan ke dunia, tentu mereka akan mengulang kembali apa yang telah dilarang mengerjakannya. Mereka itu sungguh pendusta.

6:29

# وَقَالُوْٓا اِنْ هِيَ اِلَّا حَيَاتُنَا الدُّنْيَا وَمَا نَحْنُ بِمَبْعُوْثِيْنَ

waq*aa*luu in hiya ill*aa* *h*ay*aa*tun*aa* **al**dduny*aa* wam*aa* na*h*nu bimab'uutsiin**a**

Dan tentu mereka akan mengatakan (pula), “Hidup hanyalah di dunia ini, dan kita tidak akan dibangkitkan.”

6:30

# وَلَوْ تَرٰٓى اِذْ وُقِفُوْا عَلٰى رَبِّهِمْ ۗ قَالَ اَلَيْسَ هٰذَا بِالْحَقِّ ۗقَالُوْا بَلٰى وَرَبِّنَا ۗقَالَ فَذُوْقُوا الْعَذَابَ بِمَا كُنْتُمْ تَكْفُرُوْنَ ࣖ

walaw tar*aa* i*dz* wuqifuu 'al*aa* rabbihim q*aa*la alaysa h*aadzaa* bi**a**l*h*aqqi q*aa*luu bal*aa* warabbin*aa* q*aa*la fa*dz*uuquu al'a*dzaa*ba bim*aa* kuntum takfuruun

Dan seandainya engkau (Muhammad) melihat ketika mereka dihadapkan kepada Tuhannya (tentulah engkau melihat peristiwa yang mengharukan). Dia berfirman, “Bukankah (kebangkitan) ini benar?” Mereka menjawab, “Sungguh benar, demi Tuhan kami.” Dia berfirman, “R

6:31

# قَدْ خَسِرَ الَّذِيْنَ كَذَّبُوْا بِلِقَاۤءِ اللّٰهِ ۗحَتّٰٓى اِذَا جَاۤءَتْهُمُ السَّاعَةُ بَغْتَةً قَالُوْا يٰحَسْرَتَنَا عَلٰى مَا فَرَّطْنَا فِيْهَاۙ وَهُمْ يَحْمِلُوْنَ اَوْزَارَهُمْ عَلٰى ظُهُوْرِهِمْۗ اَلَا سَاۤءَ مَا يَزِرُوْنَ

qad khasira **al**la*dz*iina ka*dzdz*abuu biliq*aa*-i **al**l*aa*hi *h*att*aa* i*dzaa* j*aa*-at-humu **al**ss*aa*'atu baghtatan q*aa*luu y*aa* *h*asratan

Sungguh rugi orang-orang yang mendustakan pertemuan dengan Allah; sehingga apabila Kiamat datang kepada mereka secara tiba-tiba, mereka berkata, “Alangkah besarnya penyesalan kami terhadap kelalaian kami tentang Kiamat itu,” sambil mereka memikul dosa-dos

6:32

# وَمَا الْحَيٰوةُ الدُّنْيَآ اِلَّا لَعِبٌ وَّلَهْوٌ ۗوَلَلدَّارُ الْاٰخِرَةُ خَيْرٌ لِّلَّذِيْنَ يَتَّقُوْنَۗ اَفَلَا تَعْقِلُوْنَ

wam*aa* **a**l*h*ay*aa*tu **al**dduny*aa* ill*aa* la'ibun walahwun wala**l**dd*aa*ru **a**l-*aa*khirati khayrun lilla*dz*iina yattaquuna afal*aa* ta'qiluun

Dan kehidupan dunia ini hanyalah permainan dan senda gurau. Sedangkan negeri akhirat itu, sungguh lebih baik bagi orang-orang yang bertakwa. Tidakkah kamu mengerti?

6:33

# قَدْ نَعْلَمُ اِنَّهٗ لَيَحْزُنُكَ الَّذِيْ يَقُوْلُوْنَ فَاِنَّهُمْ لَا يُكَذِّبُوْنَكَ وَلٰكِنَّ الظّٰلِمِيْنَ بِاٰيٰتِ اللّٰهِ يَجْحَدُوْنَ

qad na'lamu innahu laya*h*zunuka **al**la*dz*ii yaquuluuna fa-innahum l*aa* yuka*dzdz*ibuunaka wal*aa*kinna **al***zhzhaa*limiina bi-*aa*y*aa*ti **al**l*aa*hi yaj*h*a

Sungguh, Kami mengetahui bahwa apa yang mereka katakan itu menyedihkan hatimu (Muhammad), (janganlah bersedih hati) karena sebenarnya mereka bukan mendustakan engkau, tetapi orang yang zalim itu mengingkari ayat-ayat Allah.

6:34

# وَلَقَدْ كُذِّبَتْ رُسُلٌ مِّنْ قَبْلِكَ فَصَبَرُوْا عَلٰى مَا كُذِّبُوْا وَاُوْذُوْا حَتّٰٓى اَتٰىهُمْ نَصْرُنَا ۚوَلَا مُبَدِّلَ لِكَلِمٰتِ اللّٰهِ ۚوَلَقَدْ جَاۤءَكَ مِنْ نَّبَإِ۟ى الْمُرْسَلِيْنَ

walaqad ku*dzdz*ibat rusulun min qablika fa*sh*abaruu 'al*aa* m*aa* ku*dzdz*ibuu wauu*dz*uu *h*att*aa* at*aa*hum na*sh*run*aa* wal*aa* mubaddila likalim*aa*ti **al**l*aa*hi

Dan sesungguhnya rasul-rasul sebelum engkau pun telah didustakan, tetapi mereka sabar terhadap pendustaan dan penganiayaan (yang dilakukan) terhadap mereka, sampai datang pertolongan Kami kepada mereka. Dan tidak ada yang dapat mengubah kalimat-kalimat (k

6:35

# وَاِنْ كَانَ كَبُرَ عَلَيْكَ اِعْرَاضُهُمْ فَاِنِ اسْتَطَعْتَ اَنْ تَبْتَغِيَ نَفَقًا فِى الْاَرْضِ اَوْ سُلَّمًا فِى السَّمَاۤءِ فَتَأْتِيَهُمْ بِاٰيَةٍ ۗوَلَوْ شَاۤءَ اللّٰهُ لَجَمَعَهُمْ عَلَى الْهُدٰى فَلَا تَكُوْنَنَّ مِنَ الْجٰهِلِيْنَ

wa-in k*aa*na kabura 'alayka i'r*aad*uhum fa-ini ista*th*a'ta an tabtaghiya nafaqan fii **a**l-ar*dh*i aw sullaman fii **al**ssam*aa*-i fata/tiyahum bi-*aa*yatin walaw sy*aa*-a **al**

**Dan jika keberpalingan mereka terasa berat bagimu (Muhammad), maka sekiranya engkau dapat membuat lubang di bumi atau tangga ke langit lalu engkau dapat mendatangkan mukjizat kepada mereka, (maka buatlah). Dan sekiranya Allah menghendaki, tentu Dia jadika**

6:36

# ۞ اِنَّمَا يَسْتَجِيْبُ الَّذِيْنَ يَسْمَعُوْنَ ۗوَالْمَوْتٰى يَبْعَثُهُمُ اللّٰهُ ثُمَّ اِلَيْهِ يُرْجَعُوْنَ

innam*aa* yastajiibu **al**la*dz*iina yasma'uuna wa**a**lmawt*aa* yab'atsuhumu **al**l*aa*hu tsumma ilayhi yurja'uun**a**

Hanya orang-orang yang mendengar sajalah yang mematuhi (seruan Allah), dan orang-orang yang mati, kelak akan dibangkitkan oleh Allah, kemudian kepada-Nya mereka dikembalikan.

6:37

# وَقَالُوْا لَوْلَا نُزِّلَ عَلَيْهِ اٰيَةٌ مِّنْ رَّبِّهٖۗ قُلْ اِنَّ اللّٰهَ قَادِرٌ عَلٰٓى اَنْ يُّنَزِّلَ اٰيَةً وَّلٰكِنَّ اَكْثَرَهُمْ لَا يَعْلَمُوْنَ

waq*aa*luu lawl*aa* nuzzila 'alayhi *aa*yatun min rabbihi qul inna **al**l*aa*ha q*aa*dirun 'al*aa* an yunazzila *aa*yatan wal*aa*kinna aktsarahum l*aa* ya'lamuun**a**

Dan mereka (orang-orang musyrik) berkata, “Mengapa tidak diturunkan kepadanya (Muhammad) suatu mukjizat dari Tuhannya?” Katakanlah, “Sesungguhnya Allah berkuasa menurunkan suatu mukjizat, tetapi kebanyakan mereka tidak mengetahui.”

6:38

# وَمَا مِنْ دَاۤبَّةٍ فِى الْاَرْضِ وَلَا طٰۤىِٕرٍ يَّطِيْرُ بِجَنَاحَيْهِ اِلَّآ اُمَمٌ اَمْثَالُكُمْ ۗمَا فَرَّطْنَا فِى الْكِتٰبِ مِنْ شَيْءٍ ثُمَّ اِلٰى رَبِّهِمْ يُحْشَرُوْنَ

wam*aa* min d*aa*bbatin fii **a**l-ar*dh*i wal*aa* *thaa*-irin ya*th*iiru bijan*aah*ayhi ill*aa* umamun amts*aa*lukum m*aa* farra*th*n*aa* fii **a**lkit*aa*b

Dan tidak ada seekor binatang pun yang ada di bumi dan burung-burung yang terbang dengan kedua sayapnya, melainkan semuanya merupakan umat-umat (juga) seperti kamu. Tidak ada sesuatu pun yang Kami luputkan di dalam Kitab, kemudian kepada Tuhan mereka diku

6:39

# وَالَّذِيْنَ كَذَّبُوْا بِاٰيٰتِنَا صُمٌّ وَّبُكْمٌ فِى الظُّلُمٰتِۗ مَنْ يَّشَاِ اللّٰهُ يُضْلِلْهُ وَمَنْ يَّشَأْ يَجْعَلْهُ عَلٰى صِرَاطٍ مُّسْتَقِيْمٍ

wa**a**lla*dz*iina ka*dzdz*abuu bi-*aa*y*aa*tin*aa* *sh*ummun wabukmun fii **al***zhzh*ulum*aa*ti man yasya-i **al**l*aa*hu yu*dh*lilhu waman yasya/ yaj'alhu 'al*aa*

Dan orang-orang yang mendustakan ayat-ayat Kami adalah tuli, bisu dan berada dalam gelap gulita. Barangsiapa dikehendaki Allah (dalam kesesatan), niscaya disesatkan-Nya. Dan barangsiapa dikehendaki Allah (untuk diberi petunjuk), niscaya Dia menjadikannya

6:40

# قُلْ اَرَءَيْتَكُمْ اِنْ اَتٰىكُمْ عَذَابُ اللّٰهِ اَوْ اَتَتْكُمُ السَّاعَةُ اَغَيْرَ اللّٰهِ تَدْعُوْنَۚ اِنْ كُنْتُمْ صٰدِقِيْنَ

qul ara-aytakum in at*aa*kum 'a*dzaa*bu **al**l*aa*hi aw atatkumu **al**ss*aa*'atu aghayra **al**l*aa*hi tad'uuna in kuntum *shaa*diqiin**a**

Katakanlah (Muhammad), “Terangkanlah kepadaku jika siksaan Allah sampai kepadamu, atau hari Kiamat sampai kepadamu, apakah kamu akan menyeru (tuhan) selain Allah, jika kamu orang yang benar!”

6:41

# بَلْ اِيَّاهُ تَدْعُوْنَ فَيَكْشِفُ مَا تَدْعُوْنَ اِلَيْهِ اِنْ شَاۤءَ وَتَنْسَوْنَ مَا تُشْرِكُوْنَ ࣖ

bal iyy*aa*hu tad'uuna fayaksyifu m*aa* tad'uuna ilayhi in sy*aa*-a watansawna m*aa* tusyrikuun**a**

(Tidak), hanya kepada-Nya kamu minta tolong. Jika Dia menghendaki, Dia hilangkan apa (bahaya) yang kamu mohonkan kepada-Nya, dan kamu tinggalkan apa yang kamu persekutukan (dengan Allah).

6:42

# وَلَقَدْ اَرْسَلْنَآ اِلٰٓى اُمَمٍ مِّنْ قَبْلِكَ فَاَخَذْنٰهُمْ بِالْبَأْسَاۤءِ وَالضَّرَّاۤءِ لَعَلَّهُمْ يَتَضَرَّعُوْنَ

walaqad arsaln*aa* il*aa* umamin min qablika fa-akha*dz*n*aa*hum bi**a**lba/s*aa*-i wa**al***dhdh*arr*aa*\-i la'allahum yata*dh*arra'uun**a**

Dan sungguh, Kami telah mengutus (para rasul) kepada umat-umat sebelum engkau, kemudian Kami siksa mereka dengan (menimpakan) kemelaratan dan kesengsaraan, agar mereka memohon (kepada Allah) dengan kerendahan hati.

6:43

# فَلَوْلَآ اِذْ جَاۤءَهُمْ بَأْسُنَا تَضَرَّعُوْا وَلٰكِنْ قَسَتْ قُلُوْبُهُمْ وَزَيَّنَ لَهُمُ الشَّيْطٰنُ مَا كَانُوْا يَعْمَلُوْنَ

falawl*aa* i*dz* j*aa*-ahum ba/sun*aa* ta*dh*arra'uu wal*aa*kin qasat quluubuhum wazayyana lahumu **al**sysyay*thaa*nu m*aa* k*aa*nuu ya'maluun**a**

Tetapi mengapa mereka tidak memohon (kepada Allah) dengan kerendahan hati ketika siksaan Kami datang menimpa mereka? Bahkan hati mereka telah menjadi keras dan setan pun menjadikan terasa indah bagi mereka apa yang selalu mereka kerjakan.

6:44

# فَلَمَّا نَسُوْا مَا ذُكِّرُوْا بِهٖ فَتَحْنَا عَلَيْهِمْ اَبْوَابَ كُلِّ شَيْءٍۗ حَتّٰٓى اِذَا فَرِحُوْا بِمَآ اُوْتُوْٓا اَخَذْنٰهُمْ بَغْتَةً فَاِذَا هُمْ مُّبْلِسُوْنَ

falamm*aa* nasuu m*aa* *dz*ukkiruu bihi fata*h*n*aa* 'alayhim abw*aa*ba kulli syay-in *h*att*aa* i*dzaa* fari*h*uu bim*aa* uutuu akha*dz*n*aa*hum baghtatan fa-i*dzaa* hum mublisuun

Maka ketika mereka melupakan peringatan yang telah diberikan kepada mereka, Kami pun membukakan semua pintu (kesenangan) untuk mereka. Sehingga ketika mereka bergembira dengan apa yang telah diberikan kepada mereka, Kami siksa mereka secara tiba-tiba, mak

6:45

# فَقُطِعَ دَابِرُ الْقَوْمِ الَّذِيْنَ ظَلَمُوْاۗ وَالْحَمْدُ لِلّٰهِ رَبِّ الْعٰلَمِيْنَ

faqu*th*i'a d*aa*biru **a**lqawmi **al**la*dz*iina *zh*alamuu wa**a**l*h*amdu lill*aa*hi rabbi **a**l'*aa*lamiin**a**

Maka orang-orang yang zalim itu dimusnahkan sampai ke akar-akarnya. Dan segala puji bagi Allah, Tuhan seluruh alam.

6:46

# قُلْ اَرَاَيْتُمْ اِنْ اَخَذَ اللّٰهُ سَمْعَكُمْ وَاَبْصَارَكُمْ وَخَتَمَ عَلٰى قُلُوْبِكُمْ مَّنْ اِلٰهٌ غَيْرُ اللّٰهِ يَأْتِيْكُمْ بِهٖۗ اُنْظُرْ كَيْفَ نُصَرِّفُ الْاٰيٰتِ ثُمَّ هُمْ يَصْدِفُوْنَ

qul ara-aytum in akha*dz*a **al**l*aa*hu sam'akum wa-ab*shaa*rakum wakhatama 'al*aa* quluubikum man il*aa*hun ghayru **al**l*aa*hi ya/tiikum bihi un*zh*ur kayfa nu*sh*arrifu **a**

**Katakanlah (Muhammad), “Terangkanlah kepadaku jika Allah mencabut pendengaran dan penglihatan serta menutup hatimu, siapakah tuhan selain Allah yang kuasa mengembalikannya kepadamu?” Perhatikanlah, bagaimana Kami menjelaskan berulang-ulang (kepada mereka)**

6:47

# قُلْ اَرَاَيْتَكُمْ اِنْ اَتٰىكُمْ عَذَابُ اللّٰهِ بَغْتَةً اَوْ جَهْرَةً هَلْ يُهْلَكُ اِلَّا الْقَوْمُ الظّٰلِمُوْنَ

qul ara-aytakum in at*aa*kum 'a*dzaa*bu **al**l*aa*hi baghtatan aw jahratan hal yuhlaku ill*aa* **a**lqawmu **al***zhzhaa*limuun**a**

Katakanlah (Muhammad), “Terangkanlah kepadaku jika siksaan Allah sampai kepadamu secara tiba-tiba atau terang-terangan, maka adakah yang dibinasakan (Allah) selain orang-orang yang zalim?”

6:48

# وَمَا نُرْسِلُ الْمُرْسَلِيْنَ اِلَّا مُبَشِّرِيْنَ وَمُنْذِرِيْنَۚ فَمَنْ اٰمَنَ وَاَصْلَحَ فَلَا خَوْفٌ عَلَيْهِمْ وَلَا هُمْ يَحْزَنُوْنَ

wam*aa* nursilu **a**lmursaliina ill*aa* mubasysyiriina wamun*dz*iriina faman *aa*mana wa-a*sh*la*h*a fal*aa* khawfun 'alayhim wal*aa* hum ya*h*zanuun**a**

Para rasul yang Kami utus itu adalah untuk memberi kabar gembira dan memberi peringatan. Barangsiapa beriman dan mengadakan perbaikan, maka tidak ada rasa takut pada mereka dan mereka tidak bersedih hati.

6:49

# وَالَّذِيْنَ كَذَّبُوْا بِاٰيٰتِنَا يَمَسُّهُمُ الْعَذَابُ بِمَا كَانُوْا يَفْسُقُوْنَ

wa**a**lla*dz*iina ka*dzdz*abuu bi-*aa*y*aa*tin*aa* yamassuhumu **a**l'a*dzaa*bu bim*aa* k*aa*nuu yafsuquun**a**

Dan orang-orang yang mendustakan ayat-ayat Kami akan ditimpa azab karena mereka selalu berbuat fasik (berbuat dosa).

6:50

# قُلْ لَّآ اَقُوْلُ لَكُمْ عِنْدِيْ خَزَاۤىِٕنُ اللّٰهِ وَلَآ اَعْلَمُ الْغَيْبَ وَلَآ اَقُوْلُ لَكُمْ اِنِّيْ مَلَكٌۚ اِنْ اَتَّبِعُ اِلَّا مَا يُوْحٰٓى اِلَيَّۗ قُلْ هَلْ يَسْتَوِى الْاَعْمٰى وَالْبَصِيْرُۗ اَفَلَا تَتَفَكَّرُوْنَ ࣖ

qul l*aa* aquulu lakum 'indii khaz*aa*-inu **al**l*aa*hi wal*aa* a'lamu **a**lghayba wal*aa* aquulu lakum innii malakun in attabi'u ill*aa* m*aa* yuu*haa* ilayya qul hal yastawii **a<**

Katakanlah (Muhammad), “Aku tidak mengatakan kepadamu, bahwa perbendaharaan Allah ada padaku, dan aku tidak mengetahui yang gaib dan aku tidak (pula) mengatakan kepadamu bahwa aku malaikat. Aku hanya mengikuti apa yang diwahyukan kepadaku.” Katakanlah, “A

6:51

# وَاَنْذِرْ بِهِ الَّذِيْنَ يَخَافُوْنَ اَنْ يُّحْشَرُوْٓا اِلٰى رَبِّهِمْ لَيْسَ لَهُمْ مِّنْ دُوْنِهٖ وَلِيٌّ وَّلَا شَفِيْعٌ لَّعَلَّهُمْ يَتَّقُوْنَ

wa-an*dz*ir bihi **al**la*dz*iina yakh*aa*fuuna an yu*h*syaruu il*aa* rabbihim laysa lahum min duunihi waliyyun wal*aa* syafii'un la'allahum yattaquun**a**

Peringatkanlah dengannya (Al-Qur'an) itu kepada orang yang takut akan dikumpulkan menghadap Tuhannya (pada hari Kiamat), tidak ada bagi mereka pelindung dan pemberi syafaat (pertolongan) selain Allah, agar mereka bertakwa.

6:52

# وَلَا تَطْرُدِ الَّذِيْنَ يَدْعُوْنَ رَبَّهُمْ بِالْغَدٰوةِ وَالْعَشِيِّ يُرِيْدُوْنَ وَجْهَهٗ ۗمَا عَلَيْكَ مِنْ حِسَابِهِمْ مِّنْ شَيْءٍ وَّمَا مِنْ حِسَابِكَ عَلَيْهِمْ مِّنْ شَيْءٍ فَتَطْرُدَهُمْ فَتَكُوْنَ مِنَ الظّٰلِمِيْنَ

wal*aa* ta*th*rudi **al**la*dz*iina yad'uuna rabbahum bi**a**lghad*aa*ti wa**a**l'asyiyyi yuriiduuna wajhahu m*aa* 'alayka min *h*is*aa*bihim min syay-in wam*aa* min *h*is

Janganlah engkau mengusir orang-orang yang menyeru Tuhannya pada pagi dan petang hari, mereka mengharapkan keridaan-Nya. Engkau tidak memikul tanggung jawab sedikit pun terhadap perbuatan mereka dan mereka tidak memikul tanggung jawab sedikit pun terhadap

6:53

# وَكَذٰلِكَ فَتَنَّا بَعْضَهُمْ بِبَعْضٍ لِّيَقُوْلُوْٓا اَهٰٓؤُلَاۤءِ مَنَّ اللّٰهُ عَلَيْهِمْ مِّنْۢ بَيْنِنَاۗ اَلَيْسَ اللّٰهُ بِاَعْلَمَ بِالشّٰكِرِيْنَ

waka*dzaa*lika fatann*aa* ba'*dh*ahum biba'*dh*in liyaquuluu ah*aa*ul*aa*-i manna **al**l*aa*hu 'alayhim min baynin*aa* alaysa **al**l*aa*hu bi-a'lama bi**al**sysy*aa*

*Demikianlah Kami telah menguji sebagian mereka (orang yang kaya) dengan sebagian yang lain (orang yang miskin), agar mereka (orang yang kaya itu) berkata, “Orang-orang semacam inikah di antara kita yang diberi anugerah oleh Allah?” (Allah berfirman), “Tid*

6:54

# وَاِذَا جَاۤءَكَ الَّذِيْنَ يُؤْمِنُوْنَ بِاٰيٰتِنَا فَقُلْ سَلٰمٌ عَلَيْكُمْ كَتَبَ رَبُّكُمْ عَلٰى نَفْسِهِ الرَّحْمَةَۙ اَنَّهٗ مَنْ عَمِلَ مِنْكُمْ سُوْۤءًاۢ بِجَهَالَةٍ ثُمَّ تَابَ مِنْۢ بَعْدِهٖ وَاَصْلَحَ فَاَنَّهٗ غَفُوْرٌ رَّحِيْمٌ

wa-i*dzaa* j*aa*-aka **al**la*dz*iina yu/minuuna bi-*aa*y*aa*tin*aa* faqul sal*aa*mun 'alaykum kataba rabbukum 'al*aa* nafsihi **al**rra*h*mata annahu man 'amila minkum suu-an bijah

Dan apabila orang-orang yang beriman kepada ayat-ayat Kami datang kepadamu, maka katakanlah, “Salamun ‘alaikum (selamat sejahtera untuk kamu).” Tuhanmu telah menetapkan sifat kasih sayang pada diri-Nya, (yaitu) barang-siapa berbuat kejahatan di antara ka

6:55

# وَكَذٰلِكَ نُفَصِّلُ الْاٰيٰتِ وَلِتَسْتَبِيْنَ سَبِيْلُ الْمُجْرِمِيْنَ ࣖ

waka*dzaa*lika nufa*shsh*ilu **a**l-*aa*y*aa*ti walitastabiina sabiilu **a**lmujrimiin**a**

Dan demikianlah Kami terangkan ayat-ayat Al-Qur'an, (agar terlihat jelas jalan orang-orang yang saleh) dan agar terlihat jelas (pula) jalan orang-orang yang berdosa.

6:56

# قُلْ اِنِّيْ نُهِيْتُ اَنْ اَعْبُدَ الَّذِيْنَ تَدْعُوْنَ مِنْ دُوْنِ اللّٰهِ ۗ قُلْ لَّآ اَتَّبِعُ اَهْوَاۤءَكُمْۙ قَدْ ضَلَلْتُ اِذًا وَّمَآ اَنَا۠ مِنَ الْمُهْتَدِيْنَ

qul innii nuhiitu an a'buda **al**la*dz*iina tad'uuna min duuni **al**l*aa*hi qul l*aa* attabi'u ahw*aa*-akum qad *dh*alaltu i*dz*an wam*aa* an*aa* mina **a**lmuhtadiin

Katakanlah (Muhammad), “Aku dilarang menyembah tuhan-tuhan yang kamu sembah selain Allah.” Katakanlah, “Aku tidak akan mengikuti keinginanmu. Jika berbuat demikian, sungguh tersesatlah aku, dan aku tidak termasuk orang yang mendapat petunjuk.”

6:57

# قُلْ اِنِّيْ عَلٰى بَيِّنَةٍ مِّنْ رَّبِّيْ وَكَذَّبْتُمْ بِهٖۗ مَا عِنْدِيْ مَا تَسْتَعْجِلُوْنَ بِهٖۗ اِنِ الْحُكْمُ اِلَّا لِلّٰهِ ۗيَقُصُّ الْحَقَّ وَهُوَ خَيْرُ الْفَاصِلِيْنَ

qul innii 'al*aa* bayyinatin min rabbii waka*dzdz*abtum bihi m*aa* 'indii m*aa* tasta'jiluuna bihi ini **a**l*h*ukmu ill*aa* lill*aa*hi yaqu*shsh*u **a**l*h*aqqa wahuwa khayru

Katakanlah (Muhammad), “Aku (berada) di atas keterangan yang nyata (Al-Qur'an) dari Tuhanku sedang kamu mendustakannya. Bukanlah kewenanganku (untuk menurunkan azab) yang kamu tuntut untuk disegerakan kedatangannya. Menetapkan (hukum itu) hanyalah hak All

6:58

# قُلْ لَّوْ اَنَّ عِنْدِيْ مَا تَسْتَعْجِلُوْنَ بِهٖ لَقُضِيَ الْاَمْرُ بَيْنِيْ وَبَيْنَكُمْ ۗوَاللّٰهُ اَعْلَمُ بِالظّٰلِمِيْنَ

qul law anna 'indii m*aa* tasta'jiluuna bihi laqu*dh*iya **a**l-amru baynii wabaynakum wa**al**l*aa*hu a'lamu bi**al***zhzhaa*limiin**a**

Katakanlah (Muhammad), “Seandainya ada padaku apa (azab) yang kamu minta agar disegerakan kedatangannya, tentu selesailah segala perkara antara aku dan kamu.” Dan Allah lebih mengetahui tentang orang-orang yang zalim.

6:59

# ۞ وَعِنْدَهٗ مَفَاتِحُ الْغَيْبِ لَا يَعْلَمُهَآ اِلَّا هُوَۗ وَيَعْلَمُ مَا فِى الْبَرِّ وَالْبَحْرِۗ وَمَا تَسْقُطُ مِنْ وَّرَقَةٍ اِلَّا يَعْلَمُهَا وَلَا حَبَّةٍ فِيْ ظُلُمٰتِ الْاَرْضِ وَلَا رَطْبٍ وَّلَا يَابِسٍ اِلَّا فِيْ كِتٰبٍ مُّبِيْنٍ

wa'indahu maf*aa*ti*h*u **a**lghaybi l*aa* ya'lamuh*aa* ill*aa* huwa waya'lamu m*aa* fii **a**lbarri wa**a**lba*h*ri wam*aa* tasqu*th*u min waraqatin ill*aa* ya'lamuh

Dan kunci-kunci semua yang gaib ada pada-Nya; tidak ada yang mengetahui selain Dia. Dia mengetahui apa yang ada di darat dan di laut. Tidak ada sehelai daun pun yang gugur yang tidak diketahui-Nya. Tidak ada sebutir biji pun dalam kegelapan bumi dan tidak

6:60

# وَهُوَ الَّذِيْ يَتَوَفّٰىكُمْ بِالَّيْلِ وَيَعْلَمُ مَا جَرَحْتُمْ بِالنَّهَارِ ثُمَّ يَبْعَثُكُمْ فِيْهِ لِيُقْضٰٓى اَجَلٌ مُّسَمًّىۚ ثُمَّ اِلَيْهِ مَرْجِعُكُمْ ثُمَّ يُنَبِّئُكُمْ بِمَا كُنْتُمْ تَعْمَلُوْنَ ࣖ

wahuwa **al**la*dz*ii yatawaff*aa*kum bi**a**llayli waya'lamu m*aa* jara*h*tum bi**al**nnah*aa*ri tsumma yab'atsukum fiihi liyuq*daa* ajalun musamman tsumma ilayhi marji'ukum tsumma yuna

Dan Dialah yang menidurkan kamu pada malam hari dan Dia mengetahui apa yang kamu kerjakan pada siang hari. Kemudian Dia membangunkan kamu pada siang hari untuk disempurnakan umurmu yang telah ditetapkan. Kemudian kepada-Nya tempat kamu kembali, lalu Dia m

6:61

# وَهُوَ الْقَاهِرُ فَوْقَ عِبَادِهٖ وَيُرْسِلُ عَلَيْكُمْ حَفَظَةً ۗحَتّٰٓى اِذَا جَاۤءَ اَحَدَكُمُ الْمَوْتُ تَوَفَّتْهُ رُسُلُنَا وَهُمْ لَا يُفَرِّطُوْنَ

wahuwa **a**lq*aa*hiru fawqa 'ib*aa*dihi wayursilu 'alaykum *h*afa*zh*atan *h*att*aa* i*dzaa* j*aa*-a a*h*adakumu **a**lmawtu tawaffat-hu rusulun*aa* wahum l*aa* yufarri*th*

Dan Dialah Penguasa mutlak atas semua hamba-Nya, dan diutus-Nya kepadamu malaikat-malaikat penjaga, sehingga apabila kematian datang kepada salah seorang di antara kamu, malaikat-malaikat Kami mencabut nyawanya, dan mereka tidak melalaikan tugasnya.

6:62

# ثُمَّ رُدُّوْٓا اِلَى اللّٰهِ مَوْلٰىهُمُ الْحَقِّۗ اَلَا لَهُ الْحُكْمُ وَهُوَ اَسْرَعُ الْحَاسِبِيْنَ

tsumma rudduu il*aa* **al**l*aa*hi mawl*aa*humu **a**l*h*aqqi **a**l*aa* lahu **a**l*h*ukmu wahuwa asra'u **a**l*haa*sibiin**a**

Kemudian mereka (hamba-hamba Allah) dikembalikan kepada Allah, penguasa mereka yang sebenarnya. Ketahuilah bahwa segala hukum (pada hari itu) ada pada-Nya. Dan Dialah pembuat perhitungan yang paling cepat.

6:63

# قُلْ مَنْ يُّنَجِّيْكُمْ مِّنْ ظُلُمٰتِ الْبَرِّ وَالْبَحْرِ تَدْعُوْنَهٗ تَضَرُّعًا وَّخُفْيَةً ۚ لَىِٕنْ اَنْجٰىنَا مِنْ هٰذِهٖ لَنَكُوْنَنَّ مِنَ الشّٰكِرِيْنَ

qul man yunajjiikum min *zh*ulum*aa*ti **a**lbarri wa**a**lba*h*ri tad'uunahu ta*dh*arru'an wakhufyatan la-in anj*aa*n*aa* min h*aadz*ihi lanakuunanna mina **al**sysy*aa*kiriin

Katakanlah (Muhammad), “Siapakah yang dapat menyelamatkan kamu dari bencana di darat dan di laut, ketika kamu berdoa kepada-Nya dengan rendah hati dan dengan suara yang lembut?” (Dengan mengatakan), “Sekiranya Dia menyelamatkan kami dari (bencana) ini, te

6:64

# قُلِ اللّٰهُ يُنَجِّيْكُمْ مِّنْهَا وَمِنْ كُلِّ كَرْبٍ ثُمَّ اَنْتُمْ تُشْرِكُوْنَ

quli **al**l*aa*hu yunajjiikum minh*aa* wamin kulli karbin tsumma antum tusyrikuun**a**

Katakanlah (Muhammad), “Allah yang menyelamatkan kamu dari bencana itu dan dari segala macam kesusahan, namun kemudian kamu (kembali) mempersekutukan-Nya.”

6:65

# قُلْ هُوَ الْقَادِرُ عَلٰٓى اَنْ يَّبْعَثَ عَلَيْكُمْ عَذَابًا مِّنْ فَوْقِكُمْ اَوْ مِنْ تَحْتِ اَرْجُلِكُمْ اَوْ يَلْبِسَكُمْ شِيَعًا وَّيُذِيْقَ بَعْضَكُمْ بَأْسَ بَعْضٍۗ اُنْظُرْ كَيْفَ نُصَرِّفُ الْاٰيٰتِ لَعَلَّهُمْ يَفْقَهُوْنَ

qul huwa **a**lq*aa*diru 'al*aa* an yab'atsa 'alaykum 'a*dzaa*ban min fawqikum aw min ta*h*ti arjulikum aw yalbisakum syiya'an wayu*dz*iiqa ba'*dh*akum ba/sa ba'*dh*in un*zh*ur kayfa nu*sh*arrifu <

Katakanlah (Muhammad), “Dialah yang berkuasa mengirimkan azab kepadamu, dari atas atau dari bawah kakimu atau Dia mencampurkan kamu dalam golongan-golongan (yang saling bertentangan) dan merasakan kepada sebagian kamu keganasan sebagian yang lain.” Perhat

6:66

# وَكَذَّبَ بِهٖ قَوْمُكَ وَهُوَ الْحَقُّۗ قُلْ لَّسْتُ عَلَيْكُمْ بِوَكِيْلٍ ۗ

waka*dzdz*aba bihi qawmuka wahuwa **a**l*h*aqqu qul lastu 'alaykum biwakiil**in**

Dan kaummu mendustakannya (azab) padahal (azab) itu benar adanya. Katakanlah (Muhammad), “Aku ini bukanlah penanggung jawab kamu.”

6:67

# لِكُلِّ نَبَاٍ مُّسْتَقَرٌّ وَّسَوْفَ تَعْلَمُوْنَ

likulli naba-in mustaqarrun wasawfa ta'lamuun**a**

Setiap berita (yang dibawa oleh rasul) ada (waktu) terjadinya dan kelak kamu akan mengetahui.

6:68

# وَاِذَا رَاَيْتَ الَّذِيْنَ يَخُوْضُوْنَ فِيْٓ اٰيٰتِنَا فَاَعْرِضْ عَنْهُمْ حَتّٰى يَخُوْضُوْا فِيْ حَدِيْثٍ غَيْرِهٖۗ وَاِمَّا يُنْسِيَنَّكَ الشَّيْطٰنُ فَلَا تَقْعُدْ بَعْدَ الذِّكْرٰى مَعَ الْقَوْمِ الظّٰلِمِيْنَ

wa-i*dzaa* ra-ayta **al**la*dz*iina yakhuu*dh*uuna fii *aa*y*aa*tin*aa* fa-a'ri*dh* 'anhum *h*att*aa* yakhuu*dh*uu fii *h*adiitsin ghayrihi wa-imm*aa* yunsiyannaka **al**

**Apabila engkau (Muhammad) melihat orang-orang memperolok-olokkan ayat-ayat Kami, maka tinggalkanlah mereka hingga mereka beralih ke pembicaraan lain. Dan jika setan benar-benar menjadikan engkau lupa (akan larangan ini), setelah ingat kembali janganlah en**

6:69

# وَمَا عَلَى الَّذِيْنَ يَتَّقُوْنَ مِنْ حِسَابِهِمْ مِّنْ شَيْءٍ وَّلٰكِنْ ذِكْرٰى لَعَلَّهُمْ يَتَّقُوْنَ

wam*aa* 'al*aa* **al**la*dz*iina yattaquuna min *h*is*aa*bihim min syay-in wal*aa*kin *dz*ikr*aa* la'allahum yattaquun**a**

Orang-orang yang bertakwa tidak ada tanggung jawab sedikit pun atas (dosa-dosa) mereka; tetapi (berkewajiban) mengingatkan agar mereka (juga) bertakwa.

6:70

# وَذَرِ الَّذِيْنَ اتَّخَذُوْا دِيْنَهُمْ لَعِبًا وَّلَهْوًا وَّغَرَّتْهُمُ الْحَيٰوةُ الدُّنْيَا وَذَكِّرْ بِهٖٓ اَنْ تُبْسَلَ نَفْسٌۢ بِمَا كَسَبَتْۖ لَيْسَ لَهَا مِنْ دُوْنِ اللّٰهِ وَلِيٌّ وَّلَا شَفِيْعٌ ۚوَاِنْ تَعْدِلْ كُلَّ عَدْلٍ لَّا يُؤْخَذْ مِ

wa*dz*ari **al**la*dz*iina ittakha*dz*uu diinahum la'iban walahwan wagharrat-humu **a**l*h*ay*aa*tu **al**dduny*aa* wa*dz*akkir bihi an tubsala nafsun bim*aa* kasabat laysa lah

Tinggalkanlah orang-orang yang menjadikan agamanya sebagai permainan dan senda gurau, dan mereka telah tertipu oleh kehidupan dunia. Peringatkanlah (mereka) dengan Al-Qur'an agar setiap orang tidak terjerumus (ke dalam neraka), karena perbuatannya sendiri

6:71

# قُلْ اَنَدْعُوْا مِنْ دُوْنِ اللّٰهِ مَا لَا يَنْفَعُنَا وَلَا يَضُرُّنَا وَنُرَدُّ عَلٰٓى اَعْقَابِنَا بَعْدَ اِذْ هَدٰىنَا اللّٰهُ كَالَّذِى اسْتَهْوَتْهُ الشَّيٰطِيْنُ فِى الْاَرْضِ حَيْرَانَ لَهٗٓ اَصْحٰبٌ يَّدْعُوْنَهٗٓ اِلَى الْهُدَى ائْتِنَا ۗ قُلْ

qul anad'uu min duuni **al**l*aa*hi m*aa* l*aa* yanfa'un*aa* wal*aa* ya*dh*urrun*aa* wanuraddu 'al*aa* a'q*aa*bin*aa* ba'da i*dz* had*aa*n*aa* **al**l*aa*hu ka

Katakanlah (Muhammad), “Apakah kita akan memohon kepada sesuatu selain Allah, yang tidak dapat memberi manfaat dan tidak (pula) mendatangkan mudarat kepada kita, dan (apakah) kita akan dikembalikan ke belakang, setelah Allah memberi petunjuk kepada kita,

6:72

# وَاَنْ اَقِيْمُوا الصَّلٰوةَ وَاتَّقُوْهُۗ وَهُوَ الَّذِيْٓ اِلَيْهِ تُحْشَرُوْنَ

wa-an aqiimuu **al***shsh*al*aa*ta wa**i**ttaquuhu wahuwa **al**la*dz*ii ilayhi tu*h*syaruun**a**

dan agar melaksanakan salat serta bertakwa kepada-Nya.” Dan Dialah Tuhan yang kepada-Nya kamu semua akan dihimpun.

6:73

# وَهُوَ الَّذِيْ خَلَقَ السَّمٰوٰتِ وَالْاَرْضَ بِالْحَقِّۗ وَيَوْمَ يَقُوْلُ كُنْ فَيَكُوْنُۚ قَوْلُهُ الْحَقُّۗ وَلَهُ الْمُلْكُ يَوْمَ يُنْفَخُ فِى الصُّوْرِۗ عٰلِمُ الْغَيْبِ وَالشَّهَادَةِ وَهُوَ الْحَكِيْمُ الْخَبِيْرُ

wahuwa **al**la*dz*ii khalaqa **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*a bi**a**l*h*aqqi wayawma yaquulu kun fayakuunu qawluhu **a**l*h*aqqu walahu **a**

**Dialah yang menciptakan langit dan bumi dengan hak (benar), ketika Dia berkata, “Jadilah!” Maka jadilah sesuatu itu. Firman-Nya adalah benar, dan milik-Nyalah segala kekuasaan pada waktu sangkakala ditiup. Dia mengetahui yang gaib dan yang nyata. Dialah Y**

6:74

# ۞ وَاِذْ قَالَ اِبْرٰهِيْمُ لِاَبِيْهِ اٰزَرَ اَتَتَّخِذُ اَصْنَامًا اٰلِهَةً ۚاِنِّيْٓ اَرٰىكَ وَقَوْمَكَ فِيْ ضَلٰلٍ مُّبِيْنٍ

wa-i*dz* q*aa*la ibr*aa*hiimu li-abiihi *aa*zara atattakhi*dz*u a*sh*n*aa*man *aa*lihatan innii ar*aa*ka waqawmaka fii *dh*al*aa*lin mubiin**in**

Dan (ingatlah) ketika Ibrahim berkata kepada ayahnya Azar, ”Pantaskah engkau menjadikan berhala-berhala itu sebagai tuhan? Sesungguhnya aku melihat engkau dan kaummu dalam kesesatan yang nyata.”

6:75

# وَكَذٰلِكَ نُرِيْٓ اِبْرٰهِيْمَ مَلَكُوْتَ السَّمٰوٰتِ وَالْاَرْضِ وَلِيَكُوْنَ مِنَ الْمُوْقِنِيْنَ

waka*dzaa*lika nurii ibr*aa*hiima malakuuta **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i waliyakuuna mina **a**lmuuqiniin**a**

Dan demikianlah Kami memperlihatkan kepada Ibrahim kekuasaan (Kami yang terdapat) di langit dan di bumi, dan agar dia termasuk orang-orang yang yakin.

6:76

# فَلَمَّا جَنَّ عَلَيْهِ الَّيْلُ رَاٰ كَوْكَبًا ۗقَالَ هٰذَا رَبِّيْۚ فَلَمَّآ اَفَلَ قَالَ لَآ اُحِبُّ الْاٰفِلِيْنَ

falamm*aa* janna 'alayhi **al**laylu ra*aa* kawkaban q*aa*la h*aadzaa* rabbii falamm*aa* afala q*aa*la l*aa* u*h*ibbu **a**l-*aa*filiin**a**

Ketika malam telah menjadi gelap, dia (Ibrahim) melihat sebuah bintang (lalu) dia berkata, “Inilah Tuhanku.” Maka ketika bintang itu terbenam dia berkata, “Aku tidak suka kepada yang terbenam.”

6:77

# فَلَمَّا رَاَ الْقَمَرَ بَازِغًا قَالَ هٰذَا رَبِّيْ ۚفَلَمَّآ اَفَلَ قَالَ لَىِٕنْ لَّمْ يَهْدِنِيْ رَبِّيْ لَاَكُوْنَنَّ مِنَ الْقَوْمِ الضَّاۤلِّيْنَ

falamm*aa* ra*aa* **a**lqamara b*aa*zighan q*aa*la h*aadzaa* rabbii falamm*aa* afala q*aa*la la-in lam yahdinii rabbii la-akuunanna mina **a**lqawmi **al***dhdhaa*lliin

Lalu ketika dia melihat bulan terbit dia berkata, “Inilah Tuhanku.” Tetapi ketika bulan itu terbenam dia berkata, “Sungguh, jika Tuhanku tidak memberi petunjuk kepadaku, pastilah aku termasuk orang-orang yang sesat.”

6:78

# فَلَمَّا رَاَ الشَّمْسَ بَازِغَةً قَالَ هٰذَا رَبِّيْ هٰذَآ اَكْبَرُۚ فَلَمَّآ اَفَلَتْ قَالَ يٰقَوْمِ اِنِّيْ بَرِيْۤءٌ مِّمَّا تُشْرِكُوْنَ

falamm*aa* ra*aa* **al**sysyamsa b*aa*zighatan q*aa*la h*aadzaa* rabbii h*aadzaa* akbaru falamm*aa* afalat q*aa*la y*aa* qawmi innii barii-un mimm*aa* tusyrikuun**a**

Kemudian ketika dia melihat matahari terbit, dia berkata, “Inilah Tuhanku, ini lebih besar.” Tetapi ketika matahari terbenam, dia berkata, “Wahai kaumku! Sungguh, aku berlepas diri dari apa yang kamu persekutukan.”

6:79

# اِنِّيْ وَجَّهْتُ وَجْهِيَ لِلَّذِيْ فَطَرَ السَّمٰوٰتِ وَالْاَرْضَ حَنِيْفًا وَّمَآ اَنَا۠ مِنَ الْمُشْرِكِيْنَۚ

innii wajjahtu wajhiya lilla*dz*ii fa*th*ara **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*a *h*aniifan wam*aa* an*aa* mina **a**lmusyrikiin**a**

Aku hadapkan wajahku kepada (Allah) yang menciptakan langit dan bumi dengan penuh kepasrahan (mengikuti) agama yang benar, dan aku bukanlah termasuk orang-orang musyrik.

6:80

# وَحَاۤجَّهٗ قَوْمُهٗ ۗقَالَ اَتُحَاۤجُّوْۤنِّيْ فِى اللّٰهِ وَقَدْ هَدٰىنِۗ وَلَآ اَخَافُ مَا تُشْرِكُوْنَ بِهٖٓ اِلَّآ اَنْ يَّشَاۤءَ رَبِّيْ شَيْـًٔا ۗوَسِعَ رَبِّيْ كُلَّ شَيْءٍ عِلْمًا ۗ اَفَلَا تَتَذَكَّرُوْنَ

wa*haa*jjahu qawmuhu q*aa*la atu*haa*jjuunnii fii **al**l*aa*hi waqad had*aa*ni wal*aa* akh*aa*fu m*aa* tusyrikuuna bihi ill*aa* an yasy*aa*-a rabbii syay-an wasi'a rabbii kulla syay-in 'ilman

Dan kaumnya membantahnya. Dia (Ibrahim) berkata, “Apakah kamu hendak membantahku tentang Allah, padahal Dia benar-benar telah memberi petunjuk kepadaku? Aku tidak takut kepada (malapetaka dari) apa yang kamu persekutukan dengan Allah, kecuali Tuhanku meng

6:81

# وَكَيْفَ اَخَافُ مَآ اَشْرَكْتُمْ وَلَا تَخَافُوْنَ اَنَّكُمْ اَشْرَكْتُمْ بِاللّٰهِ مَا لَمْ يُنَزِّلْ بِهٖ عَلَيْكُمْ سُلْطٰنًا ۗفَاَيُّ الْفَرِيْقَيْنِ اَحَقُّ بِالْاَمْنِۚ اِنْ كُنْتُمْ تَعْلَمُوْنَۘ

wakayfa akh*aa*fu m*aa* asyraktum wal*aa* takh*aa*fuuna annakum asyraktum bi**al**l*aa*hi m*aa* lam yunazzil bihi 'alaykum sul*thaa*nan fa-ayyu **a**lfariiqayni a*h*aqqu bi**a<**

Bagaimana aku takut kepada apa yang kamu persekutukan (dengan Allah), padahal kamu tidak takut dengan apa yang Allah sendiri tidak menurunkan keterangan kepadamu untuk mempersekutukan-Nya. Manakah dari kedua golongan itu yang lebih berhak mendapat keamana

6:82

# اَلَّذِيْنَ اٰمَنُوْا وَلَمْ يَلْبِسُوْٓا اِيْمَانَهُمْ بِظُلْمٍ اُولٰۤىِٕكَ لَهُمُ الْاَمْنُ وَهُمْ مُّهْتَدُوْنَ ࣖ

**al**la*dz*iina *aa*manuu walam yalbisuu iim*aa*nahum bi*zh*ulmin ul*aa*-ika lahumu **a**l-amnu wahum muhtaduun**a**

Orang-orang yang beriman dan tidak mencampuradukkan iman mereka dengan syirik, mereka itulah orang-orang yang mendapat rasa aman dan mereka mendapat petunjuk.

6:83

# وَتِلْكَ حُجَّتُنَآ اٰتَيْنٰهَآ اِبْرٰهِيْمَ عَلٰى قَوْمِهٖۗ نَرْفَعُ دَرَجٰتٍ مَّنْ نَّشَاۤءُۗ اِنَّ رَبَّكَ حَكِيْمٌ عَلِيْمٌ

watilka *h*ujjatun*aa* *aa*tayn*aa*h*aa* ibr*aa*hiima 'al*aa* qawmihi narfa'u daraj*aa*tin man nasy*aa*u inna rabbaka *h*akiimun 'aliim**un**

Dan itulah keterangan Kami yang Kami berikan kepada Ibrahim untuk menghadapi kaumnya. Kami tinggikan derajat siapa yang Kami kehendaki. Sesungguhnya Tuhanmu Mahabijaksana, Maha Mengetahui.

6:84

# وَوَهَبْنَا لَهٗٓ اِسْحٰقَ وَيَعْقُوْبَۗ كُلًّا هَدَيْنَا وَنُوْحًا هَدَيْنَا مِنْ قَبْلُ وَمِنْ ذُرِّيَّتِهٖ دَاوٗدَ وَسُلَيْمٰنَ وَاَيُّوْبَ وَيُوْسُفَ وَمُوْسٰى وَهٰرُوْنَ ۗوَكَذٰلِكَ نَجْزِى الْمُحْسِنِيْنَۙ

wawahabn*aa* lahu is*haa*qa waya'quuba kullan hadayn*aa* wanuu*h*an hadayn*aa* min qablu wamin *dz*urriyyatihi d*aa*wuuda wasulaym*aa*na wa-ayyuuba wayuusufa wamuus*aa* wah*aa*ruuna waka*dzaa*lika naj

Dan Kami telah menganugerahkan Ishak dan Yakub kepadanya. Kepada masing-masing telah Kami beri petunjuk; dan sebelum itu Kami telah memberi petunjuk kepada Nuh, dan kepada sebagian dari keturunannya (Ibrahim) yaitu Dawud, Sulaiman, Ayyub, Yusuf, Musa, dan

6:85

# وَزَكَرِيَّا وَيَحْيٰى وَعِيْسٰى وَاِلْيَاسَۗ كُلٌّ مِّنَ الصّٰلِحِيْنَۙ

wazakariyy*aa* waya*h*y*aa* wa'iis*aa* wa**i**ly*aa*sa kullun mina **al***shshaa*li*h*iin**a**

dan Zakaria, Yahya, Isa, dan Ilyas. Semuanya termasuk orang-orang yang saleh,

6:86

# وَاِسْمٰعِيْلَ وَالْيَسَعَ وَيُوْنُسَ وَلُوْطًاۗ وَكُلًّا فَضَّلْنَا عَلَى الْعٰلَمِيْنَۙ

wa-ism*aa*'iila wa**i**lyasa'a wayuunusa waluu*th*an wakullan fa*dhdh*aln*aa* 'al*aa* **a**l'*aa*lamiin**a**

dan Ismail, Alyasa‘, Yunus, dan Lut. Masing-masing Kami lebihkan (derajatnya) di atas umat lain (pada masanya),

6:87

# وَمِنْ اٰبَاۤىِٕهِمْ وَذُرِّيّٰتِهِمْ وَاِخْوَانِهِمْ ۚوَاجْتَبَيْنٰهُمْ وَهَدَيْنٰهُمْ اِلٰى صِرَاطٍ مُّسْتَقِيْمٍ

wamin *aa*b*aa*-ihim wa*dz*urriyy*aa*tihim wa-ikhw*aa*nihim wa**i**jtabayn*aa*hum wahadayn*aa*hum il*aa* *sh*ir*aath*in mustaqiim**in**

(dan Kami lebihkan pula derajat) sebagian dari nenek moyang mereka, keturunan mereka dan saudara-saudara mereka. Kami telah memilih mereka (menjadi nabi dan rasul) dan mereka Kami beri petunjuk ke jalan yang lurus.

6:88

# ذٰلِكَ هُدَى اللّٰهِ يَهْدِيْ بِهٖ مَنْ يَّشَاۤءُ مِنْ عِبَادِهٖ ۗوَلَوْ اَشْرَكُوْا لَحَبِطَ عَنْهُمْ مَّا كَانُوْا يَعْمَلُوْنَ

*dzaa*lika hud*aa* **al**l*aa*hi yahdii bihi man yasy*aa*u min 'ib*aa*dihi walaw asyrakuu la*h*abi*th*a 'anhum m*aa* k*aa*nuu ya'maluun**a**

Itulah petunjuk Allah, dengan itu Dia memberi petunjuk kepada siapa saja di antara hamba-hamba-Nya yang Dia kehendaki. Sekiranya mereka mempersekutukan Allah, pasti lenyaplah amalan yang telah mereka kerjakan.

6:89

# اُولٰۤىِٕكَ الَّذِيْنَ اٰتَيْنٰهُمُ الْكِتٰبَ وَالْحُكْمَ وَالنُّبُوَّةَ ۚفَاِنْ يَّكْفُرْ بِهَا هٰٓؤُلَاۤءِ فَقَدْ وَكَّلْنَا بِهَا قَوْمًا لَّيْسُوْا بِهَا بِكٰفِرِيْنَ

ul*aa*-ika **al**la*dz*iina *aa*tayn*aa*humu **a**lkit*aa*ba wa**a**l*h*ukma wa**al**nnubuwwata fa-in yakfur bih*aa* h*aa*ul*aa*-i faqad wakkaln*aa* bih<

Mereka itulah orang-orang yang telah Kami berikan kitab, hikmah dan kenabian. Jika orang-orang (Quraisy) itu mengingkarinya, maka Kami akan menyerahkannya kepada kaum yang tidak mengingkarinya.

6:90

# اُولٰۤىِٕكَ الَّذِيْنَ هَدَى اللّٰهُ فَبِهُدٰىهُمُ اقْتَدِهْۗ قُلْ لَّآ اَسْـَٔلُكُمْ عَلَيْهِ اَجْرًاۗ اِنْ هُوَ اِلَّا ذِكْرٰى لِلْعٰلَمِيْنَ ࣖ

ul*aa*-ika **al**la*dz*iina had*aa* **al**l*aa*hu fabihud*aa*humu iqtadih qul l*aa* as-alukum 'alayhi ajran in huwa ill*aa* *dz*ikr*aa* lil'*aa*lamiin**a**

Mereka itulah (para nabi) yang telah diberi petunjuk oleh Allah, maka ikutilah petunjuk mereka. Katakanlah (Muhammad), “Aku tidak meminta imbalan kepadamu dalam menyampaikan (Al-Qur'an).” Al-Qur'an itu tidak lain hanyalah peringatan untuk (segala umat) se

6:91

# وَمَا قَدَرُوا اللّٰهَ حَقَّ قَدْرِهٖٓ اِذْ قَالُوْا مَآ اَنْزَلَ اللّٰهُ عَلٰى بَشَرٍ مِّنْ شَيْءٍۗ قُلْ مَنْ اَنْزَلَ الْكِتٰبَ الَّذِيْ جَاۤءَ بِهٖ مُوْسٰى نُوْرًا وَّهُدًى لِّلنَّاسِ تَجْعَلُوْنَهٗ قَرَاطِيْسَ تُبْدُوْنَهَا وَتُخْفُوْنَ كَثِيْرًاۚ و

wam*aa* qadaruu **al**l*aa*ha *h*aqqa qadrihi i*dz* q*aa*luu m*aa* anzala **al**l*aa*hu 'al*aa* basyarin min syay-in qul man anzala **a**lkit*aa*ba **al**la

Mereka tidak mengagungkan Allah sebagaimana mestinya ketika mereka berkata, “Allah tidak menurunkan sesuatu pun kepada manusia.” Katakanlah (Muhammad), “Siapakah yang menurunkan Kitab (Taurat) yang dibawa Musa sebagai cahaya dan petunjuk bagi manusia, kam

6:92

# وَهٰذَا كِتٰبٌ اَنْزَلْنٰهُ مُبٰرَكٌ مُّصَدِّقُ الَّذِيْ بَيْنَ يَدَيْهِ وَلِتُنْذِرَ اُمَّ الْقُرٰى وَمَنْ حَوْلَهَاۗ وَالَّذِيْنَ يُؤْمِنُوْنَ بِالْاٰخِرَةِ يُؤْمِنُوْنَ بِهٖ وَهُمْ عَلٰى صَلَاتِهِمْ يُحٰفِظُوْنَ

wah*aadzaa* kit*aa*bun anzaln*aa*hu mub*aa*rakun mu*sh*addiqu **al**la*dz*ii bayna yadayhi walitun*dz*ira umma **a**lqur*aa* waman *h*awlah*aa* wa**a**lla*dz*iin

Dan ini (Al-Qur'an), Kitab yang telah Kami turunkan dengan penuh berkah; membenarkan kitab-kitab yang (diturunkan) sebelumnya dan agar engkau memberi peringatan kepada (penduduk) Ummul Qura (Mekah) dan orang-orang yang ada di sekitarnya. Orang-orang yang

6:93

# وَمَنْ اَظْلَمُ مِمَّنِ افْتَرٰى عَلَى اللّٰهِ كَذِبًا اَوْ قَالَ اُوْحِيَ اِلَيَّ وَلَمْ يُوْحَ اِلَيْهِ شَيْءٌ وَّمَنْ قَالَ سَاُنْزِلُ مِثْلَ مَآ اَنْزَلَ اللّٰهُ ۗوَلَوْ تَرٰٓى اِذِ الظّٰلِمُوْنَ فِيْ غَمَرٰتِ الْمَوْتِ وَالْمَلٰۤىِٕكَةُ بَاسِطُوْٓا

waman a*zh*lamu mimmani iftar*aa* 'al*aa* **al**l*aa*hi ka*dz*iban aw q*aa*la uu*h*iya ilayya walam yuu*h*a ilayhi syay-un waman q*aa*la saunzilu mitsla m*aa* anzala **al**l*aa<*

Siapakah yang lebih zalim daripada orang-orang yang mengada-adakan dusta terhadap Allah atau yang berkata, “Telah diwahyukan kepadaku,” padahal tidak diwahyukan sesuatu pun kepadanya, dan orang yang berkata, “Aku akan menurunkan seperti apa yang diturunka

6:94

# وَلَقَدْ جِئْتُمُوْنَا فُرَادٰى كَمَا خَلَقْنٰكُمْ اَوَّلَ مَرَّةٍ وَّتَرَكْتُمْ مَّا خَوَّلْنٰكُمْ وَرَاۤءَ ظُهُوْرِكُمْۚ وَمَا نَرٰى مَعَكُمْ شُفَعَاۤءَكُمُ الَّذِيْنَ زَعَمْتُمْ اَنَّهُمْ فِيْكُمْ شُرَكٰۤؤُا ۗ لَقَدْ تَّقَطَّعَ بَيْنَكُمْ وَضَلَّ عَنْك

walaqad ji/tumuun*aa* fur*aa*d*aa* kam*aa* khalaqn*aa*kum awwala marratin wataraktum m*aa* khawwaln*aa*kum war*aa*-a *zh*uhuurikum wam*aa* nar*aa* ma'akum syufa'*aa*-akumu **al**la

Dan kamu benar-benar datang sendiri-sendiri kepada Kami sebagaimana Kami ciptakan kamu pada mulanya, dan apa yang telah Kami karuniakan kepadamu, kamu tinggalkan di belakangmu (di dunia). Kami tidak melihat pemberi syafaat (pertolongan) besertamu yang kam

6:95

# ۞ اِنَّ اللّٰهَ فَالِقُ الْحَبِّ وَالنَّوٰىۗ يُخْرِجُ الْحَيَّ مِنَ الْمَيِّتِ وَمُخْرِجُ الْمَيِّتِ مِنَ الْحَيِّ ۗذٰلِكُمُ اللّٰهُ فَاَنّٰى تُؤْفَكُوْنَ

inna **al**l*aa*ha f*aa*liqu **a**l*h*abbi wa**al**nnaw*aa* yukhriju **a**l*h*ayya mina **a**lmayyiti wamukhriju **a**lmayyiti mina **a**l<

Sungguh, Allah yang menumbuhkan butir (padi-padian) dan biji (kurma). Dia mengeluarkan yang hidup dari yang mati dan mengeluarkan yang mati dari yang hidup. Itulah (kekuasaan) Allah, maka mengapa kamu masih berpaling?

6:96

# فَالِقُ الْاِصْبَاحِۚ وَجَعَلَ الَّيْلَ سَكَنًا وَّالشَّمْسَ وَالْقَمَرَ حُسْبَانًا ۗذٰلِكَ تَقْدِيْرُ الْعَزِيْزِ الْعَلِيْمِ

f*aa*liqu **a**l-i*sh*b*aah*i waja'ala **al**layla sakanan wa**al**sysyamsa wa**a**lqamara *h*usb*aa*nan *dzaa*lika taqdiiru **a**l'aziizi **a**l'a

Dia menyingsingkan pagi dan menjadikan malam untuk beristirahat, dan (menjadikan) matahari dan bulan untuk perhitungan. Itulah ketetapan Allah Yang Mahaperkasa, Maha Mengetahui.

6:97

# وَهُوَ الَّذِيْ جَعَلَ لَكُمُ النُّجُوْمَ لِتَهْتَدُوْا بِهَا فِيْ ظُلُمٰتِ الْبَرِّ وَالْبَحْرِۗ قَدْ فَصَّلْنَا الْاٰيٰتِ لِقَوْمٍ يَّعْلَمُوْنَ

wahuwa **al**la*dz*ii ja'ala lakumu **al**nnujuuma litahtaduu bih*aa* fii *zh*ulum*aa*ti **a**lbarri wa**a**lba*h*ri qad fa*shsh*aln*aa* **a**l-*aa*y<

Dan Dialah yang menjadikan bintang-bintang bagimu, agar kamu menjadikannya petunjuk dalam kegelapan di darat dan di laut. Kami telah menjelaskan tanda-tanda (kekuasaan Kami) kepada orang-orang yang mengetahui.

6:98

# وَهُوَ الَّذِيْٓ اَنْشَاَكُمْ مِّنْ نَّفْسٍ وَّاحِدَةٍ فَمُسْتَقَرٌّ وَّمُسْتَوْدَعٌ ۗقَدْ فَصَّلْنَا الْاٰيٰتِ لِقَوْمٍ يَّفْقَهُوْنَ

wahuwa **al**la*dz*ii ansya-akum min nafsin w*aah*idatin famustaqarrun wamustawda'un qad fa*shsh*aln*aa* **a**l-*aa*y*aa*ti liqawmin yafqahuun**a**

Dan Dialah yang menciptakan kamu dari diri yang satu (Adam), maka (bagimu) ada tempat menetap dan tempat simpanan. Sesungguhnya telah Kami jelaskan tanda-tanda (kebesaran Kami) kepada orang-orang yang mengetahui.

6:99

# وَهُوَ الَّذِيْٓ اَنْزَلَ مِنَ السَّمَاۤءِ مَاۤءًۚ فَاَخْرَجْنَا بِهٖ نَبَاتَ كُلِّ شَيْءٍ فَاَخْرَجْنَا مِنْهُ خَضِرًا نُّخْرِجُ مِنْهُ حَبًّا مُّتَرَاكِبًاۚ وَمِنَ النَّخْلِ مِنْ طَلْعِهَا قِنْوَانٌ دَانِيَةٌ وَّجَنّٰتٍ مِّنْ اَعْنَابٍ وَّالزَّيْتُوْنَ

wahuwa **al**la*dz*ii anzala mina **al**ssam*aa*-i m*aa*-an fa-akhrajn*aa* bihi nab*aa*ta kulli syay-in fa-akhrajn*aa* minhu kha*dh*iran nukhriju minhu *h*abban mutar*aa*kiban wamina <

Dan Dialah yang menurunkan air dari langit, lalu Kami tumbuhkan dengan air itu segala macam tumbuh-tumbuhan, maka Kami keluarkan dari tumbuh-tumbuhan itu tanaman yang menghijau, Kami keluarkan dari tanaman yang menghijau itu butir yang banyak; dan dari ma

6:100

# وَجَعَلُوْا لِلّٰهِ شُرَكَاۤءَ الْجِنَّ وَخَلَقَهُمْ وَخَرَقُوْا لَهٗ بَنِيْنَ وَبَنٰتٍۢ بِغَيْرِ عِلْمٍۗ سُبْحٰنَهٗ وَتَعٰلٰى عَمَّا يَصِفُوْنَ ࣖ

waja'aluu lill*aa*hi syurak*aa*-a **a**ljinna wakhalaqahum wakharaquu lahu baniina waban*aa*tin bighayri 'ilmin sub*haa*nahu wata'*aa*l*aa* 'amm*aa* ya*sh*ifuun**a**

Dan mereka (orang-orang musyrik) menjadikan jin sekutu-sekutu Allah, padahal Dia yang menciptakannya (jin-jin itu), dan mereka berbohong (dengan mengatakan), “Allah mempunyai anak laki-laki dan anak perempuan,” tanpa (dasar) pengetahuan. Mahasuci Allah da

6:101

# بَدِيْعُ السَّمٰوٰتِ وَالْاَرْضِۗ اَنّٰى يَكُوْنُ لَهٗ وَلَدٌ وَّلَمْ تَكُنْ لَّهٗ صَاحِبَةٌ ۗوَخَلَقَ كُلَّ شَيْءٍۚ وَهُوَ بِكُلِّ شَيْءٍ عَلِيْمٌ

badii'u **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i ann*aa* yakuunu lahu waladun walam takun lahu *shaah*ibatun wakhalaqa kulla syay-in wahuwa bikulli syay-in 'aliim**un**

Dia (Allah) pencipta langit dan bumi. Bagaimana (mungkin) Dia mempunyai anak padahal Dia tidak mempunyai istri. Dia menciptakan segala sesuatu; dan Dia mengetahui segala sesuatu.

6:102

# ذٰلِكُمُ اللّٰهُ رَبُّكُمْۚ لَآ اِلٰهَ اِلَّا هُوَۚ خَالِقُ كُلِّ شَيْءٍ فَاعْبُدُوْهُ ۚوَهُوَ عَلٰى كُلِّ شَيْءٍ وَّكِيْلٌ

*dzaa*likumu **al**l*aa*hu rabbukum l*aa* il*aa*ha ill*aa* huwa kh*aa*liqu kulli syay-in fa**u**'buduuhu wahuwa 'al*aa* kulli syay-in wakiil**un**

Itulah Allah, Tuhan kamu; tidak ada tuhan selain Dia; pencipta segala sesuatu, maka sembahlah Dia; Dialah pemelihara segala sesuatu.

6:103

# لَا تُدْرِكُهُ الْاَبْصَارُ وَهُوَ يُدْرِكُ الْاَبْصَارَۚ وَهُوَ اللَّطِيْفُ الْخَبِيْرُ

l*aa* tudrikuhu **a**l-ab*shaa*ru wahuwa yudriku **a**l-ab*shaa*ra wahuwa **al**la*th*iifu **a**lkhabiir**u**

Dia tidak dapat dicapai oleh penglihatan mata, sedang Dia dapat melihat segala penglihatan itu, dan Dialah Yang Mahahalus, Mahateliti.

6:104

# قَدْ جَاۤءَكُمْ بَصَاۤىِٕرُ مِنْ رَّبِّكُمْۚ فَمَنْ اَبْصَرَ فَلِنَفْسِهٖۚ وَمَنْ عَمِيَ فَعَلَيْهَاۗ وَمَآ اَنَا۠ عَلَيْكُمْ بِحَفِيْظٍ

qad j*aa*-akum ba*shaa*-iru min rabbikum faman ab*sh*ara falinafsihi waman 'amiya fa'alayh*aa* wam*aa* an*aa* 'alaykum bi*h*afii*zh**\*in**

Sungguh, bukti-bukti yang nyata telah datang dari Tuhanmu. Barangsiapa melihat (kebenaran itu), maka (manfaatnya) bagi dirinya sendiri; dan barangsiapa buta (tidak melihat kebenaran itu), maka dialah yang rugi. Dan aku (Muhammad) bukanlah penjaga-(mu).

6:105

# وَكَذٰلِكَ نُصَرِّفُ الْاٰيٰتِ وَلِيَقُوْلُوْا دَرَسْتَ وَلِنُبَيِّنَهٗ لِقَوْمٍ يَّعْلَمُوْنَ

waka*dzaa*lika nu*sh*arrifu **a**l-*aa*y*aa*ti waliyaquuluu darasta walinubayyinahu liqawmin ya'lamuun**a**

Dan demikianlah Kami menjelaskan berulang-ulang ayat-ayat Kami agar orang-orang musyrik mengatakan, “Engkau telah mempelajari ayat-ayat itu (dari Ahli Kitab),” dan agar Kami menjelaskan Al-Qur'an itu kepada orang-orang yang mengetahui.

6:106

# اِتَّبِعْ مَآ اُوْحِيَ اِلَيْكَ مِنْ رَّبِّكَۚ لَآ اِلٰهَ اِلَّا هُوَۚ وَاَعْرِضْ عَنِ الْمُشْرِكِيْنَ

ittabi' m*aa* uu*h*iya ilayka min rabbika l*aa* il*aa*ha ill*aa* huwa wa-a'ri*dh* 'ani **a**lmusyrikiin**a**

Ikutilah apa yang telah diwahyukan Tuhanmu kepadamu (Muhammad); tidak ada tuhan selain Dia; dan berpalinglah dari orang-orang musyrik.

6:107

# وَلَوْ شَاۤءَ اللّٰهُ مَآ اَشْرَكُوْاۗ وَمَا جَعَلْنٰكَ عَلَيْهِمْ حَفِيْظًاۚ وَمَآ اَنْتَ عَلَيْهِمْ بِوَكِيْلٍ

walaw sy*aa*-a **al**l*aa*hu m*aa* asyrakuu wam*aa* ja'aln*aa*ka 'alayhim *h*afii*zh*an wam*aa* anta 'alayhim biwakiil**in**

Dan sekiranya Allah menghendaki, niscaya mereka tidak mempersekutukan(-Nya). Dan Kami tidak menjadikan engkau penjaga mereka; dan engkau bukan pula pemelihara mereka.

6:108

# وَلَا تَسُبُّوا الَّذِيْنَ يَدْعُوْنَ مِنْ دُوْنِ اللّٰهِ فَيَسُبُّوا اللّٰهَ عَدْوًاۢ بِغَيْرِ عِلْمٍۗ كَذٰلِكَ زَيَّنَّا لِكُلِّ اُمَّةٍ عَمَلَهُمْۖ ثُمَّ اِلٰى رَبِّهِمْ مَّرْجِعُهُمْ فَيُنَبِّئُهُمْ بِمَا كَانُوْا يَعْمَلُوْنَ

wal*aa* tasubbuu **al**la*dz*iina yad'uuna min duuni **al**l*aa*hi fayasubbuu **al**l*aa*ha 'adwan bighayri 'ilmin ka*dzaa*lika zayyann*aa* likulli ummatin 'amalahum tsumma il*aa* r

Dan janganlah kamu memaki sesembahan yang mereka sembah selain Allah, karena mereka nanti akan memaki Allah dengan melampaui batas tanpa dasar pengetahuan. Demikianlah, Kami jadikan setiap umat menganggap baik pekerjaan mereka. Kemudian kepada Tuhan tempa

6:109

# وَاَقْسَمُوْا بِاللّٰهِ جَهْدَ اَيْمَانِهِمْ لَىِٕنْ جَاۤءَتْهُمْ اٰيَةٌ لَّيُؤْمِنُنَّ بِهَاۗ قُلْ اِنَّمَا الْاٰيٰتُ عِنْدَ اللّٰهِ وَمَا يُشْعِرُكُمْ اَنَّهَآ اِذَا جَاۤءَتْ لَا يُؤْمِنُوْنَ

wa-aqsamuu bi**al**l*aa*hi jahda aym*aa*nihim la-in j*aa*-at-hum *aa*yatun layu/minunna bih*aa* qul innam*aa* **a**l-*aa*y*aa*tu 'inda **al**l*aa*hi wam*aa* yusy'iruk

Dan mereka bersumpah dengan nama Allah dengan segala kesungguhan, bahwa jika datang suatu mukjizat kepada mereka, pastilah mereka akan beriman kepadanya. Katakanlah, “Mukjizat-mukjizat itu hanya ada pada sisi Allah.” Dan tahukah kamu, bahwa apabila mukjiz

6:110

# وَنُقَلِّبُ اَفْـِٕدَتَهُمْ وَاَبْصَارَهُمْ كَمَا لَمْ يُؤْمِنُوْا بِهٖٓ اَوَّلَ مَرَّةٍ وَّنَذَرُهُمْ فِيْ طُغْيَانِهِمْ يَعْمَهُوْنَ ࣖ ۔

wanuqallibu af-idatahum wa-ab*shaa*rahum kam*aa* lam yu/minuu bihi awwala marratin wana*dz*aruhum fii *th*ughy*aa*nihim ya'mahuun**a**

Dan (begitu pula) Kami memalingkan hati dan penglihatan mereka seperti pertama kali mereka tidak beriman kepadanya (Al-Qur'an), dan Kami biarkan mereka bingung dalam kesesatan.

6:111

# ۞ وَلَوْ اَنَّنَا نَزَّلْنَآ اِلَيْهِمُ الْمَلٰۤىِٕكَةَ وَكَلَّمَهُمُ الْمَوْتٰى وَحَشَرْنَا عَلَيْهِمْ كُلَّ شَيْءٍ قُبُلًا مَّا كَانُوْا لِيُؤْمِنُوْٓا اِلَّآ اَنْ يَّشَاۤءَ اللّٰهُ وَلٰكِنَّ اَكْثَرَهُمْ يَجْهَلُوْنَ

walaw annan*aa* nazzaln*aa* ilayhimu **a**lmal*aa*-ikata wakallamahumu **a**lmawt*aa* wa*h*asyarn*aa* 'alayhim kulla syay-in qubulan m*aa* k*aa*nuu liyu/minuu ill*aa* an yasy*aa*-

Dan sekalipun Kami benar-benar menurunkan malaikat kepada mereka, dan orang yang telah mati berbicara dengan mereka dan Kami kumpulkan (pula) di hadapan mereka segala sesuatu (yang mereka inginkan), mereka tidak juga akan beriman, kecuali jika Allah mengh

6:112

# وَكَذٰلِكَ جَعَلْنَا لِكُلِّ نَبِيٍّ عَدُوًّا شَيٰطِيْنَ الْاِنْسِ وَالْجِنِّ يُوْحِيْ بَعْضُهُمْ اِلٰى بَعْضٍ زُخْرُفَ الْقَوْلِ غُرُوْرًا ۗوَلَوْ شَاۤءَ رَبُّكَ مَا فَعَلُوْهُ فَذَرْهُمْ وَمَا يَفْتَرُوْنَ

waka*dzaa*lika ja'aln*aa* likulli nabiyyin 'aduwwan syay*aath*iina **a**l-insi wa**a**ljinni yuu*h*ii ba'*dh*uhum il*aa* ba'*dh*in zukhrufa **a**lqawli ghuruuran walaw sy*aa*-a

Dan demikianlah untuk setiap nabi Kami menjadikan musuh yang terdiri dari setan-setan manusia dan jin, sebagian mereka membisikkan kepada sebagian yang lain perkataan yang indah sebagai tipuan. Dan kalau Tuhanmu menghendaki, niscaya mereka tidak akan mela

6:113

# وَلِتَصْغٰٓى اِلَيْهِ اَفْـِٕدَةُ الَّذِيْنَ لَا يُؤْمِنُوْنَ بِالْاٰخِرَةِ وَلِيَرْضَوْهُ وَلِيَقْتَرِفُوْا مَا هُمْ مُّقْتَرِفُوْنَ

walita*sh*gh*aa* ilayhi af-idatu **al**la*dz*iina l*aa* yu/minuuna bi**a**l-*aa*khirati waliyar*dh*awhu waliyaqtarifuu m*aa* hum muqtarifuun**a**

Dan agar hati kecil orang-orang yang tidak beriman kepada akhirat, tertarik kepada bisikan itu, dan menyenanginya, dan agar mereka melakukan apa yang biasa mereka lakukan.

6:114

# اَفَغَيْرَ اللّٰهِ اَبْتَغِيْ حَكَمًا وَّهُوَ الَّذِيْٓ اَنْزَلَ اِلَيْكُمُ الْكِتٰبَ مُفَصَّلًا ۗوَالَّذِيْنَ اٰتَيْنٰهُمُ الْكِتٰبَ يَعْلَمُوْنَ اَنَّهٗ مُنَزَّلٌ مِّنْ رَّبِّكَ بِالْحَقِّ فَلَا تَكُوْنَنَّ مِنَ الْمُمْتَرِيْنَ

afaghayra **al**l*aa*hi abtaghii *h*akaman wahuwa **al**la*dz*ii anzala ilaykumu **a**lkit*aa*ba mufa*shsh*alan wa**a**lla*dz*iina *aa*tayn*aa*humu **a**

**Pantaskah aku mencari hakim selain Allah, padahal Dialah yang menurunkan Kitab (Al-Qur'an) kepadamu secara rinci? Orang-orang yang telah Kami beri kitab mengetahui benar bahwa (Al-Qur'an) itu diturunkan dari Tuhanmu dengan benar. Maka janganlah kamu terma**

6:115

# وَتَمَّتْ كَلِمَتُ رَبِّكَ صِدْقًا وَّعَدْلًاۗ لَا مُبَدِّلَ لِكَلِمٰتِهٖ ۚوَهُوَ السَّمِيْعُ الْعَلِيْمُ

watammat kalimatu rabbika *sh*idqan wa'adlan l*aa* mubaddila likalim*aa*tihi wahuwa **al**ssamii'u **a**l'aliim**u**

Dan telah sempurna firman Tuhanmu (Al-Qur'an) dengan benar dan adil. Tidak ada yang dapat mengubah firman-Nya. Dan Dia Maha Mendengar, Maha Mengetahui.

6:116

# وَاِنْ تُطِعْ اَكْثَرَ مَنْ فِى الْاَرْضِ يُضِلُّوْكَ عَنْ سَبِيْلِ اللّٰهِ ۗاِنْ يَّتَّبِعُوْنَ اِلَّا الظَّنَّ وَاِنْ هُمْ اِلَّا يَخْرُصُوْنَ

wa-in tu*th*i' aktsara man fii **a**l-ar*dh*i yu*dh*illuuka 'an sabiili **al**l*aa*hi in yattabi'uuna ill*aa* **al***zhzh*anna wa-in hum ill*aa *yakhru*sh*uun**a**

Dan jika kamu mengikuti kebanyakan orang di bumi ini, niscaya mereka akan menyesatkanmu dari jalan Allah. Yang mereka ikuti hanya persangkaan belaka dan mereka hanyalah membuat kebohongan.

6:117

# اِنَّ رَبَّكَ هُوَ اَعْلَمُ مَنْ يَّضِلُّ عَنْ سَبِيْلِهٖۚ وَهُوَ اَعْلَمُ بِالْمُهْتَدِيْنَ

inna rabbaka huwa a'lamu man ya*dh*illu 'an sabiilihi wahuwa a'lamu bi**a**lmuhtadiin**a**

Sesungguhnya Tuhanmu, Dialah yang lebih mengetahui siapa yang tersesat dari jalan-Nya, dan Dia lebih mengetahui orang-orang yang mendapat petunjuk.

6:118

# فَكُلُوْا مِمَّا ذُكِرَ اسْمُ اللّٰهِ عَلَيْهِ اِنْ كُنْتُمْ بِاٰيٰتِهٖ مُؤْمِنِيْنَ

fakuluu mimm*aa* *dz*ukira ismu **al**l*aa*hi 'alayhi in kuntum bi-*aa*y*aa*tihi mu/miniin**a**

Maka makanlah dari apa (daging hewan) yang (ketika disembelih) disebut nama Allah, jika kamu beriman kepada ayat-ayat-Nya.

6:119

# وَمَا لَكُمْ اَلَّا تَأْكُلُوْا مِمَّا ذُكِرَ اسْمُ اللّٰهِ عَلَيْهِ وَقَدْ فَصَّلَ لَكُمْ مَّا حَرَّمَ عَلَيْكُمْ اِلَّا مَا اضْطُرِرْتُمْ اِلَيْهِ ۗوَاِنَّ كَثِيرًا لَّيُضِلُّوْنَ بِاَهْوَاۤىِٕهِمْ بِغَيْرِ عِلْمٍ ۗاِنَّ رَبَّكَ هُوَ اَعْلَمُ بِالْمُعْت

wam*aa* lakum **al**l*aa* ta/kuluu mimm*aa* *dz*ukira ismu **al**l*aa*hi 'alayhi waqad fa*shsh*ala lakum m*aa* *h*arrama 'alaykum ill*aa* m*aa* i*dth*urirtum ilayhi wa-inna k

Dan mengapa kamu tidak mau memakan dari apa (daging hewan) yang (ketika disembelih) disebut nama Allah, padahal Allah telah menjelaskan kepadamu apa yang diharamkan-Nya kepadamu, kecuali jika kamu dalam keadaan terpaksa. Dan sungguh, banyak yang menyesatk

6:120

# وَذَرُوْا ظَاهِرَ الْاِثْمِ وَبَاطِنَهٗ ۗاِنَّ الَّذِيْنَ يَكْسِبُوْنَ الْاِثْمَ سَيُجْزَوْنَ بِمَا كَانُوْا يَقْتَرِفُوْنَ

wa*dz*aruu *zhaa*hira **a**l-itsmi wab*aath*inahu inna **al**la*dz*iina yaksibuuna **a**l-itsma sayujzawna bim*aa* k*aa*nuu yaqtarifuun**a**

Dan tinggalkanlah dosa yang terlihat ataupun yang tersembunyi. Sungguh, orang-orang yang mengerjakan (perbuatan) dosa kelak akan diberi balasan sesuai dengan apa yang mereka kerjakan.

6:121

# وَلَا تَأْكُلُوْا مِمَّا لَمْ يُذْكَرِ اسْمُ اللّٰهِ عَلَيْهِ وَاِنَّهٗ لَفِسْقٌۗ وَاِنَّ الشَّيٰطِيْنَ لَيُوْحُوْنَ اِلٰٓى اَوْلِيَاۤىِٕهِمْ لِيُجَادِلُوْكُمْ ۚوَاِنْ اَطَعْتُمُوْهُمْ اِنَّكُمْ لَمُشْرِكُوْنَ ࣖ

wal*aa* ta/kuluu mimm*aa* lam yu*dz*kari ismu **al**l*aa*hi 'alayhi wa-innahu lafisqun wa-inna **al**sysyay*aath*iina layuu*h*uuna il*aa* awliy*aa*-ihim liyuj*aa*diluukum wa-in a*th<*

Dan janganlah kamu memakan dari apa (daging hewan) yang (ketika disembelih) tidak disebut nama Allah, perbuatan itu benar-benar suatu kefasikan. Sesungguhnya setan-setan akan membisikkan kepada kawan-kawannya agar mereka membantah kamu. Dan jika kamu menu

6:122

# اَوَمَنْ كَانَ مَيْتًا فَاَحْيَيْنٰهُ وَجَعَلْنَا لَهٗ نُوْرًا يَّمْشِيْ بِهٖ فِى النَّاسِ كَمَنْ مَّثَلُهٗ فِى الظُّلُمٰتِ لَيْسَ بِخَارِجٍ مِّنْهَاۗ كَذٰلِكَ زُيِّنَ لِلْكٰفِرِيْنَ مَا كَانُوْا يَعْمَلُوْنَ

awa man k*aa*na maytan fa-a*h*yayn*aa*hu waja'aln*aa* lahu nuuran yamsyii bihi fii **al**nn*aa*si kaman matsaluhu fii **al***zhzh*ulum*aa*ti laysa bikh*aa*rijin minh*aa *ka*dzaa*l

Dan apakah orang yang sudah mati lalu Kami hidupkan dan Kami beri dia cahaya yang membuatnya dapat berjalan di tengah-tengah orang banyak, sama dengan orang yang berada dalam kegelapan, sehingga dia tidak dapat keluar dari sana? Demikianlah dijadikan tera

6:123

# وَكَذٰلِكَ جَعَلْنَا فِيْ كُلِّ قَرْيَةٍ اَكٰبِرَ مُجْرِمِيْهَا لِيَمْكُرُوْا فِيْهَاۗ وَمَا يَمْكُرُوْنَ اِلَّا بِاَنْفُسِهِمْ وَمَا يَشْعُرُوْنَ

waka*dzaa*lika ja'aln*aa* fii kulli qaryatin ak*aa*bira mujrimiih*aa* liyamkuruu fiih*aa* wam*aa* yamkuruuna ill*aa* bi-anfusihim wam*aa* yasy'uruun**a**

Dan demikianlah pada setiap negeri Kami jadikan pembesar-pembesar yang jahat agar melakukan tipu daya di negeri itu. Tapi mereka hanya menipu diri sendiri tanpa menyadarinya.

6:124

# وَاِذَا جَاۤءَتْهُمْ اٰيَةٌ قَالُوْا لَنْ نُّؤْمِنَ حَتّٰى نُؤْتٰى مِثْلَ مَآ اُوْتِيَ رُسُلُ اللّٰهِ ۘ اَللّٰهُ اَعْلَمُ حَيْثُ يَجْعَلُ رِسٰلَتَهٗۗ سَيُصِيْبُ الَّذِيْنَ اَجْرَمُوْا صَغَارٌ عِنْدَ اللّٰهِ وَعَذَابٌ شَدِيْدٌۢ بِمَا كَانُوْا يَمْكُرُوْنَ

wa-i*dzaa* j*aa*-at-hum *aa*yatun q*aa*luu lan nu/mina *h*att*aa* nu/t*aa* mitsla m*aa* uutiya rusulu **al**l*aa*hi **al**l*aa*hu a'lamu *h*aytsu yaj'alu ris*aa*latahu s

Dan apabila datang suatu ayat kepada mereka, mereka berkata, “Kami tidak akan percaya (beriman) sebelum diberikan kepada kami seperti apa yang diberikan kepada rasul-rasul Allah.” Allah lebih mengetahui di mana Dia menempatkan tugas kerasulan-Nya. Orang-o

6:125

# فَمَنْ يُّرِدِ اللّٰهُ اَنْ يَّهْدِيَهٗ يَشْرَحْ صَدْرَهٗ لِلْاِسْلَامِۚ وَمَنْ يُّرِدْ اَنْ يُّضِلَّهٗ يَجْعَلْ صَدْرَهٗ ضَيِّقًا حَرَجًا كَاَنَّمَا يَصَّعَّدُ فِى السَّمَاۤءِۗ كَذٰلِكَ يَجْعَلُ اللّٰهُ الرِّجْسَ عَلَى الَّذِيْنَ لَا يُؤْمِنُوْنَ

faman yuridi **al**l*aa*hu an yahdiyahu yasyra*h* *sh*adrahu lil-isl*aa*mi waman yurid an yu*dh*illahu yaj'al *sh*adrahu *dh*ayyiqan *h*arajan ka-annam*aa* ya*shsh*a''adu fii **al**

**Barangsiapa dikehendaki Allah akan mendapat hidayah (petunjuk), Dia akan membukakan dadanya untuk (menerima) Islam. Dan barangsiapa dikehendaki-Nya menjadi sesat, Dia jadikan dadanya sempit dan sesak, seakan-akan dia (sedang) mendaki ke langit. Demikianla**

6:126

# وَهٰذَا صِرَاطُ رَبِّكَ مُسْتَقِيْمًاۗ قَدْ فَصَّلْنَا الْاٰيٰتِ لِقَوْمٍ يَّذَّكَّرُوْنَ

wah*aadzaa* *sh*ir*aath*u rabbika mustaqiiman qad fa*shsh*aln*aa* **a**l-*aa*y*aa*ti liqawmin ya*dzdz*akkaruun**a**

Dan inilah jalan Tuhanmu yang lurus. Kami telah menjelaskan ayat-ayat (Kami) kepada orang-orang yang menerima peringatan.

6:127

# ۞ لَهُمْ دَارُ السَّلٰمِ عِنْدَ رَبِّهِمْ وَهُوَ وَلِيُّهُمْ بِمَا كَانُوْا يَعْمَلُوْنَ

lahum d*aa*ru **al**ssal*aa*mi 'inda rabbihim wahuwa waliyyuhum bim*aa* k*aa*nuu ya'maluun**a**

Bagi mereka (disediakan) tempat yang damai (surga) di sisi Tuhannya. Dan Dialah pelindung mereka karena amal kebajikan yang mereka kerjakan.

6:128

# وَيَوْمَ يَحْشُرُهُمْ جَمِيْعًاۚ يٰمَعْشَرَ الْجِنِّ قَدِ اسْتَكْثَرْتُمْ مِّنَ الْاِنْسِ ۚوَقَالَ اَوْلِيَاۤؤُهُمْ مِّنَ الْاِنْسِ رَبَّنَا اسْتَمْتَعَ بَعْضُنَا بِبَعْضٍ وَّبَلَغْنَآ اَجَلَنَا الَّذِيْٓ اَجَّلْتَ لَنَا ۗقَالَ النَّارُ مَثْوٰىكُمْ خٰلِد

wayawma ya*h*syuruhum jamii'an y*aa* ma'syara **a**ljinni qadi istaktsartum mina **a**l-insi waq*aa*la awliy*aa*uhum mina **a**l-insi rabban*aa* istamta'a ba'*dh*un*aa* biba'*dh*

*Dan (ingatlah) pada hari ketika Dia mengumpulkan mereka semua (dan Allah berfirman), “Wahai golongan jin! Kamu telah banyak (menyesatkan) manusia.” Dan kawan-kawan mereka dari golongan manusia berkata, “Ya Tuhan, kami telah saling mendapatkan kesenangan d*

6:129

# وَكَذٰلِكَ نُوَلِّيْ بَعْضَ الظّٰلِمِيْنَ بَعْضًاۢ بِمَا كَانُوْا يَكْسِبُوْنَ ࣖ

waka*dzaa*lika nuwallii ba'*dh*a **al***zhzhaa*limiina ba'*dh*an bim*aa *k*aa*nuu yaksibuun**a**

Dan demikianlah Kami jadikan sebagian orang-orang zalim berteman dengan sesamanya, sesuai dengan apa yang mereka kerjakan.

6:130

# يٰمَعْشَرَ الْجِنِّ وَالْاِنْسِ اَلَمْ يَأْتِكُمْ رُسُلٌ مِّنْكُمْ يَقُصُّوْنَ عَلَيْكُمْ اٰيٰتِيْ وَيُنْذِرُوْنَكُمْ لِقَاۤءَ يَوْمِكُمْ هٰذَاۗ قَالُوْا شَهِدْنَا عَلٰٓى اَنْفُسِنَا وَغَرَّتْهُمُ الْحَيٰوةُ الدُّنْيَا وَشَهِدُوْا عَلٰٓى اَنْفُسِهِمْ اَنّ

y*aa* ma'syara aljinni wa**a**l-insi alam ya/tikum rusulun minkum yaqu*shsh*uuna 'alaykum *aa*y*aa*tii wayun*dz*iruunakum liq*aa*-a yawmikum h*aadzaa* q*aa*luu syahidn*aa* 'al*aa* anfusin*aa*

Wahai golongan jin dan manusia! Bukankah sudah datang kepadamu rasul-rasul dari kalanganmu sendiri, mereka menyampaikan ayat-ayat-Ku kepadamu dan memperingatkanmu tentang pertemuan pada hari ini? Mereka menjawab, “(Ya), kami menjadi saksi atas diri kami s

6:131

# ذٰلِكَ اَنْ لَّمْ يَكُنْ رَّبُّكَ مُهْلِكَ الْقُرٰى بِظُلْمٍ وَّاَهْلُهَا غٰفِلُوْنَ

*dzaa*lika an lam yakun rabbuka muhlika **a**lqur*aa* bi*zh*ulmin wa-ahluh*aa* gh*aa*filuun**a**

Demikianlah (para rasul diutus) karena Tuhanmu tidak akan membinasakan suatu negeri secara zalim, sedang penduduknya dalam keadaan lengah (belum tahu).

6:132

# وَلِكُلٍّ دَرَجٰتٌ مِّمَّا عَمِلُوْاۗ وَمَا رَبُّكَ بِغَافِلٍ عَمَّا يَعْمَلُوْنَ

walikullin daraj*aa*tun mimm*aa* 'amiluu wam*aa* rabbuka bigh*aa*filin 'amm*aa* ya'maluun**a**

Dan masing-masing orang ada tingkatannya, (sesuai) dengan apa yang mereka kerjakan. Dan Tuhanmu tidak lengah terhadap apa yang mereka kerjakan.

6:133

# وَرَبُّكَ الْغَنِيُّ ذُو الرَّحْمَةِ ۗاِنْ يَّشَأْ يُذْهِبْكُمْ وَيَسْتَخْلِفْ مِنْۢ بَعْدِكُمْ مَّا يَشَاۤءُ كَمَآ اَنْشَاَكُمْ مِّنْ ذُرِّيَّةِ قَوْمٍ اٰخَرِيْنَ

warabbuka **a**lghaniyyu *dz*uu **al**rra*h*mati in yasya/ yu*dz*hibkum wayastakhlif min ba'dikum m*aa* yasy*aa*u kam*aa* ansya-akum min *dz*urriyyati qawmin *aa*khariin**a**

Dan Tuhanmu Mahakaya, penuh rahmat. Jika Dia menghendaki, Dia akan memusnahkan kamu dan setelah kamu (musnah) akan Dia ganti dengan yang Dia kehendaki, sebagaimana Dia menjadikan kamu dari keturunan golongan lain.

6:134

# اِنَّ مَا تُوْعَدُوْنَ لَاٰتٍۙ وَّمَآ اَنْتُمْ بِمُعْجِزِيْنَ

inna m*aa* tuu'aduuna la*aa*tin wam*aa* antum bimu'jiziin**a**

Sesungguhnya apa pun yang dijanjikan kepadamu pasti datang dan kamu tidak mampu menolaknya.

6:135

# قُلْ يٰقَوْمِ اعْمَلُوْا عَلٰى مَكَانَتِكُمْ اِنِّيْ عَامِلٌۚ فَسَوْفَ تَعْلَمُوْنَۙ مَنْ تَكُوْنُ لَهٗ عَاقِبَةُ الدَّارِۗ اِنَّهٗ لَا يُفْلِحُ الظّٰلِمُوْنَ

qul y*aa* qawmi i'maluu 'al*aa* mak*aa*natikum innii '*aa*milun fasawfa ta'lamuuna man takuunu lahu '*aa*qibatu **al**dd*aa*ri innahu l*aa* yufli*h*u **al***zhzhaa*limuun**a**

**Katakanlah (Muhammad), “Wahai kaumku! Berbuatlah menurut kedudukanmu, aku pun berbuat (demikian). Kelak kamu akan mengetahui, siapa yang akan memperoleh tempat (terbaik) di akhirat (nanti). Sesungguhnya orang-orang yang zalim itu tidak akan beruntung.**

6:136

# وَجَعَلُوْا لِلّٰهِ مِمَّا ذَرَاَ مِنَ الْحَرْثِ وَالْاَنْعَامِ نَصِيْبًا فَقَالُوْا هٰذَا لِلّٰهِ بِزَعْمِهِمْ وَهٰذَا لِشُرَكَاۤىِٕنَاۚ فَمَا كَانَ لِشُرَكَاۤىِٕهِمْ فَلَا يَصِلُ اِلَى اللّٰهِ ۚوَمَا كَانَ لِلّٰهِ فَهُوَ يَصِلُ اِلٰى شُرَكَاۤىِٕهِمْۗ سَ

waja'aluu lill*aa*hi mimm*aa* *dz*ara-a mina **a**l*h*artsi wa**a**l-an'*aa*mi na*sh*iiban faq*aa*luu h*aadzaa* lill*aa*hi biza'mihim wah*aadzaa* lisyurak*aa*-in*aa* fam

Dan mereka menyediakan sebagian hasil tanaman dan hewan (bagian) untuk Allah sambil berkata menurut persangkaan mereka, “Ini untuk Allah dan yang ini untuk berhala-berhala kami.” Bagian yang untuk berhala-berhala mereka tidak akan sampai kepada Allah, dan

6:137

# وَكَذٰلِكَ زَيَّنَ لِكَثِيْرٍ مِّنَ الْمُشْرِكِيْنَ قَتْلَ اَوْلَادِهِمْ شُرَكَاۤؤُهُمْ لِيُرْدُوْهُمْ وَلِيَلْبِسُوْا عَلَيْهِمْ دِيْنَهُمْۗ وَلَوْ شَاۤءَ اللّٰهُ مَا فَعَلُوْهُ فَذَرْهُمْ وَمَا يَفْتَرُوْنَ

waka*dzaa*lika zayyana likatsiirin mina **a**lmusyrikiina qatla awl*aa*dihim syurak*aa*uhum liyurduuhum waliyalbisuu 'alayhim diinahum walaw sy*aa*-a **al**l*aa*hu m*aa* fa'aluuhu fa*dz*arhum wa

Dan demikianlah berhala-berhala mereka (setan) menjadikan terasa indah bagi banyak orang-orang musyrik membunuh anak-anak mereka, untuk membinasakan mereka dan mengacaukan agama mereka sendiri. Dan kalau Allah menghendaki, niscaya mereka tidak akan menger

6:138

# وَقَالُوْا هٰذِهٖٓ اَنْعَامٌ وَّحَرْثٌ حِجْرٌ لَّا يَطْعَمُهَآ اِلَّا مَنْ نَّشَاۤءُ بِزَعْمِهِمْ وَاَنْعَامٌ حُرِّمَتْ ظُهُوْرُهَا وَاَنْعَامٌ لَّا يَذْكُرُوْنَ اسْمَ اللّٰهِ عَلَيْهَا افْتِرَاۤءً عَلَيْهِۗ سَيَجْزِيْهِمْ بِمَا كَانُوْا يَفْتَرُوْنَ

waq*aa*luu h*aadz*ihi an'*aa*mun wa*h*artsun *h*ijrun l*aa* ya*th*'amuh*aa* ill*aa* man nasy*aa*u biza'mihim wa-an'*aa*mun *h*urrimat *zh*uhuuruh*aa* wa-an'*aa*mun l*aa* ya

Dan mereka berkata (menurut anggapan mereka), “Inilah hewan ternak dan hasil bumi yang dilarang, tidak boleh dimakan, kecuali oleh orang yang kami kehendaki.” Dan ada pula hewan yang diharamkan (tidak boleh) ditunggangi, dan ada hewan ternak yang (ketika

6:139

# وَقَالُوْا مَا فِيْ بُطُوْنِ هٰذِهِ الْاَنْعَامِ خَالِصَةٌ لِّذُكُوْرِنَا وَمُحَرَّمٌ عَلٰٓى اَزْوَاجِنَاۚ وَاِنْ يَّكُنْ مَّيْتَةً فَهُمْ فِيْهِ شُرَكَاۤءُ ۗسَيَجْزِيْهِمْ وَصْفَهُمْۗ اِنَّهٗ حَكِيْمٌ عَلِيْمٌ

waq*aa*luu m*aa* fii bu*th*uuni h*aadz*ihi **a**l-an'*aa*mi kh*aa*li*sh*atun li*dz*ukuurin*aa* wamu*h*arramun 'al*aa* azw*aa*jin*aa* wa-in yakun maytatan fahum fiihi syurak*aa*

Dan mereka berkata (pula), “Apa yang ada di dalam perut hewan ternak ini khusus untuk kaum laki-laki kami, haram bagi istri-istri kami.” Dan jika yang dalam perut itu (dilahirkan) mati, maka semua boleh (memakannya). Kelak Allah akan membalas atas ketetap

6:140

# قَدْ خَسِرَ الَّذِيْنَ قَتَلُوْٓا اَوْلَادَهُمْ سَفَهًاۢ بِغَيْرِ عِلْمٍ وَّحَرَّمُوْا مَا رَزَقَهُمُ اللّٰهُ افْتِرَاۤءً عَلَى اللّٰهِ ۗقَدْ ضَلُّوْا وَمَا كَانُوْا مُهْتَدِيْنَ ࣖ

qad khasira **al**la*dz*iina qataluu awl*aa*dahum safahan bighayri 'ilmin wa*h*arramuu m*aa* razaqahumu **al**l*aa*hu iftir*aa*-an 'al*aa* **al**l*aa*hi qad *dh*alluu wam<

Sungguh rugi mereka yang membunuh anak-anaknya karena kebodohan tanpa pengetahuan, dan mengharamkan rezeki yang dikaruniakan Allah kepada mereka dengan semata-mata membuat-buat kebohongan terhadap Allah. Sungguh, mereka telah sesat dan tidak mendapat petu

6:141

# ۞ وَهُوَ الَّذِيْٓ اَنْشَاَ جَنّٰتٍ مَّعْرُوْشٰتٍ وَّغَيْرَ مَعْرُوْشٰتٍ وَّالنَّخْلَ وَالزَّرْعَ مُخْتَلِفًا اُكُلُهٗ وَالزَّيْتُوْنَ وَالرُّمَّانَ مُتَشَابِهًا وَّغَيْرَ مُتَشَابِهٍۗ كُلُوْا مِنْ ثَمَرِهٖٓ اِذَآ اَثْمَرَ وَاٰتُوْا حَقَّهٗ يَوْمَ حَصَا

wahuwa **al**la*dz*ii ansya-a jann*aa*tin ma'ruusy*aa*tin waghayra ma'ruusy*aa*tin wa**al**nnakhla wa**al**zzar'a mukhtalifan ukuluhu wa**al**zzaytuuna wa**al**rrumm*aa*

Dan Dialah yang menjadikan tanaman-tanaman yang merambat dan yang tidak merambat, pohon kurma, tanaman yang beraneka ragam rasanya, zaitun dan delima yang serupa (bentuk dan warnanya) dan tidak serupa (rasanya). Makanlah buahnya apabila ia berbuah dan ber

6:142

# وَمِنَ الْاَنْعَامِ حَمُوْلَةً وَّفَرْشًا ۗ كُلُوْا مِمَّا رَزَقَكُمُ اللّٰهُ وَلَا تَتَّبِعُوْا خُطُوٰتِ الشَّيْطٰنِۗ اِنَّهٗ لَكُمْ عَدُوٌّ مُّبِيْنٌۙ

wamina **a**l-an'*aa*mi *h*amuulatan wafarsyan kuluu mimm*aa* razaqakumu **al**l*aa*hu wal*aa* tattabi'uu khu*th*uw*aa*ti **al**sysyay*thaa*ni innahu lakum 'aduwwun mubi

dan di antara hewan-hewan ternak itu ada yang dijadikan pengangkut beban dan ada (pula) yang untuk disembelih. Makanlah rezeki yang diberikan Allah kepadamu, dan janganlah kamu mengikuti langkah-langkah setan. Sesungguhnya setan itu musuh yang nyata bagim

6:143

# ثَمٰنِيَةَ اَزْوَاجٍۚ مِنَ الضَّأْنِ اثْنَيْنِ وَمِنَ الْمَعْزِ اثْنَيْنِۗ قُلْ ءٰۤالذَّكَرَيْنِ حَرَّمَ اَمِ الْاُنْثَيَيْنِ اَمَّا اشْتَمَلَتْ عَلَيْهِ اَرْحَامُ الْاُنْثَيَيْنِۗ نَبِّئُوْنِيْ بِعِلْمٍ اِنْ كُنْتُمْ صٰدِقِيْنَ

tsam*aa*niyata azw*aa*jin mina **al***dhdh*a/ni itsnayni wamina **a**lma'zi itsnayni qul* aa***l***dzdz*akarayni* h*arrama ami **a**luntsayayni amm*aa* isytamalat 'alay

ada delapan hewan ternak yang berpasangan (empat pasang); sepasang domba dan sepasang kambing. Katakanlah, “Apakah yang diharamkan Allah dua yang jantan atau dua yang betina atau yang ada dalam kandungan kedua betinanya? Terangkanlah kepadaku berdasar pen

6:144

# وَمِنَ الْاِبِلِ اثْنَيْنِ وَمِنَ الْبَقَرِ اثْنَيْنِۗ قُلْ ءٰۤالذَّكَرَيْنِ حَرَّمَ اَمِ الْاُنْثَيَيْنِ اَمَّا اشْتَمَلَتْ عَلَيْهِ اَرْحَامُ الْاُنْثَيَيْنِۗ اَمْ كُنْتُمْ شُهَدَاۤءَ اِذْ وَصّٰىكُمُ اللّٰهُ بِهٰذَاۚ فَمَنْ اَظْلَمُ مِمَّنِ افْتَرٰى عَل

wamina **a**l-ibili itsnayni wamina **a**lbaqari itsnayni qul *aa***l***dzdz*akarayni* h*arrama ami **a**luntsayayni amm*aa *isytamalat 'alayhi ar*haa*mu **a**luntsa

Dan dari unta sepasang dan dari sapi sepasang. Katakanlah, “Apakah yang diharamkan dua yang jantan atau dua yang betina, atau yang ada dalam kandungan kedua betinanya? Apakah kamu menjadi saksi ketika Allah menetapkan ini bagimu? Siapakah yang lebih zalim

6:145

# قُلْ لَّآ اَجِدُ فِيْ مَآ اُوْحِيَ اِلَيَّ مُحَرَّمًا عَلٰى طَاعِمٍ يَّطْعَمُهٗٓ اِلَّآ اَنْ يَّكُوْنَ مَيْتَةً اَوْ دَمًا مَّسْفُوْحًا اَوْ لَحْمَ خِنْزِيْرٍ فَاِنَّهٗ رِجْسٌ اَوْ فِسْقًا اُهِلَّ لِغَيْرِ اللّٰهِ بِهٖۚ فَمَنِ اضْطُرَّ غَيْرَ بَاغٍ وَّ

qul l*aa* ajidu fiim*aa* uu*h*iya ilayya mu*h*arraman 'al*aa* *thaa*'imin ya*th*'amuhu ill*aa* an yakuuna maytatan aw daman masfuu*h*an aw la*h*ma khinziirin fa-innahu rijsun aw fisqan uhilla lighay

Katakanlah, “Tidak kudapati di dalam apa yang diwahyukan kepadaku, sesuatu yang diharamkan memakannya bagi yang ingin memakannya, kecuali daging hewan yang mati (bangkai), darah yang mengalir, daging babi – karena semua itu kotor – atau hewan yang disembe

6:146

# وَعَلَى الَّذِيْنَ هَادُوْا حَرَّمْنَا كُلَّ ذِيْ ظُفُرٍۚ وَمِنَ الْبَقَرِ وَالْغَنَمِ حَرَّمْنَا عَلَيْهِمْ شُحُوْمَهُمَآ اِلَّا مَا حَمَلَتْ ظُهُوْرُهُمَآ اَوِ الْحَوَايَآ اَوْ مَا اخْتَلَطَ بِعَظْمٍۗ ذٰلِكَ جَزَيْنٰهُمْ بِبَغْيِهِمْۚ وَاِنَّا لَصٰدِ

wa'al*aa* **al**la*dz*iina h*aa*duu *h*arramn*aa* kulla *dz*ii *zh*ufurin wamina **a**lbaqari wa**a**lghanami *h*arramn*aa* 'alayhim syu*h*uumahum*aa* ill*aa*

*Dan kepada orang-orang Yahudi, Kami haramkan semua (hewan) yang berkuku, dan Kami haramkan kepada mereka lemak sapi dan domba, kecuali yang melekat di punggungnya, atau yang dalam isi perutnya, atau yang bercampur dengan tulang. Demikianlah Kami menghukum*

6:147

# فَاِنْ كَذَّبُوْكَ فَقُلْ رَّبُّكُمْ ذُوْ رَحْمَةٍ وَّاسِعَةٍۚ وَلَا يُرَدُّ بَأْسُهٗ عَنِ الْقَوْمِ الْمُجْرِمِيْنَ

fa-in ka*dzdz*abuuka faqul rabbukum *dz*uu ra*h*matin w*aa*si'atin wal*aa* yuraddu ba/suhu 'ani **a**lqawmi **a**lmujrimiin**a**

Maka jika mereka mendustakan kamu, katakanlah, “Tuhanmu mempunyai rahmat yang luas, dan siksa-Nya kepada orang-orang yang berdosa tidak dapat dielakkan.”

6:148

# سَيَقُوْلُ الَّذِيْنَ اَشْرَكُوْا لَوْ شَاۤءَ اللّٰهُ مَآ اَشْرَكْنَا وَلَآ اٰبَاۤؤُنَا وَلَا حَرَّمْنَا مِنْ شَيْءٍۗ كَذٰلِكَ كَذَّبَ الَّذِيْنَ مِنْ قَبْلِهِمْ حَتّٰى ذَاقُوْا بَأْسَنَاۗ قُلْ هَلْ عِنْدَكُمْ مِّنْ عِلْمٍ فَتُخْرِجُوْهُ لَنَاۗ اِنْ تَ

sayaquulu **al**la*dz*iina asyrakuu law sy*aa*-a **al**l*aa*hu m*aa* asyrakn*aa* wal*aa* *aa*b*aa*un*aa* wal*aa* *h*arramn*aa* min syay-in ka*dzaa*lika ka*dzdz*

*Orang-orang musyrik akan berkata, “Jika Allah menghendaki, tentu kami tidak akan mempersekutukan-Nya, begitu pula nenek moyang kami, dan kami tidak akan mengharamkan apa pun.” Demikian pula orang-orang sebelum mereka yang telah mendustakan (para rasul) sa*

6:149

# قُلْ فَلِلّٰهِ الْحُجَّةُ الْبَالِغَةُۚ فَلَوْ شَاۤءَ لَهَدٰىكُمْ اَجْمَعِيْنَ

qul falill*aa*hi **a**l*h*ujjatu **a**lb*aa*lighatu falaw sy*aa*-a lahad*aa*kum ajma'iin**a**

Katakanlah (Muhammad), “Alasan yang kuat hanya pada Allah. Maka kalau Dia menghendaki, niscaya kamu semua mendapat petunjuk.”

6:150

# قُلْ هَلُمَّ شُهَدَاۤءَكُمُ الَّذِيْنَ يَشْهَدُوْنَ اَنَّ اللّٰهَ حَرَّمَ هٰذَاۚ فَاِنْ شَهِدُوْا فَلَا تَشْهَدْ مَعَهُمْۚ وَلَا تَتَّبِعْ اَهْوَاۤءَ الَّذِيْنَ كَذَّبُوْا بِاٰيٰتِنَا وَالَّذِيْنَ لَا يُؤْمِنُوْنَ بِالْاٰخِرَةِ وَهُمْ بِرَبِّهِمْ يَعْدِلُ

qul halumma syuhad*aa*-akumu **al**la*dz*iina yasyhaduuna anna **al**l*aa*ha *h*arrama h*aadzaa* fa-in syahiduu fal*aa* tasyhad ma'ahum wal*aa* tattabi' ahw*aa*-a **al**la*dz*

Katakanlah (Muhammad), “Bawalah saksi-saksimu yang dapat membuktikan bahwa Allah mengharamkan ini.” Jika mereka memberikan kesaksian, engkau jangan (ikut pula) memberikan kesaksian bersama mereka. Jangan engkau ikuti keinginan orang-orang yang mendustakan

6:151

# ۞ قُلْ تَعَالَوْا اَتْلُ مَا حَرَّمَ رَبُّكُمْ عَلَيْكُمْ اَلَّا تُشْرِكُوْا بِهٖ شَيْـًٔا وَّبِالْوَالِدَيْنِ اِحْسَانًاۚ وَلَا تَقْتُلُوْٓا اَوْلَادَكُمْ مِّنْ اِمْلَاقٍۗ نَحْنُ نَرْزُقُكُمْ وَاِيَّاهُمْ ۚوَلَا تَقْرَبُوا الْفَوَاحِشَ مَا ظَهَرَ مِنْهَا

qul ta'*aa*law atlu m*aa* *h*arrama rabbukum 'alaykum **al**l*aa* tusyrikuu bihi syay-an wabi**a**lw*aa*lidayni i*h*s*aa*nan wal*aa* taqtuluu awl*aa*dakum min iml*aa*qin na*h*

*Katakanlah (Muhammad), “Marilah aku bacakan apa yang diharamkan Tuhan kepadamu. Jangan mempersekutukan-Nya dengan apa pun, berbuat baik kepada ibu bapak, janganlah membunuh anak-anakmu karena miskin. Kamilah yang memberi rezeki kepadamu dan kepada mereka;*

6:152

# وَلَا تَقْرَبُوْا مَالَ الْيَتِيْمِ اِلَّا بِالَّتِيْ هِيَ اَحْسَنُ حَتّٰى يَبْلُغَ اَشُدَّهٗ ۚوَاَوْفُوا الْكَيْلَ وَالْمِيْزَانَ بِالْقِسْطِۚ لَا نُكَلِّفُ نَفْسًا اِلَّا وُسْعَهَاۚ وَاِذَا قُلْتُمْ فَاعْدِلُوْا وَلَوْ كَانَ ذَا قُرْبٰىۚ وَبِعَهْدِ الل

wal*aa* taqrabuu m*aa*la **a**lyatiimi ill*aa* bi**a**llatii hiya a*h*sanu *h*att*aa* yablugha asyuddahu wa-awfuu **a**lkayla wa**a**lmiiz*aa*na bi**a**lqi

Dan janganlah kamu mendekati harta anak yatim, kecuali dengan cara yang lebih bermanfaat, sampai dia mencapai (usia) dewasa. Dan sempurnakanlah takaran dan timbangan dengan adil. Kami tidak membebani seseorang melainkan menurut kesanggupannya. Apabila kam

6:153

# وَاَنَّ هٰذَا صِرَاطِيْ مُسْتَقِيْمًا فَاتَّبِعُوْهُ ۚوَلَا تَتَّبِعُوا السُّبُلَ فَتَفَرَّقَ بِكُمْ عَنْ سَبِيْلِهٖ ۗذٰلِكُمْ وَصّٰىكُمْ بِهٖ لَعَلَّكُمْ تَتَّقُوْنَ

wa-anna h*aadzaa* *sh*ir*aath*ii mustaqiiman fa**i**ttabi'uuhu wal*aa* tattabi'uu **al**ssubula fatafarraqa bikum 'an sabiilihi *dzaa*likum wa*shshaa*kum bihi la'allakum tattaquun**a**

Dan sungguh, inilah jalan-Ku yang lurus. Maka ikutilah! Jangan kamu ikuti jalan-jalan (yang lain) yang akan mencerai-beraikan kamu dari jalan-Nya. Demikianlah Dia memerintahkan kepadamu agar kamu bertakwa.

6:154

# ثُمَّ اٰتَيْنَا مُوْسَى الْكِتٰبَ تَمَامًا عَلَى الَّذِيْٓ اَحْسَنَ وَتَفْصِيْلًا لِّكُلِّ شَيْءٍ وَّهُدًى وَّرَحْمَةً لَّعَلَّهُمْ بِلِقَاۤءِ رَبِّهِمْ يُؤْمِنُوْنَ ࣖ

tsumma *aa*tayn*aa* muus*aa* **a**lkit*aa*ba tam*aa*man 'al*aa* **al**la*dz*ii a*h*sana wataf*sh*iilan likulli syay-in wahudan wara*h*matan la'allahum biliq*aa*-i rabbihim yu

Kemudian Kami telah memberikan kepada Musa Kitab (Taurat) untuk menyempurnakan (nikmat Kami) kepada orang yang berbuat kebaikan, untuk menjelaskan segala sesuatu, dan sebagai petunjuk dan rahmat, agar mereka beriman akan adanya pertemuan dengan Tuhannya.

6:155

# وَهٰذَا كِتٰبٌ اَنْزَلْنٰهُ مُبٰرَكٌ فَاتَّبِعُوْهُ وَاتَّقُوْا لَعَلَّكُمْ تُرْحَمُوْنَۙ

wah*aadzaa* kit*aa*bun anzaln*aa*hu mub*aa*rakun fa**i**ttabi'uuhu wa**i**ttaquu la'allakum tur*h*amuun**a**

Dan ini adalah Kitab (Al-Qur'an) yang Kami turunkan dengan penuh berkah. Ikutilah, dan bertakwalah agar kamu mendapat rahmat,

6:156

# اَنْ تَقُوْلُوْٓا اِنَّمَآ اُنْزِلَ الْكِتٰبُ عَلٰى طَاۤىِٕفَتَيْنِ مِنْ قَبْلِنَاۖ وَاِنْ كُنَّا عَنْ دِرَاسَتِهِمْ لَغٰفِلِيْنَۙ

an taquuluu innam*aa* unzila **a**lkit*aa*bu 'al*aa* *thaa*-ifatayni min qablin*aa* wa-in kunn*aa* 'an dir*aa*satihim lagh*aa*filiin**a**

(Kami turunkan Al-Qur'an itu) agar kamu (tidak) mengatakan, “Kitab itu hanya diturunkan kepada dua golongan sebelum kami (Yahudi dan Nasrani) dan sungguh, kami tidak memperhatikan apa yang mereka baca,”

6:157

# اَوْ تَقُوْلُوْا لَوْ اَنَّآ اُنْزِلَ عَلَيْنَا الْكِتٰبُ لَكُنَّآ اَهْدٰى مِنْهُمْۚ فَقَدْ جَاۤءَكُمْ بَيِّنَةٌ مِّنْ رَّبِّكُمْ وَهُدًى وَّرَحْمَةٌ ۚفَمَنْ اَظْلَمُ مِمَّنْ كَذَّبَ بِاٰيٰتِ اللّٰهِ وَصَدَفَ عَنْهَا ۗسَنَجْزِى الَّذِيْنَ يَصْدِفُوْنَ ع

aw taquuluu law ann*aa* unzila 'alayn*aa* **a**lkit*aa*bu lakunn*aa* ahd*aa* minhum faqad j*aa*-akum bayyinatun min rabbikum wahudan wara*h*matun faman a*zh*lamu mimman ka*dzdz*aba bi-*aa*y

atau agar kamu (tidak) mengatakan, “Jikalau Kitab itu diturunkan kepada kami, tentulah kami lebih mendapat petunjuk daripada mereka.” Sungguh, telah datang kepadamu penjelasan yang nyata, petunjuk dan rahmat dari Tuhanmu. Siapakah yang lebih zalim daripad

6:158

# هَلْ يَنْظُرُوْنَ اِلَّآ اَنْ تَأْتِيَهُمُ الْمَلٰۤىِٕكَةُ اَوْ يَأْتِيَ رَبُّكَ اَوْ يَأْتِيَ بَعْضُ اٰيٰتِ رَبِّكَ ۗيَوْمَ يَأْتِيْ بَعْضُ اٰيٰتِ رَبِّكَ لَا يَنْفَعُ نَفْسًا اِيْمَانُهَا لَمْ تَكُنْ اٰمَنَتْ مِنْ قَبْلُ اَوْ كَسَبَتْ فِيْٓ اِيْمَانِهَ

hal yan*zh*uruuna ill*aa* an ta/tiyahumu **a**lmal*aa*-ikatu aw ya/tiya rabbuka aw ya/tiya ba'*dh*u *aa*y*aa*ti rabbika yawma ya/tii ba'*dh*u *aa*y*aa*ti rabbika l*aa* yanfa'u nafsan iim*aa<*

Yang mereka nanti-nantikan hanyalah kedatangan malaikat kepada mereka, atau kedatangan Tuhanmu, atau sebagian tanda-tanda dari Tuhanmu. Pada hari datangnya sebagian tanda-tanda Tuhanmu tidak berguna lagi iman seseorang yang belum beriman sebelum itu, atau

6:159

# اِنَّ الَّذِيْنَ فَرَّقُوْا دِيْنَهُمْ وَكَانُوْا شِيَعًا لَّسْتَ مِنْهُمْ فِيْ شَيْءٍۗ اِنَّمَآ اَمْرُهُمْ اِلَى اللّٰهِ ثُمَّ يُنَبِّئُهُمْ بِمَا كَانُوْا يَفْعَلُوْنَ

inna **al**la*dz*iina farraquu diinahum wak*aa*nuu syiya'an lasta minhum fii syay-in innam*aa* amruhum il*aa* **al**l*aa*hi tsumma yunabbi-uhum bim*aa* k*aa*nuu yaf'aluun**a**

Sesungguhnya orang-orang yang memecah belah agamanya dan mereka menjadi (terpecah) dalam golongan-golongan, sedikit pun bukan tanggung jawabmu (Muhammad) atas mereka. Sesungguhnya urusan mereka (terserah) kepada Allah. Kemudian Dia akan memberitahukan kep

6:160

# مَنْ جَاۤءَ بِالْحَسَنَةِ فَلَهٗ عَشْرُ اَمْثَالِهَا ۚوَمَنْ جَاۤءَ بِالسَّيِّئَةِ فَلَا يُجْزٰٓى اِلَّا مِثْلَهَا وَهُمْ لَا يُظْلَمُوْنَ

man j*aa*-a bi**a**l*h*asanati falahu 'asyru amts*aa*lih*aa* waman j*aa*-a bi**al**ssayyi-*aa*ti fal*aa* yujz*aa* ill*aa* mitslah*aa* wahum l*aa* yu*zh*lamuun**a**

**Barangsiapa berbuat kebaikan mendapat balasan sepuluh kali lipat amalnya. Dan barangsiapa berbuat kejahatan dibalas seimbang dengan kejahatannya. Mereka sedikit pun tidak dirugikan (dizalimi).**

6:161

# قُلْ اِنَّنِيْ هَدٰىنِيْ رَبِّيْٓ اِلٰى صِرَاطٍ مُّسْتَقِيْمٍ ەۚ دِيْنًا قِيَمًا مِّلَّةَ اِبْرٰهِيْمَ حَنِيْفًاۚ وَمَا كَانَ مِنَ الْمُشْرِكِيْنَ

qul innanii had*aa*nii rabbii il*aa* *sh*ir*aath*in mustaqiimin diinan qiyaman millata ibr*aa*hiima *h*aniifan wam*aa* k*aa*na mina **a**lmusyrikiin**a**

Katakanlah (Muhammad), “Sesungguhnya Tuhanku telah memberiku petunjuk ke jalan yang lurus, agama yang benar, agama Ibrahim yang lurus. Dia (Ibrahim) tidak termasuk orang-orang musyrik.”

6:162

# قُلْ اِنَّ صَلَاتِيْ وَنُسُكِيْ وَمَحْيَايَ وَمَمَاتِيْ لِلّٰهِ رَبِّ الْعٰلَمِيْنَۙ

qul inna *sh*al*aa*tii wanusukii wama*h*y*aa*ya wamam*aa*tii lill*aa*hi rabbi **a**l'*aa*lamiin**a**

Katakanlah (Muhammad), “Sesungguhnya salatku, ibadahku, hidupku dan matiku hanyalah untuk Allah, Tuhan seluruh alam,

6:163

# لَا شَرِيْكَ لَهٗ ۚوَبِذٰلِكَ اُمِرْتُ وَاَنَا۠ اَوَّلُ الْمُسْلِمِيْنَ

l*aa* syariika lahu wabi*dzaa*lika umirtu wa-an*aa* awwalu **a**lmuslimiin**a**

tidak ada sekutu bagi-Nya; dan demikianlah yang diperintahkan kepadaku dan aku adalah orang yang pertama-tama berserah diri (muslim).”

6:164

# قُلْ اَغَيْرَ اللّٰهِ اَبْغِيْ رَبًّا وَّهُوَ رَبُّ كُلِّ شَيْءٍۗ وَلَا تَكْسِبُ كُلُّ نَفْسٍ اِلَّا عَلَيْهَاۚ وَلَا تَزِرُ وَازِرَةٌ وِّزْرَ اُخْرٰىۚ ثُمَّ اِلٰى رَبِّكُمْ مَّرْجِعُكُمْ فَيُنَبِّئُكُمْ بِمَا كُنْتُمْ فِيْهِ تَخْتَلِفُوْنَ

qul aghayra **al**l*aa*hi abghii rabban wahuwa rabbu kulli syay-in wal*aa* taksibu kullu nafsin ill*aa* 'alayh*aa* wal*aa* taziru w*aa*ziratun wizra ukhr*aa* tsumma il*aa* rabbikum marji'ukum fayunabbi-

Katakanlah (Muhammad), “Apakah (patut) aku mencari tuhan selain Allah, padahal Dialah Tuhan bagi segala sesuatu. Setiap perbuatan dosa seseorang, dirinya sendiri yang bertanggung jawab. Dan seseorang tidak akan memikul beban dosa orang lain. Kemudian kepa

6:165

# وَهُوَ الَّذِيْ جَعَلَكُمْ خَلٰۤىِٕفَ الْاَرْضِ وَرَفَعَ بَعْضَكُمْ فَوْقَ بَعْضٍ دَرَجٰتٍ لِّيَبْلُوَكُمْ فِيْ مَآ اٰتٰىكُمْۗ اِنَّ رَبَّكَ سَرِيْعُ الْعِقَابِۖ وَاِنَّهٗ لَغَفُوْرٌ رَّحِيْمٌ ࣖ

wahuwa **al**la*dz*ii ja'alakum khal*aa*-ifa **a**l-ar*dh*i warafa'a ba'*dh*akum fawqa ba'*dh*in daraj*aa*tin liyabluwakum fii m*aa* *aa*t*aa*kum inna rabbaka sarii'u **a**

Dan Dialah yang menjadikan kamu penguasa-penguasa di bumi dan Dia meninggikan sebahagian kamu atas sebahagian (yang lain) beberapa derajat, untuk mengujimu tentang apa yang diberikan-Nya kepadamu. Sesungguhnya Tuhanmu amat cepat siksaan-Nya dan sesungguhnya Dia Maha Pengampun lagi Maha Penyayang.

<!--EndFragment-->