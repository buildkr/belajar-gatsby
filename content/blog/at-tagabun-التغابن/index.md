---
title: (64) At-Tagabun - التغابن
date: 2021-10-27T04:04:00.088Z
ayat: 64
description: "Jumlah Ayat: 18 / Arti: Pengungkapan Kesalahan"
---
<!--StartFragment-->

64:1

# يُسَبِّحُ لِلّٰهِ مَا فِى السَّمٰوٰتِ وَمَا فِى الْاَرْضِۗ لَهُ الْمُلْكُ وَلَهُ الْحَمْدُۖ وَهُوَ عَلٰى كُلِّ شَيْءٍ قَدِيْرٌ

yusabbi*h*u lill*aa*hi m*aa* fii **al**ssam*aa*w*aa*ti wam*aa* fii **a**l-ar*dh*i lahu **a**lmulku walahu **a**l*h*amdu wahuwa 'al*aa* kulli syay-in qadiir

Apa yang ada di langit dan apa yang ada di bumi senantiasa bertasbih kepada Allah; milik-Nya semua kerajaan dan bagi-Nya (pula) segala puji; dan Dia Mahakuasa atas segala sesuatu.

64:2

# هُوَ الَّذِيْ خَلَقَكُمْ فَمِنْكُمْ كَافِرٌ وَّمِنْكُمْ مُّؤْمِنٌۗ وَاللّٰهُ بِمَا تَعْمَلُوْنَ بَصِيْرٌ

huwa **al**la*dz*ii khalaqakum faminkum k*aa*firun waminkum mu/minun wa**al**l*aa*hu bim*aa* ta'maluuna ba*sh*iir**un**

Dialah yang menciptakan kamu, lalu di antara kamu ada yang kafir dan di antara kamu (juga) ada yang mukmin. Dan Allah Maha Melihat apa yang kamu kerjakan.

64:3

# خَلَقَ السَّمٰوٰتِ وَالْاَرْضَ بِالْحَقِّ وَصَوَّرَكُمْ فَاَحْسَنَ صُوَرَكُمْۚ وَاِلَيْهِ الْمَصِيْرُ

khalaqa **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*a bi**a**l*h*aqqi wa*sh*awwarakum fa-a*h*sana *sh*uwarakum wa-ilayhi **a**lma*sh*iir**u**

Dia menciptakan langit dan bumi dengan (tujuan) yang benar, Dia membentuk rupamu lalu memperbagus rupamu, dan kepada-Nya tempat kembali.

64:4

# يَعْلَمُ مَا فِى السَّمٰوٰتِ وَالْاَرْضِ وَيَعْلَمُ مَا تُسِرُّوْنَ وَمَا تُعْلِنُوْنَۗ وَاللّٰهُ عَلِيْمٌ ۢبِذَاتِ الصُّدُوْرِ

ya'lamu m*aa* fii **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i waya'lamu m*aa* tusirruuna wam*aa* tu'linuuna wa**al**l*aa*hu 'aliimun bi*dzaa*ti **al***shsh*uduur

Dia mengetahui apa yang di langit dan di bumi, dan mengetahui apa yang kamu rahasiakan dan apa yang kamu nyatakan. Dan Allah Maha Mengetahui segala isi hati.

64:5

# اَلَمْ يَأْتِكُمْ نَبَؤُا الَّذِيْنَ كَفَرُوْا مِنْ قَبْلُ ۖفَذَاقُوْا وَبَالَ اَمْرِهِمْ وَلَهُمْ عَذَابٌ اَلِيْمٌ

alam ya/tikum nabau **al**la*dz*iina kafaruu min qablu fa*dzaa*quu wab*aa*la amrihim walahum 'a*dzaa*bun **a**liim**un**

Apakah belum sampai kepadamu (orang-orang kafir) berita orang-orang kafir dahulu? Maka mereka telah merasakan akibat buruk dari perbuatannya dan mereka memperoleh azab yang pedih.

64:6

# ذٰلِكَ بِاَنَّهٗ كَانَتْ تَّأْتِيْهِمْ رُسُلُهُمْ بِالْبَيِّنٰتِ فَقَالُوْٓا اَبَشَرٌ يَّهْدُوْنَنَاۖ فَكَفَرُوْا وَتَوَلَّوْا وَّاسْتَغْنَى اللّٰهُ ۗوَاللّٰهُ غَنِيٌّ حَمِيْدٌ

*dzaa*lika bi-annahu k*aa*nat ta/tiihim rusuluhum bi**a**lbayyin*aa*ti faq*aa*luu abasyarun yahduunan*aa* fakafaruu watawallaw wa**i**staghn*aa* **al**l*aa*hu wa**al**l

Yang demikian itu karena sesungguhnya ketika rasul-rasul datang kepada mereka (membawa) keterangan-ke-terangan lalu mereka berkata, “Apakah (pantas) manusia yang memberi petunjuk kepada kami?” Lalu mereka ingkar dan berpaling; padahal Allah tidak memerlu

64:7

# زَعَمَ الَّذِيْنَ كَفَرُوْٓا اَنْ لَّنْ يُّبْعَثُوْاۗ قُلْ بَلٰى وَرَبِّيْ لَتُبْعَثُنَّ ثُمَّ لَتُنَبَّؤُنَّ بِمَا عَمِلْتُمْۗ وَذٰلِكَ عَلَى اللّٰهِ يَسِيْرٌ

za'ama **al**la*dz*iina kafaruu an lan yub'atsuu qul bal*aa* warabbii latub'atsunna tsumma latunabbaunna bim*aa* 'amiltum wa*dzaa*lika 'al*aa* **al**l*aa*hi yasiir**un**

Orang-orang yang kafir mengira, bahwa mereka tidak akan dibangkitkan. Katakanlah (Muhammad), “Tidak demikian, demi Tuhanku, kamu pasti dibangkitkan, kemudian diberitakan semua yang telah kamu kerjakan.” Dan yang demikian itu mudah bagi Allah.

64:8

# فَاٰمِنُوْا بِاللّٰهِ وَرَسُوْلِهٖ وَالنُّوْرِ الَّذِيْٓ اَنْزَلْنَاۗ وَاللّٰهُ بِمَا تَعْمَلُوْنَ خَبِيْرٌ

fa*aa*minuu bi**al**l*aa*hi warasuulihi wa**al**nnuuri **al**la*dz*ii anzaln*aa* wa**al**l*aa*hu bim*aa* ta'maluuna khabiir**un**

Maka berimanlah kamu kepada Allah dan Rasul-Nya dan kepada cahaya (Al-Qur'an) yang telah Kami turunkan. Dan Allah Mahateliti terhadap apa yang kamu kerjakan.

64:9

# يَوْمَ يَجْمَعُكُمْ لِيَوْمِ الْجَمْعِ ذٰلِكَ يَوْمُ التَّغَابُنِۗ وَمَنْ يُّؤْمِنْۢ بِاللّٰهِ وَيَعْمَلْ صَالِحًا يُّكَفِّرْ عَنْهُ سَيِّاٰتِهٖ وَيُدْخِلْهُ جَنّٰتٍ تَجْرِيْ مِنْ تَحْتِهَا الْاَنْهٰرُ خٰلِدِيْنَ فِيْهَآ اَبَدًاۗ ذٰلِكَ الْفَوْزُ الْعَظِ

yawma yajma'ukum liyawmi **a**ljam'i *dzaa*lika yawmu **al**ttagh*aa*buni waman yu/min bi**al**l*aa*hi waya'mal *shaa*li*h*an yukaffir 'anhu sayyi-*aa*tihi wayudkhilhu jann*aa*tin t

(Ingatlah) pada hari (ketika) Allah mengumpulkan kamu pada hari berhimpun, itulah hari pengungkapan kesalahan-kesalahan. Dan barangsiapa beriman kepada Allah dan mengerjakan kebajikan niscaya Allah akan menghapus kesalahan-kesalahannya dan memasukkannya k

64:10

# وَالَّذِيْنَ كَفَرُوْا وَكَذَّبُوْا بِاٰيٰتِنَآ اُولٰۤىِٕكَ اَصْحٰبُ النَّارِ خٰلِدِيْنَ فِيْهَاۗ وَبِئْسَ الْمَصِيْرُ ࣖ

wa**a**lla*dz*iina kafaruu waka*dzdz*abuu bi-*aa*y*aa*tin*aa* ul*aa*-ika a*sh*-*haa*bu **al**nn*aa*ri kh*aa*lidiina fiih*aa* wabi/sa **a**lma*sh*iir

Dan orang-orang yang kafir dan mendustakan ayat-ayat Kami, mereka itulah penghuni neraka, mereka kekal di dalamnya. Dan itulah seburuk-buruk tempat kembali.







64:11

# مَآ اَصَابَ مِنْ مُّصِيْبَةٍ اِلَّا بِاِذْنِ اللّٰهِ ۗوَمَنْ يُّؤْمِنْۢ بِاللّٰهِ يَهْدِ قَلْبَهٗ ۗوَاللّٰهُ بِكُلِّ شَيْءٍ عَلِيْمٌ

m*aa* a*shaa*ba min mu*sh*iibatin ill*aa* bi-i*dz*ni **al**l*aa*hi waman yu/min bi**al**l*aa*hi yahdi qalbahu wa**al**l*aa*hu bikulli syay-in 'aliim**un**

Tidak ada sesuatu musibah yang menimpa (seseorang), kecuali dengan izin Allah; dan barangsiapa beriman kepada Allah, niscaya Allah akan memberi petunjuk kepada hatinya. Dan Allah Maha Mengetahui segala sesuatu.

64:12

# وَاَطِيْعُوا اللّٰهَ وَاَطِيْعُوا الرَّسُوْلَۚ فَاِنْ تَوَلَّيْتُمْ فَاِنَّمَا عَلٰى رَسُوْلِنَا الْبَلٰغُ الْمُبِيْنُ

wa-a*th*ii'uu **al**l*aa*ha wa-a*th*ii'uu **al**rrasuula fa-in tawallaytum fa-innam*aa* 'al*aa* rasuulin*aa* **a**lbal*aa*ghu **a**lmubiin**u**

Dan taatlah kepada Allah dan taatlah kepada Rasul. Jika kamu berpaling maka sesungguhnya kewajiban Rasul Kami hanyalah menyampaikan (amanah Allah) dengan terang.

64:13

# اَللّٰهُ لَآ اِلٰهَ اِلَّا هُوَۗ وَعَلَى اللّٰهِ فَلْيَتَوَكَّلِ الْمُؤْمِنُوْنَ

**al**l*aa*hu l*aa* il*aa*ha ill*aa* huwa wa'al*aa* **al**l*aa*hi falyatawakkali **a**lmu/minuun**a**

(Dialah) Allah, tidak ada tuhan selain Dia. Dan hendaklah orang-orang mukmin bertawakal kepada Allah.

64:14

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْٓا اِنَّ مِنْ اَزْوَاجِكُمْ وَاَوْلَادِكُمْ عَدُوًّا لَّكُمْ فَاحْذَرُوْهُمْۚ وَاِنْ تَعْفُوْا وَتَصْفَحُوْا وَتَغْفِرُوْا فَاِنَّ اللّٰهَ غَفُوْرٌ رَّحِيْمٌ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu inna min azw*aa*jikum wa-awl*aa*dikum 'aduwwan lakum fa**i***hts*aruuhum wa-in ta'fuu wata*sh*fa*h*uu wataghfiruu fa-inna **al**

Wahai orang-orang yang beriman! Sesungguhnya di antara istri-istrimu dan anak-anakmu ada yang menjadi musuh bagimu, maka berhati-hatilah kamu terhadap mereka; dan jika kamu maafkan dan kamu santuni serta ampuni (mereka), maka sungguh, Allah Maha Pengampun

64:15

# اِنَّمَآ اَمْوَالُكُمْ وَاَوْلَادُكُمْ فِتْنَةٌ ۗوَاللّٰهُ عِنْدَهٗٓ اَجْرٌ عَظِيْمٌ

innam*aa* amw*aa*lukum wa-awl*aa*dukum fitnatun wa**al**l*aa*hu 'indahu ajrun 'a*zh*iim**un**

Sesungguhnya hartamu dan anak-anakmu hanyalah cobaan (bagimu), dan di sisi Allah pahala yang besar.

64:16

# فَاتَّقُوا اللّٰهَ مَا اسْتَطَعْتُمْ وَاسْمَعُوْا وَاَطِيْعُوْا وَاَنْفِقُوْا خَيْرًا لِّاَنْفُسِكُمْۗ وَمَنْ يُّوْقَ شُحَّ نَفْسِهٖ فَاُولٰۤىِٕكَ هُمُ الْمُفْلِحُوْنَ

fa**i**ttaquu **al**l*aa*ha m*aa* ista*th*a'tum wa**i**sma'uu wa-a*th*ii'uu wa-anfiquu khayran li-anfusikum waman yuuqa syu*hh*a nafsihi faul*aa*-ika humu **a**lmufli*h*

*Maka bertakwalah kamu kepada Allah menurut kesanggupanmu dan dengarlah serta taatlah; dan infakkanlah harta yang baik untuk dirimu. Dan barang-siapa dijaga dirinya dari kekikiran, mereka itulah orang-orang yang beruntung.*









64:17

# اِنْ تُقْرِضُوا اللّٰهَ قَرْضًا حَسَنًا يُّضٰعِفْهُ لَكُمْ وَيَغْفِرْ لَكُمْۗ وَاللّٰهُ شَكُوْرٌ حَلِيْمٌۙ

in tuqri*dh*uu **al**l*aa*ha qar*dh*an *h*asanan yu*daa*'ifhu lakum wayaghfir lakum wa**al**l*aa*hu syakuurun *h*aliim**un**

Jika kamu meminjamkan kepada Allah dengan pinjaman yang baik, niscaya Dia melipatgandakan (balasan) untukmu dan mengampuni kamu. Dan Allah Maha Mensyukuri, Maha Penyantun.

64:18

# عٰلِمُ الْغَيْبِ وَالشَّهَادَةِ الْعَزِيْزُ الْحَكِيْمُ ࣖ

'*aa*limu **a**lghaybi wa**al**sysyah*aa*dati **a**l'aziizu **a**l*h*akiim**u**

Yang Mengetahui yang gaib dan yang nyata. Yang Mahaperkasa, Mahabijaksana.

<!--EndFragment-->