---
title: (90) Al-Balad - البلد
date: 2021-10-27T04:26:50.336Z
ayat: 90
description: "Jumlah Ayat: 20 / Arti: Negeri"
---
<!--StartFragment-->

90:1

# لَآ اُقْسِمُ بِهٰذَا الْبَلَدِۙ

l*aa* uqsimu bih*aadzaa* **a**lbalad**i**

Aku bersumpah dengan negeri ini (Mekah),

90:2

# وَاَنْتَ حِلٌّۢ بِهٰذَا الْبَلَدِۙ

wa-anta *h*illun bih*aadzaa* **a**lbalad**i**

dan engkau (Muhammad), bertempat di negeri (Mekah) ini,

90:3

# وَوَالِدٍ وَّمَا وَلَدَۙ

waw*aa*lidin wam*aa* walad**a**

dan demi (pertalian) bapak dan anaknya.

90:4

# لَقَدْ خَلَقْنَا الْاِنْسَانَ فِيْ كَبَدٍۗ

laqad khalaqn*aa* **a**l-ins*aa*na fii kabad**in**

Sungguh, Kami telah menciptakan manusia berada dalam susah payah.

90:5

# اَيَحْسَبُ اَنْ لَّنْ يَّقْدِرَ عَلَيْهِ اَحَدٌ ۘ

aya*h*sabu an lan yaqdira 'alayhi a*h*ad**un**

Apakah dia (manusia) itu mengira bahwa tidak ada sesuatu pun yang berkuasa atasnya?

90:6

# يَقُوْلُ اَهْلَكْتُ مَالًا لُّبَدًاۗ

yaquulu ahlaktu m*aa*lan lubad*aa***n**

Dia mengatakan, “Aku telah menghabiskan harta yang banyak.”

90:7

# اَيَحْسَبُ اَنْ لَّمْ يَرَهٗٓ اَحَدٌۗ

aya*h*sabu an lam yarahu a*h*ad**un**

Apakah dia mengira bahwa tidak ada sesuatu pun yang melihatnya?

90:8

# اَلَمْ نَجْعَلْ لَّهٗ عَيْنَيْنِۙ

alam naj'al lahu 'aynayn**i**

Bukankah Kami telah menjadikan untuknya sepasang mata,

90:9

# وَلِسَانًا وَّشَفَتَيْنِۙ

walis*aa*nan wasyafatayn**i**

dan lidah dan sepasang bibir?

90:10

# وَهَدَيْنٰهُ النَّجْدَيْنِۙ

wahadayn*aa*hu **al**nnajdayn**i**

Dan Kami telah menunjukkan kepadanya dua jalan (kebajikan dan kejahatan),

90:11

# فَلَا اقْتَحَمَ الْعَقَبَةَ ۖ

fal*aa* iqta*h*ama **a**l'aqaba**ta**

tetapi dia tidak menempuh jalan yang mendaki dan sukar?

90:12

# وَمَآ اَدْرٰىكَ مَا الْعَقَبَةُ ۗ

wam*aa* adr*aa*ka m*aa* **a**l'aqaba**tu**

Dan tahukah kamu apakah jalan yang mendaki dan sukar itu?

90:13

# فَكُّ رَقَبَةٍۙ

fakku raqaba**tin**

(yaitu) melepaskan perbudakan (hamba sahaya),

90:14

# اَوْ اِطْعَامٌ فِيْ يَوْمٍ ذِيْ مَسْغَبَةٍۙ

aw i*th*'*aa*mun fii yawmin *dz*ii masghaba**tin**

atau memberi makan pada hari terjadi kelaparan,

90:15

# يَّتِيْمًا ذَا مَقْرَبَةٍۙ

yatiiman *dzaa* maqraba**tin**

(kepada) anak yatim yang ada hubungan kerabat,

90:16

# اَوْ مِسْكِيْنًا ذَا مَتْرَبَةٍۗ

aw miskiinan *dzaa* matraba**tin**

atau orang miskin yang sangat fakir.

90:17

# ثُمَّ كَانَ مِنَ الَّذِيْنَ اٰمَنُوْا وَتَوَاصَوْا بِالصَّبْرِ وَتَوَاصَوْا بِالْمَرْحَمَةِۗ

tsumma k*aa*na mina **al**la*dz*iina *aa*manuu wataw*aas*aw bi**al***shsh*abri wataw*aas*aw bi**a**lmar*h*ama**ti**

Kemudian dia termasuk orang-orang yang beriman dan saling berpesan untuk bersabar dan saling berpesan untuk berkasih sayang.

90:18

# اُولٰۤىِٕكَ اَصْحٰبُ الْمَيْمَنَةِۗ

ul*aa*-ika a*sh*-*haa*bu **a**lmaymana**ti**

Mereka (orang-orang yang beriman dan saling berpesan itu) adalah golongan kanan.

90:19

# وَالَّذِيْنَ كَفَرُوْا بِاٰيٰتِنَا هُمْ اَصْحٰبُ الْمَشْئَمَةِۗ

wa**a**lla*dz*iina kafaruu bi-*aa*y*aa*tin*aa* hum a*sh*-*haa*bu **a**lmasy-ama**ti**

Dan orang-orang yang kafir kepada ayat-ayat Kami, mereka itu adalah golongan kiri.

90:20

# عَلَيْهِمْ نَارٌ مُّؤْصَدَةٌ ࣖ

'alayhim n*aa*run mu/*sh*ada**tun**

Mereka berada dalam neraka yang ditutup rapat.

<!--EndFragment-->