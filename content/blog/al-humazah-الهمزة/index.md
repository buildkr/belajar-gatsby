---
title: (104) Al-Humazah - الهمزة
date: 2021-10-27T04:16:29.662Z
ayat: 104
description: "Jumlah Ayat: 9 / Arti: Pengumpat"
---
<!--StartFragment-->

104:1

# وَيْلٌ لِّكُلِّ هُمَزَةٍ لُّمَزَةٍۙ

waylun likulli humazatin lumaza**tin**

Celakalah bagi setiap pengumpat dan pencela,

104:2

# ۨالَّذِيْ جَمَعَ مَالًا وَّعَدَّدَهٗۙ

**al**la*dz*ii jama'a m*aa*lan wa'addadah**u**

yang mengumpulkan harta dan menghitung-hitungnya,

104:3

# يَحْسَبُ اَنَّ مَالَهٗٓ اَخْلَدَهٗۚ

ya*h*sabu anna m*aa*lahu akhladah**u**

dia (manusia) mengira bahwa hartanya itu dapat mengekalkannya.

104:4

# كَلَّا لَيُنْۢبَذَنَّ فِى الْحُطَمَةِۖ

kall*aa* layunba*dz*anna fii **a**l*h*u*th*ama**ti**

Sekali-kali tidak! Pasti dia akan dilemparkan ke dalam (neraka) Hutamah.

104:5

# وَمَآ اَدْرٰىكَ مَا الْحُطَمَةُ ۗ

wam*aa* adr*aa*ka m*aa* **a**l*h*u*th*ama**tu**

Dan tahukah kamu apakah (neraka) Hutamah itu?

104:6

# نَارُ اللّٰهِ الْمُوْقَدَةُۙ

n*aa*ru **al**l*aa*hi **a**lmuuqada**tu**

(Yaitu) api (azab) Allah yang dinyalakan,

104:7

# الَّتِيْ تَطَّلِعُ عَلَى الْاَفْـِٕدَةِۗ

**al**latii ta*ththh*ali'u 'al*aa* **a**l-af-ida**ti**

yang (membakar) sampai ke hati.

104:8

# اِنَّهَا عَلَيْهِمْ مُّؤْصَدَةٌۙ

lnnah*aa* 'alayhim mu/*sh*ada**tun**

Sungguh, api itu ditutup rapat atas (diri) mereka,

104:9

# فِيْ عَمَدٍ مُّمَدَّدَةٍ ࣖ

fii 'amadin mumaddada**tin**

(sedang mereka itu) diikat pada tiang-tiang yang panjang.

<!--EndFragment-->