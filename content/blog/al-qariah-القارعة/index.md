---
title: (101) Al-Qari'ah - القارعة
date: 2021-10-27T04:15:03.183Z
ayat: 101
description: "Jumlah Ayat: 11 / Arti: Hari Kiamat"
---
<!--StartFragment-->

101:1

# اَلْقَارِعَةُۙ

alq*aa*ri'a**tu**

Hari Kiamat,

101:2

# مَا الْقَارِعَةُ ۚ

m*aa* **a**lq*aa*ri'a**tu**

Apakah hari Kiamat itu?

101:3

# وَمَآ اَدْرٰىكَ مَا الْقَارِعَةُ ۗ

wam*aa* adr*aa*ka m*aa* **a**lq*aa*ri'a**tu**

Dan tahukah kamu apakah hari Kiamat itu?

101:4

# يَوْمَ يَكُوْنُ النَّاسُ كَالْفَرَاشِ الْمَبْثُوْثِۙ

yawma yakuunu **al**nn*aa*su ka**a**lfar*aa*syi **a**lmabtsuuts**i**

Pada hari itu manusia seperti laron yang beterbangan,

101:5

# وَتَكُوْنُ الْجِبَالُ كَالْعِهْنِ الْمَنْفُوْشِۗ

watakuunu **a**ljib*aa*lu ka**a**l'ihni **a**lmanfuusy**i**

dan gunung-gunung seperti bulu yang dihambur-hamburkan.

101:6

# فَاَمَّا مَنْ ثَقُلَتْ مَوَازِينُهٗۙ

fa-amm*aa* man tsaqulat maw*aa*ziinuh**u**

Maka adapun orang yang berat timbangan (kebaikan)nya,

101:7

# فَهُوَ فِيْ عِيْشَةٍ رَّاضِيَةٍۗ

fahuwa fii 'iisyatin r*aad*iya**tin**

maka dia berada dalam kehidupan yang memuaskan (senang).

101:8

# وَاَمَّا مَنْ خَفَّتْ مَوَازِيْنُهٗۙ

wa-amm*aa* man khaffat maw*aa*ziinuh**u**

Dan adapun orang yang ringan timbangan (kebaikan)nya,

101:9

# فَاُمُّهُ هَاوِيَةٌ ۗ

faummuhu h*aa*wiya**tun**

maka tempat kembalinya adalah neraka Hawiyah.

101:10

# وَمَآ اَدْرٰىكَ مَا هِيَهْۗ

wam*aa* adr*aa*ka m*aa* hiya**h**

Dan tahukah kamu apakah neraka Hawiyah itu?

101:11

# نَارٌ حَامِيَةٌ ࣖ

n*aa*run *haa*miya**tun**

(Yaitu) api yang sangat panas.

<!--EndFragment-->