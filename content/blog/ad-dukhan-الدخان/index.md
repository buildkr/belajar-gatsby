---
title: (44) Ad-Dukhan - الدخان
date: 2021-10-27T04:04:24.362Z
ayat: 44
description: "Jumlah Ayat: 59 / Arti: Kabut"
---
<!--StartFragment-->

44:1

# حٰمۤ ۚ

*haa*-miim

Ha Mim

44:2

# وَالْكِتٰبِ الْمُبِيْنِۙ

wa**a**lkit*aa*bi **a**lmubiin**i**

Demi Kitab (Al-Qur'an) yang jelas,

44:3

# اِنَّآ اَنْزَلْنٰهُ فِيْ لَيْلَةٍ مُّبٰرَكَةٍ اِنَّا كُنَّا مُنْذِرِيْنَ

inn*aa* anzaln*aa*hu fii laylatin mub*aa*rakatin inn*aa* kunn*aa* mun*dz*iriin**a**

sesungguhnya Kami menurunkannya pada malam yang diberkahi. ) Sungguh, Kamilah yang memberi peringatan.

44:4

# فِيْهَا يُفْرَقُ كُلُّ اَمْرٍ حَكِيْمٍۙ

fiih*aa* yufraqu kullu amrin *h*akiim**in**

Pada (malam itu) dijelaskan segala urusan yang penuh hikmah,

44:5

# اَمْرًا مِّنْ عِنْدِنَاۗ اِنَّا كُنَّا مُرْسِلِيْنَۖ

amran min 'indin*aa* inn*aa* kunn*aa* mursiliin**a**

(yaitu) urusan dari sisi Kami. Sungguh, Kamilah yang mengutus rasul-rasul,

44:6

# رَحْمَةً مِّنْ رَّبِّكَ ۗاِنَّهٗ هُوَ السَّمِيْعُ الْعَلِيْمُۗ

ra*h*matan min rabbika innahu huwa **al**ssamii'u **a**l'aliim**u**

sebagai rahmat dari Tuhanmu. Sungguh, Dia Maha Mendengar, Maha Mengetahui,

44:7

# رَبِّ السَّمٰوٰتِ وَالْاَرْضِ وَمَا بَيْنَهُمَاۘ اِنْ كُنْتُمْ مُّوْقِنِيْنَ

rabbi **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i wam*aa* baynahum*aa* in kuntum muuqiniin**a**

Tuhan (yang memelihara) langit dan bumi dan apa yang ada di antara keduanya; jika kamu orang-orang yang meyakini.

44:8

# لَآ اِلٰهَ اِلَّا هُوَ يُحْيٖ وَيُمِيْتُ ۗرَبُّكُمْ وَرَبُّ اٰبَاۤىِٕكُمُ الْاَوَّلِيْنَ

l*aa* il*aa*ha ill*aa* huwa yu*h*yii wayumiitu rabbukum warabbu *aa*b*aa*-ikumu **a**l-awwaliin**a**

Tidak ada tuhan selain Dia, Dia yang menghidupkan dan mematikan. (Dialah) Tuhanmu dan Tuhan nenek moyangmu dahulu.

44:9

# بَلْ هُمْ فِيْ شَكٍّ يَّلْعَبُوْنَ

bal hum fii syakkin yal'abuun**a**

Tetapi mereka dalam keraguan, mereka bermain-main.

44:10

# فَارْتَقِبْ يَوْمَ تَأْتِى السَّمَاۤءُ بِدُخَانٍ مُّبِيْنٍ

fa**i**rtaqib yawma ta/tii **al**ssam*aa*u bidukh*aa*nin mubiin**in**

Maka tunggulah pada hari ketika langit membawa kabut yang tampak jelas,

44:11

# يَغْشَى النَّاسَۗ هٰذَا عَذَابٌ اَلِيْمٌ

yaghsy*aa* **al**nn*aa*sa h*aadzaa* 'a*dzaa*bun **a**liim**un**

yang meliputi manusia. Inilah azab yang pedih.

44:12

# رَبَّنَا اكْشِفْ عَنَّا الْعَذَابَ اِنَّا مُؤْمِنُوْنَ

rabban*aa* iksyif 'ann*aa* **a**l'a*dzaa*ba inn*aa* mu/minuun**a**

(Mereka berdoa), “Ya Tuhan kami, lenyapkanlah azab itu dari kami. Sungguh, kami akan beriman.”

44:13

# اَنّٰى لَهُمُ الذِّكْرٰى وَقَدْ جَاۤءَهُمْ رَسُوْلٌ مُّبِيْنٌۙ

ann*aa* lahumu **al***dzdz*ikr*aa* waqad j*aa*-ahum rasuulun mubiin**un**

Bagaimana mereka dapat menerima peringatan, padahal (sebelumnya pun) seorang Rasul telah datang memberi penjelasan kepada mereka,

44:14

# ثُمَّ تَوَلَّوْا عَنْهُ وَقَالُوْا مُعَلَّمٌ مَّجْنُوْنٌۘ

tsumma tawallaw 'anhu waq*aa*luu mu'allamun majnuun**un**

kemudian mereka berpaling darinya dan berkata, “Dia itu orang yang menerima ajaran (dari orang lain) dan orang gila.”

44:15

# اِنَّا كَاشِفُوا الْعَذَابِ قَلِيْلًا اِنَّكُمْ عَاۤىِٕدُوْنَۘ

inn*aa* k*aa*syifuu **a**l'a*dzaa*bi qaliilan innakum '*aa*-iduun**a**

Sungguh (kalau) Kami melenyapkan azab itu sedikit saja, tentu kamu akan kembali (ingkar).

44:16

# يَوْمَ نَبْطِشُ الْبَطْشَةَ الْكُبْرٰىۚ اِنَّا مُنْتَقِمُوْنَ

yawma nab*th*isyu **a**lba*th*syata **a**lkubr*aa* inn*aa* muntaqimuun**a**

(Ingatlah) pada hari (ketika) Kami menghantam mereka dengan keras. Kami pasti memberi balasan.

44:17

# ۞ وَلَقَدْ فَتَنَّا قَبْلَهُمْ قَوْمَ فِرْعَوْنَ وَجَاۤءَهُمْ رَسُوْلٌ كَرِيْمٌۙ

walaqad fatann*aa* qablahum qawma fir'awna waj*aa*-ahum rasuulun kariim**un**

Dan sungguh, sebelum mereka Kami benar-benar telah menguji kaum Fir’aun dan telah datang kepada mereka seorang Rasul yang mulia,

44:18

# اَنْ اَدُّوْٓا اِلَيَّ عِبَادَ اللّٰهِ ۗاِنِّيْ لَكُمْ رَسُوْلٌ اَمِيْنٌۙ

an adduu ilayya 'ib*aa*da **al**l*aa*hi innii lakum rasuulun amiin**un**

(dengan berkata), “Serahkanlah kepadaku hamba-hamba Allah (Bani Israil). Sesungguhnya aku adalah utusan (Allah) yang dapat kamu percaya,

44:19

# وَّاَنْ لَّا تَعْلُوْا عَلَى اللّٰهِ ۚاِنِّيْٓ اٰتِيْكُمْ بِسُلْطٰنٍ مُّبِيْنٍۚ

wa-an l*aa* ta'luu 'al*aa* **al**l*aa*hi innii *aa*tiikum bisul*thaa*nin mubiin**in**

dan janganlah kamu menyombongkan diri terhadap Allah. Sungguh, aku datang kepadamu dengan membawa bukti yang nyata.

44:20

# وَاِنِّيْ عُذْتُ بِرَبِّيْ وَرَبِّكُمْ اَنْ تَرْجُمُوْنِۚ

wa-innii 'u*dz*tu birabbii warabbikum an tarjumuun**i**

Dan sesungguhnya aku berlindung kepada Tuhanku dan Tuhanmu, dari ancamanmu untuk merajamku,

44:21

# وَاِنْ لَّمْ تُؤْمِنُوْا لِيْ فَاعْتَزِلُوْنِ

wa-in lam tu/minuu lii fa**i**'taziluun**i**

dan jika kamu tidak beriman kepadaku maka biarkanlah aku (memimpin Bani Israil).”

44:22

# فَدَعَا رَبَّهٗٓ اَنَّ هٰٓؤُلَاۤءِ قَوْمٌ مُّجْرِمُوْنَ

fada'*aa* rabbahu anna h*aa*ul*aa*-i qawmun mujrimuun**a**

Kemudian dia (Musa) berdoa kepada Tuhannya, “Sungguh, mereka ini adalah kaum yang berdosa (segerakanlah azab kepada mereka).”

44:23

# فَاَسْرِ بِعِبَادِيْ لَيْلًا اِنَّكُمْ مُّتَّبَعُوْنَۙ

fa-asri bi'ib*aa*dii laylan innakum muttaba'uun**a**

(Allah berfirman), “Karena itu berjalanlah dengan hamba-hamba-Ku pada malam hari, sesungguhnya kamu akan dikejar,

44:24

# وَاتْرُكِ الْبَحْرَ رَهْوًاۗ اِنَّهُمْ جُنْدٌ مُّغْرَقُوْنَ

wa**u**truki **a**lba*h*ra rahwan innahum jundun mughraquun**a**

dan biarkanlah laut itu terbelah. Sesungguhnya mereka, bala tentara yang akan ditenggelamkan.”

44:25

# كَمْ تَرَكُوْا مِنْ جَنّٰتٍ وَّعُيُوْنٍۙ

kam tarakuu min jann*aa*tin wa'uyuun**in**

Betapa banyak taman-taman dan mata air-mata air yang mereka tinggalkan,

44:26

# وَّزُرُوْعٍ وَّمَقَامٍ كَرِيْمٍۙ

wazuruu'in wamaq*aa*min kariim**in**

juga kebun-kebun serta tempat-tempat kediaman yang indah,

44:27

# وَّنَعْمَةٍ كَانُوْا فِيْهَا فٰكِهِيْنَۙ

wana'matin k*aa*nuu fiih*aa* f*aa*kihiin**a**

dan kesenangan-kesenangan yang dapat mereka nikmati di sana,

44:28

# كَذٰلِكَ ۗوَاَوْرَثْنٰهَا قَوْمًا اٰخَرِيْنَۚ

ka*dzaa*lika wa-awratsn*aa*h*aa* qawman *aa*khariin**a**

demikianlah, dan Kami wariskan (semua) itu kepada kaum yang lain.

44:29

# فَمَا بَكَتْ عَلَيْهِمُ السَّمَاۤءُ وَالْاَرْضُۗ وَمَا كَانُوْا مُنْظَرِيْنَ ࣖ

fam*aa* bakat 'alayhimu **al**ssam*aa*u wa**a**l-ar*dh*u wam*aa* k*aa*nuu mun*zh*ariin**a**

Maka langit dan bumi tidak menangisi mereka dan mereka pun tidak diberi penangguhan waktu.

44:30

# وَلَقَدْ نَجَّيْنَا بَنِيْٓ اِسْرَاۤءِيْلَ مِنَ الْعَذَابِ الْمُهِيْنِۙ

walaqad najjayn*aa* banii isr*aa*-iila mina **a**l'a*dzaa*bi **a**lmuhiin**i**

Dan sungguh, telah Kami selamatkan Bani Israil dari siksaan yang menghinakan,

44:31

# مِنْ فِرْعَوْنَ ۗاِنَّهٗ كَانَ عَالِيًا مِّنَ الْمُسْرِفِيْنَ

min fir'awna innahu k*aa*na '*aa*liyan mina **a**lmusrifiin**a**

dari (siksaan) Fir‘aun, sungguh, dia itu orang yang sombong, termasuk orang-orang yang melampaui batas.

44:32

# وَلَقَدِ اخْتَرْنٰهُمْ عَلٰى عِلْمٍ عَلَى الْعٰلَمِيْنَ ۚ

walaqadi ikhtarn*aa*hum 'al*aa* 'ilmin 'al*aa* **a**l'*aa*lamiin**a**

Dan sungguh, Kami pilih mereka (Bani Israil) dengan ilmu (Kami) di atas semua bangsa (pada masa itu).

44:33

# وَاٰتَيْنٰهُمْ مِّنَ الْاٰيٰتِ مَا فِيْهِ بَلٰۤـؤٌا مُّبِيْنٌ

wa*aa*tayn*aa*hum mina **a**l-*aa*y*aa*ti m*aa* fiihi bal*aa*un mubiin**un**

Dan telah Kami berikan kepada mereka di antara tanda-tanda (kebesaran Kami) sesuatu yang di dalamnya terdapat nikmat yang nyata.

44:34

# اِنَّ هٰٓؤُلَاۤءِ لَيَقُوْلُوْنَۙ

inna h*aa*ul*aa*-i layaquuluun**a**

Sesungguhnya mereka (kaum musyrik) itu pasti akan berkata,

44:35

# اِنْ هِيَ اِلَّا مَوْتَتُنَا الْاُوْلٰى وَمَا نَحْنُ بِمُنْشَرِيْنَ

in hiya ill*aa* mawtatun*aa* **a**l-uul*aa* wam*aa* na*h*nu bimunsyariin**a**

”Tidak ada kematian selain kematian di dunia ini. Dan kami tidak akan dibangkitkan,

44:36

# فَأْتُوْا بِاٰبَاۤىِٕنَآ اِنْ كُنْتُمْ صٰدِقِيْنَ

fa/tuu bi-*aa*b*aa*-in*aa* in kuntum *shaa*diqiin**a**

maka hadirkanlah (kembali) nenek moyang kami jika kamu orang yang benar.”

44:37

# اَهُمْ خَيْرٌ اَمْ قَوْمُ تُبَّعٍۙ وَّالَّذِيْنَ مِنْ قَبْلِهِمْۗ اَهْلَكْنٰهُمْ اِنَّهُمْ كَانُوْا مُجْرِمِيْنَ

ahum khayrun am qawmu tubba'in wa**a**lla*dz*iina min qablihim ahlakn*aa*hum innahum k*aa*nuu mujrimiin**a**

Apakah mereka (kaum musyrikin) yang lebih baik atau kaum Tubba‘, dan orang-orang yang sebelum mereka yang telah Kami binasakan karena mereka itu adalah orang-orang yang sungguh berdosa.

44:38

# وَمَا خَلَقْنَا السَّمٰوٰتِ وَالْاَرْضَ وَمَا بَيْنَهُمَا لٰعِبِيْنَ

wam*aa* khalaqn*aa* **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*a wam*aa* baynahum*aa* l*aa*'ibiin**a**

Dan tidaklah Kami bermain-main menciptakan langit dan bumi dan apa yang ada di antara keduanya.

44:39

# مَا خَلَقْنٰهُمَآ اِلَّا بِالْحَقِّ وَلٰكِنَّ اَكْثَرَهُمْ لَا يَعْلَمُوْنَ

m*aa* khalaqn*aa*hum*aa* ill*aa* bi**a**l*h*aqqi wal*aa*kinna aktsarahum l*aa* ya'lamuun**a**

Tidaklah Kami ciptakan keduanya melainkan dengan haq (benar), tetapi kebanyakan mereka tidak mengetahui.

44:40

# اِنَّ يَوْمَ الْفَصْلِ مِيْقَاتُهُمْ اَجْمَعِيْنَ ۙ

inna yawma **a**lfa*sh*li miiq*aa*tuhum ajma'iin**a**

Sungguh, pada hari keputusan (hari Kiamat) itu adalah waktu yang dijanjikan bagi mereka semuanya,

44:41

# يَوْمَ لَا يُغْنِيْ مَوْلًى عَنْ مَّوْلًى شَيْـًٔا وَّلَا هُمْ يُنْصَرُوْنَۙ

yawma l*aa* yughnii mawlan 'an mawlan syay-an wal*aa* hum yun*sh*aruun**a**

(yaitu) pada hari (ketika) seorang teman sama sekali tidak dapat memberi manfaat kepada teman lainnya dan mereka tidak akan mendapat pertolongan,

44:42

# اِلَّا مَنْ رَّحِمَ اللّٰهُ ۗاِنَّهٗ هُوَ الْعَزِيْزُ الرَّحِيْمُ ࣖ

ill*aa* man ra*h*ima **al**l*aa*hu innahu huwa **a**l'aziizu **al**rra*h*iim**u**

Kecuali orang yang diberi rahmat oleh Allah. Sungguh, Dia Mahaperkasa, Maha Penyayang.

44:43

# اِنَّ شَجَرَتَ الزَّقُّوْمِۙ

inna syajarata **al**zzaqquum**i**

Sungguh pohon zaqqum itu,

44:44

# طَعَامُ الْاَثِيْمِ ۛ

*th*a'*aa*mu **a**l-atsiim**i**

makanan bagi orang yang banyak dosa.

44:45

# كَالْمُهْلِ ۛ يَغْلِيْ فِى الْبُطُوْنِۙ

ka**a**lmuhli yaghlii fii **a**lbu*th*uun**i**

Seperti cairan tembaga yang mendidih di dalam perut,

44:46

# كَغَلْيِ الْحَمِيْمِ ۗ

kaghalyi **a**l*h*amiim**i**

seperti mendidihnya air yang sangat panas.

44:47

# خُذُوْهُ فَاعْتِلُوْهُ اِلٰى سَوَاۤءِ الْجَحِيْمِۙ

khu*dz*uuhu fa**i**'tiluuhu il*aa* saw*aa*-i **a**lja*h*iim**i**

”Peganglah dia kemudian seretlah dia sampai ke tengah-tengah neraka,

44:48

# ثُمَّ صُبُّوْا فَوْقَ رَأْسِهٖ مِنْ عَذَابِ الْحَمِيْمِۗ

tsumma *sh*ubbuu fawqa ra/sihi min 'a*dzaa*bi **a**l*h*amiim**i**

kemudian tuangkanlah di atas kepalanya azab (dari) air yang sangat panas.”

44:49

# ذُقْۚ اِنَّكَ اَنْتَ الْعَزِيْزُ الْكَرِيْمُ

*dz*uq innaka anta **a**l'aziizu **a**lkariim**u**

”Rasakanlah, sesungguhnya kamu benar-benar orang yang perkasa lagi mulia.”

44:50

# اِنَّ هٰذَا مَا كُنْتُمْ بِهٖ تَمْتَرُوْنَ

inna h*aadzaa* m*aa* kuntum bihi tamtaruun**a**

Sungguh, inilah azab yang dahulu kamu ragukan.

44:51

# اِنَّ الْمُتَّقِيْنَ فِيْ مَقَامٍ اَمِيْنٍۙ

inna **a**lmuttaqiina fii maq*aa*min amiin**in**

Sungguh, orang-orang yang bertakwa berada dalam tempat yang aman,

44:52

# فِيْ جَنّٰتٍ وَّعُيُوْنٍ ۙ

fii jann*aa*tin wa'uyuun**in**

(yaitu) di dalam taman-taman dan mata air-mata air,

44:53

# يَّلْبَسُوْنَ مِنْ سُنْدُسٍ وَّاِسْتَبْرَقٍ مُّتَقٰبِلِيْنَۚ

yalbasuuna min sundusin wa-istabraqin mutaq*aa*biliin**a**

mereka memakai sutra yang halus dan sutra yang tebal, (duduk) berhadapan,

44:54

# كَذٰلِكَۗ وَزَوَّجْنٰهُمْ بِحُوْرٍ عِيْنٍۗ

ka*dzaa*lika wazawwajn*aa*hum bi*h*uurin 'iin**in**

demikianlah, kemudian Kami berikan kepada mereka pasangan bidadari yang bermata indah.

44:55

# يَدْعُوْنَ فِيْهَا بِكُلِّ فَاكِهَةٍ اٰمِنِيْنَۙ

yad'uuna fiih*aa* bikulli f*aa*kihatin *aa*miniin**a**

Di dalamnya mereka dapat meminta segala macam buah-buahan dengan aman dan tenteram,

44:56

# لَا يَذُوْقُوْنَ فِيْهَا الْمَوْتَ اِلَّا الْمَوْتَةَ الْاُوْلٰىۚ وَوَقٰىهُمْ عَذَابَ الْجَحِيْمِۙ

l*aa* ya*dz*uuquuna fiih*aa* **a**lmawta ill*aa* **a**lmawtata **a**l-uul*aa* wawaq*aa*hum 'a*dzaa*ba **a**lja*h*iim**i**

mereka tidak akan merasakan mati di dalamnya selain kematian pertama (di dunia). Allah melindungi mereka dari azab neraka,

44:57

# فَضْلًا مِّنْ رَّبِّكَۚ ذٰلِكَ هُوَ الْفَوْزُ الْعَظِيْمُ

fa*dh*lan min rabbika *dzaa*lika huwa **a**lfawzu **a**l'a*zh*iim**u**

itu merupakan karunia dari Tuhanmu. Demikian itulah kemenangan yang agung.

44:58

# فَاِنَّمَا يَسَّرْنٰهُ بِلِسَانِكَ لَعَلَّهُمْ يَتَذَكَّرُوْنَ

fa-innam*aa* yassarn*aa*hu bilis*aa*nika la'allahum yata*dz*akkaruun**a**

Sungguh, Kami mudahkan Al-Qur'an itu dengan bahasamu agar mereka mendapat pelajaran.

44:59

# فَارْتَقِبْ اِنَّهُمْ مُّرْتَقِبُوْنَ ࣖࣖ

fa-innam*aa* yassarn*aa*hu bilis*aa*nika la'allahum yata*dz*akkaruun**a**

Maka tunggulah; sungguh, mereka itu (juga sedang) menunggu.

<!--EndFragment-->