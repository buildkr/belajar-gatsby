---
title: (5) Al-Ma'idah - الماۤئدة
date: 2021-10-27T03:14:46.722Z
ayat: 5
description: "Jumlah Ayat: 120 / Arti: Hidangan"
---
<!--StartFragment-->

5:1

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْٓا اَوْفُوْا بِالْعُقُوْدِۗ اُحِلَّتْ لَكُمْ بَهِيْمَةُ الْاَنْعَامِ اِلَّا مَا يُتْلٰى عَلَيْكُمْ غَيْرَ مُحِلِّى الصَّيْدِ وَاَنْتُمْ حُرُمٌۗ اِنَّ اللّٰهَ يَحْكُمُ مَا يُرِيْدُ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu awfuu bi**a**l'uquudi u*h*illat lakum bahiimatu **a**l-an'*aa*mi ill*aa* m*aa* yutl*aa* 'alaykum ghayra mu*h*illii

Wahai orang-orang yang beriman! Penuhilah janji-janji. Hewan ternak dihalalkan bagimu, kecuali yang akan disebutkan kepadamu, dengan tidak menghalalkan berburu ketika kamu sedang berihram (haji atau umrah). Sesungguhnya Allah menetapkan hukum sesuai denga







5:2

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْا لَا تُحِلُّوْا شَعَاۤىِٕرَ اللّٰهِ وَلَا الشَّهْرَ الْحَرَامَ وَلَا الْهَدْيَ وَلَا الْقَلَاۤىِٕدَ وَلَآ اٰۤمِّيْنَ الْبَيْتَ الْحَرَامَ يَبْتَغُوْنَ فَضْلًا مِّنْ رَّبِّهِمْ وَرِضْوَانًا ۗوَاِذَا حَلَلْتُمْ فَاصْطَادُوْا

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu l*aa* tu*h*illuu sya'*aa*-ira **al**l*aa*hi wal*aa* **al**sysyahra **a**l*h*ar*aa*ma wal*aa*

Wahai orang-orang yang beriman! Janganlah kamu melanggar syiar-syiar kesucian Allah, dan jangan (melanggar kehormatan) bulan-bulan haram, jangan (mengganggu) hadyu (hewan-hewan kurban) dan qala'id (hewan-hewan kurban yang diberi tanda), dan jangan (pula)

5:3

# حُرِّمَتْ عَلَيْكُمُ الْمَيْتَةُ وَالدَّمُ وَلَحْمُ الْخِنْزِيْرِ وَمَآ اُهِلَّ لِغَيْرِ اللّٰهِ بِهٖ وَالْمُنْخَنِقَةُ وَالْمَوْقُوْذَةُ وَالْمُتَرَدِّيَةُ وَالنَّطِيْحَةُ وَمَآ اَكَلَ السَّبُعُ اِلَّا مَا ذَكَّيْتُمْۗ وَمَا ذُبِحَ عَلَى النُّصُبِ وَاَ

*h*urrimat 'alaykumu **a**lmaytatu wa**al**ddamu wala*h*mu **a**lkhinziiri wam*aa* uhilla lighayri **al**l*aa*hi bihi wa**a**lmunkhaniqatu wa**a**lmawquu*d*

Diharamkan bagimu (memakan) bangkai, darah, daging babi, dan (daging) hewan yang disembelih bukan atas (nama) Allah, yang tercekik, yang dipukul, yang jatuh, yang ditanduk, dan yang diterkam binatang buas, kecuali yang sempat kamu sembelih. Dan (diharamka







5:4

# يَسْـَٔلُوْنَكَ مَاذَآ اُحِلَّ لَهُمْۗ قُلْ اُحِلَّ لَكُمُ الطَّيِّبٰتُۙ وَمَا عَلَّمْتُمْ مِّنَ الْجَوَارِحِ مُكَلِّبِيْنَ تُعَلِّمُوْنَهُنَّ مِمَّا عَلَّمَكُمُ اللّٰهُ فَكُلُوْا مِمَّآ اَمْسَكْنَ عَلَيْكُمْ وَاذْكُرُوا اسْمَ اللّٰهِ عَلَيْهِ ۖوَاتَّ

yas-aluunaka m*aatsaa* u*h*illa lahum qul u*h*illa lakumu **al***ththh*ayyib*aa*tu wam*aa* 'allamtum mina **a**ljaw*aa*ri*h*i mukallibiina tu'allimuunahunna mimm*aa* 'allamakumu

Mereka bertanya kepadamu (Muhammad), “Apakah yang dihalalkan bagi mereka?” Katakanlah, ”Yang dihalalkan bagimu (adalah makanan) yang baik-baik dan (buruan yang ditangkap) oleh binatang pemburu yang telah kamu latih untuk berburu, yang kamu latih menurut a

5:5

# اَلْيَوْمَ اُحِلَّ لَكُمُ الطَّيِّبٰتُۗ وَطَعَامُ الَّذِيْنَ اُوْتُوا الْكِتٰبَ حِلٌّ لَّكُمْ ۖوَطَعَامُكُمْ حِلٌّ لَّهُمْ ۖوَالْمُحْصَنٰتُ مِنَ الْمُؤْمِنٰتِ وَالْمُحْصَنٰتُ مِنَ الَّذِيْنَ اُوْتُوا الْكِتٰبَ مِنْ قَبْلِكُمْ اِذَآ اٰتَيْتُمُوْهُنَّ اُجُ

alyawma u*h*illa lakumu **al***ththh*ayyib*aa*tu wa*th*a'*aa*mu **al**la*dz*iina uutuu **a**lkit*aa*ba *h*illun lakum wa*th*a'*aa*mukum *h*illun lahum wa

Pada hari ini dihalalkan bagimu segala yang baik-baik. Makanan (sembelihan) Ahli Kitab itu halal bagimu, dan makananmu halal bagi mereka. Dan (dihalalkan bagimu menikahi) perempuan-perempuan yang menjaga kehormatan di antara perempuan-perempuan yang berim







5:6

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْٓا اِذَا قُمْتُمْ اِلَى الصَّلٰوةِ فَاغْسِلُوْا وُجُوْهَكُمْ وَاَيْدِيَكُمْ اِلَى الْمَرَافِقِ وَامْسَحُوْا بِرُءُوْسِكُمْ وَاَرْجُلَكُمْ اِلَى الْكَعْبَيْنِۗ وَاِنْ كُنْتُمْ جُنُبًا فَاطَّهَّرُوْاۗ وَاِنْ كُنْتُمْ مَّرْضٰٓى

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu i*dzaa* qumtum il*aa* **al***shsh*al*aa*ti fa**i**ghsiluu wujuuhakum wa-aydiyakum il*aa* **a**lmar*aa*fiqi wa<

Wahai orang-orang yang beriman! Apabila kamu hendak melaksanakan salat, maka basuhlah wajahmu dan tanganmu sampai ke siku, dan sapulah kepalamu dan (basuh) kedua kakimu sampai ke kedua mata kaki. Jika kamu junub, maka mandilah. Dan jika kamu sakit atau da

5:7

# وَاذْكُرُوْا نِعْمَةَ اللّٰهِ عَلَيْكُمْ وَمِيْثَاقَهُ الَّذِيْ وَاثَقَكُمْ بِهٖٓ ۙاِذْ قُلْتُمْ سَمِعْنَا وَاَطَعْنَا ۖوَاتَّقُوا اللّٰهَ ۗاِنَّ اللّٰهَ عَلِيْمٌ ۢبِذَاتِ الصُّدُوْرِ

wa**u***dz*kuruu ni'mata **al**l*aa*hi 'alaykum wamiits*aa*qahu **al**la*dz*ii w*aa*tsaqakum bihi i*dz* qultum sami'n*aa* wa-a*th*a'n*aa* wa**i**ttaquu

Dan ingatlah akan karunia Allah kepadamu dan perjanjian-Nya yang telah diikatkan kepadamu, ketika kamu mengatakan, “Kami mendengar dan kami menaati.” Dan bertakwalah kepada Allah, sungguh, Allah Maha Mengetahui segala isi hati.

5:8

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْا كُوْنُوْا قَوَّامِيْنَ لِلّٰهِ شُهَدَاۤءَ بِالْقِسْطِۖ وَلَا يَجْرِمَنَّكُمْ شَنَاٰنُ قَوْمٍ عَلٰٓى اَلَّا تَعْدِلُوْا ۗاِعْدِلُوْاۗ هُوَ اَقْرَبُ لِلتَّقْوٰىۖ وَاتَّقُوا اللّٰهَ ۗاِنَّ اللّٰهَ خَبِيْرٌۢ بِمَا تَعْمَلُوْنَ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu kuunuu qaww*aa*miina lill*aa*hi syuhad*aa*-a bi**a**lqis*th*i wal*aa* yajrimannakum syana*aa*nu qawmin 'al*aa* **al**l

Wahai orang-orang yang beriman! Jadilah kamu sebagai penegak keadilan karena Allah, (ketika) menjadi saksi dengan adil. Dan janganlah kebencianmu terhadap suatu kaum mendorong kamu untuk berlaku tidak adil. Berlaku adillah. Karena (adil) itu lebih dekat k

5:9

# وَعَدَ اللّٰهُ الَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِۙ لَهُمْ مَّغْفِرَةٌ وَّاَجْرٌ عَظِيْمٌ

wa'ada **al**l*aa*hu **al**la*dz*iina *aa*manuu wa'amiluu **al***shshaa*li*haa*ti lahum maghfiratun wa-ajrun 'a*zh*iim**un**

Allah telah menjanjikan kepada orang-orang yang beriman dan beramal saleh, (bahwa) mereka akan mendapat ampunan dan pahala yang besar.

5:10

# وَالَّذِيْنَ كَفَرُوْا وَكَذَّبُوْا بِاٰيٰتِنَآ اُولٰۤىِٕكَ اَصْحٰبُ الْجَحِيْمِ

wa**a**lla*dz*iina kafaruu waka*dzdz*abuu bi-*aa*y*aa*tin*aa* ul*aa*-ika a*sh*-*haa*bu **a**lja*h*iim**i**

Adapun orang-orang yang kafir dan mendustakan ayat-ayat Kami, mereka itulah penghuni neraka.

5:11

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوا اذْكُرُوْا نِعْمَتَ اللّٰهِ عَلَيْكُمْ اِذْ هَمَّ قَوْمٌ اَنْ يَّبْسُطُوْٓا اِلَيْكُمْ اَيْدِيَهُمْ فَكَفَّ اَيْدِيَهُمْ عَنْكُمْۚ وَاتَّقُوا اللّٰهَ ۗوَعَلَى اللّٰهِ فَلْيَتَوَكَّلِ الْمُؤْمِنُوْنَ ࣖ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu u*dz*kuruu ni'mata **al**l*aa*hi 'alaykum i*dz* hamma qawmun an yabsu*th*uu ilaykum aydiyahum fakaffa aydiyahum 'ankum wa**i**ttaquu

Wahai orang-orang yang beriman! Ingatlah nikmat Allah (yang diberikan) kepadamu, ketika suatu kaum bermaksud hendak menyerangmu dengan tangannya, lalu Allah menahan tangan mereka dari kamu. Dan bertakwalah kepada Allah, dan hanya kepada Allah-lah hendakny

5:12

# ۞ وَلَقَدْ اَخَذَ اللّٰهُ مِيْثَاقَ بَنِيْٓ اِسْرَاۤءِيْلَۚ وَبَعَثْنَا مِنْهُمُ اثْنَيْ عَشَرَ نَقِيْبًاۗ وَقَالَ اللّٰهُ اِنِّيْ مَعَكُمْ ۗ لَىِٕنْ اَقَمْتُمُ الصَّلٰوةَ وَاٰتَيْتُمُ الزَّكٰوةَ وَاٰمَنْتُمْ بِرُسُلِيْ وَعَزَّرْتُمُوْهُمْ وَاَقْرَضْتُمُ

walaqad akha*dz*a **al**l*aa*hu miits*aa*qa banii isr*aa*-iila waba'atsn*aa* minhumu itsnay 'asyara naqiiban waq*aa*la **al**l*aa*hu innii ma'akum la-in aqamtumu **al***shsh*al

Dan sungguh, Allah telah mengambil perjanjian dari Bani Israil dan Kami telah mengangkat dua belas orang pemimpin di antara mereka. Dan Allah berfirman, “Aku bersamamu.” Sungguh, jika kamu melaksanakan salat dan menunaikan zakat serta beriman kepada rasul

5:13

# فَبِمَا نَقْضِهِمْ مِّيْثَاقَهُمْ لَعَنّٰهُمْ وَجَعَلْنَا قُلُوْبَهُمْ قٰسِيَةً ۚ يُحَرِّفُوْنَ الْكَلِمَ عَنْ مَّوَاضِعِهٖۙ وَنَسُوْا حَظًّا مِّمَّا ذُكِّرُوْا بِهٖۚ وَلَا تَزَالُ تَطَّلِعُ عَلٰى خَاۤىِٕنَةٍ مِّنْهُمْ اِلَّا قَلِيْلًا مِّنْهُمْ ۖ فَاعْفُ

fabim*aa* naq*dh*ihim miits*aa*qahum la'ann*aa*hum waja'aln*aa* quluubahum q*aa*siyatan yu*h*arrifuuna **a**lkalima 'an maw*aad*i'ihi wanasuu *h*a*zhzh*an mimm*aa* *dz*ukkiruu bihi w

(Tetapi) karena mereka melanggar janjinya, maka Kami melaknat mereka, dan Kami jadikan hati mereka keras membatu. Mereka suka mengubah firman (Allah) dari tempatnya, dan mereka (sengaja) melupakan sebagian pesan yang telah diperingatkan kepada mereka. Eng

5:14

# وَمِنَ الَّذِيْنَ قَالُوْٓا اِنَّا نَصٰرٰٓى اَخَذْنَا مِيْثَاقَهُمْ فَنَسُوْا حَظًّا مِّمَّا ذُكِّرُوْا بِهٖۖ فَاَغْرَيْنَا بَيْنَهُمُ الْعَدَاوَةَ وَالْبَغْضَاۤءَ اِلٰى يَوْمِ الْقِيٰمَةِ ۗ وَسَوْفَ يُنَبِّئُهُمُ اللّٰهُ بِمَا كَانُوْا يَصْنَعُوْنَ

wamina **al**la*dz*iina q*aa*luu inn*aa* na*shaa*r*aa* akha*dz*n*aa* miits*aa*qahum fanasuu *h*a*zhzh*an mimm*aa* *dz*ukkiruu bihi fa-aghrayn*aa* baynahumu **a**l'a

Dan di antara orang-orang yang mengatakan, “Kami ini orang Nasrani,” Kami telah mengambil perjanjian mereka, tetapi mereka (sengaja) melupakan sebagian pesan yang telah diperingatkan kepada mereka, maka Kami timbulkan permusuhan dan kebencian di antara me

5:15

# يٰٓاَهْلَ الْكِتٰبِ قَدْ جَاۤءَكُمْ رَسُوْلُنَا يُبَيِّنُ لَكُمْ كَثِيْرًا مِّمَّا كُنْتُمْ تُخْفُوْنَ مِنَ الْكِتٰبِ وَيَعْفُوْا عَنْ كَثِيْرٍەۗ قَدْ جَاۤءَكُمْ مِّنَ اللّٰهِ نُوْرٌ وَّكِتٰبٌ مُّبِيْنٌۙ

y*aa* ahla **a**lkit*aa*bi qad j*aa*-akum rasuulun*aa* yubayyinu lakum katsiiran mimm*aa* kuntum tukhfuuna mina **a**lkit*aa*bi waya'fuu 'an katsiirin qad j*aa*-akum mina **al**l

Wahai Ahli Kitab! Sungguh, Rasul Kami telah datang kepadamu, menjelaskan kepadamu banyak hal dari (isi) kitab yang kamu sembunyikan, dan banyak (pula) yang dibiarkannya. Sungguh, telah datang kepadamu cahaya dari Allah, dan Kitab yang menjelaskan.







5:16

# يَّهْدِيْ بِهِ اللّٰهُ مَنِ اتَّبَعَ رِضْوَانَهٗ سُبُلَ السَّلٰمِ وَيُخْرِجُهُمْ مِّنَ الظُّلُمٰتِ اِلَى النُّوْرِ بِاِذْنِهٖ وَيَهْدِيْهِمْ اِلٰى صِرَاطٍ مُّسْتَقِيْمٍ

yahdii bihi **al**l*aa*hu mani ittaba'a ri*dh*w*aa*nahu subula **al**ssal*aa*mi wayukhrijuhum mina **al***zhzh*ulum*aa*ti il*aa* **al**nnuuri bi-i*dz*nihi wayahdii

Dengan Kitab itulah Allah memberi petunjuk kepada orang yang mengikuti keridaan-Nya ke jalan keselamatan, dan (dengan Kitab itu pula) Allah mengeluarkan orang itu dari gelap gulita kepada cahaya dengan izin-Nya, dan menunjukkan ke jalan yang lurus.

5:17

# لَقَدْ كَفَرَ الَّذِيْنَ قَالُوْٓا اِنَّ اللّٰهَ هُوَ الْمَسِيْحُ ابْنُ مَرْيَمَۗ قُلْ فَمَنْ يَّمْلِكُ مِنَ اللّٰهِ شَيْـًٔا اِنْ اَرَادَ اَنْ يُّهْلِكَ الْمَسِيْحَ ابْنَ مَرْيَمَ وَاُمَّهٗ وَمَنْ فِى الْاَرْضِ جَمِيْعًا ۗوَلِلّٰهِ مُلْكُ السَّمٰوٰتِ وَ

laqad kafara **al**la*dz*iina q*aa*luu inna **al**l*aa*ha huwa **a**lmasii*h*u ibnu maryama qul faman yamliku mina **al**l*aa*hi syay-an in ar*aa*da an yuhlika **a**

**Sungguh, telah kafir orang yang berkata, “Sesungguhnya Allah itu dialah Al-Masih putra Maryam.” Katakanlah (Muhammad), “Siapakah yang dapat menghalang-halangi kehendak Allah, jika Dia hendak membinasakan Al-Masih putra Maryam beserta ibunya dan seluruh (m**









5:18

# وَقَالَتِ الْيَهُوْدُ وَالنَّصٰرٰى نَحْنُ اَبْنٰۤؤُ اللّٰهِ وَاَحِبَّاۤؤُهٗ ۗ قُلْ فَلِمَ يُعَذِّبُكُمْ بِذُنُوْبِكُمْ ۗ بَلْ اَنْتُمْ بَشَرٌ مِّمَّنْ خَلَقَۗ يَغْفِرُ لِمَنْ يَّشَاۤءُ وَيُعَذِّبُ مَنْ يَّشَاۤءُۗ وَلِلّٰهِ مُلْكُ السَّمٰوٰتِ وَالْاَرْضِ و

waq*aa*lati **a**lyahuudu wa**al**nna*shaa*r*aa* na*h*nu abn*aa*u **al**l*aa*hi wa-a*h*ibb*aa*uhu qul falima yu'a*dzdz*ibukum bi*dz*unuubikum bal antum basyarun mimma

Orang Yahudi dan Nasrani berkata, “Kami adalah anak-anak Allah dan kekasih-kekasih-Nya.” Katakanlah, “Mengapa Allah menyiksa kamu karena dosa-dosamu? Tidak, kamu adalah manusia (biasa) di antara orang-orang yang Dia ciptakan. Dia mengampuni siapa yang Dia

5:19

# يٰٓاَهْلَ الْكِتٰبِ قَدْ جَاۤءَكُمْ رَسُوْلُنَا يُبَيِّنُ لَكُمْ عَلٰى فَتْرَةٍ مِّنَ الرُّسُلِ اَنْ تَقُوْلُوْا مَا جَاۤءَنَا مِنْۢ بَشِيْرٍ وَّلَا نَذِيْرٍۗ فَقَدْ جَاۤءَكُمْ بَشِيْرٌ وَّنَذِيْرٌ ۗوَاللّٰهُ عَلٰى كُلِّ شَيْءٍ قَدِيْرٌ ࣖ

y*aa* ahla **a**lkit*aa*bi qad j*aa*-akum rasuulun*aa* yubayyinu lakum 'al*aa* fatratin mina **al**rrusuli an taquuluu m*aa* j*aa*-an*aa* min basyiirin wal*aa* na*dz*iirin faqad j

Wahai Ahli Kitab! Sungguh, Rasul Kami telah datang kepadamu, menjelaskan (syariat Kami) kepadamu ketika terputus (pengiriman) rasul-rasul, agar kamu tidak mengatakan, “Tidak ada yang datang kepada kami baik seorang pembawa berita gembira maupun seorang pe

5:20

# وَاِذْ قَالَ مُوْسٰى لِقَوْمِهٖ يٰقَوْمِ اذْكُرُوْا نِعْمَةَ اللّٰهِ عَلَيْكُمْ اِذْ جَعَلَ فِيْكُمْ اَنْۢبِيَاۤءَ وَجَعَلَكُمْ مُّلُوْكًاۙ وَّاٰتٰىكُمْ مَّا لَمْ يُؤْتِ اَحَدًا مِّنَ الْعٰلَمِيْنَ

wa-i*dz* q*aa*la muus*aa* liqawmihi y*aa* qawmi u*dz*kuruu ni'mata **al**l*aa*hi 'alaykum i*dz* ja'ala fiikum anbiy*aa*-a waja'alakum muluukan wa*aa*t*aa*kum m*aa* lam yu/ti a*h*adan

Dan (ingatlah) ketika Musa berkata kepada kaumnya, “Wahai kaumku! Ingatlah akan nikmat Allah kepadamu ketika Dia mengangkat nabi-nabi di antaramu, dan menjadikan kamu sebagai orang-orang merdeka, dan memberikan kepada kamu apa yang belum pernah diberikan

5:21

# يٰقَوْمِ ادْخُلُوا الْاَرْضَ الْمُقَدَّسَةَ الَّتِيْ كَتَبَ اللّٰهُ لَكُمْ وَلَا تَرْتَدُّوْا عَلٰٓى اَدْبَارِكُمْ فَتَنْقَلِبُوْا خٰسِرِيْنَ

y*aa* qawmi udkhuluu **a**l-ar*dh*a **a**lmuqaddasata **al**latii kataba **al**l*aa*hu lakum wal*aa* tartadduu 'al*aa* adb*aa*rikum fatanqalibuu kh*aa*siriin**a**

**Wahai kaumku! Masuklah ke tanah suci (Palestina) yang telah ditentukan Allah bagimu, dan janganlah kamu berbalik ke belakang (karena takut kepada musuh), nanti kamu menjadi orang yang rugi.**









5:22

# قَالُوْا يٰمُوْسٰٓى اِنَّ فِيْهَا قَوْمًا جَبَّارِيْنَۖ وَاِنَّا لَنْ نَّدْخُلَهَا حَتّٰى يَخْرُجُوْا مِنْهَاۚ فَاِنْ يَّخْرُجُوْا مِنْهَا فَاِنَّا دٰخِلُوْنَ

q*aa*luu y*aa* muus*aa* inna fiih*aa* qawman jabb*aa*riina wa-inn*aa* lan nadkhulah*aa* *h*att*aa* yakhrujuu minh*aa* fa-in yakhrujuu minh*aa* fa-inn*aa* d*aa*khiluun**a**

Mereka berkata, “Wahai Musa! Sesungguhnya di dalam negeri itu ada orang-orang yang sangat kuat dan kejam, kami tidak akan memasukinya sebelum mereka keluar darinya. Jika mereka keluar dari sana, niscaya kami akan masuk.”

5:23

# قَالَ رَجُلَانِ مِنَ الَّذِيْنَ يَخَافُوْنَ اَنْعَمَ اللّٰهُ عَلَيْهِمَا ادْخُلُوْا عَلَيْهِمُ الْبَابَۚ فَاِذَا دَخَلْتُمُوْهُ فَاِنَّكُمْ غٰلِبُوْنَ ەۙ وَعَلَى اللّٰهِ فَتَوَكَّلُوْٓا اِنْ كُنْتُمْ مُّؤْمِنِيْنَ

q*aa*la rajul*aa*ni mina **al**la*dz*iina yakh*aa*fuuna an'ama **al**l*aa*hu 'alayhim*aa* udkhuluu 'alayhimu **a**lb*aa*ba fa-i*dzaa* dakhaltumuuhu fa-innakum gh*aa*libuun

Berkatalah dua orang laki-laki di antara mereka yang bertakwa, yang telah diberi nikmat oleh Allah, “Serbulah mereka melalui pintu gerbang (negeri) itu. Jika kamu memasukinya niscaya kamu akan menang. Dan bertawakallah kamu hanya kepada Allah, jika kamu o

5:24

# قَالُوْا يٰمُوْسٰٓى اِنَّا لَنْ نَّدْخُلَهَآ اَبَدًا مَّا دَامُوْا فِيْهَا ۖفَاذْهَبْ اَنْتَ وَرَبُّكَ فَقَاتِلَآ اِنَّا هٰهُنَا قٰعِدُوْنَ

q*aa*luu y*aa* muus*aa* inn*aa* lan nadkhulah*aa* abadan m*aa* d*aa*muu fiih*aa* fa-i*dz*hab anta warabbuka faq*aa*til*aa* inn*aa* h*aa*hun*aa* q*aa*'iduun**a**

Mereka berkata, “Wahai Musa! Sampai kapan pun kami tidak akan memasukinya selama mereka masih ada di dalamnya, karena itu pergilah engkau bersama Tuhanmu, dan berperanglah kamu berdua. Biarlah kami tetap (menanti) di sini saja.”

5:25

# قَالَ رَبِّ اِنِّيْ لَآ اَمْلِكُ اِلَّا نَفْسِيْ وَاَخِيْ فَافْرُقْ بَيْنَنَا وَبَيْنَ الْقَوْمِ الْفٰسِقِيْنَ

q*aa*la rabbi innii l*aa* amliku ill*aa* nafsii wa-akhii fa**u**fruq baynan*aa* wabayna **a**lqawmi **a**lf*aa*siqiin**a**

Dia (Musa) berkata, “Ya Tuhanku, aku hanya menguasai diriku sendiri dan saudaraku. Sebab itu pisahkanlah antara kami dengan orang-orang yang fasik itu.”

5:26

# قَالَ فَاِنَّهَا مُحَرَّمَةٌ عَلَيْهِمْ اَرْبَعِيْنَ سَنَةً ۚيَتِيْهُوْنَ فِى الْاَرْضِۗ فَلَا تَأْسَ عَلَى الْقَوْمِ الْفٰسِقِيْنَ ࣖ

q*aa*la fa-innah*aa* mu*h*arramatun 'alayhim arba'iina sanatan yatiihuuna fii **a**l-ar*dh*i fal*aa* ta/sa 'al*aa* **a**lqawmi **a**lf*aa*siqiin**a**

(Allah) berfirman, “(Jika demikian), maka (negeri) itu terlarang buat mereka selama empat puluh tahun, (selama itu) mereka akan mengembara kebingungan di bumi. Maka janganlah eng-kau (Musa) bersedih hati (memikirkan nasib) orang-orang yang fasik itu.”

5:27

# ۞ وَاتْلُ عَلَيْهِمْ نَبَاَ ابْنَيْ اٰدَمَ بِالْحَقِّۘ اِذْ قَرَّبَا قُرْبَانًا فَتُقُبِّلَ مِنْ اَحَدِهِمَا وَلَمْ يُتَقَبَّلْ مِنَ الْاٰخَرِۗ قَالَ لَاَقْتُلَنَّكَ ۗ قَالَ اِنَّمَا يَتَقَبَّلُ اللّٰهُ مِنَ الْمُتَّقِيْنَ

wa**u**tlu 'alayhim naba-a ibnay *aa*dama bi**a**l*h*aqqi i*dz* qarrab*aa* qurb*aa*nan fatuqubbila min a*h*adihim*aa* walam yutaqabbal mina **a**l-*aa*khari q*aa*la la-aqt

Dan ceritakanlah (Muhammad) yang sebenarnya kepada mereka tentang kisah kedua putra Adam, ketika keduanya mempersembahkan kurban, maka (kurban) salah seorang dari mereka berdua (Habil) diterima dan dari yang lain (Qabil) tidak diterima. Dia (Qabil) berkat

5:28

# لَىِٕنْۢ بَسَطْتَّ اِلَيَّ يَدَكَ لِتَقْتُلَنِيْ مَآ اَنَا۠ بِبَاسِطٍ يَّدِيَ اِلَيْكَ لِاَقْتُلَكَۚ اِنِّيْٓ اَخَافُ اللّٰهَ رَبَّ الْعٰلَمِيْنَ

la-in basa*th*ta ilayya yadaka litaqtulanii m*aa* an*aa* bib*aa*si*th*in yadiya ilayka li-aqtulaka innii akh*aa*fu **al**l*aa*ha rabba **a**l'*aa*lamiin**a**

”Sungguh, jika engkau (Qabil) menggerakkan tanganmu kepadaku untuk membunuhku, aku tidak akan menggerakkan tanganku kepadamu untuk membunuhmu. Aku takut kepada Allah, Tuhan seluruh alam.”

5:29

# اِنِّيْٓ اُرِيْدُ اَنْ تَبُوْۤاَ بِاِثْمِيْ وَاِثْمِكَ فَتَكُوْنَ مِنْ اَصْحٰبِ النَّارِۚ وَذٰلِكَ جَزَاۤؤُا الظّٰلِمِيْنَۚ

innii uriidu an tabuu-a bi-itsmii wa-itsmika fatakuuna min a*sh*-*haa*bi **al**nn*aa*ri wa*dzaa*lika jaz*aa*u **al***zhzhaa*limiin**a**

”Sesungguhnya aku ingin agar engkau kembali dengan (membawa) dosa (membunuh)ku dan dosamu sendiri, maka engkau akan menjadi penghuni neraka; dan itulah balasan bagi orang yang zalim.”

5:30

# فَطَوَّعَتْ لَهٗ نَفْسُهٗ قَتْلَ اَخِيْهِ فَقَتَلَهٗ فَاَصْبَحَ مِنَ الْخٰسِرِيْنَ

fa*th*awwa'at lahu nafsuhu qatla akhiihi faqatalahu fa-a*sh*ba*h*a mina **a**lkh*aa*siriin**a**

Maka nafsu (Qabil) mendorongnya untuk membunuh saudaranya, kemudian dia pun (benar-benar) membunuhnya, maka jadilah dia termasuk orang yang rugi.

5:31

# فَبَعَثَ اللّٰهُ غُرَابًا يَّبْحَثُ فِى الْاَرْضِ لِيُرِيَهٗ كَيْفَ يُوَارِيْ سَوْءَةَ اَخِيْهِ ۗ قَالَ يٰوَيْلَتٰٓى اَعَجَزْتُ اَنْ اَكُوْنَ مِثْلَ هٰذَا الْغُرَابِ فَاُوَارِيَ سَوْءَةَ اَخِيْۚ فَاَصْبَحَ مِنَ النّٰدِمِيْنَ ۛ

faba'atsa **al**l*aa*hu ghur*aa*ban yab*h*atsu fii **a**l-ar*dh*i liyuriyahu kayfa yuw*aa*rii saw-ata akhiihi q*aa*la y*aa* waylat*aa* a'ajaztu an akuuna mitsla h*aadzaa* **a**

**Kemudian Allah mengutus seekor burung gagak menggali tanah untuk diperlihatkan kepadanya (Qabil). Bagaimana dia seharusnya menguburkan mayat saudaranya. Qabil berkata, “Oh, celaka aku! Mengapa aku tidak mampu berbuat seperti burung gagak ini, sehingga aku**









5:32

# مِنْ اَجْلِ ذٰلِكَ ۛ كَتَبْنَا عَلٰى بَنِيْٓ اِسْرَاۤءِيْلَ اَنَّهٗ مَنْ قَتَلَ نَفْسًاۢ بِغَيْرِ نَفْسٍ اَوْ فَسَادٍ فِى الْاَرْضِ فَكَاَنَّمَا قَتَلَ النَّاسَ جَمِيْعًاۗ وَمَنْ اَحْيَاهَا فَكَاَنَّمَآ اَحْيَا النَّاسَ جَمِيْعًا ۗوَلَقَدْ جَاۤءَتْهُمْ ر

min ajli *dzaa*lika katabn*aa* 'al*aa* banii isr*aa*-iila annahu man qatala nafsan bighayri nafsin aw fas*aa*din fii **a**l-ar*dh*i faka-annam*aa* qatala **al**nn*aa*sa jamii'an waman a*h*

Oleh karena itu Kami tetapkan (suatu hukum) bagi Bani Israil, bahwa barangsiapa membunuh seseorang, bukan karena orang itu membunuh orang lain, atau bukan karena berbuat kerusakan di bumi, maka seakan-akan dia telah membunuh semua manusia. Barangsiapa mem







5:33

# اِنَّمَا جَزٰۤؤُا الَّذِيْنَ يُحَارِبُوْنَ اللّٰهَ وَرَسُوْلَهٗ وَيَسْعَوْنَ فِى الْاَرْضِ فَسَادًا اَنْ يُّقَتَّلُوْٓا اَوْ يُصَلَّبُوْٓا اَوْ تُقَطَّعَ اَيْدِيْهِمْ وَاَرْجُلُهُمْ مِّنْ خِلَافٍ اَوْ يُنْفَوْا مِنَ الْاَرْضِۗ ذٰلِكَ لَهُمْ خِزْيٌ فِى الد

innam*aa* jaz*aa*u **al**la*dz*iina yu*haa*ribuuna **al**l*aa*ha warasuulahu wayas'awna fii **a**l-ar*dh*i fas*aa*dan an yuqattaluu aw yu*sh*allabuu aw tuqa*ththh*a'a aydi

Hukuman bagi orang-orang yang memerangi Allah dan Rasul-Nya dan membuat kerusakan di bumi hanyalah dibunuh atau disalib, atau dipotong tangan dan kaki mereka secara silang, atau diasingkan dari tempat kediamannya. Yang demikian itu kehinaan bagi mereka di

5:34

# اِلَّا الَّذِيْنَ تَابُوْا مِنْ قَبْلِ اَنْ تَقْدِرُوْا عَلَيْهِمْۚ فَاعْلَمُوْٓا اَنَّ اللّٰهَ غَفُوْرٌ رَّحِيْمٌ ࣖ

ill*aa* **al**la*dz*iina t*aa*buu min qabli an taqdiruu 'alayhim fa**i**'lamuu anna **al**l*aa*ha ghafuurun ra*h*iim**un**

Kecuali orang-orang yang bertobat sebelum kamu dapat menguasai mereka; maka ketahuilah, bahwa Allah Maha Pengampun, Maha Penyayang.

5:35

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوا اتَّقُوا اللّٰهَ وَابْتَغُوْٓا اِلَيْهِ الْوَسِيْلَةَ وَجَاهِدُوْا فِيْ سَبِيْلِهٖ لَعَلَّكُمْ تُفْلِحُوْنَ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu ittaquu **al**l*aa*ha wa**i**btaghuu ilayhi **a**lwasiilata waj*aa*hiduu fii sabiilihi la'allakum tufli*h*uun**a**

**Wahai orang-orang yang beriman! Bertakwalah kepada Allah dan carilah wasilah (jalan) untuk mendekatkan diri kepada-Nya, dan berjihadlah (berjuanglah) di jalan-Nya, agar kamu beruntung.**









5:36

# اِنَّ الَّذِيْنَ كَفَرُوْا لَوْ اَنَّ لَهُمْ مَّا فِى الْاَرْضِ جَمِيْعًا وَّمِثْلَهٗ مَعَهٗ لِيَفْتَدُوْا بِهٖ مِنْ عَذَابِ يَوْمِ الْقِيٰمَةِ مَا تُقُبِّلَ مِنْهُمْ ۚ وَلَهُمْ عَذَابٌ اَلِيْمٌ

inna **al**la*dz*iina kafaruu law anna lahum m*aa* fii **a**l-ar*dh*i jamii'an wamitslahu ma'ahu liyaftaduu bihi min 'a*dzaa*bi yawmi **a**lqiy*aa*mati m*aa* tuqubbila minhum walahum 'a<

Sesungguhnya orang-orang yang kafir, seandainya mereka memiliki segala apa yang ada di bumi dan ditambah dengan sebanyak itu (lagi) untuk menebus diri mereka dari azab pada hari Kiamat, niscaya semua (tebusan) itu tidak akan diterima dari mereka. Mereka (

5:37

# يُرِيْدُوْنَ اَنْ يَّخْرُجُوْا مِنَ النَّارِ وَمَا هُمْ بِخَارِجِيْنَ مِنْهَا ۖوَلَهُمْ عَذَابٌ مُّقِيمٌ

yuriiduuna an yakhrujuu mina **al**nn*aa*ri wam*aa* hum bikh*aa*rijiina minh*aa* walahum 'a*dzaa*bun muqiim**un**

Mereka ingin keluar dari neraka, tetapi tidak akan dapat keluar dari sana. Dan mereka mendapat azab yang kekal.

5:38

# وَالسَّارِقُ وَالسَّارِقَةُ فَاقْطَعُوْٓا اَيْدِيَهُمَا جَزَاۤءًۢ بِمَا كَسَبَا نَكَالًا مِّنَ اللّٰهِ ۗوَاللّٰهُ عَزِيْزٌ حَكِيْمٌ

wa**al**ss*aa*riqu wa**al**ss*aa*riqatu fa**i**q*th*a'uu aydiyahum*aa* jaz*aa*-an bim*aa* kasab*aa* nak*aa*lan mina **al**l*aa*hi wa**al**l*aa<*

Adapun orang laki-laki maupun perempuan yang mencuri, potonglah tangan keduanya (sebagai) balasan atas perbuatan yang mereka lakukan dan sebagai siksaan dari Allah. Dan Allah Mahaperkasa, Mahabijaksana.







5:39

# فَمَنْ تَابَ مِنْۢ بَعْدِ ظُلْمِهٖ وَاَصْلَحَ فَاِنَّ اللّٰهَ يَتُوْبُ عَلَيْهِ ۗاِنَّ اللّٰهَ غَفُوْرٌ رَّحِيْمٌ

faman t*aa*ba min ba'di *zh*ulmihi wa-a*sh*la*h*a fa-inna **al**l*aa*ha yatuubu 'alayhi inna **al**l*aa*ha ghafuurun ra*h*iim**un**

Tetapi barangsiapa bertobat setelah melakukan kejahatan itu dan memperbaiki diri, maka sesungguhnya Allah menerima tobatnya. Sungguh, Allah Maha Pengampun, Maha Penyayang.

5:40

# اَلَمْ تَعْلَمْ اَنَّ اللّٰهَ لَهٗ مُلْكُ السَّمٰوٰتِ وَالْاَرْضِۗ يُعَذِّبُ مَنْ يَّشَاۤءُ وَيَغْفِرُ لِمَنْ يَّشَاۤءُ ۗوَاللّٰهُ عَلٰى كُلِّ شَيْءٍ قَدِيْرٌ

alam ta'lam anna **al**l*aa*ha lahu mulku **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i yu'a*dzdz*ibu man yasy*aa*u wayaghfiru liman yasy*aa*u wa**al**l*aa*hu 'al*aa*

*Tidakkah kamu tahu, bahwa Allah memiliki seluruh kerajaan langit dan bumi, Dia menyiksa siapa yang Dia kehendaki dan mengampuni siapa yang Dia kehendaki. Allah Mahakuasa atas segala sesuatu.*









5:41

# ۞ يٰٓاَيُّهَا الرَّسُوْلُ لَا يَحْزُنْكَ الَّذِيْنَ يُسَارِعُوْنَ فِى الْكُفْرِ مِنَ الَّذِيْنَ قَالُوْٓا اٰمَنَّا بِاَفْوَاهِهِمْ وَلَمْ تُؤْمِنْ قُلُوْبُهُمْ ۛ وَمِنَ الَّذِيْنَ هَادُوْا ۛ سَمّٰعُوْنَ لِلْكَذِبِ سَمّٰعُوْنَ لِقَوْمٍ اٰخَرِيْنَۙ لَمْ يَأ

y*aa* ayyuh*aa* **al**rrasuulu l*aa* ya*h*zunka **al**la*dz*iina yus*aa*ri'uuna fii **a**lkufri mina **al**la*dz*iina q*aa*luu *aa*mann*aa* bi-afw*aa*

*Wahai Rasul (Muhammad)! Janganlah engkau disedihkan karena mereka berlomba-lomba dalam kekafirannya. Yaitu orang-orang (munafik) yang mengatakan dengan mulut mereka, “Kami telah beriman,” padahal hati mereka belum beriman; dan juga orang-orang Yahudi yang*









5:42

# سَمّٰعُوْنَ لِلْكَذِبِ اَكّٰلُوْنَ لِلسُّحْتِۗ فَاِنْ جَاۤءُوْكَ فَاحْكُمْ بَيْنَهُمْ اَوْ اَعْرِضْ عَنْهُمْ ۚوَاِنْ تُعْرِضْ عَنْهُمْ فَلَنْ يَّضُرُّوْكَ شَيْـًٔا ۗ وَاِنْ حَكَمْتَ فَاحْكُمْ بَيْنَهُمْ بِالْقِسْطِۗ اِنَّ اللّٰهَ يُحِبُّ الْمُقْسِطِيْنَ

samm*aa*'uuna lilka*dz*ibi akk*aa*luuna li**l**ssu*h*ti fa-in j*aa*uuka fa**u***h*kum baynahum aw a'ri*dh* 'anhum wa-in tu'ri*dh* 'anhum falan ya*dh*urruuka syay-an wa-in *h*akamt

Mereka sangat suka mendengar berita bohong, banyak memakan (makanan) yang haram. Jika mereka (orang Yahudi) datang kepadamu (Muhammad untuk meminta putusan), maka berilah putusan di antara mereka atau berpalinglah dari mereka, dan jika engkau berpaling da

5:43

# وَكَيْفَ يُحَكِّمُوْنَكَ وَعِنْدَهُمُ التَّوْرٰىةُ فِيْهَا حُكْمُ اللّٰهِ ثُمَّ يَتَوَلَّوْنَ مِنْۢ بَعْدِ ذٰلِكَ ۗوَمَآ اُولٰۤىِٕكَ بِالْمُؤْمِنِيْنَ ࣖ

wakayfa yu*h*akkimuunaka wa'indahumu **al**ttawr*aa*tu fiih*aa* *h*ukmu **al**l*aa*hi tsumma yatawallawna min ba'di *dzaa*lika wam*aa* ul*aa*-ika bi**a**lmu/miniin**a**

**Dan bagaimana mereka akan mengangkatmu menjadi hakim mereka, padahal mereka mempunyai Taurat yang di dalamnya (ada) hukum Allah, nanti mereka berpaling (dari putusanmu) setelah itu? Sungguh, mereka bukan orang-orang yang beriman.**









5:44

# اِنَّآ اَنْزَلْنَا التَّوْرٰىةَ فِيْهَا هُدًى وَّنُوْرٌۚ يَحْكُمُ بِهَا النَّبِيُّوْنَ الَّذِيْنَ اَسْلَمُوْا لِلَّذِيْنَ هَادُوْا وَالرَّبَّانِيُّوْنَ وَالْاَحْبَارُ بِمَا اسْتُحْفِظُوْا مِنْ كِتٰبِ اللّٰهِ وَكَانُوْا عَلَيْهِ شُهَدَاۤءَۚ فَلَا تَخْشَوُ

inn*aa* anzaln*aa* **al**ttawr*aa*ta fiih*aa* hudan wanuurun ya*h*kumu bih*aa* **al**nnabiyyuuna **al**la*dz*iina aslamuu lilla*dz*iina h*aa*duu wa**al**rrabb

Sungguh, Kami yang menurunkan Kitab Taurat; di dalamnya (ada) petunjuk dan cahaya. Yang dengan Kitab itu para nabi yang berserah diri kepada Allah memberi putusan atas perkara orang Yahudi, demikian juga para ulama dan pendeta-pendeta mereka, sebab mereka

5:45

# وَكَتَبْنَا عَلَيْهِمْ فِيْهَآ اَنَّ النَّفْسَ بِالنَّفْسِ وَالْعَيْنَ بِالْعَيْنِ وَالْاَنْفَ بِالْاَنْفِ وَالْاُذُنَ بِالْاُذُنِ وَالسِّنَّ بِالسِّنِّۙ وَالْجُرُوْحَ قِصَاصٌۗ فَمَنْ تَصَدَّقَ بِهٖ فَهُوَ كَفَّارَةٌ لَّهٗ ۗوَمَنْ لَّمْ يَحْكُمْ بِمَآ

wakatabn*aa* 'alayhim fiih*aa* anna **al**nnafsa bi**al**nnafsi wa**a**l'ayna bi**a**l'ayni wa**a**l-anfa bi**a**l-anfi wa**a**lu*dz*una bi**a<**

Kami telah menetapkan bagi mereka di dalamnya (Taurat) bahwa nyawa (dibalas) dengan nyawa, mata dengan mata, hidung dengan hidung, telinga dengan telinga, gigi dengan gigi, dan luka-luka (pun) ada qisas-nya (balasan yang sama). Barangsiapa melepaskan (hak







5:46

# وَقَفَّيْنَا عَلٰٓى اٰثَارِهِمْ بِعِيْسَى ابْنِ مَرْيَمَ مُصَدِّقًا لِّمَا بَيْنَ يَدَيْهِ مِنَ التَّوْرٰىةِ ۖواٰتَيْنٰهُ الْاِنْجِيْلَ فِيْهِ هُدًى وَّنُوْرٌۙ وَّمُصَدِّقًا لِّمَا بَيْنَ يَدَيْهِ مِنَ التَّوْرٰىةِ وَهُدًى وَّمَوْعِظَةً لِّلْمُتَّقِيْنَۗ

waqaffayn*aa* 'al*aa* *aa*ts*aa*rihim bi'iis*aa* ibni maryama mu*sh*addiqan lim*aa* bayna yadayhi mina **al**ttawr*aa*ti wa*aa*tayn*aa*hu **a**l-injiila fiihi hudan wanuurun wamu<

Dan Kami teruskan jejak mereka dengan mengutus Isa putra Maryam, membenarkan Kitab yang sebelumnya, yaitu Taurat. Dan Kami menurunkan Injil kepadanya, di dalamnya terdapat petunjuk dan cahaya, dan membenarkan Kitab yang sebelumnya yaitu Taurat, dan sebaga

5:47

# وَلْيَحْكُمْ اَهْلُ الْاِنْجِيْلِ بِمَآ اَنْزَلَ اللّٰهُ فِيْهِۗ وَمَنْ لَّمْ يَحْكُمْ بِمَآ اَنْزَلَ اللّٰهُ فَاُولٰۤىِٕكَ هُمُ الْفٰسِقُوْنَ

walya*h*kum ahlu **a**l-injiili bim*aa* anzala **al**l*aa*hu fiihi waman lam ya*h*kum bim*aa* anzala **al**l*aa*hu faul*aa*-ika humu **a**lf*aa*siquun**a**

**Dan hendaklah pengikut Injil memutuskan perkara menurut apa yang diturunkan Allah di dalamnya. Barangsiapa tidak memutuskan perkara menurut apa yang diturunkan Allah, maka mereka itulah orang-orang fasik.**









5:48

# وَاَنْزَلْنَآ اِلَيْكَ الْكِتٰبَ بِالْحَقِّ مُصَدِّقًا لِّمَا بَيْنَ يَدَيْهِ مِنَ الْكِتٰبِ وَمُهَيْمِنًا عَلَيْهِ فَاحْكُمْ بَيْنَهُمْ بِمَآ اَنْزَلَ اللّٰهُ وَلَا تَتَّبِعْ اَهْوَاۤءَهُمْ عَمَّا جَاۤءَكَ مِنَ الْحَقِّۗ لِكُلٍّ جَعَلْنَا مِنْكُمْ شِرْ

wa-anzaln*aa* ilayka **a**lkit*aa*ba bi**a**l*h*aqqi mu*sh*addiqan lim*aa* bayna yadayhi mina **a**lkit*aa*bi wamuhayminan 'alayhi fa**u***h*kum baynahum bim*aa* a

Dan Kami telah menurunkan Kitab (Al-Qur'an) kepadamu (Muhammad) dengan membawa kebenaran, yang membenarkan kitab-kitab yang diturunkan sebelumnya dan menjaganya, maka putuskanlah perkara mereka menurut apa yang diturunkan Allah dan janganlah engkau mengi

5:49

# وَاَنِ احْكُمْ بَيْنَهُمْ بِمَآ اَنْزَلَ اللّٰهُ وَلَا تَتَّبِعْ اَهْوَاۤءَهُمْ وَاحْذَرْهُمْ اَنْ يَّفْتِنُوْكَ عَنْۢ بَعْضِ مَآ اَنْزَلَ اللّٰهُ اِلَيْكَۗ فَاِنْ تَوَلَّوْا فَاعْلَمْ اَنَّمَا يُرِيْدُ اللّٰهُ اَنْ يُّصِيْبَهُمْ بِبَعْضِ ذُنُوْبِهِمْ ۗ

wa-ani u*h*kum baynahum bim*aa* anzala **al**l*aa*hu wal*aa* tattabi' ahw*aa*-ahum wa**i***hts*arhum an yaftinuuka 'an ba'*dh*i m*aa* anzala **al**l*aa*hu ilayka fa-in taw

dan hendaklah engkau memutuskan perkara di antara mereka menurut apa yang diturunkan Allah, dan janganlah engkau mengikuti keinginan mereka. Dan waspadalah terhadap mereka, jangan sampai mereka memperdayakan engkau terhadap sebagian apa yang telah diturun

5:50

# اَفَحُكْمَ الْجَاهِلِيَّةِ يَبْغُوْنَۗ وَمَنْ اَحْسَنُ مِنَ اللّٰهِ حُكْمًا لِّقَوْمٍ يُّوْقِنُوْنَ ࣖ

afa*h*ukma **a**lj*aa*hiliyyati yabghuuna waman a*h*sanu mina **al**l*aa*hu *h*ukman liqawmin yuuqinuun**a**

Apakah hukum Jahiliah yang mereka kehendaki? (Hukum) siapakah yang lebih baik daripada (hukum) Allah bagi orang-orang yang meyakini (agamanya)?

5:51

# ۞ يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْا لَا تَتَّخِذُوا الْيَهُوْدَ وَالنَّصٰرٰٓى اَوْلِيَاۤءَ ۘ بَعْضُهُمْ اَوْلِيَاۤءُ بَعْضٍۗ وَمَنْ يَّتَوَلَّهُمْ مِّنْكُمْ فَاِنَّهٗ مِنْهُمْ ۗ اِنَّ اللّٰهَ لَا يَهْدِى الْقَوْمَ الظّٰلِمِيْنَ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu l*aa* tattakhi*dz*uu **a**lyahuuda wa**al**nna*shaa*r*aa* awliy*aa*-a ba'*dh*uhum awliy*aa*u ba'*dh*in waman yata

Wahai orang-orang yang beriman! Janganlah kamu menjadikan orang Yahudi dan Nasrani sebagai teman setia(mu); mereka satu sama lain saling melindungi. Barangsiapa di antara kamu yang menjadikan mereka teman setia, maka sesungguhnya dia termasuk golongan mer

5:52

# فَتَرَى الَّذِيْنَ فِيْ قُلُوْبِهِمْ مَّرَضٌ يُّسَارِعُوْنَ فِيْهِمْ يَقُوْلُوْنَ نَخْشٰٓى اَنْ تُصِيْبَنَا دَاۤىِٕرَةٌ ۗفَعَسَى اللّٰهُ اَنْ يَّأْتِيَ بِالْفَتْحِ اَوْ اَمْرٍ مِّنْ عِنْدِهٖ فَيُصْبِحُوْا عَلٰى مَآ اَسَرُّوْا فِيْٓ اَنْفُسِهِمْ نٰدِمِيْ

fatar*aa* **al**la*dz*iina fii quluubihim mara*dh*un yus*aa*ri'uuna fiihim yaquuluuna nakhsy*aa* an tu*sh*iiban*aa* d*aa*-iratun fa'as*aa* **al**l*aa*hu an ya/tiya bi**a**

**Maka kamu akan melihat orang-orang yang hatinya berpenyakit segera mendekati mereka (Yahudi dan Nasrani), seraya berkata, “Kami takut akan mendapat bencana.” Mudah-mudahan Allah akan mendatangkan kemenangan (kepada Rasul-Nya), atau suatu keputusan dari si**









5:53

# وَيَقُوْلُ الَّذِيْنَ اٰمَنُوْٓا اَهٰٓؤُلَاۤءِ الَّذِيْنَ اَقْسَمُوْا بِاللّٰهِ جَهْدَ اَيْمَانِهِمْۙ اِنَّهُمْ لَمَعَكُمْۗ حَبِطَتْ اَعْمَالُهُمْ فَاَصْبَحُوْا خٰسِرِيْنَ

wayaquulu **al**la*dz*iina *aa*manuu ah*aa*ul*aa*-i **al**la*dz*iina aqsamuu bi**al**l*aa*hi jahda aym*aa*nihim innahum lama'akum *h*abi*th*at a'm*aa*luhum fa-a*sh<*

Dan orang-orang yang beriman akan berkata, “Inikah orang yang bersumpah secara sungguh-sungguh dengan (nama) Allah, bahwa mereka benar-benar beserta kamu?” Segala amal mereka menjadi sia-sia, sehingga mereka menjadi orang yang rugi.







5:54

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْا مَنْ يَّرْتَدَّ مِنْكُمْ عَنْ دِيْنِهٖ فَسَوْفَ يَأْتِى اللّٰهُ بِقَوْمٍ يُّحِبُّهُمْ وَيُحِبُّوْنَهٗٓ ۙاَذِلَّةٍ عَلَى الْمُؤْمِنِيْنَ اَعِزَّةٍ عَلَى الْكٰفِرِيْنَۖ يُجَاهِدُوْنَ فِيْ سَبِيْلِ اللّٰهِ وَلَا يَخَافُوْنَ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu man yartadda minkum 'an diinihi fasawfa ya/tii **al**l*aa*hu biqawmin yu*h*ibbuhum wayu*h*ibbuunahu a*dz*illatin 'al*aa* **a**lm

Wahai orang-orang yang beriman! Barangsiapa di antara kamu yang murtad (keluar) dari agamanya, maka kelak Allah akan mendatangkan suatu kaum, Dia mencintai mereka dan mereka pun mencintai-Nya, dan bersikap lemah lembut terhadap orang-orang yang beriman, t

5:55

# اِنَّمَا وَلِيُّكُمُ اللّٰهُ وَرَسُوْلُهٗ وَالَّذِيْنَ اٰمَنُوا الَّذِيْنَ يُقِيْمُوْنَ الصَّلٰوةَ وَيُؤْتُوْنَ الزَّكٰوةَ وَهُمْ رَاكِعُوْنَ

innam*aa* waliyyukumu **al**l*aa*hu warasuuluhu wa**a**lla*dz*iina *aa*manuu **al**la*dz*iina yuqiimuuna **al***shsh*al*aa*ta wayu/tuuna **al**zzak*aa*

*Sesungguhnya penolongmu hanyalah Allah, Rasul-Nya, dan orang-orang yang beriman, yang melaksanakan salat dan menunaikan zakat, seraya tunduk (kepada Allah).*









5:56

# وَمَنْ يَّتَوَلَّ اللّٰهَ وَرَسُوْلَهٗ وَالَّذِيْنَ اٰمَنُوْا فَاِنَّ حِزْبَ اللّٰهِ هُمُ الْغٰلِبُوْنَ ࣖ

waman yatawalla **al**l*aa*ha warasuulahu wa**a**lla*dz*iina *aa*manuu fa-inna *h*izba **al**l*aa*hi humu **a**lgh*aa*libuun**a**

Dan barangsiapa menjadikan Allah, Rasul-Nya dan orang-orang yang beriman sebagai penolongnya, maka sungguh, pengikut (agama) Allah itulah yang menang.

5:57

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْا لَا تَتَّخِذُوا الَّذِيْنَ اتَّخَذُوْا دِيْنَكُمْ هُزُوًا وَّلَعِبًا مِّنَ الَّذِيْنَ اُوْتُوا الْكِتٰبَ مِنْ قَبْلِكُمْ وَالْكُفَّارَ اَوْلِيَاۤءَۚ وَاتَّقُوا اللّٰهَ اِنْ كُنْتُمْ مُّؤْمِنِيْنَ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu l*aa* tattakhi*dz*uu **al**la*dz*iina ittakha*dz*uu diinakum huzuwan wala'iban mina **al**la*dz*iina uutuu **a**lki

Wahai orang-orang yang beriman! Janganlah kamu menjadikan pemimpinmu orang-orang yang membuat agamamu jadi bahan ejekan dan permainan, (yaitu) di antara orang-orang yang telah diberi kitab sebelummu dan orang-orang kafir (orang musyrik). Dan bertakwalah k

5:58

# وَاِذَا نَادَيْتُمْ اِلَى الصَّلٰوةِ اتَّخَذُوْهَا هُزُوًا وَّلَعِبًا ۗذٰلِكَ بِاَ نَّهُمْ قَوْمٌ لَّا يَعْقِلُوْنَ

wa-i*dzaa* n*aa*daytum il*aa* **al***shsh*al*aa*ti ittakha*dz*uuh*aa* huzuwan wala'iban *dzaa*lika bi-annahum qawmun l*aa* ya'qiluun**a**

Dan apabila kamu menyeru (mereka) untuk (melaksanakan) salat, mereka menjadikannya bahan ejekan dan permainan. Yang demikian itu adalah karena mereka orang-orang yang tidak mengerti.

5:59

# قُلْ يٰٓاَهْلَ الْكِتٰبِ هَلْ تَنْقِمُوْنَ مِنَّآ اِلَّآ اَنْ اٰمَنَّا بِاللّٰهِ وَمَآ اُنْزِلَ اِلَيْنَا وَمَآ اُنْزِلَ مِنْ قَبْلُۙ وَاَنَّ اَكْثَرَكُمْ فٰسِقُوْنَ

qul y*aa* ahla **a**lkit*aa*bi hal tanqimuuna minn*aa* ill*aa* an *aa*mann*aa* bi**al**l*aa*hi wam*aa* unzila ilayn*aa* wam*aa* unzila min qablu wa-anna aktsarakum f*aa*siquu

Katakanlah, “Wahai Ahli Kitab! Apakah kamu memandang kami salah, hanya karena kami beriman kepada Allah, kepada apa yang diturunkan kepada kami dan kepada apa yang diturunkan sebelumnya? Sungguh, kebanyakan dari kamu adalah orang-orang yang fasik.”

5:60

# قُلْ هَلْ اُنَبِّئُكُمْ بِشَرٍّ مِّنْ ذٰلِكَ مَثُوْبَةً عِنْدَ اللّٰهِ ۗمَنْ لَّعَنَهُ اللّٰهُ وَغَضِبَ عَلَيْهِ وَجَعَلَ مِنْهُمُ الْقِرَدَةَ وَالْخَنَازِيْرَ وَعَبَدَ الطَّاغُوْتَۗ اُولٰۤىِٕكَ شَرٌّ مَّكَانًا وَّاَضَلُّ عَنْ سَوَاۤءِ السَّبِيْلِ

qul hal unabbi-ukum bisyarrin min *dzaa*lika matsuubatan 'inda **al**l*aa*hi man la'anahu **al**l*aa*hu wagha*dh*iba 'alayhi waja'ala minhumu **a**lqiradata wa**a**lkhan*aa*ziira

Katakanlah (Muhammad), “Apakah akan aku beritakan kepadamu tentang orang yang lebih buruk pembalasannya dari (orang fasik) di sisi Allah? Yaitu, orang yang dilaknat dan dimurkai Allah, di antara mereka (ada) yang dijadikan kera dan babi dan (orang yang) m

5:61

# وَاِذَا جَاۤءُوْكُمْ قَالُوْٓا اٰمَنَّا وَقَدْ دَّخَلُوْا بِالْكُفْرِ وَهُمْ قَدْ خَرَجُوْا بِهٖ ۗوَاللّٰهُ اَعْلَمُ بِمَا كَانُوْا يَكْتُمُوْنَ

wa-i*dzaa* j*aa*uukum q*aa*luu *aa*mann*aa* waqad dakhaluu bi**a**lkufri wahum qad kharajuu bihi wa**al**l*aa*hu a'lamu bim*aa* k*aa*nuu yaktumuun**a**

Dan apabila mereka (Yahudi atau munafik) datang kepadamu, mereka mengatakan, “Kami telah beriman,” padahal mereka datang kepadamu dengan kekafiran dan mereka pergi pun demikian; dan Allah lebih mengetahui apa yang mereka sembunyikan.

5:62

# وَتَرٰى كَثِيْرًا مِّنْهُمْ يُسَارِعُوْنَ فِى الْاِثْمِ وَالْعُدْوَانِ وَاَكْلِهِمُ السُّحْتَۗ لَبِئْسَ مَا كَانُوْا يَعْمَلُوْنَ

watar*aa* katsiiran minhum yus*aa*ri'uuna fii **a**l-itsmi wa**a**l'udw*aa*ni wa-aklihimu **al**ssu*h*ta labi/sa m*aa* k*aa*nuu ya'maluun**a**

Dan kamu akan melihat banyak di antara mereka (orang Yahudi) berlomba dalam berbuat dosa, permusuhan dan memakan yang haram. Sungguh, sangat buruk apa yang mereka perbuat.

5:63

# لَوْلَا يَنْهٰىهُمُ الرَّبَّانِيُّوْنَ وَالْاَحْبَارُ عَنْ قَوْلِهِمُ الْاِثْمَ وَاَكْلِهِمُ السُّحْتَۗ لَبِئْسَ مَا كَانُوْا يَصْنَعُوْنَ

lawl*aa* yanh*aa*humu **al**rrabb*aa*niyyuuna wa**a**l-a*h*b*aa*ru 'an qawlihimu **a**l-itsma wa-aklihimu **al**ssu*h*ta labi/sa m*aa* k*aa*nuu ya*sh*na'uun

Mengapa para ulama dan para pendeta mereka tidak melarang mereka mengucapkan perkataan bohong dan memakan yang haram? Sungguh, sangat buruk apa yang mereka perbuat.

5:64

# وَقَالَتِ الْيَهُوْدُ يَدُ اللّٰهِ مَغْلُوْلَةٌ ۗغُلَّتْ اَيْدِيْهِمْ وَلُعِنُوْا بِمَا قَالُوْا ۘ بَلْ يَدٰهُ مَبْسُوْطَتٰنِۙ يُنْفِقُ كَيْفَ يَشَاۤءُۗ وَلَيَزِيْدَنَّ كَثِيْرًا مِّنْهُمْ مَّآ اُنْزِلَ اِلَيْكَ مِنْ رَّبِّكَ طُغْيَانًا وَّكُفْرًاۗ وَاَ

waq*aa*lati **a**lyahuudu yadu **al**l*aa*hi maghluulatun ghullat aydiihim walu'inuu bim*aa* q*aa*luu bal yad*aa*hu mabsuu*th*at*aa*ni yunfiqu kayfa yasy*aa*u walayaziidanna katsiiran minhu

Dan orang-orang Yahudi berkata, “Tangan Allah terbelenggu.” Sebenarnya tangan merekalah yang dibelenggu dan merekalah yang dilaknat disebabkan apa yang telah mereka katakan itu, padahal kedua tangan Allah terbuka; Dia memberi rezeki sebagaimana Dia kehend

5:65

# وَلَوْ اَنَّ اَهْلَ الْكِتٰبِ اٰمَنُوْا وَاتَّقَوْا لَكَفَّرْنَا عَنْهُمْ سَيِّاٰتِهِمْ وَلَاَدْخَلْنٰهُمْ جَنّٰتِ النَّعِيْمِ

walaw anna ahla **a**lkit*aa*bi *aa*manuu wa**i**ttaqaw lakaffarn*aa* 'anhum sayyi-*aa*tihim wala-adkhaln*aa*hum jann*aa*ti **al**nna'iim**i**

Dan sekiranya Ahli Kitab itu beriman dan bertakwa, niscaya Kami hapus kesalahan-kesalahan mereka, dan mereka tentu Kami masukkan ke dalam surga-surga yang penuh kenikmatan.

5:66

# وَلَوْ اَنَّهُمْ اَقَامُوا التَّوْرٰىةَ وَالْاِنْجِيْلَ وَمَآ اُنْزِلَ اِلَيْهِمْ مِّنْ رَّبِّهِمْ لَاَكَلُوْا مِنْ فَوْقِهِمْ وَمِنْ تَحْتِ اَرْجُلِهِمْۗ مِنْهُمْ اُمَّةٌ مُّقْتَصِدَةٌ ۗ وَكَثِيْرٌ مِّنْهُمْ سَاۤءَ مَا يَعْمَلُوْنَ ࣖ

walaw annahum aq*aa*muu **al**ttawr*aa*ta wa**a**l-injiila wam*aa* unzila ilayhim min rabbihim la-akaluu min fawqihim wamin ta*h*ti arjulihim minhum ummatun muqta*sh*idatun wakatsiirun minhum s*aa*-a

Dan sekiranya mereka sungguh-sungguh menjalankan (hukum) Taurat, Injil dan (Al-Qur'an) yang diturunkan kepada mereka dari Tuhannya, niscaya mereka akan mendapat makanan dari atas mereka dan dari bawah kaki mereka. Di antara mereka ada sekelompok yang juju

5:67

# ۞ يٰٓاَيُّهَا الرَّسُوْلُ بَلِّغْ مَآ اُنْزِلَ اِلَيْكَ مِنْ رَّبِّكَ ۗوَاِنْ لَّمْ تَفْعَلْ فَمَا بَلَّغْتَ رِسٰلَتَهٗ ۗوَاللّٰهُ يَعْصِمُكَ مِنَ النَّاسِۗ اِنَّ اللّٰهَ لَا يَهْدِى الْقَوْمَ الْكٰفِرِيْنَ

y*aa* ayyuh*aa* **al**rrasuulu balligh m*aa* unzila ilayka min rabbika wa-in lam taf'al fam*aa* ballaghta ris*aa*latahu wa**al**l*aa*hu ya'*sh*imuka mina **al**nn*aa*si inna

Wahai Rasul! Sampaikanlah apa yang diturunkan Tuhanmu kepadamu. Jika tidak engkau lakukan (apa yang diperintahkan itu) berarti engkau tidak menyampaikan amanat-Nya. Dan Allah memelihara engkau dari (gangguan) manusia. Sungguh, Allah tidak memberi petunjuk

5:68

# قُلْ يٰٓاَهْلَ الْكِتٰبِ لَسْتُمْ عَلٰى شَيْءٍ حَتّٰى تُقِيْمُوا التَّوْرٰىةَ وَالْاِنْجِيْلَ وَمَآ اُنْزِلَ اِلَيْكُمْ مِّنْ رَّبِّكُمْ ۗوَلَيَزِيْدَنَّ كَثِيْرًا مِّنْهُمْ مَّآ اُنْزِلَ اِلَيْكَ مِنْ رَّبِّكَ طُغْيَانًا وَّكُفْرًاۚ فَلَا تَأْسَ عَلَى

qul y*aa* ahla **a**lkit*aa*bi lastum 'al*aa* syay-in *h*att*aa* tuqiimuu **al**ttawr*aa*ta wa**a**l-injiila wam*aa* unzila ilaykum min rabbikum walayaziidanna katsiiran minhum m

Katakanlah (Muhammad), “Wahai Ahli Kitab! Kamu tidak dipandang beragama sedikit pun hingga kamu menegakkan ajaran-ajaran Taurat, Injil dan (Al-Qur'an) yang diturunkan Tuhanmu kepadamu.” Dan apa yang diturunkan Tuhanmu kepadamu pasti akan membuat banyak d







5:69

# اِنَّ الَّذِيْنَ اٰمَنُوْا وَالَّذِيْنَ هَادُوْا وَالصَّابِـُٔوْنَ وَالنَّصٰرٰى مَنْ اٰمَنَ بِاللّٰهِ وَالْيَوْمِ الْاٰخِرِ وَعَمِلَ صَالِحًا فَلَا خَوْفٌ عَلَيْهِمْ وَلَا هُمْ يَحْزَنُوْنَ

inna **al**la*dz*iina *aa*manuu wa**a**lla*dz*iina h*aa*duu wa**al***shshaa*bi-uuna wa**al**nna*shaa*r*aa* man *aa*mana bi**al**l*aa*hi wa

Sesungguhnya orang-orang yang beriman, orang-orang Yahudi, shabiin dan orang-orang Nasrani, barangsiapa beriman kepada Allah, kepada hari kemudian, dan berbuat kebajikan, maka tidak ada rasa khawatir padanya dan mereka tidak bersedih hati.

5:70

# لَقَدْ اَخَذْنَا مِيْثَاقَ بَنِيْٓ اِسْرَاۤءِيْلَ وَاَرْسَلْنَآ اِلَيْهِمْ رُسُلًا ۗ كُلَّمَا جَاۤءَهُمْ رَسُوْلٌۢ بِمَا لَا تَهْوٰٓى اَنْفُسُهُمْۙ فَرِيْقًا كَذَّبُوْا وَفَرِيْقًا يَّقْتُلُوْنَ

laqad akha*dz*n*aa* miits*aa*qa banii isr*aa*-iila wa-arsaln*aa* ilayhim rusulan kullam*aa* j*aa*-ahum rasuulun bim*aa* l*aa* tahw*aa* anfusuhum fariiqan ka*dzdz*abuu wafariiqan yaqtuluun**a**

**Sesungguhnya Kami telah mengambil perjanjian dari Bani Israil, dan telah Kami utus kepada mereka rasul-rasul. Tetapi setiap rasul datang kepada mereka dengan membawa apa yang tidak sesuai dengan keinginan mereka, (maka) sebagian (dari rasul itu) mereka du**









5:71

# وَحَسِبُوْٓا اَلَّا تَكُوْنَ فِتْنَةٌ فَعَمُوْا وَصَمُّوْا ثُمَّ تَابَ اللّٰهُ عَلَيْهِمْ ثُمَّ عَمُوْا وَصَمُّوْا كَثِيْرٌ مِّنْهُمْۗ وَاللّٰهُ بَصِيْرٌۢ بِمَا يَعْمَلُوْنَ

wa*h*asibuu **al**l*aa* takuuna fitnatun fa'amuu wa*sh*ammuu tsumma t*aa*ba **al**l*aa*hu 'alayhim tsumma 'amuu wa*sh*ammuu katsiirun minhum wa**al**l*aa*hu ba*sh*iirun bim*a*

Dan mereka mengira bahwa tidak akan terjadi bencana apa pun (terhadap mereka dengan membunuh nabi-nabi itu), karena itu mereka menjadi buta dan tuli, kemudian Allah menerima tobat mereka, lalu banyak di antara mereka buta dan tuli. Dan Allah Maha Melihat







5:72

# لَقَدْ كَفَرَ الَّذِيْنَ قَالُوْٓا اِنَّ اللّٰهَ هُوَ الْمَسِيْحُ ابْنُ مَرْيَمَ ۗوَقَالَ الْمَسِيْحُ يٰبَنِيْٓ اِسْرَاۤءِيْلَ اعْبُدُوا اللّٰهَ رَبِّيْ وَرَبَّكُمْ ۗاِنَّهٗ مَنْ يُّشْرِكْ بِاللّٰهِ فَقَدْ حَرَّمَ اللّٰهُ عَلَيْهِ الْجَنَّةَ وَمَأْوٰىهُ ا

laqad kafara **al**la*dz*iina q*aa*luu inna **al**l*aa*ha huwa **a**lmasii*h*u ibnu maryama waq*aa*la **a**lmasii*h*u y*aa* banii isr*aa*-iila u'buduu **al**

**Sungguh, telah kafir orang-orang yang berkata, “Sesungguhnya Allah itu dialah Al-Masih putra Maryam.” Padahal Al-Masih (sendiri) berkata, “Wahai Bani Israil! Sembahlah Allah, Tuhanku dan Tuhanmu.” Sesungguhnya barangsiapa mempersekutukan (sesuatu dengan)**









5:73

# لَقَدْ كَفَرَ الَّذِيْنَ قَالُوْٓا اِنَّ اللّٰهَ ثَالِثُ ثَلٰثَةٍ ۘ وَمَا مِنْ اِلٰهٍ اِلَّآ اِلٰهٌ وَّاحِدٌ ۗوَاِنْ لَّمْ يَنْتَهُوْا عَمَّا يَقُوْلُوْنَ لَيَمَسَّنَّ الَّذِيْنَ كَفَرُوْا مِنْهُمْ عَذَابٌ اَلِيْمٌ

laqad kafara **al**la*dz*iina q*aa*luu inna **al**l*aa*ha ts*aa*litsu tsal*aa*tsatin wam*aa* min il*aa*hin ill*aa* il*aa*hun w*aah*idun wa-in lam yantahuu 'amm*aa* yaquuluuna

Sungguh, telah kafir orang-orang yang mengatakan, bahwa Allah adalah salah satu dari yang tiga, padahal tidak ada tuhan (yang berhak disembah) selain Tuhan Yang Esa. Jika mereka tidak berhenti dari apa yang mereka katakan, pasti orang-orang yang kafir di

5:74

# اَفَلَا يَتُوْبُوْنَ اِلَى اللّٰهِ وَيَسْتَغْفِرُوْنَهٗۗ وَاللّٰهُ غَفُوْرٌ رَّحِيْمٌ

afal*aa* yatuubuuna il*aa* **al**l*aa*hi wayastaghfiruunahu wa**al**l*aa*hu ghafuurun ra*h*iim**un**

Mengapa mereka tidak bertobat kepada Allah dan memohon ampunan kepada-Nya? Allah Maha Pengampun, Maha Penyayang.

5:75

# مَا الْمَسِيْحُ ابْنُ مَرْيَمَ اِلَّا رَسُوْلٌۚ قَدْ خَلَتْ مِنْ قَبْلِهِ الرُّسُلُۗ وَاُمُّهٗ صِدِّيْقَةٌ ۗ كَانَا يَأْكُلَانِ الطَّعَامَ ۗ اُنْظُرْ كَيْفَ نُبَيِّنُ لَهُمُ الْاٰيٰتِ ثُمَّ انْظُرْ اَنّٰى يُؤْفَكُوْنَ

m*aa* **a**lmasii*h*u ibnu maryama ill*aa* rasuulun qad khalat min qablihi **al**rrusulu waummuhu *sh*iddiiqatun k*aa*n*aa* ya/kul*aa*ni **al***ththh*a'*aa*ma un*zh*u

Al-Masih putra Maryam hanyalah seorang Rasul. Sebelumnya pun sudah berlalu beberapa rasul. Dan ibunya seorang yang berpegang teguh pada kebenaran. Keduanya biasa memakan makanan. Perhatikanlah bagaimana Kami menjelaskan ayat-ayat (tanda-tanda kekuasaan) k

5:76

# قُلْ اَتَعْبُدُوْنَ مِنْ دُوْنِ اللّٰهِ مَا لَا يَمْلِكُ لَكُمْ ضَرًّا وَّلَا نَفْعًا ۗوَاللّٰهُ هُوَ السَّمِيْعُ الْعَلِيْمُ

qul ata'buduuna min duuni **al**l*aa*hi m*aa* l*aa* yamliku lakum *dh*arran wal*aa* naf'an wa**al**l*aa*hu huwa **al**ssamii'u **a**l'aliim**u**

Katakanlah (Muhammad), “Mengapa kamu menyembah yang selain Allah, sesuatu yang tidak dapat menimbulkan bencana kepadamu dan tidak (pula) memberi manfaat?” Dan Allah Maha Mendengar, Maha Mengetahui.

5:77

# قُلْ يٰٓاَهْلَ الْكِتٰبِ لَا تَغْلُوْا فِيْ دِيْنِكُمْ غَيْرَ الْحَقِّ وَلَا تَتَّبِعُوْٓا اَهْوَاۤءَ قَوْمٍ قَدْ ضَلُّوْا مِنْ قَبْلُ وَاَضَلُّوْا كَثِيْرًا وَّضَلُّوْا عَنْ سَوَاۤءِ السَّبِيْلِ ࣖ

qul y*aa* ahla **a**lkit*aa*bi l*aa* taghluu fii diinikum ghayra **a**l*h*aqqi wal*aa* tattabi'uu ahw*aa*-a qawmin qad *dh*alluu min qablu wa-a*dh*alluu katsiiran wa*dh*alluu 'an saw

Katakanlah (Muhammad), “Wahai Ahli Kitab! Janganlah kamu berlebih-lebihan dengan cara yang tidak benar dalam agamamu. Dan janganlah kamu mengikuti keinginan orang-orang yang telah tersesat dahulu dan (telah) menyesatkan banyak (manusia), dan mereka sendir

5:78

# لُعِنَ الَّذِيْنَ كَفَرُوْا مِنْۢ بَنِيْٓ اِسْرَاۤءِيْلَ عَلٰى لِسَانِ دَاوٗدَ وَعِيْسَى ابْنِ مَرْيَمَ ۗذٰلِكَ بِمَا عَصَوْا وَّكَانُوْا يَعْتَدُوْنَ

lu'ina **al**la*dz*iina kafaruu min banii isr*aa*-iila 'al*aa* lis*aa*ni d*aa*wuuda wa'iis*aa* ibni maryama *dzaa*lika bim*aa* 'a*sh*aw wak*aa*nuu ya'taduun**a**

Orang-orang kafir dari Bani Israil telah dilaknat melalui lisan (ucapan) Dawud dan Isa putra Maryam. Yang demikian itu karena mereka durhaka dan selalu melampaui batas.

5:79

# كَانُوْا لَا يَتَنَاهَوْنَ عَنْ مُّنْكَرٍ فَعَلُوْهُۗ لَبِئْسَ مَا كَانُوْا يَفْعَلُوْنَ

k*aa*nuu l*aa* yatan*aa*hawna 'an munkarin fa'aluuhu labi/sa m*aa* k*aa*nuu yaf'aluun**a**

Mereka tidak saling mencegah perbuatan mungkar yang selalu mereka perbuat. Sungguh, sangat buruk apa yang mereka perbuat.

5:80

# تَرٰى كَثِيْرًا مِّنْهُمْ يَتَوَلَّوْنَ الَّذِيْنَ كَفَرُوْا ۗ لَبِئْسَ مَا قَدَّمَتْ لَهُمْ اَنْفُسُهُمْ اَنْ سَخِطَ اللّٰهُ عَلَيْهِمْ وَفِى الْعَذَابِ هُمْ خٰلِدُوْنَ

tar*aa* katsiiran minhum yatawallawna **al**la*dz*iina kafaruu labi/sa m*aa* qaddamat lahum anfusuhum an sakhi*th*a **al**l*aa*hu 'alayhim wafii **a**l'a*dzaa*bi hum kh*aa*liduun

Kamu melihat banyak di antara mereka tolong-menolong dengan orang-orang kafir (musyrik). Sungguh, sangat buruk apa yang mereka lakukan untuk diri mereka sendiri, yaitu kemurkaan Allah, dan mereka akan kekal dalam azab.

5:81

# وَلَوْ كَانُوْا يُؤْمِنُوْنَ بِاللّٰهِ وَالنَّبِيِّ وَمَآ اُنْزِلَ اِلَيْهِ مَا اتَّخَذُوْهُمْ اَوْلِيَاۤءَ وَلٰكِنَّ كَثِيْرًا مِّنْهُمْ فٰسِقُوْنَ

walaw k*aa*nuu yu/minuuna bi**al**l*aa*hi wa**al**nnabiyyi wam*aa* unzila ilayhi m*aa* ittakha*dz*uuhum awliy*aa*-a wal*aa*kinna katsiiran minhum f*aa*siquun**a**

Dan sekiranya mereka beriman kepada Allah, kepada Nabi (Muhammad) dan kepada apa yang diturunkan kepadanya, niscaya mereka tidak akan menjadikan orang musyrik itu sebagai teman setia. Tetapi banyak di antara mereka orang-orang yang fasik.

5:82

# ۞ لَتَجِدَنَّ اَشَدَّ النَّاسِ عَدَاوَةً لِّلَّذِيْنَ اٰمَنُوا الْيَهُوْدَ وَالَّذِيْنَ اَشْرَكُوْاۚ وَلَتَجِدَنَّ اَقْرَبَهُمْ مَّوَدَّةً لِّلَّذِيْنَ اٰمَنُوا الَّذِيْنَ قَالُوْٓا اِنَّا نَصٰرٰىۗ ذٰلِكَ بِاَنَّ مِنْهُمْ قِسِّيْسِيْنَ وَرُهْبَانًا وَّاَن

latajidanna asyadda **al**nn*aa*si 'ad*aa*watan lilla*dz*iina *aa*manuu **a**lyahuuda wa**a**lla*dz*iina asyrakuu walatajidanna aqrabahum mawaddatan lilla*dz*iina *aa*manuu

Pasti akan kamu dapati orang yang paling keras permusuhannya terhadap orang-orang yang beriman, yaitu orang-orang Yahudi dan orang-orang musyrik. Dan pasti akan kamu dapati orang yang paling dekat persahabatannya dengan orang-orang yang beriman ialah oran







5:83

# وَاِذَا سَمِعُوْا مَآ اُنْزِلَ اِلَى الرَّسُوْلِ تَرٰٓى اَعْيُنَهُمْ تَفِيْضُ مِنَ الدَّمْعِ مِمَّا عَرَفُوْا مِنَ الْحَقِّۚ يَقُوْلُوْنَ رَبَّنَآ اٰمَنَّا فَاكْتُبْنَا مَعَ الشّٰهِدِيْنَ

wa-i*dzaa* sami'uu m*aa* unzila il*aa* **al**rrasuuli tar*aa* a'yunahum tafii*dh*u mina **al**ddam'i mimm*aa* 'arafuu mina **a**l*h*aqqi yaquuluuna rabban*aa* *aa*mann*a*

Dan apabila mereka mendengarkan apa (Al-Qur'an) yang diturunkan kepada Rasul (Muhammad), kamu lihat mata mereka mencucurkan air mata disebabkan kebenaran yang telah mereka ketahui (dari kitab-kitab mereka sendiri), seraya berkata, “Ya Tuhan, kami telah be







5:84

# وَمَا لَنَا لَا نُؤْمِنُ بِاللّٰهِ وَمَا جَاۤءَنَا مِنَ الْحَقِّۙ وَنَطْمَعُ اَنْ يُّدْخِلَنَا رَبُّنَا مَعَ الْقَوْمِ الصّٰلِحِيْنَ

wam*aa* lan*aa* l*aa* nu/minu bi**al**l*aa*hi wam*aa* j*aa*-an*aa* mina **a**l*h*aqqi wana*th*ma'u an yudkhilan*aa* rabbun*aa* ma'a **a**lqawmi **al**

**Dan mengapa kami tidak akan beriman kepada Allah dan kepada kebenaran yang datang kepada kami, padahal kami sangat ingin agar Tuhan kami memasukkan kami ke dalam golongan orang-orang saleh?”**









5:85

# فَاَثَابَهُمُ اللّٰهُ بِمَا قَالُوْا جَنّٰتٍ تَجْرِيْ مِنْ تَحْتِهَا الْاَنْهٰرُ خٰلِدِيْنَ فِيْهَاۗ وَذٰلِكَ جَزَاۤءُ الْمُحْسِنِيْنَ

fa-ats*aa*bahumu **al**l*aa*hu bim*aa* q*aa*luu jann*aa*tin tajrii min ta*h*tih*aa* **a**l-anh*aa*ru kh*aa*lidiina fiih*aa* wa*dzaa*lika jaz*aa*u **a**lmu

Maka Allah memberi pahala kepada mereka atas perkataan yang telah mereka ucapkan, (yaitu) surga yang mengalir di bawahnya sungai-sungai, mereka kekal di dalamnya. Dan itulah balasan (bagi) orang-orang yang berbuat kebaikan.

5:86

# وَالَّذِيْنَ كَفَرُوْا وَكَذَّبُوْا بِاٰيٰتِنَآ اُولٰۤىِٕكَ اَصْحٰبُ الْجَحِيْمِ ࣖ

wa**a**lla*dz*iina kafaruu waka*dzdz*abuu bi-*aa*y*aa*tin*aa* ul*aa*-ika a*sh*-*haa*bu **a**lja*h*iim**i**

Dan orang-orang yang kafir serta mendustakan ayat-ayat Kami, mereka itulah penghuni neraka.

5:87

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْا لَا تُحَرِّمُوْا طَيِّبٰتِ مَآ اَحَلَّ اللّٰهُ لَكُمْ وَلَا تَعْتَدُوْا ۗاِنَّ اللّٰهَ لَا يُحِبُّ الْمُعْتَدِيْنَ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu l*aa* tu*h*arrimuu *th*ayyib*aa*ti m*aa* a*h*alla **al**l*aa*hu lakum wal*aa* ta'taduu inna **al**l*aa*ha l<

Wahai orang-orang yang beriman! Janganlah kamu mengharamkan apa yang baik yang telah dihalalkan Allah kepadamu, dan janganlah kamu melampaui batas. Sesungguhnya Allah tidak menyukai orang-orang yang melampaui batas.

5:88

# وَكُلُوْا مِمَّا رَزَقَكُمُ اللّٰهُ حَلٰلًا طَيِّبًا ۖوَّاتَّقُوا اللّٰهَ الَّذِيْٓ اَنْتُمْ بِهٖ مُؤْمِنُوْنَ

wakuluu mimm*aa* razaqakumu **al**l*aa*hu *h*al*aa*lan *th*ayyiban wa**i**ttaquu **al**l*aa*ha **al**la*dz*ii antum bihi mu/minuun**a**

Dan makanlah dari apa yang telah diberikan Allah kepadamu sebagai rezeki yang halal dan baik, dan bertakwalah kepada Allah yang kamu beriman kepada-Nya.

5:89

# لَا يُؤَاخِذُكُمُ اللّٰهُ بِاللَّغْوِ فِيْٓ اَيْمَانِكُمْ وَلٰكِنْ يُّؤَاخِذُكُمْ بِمَا عَقَّدْتُّمُ الْاَيْمَانَۚ فَكَفَّارَتُهٗٓ اِطْعَامُ عَشَرَةِ مَسٰكِيْنَ مِنْ اَوْسَطِ مَا تُطْعِمُوْنَ اَهْلِيْكُمْ اَوْ كِسْوَتُهُمْ اَوْ تَحْرِيْرُ رَقَبَةٍ ۗفَمَن

l*aa* yu-*aa*khi*dz*ukumu **al**l*aa*hu bi**a**llaghwi fii aym*aa*nikum wal*aa*kin yu-*aa*khi*dz*ukum bim*aa* 'aqqadtumu **a**l-aym*aa*na fakaff*aa*ratuhu i*th*

Allah tidak menghukum kamu disebabkan sumpah-sumpahmu yang tidak disengaja (untuk bersumpah), tetapi Dia menghukum kamu disebabkan sumpah-sumpah yang kamu sengaja, maka kafaratnya (denda pelanggaran sumpah) ialah memberi makan sepuluh orang miskin, yaitu







5:90

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْٓا اِنَّمَا الْخَمْرُ وَالْمَيْسِرُ وَالْاَنْصَابُ وَالْاَزْلَامُ رِجْسٌ مِّنْ عَمَلِ الشَّيْطٰنِ فَاجْتَنِبُوْهُ لَعَلَّكُمْ تُفْلِحُوْنَ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu innam*aa* **a**lkhamru wa**a**lmaysiru wa**a**l-an*shaa*bu wa**a**l-azl*aa*mu rijsun min 'amali **al**

**Wahai orang-orang yang beriman! Sesungguhnya minuman keras, berjudi, (berkurban untuk) berhala, dan mengundi nasib dengan anak panah, adalah perbuatan keji dan termasuk perbuatan setan. Maka jauhilah (perbuatan-perbuatan) itu agar kamu beruntung.**









5:91

# اِنَّمَا يُرِيْدُ الشَّيْطٰنُ اَنْ يُّوْقِعَ بَيْنَكُمُ الْعَدَاوَةَ وَالْبَغْضَاۤءَ فِى الْخَمْرِ وَالْمَيْسِرِ وَيَصُدَّكُمْ عَنْ ذِكْرِ اللّٰهِ وَعَنِ الصَّلٰوةِ فَهَلْ اَنْتُمْ مُّنْتَهُوْنَ

innam*aa* yuriidu **al**sysyay*thaa*nu an yuuqi'a baynakumu **a**l'ad*aa*wata wa**a**lbagh*dhaa*-a fii **a**lkhamri wa**a**lmaysiri waya*sh*uddakum 'an *dz<*

Dengan minuman keras dan judi itu, setan hanyalah bermaksud menimbulkan permusuhan dan kebencian di antara kamu, dan menghalang-halangi kamu dari mengingat Allah dan melaksanakan salat, maka tidakkah kamu mau berhenti?







5:92

# وَاَطِيْعُوا اللّٰهَ وَاَطِيْعُوا الرَّسُوْلَ وَاحْذَرُوْا ۚفَاِنْ تَوَلَّيْتُمْ فَاعْلَمُوْٓا اَنَّمَا عَلٰى رَسُوْلِنَا الْبَلٰغُ الْمُبِيْنُ

wa-a*th*ii'uu **al**l*aa*ha wa-a*th*ii'uu **al**rrasuula wa**i***hts*aruu fa-in tawallaytum fa**i**'lamuu annam*aa* 'al*aa* rasuulin*aa* **a**lbal*aa*

Dan taatlah kamu kepada Allah dan taatlah kamu kepada Rasul serta berhati-hatilah. Jika kamu berpaling, maka ketahuilah bahwa kewajiban Rasul Kami hanyalah menyampaikan (amanat) dengan jelas.

5:93

# لَيْسَ عَلَى الَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِ جُنَاحٌ فِيْمَا طَعِمُوْٓا اِذَا مَا اتَّقَوْا وَّاٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِ ثُمَّ اتَّقَوْا وَّاٰمَنُوْا ثُمَّ اتَّقَوْا وَّاَحْسَنُوْا ۗوَاللّٰهُ يُحِبُّ الْمُحْسِنِيْنَ ࣖ

laysa 'al*aa* **al**la*dz*iina *aa*manuu wa'amiluu **al***shshaa*li*haa*ti jun*aah*un fiim*aa* *th*a'imuu i*dzaa* m*aa* ittaqaw wa*aa*manuu wa'amiluu **al***sh*

Tidak berdosa bagi orang-orang yang beriman dan mengerjakan kebajikan tentang apa yang mereka makan (dahulu), apabila mereka bertakwa dan beriman, serta mengerjakan kebajikan, kemudian mereka tetap bertakwa dan beriman, selanjutnya mereka (tetap juga) ber







5:94

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْا لَيَبْلُوَنَّكُمُ اللّٰهُ بِشَيْءٍ مِّنَ الصَّيْدِ تَنَالُهٗٓ اَيْدِيْكُمْ وَرِمَاحُكُمْ لِيَعْلَمَ اللّٰهُ مَنْ يَّخَافُهٗ بِالْغَيْبِۚ فَمَنِ اعْتَدٰى بَعْدَ ذٰلِكَ فَلَهٗ عَذَابٌ اَلِيْمٌ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu layabluwannakumu **al**l*aa*hu bisyay-in mina **al***shsh*aydi tan*aa*luhu aydiikum warim*aah*ukum liya'lama **al**l*a*

Wahai orang-orang yang beriman! Allah pasti akan menguji kamu dengan hewan buruan yang dengan mudah kamu peroleh dengan tangan dan tombakmu agar Allah mengetahui siapa yang takut kepada-Nya, meskipun dia tidak melihat-Nya. Barangsiapa melampaui batas sete







5:95

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْا لَا تَقْتُلُوا الصَّيْدَ وَاَنْتُمْ حُرُمٌ ۗوَمَنْ قَتَلَهٗ مِنْكُمْ مُّتَعَمِّدًا فَجَزَۤاءٌ مِّثْلُ مَا قَتَلَ مِنَ النَّعَمِ يَحْكُمُ بِهٖ ذَوَا عَدْلٍ مِّنْكُمْ هَدْيًاۢ بٰلِغَ الْكَعْبَةِ اَوْ كَفَّارَةٌ طَعَامُ مَسٰك

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu l*aa* taqtuluu **al***shsh*ayda wa-antum *h*urumun waman qatalahu minkum muta'ammidan fajaz*aa*un mitslu m*aa* qatala mina **al**

**Wahai orang-orang yang beriman! Janganlah kamu membunuh hewan buruan, ketika kamu sedang ihram (haji atau umrah). Barangsiapa di antara kamu membunuhnya dengan sengaja, maka dendanya ialah mengganti dengan hewan ternak yang sepadan dengan buruan yang dibu**









5:96

# اُحِلَّ لَكُمْ صَيْدُ الْبَحْرِ وَطَعَامُهٗ مَتَاعًا لَّكُمْ وَلِلسَّيَّارَةِ ۚوَحُرِّمَ عَلَيْكُمْ صَيْدُ الْبَرِّ مَا دُمْتُمْ حُرُمًا ۗوَاتَّقُوا اللّٰهَ الَّذِيْٓ اِلَيْهِ تُحْشَرُوْنَ

u*h*illa lakum *sh*aydu **a**lba*h*ri wa*th*a'*aa*muhu mat*aa*'an lakum wali**l**ssayy*aa*rati wa*h*urrima 'alaykum *sh*aydu **a**lbarri m*aa* dumtum *h*uruman w

Dihalalkan bagimu hewan buruan laut dan makanan (yang berasal) dari laut sebagai makanan yang lezat bagimu, dan bagi orang-orang yang dalam perjalanan; dan diharamkan atasmu (menangkap) hewan darat, selama kamu sedang ihram. Dan bertakwalah kepada Allah y

5:97

# ۞ جَعَلَ اللّٰهُ الْكَعْبَةَ الْبَيْتَ الْحَرَامَ قِيٰمًا لِّلنَّاسِ وَالشَّهْرَ الْحَرَامَ وَالْهَدْيَ وَالْقَلَاۤىِٕدَ ۗذٰلِكَ لِتَعْلَمُوْٓا اَنَّ اللّٰهَ يَعْلَمُ مَا فِى السَّمٰوٰتِ وَمَا فِى الْاَرْضِۙ وَاَنَّ اللّٰهَ بِكُلِّ شَيْءٍ عَلِيْمٌ

ja'ala **al**l*aa*hu **a**lka'bata **a**lbayta **a**l*h*ar*aa*ma qiy*aa*man li**l**nn*aa*si wa**al**sysyahra **a**l*h*ar*aa*ma wa

Allah telah menjadikan Ka‘bah rumah suci tempat manusia berkumpul. Demikian pula bulan haram, hadyu dan qala'id. Yang demikian itu agar kamu mengetahui, bahwa Allah mengetahui apa yang ada di langit dan apa yang ada di bumi, dan bahwa Allah Maha Mengetahu

5:98

# اِعْلَمُوْٓا اَنَّ اللّٰهَ شَدِيْدُ الْعِقَابِۙ وَاَنَّ اللّٰهَ غَفُوْرٌ رَّحِيْمٌۗ

i'lamuu anna **al**l*aa*ha syadiidu **a**l'iq*aa*bi wa-anna **al**l*aa*ha ghafuurun ra*h*iim**un**

Ketahuilah, bahwa Allah sangat keras siksaan-Nya dan bahwa Allah Maha Pengampun, Maha Penyayang.

5:99

# مَا عَلَى الرَّسُوْلِ اِلَّا الْبَلٰغُ ۗوَاللّٰهُ يَعْلَمُ مَا تُبْدُوْنَ وَمَا تَكْتُمُوْنَ

m*aa* 'al*aa* **al**rrasuuli ill*aa* **a**lbal*aa*ghu wa**al**l*aa*hu ya'lamu m*aa* tubduuna wam*aa* taktumuun**a**

Kewajiban Rasul tidak lain hanyalah menyampaikan (amanat Allah), dan Allah mengetahui apa yang kamu tampakkan dan apa yang kamu sembunyikan.

5:100

# قُلْ لَّا يَسْتَوِى الْخَبِيْثُ وَالطَّيِّبُ وَلَوْ اَعْجَبَكَ كَثْرَةُ الْخَبِيْثِۚ فَاتَّقُوا اللّٰهَ يٰٓاُولِى الْاَلْبَابِ لَعَلَّكُمْ تُفْلِحُوْنَ ࣖ

qul l*aa* yastawii **a**lkhabiitsu wa**al***ththh*ayyibu walaw a'jabaka katsratu **a**lkhabiitsi fa**i**ttaquu **al**l*aa*ha y*aa* ulii **a**l-alb*aa*bi

Katakanlah (Muhammad), “Tidaklah sama yang buruk dengan yang baik, meskipun banyaknya keburukan itu menarik hatimu, maka bertakwalah kepada Allah wahai orang-orang yang mempunyai akal sehat, agar kamu beruntung.”

5:101

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْا لَا تَسْـَٔلُوْا عَنْ اَشْيَاۤءَ اِنْ تُبْدَ لَكُمْ تَسُؤْكُمْ ۚوَاِنْ تَسْـَٔلُوْا عَنْهَا حِيْنَ يُنَزَّلُ الْقُرْاٰنُ تُبْدَ لَكُمْ ۗعَفَا اللّٰهُ عَنْهَا ۗوَاللّٰهُ غَفُوْرٌ حَلِيْمٌ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu l*aa* tas-aluu 'an asyy*aa*-a in tubda lakum tasu/kum wa-in tas-aluu 'anh*aa* *h*iina yunazzalu **a**lqur-*aa*nu tubda lakum 'af*aa*

Wahai orang-orang yang beriman! Janganlah kamu menanyakan (kepada Nabimu) hal-hal yang jika diterangkan kepadamu (justru) menyusahkan kamu. Jika kamu menanyakannya ketika Al-Qur'an sedang diturunkan, (niscaya) akan diterangkan kepadamu. Allah telah memaaf

5:102

# قَدْ سَاَلَهَا قَوْمٌ مِّنْ قَبْلِكُمْ ثُمَّ اَصْبَحُوْا بِهَا كٰفِرِيْنَ

qad sa-alah*aa* qawmun min qablikum tsumma a*sh*ba*h*uu bih*aa* k*aa*firiin**a**

Sesungguhnya sebelum kamu telah ada segolongan manusia yang menanyakan hal-hal serupa itu (kepada nabi mereka), kemudian mereka menjadi kafir.

5:103

# مَا جَعَلَ اللّٰهُ مِنْۢ بَحِيْرَةٍ وَّلَا سَاۤىِٕبَةٍ وَّلَا وَصِيْلَةٍ وَّلَا حَامٍ ۙوَّلٰكِنَّ الَّذِيْنَ كَفَرُوْا يَفْتَرُوْنَ عَلَى اللّٰهِ الْكَذِبَۗ وَاَكْثَرُهُمْ لَا يَعْقِلُوْنَ

m*aa* ja'ala **al**l*aa*hu min ba*h*iiratin wal*aa* s*aa*-ibatin wal*aa* wa*sh*iilatin wal*aa* *haa*min wal*aa*kinna **al**la*dz*iina kafaruu yaftaruuna 'al*aa*

Allah tidak pernah mensyariatkan adanya Bahirah, Sa'ibah, Wasilah dan haam. Tetapi orang-orang kafir membuat-buat kedustaan terhadap Allah, dan kebanyakan mereka tidak mengerti.







5:104

# وَاِذَا قِيْلَ لَهُمْ تَعَالَوْا اِلٰى مَآ اَنْزَلَ اللّٰهُ وَاِلَى الرَّسُوْلِ قَالُوْا حَسْبُنَا مَا وَجَدْنَا عَلَيْهِ اٰبَاۤءَنَا ۗ اَوَلَوْ كَانَ اٰبَاۤؤُهُمْ لَا يَعْلَمُوْنَ شَيْـًٔا وَّلَا يَهْتَدُوْنَ

wa-i*dzaa* qiila lahum ta'*aa*law il*aa* m*aa* anzala **al**l*aa*hu wa-il*aa* **al**rrasuuli q*aa*luu *h*asbun*aa* m*aa* wajadn*aa* 'alayhi *aa*b*aa*-an*aa* aw

Dan apabila dikatakan kepada mereka, “Marilah (mengikuti) apa yang diturunkan Allah dan (mengikuti) Rasul.” Mereka menjawab, “Cukuplah bagi kami apa yang kami dapati nenek moyang kami (mengerjakannya).” Apakah (mereka akan mengikuti) juga nenek moyang mer

5:105

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْا عَلَيْكُمْ اَنْفُسَكُمْ ۚ لَا يَضُرُّكُمْ مَّنْ ضَلَّ اِذَا اهْتَدَيْتُمْ ۗ اِلَى اللّٰهِ مَرْجِعُكُمْ جَمِيْعًا فَيُنَبِّئُكُمْ بِمَا كُنْتُمْ تَعْمَلُوْنَ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu 'alaykum anfusakum l*aa* ya*dh*urrukum man *dh*alla i*dz*a ihtadaytum il*aa* **al**l*aa*hi marji'ukum jamii'an fayunabbi-ukum bim*aa*

*Wahai orang-orang yang beriman! Jagalah dirimu; (karena) orang yang sesat itu tidak akan membahayakanmu apabila kamu telah mendapat petunjuk. Hanya kepada Allah kamu semua akan kembali, kemudian Dia akan menerangkan kepadamu apa yang telah kamu kerjakan.*









5:106

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْا شَهَادَةُ بَيْنِكُمْ اِذَا حَضَرَ اَحَدَكُمُ الْمَوْتُ حِيْنَ الْوَصِيَّةِ اثْنٰنِ ذَوَا عَدْلٍ مِّنْكُمْ اَوْ اٰخَرٰنِ مِنْ غَيْرِكُمْ اِنْ اَنْتُمْ ضَرَبْتُمْ فِى الْاَرْضِ فَاَصَابَتْكُمْ مُّصِيْبَةُ الْمَوْتِۗ تَحْبِسُ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu syah*aa*datu baynikum i*dzaa* *h*a*dh*ara a*h*adakumu **a**lmawtu *h*iina **a**lwa*sh*iyyati itsn*aa*ni *dz*

Wahai orang-orang yang beriman! Apabila salah seorang (di antara) kamu menghadapi kematian, sedang dia akan berwasiat, maka hendaklah (wasiat itu) disaksikan oleh dua orang yang adil di antara kamu, atau dua orang yang berlainan (agama) dengan kamu. Jika

5:107

# فَاِنْ عُثِرَ عَلٰٓى اَنَّهُمَا اسْتَحَقَّآ اِثْمًا فَاٰخَرٰنِ يَقُوْمٰنِ مَقَامَهُمَا مِنَ الَّذِيْنَ اسْتَحَقَّ عَلَيْهِمُ الْاَوْلَيٰنِ فَيُقْسِمٰنِ بِاللّٰهِ لَشَهَادَتُنَآ اَحَقُّ مِنْ شَهَادَتِهِمَا وَمَا اعْتَدَيْنَآ ۖاِنَّآ اِذًا لَّمِنَ الظّٰ

fa-in 'utsira 'al*aa* annahum*aa* ista*h*aqq*aa* itsman fa*aa*khara*n*i yaquum*aa*ni maq*aa*mahum*aa* mina **al**la*dz*iina ista*h*aqqa 'alayhimu **a**l-awlay*aa*ni fayu

Jika terbukti kedua saksi itu berbuat dosa, maka dua orang yang lain menggantikan kedudukannya, yaitu di antara ahli waris yang berhak dan lebih dekat kepada orang yang mati, lalu keduanya bersumpah dengan nama Allah, “Sungguh, kesaksian kami lebih layak

5:108

# ذٰلِكَ اَدْنٰٓى اَنْ يَّأْتُوْا بِالشَّهَادَةِ عَلٰى وَجْهِهَآ اَوْ يَخَافُوْٓا اَنْ تُرَدَّ اَيْمَانٌۢ بَعْدَ اَيْمَانِهِمْۗ وَاتَّقُوا اللّٰهَ وَاسْمَعُوْا ۗوَاللّٰهُ لَا يَهْدِى الْقَوْمَ الْفٰسِقِيْنَ ࣖ

*dzaa*lika adn*aa* an ya/tuu bi**al**sysyah*aa*dati 'al*aa* wajhih*aa* aw yakh*aa*fuu an turadda aym*aa*nun ba'da aym*aa*nihim wa**i**ttaquu **al**l*aa*ha wa**i**

**Dengan cara itu mereka lebih patut memberikan kesaksiannya menurut yang sebenarnya, dan mereka merasa takut akan dikembalikan sumpahnya (kepada ahli waris) setelah mereka bersumpah. Bertakwalah kepada Allah dan dengarkanlah (perintah-Nya). Dan Allah tidak**









5:109

# ۞ يَوْمَ يَجْمَعُ اللّٰهُ الرُّسُلَ فَيَقُوْلُ مَاذَٓا اُجِبْتُمْ ۗ قَالُوْا لَا عِلْمَ لَنَا ۗاِنَّكَ اَنْتَ عَلَّامُ الْغُيُوْبِ

yawma yajma'u **al**l*aa*hu **al**rrusula fayaquulu m*aatsaa* ujibtum q*aa*luu l*aa* 'ilma lan*aa* innaka anta 'all*aa*mu **a**lghuyuub**i**

(Ingatlah) pada hari ketika Allah mengumpulkan para rasul, lalu Dia bertanya (kepada mereka), “Apa jawaban (kaummu) terhadap (seruan)mu?” Mereka (para rasul) menjawab, “Kami tidak tahu (tentang itu). Sesungguhnya Engkaulah Yang Maha Mengetahui segala yang

5:110

# اِذْ قَالَ اللّٰهُ يٰعِيْسَى ابْنَ مَرْيَمَ اذْكُرْ نِعْمَتِيْ عَلَيْكَ وَعَلٰى وَالِدَتِكَ ۘاِذْ اَيَّدْتُّكَ بِرُوْحِ الْقُدُسِۗ تُكَلِّمُ النَّاسَ فِى الْمَهْدِ وَكَهْلًا ۚوَاِذْ عَلَّمْتُكَ الْكِتٰبَ وَالْحِكْمَةَ وَالتَّوْرٰىةَ وَالْاِنْجِيْلَ ۚوَاِ

i*dz* q*aa*la **al**l*aa*hu y*aa* 'iis*aa* ibna maryama u*dz*kur ni'matii 'alayka wa'al*aa* w*aa*lidatika i*dz* ayyadtuka biruu*h*i **a**lqudusi tukallimu **al**nn

Dan ingatlah ketika Allah berfirman, “Wahai Isa putra Maryam! Ingatlah nikmat-Ku kepadamu dan kepada ibumu sewaktu Aku menguatkanmu dengan Rohulkudus. Engkau dapat berbicara dengan manusia pada waktu masih dalam buaian dan setelah dewasa. Dan ingatlah ket







5:111

# وَاِذْ اَوْحَيْتُ اِلَى الْحَوَارِيّٖنَ اَنْ اٰمِنُوْا بِيْ وَبِرَسُوْلِيْ ۚ قَالُوْٓا اٰمَنَّا وَاشْهَدْ بِاَنَّنَا مُسْلِمُوْنَ

wa-i*dz* aw*h*aytu il*aa* **a**l*h*aw*aa*riyyiina an *aa*minuu bii wabirasuulii q*aa*luu *aa*mann*aa* wa**i**syhad bi-annan*aa* muslimuun**a**

Dan (ingatlah), ketika Aku ilhamkan kepada pengikut-pengikut Isa yang setia, “Berimanlah kamu kepada-Ku dan kepada Rasul-Ku.” Mereka menjawab, “Kami telah beriman, dan saksikanlah (wahai Rasul) bahwa kami adalah orang-orang yang berserah diri (Muslim).”

5:112

# اِذْ قَالَ الْحَوَارِيُّوْنَ يٰعِيْسَى ابْنَ مَرْيَمَ هَلْ يَسْتَطِيْعُ رَبُّكَ اَنْ يُّنَزِّلَ عَلَيْنَا مَاۤىِٕدَةً مِّنَ السَّمَاۤءِ ۗقَالَ اتَّقُوا اللّٰهَ اِنْ كُنْتُمْ مُّؤْمِنِيْنَ

i*dz* q*aa*la **a**l*h*aw*aa*riyyuuna y*aa* 'iis*aa* ibna maryama hal yasta*th*ii'u rabbuka an yunazzila 'alayn*aa* m*aa*-idatan mina **al**ssam*aa*-i q*aa*la ittaquu

(Ingatlah), ketika pengikut-pengikut Isa yang setia berkata, “Wahai Isa putra Maryam! Bersediakah Tuhanmu menurunkan hidangan dari langit kepada kami?” Isa menjawab, “Bertakwalah kepada Allah jika kamu orang-orang beriman.”

5:113

# قَالُوْا نُرِيْدُ اَنْ نَّأْكُلَ مِنْهَا وَتَطْمَىِٕنَّ قُلُوْبُنَا وَنَعْلَمَ اَنْ قَدْ صَدَقْتَنَا وَنَكُوْنَ عَلَيْهَا مِنَ الشّٰهِدِيْنَ

q*aa*luu nuriidu an na/kula minh*aa* wata*th*ma-inna quluubun*aa* wana'lama an qad *sh*adaqtan*aa* wanakuuna 'alayh*aa* mina **al**sysy*aa*hidiin**a**

Mereka berkata, “Kami ingin memakan hidangan itu agar tenteram hati kami dan agar kami yakin bahwa engkau telah berkata benar kepada kami, dan kami menjadi orang-orang yang menyaksikan (hidangan itu).”

5:114

# قَالَ عِيْسَى ابْنُ مَرْيَمَ اللهم رَبَّنَآ اَنْزِلْ عَلَيْنَا مَاۤىِٕدَةً مِّنَ السَّمَاۤءِ تَكُوْنُ لَنَا عِيْدًا لِّاَوَّلِنَا وَاٰخِرِنَا وَاٰيَةً مِّنْكَ وَارْزُقْنَا وَاَنْتَ خَيْرُ الرّٰزِقِيْنَ

q*aa*la 'iis*aa* ibnu maryama **al**l*aa*humma rabban*aa* anzil 'alayn*aa* m*aa*-idatan mina **al**ssam*aa*-i takuunu lan*aa* 'iidan li-awwalin*aa* wa*aa*khirin*aa* wa*aa*

*Isa putra Maryam berdoa, “Ya Tuhan kami, turunkanlah kepada kami hidangan dari langit (yang hari turunnya) akan menjadi hari raya bagi kami, yaitu bagi orang-orang yang sekarang bersama kami maupun yang datang setelah kami, dan menjadi tanda bagi kekuasa*









5:115

# قَالَ اللّٰهُ اِنِّيْ مُنَزِّلُهَا عَلَيْكُمْ ۚ فَمَنْ يَّكْفُرْ بَعْدُ مِنْكُمْ فَاِنِّيْٓ اُعَذِّبُهٗ عَذَابًا لَّآ اُعَذِّبُهٗٓ اَحَدًا مِّنَ الْعٰلَمِيْنَ ࣖ

q*aa*la **al**l*aa*hu innii munazziluh*aa* 'alaykum faman yakfur ba'du minkum fa-innii u'a*dzdz*ibuhu 'a*dzaa*ban l*aa* u'a*dzdz*ibuhu a*h*adan mina **a**l'*aa*lamiin**a**

Allah berfirman, “Sungguh, Aku akan menurunkan hidangan itu kepadamu, tetapi barangsiapa kafir di antaramu setelah (turun hidangan) itu, maka sungguh, Aku akan mengazabnya dengan azab yang tidak pernah Aku timpakan kepada seorang pun di antara umat manusi

5:116

# وَاِذْ قَالَ اللّٰهُ يٰعِيْسَى ابْنَ مَرْيَمَ ءَاَنْتَ قُلْتَ لِلنَّاسِ اتَّخِذُوْنِيْ وَاُمِّيَ اِلٰهَيْنِ مِنْ دُوْنِ اللّٰهِ ۗقَالَ سُبْحٰنَكَ مَا يَكُوْنُ لِيْٓ اَنْ اَقُوْلَ مَا لَيْسَ لِيْ بِحَقٍّ ۗاِنْ كُنْتُ قُلْتُهٗ فَقَدْ عَلِمْتَهٗ ۗتَعْلَمُ مَ

wa-i*dz* q*aa*la **al**l*aa*hu y*aa* 'iis*aa* ibna maryama a-anta qulta li**l**nn*aa*si ittakhi*dz*uunii waummiya il*aa*hayni min duuni **al**l*aa*hi q*aa*la sub*ha*

Dan (ingatlah) ketika Allah berfirman, “Wahai Isa putra Maryam! Engkaukah yang mengatakan kepada orang-orang, jadikanlah aku dan ibuku sebagai dua tuhan selain Allah?” (Isa) menjawab, “Mahasuci Engkau, tidak patut bagiku mengatakan apa yang bukan hakku. J







5:117

# مَا قُلْتُ لَهُمْ اِلَّا مَآ اَمَرْتَنِيْ بِهٖٓ اَنِ اعْبُدُوا اللّٰهَ رَبِّيْ وَرَبَّكُمْ ۚوَكُنْتُ عَلَيْهِمْ شَهِيْدًا مَّا دُمْتُ فِيْهِمْ ۚ فَلَمَّا تَوَفَّيْتَنِيْ كُنْتَ اَنْتَ الرَّقِيْبَ عَلَيْهِمْ ۗوَاَنْتَ عَلٰى كُلِّ شَيْءٍ شَهِيْدٌ

m*aa* qultu lahum ill*aa* m*aa* amartanii bihi ani u'buduu **al**l*aa*ha rabbii warabbakum wakuntu 'alayhim syahiidan m*aa* dumtu fiihim falamm*aa* tawaffaytanii kunta anta **al**rraqiiba 'alayhim wa

Aku tidak pernah mengatakan kepada mereka kecuali apa yang Engkau perintahkan kepadaku (yaitu), “Sembahlah Allah, Tuhanku dan Tuhanmu,” dan aku menjadi saksi terhadap mereka, selama aku berada di tengah-tengah mereka. Maka setelah Engkau mewafatkan aku, E

5:118

# اِنْ تُعَذِّبْهُمْ فَاِنَّهُمْ عِبَادُكَ ۚوَاِنْ تَغْفِرْ لَهُمْ فَاِنَّكَ اَنْتَ الْعَزِيْزُ الْحَكِيْمُ

in tu'a*dzdz*ibhum fa-innahum 'ib*aa*duka wa-in taghfir lahum fa-innaka anta **a**l'aziizu **a**l*h*akiim**u**

Jika Engkau menyiksa mereka, maka sesungguhnya mereka adalah hamba-hamba-Mu, dan jika Engkau mengampuni mereka, sesungguhnya Engkaulah Yang Mahaperkasa, Mahabijaksana.”

5:119

# قَالَ اللّٰهُ هٰذَا يَوْمُ يَنْفَعُ الصّٰدِقِيْنَ صِدْقُهُمْ ۗ لَهُمْ جَنّٰتٌ تَجْرِيْ مِنْ تَحْتِهَا الْاَنْهٰرُ خٰلِدِيْنَ فِيْهَآ اَبَدًا ۗرَضِيَ اللّٰهُ عَنْهُمْ وَرَضُوْا عَنْهُ ۗذٰلِكَ الْفَوْزُ الْعَظِيْمُ

q*aa*la **al**l*aa*hu h*aadzaa* yawmu yanfa'u **al***shshaa*diqiina *sh*idquhum lahum jann*aa*tun tajrii min ta*h*tih*aa* **a**l-anh*aa*ru kh*aa*lidiina fiih*aa*

Allah berfirman, “Inilah saat orang yang benar memperoleh manfaat dari kebenarannya. Mereka memperoleh surga yang mengalir di bawahnya sungai-sungai, mereka kekal di dalamnya selama-lamanya. Allah rida kepada mereka dan mereka pun rida kepada-Nya. Itulah

5:120

# لِلّٰهِ مُلْكُ السَّمٰوٰتِ وَالْاَرْضِ وَمَا فِيْهِنَّ ۗوَهُوَ عَلٰى كُلِّ شَيْءٍ قَدِيْرٌ ࣖ

lill*aa*hi mulku **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i wam*aa* fiihinna wahuwa 'al*aa* kulli syay-in qadiir**un**

Milik Allah kerajaan langit dan bumi dan apa yang ada di dalamnya; dan Dia Mahakuasa atas segala sesuatu.

<!--EndFragment-->