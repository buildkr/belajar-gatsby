---
title: (75) Al-Qiyamah - القيٰمة
date: 2021-10-27T04:07:30.553Z
ayat: 75
description: "Jumlah Ayat: 40 / Arti: Hari Kiamat"
---
<!--StartFragment-->

75:1

# لَآ اُقْسِمُ بِيَوْمِ الْقِيٰمَةِۙ

l*aa* uqsimu biyawmi **a**lqiy*aa*mat**i**

Aku bersumpah dengan hari Kiamat,

75:2

# وَلَآ اُقْسِمُ بِالنَّفْسِ اللَّوَّامَةِ

wal*aa* uqsimu bi**al**nnafsi **al**laww*aa*mat**i**

dan aku bersumpah demi jiwa yang selalu menyesali (dirinya sendiri).

75:3

# اَيَحْسَبُ الْاِنْسَانُ اَلَّنْ نَّجْمَعَ عِظَامَهٗ ۗ

aya*h*sabu **a**l-ins*aa*nu **al**lan najma'a 'i*zhaa*mah**u**

Apakah manusia mengira bahwa Kami tidak akan mengumpulkan (kembali) tulang-belulangnya?

75:4

# بَلٰى قَادِرِيْنَ عَلٰٓى اَنْ نُّسَوِّيَ بَنَانَهٗ

bal*aa* q*aa*diriina 'al*aa* an nusawwiya ban*aa*nah**u**

(Bahkan) Kami mampu menyusun (kembali) jari-jemarinya dengan sempurna.

75:5

# بَلْ يُرِيْدُ الْاِنْسَانُ لِيَفْجُرَ اَمَامَهٗۚ

bal yuriidu **a**l-ins*aa*nu liyafjura am*aa*mah**u**

Tetapi manusia hendak membuat maksiat terus-menerus.

75:6

# يَسْـَٔلُ اَيَّانَ يَوْمُ الْقِيٰمَةِۗ

yas-alu ayy*aa*na yawmu **a**lqiy*aa*ma**ti**

Dia bertanya, “Kapankah hari Kiamat itu?”

75:7

# فَاِذَا بَرِقَ الْبَصَرُۙ

fa-i*dzaa* bariqa **a**lba*sh*ar**u**

Maka apabila mata terbelalak (ketakutan),

75:8

# وَخَسَفَ الْقَمَرُۙ

wakhasafa **a**lqamar**u**

dan bulan pun telah hilang cahayanya,

75:9

# وَجُمِعَ الشَّمْسُ وَالْقَمَرُۙ

wajumi'a **al**sysyamsu wa**a**lqamar**u**

lalu matahari dan bulan dikumpulkan,

75:10

# يَقُوْلُ الْاِنْسَانُ يَوْمَىِٕذٍ اَيْنَ الْمَفَرُّۚ

yaquulu **a**l-ins*aa*nu yawma-i*dz*in ayna **a**lmafar**ru**

pada hari itu manusia berkata, “Ke mana tempat lari?”

75:11

# كَلَّا لَا وَزَرَۗ

kall*aa* l*aa* wazar**a**

Tidak! Tidak ada tempat berlindung!

75:12

# اِلٰى رَبِّكَ يَوْمَىِٕذِ ِۨالْمُسْتَقَرُّۗ

il*aa* rabbika yawma-i*dz*in **a**lmustaqar**ru**

Hanya kepada Tuhanmu tempat kembali pada hari itu.

75:13

# يُنَبَّؤُا الْاِنْسَانُ يَوْمَىِٕذٍۢ بِمَا قَدَّمَ وَاَخَّرَۗ

yunabbau **a**l-ins*aa*nu yawma-i*dz*in bim*aa* qaddama wa-akhkhar**a**

Pada hari itu diberitakan kepada manusia apa yang telah dikerjakannya dan apa yang dilalaikannya.

75:14

# بَلِ الْاِنْسَانُ عَلٰى نَفْسِهٖ بَصِيْرَةٌۙ

bali **a**l-ins*aa*nu 'al*aa* nafsihi ba*sh*iira**tun**

Bahkan manusia menjadi saksi atas dirinya sendiri,

75:15

# وَّلَوْ اَلْقٰى مَعَاذِيْرَهٗۗ

walaw **a**lq*aa* ma'*aadz*iirah**u**

dan meskipun dia mengemukakan alasan-alasannya.

75:16

# لَا تُحَرِّكْ بِهٖ لِسَانَكَ لِتَعْجَلَ بِهٖۗ

l*aa* tu*h*arrik bihi lis*aa*naka lita'jala bih**i**

Jangan engkau (Muhammad) gerakkan lidahmu (untuk membaca Al-Qur'an) karena hendak cepat-cepat (menguasai)nya.

75:17

# اِنَّ عَلَيْنَا جَمْعَهٗ وَقُرْاٰنَهٗ ۚ

inna 'alayn*aa* jam'ahu waqur-*aa*nah**u**

Sesungguhnya Kami yang akan mengumpulkannya (di dadamu) dan membacakannya.

75:18

# فَاِذَا قَرَأْنٰهُ فَاتَّبِعْ قُرْاٰنَهٗ ۚ

fa-i*dzaa* qara/n*aa*hu fa**i**ttabi' qur-*aa*nah**u**

Apabila Kami telah selesai membacakannya maka ikutilah bacaannya itu.

75:19

# ثُمَّ اِنَّ عَلَيْنَا بَيَانَهٗ ۗ

tsumma inna 'alayn*aa* bay*aa*nah**u**

Kemudian sesungguhnya Kami yang akan menjelaskannya.

75:20

# كَلَّا بَلْ تُحِبُّوْنَ الْعَاجِلَةَۙ

kall*aa* bal tu*h*ibbuuna **a**l'*aa*jila**ta**

Tidak! Bahkan kamu mencintai kehidupan dunia,

75:21

# وَتَذَرُوْنَ الْاٰخِرَةَۗ

wata*dz*aruuna **a**l-*aa*khira**ta**

dan mengabaikan (kehidupan) akhirat.

75:22

# وُجُوْهٌ يَّوْمَىِٕذٍ نَّاضِرَةٌۙ

wujuuhun yawma-i*dz*in n*aad*ira**tun**

Wajah-wajah (orang mukmin) pada hari itu berseri-seri,

75:23

# اِلٰى رَبِّهَا نَاظِرَةٌ ۚ

il*aa* rabbih*aa* n*aats*ira**tun**

memandang Tuhannya.

75:24

# وَوُجُوْهٌ يَّوْمَىِٕذٍۢ بَاسِرَةٌۙ

wawujuuhun yawma-i*dz*in b*aa*sira**tun**

Dan wajah-wajah (orang kafir) pada hari itu muram,

75:25

# تَظُنُّ اَنْ يُّفْعَلَ بِهَا فَاقِرَةٌ ۗ

ta*zh*unnu an yuf'ala bih*aa* f*aa*qira**tun**

mereka yakin bahwa akan ditimpakan kepadanya malapetaka yang sangat dahsyat.

75:26

# كَلَّآ اِذَا بَلَغَتِ التَّرَاقِيَۙ

kall*aa* i*dzaa* balaghati **al**ttar*aa*qiy**a**

Tidak! Apabila (nyawa) telah sampai ke kerongkongan,

75:27

# وَقِيْلَ مَنْ ۜرَاقٍۙ

waqiila man r*aa*q**in**

dan dikatakan (kepadanya), “Siapa yang dapat menyembuhkan?”

75:28

# وَّظَنَّ اَنَّهُ الْفِرَاقُۙ

wa*zh*anna annahu **a**lfir*aa*q**u**

Dan dia yakin bahwa itulah waktu perpisahan (dengan dunia),

75:29

# وَالْتَفَّتِ السَّاقُ بِالسَّاقِۙ

wa**i**ltaffati **al**ss*aa*qu bi**a**lss*aa*q**i**

dan bertaut betis (kiri) dengan betis (kanan),

75:30

# اِلٰى رَبِّكَ يَوْمَىِٕذِ ِۨالْمَسَاقُ ۗ ࣖ

il*aa* rabbika yawma-i*dz*in **a**lmas*aa*q**u**

kepada Tuhanmulah pada hari itu kamu dihalau.

75:31

# فَلَا صَدَّقَ وَلَا صَلّٰىۙ

fal*aa* *sh*addaqa wal*aa* *sh*all*aa*

Karena dia (dahulu) tidak mau membenarkan (Al-Qur'an dan Rasul) dan tidak mau melaksanakan salat,

75:32

# وَلٰكِنْ كَذَّبَ وَتَوَلّٰىۙ

wal*aa*kin ka*dzdz*aba watawall*aa*

tetapi justru dia mendustakan (Rasul) dan berpaling (dari kebenaran),

75:33

# ثُمَّ ذَهَبَ اِلٰٓى اَهْلِهٖ يَتَمَطّٰىۗ

tsumma *dz*ahaba il*aa* ahlihi yatama*ththaa*

kemudian dia pergi kepada keluarganya dengan sombong.

75:34

# اَوْلٰى لَكَ فَاَوْلٰىۙ

awl*aa* laka fa-awl*aa*

Celakalah kamu! Maka celakalah!

75:35

# ثُمَّ اَوْلٰى لَكَ فَاَوْلٰىۗ

tsumma awl*aa* laka fa-awl*aa*

Sekali lagi, celakalah kamu (manusia)! Maka celakalah!

75:36

# اَيَحْسَبُ الْاِنْسَانُ اَنْ يُّتْرَكَ سُدًىۗ

aya*h*sabu **a**l-ins*aa*nu an yutraka sud*aa***n**

Apakah manusia mengira, dia akan dibiarkan begitu saja (tanpa pertanggungjawaban)?

75:37

# اَلَمْ يَكُ نُطْفَةً مِّنْ مَّنِيٍّ يُّمْنٰى

alam yaku nu*th*fatan min manayyin yumn*aa*

Bukankah dia mulanya hanya setetes mani yang ditumpahkan (ke dalam rahim),

75:38

# ثُمَّ كَانَ عَلَقَةً فَخَلَقَ فَسَوّٰىۙ

tsumma k*aa*na 'alaqatan fakhalaqa fasaww*aa*

kemudian (mani itu) menjadi sesuatu yang melekat, lalu Allah menciptakannya dan menyempurnakannya,

75:39

# فَجَعَلَ مِنْهُ الزَّوْجَيْنِ الذَّكَرَ وَالْاُنْثٰىۗ

faja'ala minhu **al**zzawjayni **al***dzdz*akara wa**a**l-unts*aa*

lalu Dia menjadikan darinya sepasang laki-laki dan perempuan.

75:40

# اَلَيْسَ ذٰلِكَ بِقٰدِرٍ عَلٰٓى اَنْ يُّحْيِ َۧ الْمَوْتٰى ࣖ

alaysa *dzaa*lika biq*aa*dirin 'al*aa* an yu*h*yiya **a**lmawt*aa*

Bukankah (Allah yang berbuat) demikian berkuasa (pula) menghidupkan orang mati?

<!--EndFragment-->