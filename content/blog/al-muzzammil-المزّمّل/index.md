---
title: (73) Al-Muzzammil - المزّمّل
date: 2021-10-27T04:06:58.605Z
ayat: 73
description: "Jumlah Ayat: 20 / Arti: Orang Yang Berselimut"
---
<!--StartFragment-->

73:1

# يٰٓاَيُّهَا الْمُزَّمِّلُۙ

y*aa* ayyuh*aa* **a**lmuzzammil**u**

Wahai orang yang berselimut (Muhammad)!

73:2

# قُمِ الَّيْلَ اِلَّا قَلِيْلًاۙ

qumi **al**layla ill*aa* qaliil*aa**\*n**

Bangunlah (untuk salat) pada malam hari, kecuali sebagian kecil,

73:3

# نِّصْفَهٗٓ اَوِ انْقُصْ مِنْهُ قَلِيْلًاۙ

ni*sh*fahu awi unqu*sh* minhu qaliil*aa**\*n**

(yaitu) separuhnya atau kurang sedikit dari itu,

73:4

# اَوْ زِدْ عَلَيْهِ وَرَتِّلِ الْقُرْاٰنَ تَرْتِيْلًاۗ

aw zid 'alayhi warattili **a**lqur-*aa*na tartiil*aa**\*n**

atau lebih dari (seperdua) itu, dan bacalah Al-Qur'an itu dengan perlahan-lahan.

73:5

# اِنَّا سَنُلْقِيْ عَلَيْكَ قَوْلًا ثَقِيْلًا

inn*aa* sanulqii 'alayka qawlan tsaqiil*aa**\*n**

Sesungguhnya Kami akan menurunkan perkataan yang berat kepadamu.

73:6

# اِنَّ نَاشِئَةَ الَّيْلِ هِيَ اَشَدُّ وَطْـًٔا وَّاَقْوَمُ قِيْلًاۗ

inna n*aa*syi-ata **al**layli hiya asyaddu wa*th*-an wa-aqwamu qiil*aa**\*n**

Sungguh, bangun malam itu lebih kuat (mengisi jiwa); dan (bacaan pada waktu itu) lebih berkesan.

73:7

# اِنَّ لَكَ فِى النَّهَارِ سَبْحًا طَوِيْلًاۗ

inna laka fii **al**nnah*aa*ri sab*h*an *th*awiil*aa**\*n**

Sesungguhnya pada siang hari engkau sangat sibuk dengan urusan-urusan yang panjang.

73:8

# وَاذْكُرِ اسْمَ رَبِّكَ وَتَبَتَّلْ اِلَيْهِ تَبْتِيْلًاۗ

wa**u***dz*kuri isma rabbika watabattal ilayhi tabtiil*aa**\*n**

Dan sebutlah nama Tuhanmu, dan beribadahlah kepada-Nya dengan sepenuh hati.

73:9

# رَبُّ الْمَشْرِقِ وَالْمَغْرِبِ لَآ اِلٰهَ اِلَّا هُوَ فَاتَّخِذْهُ وَكِيْلًا

rabbu **a**lmasyriqi wa**a**lmaghribi l*aa* il*aa*ha ill*aa* huwa fa**i**ttakhi*dz*hu wakiil*aa**\*n**

(Dialah) Tuhan timur dan barat, tidak ada tuhan selain Dia, maka jadikanlah Dia sebagai pelindung.

73:10

# وَاصْبِرْ عَلٰى مَا يَقُوْلُوْنَ وَاهْجُرْهُمْ هَجْرًا جَمِيْلًا

wa**i***sh*bir 'al*aa *m*aa *yaquuluuna wa**u**hjurhum hajran jamiil*aa**\*n**

Dan bersabarlah (Muhammad) terhadap apa yang mereka katakan dan tinggalkanlah mereka dengan cara yang baik.

73:11

# وَذَرْنِيْ وَالْمُكَذِّبِيْنَ اُولِى النَّعْمَةِ وَمَهِّلْهُمْ قَلِيْلًا

wa*dz*arnii wa**a**lmuka*dzdz*ibiina ulii **al**nna'mati wamahhilhum qaliil*aa**\*n**

Dan biarkanlah Aku (yang bertindak) terhadap orang-orang yang mendustakan, yang memiliki segala kenikmatan hidup, dan berilah mereka penangguhan sebentar.

73:12

# اِنَّ لَدَيْنَآ اَنْكَالًا وَّجَحِيْمًاۙ

inna ladayn*aa* ank*aa*lan waja*h*iim*aa**\*n**

Sungguh, di sisi Kami ada belenggu-belenggu (yang berat) dan neraka yang menyala-nyala,

73:13

# وَّطَعَامًا ذَا غُصَّةٍ وَّعَذَابًا اَلِيْمًا

wa*th*a'*aa*man *dzaa* ghu*shsh*atin wa'a*dzaa*ban **a**liim*aa**\*n**

dan (ada) makanan yang menyumbat di kerongkongan dan azab yang pedih.

73:14

# يَوْمَ تَرْجُفُ الْاَرْضُ وَالْجِبَالُ وَكَانَتِ الْجِبَالُ كَثِيْبًا مَّهِيْلًا

yawma tarjufu **a**l-ar*dh*u wa**a**ljib*aa*lu wak*aa*nati **a**ljib*aa*lu katsiiban mahiil*aa**\*n**

(Ingatlah) pada hari (ketika) bumi dan gunung-gunung berguncang keras, dan menjadilah gunung-gunung itu seperti onggokan pasir yang dicurahkan.

73:15

# اِنَّآ اَرْسَلْنَآ اِلَيْكُمْ رَسُوْلًا ەۙ شَاهِدًا عَلَيْكُمْ كَمَآ اَرْسَلْنَآ اِلٰى فِرْعَوْنَ رَسُوْلًا ۗ

inn*aa* arsaln*aa* ilaykum rasuulan sy*aa*hidan 'alaykum kam*aa* arsaln*aa* il*aa* fir'awna rasuul*aa**\*n**

Sesungguhnya Kami telah mengutus seorang Rasul (Muhammad) kepada kamu, yang menjadi saksi terhadapmu, sebagaimana Kami telah mengutus seorang Rasul kepada Fir‘aun.

73:16

# فَعَصٰى فِرْعَوْنُ الرَّسُوْلَ فَاَخَذْنٰهُ اَخْذًا وَّبِيْلًاۚ

fa'a*shaa* fir'awnu **al**rrasuula fa-akha*dz*n*aa*hu akh*dz*an wabiil*aa**\*n**

Namun Fir‘aun mendurhakai Rasul itu, maka Kami siksa dia dengan siksaan yang berat.

73:17

# فَكَيْفَ تَتَّقُوْنَ اِنْ كَفَرْتُمْ يَوْمًا يَّجْعَلُ الْوِلْدَانَ شِيْبًاۖ

fakayfa tattaquuna in kafartum yawman yaj'alu **a**lwild*aa*na syiib*aa**\*n**

Lalu bagaimanakah kamu akan dapat menjaga dirimu jika kamu tetap kafir kepada hari yang menjadikan anak-anak beruban.

73:18

# ۨالسَّمَاۤءُ مُنْفَطِرٌۢ بِهٖۗ كَانَ وَعْدُهٗ مَفْعُوْلًا

a**l**ssam*aa*u munfa*th*irun bihi k*aa*na wa'duhu maf'uul*aa**\*n**

Langit terbelah pada hari itu. Janji Allah pasti terlaksana.

73:19

# اِنَّ هٰذِهٖ تَذْكِرَةٌ ۚ فَمَنْ شَاۤءَ اتَّخَذَ اِلٰى رَبِّهٖ سَبِيْلًا ࣖ

inna h*aadz*ihi ta*dz*kiratun faman sy*aa*-a ittakha*dz*a il*aa* rabbihi sabiil*aa**\*n**

Sungguh, ini adalah peringatan. Barangsiapa menghendaki, niscaya dia mengambil jalan (yang lurus) kepada Tuhannya.

73:20

# ۞ اِنَّ رَبَّكَ يَعْلَمُ اَنَّكَ تَقُوْمُ اَدْنٰى مِنْ ثُلُثَيِ الَّيْلِ وَنِصْفَهٗ وَثُلُثَهٗ وَطَاۤىِٕفَةٌ مِّنَ الَّذِيْنَ مَعَكَۗ وَاللّٰهُ يُقَدِّرُ الَّيْلَ وَالنَّهَارَۗ عَلِمَ اَنْ لَّنْ تُحْصُوْهُ فَتَابَ عَلَيْكُمْ فَاقْرَءُوْا مَا تَيَسَّرَ مِن

inna rabbaka ya’lamu annaka taqụmu adnā min ṡuluṡayil-laili wa niṣfahụ wa ṡuluṡahụ wa ṭā\`ifatum minallażīna ma’ak, wallāhu yuqaddirul-laila wan-nahār, ‘alima al lan tuḥṣụhu fa tāba ‘alaikum faqra\`ụ mā tayassara minal-qur\`ān, ‘alima an sayakụnu mingkum marḍā wa ākharụna yaḍribụna fil-arḍi yabtagụna min faḍlillāhi wa ākharụna yuqātilụna fī sabīlillāhi faqra\`ụ mā tayassara min-hu wa aqīmuṣ-ṣalāta wa ātuz-zakāta wa aqriḍullāha qarḍan ḥasanā, wa mā tuqaddimụ li`anfusikum min khairin tajidụhu ‘indallāhi huwa khairaw wa a’ẓama ajrā, wastagfirullāh, innallāha gafụrur raḥīm

Sesungguhnya Tuhanmu mengetahui bahwasanya kamu berdiri (sembahyang) kurang dari dua pertiga malam, atau seperdua malam atau sepertiganya dan (demikian pula) segolongan dari orang-orang yang bersama kamu. Dan Allah menetapkan ukuran malam dan siang. Allah mengetahui bahwa kamu sekali-kali tidak dapat menentukan batas-batas waktu-waktu itu, maka Dia memberi keringanan kepadamu, karena itu bacalah apa yang mudah (bagimu) dari Al Quran. Dia mengetahui bahwa akan ada di antara kamu orang-orang yang sakit dan orang-orang yang berjalan di muka bumi mencari sebagian karunia Allah; dan orang-orang yang lain lagi berperang di jalan Allah, maka bacalah apa yang mudah (bagimu) dari Al Quran dan dirikanlah sembahyang, tunaikanlah zakat dan berikanlah pinjaman kepada Allah pinjaman yang baik. Dan kebaikan apa saja yang kamu perbuat untuk dirimu niscaya kamu memperoleh (balasan)nya di sisi Allah sebagai balasan yang paling baik dan yang paling besar pahalanya. Dan mohonlah ampunan kepada Allah; sesungguhnya Allah Maha Pengampun lagi Maha Penyayang.

<!--EndFragment-->