---
title: (112) Al-Ikhlas - الاخلاص
date: 2021-10-27T04:19:52.891Z
ayat: 112
description: "Jumlah Ayat: 4 / Arti: Ikhlas"
---
<!--StartFragment-->

112:1

# قُلْ هُوَ اللّٰهُ اَحَدٌۚ

qul huwa **al**l*aa*hu a*h*ad**un**

Katakanlah (Muhammad), “Dialah Allah, Yang Maha Esa.

112:2

# اَللّٰهُ الصَّمَدُۚ

**al**l*aa*hu **al***shsh*amad**u**

Allah tempat meminta segala sesuatu.

112:3

# لَمْ يَلِدْ وَلَمْ يُوْلَدْۙ

lam yalid walam yuulad**u**

(Allah) tidak beranak dan tidak pula diperanakkan.

112:4

# وَلَمْ يَكُنْ لَّهٗ كُفُوًا اَحَدٌ ࣖ

walam yakun lahu kufuwan a*h*ad**un**

Dan tidak ada sesuatu yang setara dengan Dia.”

<!--EndFragment-->