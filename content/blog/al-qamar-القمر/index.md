---
title: (54) Al-Qamar - القمر
date: 2021-10-27T04:12:28.737Z
ayat: 54
description: "Jumlah Ayat: 55 / Arti: Bulan"
---
<!--StartFragment-->

54:1

# اِقْتَرَبَتِ السَّاعَةُ وَانْشَقَّ الْقَمَرُ

iqtarabati **al**ss*aa*'atu wa**i**nsyaqqa **a**lqamar**u**

Saat (hari Kiamat) semakin dekat, bulan pun terbelah.

54:2

# وَاِنْ يَّرَوْا اٰيَةً يُّعْرِضُوْا وَيَقُوْلُوْا سِحْرٌ مُّسْتَمِرٌّ

wa-in yaraw *aa*yatan yu'ri*dh*uu wayaquuluu si*h*run mustamirr**un**

Dan jika mereka (orang-orang musyrikin) melihat suatu tanda (mukjizat), mereka berpaling dan berkata, “(Ini adalah) sihir yang terus menerus.”

54:3

# وَكَذَّبُوْا وَاتَّبَعُوْٓا اَهْوَاۤءَهُمْ وَكُلُّ اَمْرٍ مُّسْتَقِرٌّ

waka*dzdz*abuu wa**i**ttaba'uu ahw*aa*-ahum wakullu amrin mustaqirr**un**

Dan mereka mendustakan (Muhammad) dan mengikuti keinginannya, padahal setiap urusan telah ada ketetapannya.

54:4

# وَلَقَدْ جَاۤءَهُمْ مِّنَ الْاَنْبَاۤءِ مَا فِيْهِ مُزْدَجَرٌۙ

walaqad j*aa*-ahum mina **a**l-anb*aa*-i m*aa* fiihi muzdajar**un**

Dan sungguh, telah datang kepada mereka beberapa kisah yang di dalamnya terdapat ancaman (terhadap kekafiran),

54:5

# حِكْمَةٌ ۢ بَالِغَةٌ فَمَا تُغْنِ النُّذُرُۙ

*h*ikmatun b*aa*lighatun fam*aa* tughnii **al**nnu*dz*ur**u**

(itulah) suatu hikmah yang sempurna, tetapi peringatan-peringatan itu tidak berguna (bagi mereka),

54:6

# فَتَوَلَّ عَنْهُمْ ۘ يَوْمَ يَدْعُ الدَّاعِ اِلٰى شَيْءٍ نُّكُرٍۙ

fatawalla 'anhum yawma yad'u **al**dd*aa*'i il*aa* syay-in nukur**in**

maka berpalinglah engkau (Muhammad) dari mereka pada hari (ketika) penyeru (malaikat) mengajak (mereka) kepada sesuatu yang tidak menyenangkan (hari pembalasan),

54:7

# خُشَّعًا اَبْصَارُهُمْ يَخْرُجُوْنَ مِنَ الْاَجْدَاثِ كَاَنَّهُمْ جَرَادٌ مُّنْتَشِرٌۙ

khusysya'an ab*shaa*ruhum yakhrujuuna mina **a**l-ajd*aa*tsi ka-annahum jar*aa*dun muntasyir**un**

pandangan mereka tertunduk, ketika mereka keluar dari kuburan, seakan-akan mereka belalang yang beterbangan,

54:8

# مُّهْطِعِيْنَ اِلَى الدَّاعِۗ يَقُوْلُ الْكٰفِرُوْنَ هٰذَا يَوْمٌ عَسِرٌ

muh*th*i'iina il*aa* **al**dd*aa*'i yaquulu **a**lk*aa*firuuna h*aadzaa* yawmun 'asir**un**

dengan patuh mereka segera datang kepada penyeru itu. Orang-orang kafir berkata, “Ini adalah hari yang sulit.”

54:9

# ۞ كَذَّبَتْ قَبْلَهُمْ قَوْمُ نُوْحٍ فَكَذَّبُوْا عَبْدَنَا وَقَالُوْا مَجْنُوْنٌ وَّازْدُجِرَ

ka*dzdz*abat qablahum qawmu nuu*h*in faka*dzdz*abuu 'abdan*aa* waq*aa*luu majnuunun wa**i**zdujir**a**

Sebelum mereka, kaum Nuh juga telah mendustakan (rasul), maka mereka mendustakan hamba Kami (Nuh) dan mengatakan, “Dia orang gila!” Lalu diusirnya dengan ancaman.

54:10

# فَدَعَا رَبَّهٗٓ اَنِّيْ مَغْلُوْبٌ فَانْتَصِرْ

fada'*aa* rabbahu annii maghluubun fa**i**nta*sh*ir

Maka dia (Nuh) mengadu kepada Tuhannya, “Sesungguhnya aku telah dikalahkan, maka tolonglah (aku).”

54:11

# فَفَتَحْنَآ اَبْوَابَ السَّمَاۤءِ بِمَاۤءٍ مُّنْهَمِرٍۖ

fafata*h*n*aa* abw*aa*ba **al**ssam*aa*-i bim*aa*-in munhamir**in**

Lalu Kami bukakan pintu-pintu langit dengan (menurunkan) air yang tercurah,

54:12

# وَّفَجَّرْنَا الْاَرْضَ عُيُوْنًا فَالْتَقَى الْمَاۤءُ عَلٰٓى اَمْرٍ قَدْ قُدِرَ ۚ

wafajjarn*aa* **a**l-ar*dh*a 'uyuunan fa**i**ltaq*aa* **a**lm*aa*u 'al*aa* amrin qad qudir**a**

dan Kami jadikan bumi menyemburkan mata-mata air maka bertemulah (air-air) itu sehingga (meluap menimbulkan) keadaan (bencana) yang telah ditetapkan.

54:13

# وَحَمَلْنٰهُ عَلٰى ذَاتِ اَلْوَاحٍ وَّدُسُرٍۙ

wa*h*amaln*aa*hu 'al*aa* *dzaa*ti **a**lw*aah*in wadusur**in**

Dan Kami angkut dia (Nuh) ke atas (kapal) yang terbuat dari papan dan pasak,

54:14

# تَجْرِيْ بِاَعْيُنِنَاۚ جَزَاۤءً لِّمَنْ كَانَ كُفِرَ

tajrii bi-a'yunin*aa* jaz*aa*-an liman k*aa*na kufir**a**

yang berlayar dengan pemeliharaan (pengawasan) Kami sebagai balasan bagi orang yang telah diingkari (kaumnya).

54:15

# وَلَقَدْ تَّرَكْنٰهَآ اٰيَةً فَهَلْ مِنْ مُّدَّكِرٍ

walaqad tarakn*aa*h*aa* *aa*yatan fahal min muddakir**in**

Dan sungguh, kapal itu telah Kami jadikan sebagai tanda (pelajaran). Maka adakah orang yang mau mengambil pelajaran?

54:16

# فَكَيْفَ كَانَ عَذَابِيْ وَنُذُرِ

fakayfa k*aa*na 'a*dzaa*bii wanu*dz*ur**i**

Maka betapa dahsyatnya azab-Ku dan peringatan-Ku!

54:17

# وَلَقَدْ يَسَّرْنَا الْقُرْاٰنَ لِلذِّكْرِ فَهَلْ مِنْ مُّدَّكِرٍ

walaqad yassarn*aa* **a**lqur-*aa*na li**l***dzdz*ikri fahal min muddakir**in**

Dan sungguh, telah Kami mudahkan Al-Qur'an untuk peringatan, maka adakah orang yang mau mengambil pelajaran?

54:18

# كَذَّبَتْ عَادٌ فَكَيْفَ كَانَ عَذَابِيْ وَنُذُرِ

ka*dzdz*abat '*aa*dun fakayfa k*aa*na 'a*dzaa*bii wanu*dz*ur**i**

Kaum ‘Ad pun telah mendustakan. Maka betapa dahsyatnya azab-Ku dan peringatan-Ku!

54:19

# اِنَّآ اَرْسَلْنَا عَلَيْهِمْ رِيْحًا صَرْصَرًا فِيْ يَوْمِ نَحْسٍ مُّسْتَمِرٍّۙ

inn*aa* arsaln*aa* 'alayhim rii*h*an *sh*ar*sh*aran fii yawmi na*h*sin mustamirr**in**

Sesungguhnya Kami telah menghembuskan angin yang sangat kencang kepada mereka pada hari nahas yang terus menerus,

54:20

# تَنْزِعُ النَّاسَۙ كَاَنَّهُمْ اَعْجَازُ نَخْلٍ مُّنْقَعِرٍ

tanzi'u **al**nn*aa*sa ka-annahum a'j*aa*zu nakhlin munqa'ir**in**

yang membuat manusia bergelimpangan, mereka bagaikan pohon-pohon kurma yang tumbang dengan akar-akarnya.

54:21

# فَكَيْفَ كَانَ عَذَابِيْ وَنُذُرِ

fakayfa k*aa*na 'a*dzaa*bii wanu*dz*ur**i**

Maka betapa dahsyatnya azab-Ku dan peringatan-Ku!

54:22

# وَلَقَدْ يَسَّرْنَا الْقُرْاٰنَ لِلذِّكْرِ فَهَلْ مِنْ مُّدَّكِرٍ ࣖ

walaqad yassarn*aa* **a**lqur-*aa*na li**l***dzdz*ikri fahal min muddakir**in**

Dan sungguh, telah Kami mudahkan Al-Qur'an untuk peringatan, maka adakah orang yang mau mengambil pelajaran?

54:23

# كَذَّبَتْ ثَمُوْدُ بِالنُّذُرِ

ka*dzdz*abat tsamuudu bi**al**nnu*dz*ur**i**

Kaum Samud pun telah mendustakan peringatan itu.

54:24

# فَقَالُوْٓا اَبَشَرًا مِّنَّا وَاحِدًا نَّتَّبِعُهٗٓ ۙاِنَّآ اِذًا لَّفِيْ ضَلٰلٍ وَّسُعُرٍ

faq*aa*luu abasyaran minn*aa* w*aah*idan nattabi'uhu inn*aa* i*dz*an lafii *dh*al*aa*lin wasu'ur**in**

Maka mereka berkata, “Bagaimana kita akan mengikuti seorang manusia (biasa) di antara kita? Sungguh, kalau begitu kita benar-benar telah sesat dan gila.

54:25

# ءَاُلْقِيَ الذِّكْرُ عَلَيْهِ مِنْۢ بَيْنِنَا بَلْ هُوَ كَذَّابٌ اَشِرٌ

aulqiya **al***dzdz*ikru 'alayhi min baynin*aa* bal huwa ka*dzdzaa*bun asyir**un**

Apakah wahyu itu diturunkan kepadanya di antara kita? Pastilah dia (Saleh) seorang yang sangat pendusta (dan) sombong.”

54:26

# سَيَعْلَمُوْنَ غَدًا مَّنِ الْكَذَّابُ الْاَشِرُ

saya'lamuuna ghadan mani **a**lka*dzdzaa*bu **a**l-asyir**u**

Kelak mereka akan mengetahui siapa yang sebenarnya sangat pendusta (dan) sombong itu.

54:27

# اِنَّا مُرْسِلُوا النَّاقَةِ فِتْنَةً لَّهُمْ فَارْتَقِبْهُمْ وَاصْطَبِرْۖ

inn*aa* mursiluu **al**nn*aa*qati fitnatan lahum fa**i**rtaqibhum wa**i***sth*abir

Sesungguhnya Kami akan mengirimkan unta betina sebagai cobaan bagi mereka, maka tunggulah mereka dan bersabarlah (Saleh).

54:28

# وَنَبِّئْهُمْ اَنَّ الْمَاۤءَ قِسْمَةٌ ۢ بَيْنَهُمْۚ كُلُّ شِرْبٍ مُّحْتَضَرٌ

wanabbi/hum anna **a**lm*aa*-a qismatun baynahum kullu syirbin mu*h*ta*dh*ar**un**

Dan beritahukanlah kepada mereka bahwa air itu dibagi di antara mereka (dengan unta betina itu); setiap orang berhak mendapat giliran minum.

54:29

# فَنَادَوْا صَاحِبَهُمْ فَتَعَاطٰى فَعَقَرَ

fan*aa*daw *shaah*ibahum fata'*aataa* fa'aqar**a**

Maka mereka memanggil kawannya, lalu dia menangkap (unta itu) dan memotongnya.

54:30

# فَكَيْفَ كَانَ عَذَابِيْ وَنُذُرِ

fakayfa k*aa*na 'a*dzaa*bii wanu*dz*ur**i**

Maka betapa dahsyatnya azab-Ku dan peringatan-Ku!

54:31

# اِنَّآ اَرْسَلْنَا عَلَيْهِمْ صَيْحَةً وَّاحِدَةً فَكَانُوْا كَهَشِيْمِ الْمُحْتَظِرِ

inn*aa* arsaln*aa* 'alayhim *sh*ay*h*atan w*aah*idatan fak*aa*nuu kahasyiimi **a**lmu*h*ta*zh*ir**i**

Kami kirimkan atas mereka satu suara yang keras mengguntur, maka jadilah mereka seperti batang-batang kering yang lapuk.

54:32

# وَلَقَدْ يَسَّرْنَا الْقُرْاٰنَ لِلذِّكْرِ فَهَلْ مِنْ مُّدَّكِرٍ

walaqad yassarn*aa* **a**lqur-*aa*na li**l***dzdz*ikri fahal min muddakir**in**

Dan sungguh, telah Kami mudahkan Al-Qur'an untuk peringatan, maka adakah orang yang mau mengambil pelajaran?

54:33

# كَذَّبَتْ قَوْمُ لُوْطٍ ۢبِالنُّذُرِ

ka*dzdz*abat qawmu luu*th*in bi**al**nnu*dz*ur**i**

Kaum Lut pun telah mendustakan peringatan itu.

54:34

# اِنَّآ اَرْسَلْنَا عَلَيْهِمْ حَاصِبًا اِلَّآ اٰلَ لُوْطٍ ۗنَجَّيْنٰهُمْ بِسَحَرٍۙ

inn*aa* arsaln*aa* 'alayhim *has*iban ill*aa* *aa*la luu*th*in najjayn*aa*hum bisa*h*ar**in**

Sesungguhnya Kami kirimkan kepada mereka badai yang membawa batu-batu (yang menimpa mereka), kecuali keluarga Lut. Kami selamatkan mereka sebelum fajar menyingsing,

54:35

# نِّعْمَةً مِّنْ عِنْدِنَاۗ كَذٰلِكَ نَجْزِيْ مَنْ شَكَرَ

ni'matan min 'indin*aa* ka*dzaa*lika najzii man syakar**a**

sebagai nikmat dari Kami. Demikianlah Kami memberi balasan kepada orang-orang yang bersyukur.

54:36

# وَلَقَدْ اَنْذَرَهُمْ بَطْشَتَنَا فَتَمَارَوْا بِالنُّذُرِ

walaqad an*dz*arahum ba*th*syatan*aa* fatam*aa*raw bi**al**nnu*dz*ur**i**

Dan sungguh, dia (Lut) telah memperingatkan mereka akan hukuman Kami, tetapi mereka mendustakan peringatan-Ku.

54:37

# وَلَقَدْ رَاوَدُوْهُ عَنْ ضَيْفِهٖ فَطَمَسْنَآ اَعْيُنَهُمْ فَذُوْقُوْا عَذَابِيْ وَنُذُرِ

walaqad r*aa*waduuhu 'an *dh*ayfihi fa*th*amasn*aa* a'yunahum fa*dz*uuquu 'a*dzaa*bii wanu*dz*ur**i**

Dan sungguh, mereka telah membujuknya (agar menyerahkan) tamunya (kepada mereka), lalu Kami butakan mata mereka, maka rasakanlah azab-Ku dan peringatan-Ku!

54:38

# وَلَقَدْ صَبَّحَهُمْ بُكْرَةً عَذَابٌ مُّسْتَقِرٌّۚ

walaqad *sh*abba*h*ahum bukratan 'a*dzaa*bun mustaqirr**un**

Dan sungguh, pada esok harinya mereka benar-benar ditimpa azab yang tetap.

54:39

# فَذُوْقُوْا عَذَابِيْ وَنُذُرِ

fa*dz*uuquu 'a*dzaa*bii wanu*dz*ur**i**

Maka rasakanlah azab-Ku dan peringatan-Ku!

54:40

# وَلَقَدْ يَسَّرْنَا الْقُرْاٰنَ لِلذِّكْرِ فَهَلْ مِنْ مُّدَّكِرٍ ࣖ

walaqad yassarn*aa* **a**lqur-*aa*na li**l***dzdz*ikri fahal min muddakir**in**

Dan sungguh, telah Kami mudahkan Al-Qur'an untuk peringatan, maka adakah orang yang mau mengambil pelajaran?

54:41

# وَلَقَدْ جَاۤءَ اٰلَ فِرْعَوْنَ النُّذُرُۚ

walaqad j*aa*-a *aa*la fir'awna **al**nnu*dz*ur**u**

Dan sungguh, peringatan telah datang kepada keluarga Fir‘aun.

54:42

# كَذَّبُوْا بِاٰيٰتِنَا كُلِّهَا فَاَخَذْنٰهُمْ اَخْذَ عَزِيْزٍ مُّقْتَدِرٍ

ka*dzdz*abuu bi-*aa*y*aa*tin*aa* kullih*aa* fa-akha*dz*n*aa*hum akh*dz*a 'aziizin muqtadir**in**

Mereka mendustakan mukjizat-mukjizat Kami semuanya, maka Kami azab mereka dengan azab dari Yang Mahaperkasa, Mahakuasa.

54:43

# اَكُفَّارُكُمْ خَيْرٌ مِّنْ اُولٰۤىِٕكُمْ اَمْ لَكُمْ بَرَاۤءَةٌ فِى الزُّبُرِۚ

akuff*aa*rukum khayrun min ul*aa*-ikum am lakum bar*aa*-atun fii **al**zzubur**i**

Apakah orang-orang kafir di lingkunganmu (kaum musyrikin) lebih baik dari mereka, ataukah kamu telah mempunyai jaminan kebebasan (dari azab) dalam kitab-kitab terdahulu?

54:44

# اَمْ يَقُوْلُوْنَ نَحْنُ جَمِيْعٌ مُّنْتَصِرٌ

am yaquuluuna na*h*nu jamii'un munta*sh*ir**un**

Atau mereka mengatakan, “Kami ini golongan yang bersatu yang pasti menang.”

54:45

# سَيُهْزَمُ الْجَمْعُ وَيُوَلُّوْنَ الدُّبُرَ

sayuhzamu **a**ljam'u wayuwalluuna **al**ddubur**a**

Golongan itu pasti akan dikalahkan dan mereka akan mundur ke belakang.

54:46

# بَلِ السَّاعَةُ مَوْعِدُهُمْ وَالسَّاعَةُ اَدْهٰى وَاَمَرُّ

bali **al**ss*aa*'atu maw'iduhum wa**al**ss*aa*'atu adh*aa* wa-amar**ru**

Bahkan hari Kiamat itulah hari yang dijanjikan kepada mereka dan hari Kiamat itu lebih dahsyat dan lebih pahit.

54:47

# اِنَّ الْمُجْرِمِيْنَ فِيْ ضَلٰلٍ وَّسُعُرٍۘ

inna **a**lmujrimiina fii *dh*al*aa*lin wasu'ur**in**

Sungguh, orang-orang yang berdosa berada dalam kesesatan (di dunia) dan akan berada dalam neraka (di akhirat).

54:48

# يَوْمَ يُسْحَبُوْنَ فِى النَّارِ عَلٰى وُجُوْهِهِمْۗ ذُوْقُوْا مَسَّ سَقَرَ

yawma yus*h*abuuna fii **al**nn*aa*ri 'al*aa* wujuuhihim *dz*uuquu massa saqar**a**

Pada hari mereka diseret ke neraka pada wajahnya. (Dikatakan kepada mereka), “Rasakanlah sentuhan api neraka.”

54:49

# اِنَّا كُلَّ شَيْءٍ خَلَقْنٰهُ بِقَدَرٍ

inn*aa* kulla syay-in khalaqn*aa*hu biqadar**in**

Sungguh, Kami menciptakan segala sesuatu menurut ukuran.

54:50

# وَمَآ اَمْرُنَآ اِلَّا وَاحِدَةٌ كَلَمْحٍ ۢبِالْبَصَرِ

wam*aa* amrun*aa* ill*aa* w*aah*idatun kalam*h*in bi**a**lba*sh*ar**i**

Dan perintah Kami hanyalah (dengan) satu perkataan seperti kejapan mata.

54:51

# وَلَقَدْ اَهْلَكْنَآ اَشْيَاعَكُمْ فَهَلْ مِنْ مُّدَّكِرٍ

walaqad ahlakn*aa* asyy*aa*'akum fahal min muddakir**in**

Dan sungguh, telah Kami binasakan orang yang serupa dengan kamu (kekafirannya). Maka adakah orang yang mau mengambil pelajaran?

54:52

# وَكُلُّ شَيْءٍ فَعَلُوْهُ فِى الزُّبُرِ

wakullu syay-in fa'aluuhu fii **al**zzubur**i**

Dan segala sesuatu yang telah mereka perbuat tercatat dalam buku-buku catatan.

54:53

# وَكُلُّ صَغِيْرٍ وَّكَبِيْرٍ مُّسْتَطَرٌ

wakullu *sh*aghiirin wakabiirin musta*th*ar**un**

Dan segala (sesuatu) yang kecil maupun yang besar (semuanya) tertulis.

54:54

# اِنَّ الْمُتَّقِيْنَ فِيْ جَنّٰتٍ وَّنَهَرٍۙ

inna **a**lmuttaqiina fii jann*aa*tin wanahar**in**

Sungguh, orang-orang yang bertakwa berada di taman-taman dan sungai-sungai,

54:55

# فِيْ مَقْعَدِ صِدْقٍ عِنْدَ مَلِيْكٍ مُّقْتَدِرٍ ࣖ

fii maq'adi *sh*idqin 'inda maliikin muqtadir**in**

di tempat yang disenangi di sisi Tuhan Yang Mahakuasa.

<!--EndFragment-->