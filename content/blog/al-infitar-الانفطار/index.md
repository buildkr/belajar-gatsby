---
title: (82) Al-Infitar - الانفطار
date: 2021-10-27T04:16:57.866Z
ayat: 82
description: "Jumlah Ayat: 19 / Arti: Terbelah"
---
<!--StartFragment-->

82:1

# اِذَا السَّمَاۤءُ انْفَطَرَتْۙ

i*dzaa* **al**ssam*aa*u infa*th*arath

Apabila langit terbelah,

82:2

# وَاِذَا الْكَوَاكِبُ انْتَثَرَتْۙ

wa-i*dzaa* **a**lkaw*aa*kibu intatsarath

dan apabila bintang-bintang jatuh berserakan,

82:3

# وَاِذَا الْبِحَارُ فُجِّرَتْۙ

wa-i*dzaa* **a**lbi*haa*ru fujjirath

dan apabila lautan dijadikan meluap,

82:4

# وَاِذَا الْقُبُوْرُ بُعْثِرَتْۙ

wa-i*dzaa* **a**lqubuuru bu'tsirath

dan apabila kuburan-kuburan dibongkar,

82:5

# عَلِمَتْ نَفْسٌ مَّا قَدَّمَتْ وَاَخَّرَتْۗ

'alimat nafsun m*aa* qaddamat wa-akhkharath

(maka) setiap jiwa akan mengetahui apa yang telah dikerjakan dan yang dilalaikan(nya).

82:6

# يٰٓاَيُّهَا الْاِنْسَانُ مَا غَرَّكَ بِرَبِّكَ الْكَرِيْمِۙ

y*aa* ayyuh*aa* **a**l-ins*aa*nu m*aa* gharraka birabbika **a**lkariim**i**

Wahai manusia! Apakah yang telah memperdayakan kamu (berbuat durhaka) terhadap Tuhanmu Yang Mahamulia,

82:7

# الَّذِيْ خَلَقَكَ فَسَوّٰىكَ فَعَدَلَكَۙ

**al**la*dz*ii khalaqaka fasaww*aa*ka fa'adalak**a**

yang telah menciptakanmu lalu menyempurnakan kejadianmu dan menjadikan (susunan tubuh)mu seimbang,

82:8

# فِيْٓ اَيِّ صُوْرَةٍ مَّا شَاۤءَ رَكَّبَكَۗ

fii ayyi *sh*uuratin m*aa* sy*aa*-a rakkabak**a**

dalam bentuk apa saja yang dikehendaki, Dia menyusun tubuhmu.

82:9

# كَلَّا بَلْ تُكَذِّبُوْنَ بِالدِّيْنِۙ

kall*aa* bal tuka*dzdz*ibuuna bi**al**ddiin**i**

Sekali-kali jangan begitu! Bahkan kamu mendustakan hari pembalasan.

82:10

# وَاِنَّ عَلَيْكُمْ لَحٰفِظِيْنَۙ

wa-inna 'alaykum la*haa*fi*zh*iin**a**

Dan sesungguhnya bagi kamu ada (malaikat-malaikat) yang mengawasi (pekerjaanmu),

82:11

# كِرَامًا كَاتِبِيْنَۙ

kir*aa*man k*aa*tibiin**a**

yang mulia (di sisi Allah) dan yang mencatat (amal perbuatanmu),

82:12

# يَعْلَمُوْنَ مَا تَفْعَلُوْنَ

ya'lamuuna m*aa* taf'aluun**a**

mereka mengetahui apa yang kamu kerjakan.

82:13

# اِنَّ الْاَبْرَارَ لَفِيْ نَعِيْمٍۙ

inna **a**l-abr*aa*ra lafii na'iim**in**

Sesungguhnya orang-orang yang berbakti benar-benar berada dalam (surga yang penuh) kenikmatan,

82:14

# وَّاِنَّ الْفُجَّارَ لَفِيْ جَحِيْمٍ

wa-inna **a**lfujj*aa*ra lafii ja*h*iim**in**

dan sesungguhnya orang-orang yang durhaka benar-benar berada dalam neraka.

82:15

# يَصْلَوْنَهَا يَوْمَ الدِّيْنِ

ya*sh*lawnah*aa* yawma **al**ddiin**i**

Mereka masuk ke dalamnya pada hari pembalasan.

82:16

# وَمَا هُمْ عَنْهَا بِغَاۤىِٕبِيْنَۗ

wam*aa* hum 'anh*aa* bigh*aa*-ibiin**a**

Dan mereka tidak mungkin keluar dari neraka itu.

82:17

# وَمَآ اَدْرٰىكَ مَا يَوْمُ الدِّيْنِۙ

wam*aa* adr*aa*ka m*aa* yawmu **al**ddiini

Dan tahukah kamu apakah hari pembalasan itu?

82:18

# ثُمَّ مَآ اَدْرٰىكَ مَا يَوْمُ الدِّيْنِۗ

tsumma m*aa* adr*aa*ka m*aa* yawmu **al**ddiin**i**

Sekali lagi, tahukah kamu apakah hari pembalasan itu?

82:19

# يَوْمَ لَا تَمْلِكُ نَفْسٌ لِّنَفْسٍ شَيْـًٔا ۗوَالْاَمْرُ يَوْمَىِٕذٍ لِّلّٰهِ ࣖ

yawma l*aa* tamliku nafsun linafsin syay-an wa**a**l-amru yawma-i*dz*in lill*aa*h**i**

(Yaitu) pada hari (ketika) seseorang sama sekali tidak berdaya (menolong) orang lain. Dan segala urusan pada hari itu dalam kekuasaan Allah.

<!--EndFragment-->