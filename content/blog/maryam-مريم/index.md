---
title: (19) Maryam - مريم
date: 2021-10-27T03:44:54.006Z
ayat: 19
description: "Jumlah Ayat: 98 / Arti: Maryam"
---
<!--StartFragment-->

19:1

# كۤهٰيٰعۤصۤ ۚ

k*aa*f-h*aa*-y*aa*-'ayn-*shaa*d

Kaf Ha Ya ‘Ain Shad.

19:2

# ذِكْرُ رَحْمَتِ رَبِّكَ عَبْدَهٗ زَكَرِيَّا ۚ

*dz*ikru ra*h*mati rabbika 'abdahu zakariyy*aa*

Yang dibacakan ini adalah) penjelasan tentang rahmat Tuhanmu kepada hamba-Nya, Zakaria,

19:3

# اِذْ نَادٰى رَبَّهٗ نِدَاۤءً خَفِيًّا

i*dz* n*aa*d*aa* rabbahu nid*aa*-an khafiyy*aa***n**

(yaitu) ketika dia berdoa kepada Tuhannya dengan suara yang lembut.

19:4

# قَالَ رَبِّ اِنِّيْ وَهَنَ الْعَظْمُ مِنِّيْ وَاشْتَعَلَ الرَّأْسُ شَيْبًا وَّلَمْ اَكُنْۢ بِدُعَاۤىِٕكَ رَبِّ شَقِيًّا

q*aa*la rabbi innii wahana **a**l'a*zh*mu minnii wa**i**syta'ala **al**rra/su syayban walam akun bidu'*aa*-ika rabbi syaqiyy*aa***n**

Dia (Zakaria) berkata, “Ya Tuhanku, sungguh tulangku telah lemah dan kepalaku telah dipenuhi uban, dan aku belum pernah kecewa dalam berdoa kepada-Mu, ya Tuhanku.

19:5

# وَاِنِّيْ خِفْتُ الْمَوَالِيَ مِنْ وَّرَاۤءِيْ وَكَانَتِ امْرَاَتِيْ عَاقِرًا فَهَبْ لِيْ مِنْ لَّدُنْكَ وَلِيًّا ۙ

wa-innii khiftu **a**lmaw*aa*liya min war*aa*-ii wak*aa*nati imra-atii '*aa*qiran fahab lii min ladunka waliyy*aa***n**

Dan sungguh, aku khawatir terhadap kerabatku sepeninggalku, padahal istriku seorang yang mandul, maka anugerahilah aku seorang anak dari sisi-Mu,

19:6

# يَّرِثُنِيْ وَيَرِثُ مِنْ اٰلِ يَعْقُوْبَ وَاجْعَلْهُ رَبِّ رَضِيًّا

yaritsunii wayaritsu min *aa*li ya'quuba wa**i**j'alhu rabbi ra*dh*iyy*aa***n**

yang akan mewarisi aku dan mewarisi dari keluarga Yakub; dan jadikanlah dia, ya Tuhanku, seorang yang diridai.”

19:7

# يٰزَكَرِيَّآ اِنَّا نُبَشِّرُكَ بِغُلٰمِ ِۨاسْمُهٗ يَحْيٰىۙ لَمْ نَجْعَلْ لَّهٗ مِنْ قَبْلُ سَمِيًّا

y*aa* zakariyy*aa* inn*aa* nubasysyiruka bighul*aa*min ismuhu ya*h*y*aa* lam naj'al lahu min qablu samiyy*aa***n**

(Allah berfirman), “Wahai Zakaria! Kami memberi kabar gembira kepadamu dengan seorang anak laki-laki namanya Yahya, yang Kami belum pernah memberikan nama seperti itu sebelumnya.”

19:8

# قَالَ رَبِّ اَنّٰى يَكُوْنُ لِيْ غُلٰمٌ وَّكَانَتِ امْرَاَتِيْ عَاقِرًا وَّقَدْ بَلَغْتُ مِنَ الْكِبَرِ عِتِيًّا

q*aa*la rabbi ann*aa* yakuunu lii ghul*aa*mun wak*aa*nati imra-atii '*aa*qiran waqad balaghtu mina **a**lkibari 'itiyy*aa***n**

Dia (Zakaria) berkata, “Ya Tuhanku, bagaimana aku akan mempunyai anak, padahal istriku seorang yang mandul dan aku (sendiri) sesungguhnya sudah mencapai usia yang sangat tua?”

19:9

# قَالَ كَذٰلِكَۗ قَالَ رَبُّكَ هُوَ عَلَيَّ هَيِّنٌ وَّقَدْ خَلَقْتُكَ مِنْ قَبْلُ وَلَمْ تَكُ شَيْـًٔا

q*aa*la ka*dzaa*lika q*aa*la rabbuka huwa 'alayya hayyinun waqad khalaqtuka min qablu walam taku syay-*aa***n**

(Allah) berfirman, “Demikianlah.” Tuhanmu berfirman, “Hal itu mudah bagi-Ku; sungguh, engkau telah Aku ciptakan sebelum itu, padahal (pada waktu itu) engkau belum berwujud sama sekali.”

19:10

# قَالَ رَبِّ اجْعَلْ لِّيْٓ اٰيَةً ۗقَالَ اٰيَتُكَ اَلَّا تُكَلِّمَ النَّاسَ ثَلٰثَ لَيَالٍ سَوِيًّا

q*aa*la rabbi ij'al lii *aa*yatan q*aa*la *aa*yatuka **al**l*aa* tukallima **al**nn*aa*sa tsal*aa*tsa lay*aa*lin sawiyy*aa***n**

Dia (Zakaria) berkata, “Ya Tuhanku, berilah aku suatu tanda.” (Allah) berfirman, “Tandamu ialah engkau tidak dapat bercakap-cakap dengan manusia selama tiga malam, padahal engkau sehat.”

19:11

# فَخَرَجَ عَلٰى قَوْمِهٖ مِنَ الْمِحْرَابِ فَاَوْحٰٓى اِلَيْهِمْ اَنْ سَبِّحُوْا بُكْرَةً وَّعَشِيًّا

fakharaja 'al*aa* qawmihi mina **a**lmi*h*r*aa*bi fa-aw*haa* ilayhim an sabbi*h*uu bukratan wa'asyiyy*aa***n**

Maka dia keluar dari mihrab menuju kaumnya, lalu dia memberi isyarat kepada mereka; bertasbihlah kamu pada waktu pagi dan petang.

19:12

# يٰيَحْيٰى خُذِ الْكِتٰبَ بِقُوَّةٍ ۗوَاٰتَيْنٰهُ الْحُكْمَ صَبِيًّاۙ

y*aa* ya*h*y*aa* khu*dz*i **a**lkit*aa*ba biquwwatin wa*aa*tayn*aa*hu **a**l*h*ukma *sh*abiyy*aa***n**

”Wahai Yahya! Ambillah (pelajarilah) Kitab (Taurat) itu dengan sungguh-sungguh.” Dan Kami berikan hikmah kepadanya (Yahya) selagi dia masih kanak-kanak,

19:13

# وَّحَنَانًا مِّنْ لَّدُنَّا وَزَكٰوةً ۗوَكَانَ تَقِيًّا ۙ

wa*h*an*aa*nan min ladunn*aa* wazak*aa*tan wak*aa*na taqiyy*aa***n**

dan (Kami jadikan) rasa kasih sayang (kepada sesama) dari Kami dan bersih (dari dosa). Dan dia pun seorang yang bertakwa,

19:14

# وَّبَرًّاۢ بِوَالِدَيْهِ وَلَمْ يَكُنْ جَبَّارًا عَصِيًّا

wabarran biw*aa*lidayhi walam yakun jabb*aa*ran 'a*sh*iyy*aa***n**

dan sangat berbakti kepada kedua orang tuanya, dan dia bukan orang yang sombong (bukan pula) orang yang durhaka.

19:15

# وَسَلٰمٌ عَلَيْهِ يَوْمَ وُلِدَ وَيَوْمَ يَمُوْتُ وَيَوْمَ يُبْعَثُ حَيًّا ࣖ

wasal*aa*mun 'alayhi yawma wulida wayawma yamuutu wayawma yub'atsu *h*ayy*aa***n**

Dan kesejahteraan bagi dirinya pada hari lahirnya, pada hari wafatnya, dan pada hari dia dibangkitkan hidup kembali.

19:16

# وَاذْكُرْ فِى الْكِتٰبِ مَرْيَمَۘ اِذِ انْتَبَذَتْ مِنْ اَهْلِهَا مَكَانًا شَرْقِيًّا ۙ

wa**u***dz*kur fii **a**lkit*aa*bi maryama i*dz*i intaba*dz*at min ahlih*aa* mak*aa*nan syarqiyy*aa***n**

Dan ceritakanlah (Muhammad) kisah Maryam di dalam Kitab (Al-Qur'an), (yaitu) ketika dia mengasingkan diri dari keluarganya ke suatu tempat di sebelah timur (Baitulmaqdis),

19:17

# فَاتَّخَذَتْ مِنْ دُوْنِهِمْ حِجَابًاۗ فَاَرْسَلْنَآ اِلَيْهَا رُوْحَنَا فَتَمَثَّلَ لَهَا بَشَرًا سَوِيًّا

fa**i**ttakha*dz*at min duunihim *h*ij*aa*ban fa-arsaln*aa* ilayh*aa* ruu*h*an*aa* fatamatstsala lah*aa* basyaran sawiyy*aa***n**

lalu dia memasang tabir (yang melindunginya) dari mereka; lalu Kami mengutus roh Kami (Jibril) kepadanya, maka dia menampakkan diri di hadapannya dalam bentuk manusia yang sempurna.

19:18

# قَالَتْ اِنِّيْٓ اَعُوْذُ بِالرَّحْمٰنِ مِنْكَ اِنْ كُنْتَ تَقِيًّا

q*aa*lat innii a'uu*dz*u bi**al**rra*h*m*aa*ni minka in kunta taqiyy*aa***n**

Dia (Maryam) berkata, “Sungguh, aku berlindung kepada Tuhan Yang Maha Pengasih terhadapmu, jika engkau orang yang bertakwa.”

19:19

# قَالَ اِنَّمَآ اَنَا۠ رَسُوْلُ رَبِّكِۖ لِاَهَبَ لَكِ غُلٰمًا زَكِيًّا

q*aa*la innam*aa* an*aa* rasuulu rabbiki li-ahaba laki ghul*aa*man zakiyy*aa***n**

Dia (Jibril) berkata, “Sesungguhnya aku hanyalah utusan Tuhanmu, untuk menyampaikan anugerah kepadamu seorang anak laki-laki yang suci.”

19:20

# قَالَتْ اَنّٰى يَكُوْنُ لِيْ غُلٰمٌ وَّلَمْ يَمْسَسْنِيْ بَشَرٌ وَّلَمْ اَكُ بَغِيًّا

q*aa*lat ann*aa* yakuunu lii ghul*aa*mun walam yamsasnii basyarun walam aku baghiyy*aa***n**

Dia (Maryam) berkata, “Bagaimana mungkin aku mempunyai anak laki-laki, padahal tidak pernah ada orang (laki-laki) yang menyentuhku dan aku bukan seorang pezina!”

19:21

# قَالَ كَذٰلِكِۚ قَالَ رَبُّكِ هُوَ عَلَيَّ هَيِّنٌۚ وَلِنَجْعَلَهٗٓ اٰيَةً لِّلنَّاسِ وَرَحْمَةً مِّنَّاۚ وَكَانَ اَمْرًا مَّقْضِيًّا

q*aa*la ka*dzaa*liki q*aa*la rabbuki huwa 'alayya hayyinun walinaj'alahu *aa*yatan li**l**nn*aa*si wara*h*matan minn*aa* wak*aa*na amran maq*dh*iyy*aa***n**

Dia (Jibril) berkata, “Demikianlah.” Tuhanmu berfirman, “Hal itu mudah bagi-Ku, dan agar Kami menjadikannya suatu tanda (kebesaran Allah) bagi manusia dan sebagai rahmat dari Kami; dan hal itu adalah suatu urusan yang (sudah) diputuskan.”

19:22

# ۞ فَحَمَلَتْهُ فَانْتَبَذَتْ بِهٖ مَكَانًا قَصِيًّا

fa*h*amalat-hu fa**i**ntaba*dz*at bihi mak*aa*nan qa*sh*iyy*aa***n**

Maka dia (Maryam) mengandung, lalu dia mengasingkan diri dengan kandungannya itu ke tempat yang jauh.

19:23

# فَاَجَاۤءَهَا الْمَخَاضُ اِلٰى جِذْعِ النَّخْلَةِۚ قَالَتْ يٰلَيْتَنِيْ مِتُّ قَبْلَ هٰذَا وَكُنْتُ نَسْيًا مَّنْسِيًّا

fa-aj*aa*-ah*aa* **a**lmakh*aad*u il*aa* ji*dz*'i **al**nnakhlati q*aa*lat y*aa* laytanii mittu qabla h*aadzaa* wakuntu nasyan mansiyy*aa***n**

Kemudian rasa sakit akan melahirkan memaksanya (bersandar) pada pangkal pohon kurma, dia (Maryam) berkata, “Wahai, betapa (baiknya) aku mati sebelum ini, dan aku menjadi seorang yang tidak diperhatikan dan dilupakan.”

19:24

# فَنَادٰىهَا مِنْ تَحْتِهَآ اَلَّا تَحْزَنِيْ قَدْ جَعَلَ رَبُّكِ تَحْتَكِ سَرِيًّا

fan*aa*d*aa*h*aa* min ta*h*tih*aa* **al**l*aa* ta*h*zanii qad ja'ala rabbuki ta*h*taki sariyy*aa***n**

Maka dia (Jibril) berseru kepadanya dari tempat yang rendah, “Janganlah engkau bersedih hati, sesungguhnya Tuhanmu telah menjadikan anak sungai di bawahmu.

19:25

# وَهُزِّيْٓ اِلَيْكِ بِجِذْعِ النَّخْلَةِ تُسٰقِطْ عَلَيْكِ رُطَبًا جَنِيًّا ۖ

wahuzzii ilayki biji*dz*'i **al**nnakhlati tus*aa*qi*th* 'alayki ru*th*aban janiyy*aa***n**

Dan goyanglah pangkal pohon kurma itu ke arahmu, niscaya (pohon) itu akan menggugurkan buah kurma yang masak kepadamu.

19:26

# فَكُلِيْ وَاشْرَبِيْ وَقَرِّيْ عَيْنًا ۚفَاِمَّا تَرَيِنَّ مِنَ الْبَشَرِ اَحَدًاۙ فَقُوْلِيْٓ اِنِّيْ نَذَرْتُ لِلرَّحْمٰنِ صَوْمًا فَلَنْ اُكَلِّمَ الْيَوْمَ اِنْسِيًّا ۚ

fakulii wa**i**syrabii waqarrii 'aynan fa-imm*aa* tarayinna mina **a**lbasyari a*h*adan faquulii innii na*dz*artu li**l**rra*h*m*aa*ni *sh*awman falan ukallima **a**lyawma i

Maka makan, minum dan bersenanghatilah engkau. Jika engkau melihat seseorang, maka katakanlah, “Sesungguhnya aku telah bernazar berpuasa untuk Tuhan Yang Maha Pengasih, maka aku tidak akan berbicara dengan siapa pun pada hari ini.”

19:27

# فَاَتَتْ بِهٖ قَوْمَهَا تَحْمِلُهٗ ۗقَالُوْا يٰمَرْيَمُ لَقَدْ جِئْتِ شَيْـًٔا فَرِيًّا

fa-atat bihi qawmah*aa* ta*h*miluhu q*aa*luu y*aa* maryamu laqad ji/ti syay-an fariyy*aa***n**

Kemudian dia (Maryam) membawa dia (bayi itu) kepada kaumnya dengan menggendongnya. Mereka (kaumnya) berkata, “Wahai Maryam! Sungguh, engkau telah membawa sesuatu yang sangat mungkar.

19:28

# يٰٓاُخْتَ هٰرُوْنَ مَا كَانَ اَبُوْكِ امْرَاَ سَوْءٍ وَّمَا كَانَتْ اُمُّكِ بَغِيًّا ۖ

y*aa* ukhta h*aa*ruuna m*aa* k*aa*na abuuki imra-a saw-in wam*aa* k*aa*nat ummuki baghiyy*aa***n**

Wahai saudara perempuan Harun (Maryam)! Ayahmu bukan seorang yang buruk perangai dan ibumu bukan seorang perempuan pezina.”

19:29

# فَاَشَارَتْ اِلَيْهِۗ قَالُوْا كَيْفَ نُكَلِّمُ مَنْ كَانَ فِى الْمَهْدِ صَبِيًّا

fa-asy*aa*rat ilayhi q*aa*luu kayfa nukallimu man k*aa*na fii **a**lmahdi *sh*abiyy*aa***n**

Maka dia (Maryam) menunjuk kepada (anak)nya. Mereka berkata, “Bagaimana kami akan berbicara dengan anak kecil yang masih dalam ayunan?”

19:30

# قَالَ اِنِّيْ عَبْدُ اللّٰهِ ۗاٰتٰنِيَ الْكِتٰبَ وَجَعَلَنِيْ نَبِيًّا ۙ

q*aa*la innii 'abdu **al**l*aa*hi *aa*t*aa*niya **a**lkit*aa*ba waja'alanii nabiyy*aa***n**

Dia (Isa) berkata, “Sesungguhnya aku hamba Allah, Dia memberiku Kitab (Injil) dan Dia menjadikan aku seorang Nabi.

19:31

# وَّجَعَلَنِيْ مُبٰرَكًا اَيْنَ مَا كُنْتُۖ وَاَوْصٰنِيْ بِالصَّلٰوةِ وَالزَّكٰوةِ مَا دُمْتُ حَيًّا ۖ

waja'alanii mub*aa*rakan aynam*aa* kuntu wa-aw*shaa*nii bi**al***shsh*al*aa*ti wa**al**zzak*aa*ti m*aa* dumtu *h*ayy*aa***n**

Dan Dia menjadikan aku seorang yang diberkahi di mana saja aku berada, dan Dia memerintahkan kepadaku (melaksanakan) salat dan (menunaikan) zakat selama aku hidup;

19:32

# وَّبَرًّاۢ بِوَالِدَتِيْ وَلَمْ يَجْعَلْنِيْ جَبَّارًا شَقِيًّا

wabarran biw*aa*lidatii walam yaj'alnii jabb*aa*ran syaqiyy*aa***n**

dan berbakti kepada ibuku, dan Dia tidak menjadikan aku seorang yang sombong lagi celaka.

19:33

# وَالسَّلٰمُ عَلَيَّ يَوْمَ وُلِدْتُّ وَيَوْمَ اَمُوْتُ وَيَوْمَ اُبْعَثُ حَيًّا

wa**al**ssal*aa*mu 'alayya yawma wulidtu wayawma amuutu wayawma ub'atsu *h*ayy*aa***n**

Dan kesejahteraan semoga dilimpahkan kepadaku, pada hari kelahiranku, pada hari wafatku, dan pada hari aku dibangkitkan hidup kembali.”

19:34

# ذٰلِكَ عِيْسَى ابْنُ مَرْيَمَ ۚقَوْلَ الْحَقِّ الَّذِيْ فِيْهِ يَمْتَرُوْنَ

*dzaa*lika 'iis*aa* ibnu maryama qawla **a**l*h*aqqi **al**la*dz*ii fiihi yamtaruun**a**

Itulah Isa putra Maryam, (yang mengatakan) perkataan yang benar, yang mereka ragukan kebenarannya.

19:35

# مَا كَانَ لِلّٰهِ اَنْ يَّتَّخِذَ مِنْ وَّلَدٍ سُبْحٰنَهٗ ۗاِذَا قَضٰٓى اَمْرًا فَاِنَّمَا يَقُوْلُ لَهٗ كُنْ فَيَكُوْنُ ۗ

m*aa* k*aa*na lill*aa*hi an yattakhi*dz*a min waladin sub*haa*nahu i*dzaa* qa*daa* amran fa-innam*aa* yaquulu lahu kun fayakuun**u**

Tidak patut bagi Allah mempunyai anak, Mahasuci Dia. Apabila Dia hendak menetapkan sesuatu, maka Dia hanya berkata kepadanya, “Jadilah!” Maka jadilah sesuatu itu.

19:36

# وَاِنَّ اللّٰهَ رَبِّيْ وَرَبُّكُمْ فَاعْبُدُوْهُ ۗهٰذَا صِرَاطٌ مُّسْتَقِيْمٌ

wa-inna **al**l*aa*ha rabbii warabbukum fa**u**'buduuhu h*aadzaa* *sh*ir*aath*un mustaqiim**un**

(Isa berkata), “Dan sesungguhnya Allah itu Tuhanku dan Tuhanmu, maka sembahlah Dia. Ini adalah jalan yang lurus.”

19:37

# فَاخْتَلَفَ الْاَحْزَابُ مِنْۢ بَيْنِهِمْۚ فَوَيْلٌ لِّلَّذِيْنَ كَفَرُوْا مِنْ مَّشْهَدِ يَوْمٍ عَظِيْمٍ

fa**i**khtalafa **a**l-a*h*z*aa*bu min baynihim fawaylun lilla*dz*iina kafaruu min masyhadi yawmin 'a*zh*iim**in**

Maka berselisihlah golongan-golongan (yang ada) di antara mereka (Yahudi dan Nasrani). Maka celakalah orang-orang kafir pada waktu menyaksikan hari yang agung!

19:38

# اَسْمِعْ بِهِمْ وَاَبْصِرْۙ يَوْمَ يَأْتُوْنَنَا لٰكِنِ الظّٰلِمُوْنَ الْيَوْمَ فِيْ ضَلٰلٍ مُّبِيْنٍ

asmi' bihim wa-ab*sh*ir yawma ya/tuunan*aa* l*aa*kini **al***zhzhaa*limuuna **a**lyawma fii *dh*al*aa*lin mubiin**in**

Alangkah tajam pendengaran mereka dan alangkah terang penglihatan mereka pada hari mereka datang kepada Kami. Tetapi orang-orang yang zalim pada hari ini (di dunia) berada dalam kesesatan yang nyata.

19:39

# وَاَنْذِرْهُمْ يَوْمَ الْحَسْرَةِ اِذْ قُضِيَ الْاَمْرُۘ وَهُمْ فِيْ غَفْلَةٍ وَّهُمْ لَا يُؤْمِنُوْنَ

wa-an*dz*irhum yawma **a**l*h*asrati i*dz* qu*dh*iya **a**l-amru wahum fii ghaflatin wahum l*aa* yu/minuun**a**

Dan berilah mereka peringatan (Muhammad) tentang hari penyesalan, (yaitu) ketika segala perkara telah diputus, sedang mereka dalam kelalaian dan mereka tidak beriman.

19:40

# اِنَّا نَحْنُ نَرِثُ الْاَرْضَ وَمَنْ عَلَيْهَا وَاِلَيْنَا يُرْجَعُوْنَ ࣖ

inn*aa* na*h*nu naritsu **a**l-ar*dh*a waman 'alayh*aa* wa-ilayn*aa* yurja'uun**a**

Sesungguhnya Kamilah yang mewarisi bumi dan semua yang ada di atasnya, dan hanya kepada Kami mereka dikembalikan.

19:41

# وَاذْكُرْ فِى الْكِتٰبِ اِبْرٰهِيْمَ ەۗ اِنَّهٗ كَانَ صِدِّيْقًا نَّبِيًّا

wa**u***dz*kur fii **a**lkit*aa*bi ibr*aa*hiima innahu k*aa*na *sh*iddiiqan nabiyy*aa***n**

Dan ceritakanlah (Muhammad) kisah Ibrahim di dalam Kitab (Al-Qur'an), sesungguhnya dia adalah seorang yang sangat membenarkan, seorang Nabi.

19:42

# اِذْ قَالَ لِاَبِيْهِ يٰٓاَبَتِ لِمَ تَعْبُدُ مَا لَا يَسْمَعُ وَلَا يُبْصِرُ وَلَا يُغْنِيْ عَنْكَ شَيْـًٔا

i*dz* q*aa*la li-abiihi y*aa* abati lima ta'budu m*aa* l*aa* yasma'u wal*aa* yub*sh*iru wal*aa* yughnii 'anka syay-*aa***n**

(Ingatlah) ketika dia (Ibrahim) berkata kepada ayahnya, “Wahai ayahku! Mengapa engkau menyembah sesuatu yang tidak mendengar, tidak melihat, dan tidak dapat menolongmu sedikit pun?

19:43

# يٰٓاَبَتِ اِنِّي قَدْ جَاۤءَنِيْ مِنَ الْعِلْمِ مَا لَمْ يَأْتِكَ فَاتَّبِعْنِيْٓ اَهْدِكَ صِرَاطًا سَوِيًّا

y*aa* abati innii qad j*aa*-anii mina **a**l'ilmi m*aa* lam ya/tika fa**i**ttabi'nii ahdika *sh*ir*aath*an sawiyy*aa***n**

Wahai ayahku! Sungguh, telah sampai kepadaku sebagian ilmu yang tidak diberikan kepadamu, maka ikutilah aku, niscaya aku akan menunjukkan kepadamu jalan yang lurus.

19:44

# يٰٓاَبَتِ لَا تَعْبُدِ الشَّيْطٰنَۗ اِنَّ الشَّيْطٰنَ كَانَ لِلرَّحْمٰنِ عَصِيًّا

y*aa* abati l*aa* ta'budi **al**sysyay*thaa*na inna **al**sysyay*thaa*na k*aa*na li**l**rra*h*m*aa*ni 'a*sh*iyy*aa***n**

Wahai ayahku! Janganlah engkau menyembah setan. Sungguh, setan itu durhaka kepada Tuhan Yang Maha Pengasih.

19:45

# يٰٓاَبَتِ اِنِّيْٓ اَخَافُ اَنْ يَّمَسَّكَ عَذَابٌ مِّنَ الرَّحْمٰنِ فَتَكُوْنَ لِلشَّيْطٰنِ وَلِيًّا

y*aa* abati innii akh*aa*fu an yamassaka 'a*dzaa*bun mina **al**rra*h*m*aa*ni fatakuuna li**l**sysyya*thaa*ni waliyy*aa***n**

Wahai ayahku! Aku sungguh khawatir engkau akan ditimpa azab dari Tuhan Yang Maha Pengasih, sehingga engkau menjadi teman bagi setan.”

19:46

# قَالَ اَرَاغِبٌ اَنْتَ عَنْ اٰلِهَتِيْ يٰٓاِبْرٰهِيْمُ ۚ لَىِٕنْ لَّمْ تَنْتَهِ لَاَرْجُمَنَّكَ وَاهْجُرْنِيْ مَلِيًّا

q*aa*la ar*aa*ghibun anta 'an *aa*lihatii y*aa* ibr*aa*hiimu la-in lam tantahi la-arjumannaka wa**u**hjurnii maliyy*aa***n**

Dia (ayahnya) berkata, “Bencikah engkau kepada tuhan-tuhanku, wahai Ibrahim? Jika engkau tidak berhenti, pasti engkau akan kurajam, maka tinggalkanlah aku untuk waktu yang lama.”

19:47

# قَالَ سَلٰمٌ عَلَيْكَۚ سَاَسْتَغْفِرُ لَكَ رَبِّيْۗ اِنَّهٗ كَانَ بِيْ حَفِيًّا

q*aa*la sal*aa*mun 'alayka sa-astaghfiru laka rabbii innahu k*aa*na bii *h*afiyy*aa***n**

Dia (Ibrahim) berkata, “Semoga keselamatan dilimpahkan kepadamu, aku akan memohonkan ampunan bagimu kepada Tuhanku. Sesungguhnya Dia sangat baik kepadaku.

19:48

# وَاَعْتَزِلُكُمْ وَمَا تَدْعُوْنَ مِنْ دُوْنِ اللّٰهِ وَاَدْعُوْ رَبِّيْۖ عَسٰٓى اَلَّآ اَكُوْنَ بِدُعَاۤءِ رَبِّيْ شَقِيًّا

wa-a'tazilukum wam*aa* tad'uuna min duuni **al**l*aa*hi wa-ad'uu rabbii 'as*aa* **al**l*aa* akuuna bidu'*aa*-i rabbii syaqiyy*aa***n**

Dan aku akan menjauhkan diri darimu dan dari apa yang engkau sembah selain Allah, dan aku akan berdoa kepada Tuhanku, mudah-mudahan aku tidak akan kecewa dengan berdoa kepada Tuhanku.”

19:49

# فَلَمَّا اعْتَزَلَهُمْ وَمَا يَعْبُدُوْنَ مِنْ دُوْنِ اللّٰهِ ۙوَهَبْنَا لَهٗٓ اِسْحٰقَ وَيَعْقُوْبَۗ وَكُلًّا جَعَلْنَا نَبِيًّا

falamm*aa* i'tazalahum wam*aa* ya'buduuna min duuni **al**l*aa*hi wahabn*aa* lahu is*haa*qa waya'quuba wakullan ja'aln*aa* nabiyy*aa***n**

Maka ketika dia (Ibrahim) sudah menjauhkan diri dari mereka dan dari apa yang mereka sembah selain Allah, Kami anugerahkan kepadanya Ishak dan Yakub. Dan masing-masing Kami angkat menjadi nabi.

19:50

# وَوَهَبْنَا لَهُمْ مِّنْ رَّحْمَتِنَا وَجَعَلْنَا لَهُمْ لِسَانَ صِدْقٍ عَلِيًّا ࣖ

wawahabn*aa* lahum min ra*h*matin*aa* waja'aln*aa* lahum lis*aa*na *sh*idqin 'aliyy*aa***n**

Dan Kami anugerahkan kepada mereka sebagian dari rahmat Kami dan Kami jadikan mereka buah tutur yang baik dan mulia.

19:51

# وَاذْكُرْ فِى الْكِتٰبِ مُوْسٰٓىۖ اِنَّهٗ كَانَ مُخْلَصًا وَّكَانَ رَسُوْلًا نَّبِيًّا

wa**u***dz*kur fii **a**lkit*aa*bi muus*aa* innahu k*aa*na mukhla*sh*an wak*aa*na rasuulan nabiyy*aa***n**

Dan ceritakanlah (Muhammad), kisah Musa di dalam Kitab (Al-Qur'an). Dia benar-benar orang yang terpilih, seorang rasul dan nabi.

19:52

# وَنَادَيْنٰهُ مِنْ جَانِبِ الطُّوْرِ الْاَيْمَنِ وَقَرَّبْنٰهُ نَجِيًّا

wan*aa*dayn*aa*hu min j*aa*nibi **al***ththh*uuri **a**l-aymani waqarrabn*aa*hu najiyy*aa***n**

Dan Kami telah memanggilnya dari sebelah kanan gunung (Sinai) dan Kami dekatkan dia untuk bercakap-cakap.

19:53

# وَوَهَبْنَا لَهٗ مِنْ رَّحْمَتِنَآ اَخَاهُ هٰرُوْنَ نَبِيًّا

wawahabn*aa* lahu min ra*h*matin*aa* akh*aa*hu h*aa*ruuna nabiyy*aa***n**

Dan Kami telah menganugerahkan sebagian rahmat Kami kepadanya, yaitu (bahwa) saudaranya, Harun, menjadi seorang nabi.

19:54

# وَاذْكُرْ فِى الْكِتٰبِ اِسْمٰعِيْلَ ۖاِنَّهٗ كَانَ صَادِقَ الْوَعْدِ وَكَانَ رَسُوْلًا نَّبِيًّا ۚ

wa**u***dz*kur fii **a**lkit*aa*bi ism*aa*'iila innahu k*aa*na *shaa*diqa **a**lwa'di wak*aa*na rasuulan nabiyy*aa***n**

Dan ceritakanlah (Muhammad), kisah Ismail di dalam Kitab (Al-Qur'an). Dia benar-benar seorang yang benar janjinya, seorang rasul dan nabi.

19:55

# وَكَانَ يَأْمُرُ اَهْلَهٗ بِالصَّلٰوةِ وَالزَّكٰوةِۖ وَكَانَ عِنْدَ رَبِّهٖ مَرْضِيًّا

wak*aa*na ya/muru ahlahu bi**al***shsh*al*aa*ti wa**al**zzak*aa*ti wak*aa*na 'inda rabbihi mar*dh*iyy*aa***n**

Dan dia menyuruh keluarganya untuk (melaksanakan) salat dan (menunaikan) zakat, dan dia seorang yang diridai di sisi Tuhannya.

19:56

# وَاذْكُرْ فِى الْكِتٰبِ اِدْرِيْسَۖ اِنَّهٗ كَانَ صِدِّيْقًا نَّبِيًّا ۙ

wa**u***dz*kur fii **a**lkit*aa*bi idriisa innahu k*aa*na *sh*iddiiqan nabiyy*aa***n**

Dan ceritakanlah (Muhammad) kisah Idris di dalam Kitab (Al-Qur'an). Sesungguhnya dia seorang yang sangat mencintai kebenaran dan seorang nabi,

19:57

# وَّرَفَعْنٰهُ مَكَانًا عَلِيًّا

warafa'n*aa*hu mak*aa*nan 'aliyy*aa***n**

dan Kami telah mengangkatnya ke martabat yang tinggi.

19:58

# اُولٰۤىِٕكَ الَّذِيْنَ اَنْعَمَ اللّٰهُ عَلَيْهِمْ مِّنَ النَّبِيّٖنَ مِنْ ذُرِّيَّةِ اٰدَمَ وَمِمَّنْ حَمَلْنَا مَعَ نُوْحٍۖ وَّمِنْ ذُرِّيَّةِ اِبْرٰهِيْمَ وَاِسْرَاۤءِيْلَ ۖوَمِمَّنْ هَدَيْنَا وَاجْتَبَيْنَاۗ اِذَا تُتْلٰى عَلَيْهِمْ اٰيٰتُ الرَّحْمٰنِ

ul*aa*-ika **al**la*dz*iina an'ama **al**l*aa*hu 'alayhim mina **al**nnabiyyiina min *dz*urriyyati *aa*dama wamimman *h*amaln*aa* ma'a nuu*h*in wamin *dz*urriyyati ibr*a*

Mereka itulah orang yang telah diberi nikmat oleh Allah, yaitu dari (golongan) para nabi dari keturunan Adam, dan dari orang yang Kami bawa (dalam kapal) bersama Nuh, dan dari keturunan Ibrahim dan Israil (Yakub) dan dari orang yang telah Kami beri petunj







19:59

# ۞ فَخَلَفَ مِنْۢ بَعْدِهِمْ خَلْفٌ اَضَاعُوا الصَّلٰوةَ وَاتَّبَعُوا الشَّهَوٰتِ فَسَوْفَ يَلْقَوْنَ غَيًّا ۙ

fakhalafa min ba'dihim khalfun a*daa*'uu **al***shsh*al*aa*ta wa**i**ttaba'uu **al**sysyahaw*aa*ti fasawfa yalqawna ghayy*aa***n**

Kemudian datanglah setelah mereka pengganti yang mengabaikan salat dan mengikuti keinginannya, maka mereka kelak akan tersesat,

19:60

# اِلَّا مَنْ تَابَ وَاٰمَنَ وَعَمِلَ صَالِحًا فَاُولٰۤىِٕكَ يَدْخُلُوْنَ الْجَنَّةَ وَلَا يُظْلَمُوْنَ شَيْـًٔا ۙ

ill*aa* man t*aa*ba wa*aa*mana wa'amila *shaa*li*h*an faul*aa*-ika yadkhuluuna **a**ljannata wal*aa* yu*zh*lamuuna syay-*aa***n**

Kecuali orang yang bertobat, beriman dan mengerjakan kebajikan, maka mereka itu akan masuk surga dan tidak dizalimi (dirugikan) sedikit pun,

19:61

# جَنّٰتِ عَدْنِ ِۨالَّتِيْ وَعَدَ الرَّحْمٰنُ عِبَادَهٗ بِالْغَيْبِۗ اِنَّهٗ كَانَ وَعْدُهٗ مَأْتِيًّا

jann*aa*ti 'adnin **al**latii wa'ada **al**rra*h*m*aa*nu 'ib*aa*dahu bi**al**ghaybi innahu k*aa*na wa'duhu ma/tiyy*aa***n**

yaitu surga ‘Adn yang telah dijanjikan oleh Tuhan Yang Maha Pengasih kepada hamba-hamba-Nya, sekalipun (surga itu) tidak tampak. Sungguh, (janji Allah) itu pasti ditepati.

19:62

# لَا يَسْمَعُوْنَ فِيْهَا لَغْوًا اِلَّا سَلٰمًاۗ وَلَهُمْ رِزْقُهُمْ فِيْهَا بُكْرَةً وَّعَشِيًّا

l*aa* yasma'uuna fiih*aa* laghwan ill*aa* sal*aa*man walahum rizquhum fiih*aa* bukratan wa'asyiyy*aa***n**

Di dalamnya mereka tidak mendengar perkataan yang tidak berguna, kecuali (ucapan) salam. Dan di dalamnya bagi mereka ada rezeki pagi dan petang.

19:63

# تِلْكَ الْجَنَّةُ الَّتِيْ نُوْرِثُ مِنْ عِبَادِنَا مَنْ كَانَ تَقِيًّا

tilka **a**ljannatu **al**latii nuuritsu min 'ib*aa*din*aa* man k*aa*na taqiyy*aa***n**

Itulah surga yang akan Kami wariskan kepada hamba-hamba Kami yang selalu bertakwa.

19:64

# وَمَا نَتَنَزَّلُ اِلَّا بِاَمْرِ رَبِّكَۚ لَهٗ مَا بَيْنَ اَيْدِيْنَا وَمَا خَلْفَنَا وَمَا بَيْنَ ذٰلِكَ وَمَا كَانَ رَبُّكَ نَسِيًّا ۚ

wam*aa* natanazzalu ill*aa* bi-amri rabbika lahu m*aa* bayna aydiin*aa* wam*aa* khalfan*aa* wam*aa* bayna *dzaa*lika wam*aa* k*aa*na rabbuka nasiyy*aa***n**

Dan tidaklah kami (Jibril) turun, kecuali atas perintah Tuhanmu. Milik-Nya segala yang ada di hadapan kita, yang ada di belakang kita, dan segala yang ada di antara keduanya, dan Tuhanmu tidak lupa.

19:65

# رَبُّ السَّمٰوٰتِ وَالْاَرْضِ وَمَا بَيْنَهُمَا فَاعْبُدْهُ وَاصْطَبِرْ لِعِبَادَتِهٖۗ هَلْ تَعْلَمُ لَهٗ سَمِيًّا ࣖ

rabbu **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i wam*aa* baynahum*aa* fa**u**'budhu wa**i***sth*abir li'ib*aa*datihi hal ta'lamu lahu samiyy*aa***n**

(Dialah) Tuhan (yang menguasai) langit dan bumi dan segala yang ada di antara keduanya, maka sembahlah Dia dan berteguhhatilah dalam beribadah kepada-Nya. Apakah engkau mengetahui ada sesuatu yang sama dengan-Nya?

19:66

# وَيَقُوْلُ الْاِنْسَانُ ءَاِذَا مَا مِتُّ لَسَوْفَ اُخْرَجُ حَيًّا

wayaquulu **a**l-ins*aa*nu a-i*dzaa* m*aa* mittu lasawfa ukhraju *h*ayy*aa***n**

Dan orang (kafir) berkata, “Betulkah apabila aku telah mati, kelak aku sungguh-sungguh akan dibangkitkan hidup kembali?”

19:67

# اَوَلَا يَذْكُرُ الْاِنْسَانُ اَنَّا خَلَقْنٰهُ مِنْ قَبْلُ وَلَمْ يَكُ شَيْـًٔا

awa l*aa* ya*dz*kuru **a**l-ins*aa*nu ann*aa* khalaqn*aa*hu min qablu walam yaku syay-*aa***n**

Dan tidakkah manusia itu memikirkan bahwa sesungguhnya Kami telah menciptakannya dahulu, padahal (sebelumnya) dia belum berwujud sama sekali?

19:68

# فَوَرَبِّكَ لَنَحْشُرَنَّهُمْ وَالشَّيٰطِيْنَ ثُمَّ لَنُحْضِرَنَّهُمْ حَوْلَ جَهَنَّمَ جِثِيًّا

fawarabbika lana*h*syurannahum wa**al**sysyay*aath*iina tsumma lanu*hd*irannahum *h*awla jahannama jitsiyy*aa***n**

Maka demi Tuhanmu, sungguh, pasti akan Kami kumpulkan mereka bersama setan, kemudian pasti akan Kami datangkan mereka ke sekeliling Jahanam dengan berlutut.

19:69

# ثُمَّ لَنَنْزِعَنَّ مِنْ كُلِّ شِيْعَةٍ اَيُّهُمْ اَشَدُّ عَلَى الرَّحْمٰنِ عِتِيًّا ۚ

tsumma lananzi'anna min kulli syii'atin ayyuhum asyaddu 'al*aa* **al**rra*h*m*aa*ni 'itiyy*aa***n**

Kemudian pasti akan Kami tarik dari setiap golongan siapa di antara mereka yang sangat durhaka kepada Tuhan Yang Maha Pengasih.

19:70

# ثُمَّ لَنَحْنُ اَعْلَمُ بِالَّذِيْنَ هُمْ اَوْ لٰى بِهَا صِلِيًّا

tsumma lana*h*nu a'lamu bi**a**lla*dz*iina hum awl*aa* bih*aa* *sh*iliyy*aa***n**

Selanjutnya Kami sungguh lebih mengetahui orang yang seharusnya (dimasukkan) ke dalam neraka.

19:71

# وَاِنْ مِّنْكُمْ اِلَّا وَارِدُهَا ۚ كَانَ عَلٰى رَبِّكَ حَتْمًا مَّقْضِيًّا ۚ

wa-in minkum ill*aa* w*aa*riduh*aa* k*aa*na 'al*aa* rabbika *h*atman maq*dh*iyy*aa***n**

Dan tidak ada seorang pun di antara kamu yang tidak mendatanginya (neraka). Hal itu bagi Tuhanmu adalah suatu ketentuan yang sudah ditetapkan.

19:72

# ثُمَّ نُنَجِّى الَّذِيْنَ اتَّقَوْا وَّنَذَرُ الظّٰلِمِيْنَ فِيْهَا جِثِيًّا

tsumma nunajjii **al**la*dz*iina ittaqaw wana*dz*aru **al***zhzhaa*limiina fiih*aa* jitsiyy*aa***n**

Kemudian Kami akan menyelamatkan orang-orang yang bertakwa dan membiarkan orang-orang yang zalim di dalam (neraka) dalam keadaan berlutut.

19:73

# وَاِذَا تُتْلٰى عَلَيْهِمْ اٰيٰتُنَا بَيِّنٰتٍ قَالَ الَّذِيْنَ كَفَرُوْا لِلَّذِيْنَ اٰمَنُوْٓاۙ اَيُّ الْفَرِيْقَيْنِ خَيْرٌ مَّقَامًا وَّاَحْسَنُ نَدِيًّا

wa-i*dzaa* tutl*aa* 'alayhim *aa*y*aa*tun*aa* bayyin*aa*tin q*aa*la **al**la*dz*iina kafaruu lilla*dz*iina *aa*manuu ayyu **a**lfariiqayni khayrun maq*aa*man wa-a*h*sanu

Dan apabila dibacakan kepada mereka ayat-ayat Kami yang jelas (maksudnya), orang-orang yang kafir berkata kepada orang-orang yang beriman, “Manakah di antara kedua golongan yang lebih baik tempat tinggalnya dan lebih indah tempat pertemuan(nya)?”

19:74

# وَكَمْ اَهْلَكْنَا قَبْلَهُمْ مِّنْ قَرْنٍ هُمْ اَحْسَنُ اَثَاثًا وَّرِءْيًا

wakam ahlakn*aa* qablahum min qarnin hum a*h*sanu ats*aa*tsan wari/y*aa***n**

Dan berapa banyak umat (yang ingkar) yang telah Kami binasakan sebelum mereka, padahal mereka lebih bagus perkakas rumah tangganya dan (lebih sedap) dipandang mata.

19:75

# قُلْ مَنْ كَانَ فِى الضَّلٰلَةِ فَلْيَمْدُدْ لَهُ الرَّحْمٰنُ مَدًّا ەۚ حَتّٰىٓ اِذَا رَاَوْا مَا يُوْعَدُوْنَ اِمَّا الْعَذَابَ وَاِمَّا السَّاعَةَ ۗفَسَيَعْلَمُوْنَ مَنْ هُوَ شَرٌّ مَّكَانًا وَّاَضْعَفُ جُنْدًا

qul man k*aa*na fii **al***dhdh*al*aa*lati falyamdud lahu **al**rra*h*m*aa*nu maddan *h*att*aa* i*dzaa* ra-aw m*aa* yuu'aduuna imm*aa* **a**l'a*dzaa*ba wa-imm

Katakanlah (Muhammad), “Barangsiapa berada dalam kesesatan, maka biarlah Tuhan Yang Maha Pengasih memperpanjang (waktu) baginya; sehingga apabila mereka telah melihat apa yang diancamkan kepada mereka, baik azab maupun Kiamat, maka mereka akan mengetahui







19:76

# وَيَزِيْدُ اللّٰهُ الَّذِيْنَ اهْتَدَوْا هُدًىۗ وَالْبٰقِيٰتُ الصّٰلِحٰتُ خَيْرٌ عِنْدَ رَبِّكَ ثَوَابًا وَّخَيْرٌ مَّرَدًّا

wayaziidu **al**l*aa*hu **al**la*dz*iina ihtadaw hudan wa**a**lb*aa*qiy*aa*tu **al***shshaa*li*haa*tu khayrun 'inda rabbika tsaw*aa*ban wakhayrun maradd*aa*

Dan Allah akan menambah petunjuk kepada mereka yang telah mendapat petunjuk. Dan amal kebajikan yang kekal itu lebih baik pahalanya di sisi Tuhanmu dan lebih baik kesudahannya.

19:77

# اَفَرَاَيْتَ الَّذِيْ كَفَرَ بِاٰيٰتِنَا وَقَالَ لَاُوْتَيَنَّ مَالًا وَّوَلَدًا ۗ

afara-ayta **al**la*dz*ii kafara bi-*aa*y*aa*tin*aa* waq*aa*la lauutayanna m*aa*lan wawalad*aa***n**

Lalu apakah engkau telah melihat orang yang mengingkari ayat-ayat Kami dan dia mengatakan, “Pasti aku akan diberi harta dan anak.”

19:78

# اَطَّلَعَ الْغَيْبَ اَمِ اتَّخَذَ عِنْدَ الرَّحْمٰنِ عَهْدًا ۙ

a*ththh*ala'a **a**lghayba ami ittakha*dz*a 'inda **al**rra*h*m*aa*ni 'ahd*aa***n**

Adakah dia melihat yang gaib atau dia telah membuat perjanjian di sisi Tuhan Yang Maha Pengasih?

19:79

# كَلَّاۗ سَنَكْتُبُ مَا يَقُوْلُ وَنَمُدُّ لَهٗ مِنَ الْعَذَابِ مَدًّا ۙ

kall*aa* sanaktubu m*aa* yaquulu wanamuddu lahu mina **a**l'a*dzaa*bi madd*aa***n**

Sama sekali tidak! Kami akan menulis apa yang dia katakan, dan Kami akan memperpanjang azab untuknya secara sempurna,

19:80

# وَّنَرِثُهٗ مَا يَقُوْلُ وَيَأْتِيْنَا فَرْدًا

wanaritsuhu m*aa* yaquulu waya/tiin*aa* fard*aa***n**

dan Kami akan mewarisi apa yang dia katakan itu, dan dia akan datang kepada Kami seorang diri.

19:81

# وَاتَّخَذُوْا مِنْ دُوْنِ اللّٰهِ اٰلِهَةً لِّيَكُوْنُوْا لَهُمْ عِزًّا ۙ

wa**i**ttakha*dz*uu min duuni **al**l*aa*hi *aa*lihatan liyakuunuu lahum 'izz*aa***n**

Dan mereka telah memilih tuhan-tuhan selain Allah, agar tuhan-tuhan itu menjadi pelindung bagi mereka.

19:82

# كَلَّا ۗسَيَكْفُرُوْنَ بِعِبَادَتِهِمْ وَيَكُوْنُوْنَ عَلَيْهِمْ ضِدًّا ࣖ

kall*aa* sayakfuruuna bi'ib*aa*datihim wayakuunuuna 'alayhim *dh*idd*aa***n**

Sama sekali tidak! Kelak mereka (sesembahan) itu akan mengingkari penyembahan mereka terhadapnya, dan akan menjadi musuh bagi mereka.

19:83

# اَلَمْ تَرَ اَنَّآ اَرْسَلْنَا الشَّيٰطِيْنَ عَلَى الْكٰفِرِيْنَ تَؤُزُّهُمْ اَزًّا ۙ

alam tara ann*aa* arsaln*aa* **al**sysyay*aath*iina 'al*aa* **a**lk*aa*firiina tauzzuhum azz*aa***n**

Tidakkah engkau melihat, bahwa sesungguhnya Kami telah mengutus setan-setan itu kepada orang-orang kafir untuk mendorong mereka (berbuat maksiat) dengan sungguh-sungguh?

19:84

# فَلَا تَعْجَلْ عَلَيْهِمْۗ اِنَّمَا نَعُدُّ لَهُمْ عَدًّا ۗ

fal*aa* ta'jal 'alayhim innam*aa* na'uddu lahum 'add*aa***n**

Maka janganlah engkau (Muhammad) tergesa-gesa (memintakan azab) terhadap mereka, karena Kami menghitung dengan hitungan teliti (datangnya hari siksaan) untuk mereka.

19:85

# يَوْمَ نَحْشُرُ الْمُتَّقِيْنَ اِلَى الرَّحْمٰنِ وَفْدًا

yawma na*h*syuru **a**lmuttaqiina il*aa* **al**rra*h*m*aa*ni wafd*aa***n**

(Ingatlah) pada hari (ketika) Kami mengumpulkan orang-orang yang bertakwa kepada (Allah) Yang Maha Pengasih, bagaikan kafilah yang terhormat,

19:86

# وَنَسُوْقُ الْمُجْرِمِيْنَ اِلٰى جَهَنَّمَ وِرْدًا ۘ

wanasuuqu **a**lmujrimiina il*aa* jahannama wird*aa***n**

dan Kami akan menggiring orang yang durhaka ke neraka Jahanam dalam keadaan dahaga.

19:87

# لَا يَمْلِكُوْنَ الشَّفَاعَةَ اِلَّا مَنِ اتَّخَذَ عِنْدَ الرَّحْمٰنِ عَهْدًا ۘ

l*aa* yamlikuuna **al**sysyaf*aa*'ata ill*aa* mani ittakha*dz*a 'inda **al**rra*h*m*aa*ni 'ahd*aa***n**

Mereka tidak berhak mendapat syafaat, (pertolongan) kecuali orang yang telah mengadakan perjanjian di sisi (Allah) Yang Maha Pengasih.

19:88

# وَقَالُوا اتَّخَذَ الرَّحْمٰنُ وَلَدًا ۗ

waq*aa*luu ittakha*dz*a **al**rra*h*m*aa*nu walad*aa***n**

Dan mereka berkata, “(Allah) Yang Maha Pengasih mempunyai anak.”

19:89

# لَقَدْ جِئْتُمْ شَيْـًٔا اِدًّا ۙ

laqad ji/tum syay-an idd*aa***n**

Sungguh, kamu telah membawa sesuatu yang sangat mungkar,

19:90

# تَكَادُ السَّمٰوٰتُ يَتَفَطَّرْنَ مِنْهُ وَتَنْشَقُّ الْاَرْضُ وَتَخِرُّ الْجِبَالُ هَدًّا ۙ

tak*aa*du **al**ssam*aa*w*aa*tu yatafa*ththh*arna minhu watansyaqqu **a**l-ar*dh*u watakhirru **a**ljib*aa*lu hadd*aa***n**

hampir saja langit pecah, dan bumi terbelah, dan gunung-gunung runtuh, (karena ucapan itu),

19:91

# اَنْ دَعَوْا لِلرَّحْمٰنِ وَلَدًا ۚ

an da'aw li**l**rra*h*m*aa*ni walad*aa***n**

karena mereka menganggap (Allah) Yang Maha Pengasih mempunyai anak.

19:92

# وَمَا يَنْۢبَغِيْ لِلرَّحْمٰنِ اَنْ يَّتَّخِذَ وَلَدًا ۗ

wam*aa* yanbaghii li**l**rra*h*m*aa*ni an yattakhi*dz*a walad*aa***n**

Dan tidak mungkin bagi (Allah) Yang Maha Pengasih mempunyai anak.

19:93

# اِنْ كُلُّ مَنْ فِى السَّمٰوٰتِ وَالْاَرْضِ اِلَّآ اٰتِى الرَّحْمٰنِ عَبْدًا ۗ

in kullu man fii **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i ill*aa* *aa*tii **al**rra*h*m*aa*ni 'abd*aa***n**

Tidak ada seorang pun di langit dan di bumi, melainkan akan datang kepada (Allah) Yang Maha Pengasih sebagai seorang hamba.

19:94

# لَقَدْ اَحْصٰىهُمْ وَعَدَّهُمْ عَدًّا ۗ

laqad a*hsaa*hum wa'addahum 'add*aa***n**

Dia (Allah) benar-benar telah menentukan jumlah mereka dan menghitung mereka dengan hitungan yang teliti.

19:95

# وَكُلُّهُمْ اٰتِيْهِ يَوْمَ الْقِيٰمَةِ فَرْدًا

wakulluhum *aa*tiihi yawma **a**lqiy*aa*mati fard*aa***n**

Dan setiap orang dari mereka akan datang kepada Allah sendiri-sendiri pada hari Kiamat.

19:96

# اِنَّ الَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِ سَيَجْعَلُ لَهُمُ الرَّحْمٰنُ وُدًّا

inna **al**la*dz*iina *aa*manuu wa'amiluu **al***shshaa*li*haa*ti sayaj'alu lahumu **al**rra*h*m*aa*nu wudd*aa***n**

Sungguh, orang-orang yang beriman dan mengerjakan kebajikan, kelak (Allah) Yang Maha Pengasih akan menanamkan rasa kasih sayang (dalam hati mereka).

19:97

# فَاِنَّمَا يَسَّرْنٰهُ بِلِسَانِكَ لِتُبَشِّرَ بِهِ الْمُتَّقِيْنَ وَتُنْذِرَ بِهٖ قَوْمًا لُّدًّا

fa-innam*aa* yassarn*aa*hu bilis*aa*nika litubasysyira bihi **a**lmuttaqiina watun*dz*ira bihi qawman ludd*aa***n**

Maka sungguh, telah Kami mudahkan (Al-Qur'an) itu dengan bahasamu (Muhammad), agar dengan itu engkau dapat memberi kabar gembira kepada orang-orang yang bertakwa, dan agar engkau dapat memberi peringatan kepada kaum yang membangkang.

19:98

# وَكَمْ اَهْلَكْنَا قَبْلَهُمْ مِّنْ قَرْنٍۗ هَلْ تُحِسُّ مِنْهُمْ مِّنْ اَحَدٍ اَوْ تَسْمَعُ لَهُمْ رِكْزًا ࣖ

wakam ahlakn*aa* qablahum min qarnin hal tu*h*issu minhum min a*h*adin aw tasma'u lahum rikz*aa***n**

Dan berapa banyak umat yang telah Kami binasakan sebelum mereka. Adakah engkau (Muhammad) melihat salah seorang dari mereka atau engkau mendengar bisikan mereka?

<!--EndFragment-->