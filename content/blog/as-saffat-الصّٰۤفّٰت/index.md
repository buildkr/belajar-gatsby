---
title: (37) As-Saffat - الصّٰۤفّٰت
date: 2021-10-27T03:56:59.969Z
ayat: 37
description: "Jumlah Ayat: 182 / Arti: Barisan-Barisan"
---
<!--StartFragment-->

37:1

# وَالصّٰۤفّٰتِ صَفًّاۙ

wa**al***shshaa*ff*aa*ti *sh*aff*aa***n**

Demi (rombongan malaikat) yang berbaris bersaf-saf,

37:2

# فَالزّٰجِرٰتِ زَجْرًاۙ

fa**al**zz*aa*jir*aa*ti zajr*aa***n**

demi (rombongan) yang mencegah dengan sungguh-sungguh,

37:3

# فَالتّٰلِيٰتِ ذِكْرًاۙ

fa**al**tt*aa*liy*aa*ti *dz*ikr*aa***n**

demi (rombongan) yang membacakan peringatan,

37:4

# اِنَّ اِلٰهَكُمْ لَوَاحِدٌۗ

inna il*aa*hakum law*aah*id**un**

sungguh, Tuhanmu benar-benar Esa.

37:5

# رَبُّ السَّمٰوٰتِ وَالْاَرْضِ وَمَا بَيْنَهُمَا وَرَبُّ الْمَشَارِقِۗ

rabbu **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i wam*aa* baynahum*aa* warabbu **a**lmasy*aa*riq**i**

Tuhan langit dan bumi dan apa yang berada di antara keduanya dan Tuhan tempat-tempat terbitnya matahari.

37:6

# اِنَّا زَيَّنَّا السَّمَاۤءَ الدُّنْيَا بِزِيْنَةِ ِۨالْكَوَاكِبِۙ

inn*aa* zayyann*aa* **al**ssam*aa*-a **al**dduny*aa* biziinatin **a**lkaw*aa*kib**i**

Sesungguhnya Kami telah menghias langit dunia (yang terdekat), dengan hiasan bintang-bintang.

37:7

# وَحِفْظًا مِّنْ كُلِّ شَيْطٰنٍ مَّارِدٍۚ

wa*h*if*zh*an min kulli syay*thaa*nin m*aa*rid**in**

Dan (Kami) telah menjaganya dari setiap setan yang durhaka,

37:8

# لَا يَسَّمَّعُوْنَ اِلَى الْمَلَاِ الْاَعْلٰى وَيُقْذَفُوْنَ مِنْ كُلِّ جَانِبٍۖ

l*aa* yassamma'uuna il*aa* **a**lmala-i **a**l-a'l*aa* wayuq*dz*afuuna min kulli j*aa*nib**in**

mereka (setan-setan itu) tidak dapat mendengar (pembicaraan) para malaikat dan mereka dilempari dari segala penjuru,

37:9

# دُحُوْرًا وَّلَهُمْ عَذَابٌ وَّاصِبٌ

du*h*uuran walahum 'a*dzaa*bun w*aas*ib**un**

untuk mengusir mereka dan mereka akan mendapat azab yang kekal,

37:10

# اِلَّا مَنْ خَطِفَ الْخَطْفَةَ فَاَتْبَعَهٗ شِهَابٌ ثَاقِبٌ

ill*aa* man kha*th*ifa **a**lkha*th*fata fa-atba'ahu syih*aa*bun ts*aa*qib**un**

kecuali (setan) yang mencuri (pembicaraan); maka ia dikejar oleh bintang yang menyala.

37:11

# فَاسْتَفْتِهِمْ اَهُمْ اَشَدُّ خَلْقًا اَمْ مَّنْ خَلَقْنَا ۗاِنَّا خَلَقْنٰهُمْ مِّنْ طِيْنٍ لَّازِبٍ

fa**i**staftihim ahum asyaddu khalqan am man khalaqn*aa* inn*aa* khalaqn*aa*hum min *th*iinin l*aa*zib**in**

Maka tanyakanlah kepada mereka (musyrik Mekah), “Apakah penciptaan mereka yang lebih sulit ataukah apa yang telah Kami ciptakan itu?” Sesungguhnya Kami telah menciptakan mereka dari tanah liat.

37:12

# بَلْ عَجِبْتَ وَيَسْخَرُوْنَ ۖ

bal 'ajibta wayaskharuun**a**

Bahkan engkau (Muhammad) menjadi heran (terhadap keingkaran mereka) dan mereka menghinakan (engkau).

37:13

# وَاِذَا ذُكِّرُوْا لَا يَذْكُرُوْنَ ۖ

wa-i*dzaa* *dz*ukkiruu l*aa* ya*dz*kuruun**a**

Dan apabila mereka diberi peringatan, mereka tidak mengindahkannya.

37:14

# وَاِذَا رَاَوْا اٰيَةً يَّسْتَسْخِرُوْنَۖ

wa-i*dzaa* ra-aw *aa*yatan yastaskhiruun**a**

Dan apabila mereka melihat suatu tanda (kebesaran) Allah, mereka memperolok-olokkan.

37:15

# وَقَالُوْٓا اِنْ هٰذَآ اِلَّا سِحْرٌ مُّبِيْنٌ ۚ

waq*aa*luu in h*aadzaa* ill*aa* si*h*run mubiin**un**

Dan mereka berkata, “Ini tidak lain hanyalah sihir yang nyata.

37:16

# ءَاِذَا مِتْنَا وَكُنَّا تُرَابًا وَّعِظَامًا ءَاِنَّا لَمَبْعُوْثُوْنَۙ

a-i*dzaa* mitn*aa* wakunn*aa* tur*aa*ban wa'i*zhaa*man a-inn*aa* lamab'uutsuun**a**

Apabila kami telah mati dan telah menjadi tanah dan tulang-belulang, apakah benar kami akan dibangkitkan (kembali)?

37:17

# اَوَاٰبَاۤؤُنَا الْاَوَّلُوْنَۗ

awa *aa*b*aa*un*aa* **a**l-awwaluun**a**

dan apakah nenek moyang kami yang telah terdahulu (akan dibangkitkan pula)?”

37:18

# قُلْ نَعَمْ وَاَنْتُمْ دَاخِرُوْنَۚ

qul na'am wa-antum d*aa*khiruun**a**

Katakanlah (Muhammad), “Ya, dan kamu akan terhina.”

37:19

# فَاِنَّمَا هِيَ زَجْرَةٌ وَّاحِدَةٌ فَاِذَا هُمْ يَنْظُرُوْنَ

fa-innam*aa* hiya zajratun w*aah*idatun fa-i*dzaa* hum yan*zh*uruun**a**

Maka sesungguhnya kebangkitan itu hanya dengan satu teriakan saja; maka seketika itu mereka melihatnya.

37:20

# وَقَالُوْا يٰوَيْلَنَا هٰذَا يَوْمُ الدِّيْنِ

waq*aa*luu y*aa* waylan*aa* h*aadzaa* yawmu **al**ddiin**i**

Dan mereka berkata, “Alangkah celaka kami! (Kiranya) inilah hari pembalasan itu.”

37:21

# هٰذَا يَوْمُ الْفَصْلِ الَّذِيْ كُنْتُمْ بِهٖ تُكَذِّبُوْنَ ࣖ

h*aadzaa* yawmu **a**lfa*sh*li **al**la*dz*ii kuntum bihi tuka*dzdz*ibuun**a**

Inilah hari keputusan yang dahulu kamu dustakan.

37:22

# اُحْشُرُوا الَّذِيْنَ ظَلَمُوْا وَاَزْوَاجَهُمْ وَمَا كَانُوْا يَعْبُدُوْنَ ۙ

u*h*syuruu **al**la*dz*iina *zh*alamuu wa-azw*aa*jahum wam*aa* k*aa*nuu ya'buduun**a**

(Diperintahkan kepada malaikat), “Kumpulkanlah orang-orang yang zalim beserta teman sejawat mereka dan apa yang dahulu mereka sembah,

37:23

# مِنْ دُوْنِ اللّٰهِ فَاهْدُوْهُمْ اِلٰى صِرَاطِ الْجَحِيْمِ

min duuni **al**l*aa*hi fa**i**hduuhum il*aa* *sh*ir*aath*i **a**lja*h*iim**i**

selain Allah, lalu tunjukkanlah kepada mereka jalan ke neraka.

37:24

# وَقِفُوْهُمْ اِنَّهُمْ مَّسْـُٔوْلُوْنَ ۙ

waqifuuhum innahum masuuluun**a**

Tahanlah mereka (di tempat perhentian), sesungguhnya mereka akan ditanya,

37:25

# مَا لَكُمْ لَا تَنَاصَرُوْنَ

m*aa* lakum l*aa* tan*aas*aruun**a**

”Mengapa kamu tidak tolong-menolong?”

37:26

# بَلْ هُمُ الْيَوْمَ مُسْتَسْلِمُوْنَ

bal humu **a**lyawma mustaslimuun**a**

Bahkan mereka pada hari itu menyerah (kepada keputusan Allah).

37:27

# وَاَقْبَلَ بَعْضُهُمْ عَلٰى بَعْضٍ يَّتَسَاۤءَلُوْنَ

wa-aqbala ba'*dh*uhum 'al*aa* ba'*dh*in yatas*aa*-aluun**a**

Dan sebagian mereka menghadap kepada sebagian yang lain saling berbantah-bantahan.

37:28

# قَالُوْٓا اِنَّكُمْ كُنْتُمْ تَأْتُوْنَنَا عَنِ الْيَمِيْنِ

q*aa*luu innakum kuntum ta/tuunan*aa* 'ani **a**lyamiin**i**

Sesungguhnya (pengikut-pengikut) mereka berkata (kepada pemimpin-pemimpin mereka), “Kamulah yang dahulu datang kepada kami dari kanan.”

37:29

# قَالُوْا بَلْ لَّمْ تَكُوْنُوْا مُؤْمِنِيْنَۚ

q*aa*luu bal lam takuunuu mu/miniin**a**

(Pemimpin-pemimpin) mereka menjawab, “(Tidak), bahkan kamulah yang tidak (mau) menjadi orang mukmin,

37:30

# وَمَا كَانَ لَنَا عَلَيْكُمْ مِّنْ سُلْطٰنٍۚ بَلْ كُنْتُمْ قَوْمًا طٰغِيْنَ

wam*aa* k*aa*na lan*aa* 'alaykum min *sh*ul*thaa*nin bal kuntum qawman *thaa*ghiin**a**

sedangkan kami tidak berkuasa terhadapmu, bahkan kamu menjadi kaum yang melampaui batas.

37:31

# فَحَقَّ عَلَيْنَا قَوْلُ رَبِّنَآ ۖاِنَّا لَذَاۤىِٕقُوْنَ

fa*h*aqqa 'alayn*aa* qawlu rabbin*aa* inn*aa* la*dzaa*-iquun**a**

Maka pantas putusan (azab) Tuhan menimpa kita; pasti kita akan merasakan (azab itu).

37:32

# فَاَغْوَيْنٰكُمْ اِنَّا كُنَّا غٰوِيْنَ

fa-aghwayn*aa*kum inn*aa* kunn*aa* gh*aa*wiin**a**

Maka kami telah menyesatkan kamu, sesungguhnya kami sendiri, orang-orang yang sesat.”

37:33

# فَاِنَّهُمْ يَوْمَىِٕذٍ فِى الْعَذَابِ مُشْتَرِكُوْنَ

fa-innahum yawma-i*dz*in fii **a**l'a*dzaa*bi musytarikuun**a**

Maka sesungguhnya mereka pada hari itu bersama-sama merasakan azab.

37:34

# اِنَّا كَذٰلِكَ نَفْعَلُ بِالْمُجْرِمِيْنَ

inn*aa* ka*dzaa*lika naf'alu bi**a**lmujrimiin**a**

Sungguh, demikianlah Kami memperlakukan terhadap orang-orang yang berbuat dosa.

37:35

# اِنَّهُمْ كَانُوْٓا اِذَا قِيْلَ لَهُمْ لَآ اِلٰهَ اِلَّا اللّٰهُ يَسْتَكْبِرُوْنَ ۙ

innahum k*aa*nuu i*dzaa* qiila lahum l*aa* il*aa*ha ill*aa* **al**l*aa*hu yastakbiruun**a**

Sungguh, dahulu apabila dikatakan kepada mereka, “La ilaha illallah” (Tidak ada tuhan selain Allah), mereka menyombongkan diri,

37:36

# وَيَقُوْلُوْنَ اَىِٕنَّا لَتَارِكُوْٓا اٰلِهَتِنَا لِشَاعِرٍ مَّجْنُوْنٍ ۗ

wayaquuluuna a-inn*aa* lat*aa*rikuu *aa*lihatin*aa* lisy*aa*'irin majnuun**in**

dan mereka berkata, “Apakah kami harus meninggalkan sesembahan kami karena seorang penyair gila?”

37:37

# بَلْ جَاۤءَ بِالْحَقِّ وَصَدَّقَ الْمُرْسَلِيْنَ

bal j*aa*-a bi**a**l*h*aqqi wa*sh*addaqa **a**lmursaliin**a**

Padahal dia (Muhammad) datang dengan membawa kebenaran dan membenarkan rasul-rasul (sebelumnya).

37:38

# اِنَّكُمْ لَذَاۤىِٕقُوا الْعَذَابِ الْاَلِيْمِ ۚ

innakum la*dzaa*-iquu **a**l'a*dzaa*bi **a**l-aliim**i**

Sungguh, kamu pasti akan merasakan azab yang pedih.

37:39

# وَمَا تُجْزَوْنَ اِلَّا مَا كُنْتُمْ تَعْمَلُوْنَۙ

wam*aa* tujzawna ill*aa* m*aa* kuntum ta'maluun**a**

Dan kamu tidak diberi balasan melainkan terhadap apa yang telah kamu kerjakan,

37:40

# اِلَّا عِبَادَ اللّٰهِ الْمُخْلَصِيْنَ

ill*aa* 'ib*aa*da **al**l*aa*hi **a**lmukhla*sh*iin**a**

tetapi hamba-hamba Allah yang dibersihkan (dari dosa),

37:41

# اُولٰۤىِٕكَ لَهُمْ رِزْقٌ مَّعْلُوْمٌۙ

ul*aa*-ika lahum rizqun ma'luum**un**

mereka itu memperoleh rezeki yang sudah ditentukan,

37:42

# فَوَاكِهُ ۚوَهُمْ مُّكْرَمُوْنَۙ

faw*aa*kihu wahum mukramuun**a**

(yaitu) buah-buahan. Dan mereka orang yang dimuliakan,

37:43

# فِيْ جَنّٰتِ النَّعِيْمِۙ

fii jann*aa*ti **al**nna'iim**i**

di dalam surga-surga yang penuh kenikmatan,

37:44

# عَلٰى سُرُرٍ مُّتَقٰبِلِيْنَ

'al*aa* sururin mutaq*aa*biliin**a**

(mereka duduk) berhadap-hadapan di atas dipan-dipan.

37:45

# يُطَافُ عَلَيْهِمْ بِكَأْسٍ مِّنْ مَّعِيْنٍۢ ۙ

yu*thaa*fu 'alayhim bika/sin min ma'iin**in**

Kepada mereka diedarkan gelas (yang berisi air) dari mata air (surga),

37:46

# بَيْضَاۤءَ لَذَّةٍ لِّلشّٰرِبِيْنَۚ

bay*dhaa*-a la*dzdz*atin li**l**sysy*aa*ribiin**a**

(warnanya) putih bersih, sedap rasanya bagi orang-orang yang minum.

37:47

# لَا فِيْهَا غَوْلٌ وَّلَا هُمْ عَنْهَا يُنْزَفُوْنَ

l*aa* fiih*aa* ghawlun wal*aa* hum 'anh*aa* yunzafuun**a**

Tidak ada di dalamnya (unsur) yang memabukkan dan mereka tidak mabuk karenanya.

37:48

# وَعِنْدَهُمْ قٰصِرٰتُ الطَّرْفِ عِيْنٌ ۙ

wa'indahum q*aas*ir*aa*tu **al***ththh*arfi 'iin**un**

Dan di sisi mereka ada (bidadari-bidadari) yang bermata indah, dan membatasi pandangannya,

37:49

# كَاَنَّهُنَّ بَيْضٌ مَّكْنُوْنٌ

ka-annahunna bay*dh*un maknuun**un**

seakan-akan mereka adalah telur yang tersimpan dengan baik.

37:50

# فَاَقْبَلَ بَعْضُهُمْ عَلٰى بَعْضٍ يَّتَسَاۤءَلُوْنَ

fa-aqbala ba'*dh*uhum 'al*aa* ba'*dh*in yatas*aa*-aluun**a**

Lalu mereka berhadap-hadapan satu sama lain sambil bercakap-cakap.

37:51

# قَالَ قَاۤىِٕلٌ مِّنْهُمْ اِنِّيْ كَانَ لِيْ قَرِيْنٌۙ

q*aa*la q*aa*-ilun minhum innii k*aa*na lii qariin**un**

Berkatalah salah seorang di antara mereka, “Sesungguhnya aku dahulu (di dunia) pernah mempunyai seorang teman,

37:52

# يَّقُوْلُ اَىِٕنَّكَ لَمِنَ الْمُصَدِّقِيْنَ

yaquulu a-innaka lamina **a**lmu*sh*addiqiin**a**

yang berkata, “Apakah sesungguhnya kamu termasuk orang-orang yang membenarkan (hari berbangkit)?

37:53

# ءَاِذَا مِتْنَا وَكُنَّا تُرَابًا وَّعِظَامًا ءَاِنَّا لَمَدِيْنُوْنَ

a-i*dzaa* mitn*aa* wakunn*aa* tur*aa*ban wa'i*zhaa*man a-inn*aa* lamadiinuun**a**

Apabila kita telah mati dan telah menjadi tanah dan tulang-belulang, apakah kita benar-benar (akan dibangkitkan) untuk diberi pembalasan?”

37:54

# قَالَ هَلْ اَنْتُمْ مُّطَّلِعُوْنَ

q*aa*la hal antum mu*ththh*ali'uun**a**

Dia berkata, “Maukah kamu meninjau (temanku itu)?”

37:55

# فَاطَّلَعَ فَرَاٰهُ فِيْ سَوَاۤءِ الْجَحِيْمِ

fa**i***ththh*ala'a fara*aa*hu fii saw*aa*-i **a**lja*h*iim**i**

Maka dia meninjaunya, lalu dia melihat (teman)nya itu di tengah-tengah neraka yang menyala-nyala.

37:56

# قَالَ تَاللّٰهِ اِنْ كِدْتَّ لَتُرْدِيْنِ ۙ

q*aa*la ta**al**l*aa*hi in kidta laturdiin**i**

Dia berkata, “Demi Allah, engkau hampir saja mencelakakanku,

37:57

# وَلَوْلَا نِعْمَةُ رَبِّيْ لَكُنْتُ مِنَ الْمُحْضَرِيْنَ

walawl*aa* ni'matu rabbii lakuntu mina **a**lmu*hd*ariin**a**

dan sekiranya bukan karena nikmat Tuhanku pastilah aku termasuk orang-orang yang diseret (ke neraka).”

37:58

# اَفَمَا نَحْنُ بِمَيِّتِيْنَۙ

afam*aa* na*h*nu bimayyitiin**a**

Maka apakah kita tidak akan mati?

37:59

# اِلَّا مَوْتَتَنَا الْاُوْلٰى وَمَا نَحْنُ بِمُعَذَّبِيْنَ

ill*aa* mawtatan*aa* **a**l-uul*aa* wam*aa* na*h*nu bimu'a*dzdz*abiin**a**

Kecuali kematian kita yang pertama saja (di dunia), dan kita tidak akan diazab (di akhirat ini)?”

37:60

# اِنَّ هٰذَا لَهُوَ الْفَوْزُ الْعَظِيْمُ

inna h*aadzaa* lahuwa **a**lfawzu **a**l'a*zh*iim**u**

Sungguh, ini benar-benar kemenangan yang agung.

37:61

# لِمِثْلِ هٰذَا فَلْيَعْمَلِ الْعٰمِلُوْنَ

limitsli h*aadzaa* falya'mali **a**l'*aa*miluun**a**

Untuk (kemenangan) serupa ini, hendaklah beramal orang-orang yang mampu beramal.

37:62

# اَذٰلِكَ خَيْرٌ نُّزُلًا اَمْ شَجَرَةُ الزَّقُّوْمِ

a*dzaa*lika khayrun nuzulan am syajaratu **al**zzaqquum**i**

Apakah (makanan surga) itu hidangan yang lebih baik ataukah pohon zaqqum.

37:63

# اِنَّا جَعَلْنٰهَا فِتْنَةً لِّلظّٰلِمِيْنَ

inn*aa* ja'aln*aa*h*aa* fitnatan li**l***zhzhaa*limiin**a**

Sungguh, Kami menjadikannya (pohon zaqqum itu) sebagai azab bagi orang-orang zalim.

37:64

# اِنَّهَا شَجَرَةٌ تَخْرُجُ فِيْٓ اَصْلِ الْجَحِيْمِۙ

innah*aa* syajaratun takhruju fii a*sh*li **a**lja*h*iim**i**

Sungguh, itu adalah pohon yang keluar dari dasar neraka Jahim,

37:65

# طَلْعُهَا كَاَنَّهٗ رُءُوْسُ الشَّيٰطِيْنِ

*th*al'uh*aa* ka-annahu ruuusu **al**sysyay*aath*iin**i**

Mayangnya seperti kepala-kepala setan.

37:66

# فَاِنَّهُمْ لَاٰكِلُوْنَ مِنْهَا فَمَالِـُٔوْنَ مِنْهَا الْبُطُوْنَۗ

fa-innahum la*aa*kiluuna minh*aa* fam*aa*li-uuna minh*aa* **a**lbu*th*uun**a**

Maka sungguh, mereka benar-benar memakan sebagian darinya (buah pohon itu), dan mereka memenuhi perutnya dengan buahnya (zaqqum).

37:67

# ثُمَّ اِنَّ لَهُمْ عَلَيْهَا لَشَوْبًا مِّنْ حَمِيْمٍۚ

tsumma inna lahum 'alayh*aa* lasyawban min *h*amiim**in**

Kemudian sungguh, setelah makan (buah zaqqum) mereka mendapat minuman yang dicampur dengan air yang sangat panas.

37:68

# ثُمَّ اِنَّ مَرْجِعَهُمْ لَاِلَى الْجَحِيْمِ

tsumma inna marji'ahum la-il*aa* **a**lja*h*iim**i**

Kemudian pasti tempat kembali mereka ke neraka Jahim.

37:69

# اِنَّهُمْ اَلْفَوْا اٰبَاۤءَهُمْ ضَاۤلِّيْنَۙ

innahum **a**lfaw *aa*b*aa*-ahum *daa*lliin**a**

Sesungguhnya mereka mendapati nenek moyang mereka dalam keadaan sesat,

37:70

# فَهُمْ عَلٰٓى اٰثٰرِهِمْ يُهْرَعُوْنَ

fahum 'al*aa* *aa*ts*aa*rihim yuhra'uun**a**

lalu mereka tergesa-gesa mengikuti jejak (nenek moyang) mereka.

37:71

# وَلَقَدْ ضَلَّ قَبْلَهُمْ اَكْثَرُ الْاَوَّلِيْنَۙ

walaqad *dh*alla qablahum aktsaru **a**l-awwaliin**a**

Dan sungguh, sebelum mereka (Suku Quraisy), telah sesat sebagian besar dari orang-orang yang dahulu,

37:72

# وَلَقَدْ اَرْسَلْنَا فِيْهِمْ مُّنْذِرِيْنَ

walaqad arsaln*aa* fiihim mun*dz*iriin**a**

dan sungguh, Kami telah mengutus (rasul) pemberi peringatan di kalangan mereka.

37:73

# فَانْظُرْ كَيْفَ كَانَ عَاقِبَةُ الْمُنْذَرِيْنَۙ

fa**u**n*zh*ur kayfa k*aa*na '*aa*qibatu **a**lmun*dz*ariin**a**

Maka perhatikanlah bagaimana kesudahan orang-orang yang diberi peringatan itu,

37:74

# اِلَّا عِبَادَ اللّٰهِ الْمُخْلَصِيْنَ ࣖ

ill*aa* 'ib*aa*da **al**l*aa*hi **a**lmukhla*sh*iin**a**

kecuali hamba-hamba Allah yang disucikan (dari dosa).

37:75

# وَلَقَدْ نَادٰىنَا نُوْحٌ فَلَنِعْمَ الْمُجِيْبُوْنَۖ

walaqad n*aa*d*aa*n*aa* nuu*h*un falani'ma **a**lmujiibuun**a**

Dan sungguh, Nuh telah berdoa kepada Kami, maka sungguh, Kamilah sebaik-baik yang memperkenankan doa.

37:76

# وَنَجَّيْنٰهُ وَاَهْلَهٗ مِنَ الْكَرْبِ الْعَظِيْمِۖ

wanajjayn*aa*hu wa-ahlahu mina **a**lkarbi **a**l'a*zh*iim**i**

Kami telah menyelamatkan dia dan pengikutnya dari bencana yang besar.

37:77

# وَجَعَلْنَا ذُرِّيَّتَهٗ هُمُ الْبٰقِيْنَ

waja'aln*aa* *dz*urriyyatahu humu **a**lb*aa*qiin**a**

Dan Kami jadikan anak cucunya orang-orang yang melanjutkan keturunan.

37:78

# وَتَرَكْنَا عَلَيْهِ فِى الْاٰخِرِيْنَ ۖ

watarakn*aa* 'alayhi fii **a**l-*aa*khiriin**a**

Dan Kami abadikan untuk Nuh (pujian) di kalangan orang-orang yang datang kemudian;

37:79

# سَلٰمٌ عَلٰى نُوْحٍ فِى الْعٰلَمِيْنَ

sal*aa*mun 'al*aa* nuu*h*in fii **a**l'*aa*lamiin**a**

”Kesejahteraan (Kami limpahkan) atas Nuh di seluruh alam.”

37:80

# اِنَّا كَذٰلِكَ نَجْزِى الْمُحْسِنِيْنَ

inn*aa* ka*dzaa*lika najzii **a**lmu*h*siniin**a**

Sungguh, demikianlah Kami memberi balasan kepada orang-orang yang berbuat baik.

37:81

# اِنَّهٗ مِنْ عِبَادِنَا الْمُؤْمِنِيْنَ

innahu min 'ib*aa*din*aa* **a**lmu/miniin**a**

Sungguh, dia termasuk di antara hamba-hamba Kami yang beriman.

37:82

# ثُمَّ اَغْرَقْنَا الْاٰخَرِيْنَ

tsumma aghraqn*aa* **a**l-*aa*khariin**a**

Kemudian Kami tenggelamkan yang lain.

37:83

# وَاِنَّ مِنْ شِيْعَتِهٖ لَاِبْرٰهِيْمَ ۘ

wa-inna min syii'atihi la-ibr*aa*hiim**a**

Dan sungguh, Ibrahim termasuk golongannya (Nuh).

37:84

# اِذْ جَاۤءَ رَبَّهٗ بِقَلْبٍ سَلِيْمٍۙ

i*dz* j*aa*-a rabbahu biqalbin saliim**in**

(Ingatlah) ketika dia datang kepada Tuhannya dengan hati yang suci,

37:85

# اِذْ قَالَ لِاَبِيْهِ وَقَوْمِهٖ مَاذَا تَعْبُدُوْنَ ۚ

i*dz* q*aa*la li-abiihi waqawmihi m*aatsaa* ta'buduun**a**

(ingatlah) ketika dia berkata kepada ayahnya dan kaumnya, “Apakah yang kamu sembah itu?

37:86

# اَىِٕفْكًا اٰلِهَةً دُوْنَ اللّٰهِ تُرِيْدُوْنَۗ

a-ifkan *aa*lihatan duuna **al**l*aa*hi turiiduun**a**

Apakah kamu menghendaki kebohongan dengan sesembahan selain Allah itu?

37:87

# فَمَا ظَنُّكُمْ بِرَبِّ الْعٰلَمِيْنَ

fam*aa* *zh*annukum birabbi **a**l'*aa*lamiin**a**

Maka bagaimana anggapanmu terhadap Tuhan seluruh alam?”

37:88

# فَنَظَرَ نَظْرَةً فِى النُّجُوْمِۙ

fana*zh*ara na*zh*ratan fii **al**nnujuum**i**

Lalu dia memandang sekilas ke bintang-bintang,

37:89

# فَقَالَ اِنِّيْ سَقِيْمٌ

faq*aa*la innii saqiim**un**

kemudian dia (Ibrahim) berkata, “Sesungguhnya aku sakit.”

37:90

# فَتَوَلَّوْا عَنْهُ مُدْبِرِيْنَ

fatawallaw 'anhu mudbiriin**a**

Lalu mereka berpaling dari dia dan pergi meninggalkannya.

37:91

# فَرَاغَ اِلٰٓى اٰلِهَتِهِمْ فَقَالَ اَلَا تَأْكُلُوْنَۚ

far*aa*gha il*aa* *aa*lihatihim faq*aa*la **a**l*aa* ta/kuluun**a**

Kemudian dia (Ibrahim) pergi dengan diam-diam kepada berhala-berhala mereka; lalu dia berkata, “Mengapa kamu tidak makan?

37:92

# مَا لَكُمْ لَا تَنْطِقُوْنَ

m*aa* lakum l*aa* tan*th*iquun**a**

Mengapa kamu tidak menjawab?”

37:93

# فَرَاغَ عَلَيْهِمْ ضَرْبًا ۢبِالْيَمِيْنِ

far*aa*gha 'alayhim *dh*arban bi**a**lyamiin**i**

Lalu dihadapinya (berhala-berhala) itu sambil memukulnya dengan tangan kanannya.

37:94

# فَاَقْبَلُوْٓا اِلَيْهِ يَزِفُّوْنَ

fa-aqbaluu ilayhi yaziffuun**a**

Kemudian mereka (kaumnya) datang bergegas kepadanya.

37:95

# قَالَ اَتَعْبُدُوْنَ مَا تَنْحِتُوْنَۙ

q*aa*la ata'buduuna m*aa* tan*h*ituun**a**

Dia (Ibrahim) berkata, “Apakah kamu menyembah patung-patung yang kamu pahat itu?

37:96

# وَاللّٰهُ خَلَقَكُمْ وَمَا تَعْمَلُوْنَ

wa**al**l*aa*hu khalaqakum wam*aa* ta'maluun**a**

Padahal Allah-lah yang menciptakan kamu dan apa yang kamu perbuat itu.”

37:97

# قَالُوا ابْنُوْا لَهٗ بُنْيَانًا فَاَلْقُوْهُ فِى الْجَحِيْمِ

q*aa*luu ibnuu lahu buny*aa*nan fa-alquuhu fii **a**lja*h*iim**i**

Mereka berkata, “Buatlah bangunan (perapian) untuknya (membakar Ibrahim); lalu lemparkan dia ke dalam api yang menyala-nyala itu.”

37:98

# فَاَرَادُوْا بِهٖ كَيْدًا فَجَعَلْنٰهُمُ الْاَسْفَلِيْنَ

fa-ar*aa*duu bihi kaydan faja'aln*aa*humu **a**l-asfaliin**a**

Maka mereka bermaksud memperdayainya dengan (membakar)nya, (namun Allah menyelamatkannya), lalu Kami jadikan mereka orang-orang yang hina.

37:99

# وَقَالَ اِنِّيْ ذَاهِبٌ اِلٰى رَبِّيْ سَيَهْدِيْنِ

waq*aa*la innii *dzaa*hibun il*aa* rabbii sayahdiin**i**

Dan dia (Ibrahim) berkata, “Sesungguhnya aku harus pergi (menghadap) kepada Tuhanku, Dia akan memberi petunjuk kepadaku.

37:100

# رَبِّ هَبْ لِيْ مِنَ الصّٰلِحِيْنَ

rabbi hab lii mina **al***shshaa*li*h*iin**a**

Ya Tuhanku, anugerahkanlah kepadaku (seorang anak) yang termasuk orang yang saleh.”

37:101

# فَبَشَّرْنٰهُ بِغُلٰمٍ حَلِيْمٍ

fabasysyarn*aa*hu bighul*aa*min *h*aliim**in**

Maka Kami beri kabar gembira kepadanya dengan (kelahiran) seorang anak yang sangat sabar (Ismail).

37:102

# فَلَمَّا بَلَغَ مَعَهُ السَّعْيَ قَالَ يٰبُنَيَّ اِنِّيْٓ اَرٰى فِى الْمَنَامِ اَنِّيْٓ اَذْبَحُكَ فَانْظُرْ مَاذَا تَرٰىۗ قَالَ يٰٓاَبَتِ افْعَلْ مَا تُؤْمَرُۖ سَتَجِدُنِيْٓ اِنْ شَاۤءَ اللّٰهُ مِنَ الصّٰبِرِيْنَ

falamm*aa* balagha ma'ahu **al**ssa'ya q*aa*la y*aa* bunayya innii ar*aa* fii **a**lman*aa*mi annii a*dz*ba*h*uka fa**u**n*zh*ur m*aatsaa* tar*aa* q*aa*la y*aa*

Maka ketika anak itu sampai (pada umur) sanggup berusaha bersamanya, (Ibrahim) berkata, “Wahai anakku! Sesungguhnya aku bermimpi bahwa aku menyembelihmu. Maka pikirkanlah bagaimana pendapatmu!” Dia (Ismail) menjawab, “Wahai ayahku! Lakukanlah apa yang dip







37:103

# فَلَمَّآ اَسْلَمَا وَتَلَّهٗ لِلْجَبِيْنِۚ

falamm*aa* aslam*aa* watallahu liljabiin**i**

Maka ketika keduanya telah berserah diri dan dia (Ibrahim) membaringkan anaknya atas pelipis(nya), (untuk melaksanakan perintah Allah).

37:104

# وَنَادَيْنٰهُ اَنْ يّٰٓاِبْرٰهِيْمُ ۙ

wan*aa*dayn*aa*hu an y*aa* ibr*aa*hiim**u**

Lalu Kami panggil dia, “Wahai Ibrahim!

37:105

# قَدْ صَدَّقْتَ الرُّؤْيَا ۚاِنَّا كَذٰلِكَ نَجْزِى الْمُحْسِنِيْنَ

qad *sh*addaqta **al**rru/y*aa* inn*aa* ka*dzaa*lika najzii **a**lmu*h*siniin**a**

sungguh, engkau telah membenarkan mimpi itu.” Sungguh, demikianlah Kami memberi balasan kepada orang-orang yang berbuat baik.

37:106

# اِنَّ هٰذَا لَهُوَ الْبَلٰۤؤُا الْمُبِيْنُ

inna h*aadzaa* lahuwa **a**lbal*aa*u **a**lmubiin**u**

Sesungguhnya ini benar-benar suatu ujian yang nyata.

37:107

# وَفَدَيْنٰهُ بِذِبْحٍ عَظِيْمٍ

wafadayn*aa*hu bi*dz*ib*h*in 'a*zh*iim**in**

Dan Kami tebus anak itu dengan seekor sembelihan yang besar.

37:108

# وَتَرَكْنَا عَلَيْهِ فِى الْاٰخِرِيْنَ ۖ

watarakn*aa* 'alayhi fii **a**l-*aa*khiriin**a**

Dan Kami abadikan untuk Ibrahim (pujian) di kalangan orang-orang yang datang kemudian,

37:109

# سَلٰمٌ عَلٰٓى اِبْرٰهِيْمَ

sal*aa*mun 'al*aa* ibr*aa*hiim**a**

”Selamat sejahtera bagi Ibrahim.”

37:110

# كَذٰلِكَ نَجْزِى الْمُحْسِنِيْنَ

ka*dzaa*lika najzii **a**lmu*h*siniin**a**

Demikianlah Kami memberi balasan kepada orang-orang yang berbuat baik.

37:111

# اِنَّهٗ مِنْ عِبَادِنَا الْمُؤْمِنِيْنَ

innahu min 'ib*aa*din*aa* **a**lmu/miniin**a**

Sungguh, dia termasuk hamba-hamba Kami yang beriman.

37:112

# وَبَشَّرْنٰهُ بِاِسْحٰقَ نَبِيًّا مِّنَ الصّٰلِحِيْنَ

wabasysyarn*aa*hu bi-is*haa*qa nabiyyan mina **al***shshaa*li*h*iin**a**

Dan Kami beri dia kabar gembira dengan (kelahiran) Ishak seorang nabi yang termasuk orang-orang yang saleh.

37:113

# وَبٰرَكْنَا عَلَيْهِ وَعَلٰٓى اِسْحٰقَۗ وَمِنْ ذُرِّيَّتِهِمَا مُحْسِنٌ وَّظَالِمٌ لِّنَفْسِهٖ مُبِيْنٌ ࣖ

wab*aa*rakn*aa* 'alayhi wa'al*aa* is*haa*qa wamin *dz*urriyyatihim*aa* mu*h*sinun wa*zhaa*limun linafsihi mubiin**un**

Dan Kami limpahkan keberkahan kepadanya dan kepada Ishak. Dan di antara keturunan keduanya ada yang berbuat baik dan ada (pula) yang terang-terangan berbuat zalim terhadap dirinya sendiri.

37:114

# وَلَقَدْ مَنَنَّا عَلٰى مُوْسٰى وَهٰرُوْنَ ۚ

walaqad manann*aa* 'al*aa* muus*aa* wah*aa*ruun**a**

Dan sungguh, Kami telah melimpahkan nikmat kepada Musa dan Harun.

37:115

# وَنَجَّيْنٰهُمَا وَقَوْمَهُمَا مِنَ الْكَرْبِ الْعَظِيْمِۚ

wanajjayn*aa*hum*aa* waqawmahum*aa* mina **a**lkarbi **a**l'a*zh*iim**i**

Dan Kami selamatkan keduanya dan kaumnya dari bencana yang besar,

37:116

# وَنَصَرْنٰهُمْ فَكَانُوْا هُمُ الْغٰلِبِيْنَۚ

wana*sh*arn*aa*hum fak*aa*nuu humu **a**lgh*aa*libiin**a**

dan Kami tolong mereka, sehingga jadilah mereka orang-orang yang menang.

37:117

# وَاٰتَيْنٰهُمَا الْكِتٰبَ الْمُسْتَبِيْنَ ۚ

wa*aa*tayn*aa*hum*aa* **a**lkit*aa*ba **a**lmustabiin**a**

Dan Kami berikan kepada keduanya Kitab yang sangat jelas,

37:118

# وَهَدَيْنٰهُمَا الصِّرَاطَ الْمُسْتَقِيْمَۚ

wahadayn*aa*hum*aa* **al***shsh*ir*aath*a **a**lmustaqiim**a**

dan Kami tunjukkan keduanya jalan yang lurus.

37:119

# وَتَرَكْنَا عَلَيْهِمَا فِى الْاٰخِرِيْنَ ۖ

watarakn*aa* 'alayhim*aa* fii **a**l-*aa*khiriin**a**

Dan Kami abadikan untuk keduanya (pujian) di kalangan orang-orang yang datang kemudian,

37:120

# سَلٰمٌ عَلٰى مُوْسٰى وَهٰرُوْنَ

sal*aa*mun 'al*aa* muus*aa* wah*aa*ruun**a**

”Selamat sejahtera bagi Musa dan Harun.”

37:121

# اِنَّا كَذٰلِكَ نَجْزِى الْمُحْسِنِيْنَ

inn*aa* ka*dzaa*lika najzii **a**lmu*h*siniin**a**

Demikianlah Kami memberi balasan kepada orang-orang yang berbuat baik.

37:122

# اِنَّهُمَا مِنْ عِبَادِنَا الْمُؤْمِنِيْنَ

innahum*aa* min 'ib*aa*din*aa* **a**lmu/miniin**a**

Sungguh, keduanya termasuk hamba-hamba Kami yang beriman.

37:123

# وَاِنَّ اِلْيَاسَ لَمِنَ الْمُرْسَلِيْنَۗ

wa-inna ily*aa*sa lamina **a**lmursaliin**a**

Dan sungguh, Ilyas benar-benar termasuk salah seorang rasul.

37:124

# اِذْ قَالَ لِقَوْمِهٖٓ اَلَا تَتَّقُوْنَ

i*dz* q*aa*la liqawmihi **a**l*aa* tattaquun**a**

(Ingatlah) ketika dia berkata kepada kaumnya, “Mengapa kamu tidak bertakwa?

37:125

# اَتَدْعُوْنَ بَعْلًا وَّتَذَرُوْنَ اَحْسَنَ الْخَالِقِيْنَۙ

atad'uuna ba'lan wata*dz*aruuna a*h*sana **a**lkh*aa*liqiin**a**

Patutkah kamu menyembah Ba’l dan kamu tinggalkan (Allah) sebaik-baik pencipta.

37:126

# اللّٰهَ رَبَّكُمْ وَرَبَّ اٰبَاۤىِٕكُمُ الْاَوَّلِيْنَ

**al**l*aa*ha rabbakum warabba *aa*b*aa*-ikumu **a**l-awwaliin**a**

(Yaitu) Allah Tuhanmu dan Tuhan nenek moyangmu yang terdahulu?”

37:127

# فَكَذَّبُوْهُ فَاِنَّهُمْ لَمُحْضَرُوْنَۙ

faka*dzdz*abuuhu fa-innahum lamu*hd*aruun**a**

Tetapi mereka mendustakannya (Ilyas), maka sungguh, mereka akan diseret (ke neraka),

37:128

# اِلَّا عِبَادَ اللّٰهِ الْمُخْلَصِيْنَ

ill*aa* 'ib*aa*da **al**l*aa*hi **a**lmukhla*sh*iin**a**

kecuali hamba-hamba Allah yang disucikan (dari dosa),

37:129

# وَتَرَكْنَا عَلَيْهِ فِى الْاٰخِرِيْنَ ۙ

watarakn*aa* 'alayhi fii **a**l-*aa*khiriin**a**

Dan Kami abadikan untuk Ilyas (pujian) di kalangan orang-orang yang datang kemudian.

37:130

# سَلٰمٌ عَلٰٓى اِلْ يَاسِيْنَ

sal*aa*mun 'al*aa* il y*aa*siin**a**

”Selamat sejahtera bagi Ilyas.”

37:131

# اِنَّا كَذٰلِكَ نَجْزِى الْمُحْسِنِيْنَ

inn*aa* ka*dzaa*lika najzii **a**lmu*h*siniin**a**

Demikianlah Kami memberi balasan kepada orang-orang yang berbuat baik.

37:132

# اِنَّهٗ مِنْ عِبَادِنَا الْمُؤْمِنِيْنَ

innahu min 'ib*aa*din*aa* **a**lmu/miniin**a**

Sungguh, dia termasuk hamba-hamba Kami yang beriman.

37:133

# وَاِنَّ لُوْطًا لَّمِنَ الْمُرْسَلِيْنَۗ

wa-inna luu*th*an lamina **a**lmursaliin**a**

Dan sungguh, Lut benar-benar termasuk salah seorang rasul.

37:134

# اِذْ نَجَّيْنٰهُ وَاَهْلَهٗٓ اَجْمَعِيْۙنَ

i*dz* najjayn*aa*hu wa-ahlahu ajma'iin**a**

(Ingatlah) ketika Kami telah menyelamatkan dia dan pengikutnya semua,

37:135

# اِلَّا عَجُوْزًا فِى الْغٰبِرِيْنَ

ill*aa* 'ajuuzan fii **a**lgh*aa*biriin**a**

kecuali seorang perempuan tua (istrinya) bersama-sama orang yang tinggal (di kota).

37:136

# ثُمَّ دَمَّرْنَا الْاٰخَرِيْنَ

tsumma dammarn*aa* **a**l-*aa*khariin**a**

Kemudian Kami binasakan orang-orang yang lain.

37:137

# وَاِنَّكُمْ لَتَمُرُّوْنَ عَلَيْهِمْ مُّصْبِحِيْنَۙ

wa-innakum latamurruuna 'alayhim mu*sh*bi*h*iin**a**

Dan sesungguhnya kamu (penduduk Mekah) benar-benar akan melalui (bekas-bekas) mereka pada waktu pagi,

37:138

# وَبِالَّيْلِۗ اَفَلَا تَعْقِلُوْنَ ࣖ

wabi**a**llayli afal*aa* ta'qiluun**a**

dan pada waktu malam. Maka mengapa kamu tidak mengerti?

37:139

# وَاِنَّ يُوْنُسَ لَمِنَ الْمُرْسَلِيْنَۗ

wa-inna yuunusa lamina **a**lmursaliin**a**

Dan sungguh, Yunus benar-benar termasuk salah seorang rasul,

37:140

# اِذْ اَبَقَ اِلَى الْفُلْكِ الْمَشْحُوْنِۙ

i*dz* abaqa il*aa* **a**lfulki **a**lmasy*h*uun**i**

(ingatlah) ketika dia lari, ke kapal yang penuh muatan,

37:141

# فَسَاهَمَ فَكَانَ مِنَ الْمُدْحَضِيْنَۚ

fas*aa*hama fak*aa*na mina **a**lmud*h*a*dh*iin**a**

kemudian dia ikut diundi ternyata dia termasuk orang-orang yang kalah (dalam undian).

37:142

# فَالْتَقَمَهُ الْحُوْتُ وَهُوَ مُلِيْمٌ

fa**i**ltaqamahu **a**l*h*uutu wahuwa muliim**un**

Maka dia ditelan oleh ikan besar dalam keadaan tercela.

37:143

# فَلَوْلَآ اَنَّهٗ كَانَ مِنَ الْمُسَبِّحِيْنَ ۙ

falawl*aa* annahu k*aa*na mina **a**lmusabbi*h*iin**a**

Maka sekiranya dia tidak termasuk orang yang banyak berzikir (bertasbih) kepada Allah,

37:144

# لَلَبِثَ فِيْ بَطْنِهٖٓ اِلٰى يَوْمِ يُبْعَثُوْنَۚ

lalabitsa fii ba*th*nihi il*aa* yawmi yub'atsuun**a**

niscaya dia akan tetap tinggal di perut (ikan itu) sampai hari kebangkitan.

37:145

# فَنَبَذْنٰهُ بِالْعَرَاۤءِ وَهُوَ سَقِيْمٌ ۚ

fanaba*dz*n*aa*hu bi**a**l'ar*aa*-i wahuwa saqiim**un**

Kemudian Kami lemparkan dia ke daratan yang tandus, sedang dia dalam keadaan sakit.

37:146

# وَاَنْۢبَتْنَا عَلَيْهِ شَجَرَةً مِّنْ يَّقْطِيْنٍۚ

wa-anbatn*aa* 'alayhi syajaratan min yaq*th*iin**in**

Kemudian untuk dia Kami tumbuhkan sebatang pohon dari jenis labu.

37:147

# وَاَرْسَلْنٰهُ اِلٰى مِائَةِ اَلْفٍ اَوْ يَزِيْدُوْنَۚ

wa-arsaln*aa*hu il*aa* mi-ati **a**lfin aw yaziiduun**a**

Dan Kami utus dia kepada seratus ribu (orang) atau lebih,

37:148

# فَاٰمَنُوْا فَمَتَّعْنٰهُمْ اِلٰى حِيْنٍ

fa*aa*manuu famatta'n*aa*hum il*aa* *h*iin**in**

sehingga mereka beriman, karena itu Kami anugerahkan kenikmatan hidup kepada mereka hingga waktu tertentu.

37:149

# فَاسْتَفْتِهِمْ اَلِرَبِّكَ الْبَنَاتُ وَلَهُمُ الْبَنُوْنَۚ

fa**i**staftihim **a**lirabbika **a**lban*aa*tu walahumu **a**lbanuun**a**

Maka tanyakanlah (Muhammad) kepada mereka (orang-orang kafir Mekah), “Apakah anak-anak perempuan itu untuk Tuhanmu sedangkan untuk mereka anak-anak laki-laki?”

37:150

# اَمْ خَلَقْنَا الْمَلٰۤىِٕكَةَ اِنَاثًا وَّهُمْ شَاهِدُوْنَ

am khalaqn*aa* **a**lmal*aa*-ikata in*aa*tsan wahum sy*aa*hiduun**a**

atau apakah Kami menciptakan malaikat-malaikat berupa perempuan sedangkan mereka menyaksikan(nya)?

37:151

# اَلَآ اِنَّهُمْ مِّنْ اِفْكِهِمْ لَيَقُوْلُوْنَۙ

al*aa* innahum min ifkihim layaquuluun**a**

Ingatlah, sesungguhnya di antara kebohongannya mereka benar-benar mengatakan,

37:152

# وَلَدَ اللّٰهُ ۙوَاِنَّهُمْ لَكٰذِبُوْنَۙ

walada **al**l*aa*hu wa-innahum lak*aadz*ibuun**a**

”Allah mempunyai anak.” Dan sungguh, mereka benar-benar pendusta,

37:153

# اَصْطَفَى الْبَنَاتِ عَلَى الْبَنِيْنَۗ

a*sth*af*aa* **a**lban*aa*ti 'al*aa* **a**lbaniin**a**

apakah Dia (Allah) memilih anak-anak perempuan daripada anak-anak laki-laki?

37:154

# مَا لَكُمْۗ كَيْفَ تَحْكُمُوْنَ

m*aa* lakum kayfa ta*h*kumuun**a**

Mengapa kamu ini? Bagaimana (caranya) kamu menetapkan?

37:155

# اَفَلَا تَذَكَّرُوْنَۚ

afal*aa* ta*dz*akkaruun**a**

Maka mengapa kamu tidak memikirkan?

37:156

# اَمْ لَكُمْ سُلْطٰنٌ مُّبِيْنٌۙ

am lakum sul*thaa*nun mubiin**un**

Ataukah kamu mempunyai bukti yang jelas?

37:157

# فَأْتُوْا بِكِتٰبِكُمْ اِنْ كُنْتُمْ صٰدِقِيْنَ

fa/tuu bikit*aa*bikum in kuntum *shaa*diqiin**a**

(Kalau begitu) maka bawalah kitabmu jika kamu orang yang benar.

37:158

# وَجَعَلُوْا بَيْنَهٗ وَبَيْنَ الْجِنَّةِ نَسَبًا ۗوَلَقَدْ عَلِمَتِ الْجِنَّةُ اِنَّهُمْ لَمُحْضَرُوْنَۙ

waja'aluu baynahu wabayna **a**ljinnati nasaban walaqad 'alimati **a**ljinnatu innahum lamu*hd*aruun**a**

Dan mereka mengadakan (hubungan) nasab (keluarga) antara Dia (Allah) dan jin. Dan sungguh, jin telah mengetahui bahwa mereka pasti akan diseret (ke neraka),

37:159

# سُبْحٰنَ اللّٰهِ عَمَّا يَصِفُوْنَۙ

sub*haa*na **al**l*aa*hi 'amm*aa* ya*sh*ifuun**a**

Mahasuci Allah dari apa yang mereka sifatkan,

37:160

# اِلَّا عِبَادَ اللّٰهِ الْمُخْلَصِيْنَ

ill*aa* 'ib*aa*da **al**l*aa*hi **a**lmukhla*sh*iin**a**

kecuali hamba-hamba Allah yang disucikan (dari dosa).

37:161

# فَاِنَّكُمْ وَمَا تَعْبُدُوْنَۙ

fa-innakum wam*aa* ta'buduun**a**

Maka sesungguhnya kamu dan apa yang kamu sembah itu,

37:162

# مَآ اَنْتُمْ عَلَيْهِ بِفَاتِنِيْنَۙ

m*aa* antum 'alayhi bif*aa*tiniin**a**

tidak akan dapat menyesatkan (seseorang) terhadap Allah,

37:163

# اِلَّا مَنْ هُوَ صَالِ الْجَحِيْمِ

ill*aa* man huwa *shaa*li **a**lja*h*iim**i**

kecuali orang-orang yang akan masuk ke neraka Jahim.

37:164

# وَمَا مِنَّآ اِلَّا لَهٗ مَقَامٌ مَّعْلُوْمٌۙ

wam*aa* minn*aa* ill*aa* lahu maq*aa*mun ma'luum**un**

Dan tidak satu pun di antara kami (malaikat) melainkan masing-masing mempunyai kedudukan tertentu,

37:165

# وَاِنَّا لَنَحْنُ الصَّۤافُّوْنَۖ

wa-inn*aa* lana*h*nu **al***shshaa*ffuun**a**

dan sesungguhnya kami selalu teratur dalam barisan (dalam melaksanakan perintah Allah).

37:166

# وَاِنَّا لَنَحْنُ الْمُسَبِّحُوْنَ

wa-inn*aa* lana*h*nu **a**lmusabbi*h*uun**a**

Dan sungguh, kami benar-benar terus bertasbih (kepada Allah).

37:167

# وَاِنْ كَانُوْا لَيَقُوْلُوْنَۙ

wa-in k*aa*nuu layaquuluun**a**

Dan sesungguhnya mereka (orang kafir Mekah) benar-benar pernah berkata,

37:168

# لَوْ اَنَّ عِنْدَنَا ذِكْرًا مِّنَ الْاَوَّلِيْنَۙ

law anna 'indan*aa* *dz*ikran mina **a**l-awwaliin**a**

”Sekiranya di sisi kami ada sebuah kitab dari (kitab-kitab yang diturunkan) kepada orang-orang dahulu,

37:169

# لَكُنَّا عِبَادَ اللّٰهِ الْمُخْلَصِيْنَ

lakunn*aa* 'ib*aa*da **al**l*aa*hi **a**lmukhla*sh*iin**a**

tentu kami akan menjadi hamba Allah yang disucikan (dari dosa).”

37:170

# فَكَفَرُوْا بِهٖۚ فَسَوْفَ يَعْلَمُوْنَ

fakafaruu bihi fasawfa ya'lamuun**a**

Tetapi ternyata mereka mengingkarinya (Al-Qur'an); maka kelak mereka akan mengetahui (akibat keingkarannya itu).

37:171

# وَلَقَدْ سَبَقَتْ كَلِمَتُنَا لِعِبَادِنَا الْمُرْسَلِيْنَ ۖ

walaqad sabaqat kalimatun*aa* li'ib*aa*din*aa* **a**lmursaliin**a**

Dan sungguh, janji Kami telah tetap bagi hamba-hamba Kami yang menjadi rasul,

37:172

# اِنَّهُمْ لَهُمُ الْمَنْصُوْرُوْنَۖ

innahum lahumu **a**lman*sh*uuruun**a**

(yaitu) mereka itu pasti akan mendapat pertolongan.

37:173

# وَاِنَّ جُنْدَنَا لَهُمُ الْغٰلِبُوْنَ

wa-inna jundan*aa* lahumu **a**lgh*aa*libuun**a**

Dan sesungguhnya bala tentara Kami itulah yang pasti menang.

37:174

# فَتَوَلَّ عَنْهُمْ حَتّٰى حِيْنٍۙ

fatawalla 'anhum *h*att*aa* *h*iin**in**

Maka berpalinglah engkau (Muhammad) dari mereka sampai waktu tertentu,

37:175

# وَّاَبْصِرْهُمْۗ فَسَوْفَ يُبْصِرُوْنَ

wa-ab*sh*irhum fasawfa yub*sh*iruun**a**

dan perlihatkanlah kepada mereka, maka kelak mereka akan melihat (azab itu).

37:176

# اَفَبِعَذَابِنَا يَسْتَعْجِلُوْنَ

afabi'a*dzaa*bin*aa* yasta'jiluun**a**

Maka apakah mereka meminta agar azab Kami disegerakan?

37:177

# فَاِذَا نَزَلَ بِسَاحَتِهِمْ فَسَاۤءَ صَبَاحُ الْمُنْذَرِيْنَ

fa-i*dzaa* nazala bis*aah*atihim fas*aa*-a *sh*ab*aah*u **a**lmun*dz*ariin**a**

Maka apabila (siksaan) itu turun di halaman mereka, maka sangat buruklah pagi hari bagi orang-orang yang diperingatkan itu.

37:178

# وَتَوَلَّ عَنْهُمْ حَتّٰى حِيْنٍۙ

watawalla 'anhum *h*att*aa* *h*iin**in**

Dan berpalinglah engkau dari mereka sampai waktu tertentu.

37:179

# وَّاَبْصِرْۗ فَسَوْفَ يُبْصِرُوْنَ

wa-ab*sh*ir fasawfa yub*sh*iruun**a**

Dan perlihatkanlah, maka kelak mereka akan melihat (azab itu).

37:180

# سُبْحٰنَ رَبِّكَ رَبِّ الْعِزَّةِ عَمَّا يَصِفُوْنَۚ

sub*haa*na rabbika rabbi **a**l'izzati 'amm*aa* ya*sh*ifuun**a**

Mahasuci Tuhanmu, Tuhan Yang Mahaperkasa dari sifat yang mereka katakan.

37:181

# وَسَلٰمٌ عَلَى الْمُرْسَلِيْنَۚ

wasal*aa*mun 'al*aa* **a**lmursaliin**a**

Dan selamat sejahtera bagi para rasul.

37:182

# وَالْحَمْدُ لِلّٰهِ رَبِّ الْعٰلَمِيْنَ ࣖ

wa**a**l*h*amdu lill*aa*hi rabbi **a**l'*aa*lamiin**a**

Dan segala puji bagi Allah Tuhan seluruh alam.

<!--EndFragment-->