---
title: (12) Yusuf - يوسف
date: 2021-10-27T03:33:54.850Z
ayat: 12
description: "Jumlah Ayat: 111 / Arti: Yusuf"
---
<!--StartFragment-->

12:1

# الۤرٰ ۗ تِلْكَ اٰيٰتُ الْكِتٰبِ الْمُبِيْنِۗ

alif-l*aa*m-r*aa* tilka *aa*y*aa*tu **a**lkit*aa*bi **a**lmubiin**u**

Alif Lam Ra. Ini adalah ayat-ayat Kitab (Al-Qur'an) yang jelas.

12:2

# اِنَّآ اَنْزَلْنٰهُ قُرْاٰنًا عَرَبِيًّا لَّعَلَّكُمْ تَعْقِلُوْنَ

inn*aa* anzaln*aa*hu qur-*aa*nan 'arabiyyan la'allakum ta'qiluun**a**

Sesungguhnya Kami menurunkannya sebagai Qur'an berbahasa Arab, agar kamu mengerti.

12:3

# نَحْنُ نَقُصُّ عَلَيْكَ اَحْسَنَ الْقَصَصِ بِمَآ اَوْحَيْنَآ اِلَيْكَ هٰذَا الْقُرْاٰنَۖ وَاِنْ كُنْتَ مِنْ قَبْلِهٖ لَمِنَ الْغٰفِلِيْنَ

na*h*nu naqu*shsh*u 'alayka a*h*sana **a**lqa*sh*a*sh*i bim*aa* aw*h*ayn*aa* ilayka h*aadzaa* **a**lqur-*aa*na wa-in kunta min qablihi lamina **a**lgh*aa*filiin<

Kami menceritakan kepadamu (Muhammad) kisah yang paling baik dengan mewahyukan Al-Qur'an ini kepadamu, dan sesungguhnya engkau sebelum itu termasuk orang yang tidak mengetahui.

12:4

# اِذْ قَالَ يُوْسُفُ لِاَبِيْهِ يٰٓاَبَتِ اِنِّيْ رَاَيْتُ اَحَدَ عَشَرَ كَوْكَبًا وَّالشَّمْسَ وَالْقَمَرَ رَاَيْتُهُمْ لِيْ سٰجِدِيْنَ

i*dz* q*aa*la yuusufu li-abiihi y*aa* abati innii ra-aytu a*h*ada 'asyara kawkaban wa**al**sysyamsa wa**a**lqamara ra-aytuhum lii s*aa*jidiin**a**

(Ingatlah), ketika Yusuf berkata kepada ayahnya, “Wahai ayahku! Sungguh, aku (bermimpi) melihat sebelas bintang, matahari dan bulan; kulihat semuanya sujud kepadaku.”

12:5

# قَالَ يٰبُنَيَّ لَا تَقْصُصْ رُءْيَاكَ عَلٰٓى اِخْوَتِكَ فَيَكِيْدُوْا لَكَ كَيْدًا ۗاِنَّ الشَّيْطٰنَ لِلْاِنْسَانِ عَدُوٌّ مُّبِيْنٌ

q*aa*la y*aa* bunayya l*aa* taq*sh*u*sh* ru/y*aa*ka 'al*aa* ikhwatika fayakiiduu laka kaydan inna **al**sysyay*thaa*na lil-ins*aa*ni 'aduwwun mubiin**un**

Dia (ayahnya) berkata, “Wahai anakku! Janganlah engkau ceritakan mimpimu kepada saudara-saudaramu, mereka akan membuat tipu daya (untuk membinasakan)mu. Sungguh, setan itu musuh yang jelas bagi manusia.”

12:6

# وَكَذٰلِكَ يَجْتَبِيْكَ رَبُّكَ وَيُعَلِّمُكَ مِنْ تَأْوِيْلِ الْاَحَادِيْثِ وَيُتِمُّ نِعْمَتَهٗ عَلَيْكَ وَعَلٰٓى اٰلِ يَعْقُوْبَ كَمَآ اَتَمَّهَا عَلٰٓى اَبَوَيْكَ مِنْ قَبْلُ اِبْرٰهِيْمَ وَاِسْحٰقَۗ اِنَّ رَبَّكَ عَلِيْمٌ حَكِيْمٌ ࣖ

waka*dzaa*lika yajtabiika rabbuka wayu'allimuka min ta/wiili **a**l-a*haa*diitsi wayutimmu ni'matahu 'alayka wa'al*aa* *aa*li ya'quuba kam*aa* atammah*aa* 'al*aa* abawayka min qablu ibr*aa*hiima wa-is

Dan demikianlah, Tuhan memilih engkau (untuk menjadi Nabi) dan mengajarkan kepadamu sebagian dari takwil mimpi dan menyempurnakan (nikmat-Nya) kepadamu dan kepada keluarga Yakub, sebagaimana Dia telah menyempurnakan nikmat-Nya kepada kedua orang kakekmu s

12:7

# ۞ لَقَدْ كَانَ فِيْ يُوْسُفَ وَاِخْوَتِهٖٓ اٰيٰتٌ لِّلسَّاۤىِٕلِيْنَ

laqad k*aa*na fii yuusufa wa-ikhwatihi *aa*y*aa*tun li**l**ss*aa*-iliin**a**

Sungguh, dalam (kisah) Yusuf dan saudara-saudaranya terdapat tanda-tanda (kekuasaan Allah) bagi orang yang bertanya.

12:8

# اِذْ قَالُوْا لَيُوْسُفُ وَاَخُوْهُ اَحَبُّ اِلٰٓى اَبِيْنَا مِنَّا وَنَحْنُ عُصْبَةٌ ۗاِنَّ اَبَانَا لَفِيْ ضَلٰلٍ مُّبِيْنٍۙ

i*dz* q*aa*luu layuusufu wa-akhuuhu a*h*abbu il*aa* abiin*aa* minn*aa* wana*h*nu 'u*sh*batun inna ab*aa*n*aa* lafii *dh*al*aa*lin mubiin**in**

Ketika mereka berkata, “Sesungguhnya Yusuf dan saudaranya (Bunyamin) lebih dicintai ayah daripada kita, padahal kita adalah satu golongan (yang kuat). Sungguh, ayah kita dalam kekeliruan yang nyata.

12:9

# ۨاقْتُلُوْا يُوْسُفَ اَوِ اطْرَحُوْهُ اَرْضًا يَّخْلُ لَكُمْ وَجْهُ اَبِيْكُمْ وَتَكُوْنُوْا مِنْۢ بَعْدِهٖ قَوْمًا صٰلِحِيْنَ

uqtuluu yuusufa awi i*th*ra*h*uuhu ar*dh*an yakhlu lakum wajhu abiikum watakuunuu min ba'dihi qawman *shaa*li*h*iin**a**

Bunuhlah Yusuf atau buanglah dia ke suatu tempat agar perhatian ayah tertumpah kepadamu, dan setelah itu kamu menjadi orang yang baik.”

12:10

# قَالَ قَاۤئِلٌ مِّنْهُمْ لَا تَقْتُلُوْا يُوْسُفَ وَاَلْقُوْهُ فِيْ غَيٰبَتِ الْجُبِّ يَلْتَقِطْهُ بَعْضُ السَّيَّارَةِ اِنْ كُنْتُمْ فٰعِلِيْنَ

q*aa*la q*aa*-ilun minhum l*aa* taqtuluu yuusufa wa-alquuhu fii ghay*aa*bati **a**ljubbi yaltaqi*th*hu ba'*dh*u **al**ssayy*aa*rati in kuntum f*aa*'iliin**a**

Seorang di antara mereka berkata, “Janganlah kamu membunuh Yusuf, tetapi masukan saja dia ke dasar sumur agar dia dipungut oleh sebagian musafir, jika kamu hendak berbuat.”

12:11

# قَالُوْا يٰٓاَبَانَا مَالَكَ لَا تَأْمَنَّ۫ا عَلٰى يُوْسُفَ وَاِنَّا لَهٗ لَنَاصِحُوْنَ

q*aa*luu y*aa* ab*aa*n*aa* m*aa* laka l*aa* ta/mann*aa* 'al*aa* yuusufa wa-inn*aa* lahu lan*aas*i*h*uun**a**

Mereka berkata, “Wahai ayah kami! Mengapa engkau tidak mempercayai kami terhadap Yusuf, padahal sesungguhnya kami semua menginginkan kebaikan baginya.

12:12

# اَرْسِلْهُ مَعَنَا غَدًا يَّرْتَعْ وَيَلْعَبْ وَاِنَّا لَهٗ لَحٰفِظُوْنَ

arsilhu ma'an*aa* ghadan yarta' wayal'ab wa-inn*aa* lahu la*haa*fi*zh*uun**a**

Biarkanlah dia pergi bersama kami besok pagi, agar dia bersenang-senang dan bermain-main, dan kami pasti menjaganya.”

12:13

# قَالَ اِنِّيْ لَيَحْزُنُنِيْٓ اَنْ تَذْهَبُوْا بِهٖ وَاَخَافُ اَنْ يَّأْكُلَهُ الذِّئْبُ وَاَنْتُمْ عَنْهُ غٰفِلُوْنَ

q*aa*la innii laya*h*zununii an tsa*dz*habuu bihi wa-akh*aa*fu an ya/kulahu **al***dzdz*i/bu wa-antum 'anhu gh*aa*filuun**a**

Dia (Yakub) berkata, “Sesungguhnya kepergian kamu bersama dia (Yusuf) sangat menyedihkanku dan aku khawatir dia dimakan serigala, sedang kamu lengah darinya.”

12:14

# قَالُوْا لَىِٕنْ اَكَلَهُ الذِّئْبُ وَنَحْنُ عُصْبَةٌ اِنَّآ اِذًا لَّخٰسِرُوْنَ

q*aa*luu la-in akalahu **al***dzdz*i/bu wana*h*nu 'u*sh*batun inn*aa *i*dz*an lakh*aa*siruun**a**

Sesungguhnya mereka berkata, “Jika dia dimakan serigala, padahal kami golongan (yang kuat), kalau demikian tentu kami orang-orang yang rugi.”

12:15

# فَلَمَّا ذَهَبُوْا بِهٖ وَاَجْمَعُوْٓا اَنْ يَّجْعَلُوْهُ فِيْ غَيٰبَتِ الْجُبِّۚ وَاَوْحَيْنَآ اِلَيْهِ لَتُنَبِّئَنَّهُمْ بِاَمْرِهِمْ هٰذَا وَهُمْ لَا يَشْعُرُوْنَ

falamm*aa* *dz*ahabuu bihi wa-ajma'uu an yaj'aluuhu fii ghay*aa*bati **a**ljubbi wa-aw*h*ayn*aa* ilayhi latunabi-annahum bi-amrihim h*aadzaa* wahum l*aa* yasy'uruun**a**

Maka ketika mereka membawanya dan sepakat memasukkan ke dasar sumur, Kami wahyukan kepadanya, “Engkau kelak pasti akan menceritakan perbuatan ini kepada mereka, sedang mereka tidak menyadari.”

12:16

# وَجَاۤءُوْٓ اَبَاهُمْ عِشَاۤءً يَّبْكُوْنَۗ

waj*aa*uu ab*aa*hum 'isy*aa*-an yabkuun**a**

Kemudian mereka datang kepada ayah mereka pada petang hari sambil menangis.

12:17

# قَالُوْا يٰٓاَبَانَآ اِنَّا ذَهَبْنَا نَسْتَبِقُ وَتَرَكْنَا يُوْسُفَ عِنْدَ مَتَاعِنَا فَاَكَلَهُ الذِّئْبُۚ وَمَآ اَنْتَ بِمُؤْمِنٍ لَّنَا وَلَوْ كُنَّا صٰدِقِيْنَ

q*aa*luu y*aa* ab*aa*n*aa* inn*aa* *dz*ahabn*aa* nastabiqu watarakn*aa* yuusufa 'inda mat*aa*'in*aa* fa-akalahu **al***dzdz*i/bu wam*aa *anta bimu/minin lan*aa *walaw kunn*aa<*

Mereka berkata, “Wahai ayah kami! Sesungguhnya kami pergi berlomba dan kami tinggalkan Yusuf di dekat barang-barang kami, lalu dia dimakan serigala; dan engkau tentu tidak akan percaya kepada kami, sekalipun kami berkata benar.”

12:18

# وَجَاۤءُوْ عَلٰى قَمِيْصِهٖ بِدَمٍ كَذِبٍۗ قَالَ بَلْ سَوَّلَتْ لَكُمْ اَنْفُسُكُمْ اَمْرًاۗ فَصَبْرٌ جَمِيْلٌ ۗوَاللّٰهُ الْمُسْتَعَانُ عَلٰى مَا تَصِفُوْنَ

waj*aa*uu 'al*aa* qamii*sh*ihi bidamin ka*dz*ibin q*aa*la bal sawwalat lakum anfusukum amran fa*sh*abrun jamiilun wa**al**l*aa*hu **a**lmusta'*aa*nu 'al*aa* m*aa* ta*sh*ifuun

Dan mereka datang membawa baju gamisnya (yang berlumuran) darah palsu. Dia (Yakub) berkata, “Sebenarnya hanya dirimu sendirilah yang memandang baik urusan yang buruk itu; maka hanya bersabar itulah yang terbaik (bagiku). Dan kepada Allah saja memohon pert

12:19

# وَجَاۤءَتْ سَيَّارَةٌ فَاَرْسَلُوْا وَارِدَهُمْ فَاَدْلٰى دَلْوَهٗ ۗقَالَ يٰبُشْرٰى هٰذَا غُلٰمٌ ۗوَاَسَرُّوْهُ بِضَاعَةً ۗوَاللّٰهُ عَلِيْمٌ ۢبِمَا يَعْمَلُوْنَ

waj*aa*-at sayy*aa*ratun fa-arsaluu w*aa*ridahum fa-adl*aa* dalwahu q*aa*la y*aa* busyr*aa* h*aadzaa* ghul*aa*mun wa-asarruuhu bi*daa*'atan wa**al**l*aa*hu 'aliimun bim*aa* ya'maluun

Dan datanglah sekelompok musafir, mereka menyuruh seorang pengambil air. Lalu dia menurunkan timbanya. Dia berkata, “Oh, senangnya, ini ada seorang anak muda!” Kemudian mereka menyembunyikannya sebagai barang dagangan. Dan Allah Maha Mengetahui apa yang m

12:20

# وَشَرَوْهُ بِثَمَنٍۢ بَخْسٍ دَرَاهِمَ مَعْدُوْدَةٍ ۚوَكَانُوْا فِيْهِ مِنَ الزَّاهِدِيْنَ ࣖ

wasyarawhu bitsamanin bakhsin dar*aa*hima ma'duudatin wak*aa*nuu fiihi mina **al**zz*aa*hidiin**a**

Dan mereka menjualnya (Yusuf) dengan harga rendah, yaitu beberapa dirham saja, sebab mereka tidak tertarik kepadanya.

12:21

# وَقَالَ الَّذِى اشْتَرٰىهُ مِنْ مِّصْرَ لِامْرَاَتِهٖٓ اَكْرِمِيْ مَثْوٰىهُ عَسٰىٓ اَنْ يَّنْفَعَنَآ اَوْ نَتَّخِذَهٗ وَلَدًا ۗوَكَذٰلِكَ مَكَّنَّا لِيُوْسُفَ فِى الْاَرْضِۖ وَلِنُعَلِّمَهٗ مِنْ تَأْوِيْلِ الْاَحَادِيْثِۗ وَاللّٰهُ غَالِبٌ عَلٰٓى اَمْرِه

waq*aa*la **al**la*dz*ii isytar*aa*hu min mi*sh*ra li**i**mra-atihi akrimii matsw*aa*hu 'as*aa* an yanfa'an*aa* aw nattakhi*dz*ahu waladan waka*dzaa*lika makkann*aa* liyuusufa fii

Dan orang dari Mesir yang membelinya berkata kepada istrinya,” Berikanlah kepadanya tempat (dan layanan) yang baik, mudah-mudahan dia bermanfaat bagi kita atau kita pungut dia sebagai anak.” Dan demikianlah Kami memberikan kedudukan yang baik kepada Yusuf

12:22

# وَلَمَّا بَلَغَ اَشُدَّهٗٓ اٰتَيْنٰهُ حُكْمًا وَّعِلْمًا ۗوَكَذٰلِكَ نَجْزِى الْمُحْسِنِيْنَ

walamm*aa* balagha asyuddahu *aa*tayn*aa*hu *h*ukman wa'ilman waka*dzaa*lika najzii **a**lmu*h*siniin**a**

Dan ketika dia telah cukup dewasa Kami berikan kepadanya kekuasaan dan ilmu. Demikianlah Kami memberi balasan kepada orang-orang yang berbuat baik.

12:23

# وَرَاوَدَتْهُ الَّتِيْ هُوَ فِيْ بَيْتِهَا عَنْ نَّفْسِهٖ وَغَلَّقَتِ الْاَبْوَابَ وَقَالَتْ هَيْتَ لَكَ ۗقَالَ مَعَاذَ اللّٰهِ اِنَّهٗ رَبِّيْٓ اَحْسَنَ مَثْوَايَۗ اِنَّهٗ لَا يُفْلِحُ الظّٰلِمُوْنَ

war*aa*wadat-hu **al**latii huwa fii baytih*aa* 'an nafsihi waghallaqati **a**l-abw*aa*ba waq*aa*lat hayta laka q*aa*la ma'*aadz*a **al**l*aa*hi innahu rabbii a*h*sana matsw

Dan perempuan yang dia (Yusuf) tinggal di rumahnya menggoda dirinya. Dan dia menutup pintu-pintu, lalu berkata, “Marilah mendekat kepadaku.” Yusuf berkata, “Aku berlindung kepada Allah, sungguh, tuanku telah memperlakukan aku dengan baik.” Sesungguhnya or

12:24

# وَلَقَدْ هَمَّتْ بِهٖۙ وَهَمَّ بِهَا ۚ لَوْلَآ اَنْ رَّاٰى بُرْهَانَ رَبِّهٖۗ كَذٰلِكَ لِنَصْرِفَ عَنْهُ السُّوْۤءَ وَالْفَحْشَاۤءَۗ اِنَّهٗ مِنْ عِبَادِنَا الْمُخْلَصِيْنَ

walaqad hammat bihi wahamma bih*aa* lawl*aa* an ra*aa* burh*aa*na rabbihi ka*dzaa*lika lina*sh*rifa 'anhu **al**ssuu-a wa**a**lfa*h*sy*aa*-a innahu min 'ib*aa*din*aa* **a**

**Dan sungguh, perempuan itu telah berkehendak kepadanya (Yusuf). Dan Yusuf pun berkehendak kepadanya, sekiranya dia tidak melihat tanda (dari) Tuhannya. Demikianlah, Kami palingkan darinya keburukan dan kekejian. Sungguh, dia (Yusuf) termasuk hamba Kami ya**

12:25

# وَاسْتَبَقَا الْبَابَ وَقَدَّتْ قَمِيْصَهٗ مِنْ دُبُرٍ وَّاَلْفَيَا سَيِّدَهَا لَدَا الْبَابِۗ قَالَتْ مَا جَزَاۤءُ مَنْ اَرَادَ بِاَهْلِكَ سُوْۤءًا اِلَّآ اَنْ يُّسْجَنَ اَوْ عَذَابٌ اَلِيْمٌ

wa**i**stabaq*aa* **a**lb*aa*ba waqaddat qamii*sh*ahu min duburin wa-alfay*aa* sayyidah*aa* lad*aa* **a**lb*aa*bi q*aa*lat m*aa* jaz*aa*u man ar*aa*da bi-ahlika

Dan keduanya berlomba menuju pintu dan perempuan itu menarik baju gamisnya (Yusuf) dari belakang hingga koyak dan keduanya mendapati suami perempuan itu di depan pintu. Dia (perempuan itu) berkata, “Apakah balasan terhadap orang yang bermaksud buruk terha

12:26

# قَالَ هِيَ رَاوَدَتْنِيْ عَنْ نَّفْسِيْ وَشَهِدَ شَاهِدٌ مِّنْ اَهْلِهَاۚ اِنْ كَانَ قَمِيْصُهٗ قُدَّ مِنْ قُبُلٍ فَصَدَقَتْ وَهُوَ مِنَ الْكٰذِبِيْنَ

q*aa*la hiya r*aa*wadatnii 'an nafsii wasyahida sy*aa*hidun min ahlih*aa* in k*aa*na qamii*sh*uhu qudda min qubulin fa*sh*adaqat wahuwa mina **a**lk*aadz*ibiin**a**

Dia (Yusuf) berkata, “Dia yang menggodaku dan merayu diriku.” Seorang saksi dari keluarga perempuan itu memberikan kesaksian, “Jika baju gamisnya koyak di bagian depan, maka perempuan itu benar, dan dia (Yusuf) termasuk orang yang dusta.

12:27

# وَاِنْ كَانَ قَمِيْصُهٗ قُدَّ مِنْ دُبُرٍ فَكَذَبَتْ وَهُوَ مِنَ الصّٰدِقِيْنَ

wa-in k*aa*na qamii*sh*uhu qudda min duburin faka*dz*abat wahuwa mina **al***shshaa*diqiin**a**

Dan jika baju gamisnya koyak di bagian belakang, maka perempuan itulah yang dusta, dan dia (Yusuf) termasuk orang yang benar.”

12:28

# فَلَمَّا رَاٰى قَمِيْصَهٗ قُدَّ مِنْ دُبُرٍ قَالَ اِنَّهٗ مِنْ كَيْدِكُنَّ ۗاِنَّ كَيْدَكُنَّ عَظِيْمٌ

falamm*aa* ra*aa* qamii*sh*ahu qudda min duburin q*aa*la innahu min kaydikunna inna kaydakunna 'a*zh*iim**un**

Maka ketika dia (suami perempuan itu) melihat baju gamisnya (Yusuf) koyak di bagian belakang, dia berkata, “Sesungguhnya ini adalah tipu dayamu. Tipu dayamu benar-benar hebat.”

12:29

# يُوْسُفُ اَعْرِضْ عَنْ هٰذَا وَاسْتَغْفِرِيْ لِذَنْۢبِكِۖ اِنَّكِ كُنْتِ مِنَ الْخٰطِـِٕيْنَ ࣖ

yuusufu a'ri*dh* 'an h*aadzaa* wa**i**staghfirii li*dz*anbiki innaki kunti mina **a**lkh*aath*i-iin**a**

Wahai Yusuf! ”Lupakanlah ini, dan (istriku) mohonlah ampunan atas dosamu, karena engkau termasuk orang yang bersalah.”

12:30

# ۞ وَقَالَ نِسْوَةٌ فِى الْمَدِيْنَةِ امْرَاَتُ الْعَزِيْزِ تُرَاوِدُ فَتٰىهَا عَنْ نَّفْسِهٖۚ قَدْ شَغَفَهَا حُبًّاۗ اِنَّا لَنَرٰىهَا فِيْ ضَلٰلٍ مُّبِيْنٍ

waq*aa*la niswatun fii **a**lmadiinati imra-atu **a**l'aziizi tur*aa*widu fat*aa*h*aa* 'an nafsihi qad syaghafah*aa* *h*ubban inn*aa* lanar*aa*h*aa* fii *dh*al*aa*lin mubiin<

Dan perempuan-perempuan di kota berkata, “Istri Al-Aziz menggoda dan merayu pelayannya untuk menundukkan dirinya, pelayannya benar-benar membuatnya mabuk cinta. Kami pasti memandang dia dalam kesesatan yang nyata.”

12:31

# فَلَمَّا سَمِعَتْ بِمَكْرِهِنَّ اَرْسَلَتْ اِلَيْهِنَّ وَاَعْتَدَتْ لَهُنَّ مُتَّكَاً وَّاٰتَتْ كُلَّ وَاحِدَةٍ مِّنْهُنَّ سِكِّيْنًا وَّقَالَتِ اخْرُجْ عَلَيْهِنَّ ۚ فَلَمَّا رَاَيْنَهٗٓ اَكْبَرْنَهٗ وَقَطَّعْنَ اَيْدِيَهُنَّۖ وَقُلْنَ حَاشَ لِلّٰهِ مَا

falamm*aa* sami'at bimakrihinna arsalat ilayhinna wa-a'tadat lahunna muttaka-an wa*aa*tat kulla w*aah*idatin minhunna sikkiinan waq*aa*lati ukhruj 'alayhinna falamm*aa* ra-aynahu akbarnahu waqa*ththh*a'na aydiyahunna waqulna

Maka ketika perempuan itu mendengar cercaan mereka, diundangnyalah perempuan-perempuan itu dan disediakannya tempat duduk bagi mereka, dan kepada masing-masing mereka diberikan sebuah pisau (untuk memotong jamuan), kemudian dia berkata (kepada Yusuf), “Ke

12:32

# قَالَتْ فَذٰلِكُنَّ الَّذِيْ لُمْتُنَّنِيْ فِيْهِ ۗوَلَقَدْ رَاوَدْتُّهٗ عَنْ نَّفْسِهٖ فَاسْتَعْصَمَ ۗوَلَىِٕنْ لَّمْ يَفْعَلْ مَآ اٰمُرُهٗ لَيُسْجَنَنَّ وَلَيَكُوْنًا مِّنَ الصّٰغِرِيْنَ

q*aa*lat fa*dzaa*likunna **al**la*dz*ii lumtunnanii fiihi walaqad r*aa*wadtuhu 'an nafsihi fa**i**st'*sh*ama wala-in lam yaf'al m*aa* *aa*muruhu layusjananna walayakuunan mina **al**

**Dia (istri Al-Aziz) berkata, “Itulah orangnya yang menyebabkan kamu mencela aku karena (aku tertarik) kepadanya, dan sungguh, aku telah menggoda untuk menundukkan dirinya tetapi dia menolak. Jika dia tidak melakukan apa yang aku perintahkan kepadanya, nis**

12:33

# قَالَ رَبِّ السِّجْنُ اَحَبُّ اِلَيَّ مِمَّا يَدْعُوْنَنِيْٓ اِلَيْهِ ۚوَاِلَّا تَصْرِفْ عَنِّيْ كَيْدَهُنَّ اَصْبُ اِلَيْهِنَّ وَاَكُنْ مِّنَ الْجٰهِلِيْنَ

q*aa*la rabbi **al**ssijnu a*h*abbu ilayya mimm*aa* yad'uunanii ilayhi wa-ill*aa* ta*sh*rif 'annii kaydahunna a*sh*bu ilayhinna wa-akun mina **a**lj*aa*hiliin**a**

Yusuf berkata, “Wahai Tuhanku! Penjara lebih aku sukai daripada memenuhi ajakan mereka. Jika aku tidak Engkau hindarkan dari tipu daya mereka, niscaya aku akan cenderung untuk (memenuhi keinginan mereka) dan tentu aku termasuk orang yang bodoh.”

12:34

# فَاسْتَجَابَ لَهٗ رَبُّهٗ فَصَرَفَ عَنْهُ كَيْدَهُنَّ ۗاِنَّهٗ هُوَ السَّمِيْعُ الْعَلِيْمُ

fa**i**staj*aa*ba lahu rabbuhu fa*sh*arafa 'anhu kaydahunna innahu huwa **al**ssamii'u **a**l'aliim**u**

Maka Tuhan memperkenankan doa Yusuf, dan Dia menghindarkan Yusuf dari tipu daya mereka. Dialah Yang Maha Mendengar, Maha Mengetahui.

12:35

# ثُمَّ بَدَا لَهُمْ مِّنْۢ بَعْدِ مَا رَاَوُا الْاٰيٰتِ لَيَسْجُنُنَّهٗ حَتّٰى حِيْنٍ ࣖ

tsumma bad*aa* lahum min ba'di m*aa* ra-awuu **a**l-*aa*y*aa*ti layasjununnahu *h*att*aa* *h*iin**in**

Kemudian timbul pikiran pada mereka setelah melihat tanda-tanda (kebenaran Yusuf) bahwa mereka harus memenjarakannya sampai waktu tertentu.

12:36

# وَدَخَلَ مَعَهُ السِّجْنَ فَتَيٰنِ ۗقَالَ اَحَدُهُمَآ اِنِّيْٓ اَرٰىنِيْٓ اَعْصِرُ خَمْرًا ۚوَقَالَ الْاٰخَرُ اِنِّيْٓ اَرٰىنِيْٓ اَحْمِلُ فَوْقَ رَأْسِيْ خُبْزًا تَأْكُلُ الطَّيْرُ مِنْهُ ۗنَبِّئْنَا بِتَأْوِيْلِهٖ ۚاِنَّا نَرٰىكَ مِنَ الْمُحْسِنِيْنَ

wadakhala ma'ahu **al**ssijna fatay*aa*ni q*aa*la a*h*aduhum*aa* innii ar*aa*nii a'*sh*iru khamran waq*aa*la **a**l-*aa*kharu innii ar*aa*nii a*h*milu fawqa ra/sii khubzan ta/kulu

Dan bersama dia masuk pula dua orang pemuda ke dalam penjara. Salah satunya berkata, “Sesungguhnya aku bermimpi memeras anggur,” dan yang lainnya berkata, “Aku bermimpi, membawa roti di atas kepalaku, sebagiannya dimakan burung.” Berikanlah kepada kami ta

12:37

# قَالَ لَا يَأْتِيْكُمَا طَعَامٌ تُرْزَقٰنِهٖٓ اِلَّا نَبَّأْتُكُمَا بِتَأْوِيْلِهٖ قَبْلَ اَنْ يَّأْتِيَكُمَا ۗذٰلِكُمَا مِمَّا عَلَّمَنِيْ رَبِّيْۗ اِنِّيْ تَرَكْتُ مِلَّةَ قَوْمٍ لَّا يُؤْمِنُوْنَ بِاللّٰهِ وَهُمْ بِالْاٰخِرَةِ هُمْ كٰفِرُوْنَۙ

q*aa*la l*aa* ya/tiikum*aa* *th*a'*aa*mun turzaq*aa*nihi ill*aa* nabba/tukum*aa* bita/wiilihi qabla an ya/tiyakum*aa* *dzaa*likum*aa* mimm*aa* 'allamanii rabbii innii taraktu millata qawmin l*a*

Dia (Yusuf) berkata, “Makanan apa pun yang akan diberikan kepadamu berdua aku telah dapat menerangkan takwilnya, sebelum (makanan) itu sampai kepadamu. Itu sebagian dari yang diajarkan Tuhan kepadaku. Sesungguhnya aku telah meninggalkan agama orang-orang

12:38

# وَاتَّبَعْتُ مِلَّةَ اٰبَاۤءِيْٓ اِبْرٰهِيْمَ وَاِسْحٰقَ وَيَعْقُوْبَۗ مَا كَانَ لَنَآ اَنْ نُّشْرِكَ بِاللّٰهِ مِنْ شَيْءٍۗ ذٰلِكَ مِنْ فَضْلِ اللّٰهِ عَلَيْنَا وَعَلَى النَّاسِ وَلٰكِنَّ اَكْثَرَ النَّاسِ لَا يَشْكُرُوْنَ

wa**i**ttaba'tu millata *aa*b*aa*-ii ibr*aa*hiima wa-is*haa*qa waya'quuba m*aa* k*aa*na lan*aa* an nusyrika bi**al**l*aa*hi min syay-in *dzaa*lika min fa*dh*li **al**

Dan aku mengikuti agama nenek moyangku: Ibrahim, Ishak dan Yakub. Tidak pantas bagi kami (para nabi) mempersekutukan sesuatu apa pun dengan Allah. Itu adalah dari karunia Allah kepada kami dan kepada manusia (semuanya); tetapi kebanyakan manusia tidak ber

12:39

# يٰصَاحِبَيِ السِّجْنِ ءَاَرْبَابٌ مُتَفَرِّقُوْنَ خَيْرٌ اَمِ اللّٰهُ الْوَاحِدُ الْقَهَّارُۗ

y*aa* *shaah*ibayi **al**ssijni a-arb*aa*bun mutafarriquuna khayrun ami **al**l*aa*hu **a**lw*aah*idu **a**lqahh*aa*r**u**

Wahai kedua penghuni penjara! Manakah yang baik, tuhan-tuhan yang bermacam-macam itu ataukah Allah Yang Maha Esa, Mahaperkasa?

12:40

# مَا تَعْبُدُوْنَ مِنْ دُوْنِهٖٓ اِلَّآ اَسْمَاۤءً سَمَّيْتُمُوْهَآ اَنْتُمْ وَاٰبَاۤؤُكُمْ مَّآ اَنْزَلَ اللّٰهُ بِهَا مِنْ سُلْطٰنٍۗ اِنِ الْحُكْمُ اِلَّا لِلّٰهِ ۗاَمَرَ اَلَّا تَعْبُدُوْٓا اِلَّآ اِيَّاهُ ۗذٰلِكَ الدِّيْنُ الْقَيِّمُ وَلٰكِنَّ اَكْ

m*aa* ta'buduuna min duunihi ill*aa* asm*aa*-an sammaytumuuh*aa* antum wa*aa*b*aa*ukum m*aa* anzala **al**l*aa*hu bih*aa* min sul*thaa*nin ini **a**l*h*ukmu ill*aa*

*Apa yang kamu sembah selain Dia, hanyalah nama-nama yang kamu buat-buat baik oleh kamu sendiri maupun oleh nenek moyangmu. Allah tidak menurunkan suatu keterangan pun tentang hal (nama-nama) itu. Keputusan itu hanyalah milik Allah. Dia telah memerintahkan*

12:41

# يٰصَاحِبَيِ السِّجْنِ اَمَّآ اَحَدُكُمَا فَيَسْقِيْ رَبَّهٗ خَمْرًا ۗوَاَمَّا الْاٰخَرُ فَيُصْلَبُ فَتَأْكُلُ الطَّيْرُ مِنْ رَّأْسِهٖ ۗ قُضِيَ الْاَمْرُ الَّذِيْ فِيْهِ تَسْتَفْتِيٰنِۗ

y*aa* *shaah*ibayi **al**ssijni amm*aa* a*h*adukum*aa* fayasqii rabbahu khamran wa-amm*aa* **a**l-*aa*kharu fayu*sh*labu fata/kulu **al***ththh*ayru min ra/sihi qu*dh*

*Wahai kedua penghuni penjara, “Salah seorang di antara kamu, akan bertugas menyediakan minuman khamar bagi tuannya. Adapun yang seorang lagi dia akan disalib, lalu burung memakan sebagian kepalanya. Telah terjawab perkara yang kamu tanyakan (kepadaku).”*

12:42

# وَقَالَ لِلَّذِيْ ظَنَّ اَنَّهٗ نَاجٍ مِّنْهُمَا اذْكُرْنِيْ عِنْدَ رَبِّكَۖ فَاَنْسٰىهُ الشَّيْطٰنُ ذِكْرَ رَبِّهٖ فَلَبِثَ فِى السِّجْنِ بِضْعَ سِنِيْنَ ࣖ

waq*aa*la lilla*dz*ii *zh*anna annahu n*aa*jin minhum*aa* u*dz*kurnii 'inda rabbika fa-ans*aa*hu **al**sysyay*thaa*nu *dz*ikra rabbihi falabitsa fii **al**ssijni bi*dh*'a s

Dan dia (Yusuf) berkata kepada orang yang diketahuinya akan selamat di antara mereka berdua, “Terangkanlah keadaanku kepada tuanmu.” Maka setan menjadikan dia lupa untuk menerangkan (keadaan Yusuf) kepada tuannya. Karena itu dia (Yusuf) tetap dalam penjar

12:43

# وَقَالَ الْمَلِكُ اِنِّيْٓ اَرٰى سَبْعَ بَقَرٰتٍ سِمَانٍ يَّأْكُلُهُنَّ سَبْعٌ عِجَافٌ وَّسَبْعَ سُنْۢبُلٰتٍ خُضْرٍ وَّاُخَرَ يٰبِسٰتٍۗ يٰٓاَيُّهَا الْمَلَاُ اَفْتُوْنِيْ فِيْ رُؤْيَايَ اِنْ كُنْتُمْ لِلرُّءْيَا تَعْبُرُوْنَ

waq*aa*la **a**lmaliku innii ar*aa* sab'a baqar*aa*tin sim*aa*nin ya/kuluhunna sab'un 'ij*aa*fun wasab'a sunbul*aa*tin khu*dh*rin waukhara y*aa*bis*aa*tin y*aa* ayyuh*aa* **a**

**Dan raja berkata (kepada para pemuka kaumnya), “Sesungguhnya aku bermimpi melihat tujuh ekor sapi betina yang gemuk dimakan oleh tujuh ekor sapi betina yang kurus; tujuh tangkai (gandum) yang hijau dan (tujuh tangkai) lainnya yang kering. Wahai orang yang**

12:44

# قَالُوْٓا اَضْغَاثُ اَحْلَامٍ ۚوَمَا نَحْنُ بِتَأْوِيْلِ الْاَحْلَامِ بِعٰلِمِيْنَ

q*aa*luu a*dh*gh*aa*tsu a*h*l*aa*min wam*aa* na*h*nu bita/wiili **a**l-a*h*l*aa*mi bi'*aa*limiin**a**

Mereka menjawab, “(Itu) mimpi-mimpi yang kosong dan kami tidak mampu menakwilkan mimpi itu.”

12:45

# وَقَالَ الَّذِيْ نَجَا مِنْهُمَا وَادَّكَرَ بَعْدَ اُمَّةٍ اَنَا۠ اُنَبِّئُكُمْ بِتَأْوِيْلِهٖ فَاَرْسِلُوْنِ

waq*aa*la **al**la*dz*ii naj*aa* minhum*aa* wa**i**ddakara ba'da ummatin an*aa* unabbi-ukum bita/wiilihi fa-arsiluun**i**

Dan berkatalah orang yang selamat di antara mereka berdua dan teringat (kepada Yusuf) setelah beberapa waktu lamanya, “Aku akan memberitahukan kepadamu tentang (orang yang pandai) menakwilkan mimpi itu, maka utuslah aku (kepadanya).”

12:46

# يُوْسُفُ اَيُّهَا الصِّدِّيْقُ اَفْتِنَا فِيْ سَبْعِ بَقَرٰتٍ سِمَانٍ يَّأْكُلُهُنَّ سَبْعٌ عِجَافٌ وَّسَبْعِ سُنْۢبُلٰتٍ خُضْرٍ وَّاُخَرَ يٰبِسٰتٍۙ لَّعَلِّيْٓ اَرْجِعُ اِلَى النَّاسِ لَعَلَّهُمْ يَعْلَمُوْنَ

yuusufu ayyuh*aa* **al***shsh*iddiiqu aftin*aa *fii sab'i baqar*aa*tin sim*aa*nin ya/kuluhunna sab'un 'ij*aa*fun wasab'i sunbul*aa*tin khu*dh*rin waukhara y*aa*bis*aa*tin la'allii arji'u il

”Yusuf, wahai orang yang sangat dipercaya! Terangkanlah kepada kami (takwil mimpi) tentang tujuh ekor sapi betina yang gemuk yang dimakan oleh tujuh (ekor sapi betina) yang kurus, tujuh tangkai (gandum) yang hijau dan (tujuh tangkai) lainnya yang kering a

12:47

# قَالَ تَزْرَعُوْنَ سَبْعَ سِنِيْنَ دَاَبًاۚ فَمَا حَصَدْتُّمْ فَذَرُوْهُ فِيْ سُنْۢبُلِهٖٓ اِلَّا قَلِيْلًا مِّمَّا تَأْكُلُوْنَ

q*aa*la tazra'uuna sab'a siniina da-aban fam*aa* *h*a*sh*adtum fa*dz*aruuhu fii sunbulihi ill*aa* qaliilan mimm*aa* ta/kuluun**a**

Dia (Yusuf) berkata, “Agar kamu bercocok tanam tujuh tahun (berturut-turut) sebagaimana biasa; kemudian apa yang kamu tuai hendaklah kamu biarkan di tangkainya kecuali sedikit untuk kamu makan.

12:48

# ثُمَّ يَأْتِيْ مِنْۢ بَعْدِ ذٰلِكَ سَبْعٌ شِدَادٌ يَّأْكُلْنَ مَا قَدَّمْتُمْ لَهُنَّ اِلَّا قَلِيْلًا مِّمَّا تُحْصِنُوْنَ

tsumma ya/tii min ba'di *dzaa*lika sab'un syid*aa*dun ya/kulna m*aa* qaddamtum lahunna ill*aa* qaliilan mimm*aa* tu*hs*inuun**a**

Kemudian setelah itu akan datang tujuh (tahun) yang sangat sulit, yang menghabiskan apa yang kamu simpan untuk menghadapinya (tahun sulit), kecuali sedikit dari apa (bibit gandum) yang kamu simpan.

12:49

# ثُمَّ يَأْتِيْ مِنْۢ بَعْدِ ذٰلِكَ عَامٌ فِيْهِ يُغَاثُ النَّاسُ وَفِيْهِ يَعْصِرُوْنَ ࣖ

tsumma ya/tii min ba'di *dzaa*lika '*aa*mun fiihi yugh*aa*tsu **al**nn*aa*su wafiihi ya'*sh*iruun**a**

Setelah itu akan datang tahun, di mana manusia diberi hujan (dengan cukup) dan pada masa itu mereka memeras (anggur).”

12:50

# وَقَالَ الْمَلِكُ ائْتُوْنِيْ بِهٖ ۚفَلَمَّا جَاۤءَهُ الرَّسُوْلُ قَالَ ارْجِعْ اِلٰى رَبِّكَ فَسْـَٔلْهُ مَا بَالُ النِّسْوَةِ الّٰتِيْ قَطَّعْنَ اَيْدِيَهُنَّ ۗاِنَّ رَبِّيْ بِكَيْدِهِنَّ عَلِيْمٌ

waq*aa*la **a**lmaliku i/tuunii bihi falamm*aa* j*aa*-ahu **al**rrasuulu q*aa*la irji' il*aa* rabbika fa**i**s-alhu m*aa* b*aa*lu **al**nniswati **al**l

Dan raja berkata, “Bawalah dia kepadaku.” Ketika utusan itu datang kepadanya, dia (Yusuf) berkata, “Kembalilah kepada tuanmu dan tanyakan kepadanya bagaimana halnya perempuan-perempuan yang telah melukai tangannya. Sungguh, Tuhanku Maha Mengetahui tipu da

12:51

# قَالَ مَا خَطْبُكُنَّ اِذْ رَاوَدْتُّنَّ يُوْسُفَ عَنْ نَّفْسِهٖۗ قُلْنَ حَاشَ لِلّٰهِ مَا عَلِمْنَا عَلَيْهِ مِنْ سُوْۤءٍ ۗقَالَتِ امْرَاَتُ الْعَزِيْزِ الْـٰٔنَ حَصْحَصَ الْحَقُّۖ اَنَا۠ رَاوَدْتُّهٗ عَنْ نَّفْسِهٖ وَاِنَّهٗ لَمِنَ الصّٰدِقِيْنَ

q*aa*la m*aa* kha*th*bukunna i*dz* r*aa*wadtunna yuusufa 'an nafsihi qulna *haa*sya lill*aa*hi m*aa* 'alimn*aa* 'alayhi min suu-in q*aa*lati imra-atu **a**l'aziizi **a**l-*aa*

*Dia (raja) berkata (kepada perempuan-perempuan itu), “Bagaimana keadaanmu ketika kamu menggoda Yusuf untuk menundukkan dirinya?” Mereka berkata, “Mahasempurna Allah, kami tidak mengetahui sesuatu keburukan darinya.” Istri Al-Aziz berkata, “Sekarang jelasl*

12:52

# ذٰلِكَ لِيَعْلَمَ اَنِّيْ لَمْ اَخُنْهُ بِالْغَيْبِ وَاَنَّ اللّٰهَ لَا يَهْدِيْ كَيْدَ الْخَاۤىِٕنِيْنَ ۔

*dzaa*lika liya'lama annii lam akhunhu bi**a**lghaybi wa-anna **al**l*aa*ha l*aa* yahdii kayda **a**lkh*aa*-iniin**a**

(Yusuf berkata), “Yang demikian itu agar dia (Al-Aziz) mengetahui bahwa aku benar-benar tidak mengkhianatinya ketika dia tidak ada (di rumah), dan bahwa Allah tidak meridai tipu daya orang-orang yang berkhianat.

12:53

# ۞ وَمَآ اُبَرِّئُ نَفْسِيْۚ اِنَّ النَّفْسَ لَاَمَّارَةٌ ۢ بِالسُّوْۤءِ اِلَّا مَا رَحِمَ رَبِّيْۗ اِنَّ رَبِّيْ غَفُوْرٌ رَّحِيْمٌ

wam*aa* ubarri-u nafsii inna **al**nnafsa la-amm*aa*ratun bi**al**ssuu-i ill*aa* m*aa* ra*h*ima rabbii inna rabbii ghafuurun ra*h*iim**un**

Dan aku tidak (menyatakan) diriku bebas (dari kesalahan), karena sesungguhnya nafsu itu selalu mendorong kepada kejahatan, kecuali (nafsu) yang diberi rahmat oleh Tuhanku. Sesungguhnya Tuhanku Maha Pengampun, Maha Penyayang.

12:54

# وَقَالَ الْمَلِكُ ائْتُوْنِيْ بِهٖٓ اَسْتَخْلِصْهُ لِنَفْسِيْۚ فَلَمَّا كَلَّمَهٗ قَالَ اِنَّكَ الْيَوْمَ لَدَيْنَا مَكِيْنٌ اَمِيْنٌ

waq*aa*la **a**lmaliku i/tuunii bihi astakhli*sh*hu linafsii falamm*aa* kallamahu q*aa*la innaka **a**lyawma ladayn*aa* makiinun amiin**un**

Dan raja berkata, “Bawalah dia (Yusuf) kepadaku, agar aku memilih dia (sebagai orang yang dekat) kepadaku.” Ketika dia (raja) telah bercakap-cakap dengan dia, dia (raja) berkata, “Sesungguhnya kamu (mulai) hari ini menjadi seorang yang berkedudukan tingg

12:55

# قَالَ اجْعَلْنِيْ عَلٰى خَزَاۤىِٕنِ الْاَرْضِۚ اِنِّيْ حَفِيْظٌ عَلِيْمٌ

q*aa*la ij'alnii 'al*aa* khaz*aa*-ini **a**l-ar*dh*i innii *h*afii*zh*un 'aliim**un**

Dia (Yusuf) berkata, “Jadikanlah aku bendaharawan negeri (Mesir); karena sesungguhnya aku adalah orang yang pandai menjaga, dan berpengetahuan.”

12:56

# وَكَذٰلِكَ مَكَّنَّا لِيُوْسُفَ فِى الْاَرْضِ يَتَبَوَّاُ مِنْهَا حَيْثُ يَشَاۤءُۗ نُصِيْبُ بِرَحْمَتِنَا مَنْ نَّشَاۤءُ وَلَا نُضِيْعُ اَجْرَ الْمُحْسِنِيْنَ

waka*dzaa*lika makann*aa* liyuusufa fii **a**l-ar*dh*i yatabawwau minh*aa* *h*aytsu yasy*aa*u nu*sh*iibu bira*h*matin*aa* man nasy*aa*u wal*aa* nu*dh*ii'u ajra **a**lmu<

Dan demikianlah Kami memberi kedudukan kepada Yusuf di negeri ini (Mesir); untuk tinggal di mana saja yang dia kehendaki. Kami melimpahkan rahmat kepada siapa yang Kami kehendaki dan Kami tidak menyia-nyiakan pahala orang yang berbuat baik.

12:57

# وَلَاَجْرُ الْاٰخِرَةِ خَيْرٌ لِّلَّذِيْنَ اٰمَنُوْا وَكَانُوْا يَتَّقُوْنَ ࣖ

wala-ajru **a**l-*aa*khirati khayrun lilla*dz*iina *aa*manuu wak*aa*nuu yattaquun**a**

Dan sungguh, pahala akhirat itu lebih baik bagi orang-orang yang beriman dan selalu bertakwa.

12:58

# وَجَاۤءَ اِخْوَةُ يُوْسُفَ فَدَخَلُوْا عَلَيْهِ فَعَرَفَهُمْ وَهُمْ لَهٗ مُنْكِرُوْنَ

waj*aa*-a ikhwatu yuusufa fadakhaluu 'alayhi fa'arafahum wahum lahu munkiruun**a**

Dan saudara-saudara Yusuf datang (ke Mesir) lalu mereka masuk ke (tempat)nya. Maka dia (Yusuf) mengenal mereka, sedang mereka tidak mengenalinya (lagi) kepadanya.

12:59

# وَلَمَّا جَهَّزَهُمْ بِجَهَازِهِمْ قَالَ ائْتُوْنِيْ بِاَخٍ لَّكُمْ مِّنْ اَبِيْكُمْ ۚ اَلَا تَرَوْنَ اَنِّيْٓ اُوْفِى الْكَيْلَ وَاَنَا۠ خَيْرُ الْمُنْزِلِيْنَ

walamm*aa* jahhazahum bijah*aa*zihim q*aa*la i/tuunii bi-akhin lakum min abiikum **a**l*aa* tarawna annii uufii **a**lkayla wa-an*aa* khayru **a**lmunziliin**a**

Dan ketika dia (Yusuf) menyiapkan bahan makanan untuk mereka, dia berkata, “Bawalah kepadaku saudaramu yang seayah dengan kamu (Bunyamin), tidakkah kamu melihat bahwa aku menyempurnakan takaran dan aku adalah penerima tamu yang terbaik?

12:60

# فَاِنْ لَّمْ تَأْتُوْنِيْ بِهٖ فَلَا كَيْلَ لَكُمْ عِنْدِيْ وَلَا تَقْرَبُوْنِ

fa-in lam ta/tuunii bihi fal*aa* kayla lakum 'indii wal*aa* taqrabuun**i**

Maka jika kamu tidak membawanya kepadaku, maka kamu tidak akan mendapat jatah (gandum) lagi dariku dan jangan kamu mendekatiku.”

12:61

# قَالُوْا سَنُرَاوِدُ عَنْهُ اَبَاهُ وَاِنَّا لَفَاعِلُوْنَ

q*aa*luu sanur*aa*widu 'anhu ab*aa*hu wa-inn*aa* laf*aa*'iluun**a**

Mereka berkata, “Kami akan membujuk ayahnya (untuk membawanya) dan kami benar-benar akan melaksanakannya.”

12:62

# وَقَالَ لِفِتْيٰنِهِ اجْعَلُوْا بِضَاعَتَهُمْ فِيْ رِحَالِهِمْ لَعَلَّهُمْ يَعْرِفُوْنَهَآ اِذَا انْقَلَبُوْٓا اِلٰٓى اَهْلِهِمْ لَعَلَّهُمْ يَرْجِعُوْنَ

waq*aa*la lifity*aa*nihi ij'aluu bi*daa*'atahum fii ri*haa*lihim la'allahum ya'rifuunah*aa* i*dzaa* inqalabuu il*aa* ahlihim la'allahum yarji'uun**a**

Dan dia (Yusuf) berkata kepada pelayan-pelayannya, “Masukkanlah barang-barang (penukar) mereka ke dalam karung-karungnya, agar mereka mengetahuinya apabila telah kembali kepada keluarganya, mudah-mudahan mereka kembali lagi.”

12:63

# فَلَمَّا رَجَعُوْٓا اِلٰٓى اَبِيْهِمْ قَالُوْا يٰٓاَبَانَا مُنِعَ مِنَّا الْكَيْلُ فَاَرْسِلْ مَعَنَآ اَخَانَا نَكْتَلْ وَاِنَّا لَهٗ لَحٰفِظُوْنَ

falamm*aa* raja'uu il*aa* abiihim q*aa*luu y*aa* ab*aa*n*aa* muni'a minn*aa* **a**lkaylu fa-arsil ma'an*aa* akh*aa*n*aa* naktal wa-inn*aa* lahu la*haa*fi*zh*uun**a**

**Maka ketika mereka telah kembali kepada ayahnya (Yakub) mereka berkata, “Wahai ayah kami! Kami tidak akan mendapat jatah (gandum) lagi, (jika tidak membawa saudara kami), sebab itu biarkanlah saudara kami pergi bersama kami agar kami mendapat jatah, dan k**

12:64

# قَالَ هَلْ اٰمَنُكُمْ عَلَيْهِ اِلَّا كَمَآ اَمِنْتُكُمْ عَلٰٓى اَخِيْهِ مِنْ قَبْلُۗ فَاللّٰهُ خَيْرٌ حٰفِظًا وَّهُوَ اَرْحَمُ الرّٰحِمِيْنَ

q*aa*la hal *aa*manukum 'alayhi ill*aa* kam*aa* amintukum 'al*aa* akhiihi min qablu fa**al**l*aa*hu khayrun *haa*fi*zh*an wahuwa ar*h*amu **al**rr*aah*imiin**a**

Dia (Yakub) berkata, “Bagaimana aku akan mempercayakannya (Bunyamin) kepadamu, seperti aku telah mempercayakan saudaranya (Yusuf) kepada kamu dahulu?” Maka Allah adalah penjaga yang terbaik dan Dia Maha Penyayang di antara para penyayang.

12:65

# وَلَمَّا فَتَحُوْا مَتَاعَهُمْ وَجَدُوْا بِضَاعَتَهُمْ رُدَّتْ اِلَيْهِمْۗ قَالُوْا يٰٓاَبَانَا مَا نَبْغِيْۗ هٰذِهٖ بِضَاعَتُنَا رُدَّتْ اِلَيْنَا وَنَمِيْرُ اَهْلَنَا وَنَحْفَظُ اَخَانَا وَنَزْدَادُ كَيْلَ بَعِيْرٍۗ ذٰلِكَ كَيْلٌ يَّسِيْرٌ

walamm*aa* fata*h*uu mat*aa*'ahum wajaduu bi*daa*'atahum ruddat ilayhim q*aa*luu y*aa* ab*aa*n*aa* m*aa* nabghii h*aadz*ihi bi*daa*'atun*aa* ruddat ilayn*aa* wanamiiru ahlan*aa* wana

Dan ketika mereka membuka barang-barangnya, mereka menemukan barang-barang (penukar) mereka dikembalikan kepada mereka. Mereka berkata, “Wahai ayah kami! Apalagi yang kita inginkan. Ini barang-barang kita dikembalikan kepada kita, dan kita akan dapat memb

12:66

# قَالَ لَنْ اُرْسِلَهٗ مَعَكُمْ حَتّٰى تُؤْتُوْنِ مَوْثِقًا مِّنَ اللّٰهِ لَتَأْتُنَّنِيْ بِهٖٓ اِلَّآ اَنْ يُّحَاطَ بِكُمْۚ فَلَمَّآ اٰتَوْهُ مَوْثِقَهُمْ قَالَ اللّٰهُ عَلٰى مَا نَقُوْلُ وَكِيْلٌ

q*aa*la lan ursilahu ma'akum *h*att*aa* tu/tuuni mawtsiqan mina **al**l*aa*hi lata/tunnanii bihi ill*aa* an yu*hath*a bikum falamm*aa* *aa*tawhu mawtsiqahum q*aa*la **al**l*aa*hu

Dia (Yakub) berkata, “Aku tidak akan melepaskannya (pergi) bersama kamu, sebelum kamu bersumpah kepadaku atas (nama) Allah, bahwa kamu pasti akan membawanya kepadaku kembali, kecuali jika kamu dikepung (musuh).” Setelah mereka mengucapkan sumpah, dia (Yak

12:67

# وَقَالَ يٰبَنِيَّ لَا تَدْخُلُوْا مِنْۢ بَابٍ وَّاحِدٍ وَّادْخُلُوْا مِنْ اَبْوَابٍ مُّتَفَرِّقَةٍۗ وَمَآ اُغْنِيْ عَنْكُمْ مِّنَ اللّٰهِ مِنْ شَيْءٍۗ اِنِ الْحُكْمُ اِلَّا لِلّٰهِ ۗعَلَيْهِ تَوَكَّلْتُ وَعَلَيْهِ فَلْيَتَوَكَّلِ الْمُتَوَكِّلُوْنَ

waq*aa*la y*aa* baniyya l*aa* tadkhuluu min b*aa*bin w*aah*idin wa**u**dkhuluu min abw*aa*bin mutafarriqatin wam*aa* ughnii 'ankum mina **al**l*aa*hi min syay-in ini **a**l*h*

Dan dia (Yakub) berkata, “Wahai anak-anakku! Janganlah kamu masuk dari satu pintu gerbang, dan masuklah dari pintu-pintu gerbang yang berbeda; namun demikian aku tidak dapat mempertahankan kamu sedikit pun dari (takdir) Allah. Keputusan itu hanyalah bagi

12:68

# وَلَمَّا دَخَلُوْا مِنْ حَيْثُ اَمَرَهُمْ اَبُوْهُمْۗ مَا كَانَ يُغْنِيْ عَنْهُمْ مِّنَ اللّٰهِ مِنْ شَيْءٍ اِلَّا حَاجَةً فِيْ نَفْسِ يَعْقُوْبَ قَضٰىهَاۗ وَاِنَّهٗ لَذُوْ عِلْمٍ لِّمَا عَلَّمْنٰهُ وَلٰكِنَّ اَكْثَرَ النَّاسِ لَا يَعْلَمُوْنَ ࣖ

walamm*aa* dakhaluu min *h*aytsu amarahum abuuhum m*aa* k*aa*na yughnii 'anhum mina **al**l*aa*hi min syay-in ill*aa* *haa*jatan fii nafsi ya'quuba qa*daa*h*aa* wa-innahu la*dz*uu 'ilmin lim

Dan ketika mereka masuk sesuai dengan perintah ayah mereka, (masuknya mereka itu) tidak dapat menolak sedikit pun keputusan Allah, (tetapi itu) hanya suatu keinginan pada diri Yakub yang telah ditetapkannya. Dan sesungguhnya dia mempunyai pengetahuan, kar

12:69

# وَلَمَّا دَخَلُوْا عَلٰى يُوْسُفَ اٰوٰٓى اِلَيْهِ اَخَاهُ قَالَ اِنِّيْٓ اَنَا۠ اَخُوْكَ فَلَا تَبْتَىِٕسْ بِمَا كَانُوْا يَعْمَلُوْنَ

walamm*aa* dakhaluu 'al*aa* yuusufa *aa*w*aa* ilayhi akh*aa*hu q*aa*la innii an*aa* akhuuka fal*aa* tabta-is bim*aa* k*aa*nuu ya'maluun**a**

Dan ketika mereka masuk ke (tempat) Yusuf, dia menempatkan saudaranya (Bunyamin) di tempatnya, dia (Yusuf) berkata, “Sesungguhnya aku adalah saudaramu, jangan engkau bersedih hati terhadap apa yang telah mereka kerjakan.”

12:70

# فَلَمَّا جَهَّزَهُمْ بِجَهَازِهِمْ جَعَلَ السِّقَايَةَ فِيْ رَحْلِ اَخِيْهِ ثُمَّ اَذَّنَ مُؤَذِّنٌ اَيَّتُهَا الْعِيْرُ اِنَّكُمْ لَسَارِقُوْنَ

falamm*aa* jahhazahum bijah*aa*zihim ja'ala **al**ssiq*aa*yata fii ra*h*li akhiihi tsumma a*dzdz*ana mu-a*dzdz*inun ayyatuh*aa* **a**l'iiru innakum las*aa*riquun**a**

Maka ketika telah disiapkan bahan makanan untuk mereka, dia (Yusuf) memasukkan piala ke dalam karung saudaranya. Kemudian berteriaklah seseorang yang menyerukan, “Wahai kafilah! Sesungguhnya kamu pasti pencuri.”

12:71

# قَالُوْا وَاَقْبَلُوْا عَلَيْهِمْ مَّاذَا تَفْقِدُوْنَ

q*aa*luu wa-aqbaluu 'alayhim m*aatsaa* tafqiduun**a**

Mereka bertanya, sambil menghadap kepada mereka (yang menuduh), “Kamu kehilangan apa?”

12:72

# قَالُوْا نَفْقِدُ صُوَاعَ الْمَلِكِ وَلِمَنْ جَاۤءَ بِهٖ حِمْلُ بَعِيْرٍ وَّاَنَا۠ بِهٖ زَعِيْمٌ

q*aa*luu nafqidu *sh*uw*aa*'a **a**lmaliki waliman j*aa*-a bihi *h*imlu ba'iirin wa-an*aa* bihi za'iim**un**

Mereka menjawab, “Kami kehilangan piala raja, dan siapa yang dapat mengembalikannya akan memperoleh (bahan makanan seberat) beban unta, dan aku jamin itu.”

12:73

# قَالُوْا تَاللّٰهِ لَقَدْ عَلِمْتُمْ مَّا جِئْنَا لِنُفْسِدَ فِى الْاَرْضِ وَمَا كُنَّا سَارِقِيْنَ

q*aa*luu ta**al**l*aa*hi laqad 'alimtum m*aa* ji/n*aa* linufsida fii **a**l-ar*dh*i wam*aa* kunn*aa* s*aa*riqiin**a**

Mereka (saudara-saudara Yusuf) menjawab, “Demi Allah, sungguh, kamu mengetahui bahwa kami datang bukan untuk berbuat kerusakan di negeri ini dan kami bukanlah para pencuri.”

12:74

# قَالُوْا فَمَا جَزَاۤؤُهٗٓ اِنْ كُنْتُمْ كٰذِبِيْنَ

q*aa*luu fam*aa* jaz*aa*uhu in kuntum k*aadz*ibiin**a**

Mereka berkata, “Tetapi apa hukumannya jika kamu dusta?”

12:75

# قَالُوْا جَزَاۤؤُهٗ مَنْ وُّجِدَ فِيْ رَحْلِهٖ فَهُوَ جَزَاۤؤُهٗ ۗ كَذٰلِكَ نَجْزِى الظّٰلِمِيْنَ

q*aa*luu jaz*aa*uhu man wujida fii ra*h*lihi fahuwa jaz*aa*uhu ka*dzaa*lika najzii **al***zhzhaa*limiin**a**

Mereka menjawab, “Hukumannya ialah pada siapa ditemukan dalam karungnya (barang yang hilang itu), maka dia sendirilah menerima hukumannya. Demikianlah kami memberi hukuman kepada orang-orang zalim.”

12:76

# فَبَدَاَ بِاَوْعِيَتِهِمْ قَبْلَ وِعَاۤءِ اَخِيْهِ ثُمَّ اسْتَخْرَجَهَا مِنْ وِّعَاۤءِ اَخِيْهِۗ كَذٰلِكَ كِدْنَا لِيُوْسُفَۗ مَا كَانَ لِيَأْخُذَ اَخَاهُ فِيْ دِيْنِ الْمَلِكِ اِلَّآ اَنْ يَّشَاۤءَ اللّٰهُ ۗنَرْفَعُ دَرَجَاتٍ مَّنْ نَّشَاۤءُۗ وَفَوْقَ

fabada-a bi-aw'iyatihim qabla wi'*aa*-i akhiihi tsumma istakhrajah*aa* min wi'*aa*-i akhiihi ka*dzaa*lika kidn*aa* liyuusufa m*aa* k*aa*na liya/khu*dz*a akh*aa*hu fii diini **a**lmaliki ill*aa*

*Maka mulailah dia (memeriksa) karung-karung mereka sebelum (memeriksa) karung saudaranya sendiri, kemudian dia mengeluarkan (piala raja) itu dari karung saudaranya. Demikianlah Kami mengatur (rencana) untuk Yusuf. Dia tidak dapat menghukum saudaranya menu*

12:77

# ۞ قَالُوْٓا اِنْ يَّسْرِقْ فَقَدْ سَرَقَ اَخٌ لَّهٗ مِنْ قَبْلُۚ فَاَسَرَّهَا يُوْسُفُ فِيْ نَفْسِهٖ وَلَمْ يُبْدِهَا لَهُمْۚ قَالَ اَنْتُمْ شَرٌّ مَّكَانًا ۚوَاللّٰهُ اَعْلَمُ بِمَا تَصِفُوْنَ

q*aa*luu in yasriq faqad saraqa akhun lahu min qablu fa-asarrah*aa* yuusufu fii nafsihi walam yubdih*aa* lahum q*aa*la antum syarrun mak*aa*nan wa**al**l*aa*hu a'lamu bim*aa* ta*sh*ifuun**a**

**Mereka berkata, “Jika dia mencuri, maka sungguh sebelum itu saudaranya pun pernah pula mencuri.” Maka Yusuf menyembunyikan (kejengkelan) dalam hatinya dan tidak ditampakkannya kepada mereka. Dia berkata (dalam hatinya), “Kedudukanmu justru lebih buruk. Da**

12:78

# قَالُوْا يٰٓاَيُّهَا الْعَزِيْزُ اِنَّ لَهٗٓ اَبًا شَيْخًا كَبِيْرًا فَخُذْ اَحَدَنَا مَكَانَهٗ ۚاِنَّا نَرٰىكَ مِنَ الْمُحْسِنِيْنَ

q*aa*luu y*aa* ayyuh*aa* **a**l'aziizu inna lahu aban syaykhan kabiiran fakhu*dz* a*h*adan*aa* mak*aa*nahu inn*aa* nar*aa*ka mina **a**lmu*h*siniin**a**

Mereka berkata, “Wahai Al-Aziz! Dia mempunyai ayah yang sudah lanjut usia, karena itu ambillah salah seorang di antara kami sebagai gantinya, sesungguhnya kami melihat engkau termasuk orang-orang yang berbuat baik.”

12:79

# قَالَ مَعَاذَ اللّٰهِ اَنْ نَّأْخُذَ اِلَّا مَنْ وَّجَدْنَا مَتَاعَنَا عِنْدَهٗٓ ۙاِنَّآ اِذًا لَّظٰلِمُوْنَ ࣖ

q*aa*la ma'*aadz*a **al**l*aa*hi an na/khu*dz*a ill*aa* man wajadn*aa* mat*aa*'an*aa* 'indahu inn*aa* i*dz*an la*zhaa*limuun**a**

Dia (Yusuf) berkata, “Aku memohon perlindungan kepada Allah dari menahan (seseorang), kecuali orang yang kami temukan harta kami padanya, jika kami (berbuat) demikian, berarti kami orang yang zalim.”

12:80

# فَلَمَّا اسْتَا۟يْـَٔسُوْا مِنْهُ خَلَصُوْا نَجِيًّاۗ قَالَ كَبِيْرُهُمْ اَلَمْ تَعْلَمُوْٓا اَنَّ اَبَاكُمْ قَدْ اَخَذَ عَلَيْكُمْ مَّوْثِقًا مِّنَ اللّٰهِ وَمِنْ قَبْلُ مَا فَرَّطْتُّمْ فِيْ يُوْسُفَ فَلَنْ اَبْرَحَ الْاَرْضَ حَتّٰى يَأْذَنَ لِيْٓ اَبِي

falamm*aa* istay-asuu minhu khala*sh*uu najiyyan q*aa*la kabiiruhum alam ta'lamuu anna ab*aa*kum qad akha*dz*a 'alaykum mawtsiqan mina **al**l*aa*hi wamin qablu m*aa* farra*th*tum fii yuusufa falan abra

Maka ketika mereka berputus asa darinya (putusan Yusuf) mereka menyendiri (sambil berunding) dengan berbisik-bisik. Yang tertua di antara mereka berkata, “Tidakkah kamu ketahui bahwa ayahmu telah mengambil janji dari kamu dengan (nama) Allah dan sebelum i

12:81

# اِرْجِعُوْٓا اِلٰٓى اَبِيْكُمْ فَقُوْلُوْا يٰٓاَبَانَآ اِنَّ ابْنَكَ سَرَقَۚ وَمَا شَهِدْنَآ اِلَّا بِمَا عَلِمْنَا وَمَا كُنَّا لِلْغَيْبِ حٰفِظِيْنَ

irji'uu il*aa* abiikum faquuluu y*aa* ab*aa*n*aa* inna ibnaka saraqa wam*aa* syahidn*aa* ill*aa* bim*aa* 'alimn*aa* wam*aa* kunn*aa* lilghaybi *haa*fi*zh*iin**a**

Kembalilah kepada ayahmu dan katakanlah, “Wahai ayah kami! Sesungguhnya anakmu telah mencuri dan kami hanya menyaksikan apa yang kami ketahui dan kami tidak mengetahui apa yang di balik itu.

12:82

# وَسْـَٔلِ الْقَرْيَةَ الَّتِيْ كُنَّا فِيْهَا وَالْعِيْرَ الَّتِيْٓ اَقْبَلْنَا فِيْهَاۗ وَاِنَّا لَصٰدِقُوْنَ

wa**i**s-ali **a**lqaryata **al**latii kunn*aa* fiih*aa* wa**a**l'iira **al**latii aqbaln*aa* fiih*aa* wa-inn*aa* la*shaa*diquun**a**

Dan tanyalah (penduduk) negeri tempat kami berada, dan kafilah yang datang bersama kami. Dan kami adalah orang yang benar.”

12:83

# قَالَ بَلْ سَوَّلَتْ لَكُمْ اَنْفُسُكُمْ اَمْرًاۗ فَصَبْرٌ جَمِيْلٌ ۗعَسَى اللّٰهُ اَنْ يَّأْتِيَنِيْ بِهِمْ جَمِيْعًاۗ اِنَّهٗ هُوَ الْعَلِيْمُ الْحَكِيْمُ

q*aa*la bal sawwalat lakum anfusukum amran fa*sh*abrun jamiilun 'as*aa* **al**l*aa*hu an ya/tiyanii bihim jamii'an innahu huwa **a**l'aliimu **a**l*h*akiim**u**

Dia (Yakub) berkata, “Sebenarnya hanya dirimu sendiri yang memandang baik urusan (yang buruk) itu. Maka (kesabaranku) adalah kesabaran yang baik. Mudah-mudahan Allah mendatangkan mereka semuanya kepadaku. Sungguh, Dialah Yang Maha Mengetahui, Mahabijaksan

12:84

# وَتَوَلّٰى عَنْهُمْ وَقَالَ يٰٓاَسَفٰى عَلٰى يُوْسُفَ وَابْيَضَّتْ عَيْنٰهُ مِنَ الْحُزْنِ فَهُوَ كَظِيْمٌ

watawall*aa* 'anhum waq*aa*la y*aa* asaf*aa* 'al*aa* yuusufa wa**i**bya*dhdh*at 'ayn*aa*hu mina **a**l*h*uzni fahuwa ka*zh*iim**un**

Dan dia (Yakub) berpaling dari mereka (anak-anaknya) seraya berkata, “Aduhai dukacitaku terhadap Yusuf,” dan kedua matanya menjadi putih karena sedih. Dia diam menahan amarah (terhadap anak-anaknya).

12:85

# قَالُوْا تَاللّٰهِ تَفْتَؤُا تَذْكُرُ يُوْسُفَ حَتّٰى تَكُوْنَ حَرَضًا اَوْ تَكُوْنَ مِنَ الْهَالِكِيْنَ

q*aa*luu ta**al**l*aa*hi taftau ta*dz*kuru yuusufa *h*att*aa* takuuna *h*ara*dh*an aw takuuna mina **a**lh*aa*likiin**a**

Mereka berkata, “Demi Allah, engkau tidak henti-hentinya mengingat Yusuf, sehingga engkau (mengidap) penyakit berat atau engkau termasuk orang-orang yang akan binasa.”

12:86

# قَالَ اِنَّمَآ اَشْكُوْا بَثِّيْ وَحُزْنِيْٓ اِلَى اللّٰهِ وَاَعْلَمُ مِنَ اللّٰهِ مَا لَا تَعْلَمُوْنَ

q*aa*la innam*aa* asykuu batstsii wa*h*uznii il*aa* **al**l*aa*hi wa-a'lamu mina **al**l*aa*hi m*aa* l*aa* ta'lamuun**a**

Dia (Yakub) menjawab, “Hanya kepada Allah aku mengadukan kesusahan dan kesedihanku. Dan aku mengetahui dari Allah apa yang tidak kamu ketahui.

12:87

# يٰبَنِيَّ اذْهَبُوْا فَتَحَسَّسُوْا مِنْ يُّوْسُفَ وَاَخِيْهِ وَلَا تَا۟يْـَٔسُوْا مِنْ رَّوْحِ اللّٰهِ ۗاِنَّهٗ لَا يَا۟يْـَٔسُ مِنْ رَّوْحِ اللّٰهِ اِلَّا الْقَوْمُ الْكٰفِرُوْنَ

y*aa* baniyya i*dz*habuu fata*h*assasuu min yuusufa wa-akhiihi wal*aa* tay-asuu min raw*h*i **al**l*aa*hi innahu l*aa* yay-asu min raw*h*i **al**l*aa*hi ill*aa* **a**

Wahai anak-anakku! Pergilah kamu, carilah (berita) tentang Yusuf dan saudaranya dan jangan kamu berputus asa dari rahmat Allah. Sesungguhnya yang berputus asa dari rahmat Allah, hanyalah orang-orang yang kafir.”

12:88

# فَلَمَّا دَخَلُوْا عَلَيْهِ قَالُوْا يٰٓاَيُّهَا الْعَزِيْزُ مَسَّنَا وَاَهْلَنَا الضُّرُّ وَجِئْنَا بِبِضَاعَةٍ مُّزْجٰىةٍ فَاَوْفِ لَنَا الْكَيْلَ وَتَصَدَّقْ عَلَيْنَاۗ اِنَّ اللّٰهَ يَجْزِى الْمُتَصَدِّقِيْنَ

falamm*aa* dakhaluu 'alayhi q*aa*luu y*aa* ayyuh*aa* **a**l'aziizu massan*aa* wa-ahlan*aa* **al***dhdh*urru waji/n*aa *bibi*daa*'atin muzj*aa*tin fa-awfi lan*aa* **a**

**Maka ketika mereka masuk ke (tempat) Yusuf, mereka berkata, “Wahai Al-Aziz! Kami dan keluarga kami telah ditimpa kesengsaraan dan kami datang membawa barang-barang yang tidak berharga, maka penuhilah jatah (gandum) untuk kami, dan bersedekahlah kepada kam**

12:89

# قَالَ هَلْ عَلِمْتُمْ مَّا فَعَلْتُمْ بِيُوْسُفَ وَاَخِيْهِ اِذْ اَنْتُمْ جَاهِلُوْنَ

q*aa*la hal 'alimtum m*aa* fa'altum biyuusufa wa-akhiihi i*dz* antum j*aa*hiluun**a**

Dia (Yusuf) berkata, “Tahukah kamu (kejelekan) apa yang telah kamu perbuat terhadap Yusuf dan saudaranya karena kamu tidak menyadari (akibat) perbuatanmu itu?”

12:90

# قَالُوْٓا ءَاِنَّكَ لَاَنْتَ يُوْسُفُۗ قَالَ اَنَا۠ يُوْسُفُ وَهٰذَآ اَخِيْ قَدْ مَنَّ اللّٰهُ عَلَيْنَاۗ اِنَّهٗ مَنْ يَّتَّقِ وَيَصْبِرْ فَاِنَّ اللّٰهَ لَا يُضِيْعُ اَجْرَ الْمُحْسِنِيْنَ

q*aa*luu a-innaka la-anta yuusufa q*aa*la an*aa* yuusufu wah*aadzaa* akhii qad manna **al**l*aa*hu 'alayn*aa* innahu man yattaqi waya*sh*bir fa-inna **al**l*aa*ha l*aa* yu*dh*ii'u

Mereka berkata, “Apakah engkau benar-benar Yusuf?” Dia (Yusuf) menjawab, “Aku Yusuf dan ini saudaraku. Sungguh, Allah telah melimpahkan karunia-Nya kepada kami. Sesungguhnya barangsiapa bertakwa dan bersabar, maka Sungguh, Allah tidak menyia-nyiakan pahal

12:91

# قَالُوْا تَاللّٰهِ لَقَدْ اٰثَرَكَ اللّٰهُ عَلَيْنَا وَاِنْ كُنَّا لَخٰطِـِٕيْنَ

q*aa*luu ta**al**l*aa*hi laqad *aa*tsaraka **al**l*aa*hu 'alayn*aa* wa-in kunn*aa* lakh*aath*i-iin**a**

Mereka berkata, “Demi Allah, sungguh Allah telah melebihkan engkau di atas kami, dan sesungguhnya kami adalah orang yang bersalah (berdosa).”

12:92

# قَالَ لَا تَثْرِيْبَ عَلَيْكُمُ الْيَوْمَۗ يَغْفِرُ اللّٰهُ لَكُمْ ۖوَهُوَ اَرْحَمُ الرّٰحِمِيْنَ

q*aa*la l*aa* tatsriiba 'alaykumu **a**lyawma yaghfiru **al**l*aa*hu lakum wahuwa ar*h*amu **al**rr*aah*imiin**a**

Dia (Yusuf) berkata, “Pada hari ini tidak ada cercaan terhadap kamu, mudah-mudahan Allah mengampuni kamu. Dan Dia Maha Penyayang di antara para penyayang.

12:93

# اِذْهَبُوْا بِقَمِيْصِيْ هٰذَا فَاَلْقُوْهُ عَلٰى وَجْهِ اَبِيْ يَأْتِ بَصِيْرًا ۚوَأْتُوْنِيْ بِاَهْلِكُمْ اَجْمَعِيْنَ ࣖ

i*dz*habuu biqamii*sh*ii h*aadzaa* fa-alquuhu 'al*aa* wajhi abii ya/ti ba*sh*iiran wa/tuunii bi-ahlikum ajma'iin**a**

Pergilah kamu dengan membawa bajuku ini, lalu usapkan ke wajah ayahku, nanti dia akan melihat kembali; dan bawalah seluruh keluargamu kepadaku.”

12:94

# وَلَمَّا فَصَلَتِ الْعِيْرُ قَالَ اَبُوْهُمْ اِنِّيْ لَاَجِدُ رِيْحَ يُوْسُفَ لَوْلَآ اَنْ تُفَنِّدُوْنِ

walamm*aa* fa*sh*alati **a**l'iiru q*aa*la abuuhum innii la-ajidu rii*h*a yuusufa lawl*aa* an tufanniduun**i**

Dan ketika kafilah itu telah keluar (dari negeri Mesir), ayah mereka berkata, “Sesungguhnya Aku mencium bau Yusuf, sekiranya kamu tidak menuduhku lemah akal (tentu kamu membenarkan aku).”

12:95

# قَالُوْا تَاللّٰهِ اِنَّكَ لَفِيْ ضَلٰلِكَ الْقَدِيْمِ

q*aa*luu ta**al**l*aa*hi innaka lafii *dh*al*aa*lika **a**lqadiim**i**

Mereka (keluarganya) berkata, “Demi Allah, sesungguhnya engkau masih dalam kekeliruanmu yang dahulu.”

12:96

# فَلَمَّآ اَنْ جَاۤءَ الْبَشِيْرُ اَلْقٰىهُ عَلٰى وَجْهِهٖ فَارْتَدَّ بَصِيْرًاۗ قَالَ اَلَمْ اَقُلْ لَّكُمْۙ اِنِّيْٓ اَعْلَمُ مِنَ اللّٰهِ مَا لَا تَعْلَمُوْنَ

falamm*aa* an j*aa*-a albasyiiru alq*aa*hu 'al*aa* wajhihi fa**i**rtadda ba*sh*iiran q*aa*la alam aqul lakum innii a'lamu mina **al**l*aa*hi m*aa* l*aa* ta'lamuun**a**

Maka ketika telah tiba pembawa kabar gembira itu, maka diusapkannya (baju itu) ke wajahnya (Yakub), lalu dia dapat melihat kembali. Dia (Yakub) berkata, “Bukankah telah aku katakan kepadamu, bahwa aku mengetahui dari Allah apa yang tidak kamu ketahui.”

12:97

# قَالُوْا يٰٓاَبَانَا اسْتَغْفِرْ لَنَا ذُنُوْبَنَآ اِنَّا كُنَّا خٰطِـِٕيْنَ

q*aa*la sawfa astaghfiru lakum rabbii innahu huwa **a**lghafuuru **al**rra*h*iim**u**

Mereka berkata, “Wahai ayah kami! Mohonkanlah ampunan untuk kami atas dosa-dosa kami, sesungguhnya kami adalah orang yang bersalah (berdosa).”

12:98

# قَالَ سَوْفَ اَسْتَغْفِرُ لَكُمْ رَبِّيْ ۗاِنَّهٗ هُوَ الْغَفُوْرُ الرَّحِيْمُ

falamm*aa* dakhaluu 'al*aa* yuusufa *aa*w*aa* ilayhi abawayhi waq*aa*la udkhuluu mi*sh*ra in sy*aa*-a **al**l*aa*hu *aa*miniin**a**

Dia (Yakub) berkata, “Aku akan memohonkan ampunan bagimu kepada Tuhanku. Sungguh, Dia Yang Maha Pengampun, Maha Penyayang.”

12:99

# فَلَمَّا دَخَلُوْا عَلٰى يُوْسُفَ اٰوٰٓى اِلَيْهِ اَبَوَيْهِ وَقَالَ ادْخُلُوْا مِصْرَ اِنْ شَاۤءَ اللّٰهُ اٰمِنِيْنَ ۗ

warafa'a abawayhi 'al*aa* **a**l'arsyi wakharruu lahu sujjadan waq*aa*la y*aa* abati h*aadzaa* ta/wiilu ru/y*aa*ya min qablu qad ja'alah*aa* rabbii *h*aqqan waqad a*h*sana bii i*dz* akhrajanii mina

Maka ketika mereka masuk ke (tempat) Yusuf, dia merangkul (dan menyiapkan tempat untuk) kedua orang tuanya seraya berkata, “Masuklah kamu ke negeri Mesir, insya Allah dalam keadaan aman.”

12:100

# وَرَفَعَ اَبَوَيْهِ عَلَى الْعَرْشِ وَخَرُّوْا لَهٗ سُجَّدًاۚ وَقَالَ يٰٓاَبَتِ هٰذَا تَأْوِيْلُ رُءْيَايَ مِنْ قَبْلُ ۖقَدْ جَعَلَهَا رَبِّيْ حَقًّاۗ وَقَدْ اَحْسَنَ بِيْٓ اِذْ اَخْرَجَنِيْ مِنَ السِّجْنِ وَجَاۤءَ بِكُمْ مِّنَ الْبَدْوِ مِنْۢ بَعْدِ اَن

rabbi qad *aa*taytanii mina **a**lmulki wa'allamtanii min ta/wiili **a**l-a*haa*diitsi f*aath*ira **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i anta waliyyii fii **al**

**Dan dia menaikkan kedua orang tuanya ke atas singgasana. Dan mereka (semua) tunduk bersujud kepadanya (Yusuf). Dan dia (Yusuf) berkata, “Wahai ayahku! Inilah takwil mimpiku yang dahulu itu. Dan sesungguhnya Tuhanku telah menjadikannya kenyataan. Sesungguh**

12:101

# ۞ رَبِّ قَدْ اٰتَيْتَنِيْ مِنَ الْمُلْكِ وَعَلَّمْتَنِيْ مِنْ تَأْوِيْلِ الْاَحَادِيْثِۚ فَاطِرَ السَّمٰوٰتِ وَالْاَرْضِۗ اَنْتَ وَلِيّٖ فِى الدُّنْيَا وَالْاٰخِرَةِۚ تَوَفَّنِيْ مُسْلِمًا وَّاَلْحِقْنِيْ بِالصّٰلِحِيْنَ

rabbi qad *aa*taytanii mina **a**lmulki wa'allamtanii min ta/wiili **a**l-a*haa*diitsi f*aath*ira **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i anta waliyyii fii **al**

**Tuhanku, sesungguhnya Engkau telah menganugerahkan kepadaku sebagian kekuasaan dan telah mengajarkan kepadaku sebagian takwil mimpi. (Wahai Tuhan) pencipta langit dan bumi, Engkaulah pelindungku di dunia dan di akhirat, wafatkanlah aku dalam keadaan musli**

12:102

# ذٰلِكَ مِنْ اَنْۢبَاۤءِ الْغَيْبِ نُوْحِيْهِ اِلَيْكَۚ وَمَا كُنْتَ لَدَيْهِمْ اِذْ اَجْمَعُوْٓا اَمْرَهُمْ وَهُمْ يَمْكُرُوْنَ

*dzaa*lika min anb*aa*-i **a**lghaybi nuu*h*iihi ilayka wam*aa* kunta ladayhim i*dz* ajma'uu amrahum wahum yamkuruun**a**

Itulah sebagian berita gaib yang Kami wahyukan kepadamu (Muhammad); padahal engkau tidak berada di samping mereka, ketika mereka bersepakat mengatur tipu muslihat (untuk memasukkan Yusuf ke dalam sumur).

12:103

# وَمَآ اَكْثَرُ النَّاسِ وَلَوْ حَرَصْتَ بِمُؤْمِنِيْنَ

wam*aa* aktsaru **al**nn*aa*si walaw *h*ara*sh*ta bimu/miniin**a**

Dan kebanyakan manusia tidak akan beriman walaupun engkau sangat menginginkannya.

12:104

# وَمَا تَسْـَٔلُهُمْ عَلَيْهِ مِنْ اَجْرٍۗ اِنْ هُوَ اِلَّا ذِكْرٌ لِّلْعٰلَمِيْنَ ࣖ

wam*aa* tas-aluhum 'alayhi min ajrin in huwa ill*aa* *dz*ikrun lil'*aa*lamiin**a**

Dan engkau tidak meminta imbalan apa pun kepada mereka (terhadap seruanmu ini), sebab (seruan) itu adalah pengajaran bagi seluruh alam.

12:105

# وَكَاَيِّنْ مِّنْ اٰيَةٍ فِى السَّمٰوٰتِ وَالْاَرْضِ يَمُرُّوْنَ عَلَيْهَا وَهُمْ عَنْهَا مُعْرِضُوْنَ

waka-ayyin min *aa*yatin fii **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i yamurruuna 'alayh*aa* wahum 'anh*aa* mu'ri*dh*uun**a**

Dan berapa banyak tanda-tanda (kebesaran Allah) di langit dan di bumi yang mereka lalui, namun mereka berpaling daripadanya.

12:106

# وَمَا يُؤْمِنُ اَكْثَرُهُمْ بِاللّٰهِ اِلَّا وَهُمْ مُّشْرِكُوْنَ

wam*aa* yu/minu aktsaruhum bi**al**l*aa*hi ill*aa* wahum musyrikuun**a**

Dan kebanyakan mereka tidak beriman kepada Allah, bahkan mereka mempersekutukan-Nya.

12:107

# اَفَاَمِنُوْٓا اَنْ تَأْتِيَهُمْ غَاشِيَةٌ مِّنْ عَذَابِ اللّٰهِ اَوْ تَأْتِيَهُمُ السَّاعَةُ بَغْتَةً وَّهُمْ لَا يَشْعُرُوْنَ

afa-aminuu an ta/tiyahum gh*aa*syiyatun min 'a*dzaa*bi **al**l*aa*hi aw ta/tiyahumu **al**ss*aa*'atu baghtatan wahum l*aa* yasy'uruun**a**

Apakah mereka merasa aman dari kedatangan siksa Allah yang meliputi mereka, atau kedatangan Kiamat kepada mereka secara mendadak, sedang mereka tidak menyadarinya?

12:108

# قُلْ هٰذِهٖ سَبِيْلِيْٓ اَدْعُوْٓا اِلَى اللّٰهِ ۗعَلٰى بَصِيْرَةٍ اَنَا۠ وَمَنِ اتَّبَعَنِيْ ۗوَسُبْحٰنَ اللّٰهِ وَمَآ اَنَا۠ مِنَ الْمُشْرِكِيْنَ

qul h*aadz*ihi sabiilii ad'uu il*aa* **al**l*aa*hi 'al*aa* ba*sh*iiratin an*aa* wamani ittaba'anii wasub*haa*na **al**l*aa*hi wam*aa* an*aa* mina **a**lmusyrikiin

Katakanlah (Muhammad), “Inilah jalanku, aku dan orang-orang yang mengikutiku mengajak (kamu) kepada Allah dengan yakin, Mahasuci Allah, dan aku tidak termasuk orang-orang musyrik.”

12:109

# وَمَآ اَرْسَلْنَا مِنْ قَبْلِكَ اِلَّا رِجَالًا نُّوْحِيْٓ اِلَيْهِمْ مِّنْ اَهْلِ الْقُرٰىۗ اَفَلَمْ يَسِيْرُوْا فِى الْاَرْضِ فَيَنْظُرُوْا كَيْفَ كَانَ عَاقِبَةُ الَّذِيْنَ مِنْ قَبْلِهِمْۗ وَلَدَارُ الْاٰخِرَةِ خَيْرٌ لِّلَّذِيْنَ اتَّقَوْاۗ اَفَلَا

wam*aa* arsaln*aa* min qablika ill*aa* rij*aa*lan nuu*h*ii ilayhim min ahli **a**lqur*aa* afalam yasiiruu fii **a**l-ar*dh*i fayan*zh*uruu kayfa k*aa*na '*aa*qibatu **al**

**Dan Kami tidak mengutus sebelummu (Muhammad), melainkan orang laki-laki yang Kami berikan wahyu kepadanya di antara penduduk negeri. Tidakkah mereka bepergian di bumi lalu melihat bagaimana kesudahan orang-orang sebelum mereka (yang mendustakan rasul). Da**

12:110

# حَتّٰٓى اِذَا اسْتَا۟يْـَٔسَ الرُّسُلُ وَظَنُّوْٓا اَنَّهُمْ قَدْ كُذِبُوْا جَاۤءَهُمْ نَصْرُنَاۙ فَنُجِّيَ مَنْ نَّشَاۤءُ ۗوَلَا يُرَدُّ بَأْسُنَا عَنِ الْقَوْمِ الْمُجْرِمِيْنَ

*h*att*aa* i*dzaa* istay-asa **al**rrusulu wa*zh*annuu annahum qad ku*dz*ibuu j*aa*-ahum na*sh*run*aa* fanujjiya man nasy*aa*u wal*aa* yuraddu ba/sun*aa* 'ani **a**lqawmi

Sehingga apabila para rasul tidak mempunyai harapan lagi (tentang keimanan kaumnya) dan telah meyakini bahwa mereka telah didustakan, datanglah kepada mereka (para rasul) itu pertolongan Kami, lalu diselamatkan orang yang Kami kehendaki. Dan siksa Kami ti

12:111

# لَقَدْ كَانَ فِيْ قَصَصِهِمْ عِبْرَةٌ لِّاُولِى الْاَلْبَابِۗ مَا كَانَ حَدِيْثًا يُّفْتَرٰى وَلٰكِنْ تَصْدِيْقَ الَّذِيْ بَيْنَ يَدَيْهِ وَتَفْصِيْلَ كُلِّ شَيْءٍ وَّهُدًى وَّرَحْمَةً لِّقَوْمٍ يُّؤْمِنُوْنَ ࣖ

laqad k*aa*na fii qa*sh*a*sh*ihim 'ibratun li-ulii **a**l-alb*aa*bi m*aa* k*aa*na *h*adiitsan yuftar*aa* wal*aa*kin ta*sh*diiqa **al**la*dz*ii bayna yadayhi wataf*sh*iil

Sesungguhnya pada kisah-kisah mereka itu terdapat pengajaran bagi orang-orang yang mempunyai akal. Al Quran itu bukanlah cerita yang dibuat-buat, akan tetapi membenarkan (kitab-kitab) yang sebelumnya dan menjelaskan segala sesuatu, dan sebagai petunjuk dan rahmat bagi kaum yang beriman.

<!--EndFragment-->