---
title: (114) An-Nas - الناس
date: 2021-10-27T04:20:36.764Z
ayat: 114
description: "Jumlah Ayat: 6 / Arti: Manusia"
---
<!--StartFragment-->

114:1

# قُلْ اَعُوْذُ بِرَبِّ النَّاسِۙ

qul a'uu*dz*u birabbi **al**nn*aa*s**i**

Katakanlah, “Aku berlindung kepada Tuhannya manusia,

114:2

# مَلِكِ النَّاسِۙ

maliki **al**nn*aa*s**i**

Raja manusia,

114:3

# اِلٰهِ النَّاسِۙ

il*aa*hi **al**nn*aa*s**i**

sembahan manusia,

114:4

# مِنْ شَرِّ الْوَسْوَاسِ ەۙ الْخَنَّاسِۖ

min syarri **a**lwasw*aa*si **a**lkhann*aa*s**i**

dari kejahatan (bisikan) setan yang bersembunyi,

114:5

# الَّذِيْ يُوَسْوِسُ فِيْ صُدُوْرِ النَّاسِۙ

**al**la*dz*ii yuwaswisu fii *sh*uduuri **al**nn*aa*s**i**

yang membisikkan (kejahatan) ke dalam dada manusia,

114:6

# مِنَ الْجِنَّةِ وَالنَّاسِ ࣖ

mina **a**ljinnati wa**al**nn*aa*s**i**

dari (golongan) jin dan manusia.”

<!--EndFragment-->