---
title: (86) At-Tariq - الطارق
date: 2021-10-27T04:25:03.701Z
ayat: 86
description: "Jumlah Ayat: 17 / Arti: Yang Datang Di Malam Hari"
---
<!--StartFragment-->

86:1

# وَالسَّمَاۤءِ وَالطَّارِقِۙ

wa**al**ssam*aa*-i wa**al***ththaa*riq**i**

Demi langit dan yang datang pada malam hari.

86:2

# وَمَآ اَدْرٰىكَ مَا الطَّارِقُۙ

wam*aa* adr*aa*ka m*aa* **al***ththaa*riq**u**

Dan tahukah kamu apakah yang datang pada malam hari itu?

86:3

# النَّجْمُ الثَّاقِبُۙ

a**l**nnajmu **al**tsts*aa*qib**u**

(yaitu) bintang yang bersinar tajam,

86:4

# اِنْ كُلُّ نَفْسٍ لَّمَّا عَلَيْهَا حَافِظٌۗ

in kullu nafsin lamm*aa* 'alayh*aa* *haa*fi*zh***un**

setiap orang pasti ada penjaganya.

86:5

# فَلْيَنْظُرِ الْاِنْسَانُ مِمَّ خُلِقَ

falyan*zh*uri **a**l-ins*aa*nu mimma khuliq**a**

Maka hendaklah manusia memperhatikan dari apa dia diciptakan.

86:6

# خُلِقَ مِنْ مَّاۤءٍ دَافِقٍۙ

khuliqa min m*aa*-in d*aa*fiq**in**

Dia diciptakan dari air (mani) yang terpancar,

86:7

# يَّخْرُجُ مِنْۢ بَيْنِ الصُّلْبِ وَالتَّرَاۤىِٕبِۗ

yakhruju min bayni **al***shsh*ulbi wa**al**ttar*aa*-ib**i**

yang keluar dari antara tulang punggung (sulbi) dan tulang dada.

86:8

# اِنَّهٗ عَلٰى رَجْعِهٖ لَقَادِرٌۗ

innahu 'al*aa* raj'ihi laq*aa*dir**un**

Sungguh, Allah benar-benar kuasa untuk mengembalikannya (hidup setelah mati).

86:9

# يَوْمَ تُبْلَى السَّرَاۤىِٕرُۙ

yawma tubl*aa* **al**ssar*aa*-ir**u**

Pada hari ditampakkan segala rahasia,

86:10

# فَمَا لَهٗ مِنْ قُوَّةٍ وَّلَا نَاصِرٍۗ

fam*aa* lahu min quwwatin wal*aa* n*aas*ir**in**

maka manusia tidak lagi mempunyai suatu kekuatan dan tidak (pula) ada penolong.

86:11

# وَالسَّمَاۤءِ ذَاتِ الرَّجْعِۙ

wa**al**ssam*aa*-i *dzaa*ti **al**rraj'**i**

Demi langit yang mengandung hujan,

86:12

# وَالْاَرْضِ ذَاتِ الصَّدْعِۙ

wa**a**l-ar*dh*i *dzaa*ti **al***shsh*ad'**i**

dan bumi yang mempunyai tumbuh-tumbuhan,

86:13

# اِنَّهٗ لَقَوْلٌ فَصْلٌۙ

innahu laqawlun fa*sh*l**un**

sungguh, (Al-Qur'an) itu benar-benar firman pemisah (antara yang hak dan yang batil),

86:14

# وَّمَا هُوَ بِالْهَزْلِۗ

wam*aa* huwa bi**a**lhazl**i**

dan (Al-Qur'an) itu bukanlah sendagurauan.

86:15

# اِنَّهُمْ يَكِيْدُوْنَ كَيْدًاۙ

innahum yakiiduuna kayd*aa***n**

Sungguh, mereka (orang kafir) merencanakan tipu daya yang jahat.

86:16

# وَّاَكِيْدُ كَيْدًاۖ

wa-akiidu kayd*aa***n**

Dan Aku pun membuat rencana (tipu daya) yang jitu.

86:17

# فَمَهِّلِ الْكٰفِرِيْنَ اَمْهِلْهُمْ رُوَيْدًا ࣖ

famahhili **a**lk*aa*firiina amhilhum ruwayd*aa***n**

Karena itu berilah penangguhan kepada orang-orang kafir itu. Berilah mereka itu kesempatan untuk sementara waktu.

<!--EndFragment-->