---
title: (32) As-Sajdah - السّجدة
date: 2021-10-27T03:55:33.138Z
ayat: 32
description: "Jumlah Ayat: 30 / Arti: Sajdah"
---
<!--StartFragment-->

32:1

# الۤمّۤ ۗ

alif-l*aa*m-miim

Alif Lam Mim.

32:2

# تَنْزِيْلُ الْكِتٰبِ لَا رَيْبَ فِيْهِ مِنْ رَّبِّ الْعٰلَمِيْنَۗ

tanziilu **a**lkit*aa*bi l*aa* rayba fiihi min rabbi **a**l'*aa*lamiin**a**

Turunnya Al-Qur'an itu tidak ada keraguan padanya, (yaitu) dari Tuhan seluruh alam.

32:3

# اَمْ يَقُوْلُوْنَ افْتَرٰىهُ ۚ بَلْ هُوَ الْحَقُّ مِنْ رَّبِّكَ لِتُنْذِرَ قَوْمًا مَّآ اَتٰىهُمْ مِّنْ نَّذِيْرٍ مِّنْ قَبْلِكَ لَعَلَّهُمْ يَهْتَدُوْنَ

am yaquuluuna iftar*aa*hu bal huwa **a**l*h*aqqu min rabbika litun*dz*ira qawman m*aa* at*aa*hum min na*dz*iirin min qablika la'allahum yahtaduun**a**

Tetapi mengapa mereka (orang kafir) mengatakan, “Dia (Muhammad) telah mengada-adakannya.” Tidak, Al-Qur'an itu kebenaran (yang datang) dari Tuhanmu, agar engkau memberi peringatan kepada kaum yang belum pernah didatangi orang yang memberi peringatan sebel

32:4

# اَللّٰهُ الَّذِيْ خَلَقَ السَّمٰوٰتِ وَالْاَرْضَ وَمَا بَيْنَهُمَا فِيْ سِتَّةِ اَيَّامٍ ثُمَّ اسْتَوٰى عَلَى الْعَرْشِۗ مَا لَكُمْ مِّنْ دُوْنِهٖ مِنْ وَّلِيٍّ وَّلَا شَفِيْعٍۗ اَفَلَا تَتَذَكَّرُوْنَ

**al**l*aa*hu **al**la*dz*ii khalaqa **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*a wam*aa* baynahum*aa* fii sittati ayy*aa*min tsumma istaw*aa* 'al*aa*

Allah yang menciptakan langit dan bumi dan apa yang ada di antara keduanya dalam enam masa, kemudian Dia bersemayam di atas ‘Arsy. Bagimu tidak ada seorang pun penolong maupun pemberi syafaat selain Dia. Maka apakah kamu tidak memperhatikan?







32:5

# يُدَبِّرُ الْاَمْرَ مِنَ السَّمَاۤءِ اِلَى الْاَرْضِ ثُمَّ يَعْرُجُ اِلَيْهِ فِيْ يَوْمٍ كَانَ مِقْدَارُهٗٓ اَلْفَ سَنَةٍ مِّمَّا تَعُدُّوْنَ

yudabbiru **a**l-amra mina **al**ssam*aa*-i il*aa* **a**l-ar*dh*i tsumma ya'ruju ilayhi fii yawmin k*aa*na miqd*aa*ruhu **a**lfa sanatin mimm*aa* ta'udduun**a**

Dia mengatur segala urusan dari langit ke bumi, kemudian (urusan) itu naik kepada-Nya dalam satu hari yang kadarnya (lamanya) adalah seribu tahun menurut perhitunganmu.

32:6

# ذٰلِكَ عٰلِمُ الْغَيْبِ وَالشَّهَادَةِ الْعَزِيْزُ الرَّحِيْمُۙ

*dzaa*lika '*aa*limu **a**lghaybi wa**al**sysyah*aa*dati **a**l'aziizu **al**rra*h*iim**u**

Yang demikian itu, ialah Tuhan yang mengetahui yang gaib dan yang nyata, Yang Mahaperkasa, Maha Penyayang.

32:7

# الَّذِيْٓ اَحْسَنَ كُلَّ شَيْءٍ خَلَقَهٗ وَبَدَاَ خَلْقَ الْاِنْسَانِ مِنْ طِيْنٍ

**al**la*dz*ii a*h*sana kulla syay-in khalaqahu wabada-a khalqa **a**l-ins*aa*ni min *th*iin**in**

Yang memperindah segala sesuatu yang Dia ciptakan dan yang memulai penciptaan manusia dari tanah,

32:8

# ثُمَّ جَعَلَ نَسْلَهٗ مِنْ سُلٰلَةٍ مِّنْ مَّاۤءٍ مَّهِيْنٍ ۚ

tsumma ja'ala naslahu min sul*aa*latin min m*aa*-in mahiin**in**

kemudian Dia menjadikan keturunannya dari sari pati air yang hina (air mani).

32:9

# ثُمَّ سَوّٰىهُ وَنَفَخَ فِيْهِ مِنْ رُّوْحِهٖ وَجَعَلَ لَكُمُ السَّمْعَ وَالْاَبْصَارَ وَالْاَفْـِٕدَةَۗ قَلِيْلًا مَّا تَشْكُرُوْنَ

tsumma saww*aa*hu wanafakha fiihi min ruu*h*ihi waja'ala lakumu **al**ssam'a wa**a**l-ab*shaa*ra wa**a**l-af-idata qaliilan m*aa* tasykuruun**a**

Kemudian Dia menyempurnakannya dan meniupkan roh (ciptaan)-Nya ke dalam (tubuh)nya dan Dia menjadikan pendengaran, penglihatan dan hati bagimu, (tetapi) sedikit sekali kamu bersyukur.

32:10

# وَقَالُوْٓا ءَاِذَا ضَلَلْنَا فِى الْاَرْضِ ءَاِنَّا لَفِيْ خَلْقٍ جَدِيْدٍ ەۗ بَلْ هُمْ بِلِقَاۤءِ رَبِّهِمْ كٰفِرُوْنَ

waq*aa*luu a-i*dzaa* *dh*al*aa*ln*aa* fii **a**l-ar*dh*i a-inn*aa* lafii khalqin jadiidin bal hum biliq*aa*-i rabbihim k*aa*firuun**a**

Dan mereka berkata, “Apakah apabila kami telah lenyap (hancur) di dalam tanah, kami akan berada dalam ciptaan yang baru?” Bahkan mereka mengingkari pertemuan dengan Tuhannya.

32:11

# ۞ قُلْ يَتَوَفّٰىكُمْ مَّلَكُ الْمَوْتِ الَّذِيْ وُكِّلَ بِكُمْ ثُمَّ اِلٰى رَبِّكُمْ تُرْجَعُوْنَ ࣖ

qul yatawaff*aa*kum malaku **a**lmawti **al**la*dz*ii wukkila bikum tsumma il*aa* rabbikum turja'uun**a**

Katakanlah, “Malaikat maut yang diserahi untuk (mencabut nyawa)mu akan mematikan kamu, kemudian kepada Tuhanmu, kamu akan dikembalikan.”

32:12

# وَلَوْ تَرٰىٓ اِذِ الْمُجْرِمُوْنَ نَاكِسُوْا رُءُوْسِهِمْ عِنْدَ رَبِّهِمْۗ رَبَّنَآ اَبْصَرْنَا وَسَمِعْنَا فَارْجِعْنَا نَعْمَلْ صَالِحًا اِنَّا مُوْقِنُوْنَ

walaw tar*aa* i*dz*i **a**lmujrimuuna n*aa*kisuu ruuusihim 'inda rabbihim rabban*aa* ab*sh*arn*aa* wasami'n*aa* fa-arji'n*aa* na'mal *shaa*li*h*an inn*aa* muuqinuun**a**

Dan (alangkah ngerinya), jika sekiranya kamu melihat orang-orang yang berdosa itu menundukkan kepalanya di hadapan Tuhannya, (mereka berkata), “Ya Tuhan kami, kami telah melihat dan mendengar, maka kembalikanlah kami (ke dunia), niscaya kami akan mengerja

32:13

# وَلَوْ شِئْنَا لَاٰتَيْنَا كُلَّ نَفْسٍ هُدٰىهَا وَلٰكِنْ حَقَّ الْقَوْلُ مِنِّيْ لَاَمْلَـَٔنَّ جَهَنَّمَ مِنَ الْجِنَّةِ وَالنَّاسِ اَجْمَعِيْنَ

walaw syi-n*aa* la*aa*tayn*aa* kulla nafsin hud*aa*h*aa* wal*aa*kin *h*aqqa **a**lqawlu minnii la-amla-anna jahannama mina **a**ljinnati wa**al**nn*aa*si ajma'iin**a**

**Dan jika Kami menghendaki niscaya Kami berikan kepada setiap jiwa petunjuk (bagi)nya, tetapi telah ditetapkan perkataan (ketetapan) dari-Ku, “Pasti akan Aku penuhi neraka Jahanam dengan jin dan manusia bersama-sama.**









32:14

# فَذُوْقُوْا بِمَا نَسِيْتُمْ لِقَاۤءَ يَوْمِكُمْ هٰذَاۚ اِنَّا نَسِيْنٰكُمْ وَذُوْقُوْا عَذَابَ الْخُلْدِ بِمَا كُنْتُمْ تَعْمَلُوْنَ

fa*dz*uuquu bim*aa* nasiitum liq*aa*-a yawmikum h*aadzaa* inn*aa* nasiin*aa*kum wa*dz*uuquu 'a*dzaa*ba **a**lkhuldi bim*aa* kuntum ta'maluun**a**

Maka rasakanlah olehmu (azab ini) disebabkan kamu melalaikan pertemuan dengan harimu ini (hari Kiamat), sesungguhnya Kami pun melalaikan kamu dan rasakanlah azab yang kekal, atas apa yang telah kamu kerjakan.”

32:15

# اِنَّمَا يُؤْمِنُ بِاٰيٰتِنَا الَّذِيْنَ اِذَا ذُكِّرُوْا بِهَا خَرُّوْا سُجَّدًا وَّسَبَّحُوْا بِحَمْدِ رَبِّهِمْ وَهُمْ لَا يَسْتَكْبِرُوْنَ ۩

innam*aa* yu/minu bi-*aa*y*aa*tin*aa* **al**la*dz*iina i*dzaa* *dz*ukkiruu bih*aa* kharruu sujjadan wasabba*h*uu bi*h*amdi rabbihim wahum l*aa* yastakbiruun**a**

Orang-orang yang beriman dengan ayat-ayat Kami, hanyalah orang-orang yang apabila diperingatkan dengannya (ayat-ayat Kami), mereka menyungkur sujud dan bertasbih serta memuji Tuhannya, dan mereka tidak menyombongkan diri.

32:16

# تَتَجَافٰى جُنُوْبُهُمْ عَنِ الْمَضَاجِعِ يَدْعُوْنَ رَبَّهُمْ خَوْفًا وَّطَمَعًاۖ وَّمِمَّا رَزَقْنٰهُمْ يُنْفِقُوْنَ

tataj*aa*f*aa* junuubuhum 'ani **a**lma*daa*ji'i yad'uuna rabbahum khawfan wa*th*ama'an wamimm*aa* razaqn*aa*hum yunfiquun**a**

Lambung mereka jauh dari tempat tidurnya, mereka berdoa kepada Tuhannya dengan rasa takut dan penuh harap, dan mereka menginfakkan sebagian dari rezeki yang Kami berikan kepada mereka.

32:17

# فَلَا تَعْلَمُ نَفْسٌ مَّآ اُخْفِيَ لَهُمْ مِّنْ قُرَّةِ اَعْيُنٍۚ جَزَاۤءًۢ بِمَا كَانُوْا يَعْمَلُوْنَ

fal*aa* ta'lamu nafsun m*aa* ukhfiya lahum min qurrati a'yunin jaz*aa*-an bim*aa* k*aa*nuu ya'maluun**a**

Maka tidak seorang pun mengetahui apa yang disembunyikan untuk mereka yaitu (bermacam-macam nikmat) yang menyenangkan hati sebagai balasan terhadap apa yang mereka kerjakan.

32:18

# اَفَمَنْ كَانَ مُؤْمِنًا كَمَنْ كَانَ فَاسِقًاۗ لَا يَسْتَوٗنَ

afaman k*aa*na mu/minan kaman k*aa*na f*aa*siqan l*aa* yastawuun**a**

Maka apakah orang yang beriman seperti orang yang fasik (kafir)? Mereka tidak sama.

32:19

# اَمَّا الَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِ فَلَهُمْ جَنّٰتُ الْمَأْوٰىۖ نُزُلًا ۢبِمَا كَانُوْا يَعْمَلُوْنَ

amm*aa* **al**la*dz*iina *aa*manuu wa'amiluu **al***shshaa*li*haa*ti falahum jann*aa*tu **a**lma/w*aa* nuzulan bim*aa* k*aa*nuu ya'maluun**a**

Adapun orang-orang yang beriman dan mengerjakan kebajikan, maka mereka akan mendapat surga-surga tempat kediaman, sebagai pahala atas apa yang telah mereka kerjakan.

32:20

# وَاَمَّا الَّذِيْنَ فَسَقُوْا فَمَأْوٰىهُمُ النَّارُ كُلَّمَآ اَرَادُوْٓا اَنْ يَّخْرُجُوْا مِنْهَآ اُعِيْدُوْا فِيْهَا وَقِيْلَ لَهُمْ ذُوْقُوْا عَذَابَ النَّارِ الَّذِيْ كُنْتُمْ بِهٖ تُكَذِّبُوْنَ

wa-amm*aa* **al**la*dz*iina fasaquu fama/w*aa*humu **al**nn*aa*ru kullam*aa* ar*aa*duu an yakhrujuu minh*aa* u'iiduu fiih*aa* waqiila lahum *dz*uuquu 'a*dzaa*ba **al**

**Dan adapun orang-orang yang fasik (kafir), maka tempat kediaman mereka adalah neraka. Setiap kali mereka hendak keluar darinya, mereka dikembalikan (lagi) ke dalamnya dan dikatakan kepada mereka, “Rasakanlah azab neraka yang dahulu kamu dustakan.”**









32:21

# وَلَنُذِيْقَنَّهُمْ مِّنَ الْعَذَابِ الْاَدْنٰى دُوْنَ الْعَذَابِ الْاَكْبَرِ لَعَلَّهُمْ يَرْجِعُوْنَ

walanu*dz*iiqannahum mina **a**l'a*dzaa*bi **a**l-adn*aa* duuna **a**l'a*dzaa*bi **a**l-akbari la'allahum yarji'uun**a**

Dan pasti Kami timpakan kepada mereka sebagian siksa yang dekat (di dunia) sebelum azab yang lebih besar (di akhirat); agar mereka kembali (ke jalan yang benar).

32:22

# وَمَنْ اَظْلَمُ مِمَّنْ ذُكِّرَ بِاٰيٰتِ رَبِّهٖ ثُمَّ اَعْرَضَ عَنْهَا ۗاِنَّا مِنَ الْمُجْرِمِيْنَ مُنْتَقِمُوْنَ ࣖ

waman a*zh*lamu mimman *dz*ukkira bi-*aa*y*aa*ti rabbihi tsumma a'ra*dh*a 'anh*aa* inn*aa* mina **a**lmujrimiina muntaqimuun**a**

Dan siapakah yang lebih zalim daripada orang yang telah diperingatkan dengan ayat-ayat Tuhannya, kemudian dia berpaling darinya? Sungguh, Kami akan memberikan balasan kepada orang-orang yang berdosa.

32:23

# وَلَقَدْ اٰتَيْنَا مُوْسَى الْكِتٰبَ فَلَا تَكُنْ فِيْ مِرْيَةٍ مِّنْ لِّقَاۤىِٕهٖ وَجَعَلْنٰهُ هُدًى لِّبَنِيْٓ اِسْرَاۤءِيْلَۚ

walaqad *aa*tayn*aa* muus*aa* **a**lkit*aa*ba fal*aa* takun fii miryatin min liq*aa*-ihi waja'aln*aa*hu hudan libanii isr*aa*-iil**a**

Dan sungguh, telah Kami anugerahkan Kitab (Taurat) kepada Musa, maka janganlah engkau (Muhammad) ragu-ragu menerimanya (Al-Qur'an) dan Kami jadikan Kitab (Taurat) itu petunjuk bagi Bani Israil.

32:24

# وَجَعَلْنَا مِنْهُمْ اَىِٕمَّةً يَّهْدُوْنَ بِاَمْرِنَا لَمَّا صَبَرُوْاۗ وَكَانُوْا بِاٰيٰتِنَا يُوْقِنُوْنَ

waja'aln*aa* minhum a-immatan yahduuna bi-amrin*aa* lamm*aa* *sh*abaruu wak*aa*nuu bi-*aa*y*aa*tin*aa* yuuqinuun**a**

Dan Kami jadikan di antara mereka itu pemimpin-pemimpin yang memberi petunjuk dengan perintah Kami selama mereka sabar. Mereka meyakini ayat-ayat Kami.

32:25

# اِنَّ رَبَّكَ هُوَ يَفْصِلُ بَيْنَهُمْ يَوْمَ الْقِيٰمَةِ فِيْمَا كَانُوْا فِيْهِ يَخْتَلِفُوْنَ

inna rabbaka huwa yaf*sh*ilu baynahum yawma **a**lqiy*aa*mati fiim*aa* k*aa*nuu fiihi yakhtalifuun**a**

Sungguh Tuhanmu, Dia yang memberikan keputusan di antara mereka pada hari Kiamat tentang apa yang dahulu mereka perselisihkan padanya.

32:26

# اَوَلَمْ يَهْدِ لَهُمْ كَمْ اَهْلَكْنَا مِنْ قَبْلِهِمْ مِّنَ الْقُرُوْنِ يَمْشُوْنَ فِيْ مَسٰكِنِهِمْ ۗاِنَّ فِيْ ذٰلِكَ لَاٰيٰتٍۗ اَفَلَا يَسْمَعُوْنَ

awa lam yahdi lahum kam ahlakn*aa* min qablihim mina **a**lquruuni yamsyuuna fii mas*aa*kinihim inna fii *dzaa*lika la*aa*y*aa*tin afal*aa* yasma'uun**a**

Dan tidakkah menjadi petunjuk bagi mereka, betapa banyak umat-umat sebelum mereka yang telah Kami binasakan, sedangkan mereka sendiri berjalan di tempat-tempat kediaman mereka itu. Sungguh, pada yang demikian itu terdapat tanda-tanda (kekuasaan Allah). Ap

32:27

# اَوَلَمْ يَرَوْا اَنَّا نَسُوْقُ الْمَاۤءَ اِلَى الْاَرْضِ الْجُرُزِ فَنُخْرِجُ بِهٖ زَرْعًا تَأْكُلُ مِنْهُ اَنْعَامُهُمْ وَاَنْفُسُهُمْۗ اَفَلَا يُبْصِرُوْنَ

awa lam yaraw ann*aa* nasuuqu **a**lm*aa*-a il*aa* **a**l-ar*dh*i **a**ljuruzi fanukhriju bihi zar'an ta-kulu minhu an'*aa*muhum wa-anfusuhum afal*aa* yub*sh*iruun**a**

Dan tidakkah mereka memperhatikan, bahwa Kami mengarahkan (awan yang mengandung) air ke bumi yang tandus, lalu Kami tumbuhkan (dengan air hujan itu) tanam-tanaman sehingga hewan-hewan ternak mereka dan mereka sendiri dapat makan darinya. Maka mengapa mere

32:28

# وَيَقُوْلُوْنَ مَتٰى هٰذَا الْفَتْحُ اِنْ كُنْتُمْ صٰدِقِيْنَ

wayaquuluuna mat*aa* h*aadzaa* **a**lfat*h*u in kuntum *shaa*diqiin**a**

Dan mereka bertanya, “Kapankah kemenangan itu (datang) jika engkau orang yang benar?”

32:29

# قُلْ يَوْمَ الْفَتْحِ لَا يَنْفَعُ الَّذِيْنَ كَفَرُوْٓا اِيْمَانُهُمْ وَلَا هُمْ يُنْظَرُوْنَ

qul yawma **a**lfat*h*i l*aa* yanfa'u **al**la*dz*iina kafaruu iim*aa*nuhum wal*aa* hum yun*zh*aruun**a**

Katakanlah, “Pada hari kemenangan itu, tidak berguna lagi bagi orang-orang kafir keimanan mereka dan mereka tidak diberi penangguhan.”

32:30

# فَاَعْرِضْ عَنْهُمْ وَانْتَظِرْ اِنَّهُمْ مُّنْتَظِرُوْنَ ࣖ

fa-a'ri*dh* 'anhum wa**i**nta*zh*ir innahum munta*zh*iruun**a**

Maka berpalinglah engkau dari mereka dan tunggulah, sesungguhnya mereka (juga) menunggu.

<!--EndFragment-->