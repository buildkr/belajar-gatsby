---
title: (23) Al-Mu'minun - المؤمنون
date: 2021-10-27T03:50:55.175Z
ayat: 23
description: "Jumlah Ayat: 118 / Arti: Orang-Orang Mukmin"
---
<!--StartFragment-->

23:1

# قَدْ اَفْلَحَ الْمُؤْمِنُوْنَ ۙ

qad afla*h*a **a**lmu/minuun**a**

Sungguh beruntung orang-orang yang beriman,

23:2

# الَّذِيْنَ هُمْ فِيْ صَلٰو تِهِمْ خَاشِعُوْنَ

**al**la*dz*iina hum fii *sh*al*aa*tihim kh*aa*syi'uun**a**

(yaitu) orang yang khusyuk dalam salatnya,

23:3

# وَالَّذِيْنَ هُمْ عَنِ اللَّغْوِ مُعْرِضُوْنَ ۙ

wa**a**lla*dz*iina hum 'ani **al**laghwi mu'ri*dh*uun**a**

dan orang yang menjauhkan diri dari (perbuatan dan perkataan) yang tidak berguna,

23:4

# وَالَّذِيْنَ هُمْ لِلزَّكٰوةِ فَاعِلُوْنَ ۙ

wa**a**lla*dz*iina hum li**l**zzak*aa*ti f*aa*'iluun**a**

dan orang yang menunaikan zakat,

23:5

# وَالَّذِيْنَ هُمْ لِفُرُوْجِهِمْ حٰفِظُوْنَ ۙ

wa**a**lla*dz*iina hum lifuruujihim *haa*fi*zh*uun**a**

dan orang yang memelihara kemaluannya,

23:6

# اِلَّا عَلٰٓى اَزْوَاجِهِمْ اَوْ مَا مَلَكَتْ اَيْمَانُهُمْ فَاِنَّهُمْ غَيْرُ مَلُوْمِيْنَۚ

ill*aa* 'al*aa* azw*aa*jihim aw m*aa* malakat aym*aa*nuhum fa-innahum ghayru maluumiin**a**

kecuali terhadap istri-istri mereka atau hamba sahaya yang mereka miliki; maka sesungguhnya mereka tidak tercela.

23:7

# فَمَنِ ابْتَغٰى وَرَاۤءَ ذٰلِكَ فَاُولٰۤىِٕكَ هُمُ الْعٰدُوْنَ ۚ

famani ibtagh*aa* war*aa*-a *dzaa*lika faul*aa*-ika humu **a**l'*aa*duun**a**

Tetapi barang siapa mencari di balik itu (zina, dan sebagainya), maka mereka itulah orang-orang yang melampaui batas.

23:8

# وَالَّذِيْنَ هُمْ لِاَمٰنٰتِهِمْ وَعَهْدِهِمْ رَاعُوْنَ ۙ

wa**a**lla*dz*iina hum li-am*aa*n*aa*tihim wa'ahdihim r*aa*'uun**a**

Dan (sungguh beruntung) orang yang memelihara amanat-amanat dan janjinya,

23:9

# وَالَّذِيْنَ هُمْ عَلٰى صَلَوٰتِهِمْ يُحَافِظُوْنَ ۘ

wa**a**lla*dz*iina hum 'al*aa* *sh*alaw*aa*tihim yu*haa*fi*zh*uun**a**

serta orang yang memelihara salatnya.

23:10

# اُولٰۤىِٕكَ هُمُ الْوَارِثُوْنَ ۙ

ul*aa*-ika humu **a**lw*aa*ritsuun**a**

Mereka itulah orang yang akan mewarisi,

23:11

# الَّذِيْنَ يَرِثُوْنَ الْفِرْدَوْسَۗ هُمْ فِيْهَا خٰلِدُوْنَ

**al**la*dz*iina yaritsuuna **a**lfirdawsa hum fiih*aa* kh*aa*liduun**a**

(yakni) yang akan mewarisi (surga) Firdaus. Mereka kekal di dalamnya.

23:12

# وَلَقَدْ خَلَقْنَا الْاِنْسَانَ مِنْ سُلٰلَةٍ مِّنْ طِيْنٍ ۚ

walaqad khalaqn*aa* **a**l-ins*aa*na min sul*aa*latin min *th*iin**in**

Dan sungguh, Kami telah menciptakan manusia dari saripati (berasal) dari tanah.

23:13

# ثُمَّ جَعَلْنٰهُ نُطْفَةً فِيْ قَرَارٍ مَّكِيْنٍ ۖ

tsumma ja'aln*aa*hu nu*th*fatan fii qar*aa*rin makiin**in**

Kemudian Kami menjadikannya air mani (yang disimpan) dalam tempat yang kokoh (rahim).

23:14

# ثُمَّ خَلَقْنَا النُّطْفَةَ عَلَقَةً فَخَلَقْنَا الْعَلَقَةَ مُضْغَةً فَخَلَقْنَا الْمُضْغَةَ عِظٰمًا فَكَسَوْنَا الْعِظٰمَ لَحْمًا ثُمَّ اَنْشَأْنٰهُ خَلْقًا اٰخَرَۗ فَتَبَارَكَ اللّٰهُ اَحْسَنُ الْخَالِقِيْنَۗ

tsumma khalaqn*aa* **al**nnu*th*fata 'alaqatan fakhalaqn*aa* **a**l'alaqata mu*dh*ghatan fakhalaqn*aa* **a**lmu*dh*ghata 'i*zhaa*man fakasawn*aa* **a**l'i*zhaa*

*Kemudian, air mani itu Kami jadikan sesuatu yang melekat, lalu sesuatu yang melekat itu Kami jadikan segumpal daging, dan segumpal daging itu Kami jadikan tulang belulang, lalu tulang belulang itu Kami bungkus dengan daging. Kemudian, Kami menjadikannya m*









23:15

# ثُمَّ اِنَّكُمْ بَعْدَ ذٰلِكَ لَمَيِّتُوْنَ ۗ

tsumma innakum ba'da *dzaa*lika lamayyituun**a**

Kemudian setelah itu, sesungguhnya kamu pasti mati.

23:16

# ثُمَّ اِنَّكُمْ يَوْمَ الْقِيٰمَةِ تُبْعَثُوْنَ

tsumma innakum yawma **a**lqiy*aa*mati tub'atsuun**a**

Kemudian, sesungguhnya kamu akan dibangkitkan (dari kuburmu) pada hari Kiamat.

23:17

# وَلَقَدْ خَلَقْنَا فَوْقَكُمْ سَبْعَ طَرَاۤىِٕقَۖ وَمَا كُنَّا عَنِ الْخَلْقِ غٰفِلِيْنَ

walaqad khalaqn*aa* fawqakum sab'a *th*ar*aa*-iqa wam*aa* kunn*aa* 'ani **a**lkhalqi gh*aa*filiin**a**

Dan sungguh, Kami telah menciptakan tujuh (lapis) langit di atas kamu, dan Kami tidaklah lengah terhadap ciptaan (Kami).

23:18

# وَاَنْزَلْنَا مِنَ السَّمَاۤءِ مَاۤءًۢ بِقَدَرٍ فَاَسْكَنّٰهُ فِى الْاَرْضِۖ وَاِنَّا عَلٰى ذَهَابٍۢ بِهٖ لَقٰدِرُوْنَ ۚ

wa-anzaln*aa* mina **al**ssam*aa*-i m*aa*-an biqadarin fa-askann*aa*hu fii **a**l-ar*dh*i wa-inn*aa* 'al*aa* *dz*ah*aa*bin bihi laq*aa*diruun**a**

Dan Kami turunkan air dari langit dengan suatu ukuran; lalu Kami jadikan air itu menetap di bumi, dan pasti Kami berkuasa melenyapkannya.

23:19

# فَاَنْشَأْنَا لَكُمْ بِهٖ جَنّٰتٍ مِّنْ نَّخِيْلٍ وَّاَعْنَابٍۘ لَكُمْ فِيْهَا فَوَاكِهُ كَثِيْرَةٌ وَّمِنْهَا تَأْكُلُوْنَ ۙ

fa-ansya/n*aa* lakum bihi jann*aa*tin min nakhiilin wa-a'n*aa*bin lakum fiih*aa* faw*aa*kihu katsiiratun waminh*aa* ta/kuluun**a**

Lalu dengan (air) itu, Kami tumbuhkan untukmu kebun-kebun kurma dan anggur; di sana kamu memperoleh buah-buahan yang banyak dan sebagian dari (buah-buahan) itu kamu makan,

23:20

# وَشَجَرَةً تَخْرُجُ مِنْ طُوْرِ سَيْنَاۤءَ تَنْۢبُتُ بِالدُّهْنِ وَصِبْغٍ لِّلْاٰكِلِيْنَ

wasyajaratan takhruju min *th*uuri sayn*aa*-a tanbutu bi**al**dduhni wa*sh*ibghin lil*aa*kiliin**a**

dan (Kami tumbuhkan) pohon (zaitun) yang tumbuh dari gunung Sinai, yang menghasilkan minyak, dan bahan pembangkit selera bagi orang-orang yang makan.

23:21

# وَاِنَّ لَكُمْ فِى الْاَنْعَامِ لَعِبْرَةًۗ نُسْقِيْكُمْ مِّمَّا فِيْ بُطُوْنِهَا وَلَكُمْ فِيْهَا مَنَافِعُ كَثِيْرَةٌ وَّمِنْهَا تَأْكُلُوْنَ ۙ

wa-inna lakum fii **a**l-an'*aa*mi la'ibratan nusqiikum mimm*aa* fii bu*th*uunih*aa* walakum fiih*aa* man*aa*fi'u katsiiratun waminh*aa* ta/kuluun**a**

Dan sesungguhnya pada hewan-hewan ternak terdapat suatu pelajaran bagimu. Kami memberi minum kamu dari (air susu) yang ada dalam perutnya, dan padanya juga terdapat banyak manfaat untukmu, dan sebagian darinya kamu makan,

23:22

# وَعَلَيْهَا وَعَلَى الْفُلْكِ تُحْمَلُوْنَ ࣖ

wa'alayh*aa* wa'al*aa* **a**lfulki tu*h*maluun**a**

di atasnya (hewan-hewan ternak) dan di atas kapal-kapal kamu diangkut.

23:23

# وَلَقَدْ اَرْسَلْنَا نُوْحًا اِلٰى قَوْمِهٖ فَقَالَ يٰقَوْمِ اعْبُدُوا اللّٰهَ مَا لَكُمْ مِّنْ اِلٰهٍ غَيْرُهٗۗ اَفَلَا تَتَّقُوْنَ

walaqad arsaln*aa* nuu*h*an il*aa* qawmihi faq*aa*la y*aa* qawmi u'buduu **al**l*aa*ha m*aa* lakum min il*aa*hin ghayruhu afal*aa* tattaquun**a**

Dan sungguh, Kami telah mengutus Nuh kepada kaumnya, lalu dia berkata, “Wahai kaumku! Sembahlah Allah, (karena) tidak ada tuhan (yang berhak disembah) bagimu selain Dia. Maka mengapa kamu tidak bertakwa (kepada-Nya)?”

23:24

# فَقَالَ الْمَلَؤُا الَّذِيْنَ كَفَرُوْا مِنْ قَوْمِهٖ مَا هٰذَآ اِلَّا بَشَرٌ مِّثْلُكُمْۙ يُرِيْدُ اَنْ يَّتَفَضَّلَ عَلَيْكُمْۗ وَلَوْ شَاۤءَ اللّٰهُ لَاَنْزَلَ مَلٰۤىِٕكَةً ۖمَّا سَمِعْنَا بِهٰذَا فِيْٓ اٰبَاۤىِٕنَا الْاَوَّلِيْنَ ۚ

faq*aa*la **a**lmalau **al**la*dz*iina kafaruu min qawmihi m*aa* h*aadzaa* ill*aa* basyarun mitslukum yuriidu an yatafa*dhdh*ala 'alaykum walaw sy*aa*-a **al**l*aa*hu la-anzala

Maka berkatalah para pemuka orang kafir dari kaumnya, “Orang ini tidak lain hanyalah manusia seperti kamu, yang ingin menjadi orang yang lebih mulia daripada kamu. Dan seandainya Allah menghendaki, tentu Dia mengutus malaikat. Belum pernah kami mendengar

23:25

# اِنْ هُوَ اِلَّا رَجُلٌۢ بِهٖ جِنَّةٌ فَتَرَبَّصُوْا بِهٖ حَتّٰى حِيْنٍ

in huwa ill*aa* rajulun bihi jinnatun fatarabba*sh*uu bihi *h*att*aa* *h*iin**in**

Dia hanyalah seorang laki-laki yang gila, maka tunggulah (sabarlah) terhadapnya sampai waktu yang ditentukan.”

23:26

# قَالَ رَبِّ انْصُرْنِيْ بِمَا كَذَّبُوْنِ

q*aa*la rabbi un*sh*urnii bim*aa* ka*dzdz*abuun**i**

Dia (Nuh) berdoa, “Ya Tuhanku, tolonglah aku karena mereka mendustakan aku.”

23:27

# فَاَوْحَيْنَآ اِلَيْهِ اَنِ اصْنَعِ الْفُلْكَ بِاَعْيُنِنَا وَوَحْيِنَا فَاِذَا جَاۤءَ اَمْرُنَا وَفَارَ التَّنُّوْرُۙ فَاسْلُكْ فِيْهَا مِنْ كُلٍّ زَوْجَيْنِ اثْنَيْنِ وَاَهْلَكَ اِلَّا مَنْ سَبَقَ عَلَيْهِ الْقَوْلُ مِنْهُمْۚ وَلَا تُخَاطِبْنِيْ فِى ال

fa-aw*h*ayn*aa* ilayhi ani i*sh*na'i **a**lfulka bi-a'yunin*aa* wawa*h*yin*aa* fa-i*dzaa* j*aa*-a amrun*aa* waf*aa*ra **al**ttannuuru fa**u**sluk fiih*aa* min ku

Lalu Kami wahyukan kepadanya, “Buatlah kapal di bawah pengawasan dan petunjuk Kami, maka apabila perintah Kami datang dan tanur (dapur) telah memancarkan air, maka masukkanlah ke dalam (kapal) itu sepasang-sepasang dari setiap jenis, juga keluargamu, kecu

23:28

# فَاِذَا اسْتَوَيْتَ اَنْتَ وَمَنْ مَّعَكَ عَلَى الْفُلْكِ فَقُلِ الْحَمْدُ لِلّٰهِ الَّذِيْ نَجّٰىنَا مِنَ الْقَوْمِ الظّٰلِمِيْنَ

fa-i*dzaa* istawayta anta waman ma'aka 'al*aa* **a**lfulki faquli **a**l*h*amdu lill*aa*hi **al**la*dz*ii najj*aa*n*aa* mina **a**lqawmi **al***zhzhaa*

*Dan apabila engkau dan orang-orang yang bersamamu telah berada di atas kapal, maka ucapkanlah, “Segala puji bagi Allah yang telah menyelamatkan kami dari orang-orang yang zalim.”*









23:29

# وَقُلْ رَّبِّ اَنْزِلْنِيْ مُنْزَلًا مُّبٰرَكًا وَّاَنْتَ خَيْرُ الْمُنْزِلِيْنَ

waqul rabbi anzilnii munzalan mub*aa*rakan wa-anta khayru **a**lmunziliin**a**

Dan berdoalah, “Ya Tuhanku, tempatkanlah aku pada tempat yang diberkahi, dan Engkau adalah sebaik-baik pemberi tempat.”

23:30

# اِنَّ فِيْ ذٰلِكَ لَاٰيٰتٍ وَّاِنْ كُنَّا لَمُبْتَلِيْنَ

inna fii *dzaa*lika la*aa*y*aa*tin wa-in kunn*aa* lamubtaliin**a**

Sungguh, pada (kejadian) itu benar-benar terdapat tanda-tanda (kebesaran Allah); dan sesungguhnya Kami benar-benar menimpakan siksaan (kepada kaum Nuh itu).

23:31

# ثُمَّ اَنْشَأْنَا مِنْۢ بَعْدِهِمْ قَرْنًا اٰخَرِيْنَ ۚ

tsumma ansya/n*aa* min ba'dihim qarnan *aa*khariin**a**

Kemudian setelah mereka, Kami ciptakan umat yang lain (kaum ‘Ad).

23:32

# فَاَرْسَلْنَا فِيْهِمْ رَسُوْلًا مِّنْهُمْ اَنِ اعْبُدُوا اللّٰهَ مَا لَكُمْ مِّنْ اِلٰهٍ غَيْرُهٗۗ اَفَلَا تَتَّقُوْنَ ࣖ

fa-arsaln*aa* fiihim rasuulan minhum ani u'buduu **al**l*aa*ha m*aa* lakum min il*aa*hin ghayruhu afal*aa* tattaquun**a**

Lalu Kami utus kepada mereka, seorang rasul dari kalangan mereka sendiri (yang berkata), “Sembahlah Allah! Tidak ada tuhan (yang berhak disembah) bagimu selain Dia. Maka mengapa kamu tidak bertakwa (kepada-Nya)?”

23:33

# وَقَالَ الْمَلَاُ مِنْ قَوْمِهِ الَّذِيْنَ كَفَرُوْا وَكَذَّبُوْا بِلِقَاۤءِ الْاٰخِرَةِ وَاَتْرَفْنٰهُمْ فِى الْحَيٰوةِ الدُّنْيَاۙ مَا هٰذَآ اِلَّا بَشَرٌ مِّثْلُكُمْۙ يَأْكُلُ مِمَّا تَأْكُلُوْنَ مِنْهُ وَيَشْرَبُ مِمَّا تَشْرَبُوْنَ

waq*aa*la **a**lmalau min qawmihi **al**la*dz*iina kafaruu waka*dzdz*abuu biliq*aa*-i **a**l-*aa*khirati wa-atrafn*aa*hum fii **a**l*h*ay*aa*ti **al**

Dan berkatalah para pemuka orang kafir dari kaumnya dan yang mendustakan pertemuan hari akhirat serta mereka yang telah Kami beri kemewahan dan kesenangan dalam kehidupan di dunia, “(Orang) ini tidak lain hanyalah manusia seperti kamu, dia makan apa yang

23:34

# وَلَىِٕنْ اَطَعْتُمْ بَشَرًا مِّثْلَكُمْ اِنَّكُمْ اِذًا لَّخٰسِرُوْنَ ۙ

wala-in a*th*a'tum basyaran mitslakum innakum i*dz*an lakh*aa*siruun**a**

Dan sungguh, jika kamu menaati manusia seperti kamu, niscaya kamu pasti rugi.

23:35

# اَيَعِدُكُمْ اَنَّكُمْ اِذَا مِتُّمْ وَكُنْتُمْ تُرَابًا وَّعِظَامًا اَنَّكُمْ مُّخْرَجُوْنَ ۖ

aya'idukum annakum i*dzaa* mittum wakuntum tur*aa*ban wa'i*zhaa*man annakum mukhrajuun**a**

Adakah dia menjanjikan kepada kamu, bahwa apabila kamu telah mati dan menjadi tanah dan tulang belulang, sesungguhnya kamu akan dikeluarkan (dari kuburmu)?

23:36

# ۞ هَيْهَاتَ هَيْهَاتَ لِمَا تُوْعَدُوْنَ ۖ

hayh*aa*ta hayh*aa*ta lim*aa* tuu'aduun**a**

Jauh! Jauh sekali (dari kebenaran) apa yang diancamkan kepada kamu,

23:37

# اِنْ هِيَ اِلَّا حَيَاتُنَا الدُّنْيَا نَمُوْتُ وَنَحْيَا وَمَا نَحْنُ بِمَبْعُوْثِيْنَ ۖ

in hiya ill*aa* *h*ay*aa*tun*aa* **al**dduny*aa* namuutu wana*h*y*aa* wam*aa* na*h*nu bimab'uutsiin**a**

(kehidupan itu) tidak lain hanyalah kehidupan kita di dunia ini, (di sanalah) kita mati dan hidup dan tidak akan dibangkitkan (lagi),

23:38

# اِنْ هُوَ اِلَّا رَجُلُ ِۨافْتَرٰى عَلَى اللّٰهِ كَذِبًا وَّمَا نَحْنُ لَهٗ بِمُؤْمِنِيْنَ

in huwa ill*aa* rajulun iftar*aa* 'al*aa* **al**l*aa*hi ka*dz*iban wam*aa* na*h*nu lahu bimu/miniin**a**

Dia tidak lain hanyalah seorang laki-laki yang mengada-adakan kebohongan terhadap Allah, dan kita tidak akan mempercayainya.

23:39

# قَالَ رَبِّ انْصُرْنِيْ بِمَا كَذَّبُوْنِ

q*aa*la rabbi un*sh*urnii bim*aa* ka*dzdz*abuun**i**

Dia (Hud) berdoa, “Ya Tuhanku, tolonglah aku karena mereka mendustakan aku.”

23:40

# قَالَ عَمَّا قَلِيْلٍ لَّيُصْبِحُنَّ نٰدِمِيْنَ ۚ

q*aa*la 'amm*aa* qaliilin layu*sh*bi*h*unna n*aa*dimiin**a**

Dia (Allah) berfirman, “Tidak lama lagi mereka pasti akan menyesal.”

23:41

# فَاَخَذَتْهُمُ الصَّيْحَةُ بِالْحَقِّ فَجَعَلْنٰهُمْ غُثَاۤءًۚ فَبُعْدًا لِّلْقَوْمِ الظّٰلِمِيْنَ

fa-akha*dz*at-humu **al***shsh*ay*h*atu bi**a**l*h*aqqi faja'aln*aa*hum ghuts*aa*-an fabu'dan lilqawmi **al***zhzhaa*limiin**a**

Lalu mereka benar-benar dimusnahkan oleh suara yang mengguntur, dan Kami jadikan mereka (seperti) sampah yang dibawa banjir. Maka binasalah bagi orang-orang yang zalim.

23:42

# ثُمَّ اَنْشَأْنَا مِنْۢ بَعْدِهِمْ قُرُوْنًا اٰخَرِيْنَ ۗ

tsumma ansya/n*aa* min ba'dihim quruunan *aa*khariin**a**

Kemudian setelah mereka Kami ciptakan umat-umat yang lain.

23:43

# مَا تَسْبِقُ مِنْ اُمَّةٍ اَجَلَهَا وَمَا يَسْتَأْخِرُوْنَ ۗ

m*aa* tasbiqu min ummatin ajalah*aa* wam*aa* yasta/khiruun**a**

Tidak ada satu umat pun yang dapat menyegerakan ajalnya, dan tidak (pula) menangguhkannya.

23:44

# ثُمَّ اَرْسَلْنَا رُسُلَنَا تَتْرَاۗ كُلَّمَا جَاۤءَ اُمَّةً رَّسُوْلُهَا كَذَّبُوْهُ فَاَتْبَعْنَا بَعْضَهُمْ بَعْضًا وَّجَعَلْنٰهُمْ اَحَادِيْثَۚ فَبُعْدًا لِّقَوْمٍ لَّا يُؤْمِنُوْنَ

tsumma arsaln*aa* rusulan*aa* tatr*aa* kulla m*aa* j*aa*-a ummatan rasuuluh*aa* ka*dzdz*abuuhu fa-atba'n*aa* ba'*dh*ahum ba'*dh*an waja'aln*aa*hum a*haa*diitsa fabu'dan liqawmin l*aa* yu/min

Kemudian, Kami utus rasul-rasul Kami berturut-turut. Setiap kali seorang rasul datang kepada suatu umat, mereka mendustakannya, maka Kami silihgantikan sebagian mereka dengan sebagian yang lain (dalam kebinasaan). Dan Kami jadikan mereka bahan cerita (bag

23:45

# ثُمَّ اَرْسَلْنَا مُوْسٰى وَاَخَاهُ هٰرُوْنَ ەۙ بِاٰيٰتِنَا وَسُلْطٰنٍ مُّبِيْنٍۙ

tsumma arsaln*aa* muus*aa* wa-akh*aa*hu h*aa*ruuna bi-*aa*y*aa*tin*aa* wasul*thaa*nin mubiin**in**

Kemudian Kami utus Musa dan saudaranya Harun dengan membawa tanda-tanda (kebesaran) Kami, dan bukti yang nyata,

23:46

# اِلٰى فِرْعَوْنَ وَمَلَا۟ىِٕهٖ فَاسْتَكْبَرُوْا وَكَانُوْا قَوْمًا عَالِيْنَ ۚ

il*aa* fir'awna wamala-ihi fa**i**stakbaruu wak*aa*nuu qawman '*aa*liin**a**

kepada Fir‘aun dan para pemuka kaumnya, tetapi mereka angkuh dan mereka memang kaum yang sombong.

23:47

# فَقَالُوْٓا اَنُؤْمِنُ لِبَشَرَيْنِ مِثْلِنَا وَقَوْمُهُمَا لَنَا عٰبِدُوْنَ ۚ

faq*aa*luu anu/minu libasyarayni mitslin*aa* waqawmuhum*aa* lan*aa* '*aa*biduun**a**

Maka mereka berkata, “Apakah (pantas) kita percaya kepada dua orang manusia seperti kita, padahal kaum mereka (Bani Israil) adalah orang-orang yang menghambakan diri kepada kita?”

23:48

# فَكَذَّبُوْهُمَا فَكَانُوْا مِنَ الْمُهْلَكِيْنَ

faka*dzdz*abuuhum*aa* fak*aa*nuu mina **a**lmuhlakiin**a**

Maka mereka mendustakan keduanya, sebab itu mereka termasuk orang yang dibinasakan.

23:49

# وَلَقَدْ اٰتَيْنَا مُوْسَى الْكِتٰبَ لَعَلَّهُمْ يَهْتَدُوْنَ

walaqad *aa*tayn*aa* muus*aa* **a**lkit*aa*ba la'allahum yahtaduun**a**

Dan sungguh, telah Kami anugerahi kepada Musa Kitab (Taurat), agar mereka (Bani Israil) mendapat petunjuk.

23:50

# وَجَعَلْنَا ابْنَ مَرْيَمَ وَاُمَّهٗٓ اٰيَةً وَّاٰوَيْنٰهُمَآ اِلٰى رَبْوَةٍ ذَاتِ قَرَارٍ وَّمَعِيْنٍ ࣖ

waja'aln*aa* ibna maryama waummahu *aa*yatan wa*aa*wayn*aa*hum*aa* il*aa* rabwatin *dzaa*ti qar*aa*rin wama'iin**in**

Dan telah Kami jadikan (Isa) putra Maryam bersama ibunya sebagai suatu bukti yang nyata bagi (kebesaran Kami), dan Kami melindungi mereka di sebuah dataran tinggi, (tempat yang tenang, rindang dan banyak buah-buahan) dengan mata air yang mengalir.

23:51

# يٰٓاَيُّهَا الرُّسُلُ كُلُوْا مِنَ الطَّيِّبٰتِ وَاعْمَلُوْا صَالِحًاۗ اِنِّيْ بِمَا تَعْمَلُوْنَ عَلِيْمٌ ۗ

y*aa* ayyuh*aa* **al**rrusulu kuluu mina **al***ththh*ayyib*aa*ti wa**i**'maluu *shaa*li*h*an innii bim*aa* ta'maluuna 'aliim**un**

Allah berfirman, “Wahai para rasul! Makanlah dari (makanan) yang baik-baik, dan kerjakanlah kebajikan. Sungguh, Aku Maha Mengetahui apa yang kamu kerjakan.

23:52

# وَاِنَّ هٰذِهٖٓ اُمَّتُكُمْ اُمَّةً وَّاحِدَةً وَّاَنَا۠ رَبُّكُمْ فَاتَّقُوْنِ

wa-inna h*aadz*ihi ummatukum ummatan w*aah*idatan wa-an*aa* rabbukum fa**i**ttaquun**i**

Dan sungguh, (agama tauhid) inilah agama kamu, agama yang satu dan Aku adalah Tuhanmu, maka bertakwalah kepada-Ku.”

23:53

# فَتَقَطَّعُوْٓا اَمْرَهُمْ بَيْنَهُمْ زُبُرًاۗ كُلُّ حِزْبٍۢ بِمَا لَدَيْهِمْ فَرِحُوْنَ

fataqa*ththh*a'uu amrahum baynahum zuburan kullu *h*izbin bim*aa* ladayhim fari*h*uun**a**

Kemudian mereka terpecah belah dalam urusan (agama)nya menjadi beberapa golongan. Setiap golongan (merasa) bangga dengan apa yang ada pada mereka (masing-masing).

23:54

# فَذَرْهُمْ فِيْ غَمْرَتِهِمْ حَتّٰى حِيْنٍ

fa*dz*arhum fii ghamratihim *h*att*aa* *h*iin**in**

Maka biarkanlah mereka dalam kesesatannya sampai waktu yang ditentukan.

23:55

# اَيَحْسَبُوْنَ اَنَّمَا نُمِدُّهُمْ بِهٖ مِنْ مَّالٍ وَّبَنِيْنَ ۙ

aya*h*sabuuna annam*aa* numidduhum bihi min m*aa*lin wabaniin**a**

Apakah mereka mengira bahwa Kami memberikan harta dan anak-anak kepada mereka itu (berarti bahwa),

23:56

# نُسَارِعُ لَهُمْ فِى الْخَيْرٰتِۗ بَلْ لَّا يَشْعُرُوْنَ

nus*aa*ri'u lahum fii **a**lkhayr*aa*ti bal l*aa* yasy'uruun**a**

Kami segera memberikan kebaikan-kebaikan kepada mereka? (Tidak), tetapi mereka tidak menyadarinya.

23:57

# اِنَّ الَّذِيْنَ هُمْ مِّنْ خَشْيَةِ رَبِّهِمْ مُّشْفِقُوْنَ ۙ

inna **al**la*dz*iina hum min khasyyati rabbihim musyfiquun**a**

Sungguh, orang-orang yang karena takut (azab) Tuhannya, mereka sangat berhati-hati,

23:58

# وَالَّذِيْنَ هُمْ بِاٰيٰتِ رَبِّهِمْ يُؤْمِنُوْنَ ۙ

wa**a**lla*dz*iina hum bi-*aa*y*aa*ti rabbihim yu/minuun**a**

dan mereka yang beriman dengan tanda-tanda (kekuasaan) Tuhannya,

23:59

# وَالَّذِيْنَ هُمْ بِرَبِّهِمْ لَا يُشْرِكُوْنَ ۙ

wa**a**lla*dz*iina hum birabbihim l*aa* yusyrikuun**a**

dan mereka yang tidak mempersekutukan Tuhannya,

23:60

# وَالَّذِيْنَ يُؤْتُوْنَ مَآ اٰتَوْا وَّقُلُوْبُهُمْ وَجِلَةٌ اَنَّهُمْ اِلٰى رَبِّهِمْ رٰجِعُوْنَ ۙ

wa**a**lla*dz*iina yu/tuuna m*aa* *aa*taw waquluubuhum wajilatun annahum il*aa* rabbihim r*aa*ji'uun**a**

dan mereka yang memberikan apa yang mereka berikan (sedekah) dengan hati penuh rasa takut (karena mereka tahu) bahwa sesungguhnya mereka akan kembali kepada Tuhannya,

23:61

# اُولٰۤىِٕكَ يُسَارِعُوْنَ فِى الْخَيْرٰتِ وَهُمْ لَهَا سٰبِقُوْنَ

ul*aa*-ika yus*aa*ri'uuna fii **a**lkhayr*aa*ti wah

mereka itu bersegera dalam kebaikan-kebaikan, dan merekalah orang-orang yang lebih dahulu memperolehnya.

23:62

# وَلَا نُكَلِّفُ نَفْسًا اِلَّا وُسْعَهَاۖ وَلَدَيْنَا كِتٰبٌ يَّنْطِقُ بِالْحَقِّ وَهُمْ لَا يُظْلَمُوْنَ

wal*aa* nukallifu nafsan ill*aa* wus'ah*aa* waladayn*aa* kit*aa*bun yan*th*iqu bi**a**l*h*aqqi wahum l*aa* yu*zh*lamuun**a**

Dan Kami tidak membebani seseorang melainkan menurut kesanggupannya, dan pada Kami ada suatu catatan yang menuturkan dengan sebenarnya, dan mereka tidak dizalimi (dirugikan).

23:63

# بَلْ قُلُوْبُهُمْ فِيْ غَمْرَةٍ مِّنْ هٰذَا وَلَهُمْ اَعْمَالٌ مِّنْ دُوْنِ ذٰلِكَ هُمْ لَهَا عَامِلُوْنَ

bal quluubuhum fii ghamratin min h*aadzaa* walahum a'm*aa*lun min duuni *dzaa*lika hum lah*aa* '*aa*miluun**a**

Tetapi, hati mereka (orang-orang kafir) itu dalam kesesatan dari (memahami Al-Qur'an) ini, dan mereka mempunyai (kebiasaan banyak mengerjakan) perbuatan-perbuatan lain (buruk) yang terus mereka kerjakan.

23:64

# حَتّٰٓى اِذَآ اَخَذْنَا مُتْرَفِيْهِمْ بِالْعَذَابِ اِذَا هُمْ يَجْـَٔرُوْنَ ۗ

*h*att*aa* i*dzaa* akha*dz*n*aa* mutrafiihim bi**a**l'a*dzaa*bi i*dzaa* hum yaj-aruun**a**

Sehingga apabila Kami timpakan siksaan kepada orang-orang yang hidup bermewah-mewah di antara mereka, seketika itu mereka berteriak-teriak meminta tolong.

23:65

# لَا تَجْـَٔرُوا الْيَوْمَۖ اِنَّكُمْ مِّنَّا لَا تُنْصَرُوْنَ

l*aa* taj-aruu **a**lyawma innakum minn*aa* l*aa* tun*sh*aruun**a**

Janganlah kamu berteriak-teriak meminta tolong pada hari ini! Sungguh, kamu tidak akan mendapat pertolongan dari Kami.

23:66

# قَدْ كَانَتْ اٰيٰتِيْ تُتْلٰى عَلَيْكُمْ فَكُنْتُمْ عَلٰٓى اَعْقَابِكُمْ تَنْكِصُوْنَ ۙ

qad k*aa*nat *aa*y*aa*tii tutl*aa* 'alaykum fakuntum 'al*aa* a'q*aa*bikum tanki*sh*uun**a**

Sesungguhnya ayat-ayat-Ku (Al-Qur'an) selalu dibacakan kepada kamu, tetapi kamu selalu berpaling ke belakang,

23:67

# مُسْتَكْبِرِيْنَۙ بِهٖ سٰمِرًا تَهْجُرُوْنَ

mustakbiriina bihi s*aa*miran tahjuruun**a**

dengan menyombongkan diri dan mengucapkan perkataan-perkataan keji terhadapnya (Al-Qur'an) pada waktu kamu bercakap-cakap pada malam hari.

23:68

# اَفَلَمْ يَدَّبَّرُوا الْقَوْلَ اَمْ جَاۤءَهُمْ مَّا لَمْ يَأْتِ اٰبَاۤءَهُمُ الْاَوَّلِيْنَ ۖ

afalam yaddabbaruu **a**lqawla am j*aa*-ahum m*aa* lam ya/ti *aa*b*aa*-ahumu **a**l-awwaliin**a**

Maka tidakkah mereka menghayati firman (Allah), atau adakah telah datang kepada mereka apa yang tidak pernah datang kepada nenek moyang mereka terdahulu?

23:69

# اَمْ لَمْ يَعْرِفُوْا رَسُوْلَهُمْ فَهُمْ لَهٗ مُنْكِرُوْنَ ۖ

am lam ya'rifuu rasuulahum fahum lahu munkiruun**a**

Ataukah mereka tidak mengenal Rasul mereka (Muhammad), karena itu mereka mengingkarinya?

23:70

# اَمْ يَقُوْلُوْنَ بِهٖ جِنَّةٌ ۗ بَلْ جَاۤءَهُمْ بِالْحَقِّ وَاَكْثَرُهُمْ لِلْحَقِّ كٰرِهُوْنَ

am yaquuluuna bihi jinnatun bal j*aa*-ahum bi**a**l*h*aqqi wa-aktsaruhum lil*h*aqqi k*aa*rihuun**a**

Atau mereka berkata, “Orang itu (Muhammad) gila.” Padahal, dia telah datang membawa kebenaran kepada mereka, tetapi kebanyakan mereka membenci kebenaran.

23:71

# وَلَوِ اتَّبَعَ الْحَقُّ اَهْوَاۤءَهُمْ لَفَسَدَتِ السَّمٰوٰتُ وَالْاَرْضُ وَمَنْ فِيْهِنَّۗ بَلْ اَتَيْنٰهُمْ بِذِكْرِهِمْ فَهُمْ عَنْ ذِكْرِهِمْ مُّعْرِضُوْنَ ۗ

walawi ittaba'a **a**l*h*aqqu ahw*aa*-ahum lafasadati **al**ssam*aa*w*aa*tu wa**a**l-ar*dh*u waman fiihinna bal atayn*aa*hum bi*dz*ikrihim fahum 'an *dz*ikrihim mu'ri*dh*u

Dan seandainya kebenaran itu menuruti keinginan mereka, pasti binasalah langit dan bumi, dan semua yang ada di dalamnya. Bahkan Kami telah memberikan peringatan kepada mereka, tetapi mereka berpaling dari peringatan itu.

23:72

# اَمْ تَسْـَٔلُهُمْ خَرْجًا فَخَرَاجُ رَبِّكَ خَيْرٌ ۖوَّهُوَ خَيْرُ الرّٰزِقِيْنَ

am tas-aluhum kharjan fakhar*aa*ju rabbika khayrun wahuwa khayru **al**rr*aa*ziqiin**a**

Atau engkau (Muhammad) meminta imbalan kepada mereka? Sedangkan imbalan dari Tuhanmu lebih baik, karena Dia pemberi rezeki yang terbaik.

23:73

# وَاِنَّكَ لَتَدْعُوْهُمْ اِلٰى صِرَاطٍ مُّسْتَقِيْمٍ

wa-innaka latad'uuhum il*aa* *sh*ir*aath*in mustaqiim**in**

Dan sesungguhnya engkau pasti telah menyeru mereka kepada jalan yang lurus.

23:74

# وَاِنَّ الَّذِيْنَ لَا يُؤْمِنُوْنَ بِالْاٰخِرَةِ عَنِ الصِّرَاطِ لَنَاكِبُوْنَ

wa-inna **al**la*dz*iina l*aa* yu/minuuna bi**a**l-*aa*khirati 'ani **al***shsh*ir*aath*i lan*aa*kibuun**a**

Dan sesungguhnya orang-orang yang tidak beriman kepada akhirat benar-benar telah menyimpang jauh dari jalan (yang lurus).

23:75

# ۞ وَلَوْ رَحِمْنٰهُمْ وَكَشَفْنَا مَا بِهِمْ مِّنْ ضُرٍّ لَّلَجُّوْا فِيْ طُغْيَانِهِمْ يَعْمَهُوْنَ

walaw ra*h*imn*aa*hum wakasyafn*aa* m*aa* bihim min *dh*urrin lalajjuu fii *th*ughy*aa*nihim ya'mahuun**a**

Dan seandainya mereka Kami kasihani, dan Kami lenyapkan malapetaka yang menimpa mereka, pasti mereka akan terus-menerus terombang-ambing dalam kesesatan mereka.

23:76

# وَلَقَدْ اَخَذْنٰهُمْ بِالْعَذَابِ فَمَا اسْتَكَانُوْا لِرَبِّهِمْ وَمَا يَتَضَرَّعُوْنَ

walaqad akha*dz*n*aa*hum bi**a**l'a*dzaa*bi fam*aa* istak*aa*nuu lirabbihim wam*aa* yata*dh*arra'uun**a**

Dan sungguh Kami telah menimpakan siksaan kepada mereka, tetapi mereka tidak mau tunduk kepada Tuhannya, dan (juga) tidak merendahkan diri.

23:77

# حَتّٰٓى اِذَا فَتَحْنَا عَلَيْهِمْ بَابًا ذَا عَذَابٍ شَدِيْدٍ اِذَا هُمْ فِيْهِ مُبْلِسُوْنَ ࣖ

*h*att*aa* i*dzaa* fata*h*n*aa* 'alayhim b*aa*ban *dzaa* 'a*dzaa*bin syadiidin i*dzaa* hum fiihi mublisuun**a**

Sehingga apabila Kami bukakan untuk mereka pintu azab yang sangat keras, seketika itu mereka menjadi putus asa.

23:78

# وَهُوَ الَّذِيْٓ اَنْشَاَ لَكُمُ السَّمْعَ وَالْاَبْصَارَ وَالْاَفْـِٕدَةَۗ قَلِيْلًا مَّا تَشْكُرُوْنَ

wahuwa **al**la*dz*ii ansya-a lakumu **al**ssam'a wa**a**l-ab*shaa*ra wa**a**l-af-idata qaliilan m*aa* tasykuruun**a**

Dan Dialah yang telah menciptakan bagimu pendengaran, penglihatan dan hati nurani, tetapi sedikit sekali kamu bersyukur.

23:79

# وَهُوَ الَّذِيْ ذَرَاَكُمْ فِى الْاَرْضِ وَاِلَيْهِ تُحْشَرُوْنَ

wahuwa **al**la*dz*ii *dz*ara-akum fii **a**l-ar*dh*i wa-ilayhi tu*h*syaruun**a**

Dan Dialah yang menciptakan dan mengembangbiakkan kamu di bumi dan kepada-Nyalah kamu akan dikumpulkan.

23:80

# وَهُوَ الَّذِيْ يُحْيٖ وَيُمِيْتُ وَلَهُ اخْتِلَافُ الَّيْلِ وَالنَّهَارِۗ اَفَلَا تَعْقِلُوْنَ

wahuwa **al**la*dz*ii yu*h*yii wayumiitu walahu ikhtil*aa*fu **al**layli wa**al**nnah*aa*ri afal*aa* ta'qiluun**a**

Dan Dialah yang menghidupkan dan mematikan, dan Dialah yang (mengatur) pergantian malam dan siang. Tidakkah kamu mengerti?

23:81

# بَلْ قَالُوْا مِثْلَ مَا قَالَ الْاَوَّلُوْنَ

bal q*aa*luu mitsla m*aa* q*aa*la **a**l-awwaluun**a**

Bahkan mereka mengucapkan perkataan yang serupa dengan apa yang diucapkan oleh orang-orang terdahulu.

23:82

# قَالُوْٓا ءَاِذَا مِتْنَا وَكُنَّا تُرَابًا وَّعِظَامًا ءَاِنَّا لَمَبْعُوْثُوْنَ

q*aa*luu a-i*dzaa* mitn*aa* wakunn*aa* tur*aa*ban wa'i*zhaa*man a-inn*aa* lamab'uutsuun**a**

Mereka berkata, “Apakah betul, apabila kami telah mati dan telah menjadi tanah dan tulang belulang, kami benar-benar akan dibangkitkan kembali?

23:83

# لَقَدْ وُعِدْنَا نَحْنُ وَاٰبَاۤؤُنَا هٰذَا مِنْ قَبْلُ اِنْ هٰذَآ اِلَّآ اَسَاطِيْرُ الْاَوَّلِيْنَ

laqad wu'idn*aa* na*h*nu wa*aa*b*aa*un*aa* h*aadzaa* min qablu in h*aadzaa* ill*aa* as*aath*iiru **a**l-awwaliin**a**

Sungguh, yang demikian ini sudah dijanjikan kepada kami dan kepada nenek moyang kami dahulu, ini hanyalah dongeng orang-orang terdahulu!”

23:84

# قُلْ لِّمَنِ الْاَرْضُ وَمَنْ فِيْهَآ اِنْ كُنْتُمْ تَعْلَمُوْنَ

qul limani **a**l-ar*dh*u waman fiih*aa* in kuntum ta'lamuun**a**

Katakanlah (Muhammad), “Milik siapakah bumi, dan semua yang ada di dalamnya, jika kamu mengetahui?”

23:85

# سَيَقُوْلُوْنَ لِلّٰهِ ۗقُلْ اَفَلَا تَذَكَّرُوْنَ

sayaquuluuna lill*aa*hi qul afal*aa* ta*dz*akkaruun**a**

Mereka akan menjawab, “Milik Allah.” Katakanlah, “Maka apakah kamu tidak ingat?”

23:86

# قُلْ مَنْ رَّبُّ السَّمٰوٰتِ السَّبْعِ وَرَبُّ الْعَرْشِ الْعَظِيْمِ

qul man rabbu **al**ssam*aa*w*aa*ti **al**ssab'i warabbu **a**l'arsyi **a**l'a*zh*iim**i**

Katakanlah, “Siapakah Tuhan yang memiliki langit yang tujuh dan yang memiliki ‘Arsy yang agung?”

23:87

# سَيَقُوْلُوْنَ لِلّٰهِ ۗقُلْ اَفَلَا تَتَّقُوْنَ

sayaquuluuna lill*aa*hi qul afal*aa* tattaquun**a**

Mereka akan menjawab, “(Milik) Allah.” Katakanlah, “Maka mengapa kamu tidak bertakwa?”

23:88

# قُلْ مَنْۢ بِيَدِهٖ مَلَكُوْتُ كُلِّ شَيْءٍ وَّهُوَ يُجِيْرُ وَلَا يُجَارُ عَلَيْهِ اِنْ كُنْتُمْ تَعْلَمُوْنَ

qul man biyadihi malakuutu kulli syay-in wahuwa yujiiru wal*aa* yuj*aa*ru 'alayhi in kuntum ta'lamuun**a**

Katakanlah, “Siapakah yang di tangan-Nya berada kekuasaan segala sesuatu. Dia melindungi, dan tidak ada yang dapat dilindungi dari (azab-Nya), jika kamu mengetahui?”

23:89

# سَيَقُوْلُوْنَ لِلّٰهِ ۗقُلْ فَاَنّٰى تُسْحَرُوْنَ

sayaquuluuna lill*aa*hi qul fa-ann*aa* tus*h*aruun**a**

Mereka akan menjawab, “(Milik) Allah.” Katakanlah, “(Kalau demikian), maka bagaimana kamu sampai tertipu?”

23:90

# بَلْ اَتَيْنٰهُمْ بِالْحَقِّ وَاِنَّهُمْ لَكٰذِبُوْنَ

bal atayn*aa*hum bi**a**l*h*aqqi wa-innahum lak*aadz*ibuun**a**

Padahal Kami telah membawa kebenaran kepada mereka, tetapi mere-ka benar-benar pendusta.

23:91

# مَا اتَّخَذَ اللّٰهُ مِنْ وَّلَدٍ وَّمَا كَانَ مَعَهٗ مِنْ اِلٰهٍ اِذًا لَّذَهَبَ كُلُّ اِلٰهٍۢ بِمَا خَلَقَ وَلَعَلَا بَعْضُهُمْ عَلٰى بَعْضٍۗ سُبْحٰنَ اللّٰهِ عَمَّا يَصِفُوْنَ ۙ

m*aa* ittakha*dz*a **al**l*aa*hu min waladin wam*aa* k*aa*na ma'ahu min il*aa*hin i*dz*an la*dz*ahaba kullu il*aa*hin bim*aa* khalaqa wala'al*aa* ba'*dh*uhum 'al*aa* ba'*dh*

*Allah tidak mempunyai anak, dan tidak ada tuhan (yang lain) bersama-Nya, (sekiranya tuhan banyak), maka masing-masing tuhan itu akan membawa apa (makhluk) yang diciptakannya, dan sebagian dari tuhan-tuhan itu akan mengalahkan sebagian yang lain. Mahasuci*









23:92

# عٰلِمِ الْغَيْبِ وَالشَّهَادَةِ فَتَعٰلٰى عَمَّا يُشْرِكُوْنَ ࣖ

'*aa*limi **a**lghaybi wa**al**sysyah*aa*dati fata'*aa*l*aa* 'amm*aa* yusyrikuun**a**

(Dialah Tuhan) yang mengetahui semua yang gaib dan semua yang tampak. Mahatinggi (Allah) dari apa yang mereka persekutukan.

23:93

# قُلْ رَّبِّ اِمَّا تُرِيَنِّيْ مَا يُوْعَدُوْنَ ۙ

qul rabbi imm*aa* turiyannii m*aa* yuu'aduun**a**

Katakanlah (Muhammad), “Ya Tuhanku, seandainya Engkau hendak memperlihatkan kepadaku apa (azab) yang diancamkan kepada mereka,

23:94

# رَبِّ فَلَا تَجْعَلْنِيْ فِى الْقَوْمِ الظّٰلِمِيْنَ

rabbi fal*aa* taj'alnii fii **a**lqawmi **al***zhzhaa*limiin**a**

Ya Tuhanku, maka janganlah Engkau jadikan aku dalam golongan orang-orang zalim.”

23:95

# وَاِنَّا عَلٰٓى اَنْ نُّرِيَكَ مَا نَعِدُهُمْ لَقٰدِرُوْنَ

wa-inn*aa* 'al*aa* an nuriyaka m*aa* na'iduhum laq*aa*diruun**a**

Dan sungguh, Kami kuasa untuk memperlihatkan kepadamu (Muhammad) apa yang Kami ancamkan kepada mereka.

23:96

# اِدْفَعْ بِالَّتِيْ هِيَ اَحْسَنُ السَّيِّئَةَۗ نَحْنُ اَعْلَمُ بِمَا يَصِفُوْنَ

idfa' bi**a**llatii hiya a*h*sanu **al**ssayyi-ata na*h*nu a'lamu bim*aa* ya*sh*ifuun**a**

Tolaklah perbuatan buruk mereka dengan (cara) yang lebih baik, Kami lebih mengetahui apa yang mereka sifatkan (kepada Allah).

23:97

# وَقُلْ رَّبِّ اَعُوْذُ بِكَ مِنْ هَمَزٰتِ الشَّيٰطِيْنِ ۙ

waqul rabbi a'uu*dz*u bika min hamaz*aa*ti **al**sysyay*aath*iin**i**

Dan katakanlah, “Ya Tuhanku, aku berlindung kepada Engkau dari bisikan-bisikan setan,

23:98

# وَاَعُوْذُ بِكَ رَبِّ اَنْ يَّحْضُرُوْنِ

wa-a'uu*dz*u bika rabbi an ya*hd*uruun**i**

dan aku berlindung (pula) kepada Engkau ya Tuhanku, agar mereka tidak mendekati aku.”

23:99

# حَتّٰٓى اِذَا جَاۤءَ اَحَدَهُمُ الْمَوْتُ قَالَ رَبِّ ارْجِعُوْنِ ۙ

*h*att*aa* i*dzaa* j*aa*-a a*h*adahumu **a**lmawtu q*aa*la rabbi irji'uun**i**

(Demikianlah keadaan orang-orang kafir itu), hingga apabila datang kematian kepada seseorang dari mereka, dia berkata, “Ya Tuhanku, kembalikanlah aku (ke dunia),

23:100

# لَعَلِّيْٓ اَعْمَلُ صَالِحًا فِيْمَا تَرَكْتُ كَلَّاۗ اِنَّهَا كَلِمَةٌ هُوَ قَاۤىِٕلُهَاۗ وَمِنْ وَّرَاۤىِٕهِمْ بَرْزَخٌ اِلٰى يَوْمِ يُبْعَثُوْنَ

la'allii a'malu *shaa*li*h*an fiim*aa* taraktu kall*aa* innah*aa* kalimatun huwa q*aa*-iluh*aa* wamin war*aa*-ihim barzakhun il*aa* yawmi yub'atsuun**a**

agar aku dapat berbuat kebajikan yang telah aku tinggalkan.” Sekali-kali tidak! Sesungguhnya itu adalah dalih yang diucapkannya saja. Dan di hadapan mereka ada barzakh sampai pada hari mereka dibangkitkan.

23:101

# فَاِذَا نُفِخَ فِى الصُّوْرِ فَلَآ اَنْسَابَ بَيْنَهُمْ يَوْمَىِٕذٍ وَّلَا يَتَسَاۤءَلُوْنَ

fa-i*dzaa* nufikha fii **al***shsh*uuri fal*aa* ans*aa*ba baynahum yawma-i*dz*in wal*aa* yatas*aa*-aluun**a**

Apabila sangkakala ditiup maka tidak ada lagi pertalian keluarga di antara mereka pada hari itu (hari Kiamat), dan tidak (pula) mereka saling bertanya.

23:102

# فَمَنْ ثَقُلَتْ مَوَازِيْنُهٗ فَاُولٰۤىِٕكَ هُمُ الْمُفْلِحُوْنَ

faman tsaqulat maw*aa*ziinuhu faul*aa*-ika humu **a**lmufli*h*uun**a**

Barangsiapa berat timbangan (kebaikan)nya, maka mereka itulah orang-orang yang beruntung.

23:103

# وَمَنْ خَفَّتْ مَوَازِيْنُهٗ فَاُولٰۤىِٕكَ الَّذِيْنَ خَسِرُوْٓا اَنْفُسَهُمْ فِيْ جَهَنَّمَ خٰلِدُوْنَ ۚ

waman khaffat maw*aa*ziinuhu faul*aa*-ika **al**la*dz*iina khasiruu anfusahum fii jahannama kh*aa*liduun**a**

Dan barang siapa ringan timbangan (kebaikan)nya, maka mereka itulah orang-orang yang merugikan dirinya sendiri, mereka kekal di dalam neraka Jahanam

23:104

# تَلْفَحُ وُجُوْهَهُمُ النَّارُ وَهُمْ فِيْهَا كَالِحُوْنَ

talfa*h*u wujuuhahumu **al**nn*aa*ru wahum fiih*aa* k*aa*li*h*uun**a**

Wajah mereka dibakar api neraka, dan mereka di neraka dalam keadaan muram dengan bibir yang cacat.

23:105

# اَلَمْ تَكُنْ اٰيٰتِيْ تُتْلٰى عَلَيْكُمْ فَكُنْتُمْ بِهَا تُكَذِّبُوْنَ

alam takun *aa*y*aa*tii tutl*aa* 'alaykum fakuntum bih*aa* tuka*dzdz*ibuun**a**

Bukankah ayat-ayat-Ku telah dibacakan kepadamu, tetapi kamu selalu mendustakannya?

23:106

# قَالُوْا رَبَّنَا غَلَبَتْ عَلَيْنَا شِقْوَتُنَا وَكُنَّا قَوْمًا ضَاۤلِّيْنَ

q*aa*luu rabban*aa* ghalabat 'alayn*aa* syiqwatun*aa* wakunn*aa* qawman *daa*lliin**a**

Mereka berkata, “Ya Tuhan kami, kami telah dikuasai oleh kejahatan kami, dan kami adalah orang-orang yang sesat.

23:107

# رَبَّنَآ اَخْرِجْنَا مِنْهَا فَاِنْ عُدْنَا فَاِنَّا ظٰلِمُوْنَ

rabban*aa* akhrijn*aa* minh*aa* fa-in 'udn*aa* fa-inn*aa* *zhaa*limuun**a**

Ya Tuhan kami, keluarkanlah kami darinya (kembalikanlah kami ke dunia), jika kami masih juga kembali (kepada kekafiran), sungguh, kami adalah orang-orang yang zalim.”

23:108

# قَالَ اخْسَـُٔوْا فِيْهَا وَلَا تُكَلِّمُوْنِ

q*aa*la ikhsauu fiih*aa* wal*aa* tukallimuun**i**

Dia (Allah) berfirman, “Tinggallah dengan hina di dalamnya, dan janganlah kamu berbicara dengan Aku.”

23:109

# اِنَّهٗ كَانَ فَرِيْقٌ مِّنْ عِبَادِيْ يَقُوْلُوْنَ رَبَّنَآ اٰمَنَّا فَاغْفِرْ لَنَا وَارْحَمْنَا وَاَنْتَ خَيْرُ الرّٰحِمِيْنَ ۚ

innahu k*aa*na fariiqun min 'ib*aa*dii yaquuluuna rabban*aa* *aa*mann*aa* fa**i**ghfir lan*aa* wa**i**r*h*amn*aa* wa-anta khayru **al**rr*aah*imiin**a**

Sesungguhnya ada segolongan dari hamba-hamba-Ku berdoa, “Ya Tuhan kami, kami telah beriman, maka ampunilah kami dan berilah kami rahmat, Engkau adalah pemberi rahmat yang terbaik.”

23:110

# فَاتَّخَذْتُمُوْهُمْ سِخْرِيًّا حَتّٰٓى اَنْسَوْكُمْ ذِكْرِيْ وَكُنْتُمْ مِّنْهُمْ تَضْحَكُوْنَ

fa**i**ttakha*dz*tumuuhum sikhriyyan *h*att*aa* ansawkum *dz*ikrii wakuntum minhum ta*dh*akuun**a**

Lalu kamu jadikan mereka buah ejekan, sehingga kamu lupa mengingat Aku, dan kamu (selalu) menertawakan mereka,

23:111

# اِنِّيْ جَزَيْتُهُمُ الْيَوْمَ بِمَا صَبَرُوْٓاۙ اَنَّهُمْ هُمُ الْفَاۤىِٕزُوْنَ

innii jazaytuhumu **a**lyawma bim*aa* *sh*abaruu annahum humu **a**lf*aa*-izuun**a**

sesungguhnya pada hari ini Aku memberi balasan kepada mereka, karena kesabaran mereka; sesungguhnya mereka itulah orang-orang yang memperoleh kemenangan.

23:112

# قٰلَ كَمْ لَبِثْتُمْ فِى الْاَرْضِ عَدَدَ سِنِيْنَ

q*aa*la kam labitstum fii **a**l-ar*dh*i 'adada siniin**a**

Dia (Allah) berfirman, “Berapa tahunkah lamanya kamu tinggal di bumi?”

23:113

# قَالُوْا لَبِثْنَا يَوْمًا اَوْ بَعْضَ يَوْمٍ فَسْـَٔلِ الْعَاۤدِّيْنَ

q*aa*luu labitsn*aa* yawman aw ba'*dh*a yawmin fa**i**s-ali **a**l'*aa*ddiin**a**

Mereka menjawab, “Kami tinggal (di bumi) sehari atau setengah hari, maka tanyakanlah kepada mereka yang menghitung.”

23:114

# قٰلَ اِنْ لَّبِثْتُمْ اِلَّا قَلِيْلًا لَّوْ اَنَّكُمْ كُنْتُمْ تَعْلَمُوْنَ

q*aa*la in labitstum ill*aa* qaliilan law annakum kuntum ta'lamuun**a**

Dia (Allah) berfirman, “Kamu tinggal (di bumi) hanya sebentar saja, jika kamu benar-benar mengetahui.”

23:115

# اَفَحَسِبْتُمْ اَنَّمَا خَلَقْنٰكُمْ عَبَثًا وَّاَنَّكُمْ اِلَيْنَا لَا تُرْجَعُوْنَ

afa*h*asibtum annam*aa* khalaqn*aa*kum 'abatsan wa-annakum ilayn*aa* l*aa* turja'uun**a**

Maka apakah kamu mengira, bahwa Kami menciptakan kamu main-main (tanpa ada maksud) dan bahwa kamu tidak akan dikembalikan kepada Kami?

23:116

# فَتَعٰلَى اللّٰهُ الْمَلِكُ الْحَقُّۚ لَآ اِلٰهَ اِلَّا هُوَۚ رَبُّ الْعَرْشِ الْكَرِيْمِ

fata'*aa*l*aa* **al**l*aa*hu **a**lmaliku **a**l*h*aqqu l*aa* il*aa*ha ill*aa* huwa rabbu **a**l'arsyi **a**lkariim**i**

Maka Mahatinggi Allah, Raja yang sebenarnya; tidak ada tuhan (yang berhak disembah) selain Dia, Tuhan (yang memiliki) ‘Arsy yang mulia.

23:117

# وَمَنْ يَّدْعُ مَعَ اللّٰهِ اِلٰهًا اٰخَرَ لَا بُرْهَانَ لَهٗ بِهٖۙ فَاِنَّمَا حِسَابُهٗ عِنْدَ رَبِّهٖۗ اِنَّهٗ لَا يُفْلِحُ الْكٰفِرُوْنَ

waman yad'u ma'a **al**l*aa*hi il*aa*han *aa*khara l*aa* burh*aa*na lahu bihi fa-innam*aa* *h*is*aa*buhu 'inda rabbihi innahu l*aa* yufli*h*u **a**lk*aa*firuun**a**

**Dan barang siapa menyembah tuhan yang lain selain Allah, padahal tidak ada suatu bukti pun baginya tentang itu, maka perhitungannya hanya pada Tuhannya. Sesungguhnya orang-orang kafir itu tidak akan beruntung.**









23:118

# وَقُلْ رَّبِّ اغْفِرْ وَارْحَمْ وَاَنْتَ خَيْرُ الرّٰحِمِيْنَ ࣖ

waqul rabbi ighfir wa**i**r*h*am wa-anta khayru **al**rr*aah*imiin**a**

Dan katakanlah (Muhammad), “Ya Tuhanku, berilah ampunan dan (berilah) rahmat, Engkaulah pemberi rahmat yang terbaik.”

<!--EndFragment-->