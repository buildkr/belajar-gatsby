---
title: (107) Al-Ma'un - الماعون
date: 2021-10-27T04:17:36.694Z
ayat: 107
description: "Jumlah Ayat: 7 / Arti: Barang Yang Berguna"
---
<!--StartFragment-->

107:1

# اَرَءَيْتَ الَّذِيْ يُكَذِّبُ بِالدِّيْنِۗ

ara-ayta **al**la*dz*ii yuka*dzdz*ibu bi**al**ddiin**i**

Tahukah kamu (orang) yang mendustakan agama?

107:2

# فَذٰلِكَ الَّذِيْ يَدُعُّ الْيَتِيْمَۙ

fa*dzaa*lika **al**la*dz*ii yadu''u **a**lyatiim**a**

Maka itulah orang yang menghardik anak yatim,

107:3

# وَلَا يَحُضُّ عَلٰى طَعَامِ الْمِسْكِيْنِۗ

wal*aa* ya*h*u*dhdh*u 'al*aa* *th*a'*aa*mi **a**lmiskiin**i**

dan tidak mendorong memberi makan orang miskin.

107:4

# فَوَيْلٌ لِّلْمُصَلِّيْنَۙ

fawaylun lilmu*sh*alliin**a**

Maka celakalah orang yang salat,

107:5

# الَّذِيْنَ هُمْ عَنْ صَلَاتِهِمْ سَاهُوْنَۙ

**al**la*dz*iina hum 'an *sh*al*aa*tihim s*aa*huun**a**

(yaitu) orang-orang yang lalai terhadap salatnya,

107:6

# الَّذِيْنَ هُمْ يُرَاۤءُوْنَۙ

**al**la*dz*iina hum yur*aa*uun**a**

yang berbuat ria,

107:7

# وَيَمْنَعُوْنَ الْمَاعُوْنَ ࣖ

wayamna'uuna **a**lm*aa*'uun**a**

dan enggan (memberikan) bantuan.

<!--EndFragment-->