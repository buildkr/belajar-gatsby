---
title: (81) At-Takwir - التكوير
date: 2021-10-27T04:15:53.287Z
ayat: 81
description: "Jumlah Ayat: 29 / Arti: Penggulungan"
---
<!--StartFragment-->

81:1

# اِذَا الشَّمْسُ كُوِّرَتْۖ

i*dzaa* **al**sysyamsu kuwwirat

Apabila matahari digulung,

81:2

# وَاِذَا النُّجُوْمُ انْكَدَرَتْۖ

wa-i*dzaa* **al**nnujuumu inkadarat

dan apabila bintang-bintang berjatuhan,

81:3

# وَاِذَا الْجِبَالُ سُيِّرَتْۖ

wa-i*dzaa* **a**ljib*aa*lu suyyirath

dan apabila gunung-gunung dihancurkan,

81:4

# وَاِذَا الْعِشَارُ عُطِّلَتْۖ

wa-i*dzaa* **a**l'isy*aa*ru 'u*ththh*ilath

dan apabila unta-unta yang bunting ditinggalkan (tidak terurus),

81:5

# وَاِذَا الْوُحُوْشُ حُشِرَتْۖ

wa-i*dzaa* **a**lwu*h*uusyu *h*usyirath

dan apabila binatang-binatang liar dikumpulkan,

81:6

# وَاِذَا الْبِحَارُ سُجِّرَتْۖ

wa-i*dzaa* **a**lbi*haa*ru sujjirath

dan apabila lautan dipanaskan,

81:7

# وَاِذَا النُّفُوْسُ زُوِّجَتْۖ

wa-i*dzaa* **al**nnufuusu zuwwijat

dan apabila roh-roh dipertemukan (dengan tubuh),

81:8

# وَاِذَا الْمَوْءٗدَةُ سُىِٕلَتْۖ

wa-i*dzaa* **a**lmawuudatu su-ilath

dan apabila bayi-bayi perempuan yang dikubur hidup-hidup ditanya,

81:9

# بِاَيِّ ذَنْۢبٍ قُتِلَتْۚ

bi-ayyi *dz*anbin qutilath

karena dosa apa dia dibunuh?

81:10

# وَاِذَا الصُّحُفُ نُشِرَتْۖ

wa-i*dzaa* **al***shsh*u*h*ufu nusyirath

Dan apabila lembaran-lembaran (catatan amal) telah dibuka lebar-lebar,

81:11

# وَاِذَا السَّمَاۤءُ كُشِطَتْۖ

wa-i*dzaa* **al**ssam*aa*u kusyi*th*ath

dan apabila langit dilenyapkan,

81:12

# وَاِذَا الْجَحِيْمُ سُعِّرَتْۖ

wa-i*dzaa* **a**lja*h*iimu su''irath

dan apabila neraka Jahim dinyalakan,

81:13

# وَاِذَا الْجَنَّةُ اُزْلِفَتْۖ

wa-i*dzaa* **a**ljannatu uzlifath

dan apabila surga didekatkan,

81:14

# عَلِمَتْ نَفْسٌ مَّآ اَحْضَرَتْۗ

'alimat nafsun m*aa* a*hd*arath

setiap jiwa akan mengetahui apa yang telah dikerjakannya.

81:15

# فَلَآ اُقْسِمُ بِالْخُنَّسِۙ

fal*aa* uqsimu bi**a**lkhunnas**i**

Aku bersumpah demi bintang-bintang,

81:16

# الْجَوَارِ الْكُنَّسِۙ

aljaw*aa*ri **a**lkunnas**i**

yang beredar dan terbenam,

81:17

# وَالَّيْلِ اِذَا عَسْعَسَۙ

wa**a**llayli i*dzaa* 'as'as**a**

demi malam apabila telah larut,

81:18

# وَالصُّبْحِ اِذَا تَنَفَّسَۙ

wa**al***shsh*ub*h*i i*dzaa* tanaffas**a**

dan demi subuh apabila fajar telah menyingsing,

81:19

# اِنَّهٗ لَقَوْلُ رَسُوْلٍ كَرِيْمٍۙ

innahu laqawlu rasuulin kariim**in**

sesungguhnya (Al-Qur'an) itu benar-benar firman (Allah yang dibawa oleh) utusan yang mulia (Jibril),

81:20

# ذِيْ قُوَّةٍ عِنْدَ ذِى الْعَرْشِ مَكِيْنٍۙ

*dz*ii quwwatin 'inda *dz*ii **a**l'arsyi makiin**in**

yang memiliki kekuatan, memiliki kedudukan tinggi di sisi (Allah) yang memiliki ‘Arsy,

81:21

# مُّطَاعٍ ثَمَّ اَمِيْنٍۗ

mu*thaa*'in tsamma amiin**in**

yang di sana (di alam malaikat) ditaati dan dipercaya.

81:22

# وَمَا صَاحِبُكُمْ بِمَجْنُوْنٍۚ

wam*aa* *shaah*ibukum bimajnuun**in**

Dan temanmu (Muhammad) itu bukanlah orang gila.

81:23

# وَلَقَدْ رَاٰهُ بِالْاُفُقِ الْمُبِيْنِۚ

walaqad ra*aa*hu bi**a**lufuqi **a**lmubiin**i**

Dan sungguh, dia (Muhammad) telah melihatnya (Jibril) di ufuk yang terang.

81:24

# وَمَا هُوَ عَلَى الْغَيْبِ بِضَنِيْنٍۚ

wam*aa* huwa 'al*aa* **a**lghaybi bi*dh*aniin**in**

Dan dia (Muhammad) bukanlah seorang yang kikir (enggan) untuk menerangkan yang gaib.

81:25

# وَمَا هُوَ بِقَوْلِ شَيْطٰنٍ رَّجِيْمٍۚ

wam*aa* huwa biqawli syay*thaa*nin rajiim**in**

Dan (Al-Qur'an) itu bukanlah perkataan setan yang terkutuk,

81:26

# فَاَيْنَ تَذْهَبُوْنَۗ

fa-ayna ta*dz*habuun**a**

maka ke manakah kamu akan pergi?

81:27

# اِنْ هُوَ اِلَّا ذِكْرٌ لِّلْعٰلَمِيْنَۙ

in huwa ill*aa* *dz*ikrun lil'*aa*lamiin**a**

(Al-Qur'an) itu tidak lain adalah peringatan bagi seluruh alam,

81:28

# لِمَنْ شَاۤءَ مِنْكُمْ اَنْ يَّسْتَقِيْمَۗ

liman sy*aa*-a minkum an yastaqiim**a**

(yaitu) bagi siapa di antara kamu yang menghendaki menempuh jalan yang lurus.

81:29

# وَمَا تَشَاۤءُوْنَ اِلَّآ اَنْ يَّشَاۤءَ اللّٰهُ رَبُّ الْعٰلَمِيْنَ ࣖ

wam*aa* tasy*aa*uuna ill*aa* an yasy*aa*-a **al**l*aa*hu rabbu **a**l'*aa*lamiin**a**

Dan kamu tidak dapat menghendaki (menempuh jalan itu) kecuali apabila dikehendaki Allah, Tuhan seluruh alam.

<!--EndFragment-->