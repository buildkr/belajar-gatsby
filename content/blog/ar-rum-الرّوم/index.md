---
title: (30) Ar-Rum - الرّوم
date: 2021-10-27T03:54:54.703Z
ayat: 30
description: "Jumlah Ayat: 60 / Arti: Romawi"
---
<!--StartFragment-->

30:1

# الۤمّۤ ۚ

alif-l*aa*m-miim

Alif Lam Mim.

30:2

# غُلِبَتِ الرُّوْمُۙ

ghulibati **al**rruum**u**

Bangsa Romawi telah dikalahkan,

30:3

# فِيْٓ اَدْنَى الْاَرْضِ وَهُمْ مِّنْۢ بَعْدِ غَلَبِهِمْ سَيَغْلِبُوْنَۙ

fii adn*aa* **a**l-ar*dh*i wahum min ba'di ghalabihim sayaghlibuun**a**

di negeri yang terdekat dan mereka setelah kekalahannya itu akan menang,

30:4

# فِيْ بِضْعِ سِنِيْنَ ەۗ لِلّٰهِ الْاَمْرُ مِنْ قَبْلُ وَمِنْۢ بَعْدُ ۗوَيَوْمَىِٕذٍ يَّفْرَحُ الْمُؤْمِنُوْنَۙ

fii bi*dh*'i siniina lill*aa*hi **a**l-amru min qablu wamin ba'du wayawma-i*dz*in yafra*h*u **a**lmu/minuun**a**

dalam beberapa tahun (lagi). Bagi Allah-lah urusan sebelum dan setelah (mereka menang). Dan pada hari (kemenangan bangsa Romawi) itu bergembiralah orang-orang yang beriman,

30:5

# بِنَصْرِ اللّٰهِ ۗيَنْصُرُ مَنْ يَّشَاۤءُۗ وَهُوَ الْعَزِيْزُ الرَّحِيْمُ

bina*sh*ri **al**l*aa*hi yan*sh*uru man yasy*aa*u wahuwa **a**l'aziizu **al**rra*h*iim**u**

karena pertolongan Allah. Dia menolong siapa yang Dia kehendaki. Dia Mahaperkasa, Maha Penyayang.

30:6

# وَعْدَ اللّٰهِ ۗ لَا يُخْلِفُ اللّٰهُ وَعْدَهٗ وَلٰكِنَّ اَكْثَرَ النَّاسِ لَا يَعْلَمُوْنَ

wa'da **al**l*aa*hi l*aa* yukhlifu **al**l*aa*hu wa'dahu wal*aa*kinna aktsara **al**nn*aa*si l*aa* ya'lamuun**a**

(Itulah) janji Allah. Allah tidak akan menyalahi janji-Nya, tetapi kebanyakan manusia tidak mengetahui.

30:7

# يَعْلَمُوْنَ ظَاهِرًا مِّنَ الْحَيٰوةِ الدُّنْيَاۖ وَهُمْ عَنِ الْاٰخِرَةِ هُمْ غٰفِلُوْنَ

ya'lamuuna *zhaa*hiran mina **a**l*h*ay*aa*ti **al**dduny*aa* wahum 'ani **a**l-*aa*khirati hum gh*aa*filuun**a**

Mereka mengetahui yang lahir (tampak) dari kehidupan dunia; sedangkan terhadap (kehidupan) akhirat mereka lalai.

30:8

# اَوَلَمْ يَتَفَكَّرُوْا فِيْٓ اَنْفُسِهِمْ ۗ مَا خَلَقَ اللّٰهُ السَّمٰوٰتِ وَالْاَرْضَ وَمَا بَيْنَهُمَآ اِلَّا بِالْحَقِّ وَاَجَلٍ مُّسَمًّىۗ وَاِنَّ كَثِيْرًا مِّنَ النَّاسِ بِلِقَاۤئِ رَبِّهِمْ لَكٰفِرُوْنَ

awa lam yatafakkaruu fii anfusihim m*aa* khalaqa **al**l*aa*hu **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*a wam*aa* baynahum*aa* ill*aa* bi**a**l*h*aqqi wa-ajalin m

Dan mengapa mereka tidak memikirkan tentang (kejadian) diri mereka? Allah tidak menciptakan langit dan bumi dan apa yang ada di antara keduanya melainkan dengan (tujuan) yang benar dan dalam waktu yang ditentukan. Dan sesungguhnya kebanyakan di antara man

30:9

# اَوَلَمْ يَسِيْرُوْا فِى الْاَرْضِ فَيَنْظُرُوْا كَيْفَ كَانَ عَاقِبَةُ الَّذِيْنَ مِنْ قَبْلِهِمْۗ كَانُوْٓا اَشَدَّ مِنْهُمْ قُوَّةً وَّاَثَارُوا الْاَرْضَ وَعَمَرُوْهَآ اَكْثَرَ مِمَّا عَمَرُوْهَا وَجَاۤءَتْهُمْ رُسُلُهُمْ بِالْبَيِّنٰتِۗ فَمَا كَانَ

awa lam yasiiruu fii **a**l-ar*dh*i fayan*zh*uruu kayfa k*aa*na '*aa*qibatu **al**la*dz*iina min qablihim k*aa*nuu asyadda minhum quwwatan wa-ats*aa*ruu **a**l-ar*dh*a wa'amaru

Dan tidakkah mereka bepergian di bumi lalu melihat bagaimana kesudahan orang-orang sebelum mereka (yang mendustakan rasul)? Orang-orang itu lebih kuat dari mereka (sendiri) dan mereka telah mengolah bumi (tanah) serta memakmurkannya melebihi apa yang tela

30:10

# ثُمَّ كَانَ عَاقِبَةَ الَّذِيْنَ اَسَاۤءُوا السُّوْۤاٰىٓ اَنْ كَذَّبُوْا بِاٰيٰتِ اللّٰهِ وَكَانُوْا بِهَا يَسْتَهْزِءُوْنَ ࣖ

tsumma k*aa*na '*aa*qibata **al**la*dz*iina as*aa*uu **al**ssuu-*aa* an ka*dzdz*abuu bi-*aa*y*aa*ti **al**l*aa*hi wak*aa*nuu bih*aa* yastahzi-uun**a**

**Kemudian, azab yang lebih buruk adalah kesudahan bagi orang-orang yang mengerjakan kejahatan. Karena mereka mendustakan ayat-ayat Allah dan mereka selalu memperolok-olokkannya.**









30:11

# اَللّٰهُ يَبْدَؤُا الْخَلْقَ ثُمَّ يُعِيْدُهٗ ثُمَّ اِلَيْهِ تُرْجَعُوْنَ

**al**l*aa*hu yabdau **a**lkhalqa tsumma yu'iiduhu tsumma ilayhi turja'uun**a**

Allah yang memulai penciptaan (makhluk), kemudian mengulanginya kembali; kemudian kepada-Nya kamu dikembalikan.

30:12

# وَيَوْمَ تَقُوْمُ السَّاعَةُ يُبْلِسُ الْمُجْرِمُوْنَ

wayawma taquumu **al**ss*aa*'atu yublisu **a**lmujrimuun**a**

Dan pada hari (ketika) terjadi Kiamat, orang-orang yang berdosa (kaum musyrik) terdiam berputus asa.

30:13

# وَلَمْ يَكُنْ لَّهُمْ مِّنْ شُرَكَاۤىِٕهِمْ شُفَعٰۤؤُا وَكَانُوْا بِشُرَكَاۤىِٕهِمْ كٰفِرِيْنَ

walam yakun lahum min syurak*aa*-ihim syufa'*aa*u wak*aa*nuu bisyurak*aa*-ihim k*aa*firiin**a**

Dan tidak mungkin ada pemberi syafaat (pertolongan) bagi mereka dari berhala-berhala mereka, sedangkan mereka mengingkari berhala-berhala mereka itu.

30:14

# وَيَوْمَ تَقُوْمُ السَّاعَةُ يَوْمَىِٕذٍ يَّتَفَرَّقُوْنَ

wayawma taquumu **al**ss*aa*'atu yawma-i*dz*in yatafarraquun**a**

Dan pada hari (ketika) terjadi Kiamat, pada hari itu manusia terpecah-pecah (dalam kelompok).

30:15

# فَاَمَّا الَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِ فَهُمْ فِيْ رَوْضَةٍ يُّحْبَرُوْنَ

fa-amm*aa* **al**la*dz*iina *aa*manuu wa'amiluu **al***shshaa*li*haa*ti fahum fii raw*dh*atin yu*h*baruun**a**

Maka adapun orang-orang yang beriman dan mengerjakan kebajikan, maka mereka di dalam taman (surga) bergembira.

30:16

# وَاَمَّا الَّذِيْنَ كَفَرُوْا وَكَذَّبُوْا بِاٰيٰتِنَا وَلِقَاۤئِ الْاٰخِرَةِ فَاُولٰۤىِٕكَ فِى الْعَذَابِ مُحْضَرُوْنَ

wa-amm*aa* **al**la*dz*iina kafaruu waka*dzdz*abuu bi-*aa*y*aa*tin*aa* waliq*aa*-i **a**l-*aa*khirati faul*aa*-ika fii **a**l'a*dzaa*bi mu*hd*aruun**a**

**Dan adapun orang-orang yang kafir dan mendustakan ayat-ayat Kami serta (mendustakan) pertemuan hari akhirat, maka mereka tetap berada di dalam azab (neraka).**









30:17

# فَسُبْحٰنَ اللّٰهِ حِيْنَ تُمْسُوْنَ وَحِيْنَ تُصْبِحُوْنَ

fasub*haa*na **al**l*aa*hi *h*iina tumsuuna wa*h*iina tu*sh*bi*h*uun**a**

Maka bertasbihlah kepada Allah pada petang hari dan pada pagi hari (waktu subuh),

30:18

# وَلَهُ الْحَمْدُ فِى السَّمٰوٰتِ وَالْاَرْضِ وَعَشِيًّا وَّحِيْنَ تُظْهِرُوْنَ

walahu **a**l*h*amdu fii **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i wa'asyiyyan wa*h*iina tu*zh*hiruun**a**

dan segala puji bagi-Nya baik di langit, di bumi, pada malam hari dan pada waktu zuhur (tengah hari).

30:19

# يُخْرِجُ الْحَيَّ مِنَ الْمَيِّتِ وَيُخْرِجُ الْمَيِّتَ مِنَ الْحَيِّ وَيُحْيِ الْاَرْضَ بَعْدَ مَوْتِهَا ۗوَكَذٰلِكَ تُخْرَجُوْنَ ࣖ

yukhriju **a**l*h*ayya mina **a**lmayyiti wayukhriju **a**lmayyita mina **a**l*h*ayyi wayu*h*yii **a**l-ar*dh*a ba'da mawtih*aa* waka*dzaa*lika tukhrajuun

Dia mengeluarkan yang hidup dari yang mati dan mengeluarkan yang mati dari yang hidup dan menghidupkan bumi setelah mati (kering). Dan seperti itulah kamu akan dikeluarkan (dari kubur).

30:20

# وَمِنْ اٰيٰتِهٖٓ اَنْ خَلَقَكُمْ مِّنْ تُرَابٍ ثُمَّ اِذَآ اَنْتُمْ بَشَرٌ تَنْتَشِرُوْنَ

wamin *aa*y*aa*tihi an khalaqakum min tur*aa*bin tsumma i*dzaa* antum basyarun tantasyiruun**a**

Dan di antara tanda-tanda (kebesaran)-Nya ialah Dia menciptakan kamu dari tanah, kemudian tiba-tiba kamu (menjadi) manusia yang berkembang biak.

30:21

# وَمِنْ اٰيٰتِهٖٓ اَنْ خَلَقَ لَكُمْ مِّنْ اَنْفُسِكُمْ اَزْوَاجًا لِّتَسْكُنُوْٓا اِلَيْهَا وَجَعَلَ بَيْنَكُمْ مَّوَدَّةً وَّرَحْمَةً ۗاِنَّ فِيْ ذٰلِكَ لَاٰيٰتٍ لِّقَوْمٍ يَّتَفَكَّرُوْنَ

wamin *aa*y*aa*tihi an khalaqa lakum min anfusikum azw*aa*jan litaskunuu ilayh*aa* waja'ala baynakum mawaddatan wara*h*matan inna fii *dzaa*lika la*aa*y*aa*tin liqawmin yatafakkaruun**a**

Dan di antara tanda-tanda (kebesaran)-Nya ialah Dia menciptakan pasangan-pasangan untukmu dari jenismu sendiri, agar kamu cenderung dan merasa tenteram kepadanya, dan Dia menjadikan di antaramu rasa kasih dan sayang. Sungguh, pada yang demikian itu benar-

30:22

# وَمِنْ اٰيٰتِهٖ خَلْقُ السَّمٰوٰتِ وَالْاَرْضِ وَاخْتِلَافُ اَلْسِنَتِكُمْ وَاَلْوَانِكُمْۗ اِنَّ فِيْ ذٰلِكَ لَاٰيٰتٍ لِّلْعٰلِمِيْنَ

wamin *aa*y*aa*tihi khalqu **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i wa**i**khtil*aa*fu **a**lsinatikum wa-alw*aa*nikum inna fii *dzaa*lika la*aa*y*aa*tin l

Dan di antara tanda-tanda (kebesaran)-Nya ialah penciptaan langit dan bumi, perbedaan bahasamu dan warna kulitmu. Sungguh, pada yang demikian itu benar-benar terdapat tanda-tanda bagi orang-orang yang mengetahui.

30:23

# وَمِنْ اٰيٰتِهٖ مَنَامُكُمْ بِالَّيْلِ وَالنَّهَارِ وَابْتِغَاۤؤُكُمْ مِّنْ فَضْلِهٖۗ اِنَّ فِيْ ذٰلِكَ لَاٰيٰتٍ لِّقَوْمٍ يَّسْمَعُوْنَ

wamin *aa*y*aa*tihi man*aa*mukum bi**a**llayli wa**al**nnah*aa*ri wa**i**btigh*aa*ukum min fa*dh*lihi inna fii *dzaa*lika la*aa*y*aa*tin liqawmin yasma'uun**a**

**Dan di antara tanda-tanda (kebesaran)-Nya ialah tidurmu pada waktu malam dan siang hari dan usahamu mencari sebagian dari karunia-Nya. Sungguh, pada yang demikian itu benar-benar terdapat tanda-tanda bagi kaum yang mendengarkan.**









30:24

# وَمِنْ اٰيٰتِهٖ يُرِيْكُمُ الْبَرْقَ خَوْفًا وَّطَمَعًا وَّيُنَزِّلُ مِنَ السَّمَاۤءِ مَاۤءً فَيُحْيٖ بِهِ الْاَرْضَ بَعْدَ مَوْتِهَاۗ اِنَّ فِيْ ذٰلِكَ لَاٰيٰتٍ لِّقَوْمٍ يَّعْقِلُوْنَ

wamin *aa*y*aa*tihi yuriikumu **a**lbarqa khawfan wa*th*ama'an wayunazzilu mina **al**ssam*aa*-i m*aa*-an fayu*h*yii bihi **a**l-ar*dh*a ba'da mawtih*aa* inna fii *dzaa*li

Dan di antara tanda-tanda (kebesaran)-Nya, Dia memperlihatkan kilat kepadamu untuk (menimbulkan) ketakutan dan harapan, dan Dia menurunkan air (hujan) dari langit, lalu dengan air itu dihidupkannya bumi setelah mati (kering). Sungguh, pada yang demikian i

30:25

# وَمِنْ اٰيٰتِهٖٓ اَنْ تَقُوْمَ السَّمَاۤءُ وَالْاَرْضُ بِاَمْرِهٖۗ ثُمَّ اِذَا دَعَاكُمْ دَعْوَةًۖ مِّنَ الْاَرْضِ اِذَآ اَنْتُمْ تَخْرُجُوْنَ

wamin *aa*y*aa*tihi an taquuma **al**ssam*aa*u wa**a**l-ar*dh*u bi-amrihi tsumma i*dzaa* da'*aa*kum da'watan mina **a**l-ar*dh*i i*dzaa* antum takhrujuun**a**

Dan di antara tanda-tanda (kebesaran)-Nya ialah berdirinya langit dan bumi dengan kehendak-Nya. Kemudian apabila Dia memanggil kamu sekali panggil dari bumi, seketika itu kamu keluar (dari kubur).

30:26

# وَلَهٗ مَنْ فِى السَّمٰوٰتِ وَالْاَرْضِۗ كُلٌّ لَّهٗ قَانِتُوْنَ

walahu man fii **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i kullun lahu q*aa*nituun**a**

Dan milik-Nya apa yang di langit dan di bumi. Semuanya hanya kepada-Nya tunduk.

30:27

# وَهُوَ الَّذِيْ يَبْدَؤُا الْخَلْقَ ثُمَّ يُعِيْدُهٗ وَهُوَ اَهْوَنُ عَلَيْهِۗ وَلَهُ الْمَثَلُ الْاَعْلٰى فِى السَّمٰوٰتِ وَالْاَرْضِۚ وَهُوَ الْعَزِيْزُ الْحَكِيْمُ ࣖ

wahuwa **al**la*dz*ii yabdau **a**lkhalqa tsumma yu'iiduhu wahuwa ahwanu 'alayhi walahu **a**lmatsalu **a**l-a'l*aa* fii **al**ssam*aa*w*aa*ti wa**a**l-ar

Dan Dialah yang memulai penciptaan, kemudian mengulanginya kembali, dan itu lebih mudah bagi-Nya. Dia memiliki sifat yang Mahatinggi di langit dan di bumi. Dan Dialah Yang Mahaperkasa, Mahabijaksana.







30:28

# ضَرَبَ لَكُمْ مَّثَلًا مِّنْ اَنْفُسِكُمْۗ هَلْ لَّكُمْ مِّنْ مَّا مَلَكَتْ اَيْمَانُكُمْ مِّنْ شُرَكَاۤءَ فِيْ مَا رَزَقْنٰكُمْ فَاَنْتُمْ فِيْهِ سَوَاۤءٌ تَخَافُوْنَهُمْ كَخِيْفَتِكُمْ اَنْفُسَكُمْۗ كَذٰلِكَ نُفَصِّلُ الْاٰيٰتِ لِقَوْمٍ يَّعْقِلُوْنَ

*dh*araba lakum matsalan min anfusikum hal lakum mimm*aa* malakat aym*aa*nukum min syurak*aa*-a fii m*aa* razaqn*aa*kum fa-antum fiihi saw*aa*un takh*aa*fuunahum kakhiifatikum anfusakum ka*dzaa*lika nufa*shsh*

Dia membuat perumpamaan bagimu dari dirimu sendiri. Apakah (kamu rela jika) ada di antara hamba sahaya yang kamu miliki, menjadi sekutu bagimu dalam (memiliki) rezeki yang telah Kami berikan kepadamu, sehingga kamu menjadi setara dengan mereka dalam hal i







30:29

# بَلِ اتَّبَعَ الَّذِيْنَ ظَلَمُوْٓا اَهْوَاۤءَهُمْ بِغَيْرِ عِلْمٍۗ فَمَنْ يَّهْدِيْ مَنْ اَضَلَّ اللّٰهُ ۗوَمَا لَهُمْ مِّنْ نّٰصِرِيْنَ

bali ittaba'a **al**la*dz*iina *zh*alamuu ahw*aa*-ahum bighayri 'ilmin faman yahdii man a*dh*alla **al**l*aa*hu wam*aa* lahum min n*aas*iriin**a**

Tetapi orang-orang yang zalim, mengikuti keinginannya tanpa ilmu pengetahuan; maka siapakah yang dapat memberi petunjuk kepada orang yang telah disesatkan Allah. Dan tidak ada seorang penolong pun bagi mereka.

30:30

# فَاَقِمْ وَجْهَكَ لِلدِّيْنِ حَنِيْفًاۗ فِطْرَتَ اللّٰهِ الَّتِيْ فَطَرَ النَّاسَ عَلَيْهَاۗ لَا تَبْدِيْلَ لِخَلْقِ اللّٰهِ ۗذٰلِكَ الدِّيْنُ الْقَيِّمُۙ وَلٰكِنَّ اَكْثَرَ النَّاسِ لَا يَعْلَمُوْنَۙ

fa-aqim wajhaka li**l**ddiini *h*aniifan fi*th*rata **al**l*aa*hi **al**latii fa*th*ara **al**nn*aa*sa 'alayh*aa* l*aa* tabdiila likhalqi **al**l*aa*h

Maka hadapkanlah wajahmu dengan lurus kepada agama (Islam); (sesuai) fitrah Allah disebabkan Dia telah menciptakan manusia menurut (fitrah) itu. Tidak ada perubahan pada ciptaan Allah. (Itulah) agama yang lurus, tetapi kebanyakan manusia tidak mengetahui,

30:31

# ۞ مُنِيْبِيْنَ اِلَيْهِ وَاتَّقُوْهُ وَاَقِيْمُوا الصَّلٰوةَ وَلَا تَكُوْنُوْا مِنَ الْمُشْرِكِيْنَۙ

muniibiina ilayhi wa**i**ttaquuhu wa-aqiimuu **al***shsh*al*aa*ta wal*aa* takuunuu mina **a**lmusyrikiin**a**

dengan kembali bertobat kepada-Nya dan bertakwalah kepada-Nya serta laksanakanlah salat dan janganlah kamu termasuk orang-orang yang mempersekutukan Allah,

30:32

# مِنَ الَّذِيْنَ فَرَّقُوْا دِيْنَهُمْ وَكَانُوْا شِيَعًا ۗ كُلُّ حِزْبٍۢ بِمَا لَدَيْهِمْ فَرِحُوْنَ

mina **al**la*dz*iina farraquu diinahum wak*aa*nuu syiya'an kullu *h*izbin bim*aa* ladayhim fari*h*uun**a**

yaitu orang-orang yang memecah belah agama mereka dan mereka menjadi beberapa golongan. Setiap golongan merasa bangga dengan apa yang ada pada golongan mereka.

30:33

# وَاِذَا مَسَّ النَّاسَ ضُرٌّ دَعَوْا رَبَّهُمْ مُّنِيْبِيْنَ اِلَيْهِ ثُمَّ اِذَآ اَذَاقَهُمْ مِّنْهُ رَحْمَةً اِذَا فَرِيْقٌ مِّنْهُمْ بِرَبِّهِمْ يُشْرِكُوْنَۙ

wa-i*dzaa* massa **al**nn*aa*sa *dh*urrun da'aw rabbahum muniibiina ilayhi tsumma i*dzaa* a*dzaa*qahum minhu ra*h*matan i*dzaa* fariiqun minhum birabbihim yusyrikuun**a**

Dan apabila manusia ditimpa oleh suatu bahaya, mereka menyeru Tuhannya dengan kembali (bertobat) kepada-Nya, kemudian apabila Dia memberikan sedikit rahmat-Nya kepada mereka, tiba-tiba sebagian mereka mempersekutukan Allah.

30:34

# لِيَكْفُرُوْا بِمَآ اٰتَيْنٰهُمْۗ فَتَمَتَّعُوْاۗ فَسَوْفَ تَعْلَمُوْنَ

liyakfuruu bim*aa* *aa*tayn*aa*hum fatamatta'uu fasawfa ta'lamuun**a**

Biarkan mereka mengingkari rahmat yang telah Kami berikan. Dan bersenang-senanglah kamu, maka kelak kamu akan mengetahui (akibat perbuatanmu).

30:35

# اَمْ اَنْزَلْنَا عَلَيْهِمْ سُلْطٰنًا فَهُوَ يَتَكَلَّمُ بِمَا كَانُوْا بِهٖ يُشْرِكُوْنَ

am anzaln*aa* 'alayhim sul*thaa*nan fahuwa yatakallamu bim*aa* k*aa*nuu bihi yusyrikuun**a**

Atau pernahkah Kami menurunkan kepada mereka keterangan, yang menjelaskan (membenarkan) apa yang (selalu) mereka persekutukan dengan Tuhan?

30:36

# وَاِذَآ اَذَقْنَا النَّاسَ رَحْمَةً فَرِحُوْا بِهَاۗ وَاِنْ تُصِبْهُمْ سَيِّئَةٌ ۢبِمَا قَدَّمَتْ اَيْدِيْهِمْ اِذَا هُمْ يَقْنَطُوْنَ

wa**i***dzaa* a*dz*aqn*aa* **al**nn*aa*sa ra*h*matan fari*h*uu bih*aa* wa-in tu*sh*ibhum sayyi-atun bim*aa* qaddamat aydiihim i*dzaa* hum yaqna*th*uun**a**

Dan apabila Kami berikan sesuatu rahmat kepada manusia, niscaya mereka gembira dengan (rahmat) itu. Tetapi apabila mereka ditimpa sesuatu musibah (bahaya) karena kesalahan mereka sendiri, seketika itu mereka berputus asa.

30:37

# اَوَلَمْ يَرَوْا اَنَّ اللّٰهَ يَبْسُطُ الرِّزْقَ لِمَنْ يَّشَاۤءُ وَيَقْدِرُۗ اِنَّ فِيْ ذٰلِكَ لَاٰيٰتٍ لِّقَوْمٍ يُّؤْمِنُوْنَ

awa lam yaraw anna **al**l*aa*ha yabsu*th*u **al**rrizqa liman yasy*aa*u wayaqdiru inna fii *dzaa*lika la*aa*y*aa*tin liqawmin yu/minuun**a**

Dan tidakkah mereka memperhatikan bahwa Allah yang melapangkan rezeki bagi siapa yang Dia kehendaki dan Dia (pula) yang membatasi (bagi siapa yang Dia kehendaki). Sungguh, pada yang demikian itu benar-benar terdapat tanda-tanda (kebesaran Allah) bagi kaum

30:38

# فَاٰتِ ذَا الْقُرْبٰى حَقَّهٗ وَالْمِسْكِيْنَ وَابْنَ السَّبِيْلِۗ ذٰلِكَ خَيْرٌ لِّلَّذِيْنَ يُرِيْدُوْنَ وَجْهَ اللّٰهِ ۖوَاُولٰۤىِٕكَ هُمُ الْمُفْلِحُوْنَ

fa*aa*ti *dzaa* **a**lqurb*aa* *h*aqqahu wa**a**lmiskiina wa**i**bna **al**ssabiili *dzaa*lika khayrun lilla*dz*iina yuriiduuna wajha **al**l*aa*hi waul

Maka berikanlah haknya kepada kerabat dekat, juga kepada orang miskin dan orang-orang yang dalam perjalanan. Itulah yang lebih baik bagi orang-orang yang mencari keridaan Allah. Dan mereka itulah orang-orang yang beruntung.







30:39

# وَمَآ اٰتَيْتُمْ مِّنْ رِّبًا لِّيَرْبُوَا۠ فِيْٓ اَمْوَالِ النَّاسِ فَلَا يَرْبُوْا عِنْدَ اللّٰهِ ۚوَمَآ اٰتَيْتُمْ مِّنْ زَكٰوةٍ تُرِيْدُوْنَ وَجْهَ اللّٰهِ فَاُولٰۤىِٕكَ هُمُ الْمُضْعِفُوْنَ

wam*aa* *aa*taytum min riban liyarbuwa fii amw*aa*li **al**nn*aa*si fal*aa* yarbuu 'inda **al**l*aa*hi wam*aa* *aa*taytum min zak*aa*tin turiiduuna wajha **al**l*aa*h

Dan sesuatu riba (tambahan) yang kamu berikan agar harta manusia bertambah, maka tidak bertambah dalam pandangan Allah. Dan apa yang kamu berikan berupa zakat yang kamu maksudkan untuk memperoleh keridaan Allah, maka itulah orang-orang yang melipatgandaka

30:40

# اَللّٰهُ الَّذِيْ خَلَقَكُمْ ثُمَّ رَزَقَكُمْ ثُمَّ يُمِيْتُكُمْ ثُمَّ يُحْيِيْكُمْۗ هَلْ مِنْ شُرَكَاۤىِٕكُمْ مَّنْ يَّفْعَلُ مِنْ ذٰلِكُمْ مِّنْ شَيْءٍۗ سُبْحٰنَهٗ وَتَعٰلٰى عَمَّا يُشْرِكُوْنَ ࣖ

**al**l*aa*hu **al**la*dz*ii khalaqakum tsumma razaqakum tsumma yumiitukum tsumma yu*h*yiikum hal min syurak*aa*-ikum man yaf'alu min *dzaa*likum min syay-in sub*haa*nahu wata'*aa*l*aa* 'am

Allah yang menciptakan kamu, kemudian memberimu rezeki, lalu mematikanmu, kemudian menghidupkanmu (kembali). Adakah di antara mereka yang kamu sekutukan dengan Allah itu yang dapat berbuat sesuatu yang demikian itu? Mahasuci Dia dan Mahatinggi dari apa ya

30:41

# ظَهَرَ الْفَسَادُ فِى الْبَرِّ وَالْبَحْرِ بِمَا كَسَبَتْ اَيْدِى النَّاسِ لِيُذِيْقَهُمْ بَعْضَ الَّذِيْ عَمِلُوْا لَعَلَّهُمْ يَرْجِعُوْنَ

*zh*ahara **a**lfas*aa*du fii **a**lbarri wa**a**lba*h*ri bim*aa* kasabat aydii **al**nn*aa*si liyu*dz*iiqahum ba'*dh*a **al**la*dz*ii 'amiluu la'alla

Telah tampak kerusakan di darat dan di laut disebabkan karena perbuatan tangan manusia; Allah menghendaki agar mereka merasakan sebagian dari (akibat) perbuatan mereka, agar mereka kembali (ke jalan yang benar).

30:42

# قُلْ سِيْرُوْا فِى الْاَرْضِ فَانْظُرُوْا كَيْفَ كَانَ عَاقِبَةُ الَّذِيْنَ مِنْ قَبْلُۗ كَانَ اَكْثَرُهُمْ مُّشْرِكِيْنَ

qul siiruu fii **a**l-ar*dh*i fa**u**n*zh*uruu kayfa k*aa*na '*aa*qibatu **al**la*dz*iina min qablu k*aa*na aktsaruhum musyrikiin**a**

Katakanlah (Muhammad), “Bepergianlah di bumi lalu lihatlah bagaimana kesudahan orang-orang dahulu. Kebanyakan dari mereka adalah orang-orang yang mempersekutukan (Allah).”

30:43

# فَاَقِمْ وَجْهَكَ لِلدِّيْنِ الْقَيِّمِ مِنْ قَبْلِ اَنْ يَّأْتِيَ يَوْمٌ لَّا مَرَدَّ لَهٗ مِنَ اللّٰهِ يَوْمَىِٕذٍ يَّصَّدَّعُوْنَ

fa-aqim wajhaka li**l**ddiini **a**lqayyimi min qabli an ya/tiya yawmun l*aa* maradda lahu mina **al**l*aa*hi yawma-i*dz*in ya*shsh*adda'uun**a**

Oleh karena itu, hadapkanlah wajahmu kepada agama yang lurus (Islam) sebelum datang dari Allah suatu hari (Kiamat) yang tidak dapat ditolak, pada hari itu mereka terpisah-pisah.

30:44

# مَنْ كَفَرَ فَعَلَيْهِ كُفْرُهٗۚ وَمَنْ عَمِلَ صَالِحًا فَلِاَنْفُسِهِمْ يَمْهَدُوْنَۙ

man kafara fa'alayhi kufruhu waman 'amila *shaa*li*h*an fali-anfusihim yamhaduun**a**

Barangsiapa kafir maka dia sendirilah yang menanggung (akibat) kekafirannya itu; dan barangsiapa mengerjakan kebajikan maka mereka menyiapkan untuk diri mereka sendiri (tempat yang menyenangkan),

30:45

# لِيَجْزِيَ الَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِ مِنْ فَضْلِهٖۗ اِنَّهٗ لَا يُحِبُّ الْكٰفِرِيْنَ

liyajziya **al**la*dz*iina *aa*manuu wa'amiluu **al***shshaa*li*haa*ti min fa*dh*lihi innahu l*aa* yu*h*ibbu **a**lk*aa*firiin**a**

agar Allah memberi balasan (pahala) kepada orang-orang yang beriman dan mengerjakan kebajikan dari karunia-Nya. Sungguh, Dia tidak menyukai orang-orang yang ingkar (kafir).

30:46

# وَمِنْ اٰيٰتِهٖٓ اَنْ يُّرْسِلَ الرِّيٰحَ مُبَشِّرٰتٍ وَّلِيُذِيْقَكُمْ مِّنْ رَّحْمَتِهٖ وَلِتَجْرِيَ الْفُلْكُ بِاَمْرِهٖ وَلِتَبْتَغُوْا مِنْ فَضْلِهٖ وَلَعَلَّكُمْ تَشْكُرُوْنَ

wamin *aa*y*aa*tihi an yursila **al**rriy*aah*a mubasysyir*aa*tin waliyu*dz*iiqakum min ra*h*matihi walitajriya **a**lfulku bi-amrihi walitabtaghuu min fa*dh*lihi wala'allakum tasykuruun

Dan di antara tanda-tanda (kebesaran)-Nya adalah bahwa Dia mengirimkan angin sebagai pembawa berita gembira dan agar kamu merasakan sebagian dari rahmat-Nya dan agar kapal dapat berlayar dengan perintah-Nya dan (juga) agar kamu dapat mencari sebagian dari







30:47

# وَلَقَدْ اَرْسَلْنَا مِنْ قَبْلِكَ رُسُلًا اِلٰى قَوْمِهِمْ فَجَاۤءُوْهُمْ بِالْبَيِّنٰتِ فَانْتَقَمْنَا مِنَ الَّذِيْنَ اَجْرَمُوْاۗ وَكَانَ حَقًّاۖ عَلَيْنَا نَصْرُ الْمُؤْمِنِيْنَ

walaqad arsaln*aa* min qablika rusulan il*aa* qawmihim faj*aa*uuhum bi**a**lbayyin*aa*ti fa**i**ntaqamn*aa* mina **al**la*dz*iina ajramuu wak*aa*na *h*aqqan 'alayn*aa* na<

Dan sungguh, Kami telah mengutus sebelum engkau (Muhammad) beberapa orang rasul kepada kaumnya, mereka datang kepadanya dengan membawa keterangan-keterangan (yang cukup), lalu Kami melakukan pembalasan terhadap orang-orang yang berdosa. Dan merupakan hak

30:48

# اَللّٰهُ الَّذِيْ يُرْسِلُ الرِّيٰحَ فَتُثِيْرُ سَحَابًا فَيَبْسُطُهٗ فِى السَّمَاۤءِ كَيْفَ يَشَاۤءُ وَيَجْعَلُهٗ كِسَفًا فَتَرَى الْوَدْقَ يَخْرُجُ مِنْ خِلٰلِهٖۚ فَاِذَآ اَصَابَ بِهٖ مَنْ يَّشَاۤءُ مِنْ عِبَادِهٖٓ اِذَا هُمْ يَسْتَبْشِرُوْنَۚ

**al**l*aa*hu **al**la*dz*ii yursilu **al**rriy*aah*a fatutsiiru sa*haa*ban fayabsu*th*uhu fii **al**ssam*aa*-i kayfa yasy*aa*u wayaj'aluhu kisafan fatar*aa*

Allah-lah yang mengirimkan angin, lalu angin itu menggerakkan awan dan Allah membentangkannya di langit menurut yang Dia kehendaki, dan menjadikannya bergumpal-gumpal, lalu engkau lihat hujan keluar dari celah-celahnya, maka apabila Dia menurunkannya kepa

30:49

# وَاِنْ كَانُوْا مِنْ قَبْلِ اَنْ يُّنَزَّلَ عَلَيْهِمْ مِّنْ قَبْلِهٖ لَمُبْلِسِيْنَۚ

wa-in k*aa*nuu min qabli an yunazzala 'alayhim min qablihi lamublisiin**a**

Padahal walaupun sebelum hujan diturunkan kepada mereka, mereka benar-benar telah berputus asa.

30:50

# فَانْظُرْ اِلٰٓى اٰثٰرِ رَحْمَتِ اللّٰهِ كَيْفَ يُحْيِ الْاَرْضَ بَعْدَ مَوْتِهَاۗ اِنَّ ذٰلِكَ لَمُحْيِ الْمَوْتٰىۚ وَهُوَ عَلٰى كُلِّ شَيْءٍ قَدِيْرٌ

fa**u**n*zh*ur il*aa* *aa*ts*aa*ri ra*h*mati **al**l*aa*hi kayfa yu*h*yii **a**l-ar*dh*a ba'da mawtih*aa* inna *dzaa*lika lamu*h*yii **a**lmawt*aa*

Maka perhatikanlah bekas-bekas rahmat Allah, bagaimana Allah menghidupkan bumi setelah mati (kering). Sungguh, itu berarti Dia pasti (berkuasa) menghidupkan yang telah mati. Dan Dia Mahakuasa atas segala sesuatu.







30:51

# وَلَىِٕنْ اَرْسَلْنَا رِيْحًا فَرَاَوْهُ مُصْفَرًّا لَّظَلُّوْا مِنْۢ بَعْدِهٖ يَكْفُرُوْنَ

wala-in arsaln*aa* rii*h*an fara-awhu mu*sh*farran la*zh*alluu min ba'dihi yakfuruun**a**

Dan sungguh, jika Kami mengirimkan angin lalu mereka melihat (tumbuh-tumbuhan itu) menjadi kuning (kering), niscaya setelah itu mereka tetap ingkar.

30:52

# فَاِنَّكَ لَا تُسْمِعُ الْمَوْتٰى وَلَا تُسْمِعُ الصُّمَّ الدُّعَاۤءَ اِذَا وَلَّوْا مُدْبِرِيْنَ

fa-innaka l*aa* tusmi'u **a**lmawt*aa* wal*aa* tusmi'u **al***shsh*umma **al**ddu'*aa*-a i*dzaa* wallaw mudbiriin**a**

Maka sungguh, engkau tidak akan sanggup menjadikan orang-orang yang mati itu dapat mendengar, dan menjadikan orang-orang yang tuli dapat mendengar seruan, apabila mereka berpaling ke belakang.

30:53

# وَمَآ اَنْتَ بِهٰدِ الْعُمْيِ عَنْ ضَلٰلَتِهِمْۗ اِنْ تُسْمِعُ اِلَّا مَنْ يُّؤْمِنُ بِاٰيٰتِنَا فَهُمْ مُّسْلِمُوْنَ ࣖ

wam*aa* anta bih*aa*di **a**l'umyi 'an *dh*al*aa*latihim in tusmi'u ill*aa* man yu/minu bi-*aa*y*aa*tin*aa* fahum muslimuun**a**

Dan engkau tidak akan dapat memberi petunjuk kepada orang-orang yang buta (mata hatinya) dari kesesatannya. Dan engkau tidak dapat memperdengarkan (petunjuk Tuhan) kecuali kepada orang-orang yang beriman dengan ayat-ayat Kami, maka mereka itulah orang-ora

30:54

# ۞ اَللّٰهُ الَّذِيْ خَلَقَكُمْ مِّنْ ضَعْفٍ ثُمَّ جَعَلَ مِنْۢ بَعْدِ ضَعْفٍ قُوَّةً ثُمَّ جَعَلَ مِنْۢ بَعْدِ قُوَّةٍ ضَعْفًا وَّشَيْبَةً ۗيَخْلُقُ مَا يَشَاۤءُۚ وَهُوَ الْعَلِيْمُ الْقَدِيْرُ

**al**l*aa*hu **al**la*dz*ii khalaqakum min *dh*a'fin tsumma ja'ala min ba'di *dh*a'fin quwwatan tsumma ja'ala min ba'di quwwatin *dh*a'fan wasyaybatan yakhluqu m*aa* yasy*aa*u wahuwa **a<**

Allah-lah yang menciptakan kamu dari keadaan lemah, kemudian Dia menjadikan (kamu) setelah keadaan lemah itu menjadi kuat, kemudian Dia menjadikan (kamu) setelah kuat itu lemah (kembali) dan beruban. Dia menciptakan apa yang Dia kehendaki. Dan Dia Maha Me







30:55

# وَيَوْمَ تَقُوْمُ السَّاعَةُ يُقْسِمُ الْمُجْرِمُوْنَ ەۙ مَا لَبِثُوْا غَيْرَ سَاعَةٍ ۗ كَذٰلِكَ كَانُوْا يُؤْفَكُوْنَ

wayawma taquumu **al**ss*aa*'atu yuqsimu **a**lmujrimuuna m*aa* labitsuu ghayra s*aa*'atin ka*dzaa*lika k*aa*nuu yu/fakuun**a**

Dan pada hari (ketika) terjadinya Kiamat, orang-orang yang berdosa bersumpah, bahwa mereka berdiam (dalam kubur) hanya sesaat (saja). Begitulah dahulu mereka dipalingkan (dari kebenaran).

30:56

# وَقَالَ الَّذِيْنَ اُوْتُوا الْعِلْمَ وَالْاِيْمَانَ لَقَدْ لَبِثْتُمْ فِيْ كِتٰبِ اللّٰهِ اِلٰى يَوْمِ الْبَعْثِۖ فَهٰذَا يَوْمُ الْبَعْثِ وَلٰكِنَّكُمْ كُنْتُمْ لَا تَعْلَمُوْنَ

waq*aa*la **al**la*dz*iina uutuu **a**l'ilma wa**a**l-iim*aa*na laqad labitstum fii kit*aa*bi **al**l*aa*hi il*aa* yawmi **a**lba'tsi fah*aadzaa* yawmu

Dan orang-orang yang diberi ilmu dan keimanan berkata (kepada orang-orang kafir), “Sungguh, kamu telah berdiam (dalam kubur) menurut ketetapan Allah, sampai hari kebangkitan. Maka inilah hari kebangkitan itu, tetapi (dahulu) kamu tidak meyakini(nya).”

30:57

# فَيَوْمَىِٕذٍ لَّا يَنْفَعُ الَّذِيْنَ ظَلَمُوْا مَعْذِرَتُهُمْ وَلَا هُمْ يُسْتَعْتَبُوْنَ

fayawma-i*dz*in l*aa* yanfa'u **al**la*dz*iina *zh*alamuu ma'*dz*iratuhum wal*aa* hum yusta'tabuun**a**

Maka pada hari itu tidak bermanfaat (lagi) permintaan maaf orang-orang yang zalim, dan mereka tidak pula diberi kesempatan bertobat lagi.

30:58

# وَلَقَدْ ضَرَبْنَا لِلنَّاسِ فِيْ هٰذَا الْقُرْاٰنِ مِنْ كُلِّ مَثَلٍۗ وَلَىِٕنْ جِئْتَهُمْ بِاٰيَةٍ لَّيَقُوْلَنَّ الَّذِيْنَ كَفَرُوْٓا اِنْ اَنْتُمْ اِلَّا مُبْطِلُوْنَ

walaqad *dh*arabn*aa* li**l**nn*aa*si fii h*aadzaa* **a**lqur-*aa*ni min kulli matsalin wala-in ji/tahum bi-*aa*yatin layaquulanna **al**la*dz*iina kafaruu in antum ill*aa* mub

Dan sesungguhnya telah Kami jelaskan kepada manusia segala macam perumpamaan dalam Al-Qur'an ini. Dan jika engkau membawa suatu ayat kepada mereka, pastilah orang-orang kafir itu akan berkata, ”Kamu hanyalah orang-orang yang membuat kepalsuan belaka.”

30:59

# كَذٰلِكَ يَطْبَعُ اللّٰهُ عَلٰى قُلُوْبِ الَّذِيْنَ لَا يَعْلَمُوْنَ

ka*dzaa*lika ya*th*ba'u **al**l*aa*hu 'al*aa* quluubi **al**la*dz*iina l*aa* ya'lamuun**a**

Demikianlah Allah mengunci hati orang-orang yang tidak (mau) memahami.

30:60

# فَاصْبِرْ اِنَّ وَعْدَ اللّٰهِ حَقٌّ وَّلَا يَسْتَخِفَّنَّكَ الَّذِيْنَ لَا يُوْقِنُوْنَ ࣖ

fa**i***sh*bir inna wa'da **al**l*aa*hi *h*aqqun wal*aa* yastakhiffannaka **al**la*dz*iina l*aa* yuuqinuun**a**

Maka bersabarlah engkau (Muhammad), sungguh, janji Allah itu benar dan sekali-kali jangan sampai orang-orang yang tidak meyakini (kebenaran ayat-ayat Allah) itu menggelisahkan engkau.

<!--EndFragment-->