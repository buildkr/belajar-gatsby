---
title: (103) Al-'Asr - العصر
date: 2021-10-27T04:16:09.088Z
ayat: 103
description: "Jumlah Ayat: 3 / Arti: Asar"
---
<!--StartFragment-->

103:1

# وَالْعَصْرِۙ

wa**a**l'a*sh*r**i**

Demi masa,

103:2

# اِنَّ الْاِنْسَانَ لَفِيْ خُسْرٍۙ

inna **a**l-ins*aa*na lafii khusr**in**

sungguh, manusia berada dalam kerugian,

103:3

# اِلَّا الَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِ وَتَوَاصَوْا بِالْحَقِّ ەۙ وَتَوَاصَوْا بِالصَّبْرِ ࣖ

ill*aa* **al**la*dz*iina *aa*manuu wa'amiluu **al***shshaa*li*haa*ti wataw*aas*aw bi**a**l*h*aqqi wataw*aas*aw bi**al***shsh*abr**i**

kecuali orang-orang yang beriman dan mengerjakan kebajikan serta saling menasihati untuk kebenaran dan saling menasihati untuk kesabaran.

<!--EndFragment-->