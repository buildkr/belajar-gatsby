---
title: (45) Al-Jasiyah - الجاثية
date: 2021-10-27T04:06:22.471Z
ayat: 45
description: "Jumlah Ayat: 37 / Arti: Berlutut"
---
<!--StartFragment-->

45:1

# حٰمۤ

*haa*-miim

Ha Mim

45:2

# تَنْزِيْلُ الْكِتٰبِ مِنَ اللّٰهِ الْعَزِيْزِ الْحَكِيْمِ

tanziilu **a**lkit*aa*bi mina **al**l*aa*hi **a**l'aziizi **a**l*h*akiim**i**

Kitab (ini) diturunkan dari Allah Yang Mahaperkasa, Mahabijaksana.

45:3

# اِنَّ فِى السَّمٰوٰتِ وَالْاَرْضِ لَاٰيٰتٍ لِّلْمُؤْمِنِيْنَۗ

inna fii **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i la*aa*y*aa*tin lilmu/miniin**a**

Sungguh, pada langit dan bumi benar-benar terdapat tanda-tanda (kebesaran Allah) bagi orang-orang mukmin.

45:4

# وَفِيْ خَلْقِكُمْ وَمَا يَبُثُّ مِنْ دَاۤبَّةٍ اٰيٰتٌ لِّقَوْمٍ يُّوْقِنُوْنَۙ

wafii khalqikum wam*aa* yabutstsu min d*aa*bbatin *aa*y*aa*tun liqawmin yuuqinuun**a**

Dan pada penciptaan dirimu dan pada makhluk bergerak yang bernyawa yang bertebaran (di bumi) terdapat tanda-tanda (kebesaran Allah) untuk kaum yang meyakini,

45:5

# وَاخْتِلَافِ الَّيْلِ وَالنَّهَارِ وَمَآ اَنْزَلَ اللّٰهُ مِنَ السَّمَاۤءِ مِنْ رِّزْقٍ فَاَحْيَا بِهِ الْاَرْضَ بَعْدَ مَوْتِهَا وَتَصْرِيْفِ الرِّيٰحِ اٰيٰتٌ لِّقَوْمٍ يَّعْقِلُوْنَ

wa**i**khtil*aa*fi **al**layli wa**al**nnah*aa*ri wam*aa* anzala **al**l*aa*hu mina **al**ssam*aa*-i min rizqin fa-a*h*y*aa* bihi **a**l-ar

dan pada pergantian malam dan siang dan hujan yang diturunkan Allah dari langit lalu dengan (air hujan) itu dihidupkan-Nya bumi setelah mati (kering); dan pada perkisaran angin terdapat pula tanda-tanda (kebesaran Allah) bagi kaum yang mengerti.







45:6

# تِلْكَ اٰيٰتُ اللّٰهِ نَتْلُوْهَا عَلَيْكَ بِالْحَقِّۚ فَبِاَيِّ حَدِيْثٍۢ بَعْدَ اللّٰهِ وَاٰيٰتِهٖ يُؤْمِنُوْنَ

tilka *aa*y*aa*tu **al**l*aa*hi natluuh*aa* 'alayka bi**a**l*h*aqqi fabi-ayyi *h*adiitsin ba'da **al**l*aa*hi wa*aa*y*aa*tihi yu/minuun**a**

Itulah ayat-ayat Allah yang Kami bacakan kepadamu dengan sebenarnya; maka dengan perkataan mana lagi mereka akan beriman setelah Allah dan ayat-ayat-Nya.

45:7

# وَيْلٌ لِّكُلِّ اَفَّاكٍ اَثِيْمٍۙ

waylun likulli aff*aa*kin atsiim**in**

Celakalah bagi setiap orang yang banyak berdusta lagi banyak berdosa,

45:8

# يَّسْمَعُ اٰيٰتِ اللّٰهِ تُتْلٰى عَلَيْهِ ثُمَّ يُصِرُّ مُسْتَكْبِرًا كَاَنْ لَّمْ يَسْمَعْهَاۚ فَبَشِّرْهُ بِعَذَابٍ اَلِيْمٍ

yasma'u *aa*y*aa*ti **al**l*aa*hi tutl*aa* 'alayhi tsumma yu*sh*irru mustakbiran ka-an lam yasma'h*aa* fabasysyirhu bi'a*dzaa*bin **a**liim**in**

(yaitu) orang yang mendengar ayat-ayat Allah ketika dibacakan kepadanya namun dia tetap menyombongkan diri seakan-akan dia tidak mendengarnya. Maka peringatkanlah dia dengan azab yang pedih.

45:9

# وَاِذَا عَلِمَ مِنْ اٰيٰتِنَا شَيْـًٔا ۨاتَّخَذَهَا هُزُوًاۗ اُولٰۤىِٕكَ لَهُمْ عَذَابٌ مُّهِيْنٌۗ

wa-i*dzaa* 'alima min *aa*y*aa*tin*aa* syay-an ittakha*dz*ah*aa* huzuwan ul*aa*-ika lahum 'a*dzaa*bun muhiin**un**

Dan apabila dia mengetahui sedikit tentang ayat-ayat Kami, maka (ayat-ayat itu) dijadikan olok-olok. Merekalah yang akan menerima azab yang menghinakan.

45:10

# مِنْ وَّرَاۤىِٕهِمْ جَهَنَّمُ ۚوَلَا يُغْنِيْ عَنْهُمْ مَّا كَسَبُوْا شَيْـًٔا وَّلَا مَا اتَّخَذُوْا مِنْ دُوْنِ اللّٰهِ اَوْلِيَاۤءَۚ وَلَهُمْ عَذَابٌ عَظِيْمٌۗ

min war*aa*-ihim jahannamu wal*aa* yughnii 'anhum m*aa* kasabuu syay-an wal*aa* m*aa* ittakha*dz*uu min duuni **al**l*aa*hi awliy*aa*-a walahum 'a*dzaa*bun 'a*zh*iim**un**

Di hadapan mereka neraka Jahanam dan tidak akan berguna bagi mereka sedikit pun apa yang telah mereka kerjakan, dan tidak pula (bermanfaat) apa yang mereka jadikan sebagai pelindung-pelindung (mereka) selain Allah. Dan mereka akan mendapat azab yang besar

45:11

# هٰذَا هُدًىۚ وَالَّذِيْنَ كَفَرُوْا بِاٰيٰتِ رَبِّهِمْ لَهُمْ عَذَابٌ مِّنْ رِّجْزٍ اَلِيْمٌ ࣖ

h*aadzaa* hudan wa**a**lla*dz*iina kafaruu bi-*aa*y*aa*ti rabbihim lahum 'a*dzaa*bun min rijzin **a**liim**in**

Ini (Al-Qur'an) adalah petunjuk. Dan orang-orang yang mengingkari ayat-ayat Tuhannya mereka akan mendapat azab berupa siksaan yang sangat pedih.

45:12

# ۞ اَللّٰهُ الَّذِيْ سَخَّرَ لَكُمُ الْبَحْرَ لِتَجْرِيَ الْفُلْكُ فِيْهِ بِاَمْرِهٖ وَلِتَبْتَغُوْا مِنْ فَضْلِهٖ وَلَعَلَّكُمْ تَشْكُرُوْنَۚ

**al**l*aa*hu **al**la*dz*ii sakhkhara lakumu **a**lba*h*ra litajriya **a**lfulku fiihi bi-amrihi walitabtaghuu min fa*dh*lihi wala'allakum tasykuruun**a**

Allah-lah yang menundukkan laut untukmu agar kapal-kapal dapat berlayar di atasnya dengan perintah-Nya, dan agar kamu dapat mencari sebagian karunia-Nya dan agar kamu bersyukur.

45:13

# وَسَخَّرَ لَكُمْ مَّا فِى السَّمٰوٰتِ وَمَا فِى الْاَرْضِ جَمِيْعًا مِّنْهُ ۗاِنَّ فِيْ ذٰلِكَ لَاٰيٰتٍ لِّقَوْمٍ يَّتَفَكَّرُوْنَ

wasakhkhara lakum m*aa* fii **al**ssam*aa*w*aa*ti wam*aa* fii **a**l-ar*dh*i jamii'an minhu inna fii *dzaa*lika la*aa*y*aa*tin liqawmin yatafakkaruun**a**

Dan Dia menundukkan apa yang ada di langit dan apa yang ada di bumi untukmu semuanya (sebagai rahmat) dari-Nya. Sungguh, dalam hal yang demikian itu benar-benar terdapat tanda-tanda (kebesaran Allah) bagi orang-orang yang berpikir.

45:14

# قُلْ لِّلَّذِيْنَ اٰمَنُوْا يَغْفِرُوْا لِلَّذِيْنَ لَا يَرْجُوْنَ اَيَّامَ اللّٰهِ لِيَجْزِيَ قَوْمًا ۢبِمَا كَانُوْا يَكْسِبُوْنَ

qul lilla*dz*iina *aa*manuu yaghfiruu lilla*dz*iina l*aa* yarjuuna ayy*aa*ma **al**l*aa*hi liyajziya qawman bim*aa* k*aa*nuu yaksibuun**a**

Katakanlah (Muhammad) kepada orang-orang yang beriman hendaklah mereka memaafkan orang-orang yang tidak takut akan hari-hari Allah karena Dia akan membalas suatu kaum sesuai dengan apa yang telah mereka kerjakan.

45:15

# مَنْ عَمِلَ صَالِحًا فَلِنَفْسِهٖۚ وَمَنْ اَسَاۤءَ فَعَلَيْهَا ۖ ثُمَّ اِلٰى رَبِّكُمْ تُرْجَعُوْنَ

man 'amila *shaa*li*h*an falinafsihi waman as*aa*-a fa'alayh*aa* tsumma il*aa* rabbikum turja'uun**a**

Barangsiapa mengerjakan kebajikan maka itu adalah untuk dirinya sendiri, dan barangsiapa mengerjakan kejahatan, maka itu akan menimpa dirinya sendiri, kemudian kepada Tuhanmu kamu dikembalikan.

45:16

# وَلَقَدْ اٰتَيْنَا بَنِيْٓ اِسْرَاۤءِيْلَ الْكِتٰبَ وَالْحُكْمَ وَالنُّبُوَّةَ وَرَزَقْنٰهُمْ مِّنَ الطَّيِّبٰتِ وَفَضَّلْنٰهُمْ عَلَى الْعٰلَمِيْنَ ۚ

walaqad *aa*tayn*aa* banii isr*aa*-iila **a**lkit*aa*ba wa**a**l*h*ukma wa**al**nnubuwwata warazaqn*aa*hum mina **al***ththh*ayyib*aa*ti wafa*dhdh*aln*aa*

*Dan sungguh, kepada Bani Israil telah Kami berikan Kitab (Taurat), kekuasaan dan kenabian, Kami anugerahkan kepada mereka rezeki yang baik dan Kami lebihkan mereka atas bangsa-bangsa (pada masa itu).*









45:17

# وَاٰتَيْنٰهُمْ بَيِّنٰتٍ مِّنَ الْاَمْرِۚ فَمَا اخْتَلَفُوْٓا اِلَّا مِنْۢ بَعْدِ مَا جَاۤءَهُمُ الْعِلْمُ بَغْيًاۢ بَيْنَهُمْ ۗاِنَّ رَبَّكَ يَقْضِيْ بَيْنَهُمْ يَوْمَ الْقِيٰمَةِ فِيْمَا كَانُوْا فِيْهِ يَخْتَلِفُوْنَ

wa*aa*tayn*aa*hum bayyin*aa*tin mina **a**l-amri fam*aa* ikhtalafuu ill*aa* min ba'di m*aa* j*aa*-ahumu **a**l'ilmu baghyan baynahum inna rabbaka yaq*dh*ii baynahum yawma **a**

Dan Kami berikan kepada mereka keterangan-keterangan yang jelas tentang urusan (agama); maka mereka tidak berselisih kecuali setelah datang ilmu kepada mereka, karena kedengkian (yang ada) di antara mereka. Sungguh, Tuhanmu akan memberi putusan kepada mer

45:18

# ثُمَّ جَعَلْنٰكَ عَلٰى شَرِيْعَةٍ مِّنَ الْاَمْرِ فَاتَّبِعْهَا وَلَا تَتَّبِعْ اَهْوَاۤءَ الَّذِيْنَ لَا يَعْلَمُوْنَ

tsumma ja'aln*aa*ka 'al*aa* syarii'atin mina **a**l-amri fa**i**ttabi'h*aa* wal*aa* tattabi' ahw*aa*-a **al**la*dz*iina l*aa* ya'lamuun**a**

Kemudian Kami jadikan engkau (Muhammad) mengikuti syariat (peraturan) dari agama itu, maka ikutilah (syariat itu) dan janganlah engkau ikuti keinginan orang-orang yang tidak mengetahui.

45:19

# اِنَّهُمْ لَنْ يُّغْنُوْا عَنْكَ مِنَ اللّٰهِ شَيْـًٔا ۗوَاِنَّ الظّٰلِمِيْنَ بَعْضُهُمْ اَوْلِيَاۤءُ بَعْضٍۚ وَاللّٰهُ وَلِيُّ الْمُتَّقِيْنَ

innahum lan yughnuu 'anka mina **al**l*aa*hi syay-an wa-inna **al***zhzhaa*limiina ba'*dh*uhum awliy*aa*u ba'*dh*in wa**al**l*aa*hu waliyyu **a**lmuttaqiin**a**

Sungguh, mereka tidak akan dapat menghindarkan engkau sedikit pun dari (azab) Allah. Dan sungguh, orang-orang yang zalim itu sebagian menjadi pelindung atas sebagian yang lain, sedangkan Allah pelindung bagi orang-orang yang bertakwa.

45:20

# هٰذَا بَصَاۤىِٕرُ لِلنَّاسِ وَهُدًى وَّرَحْمَةٌ لِّقَوْمٍ يُّوْقِنُوْنَ

h*aadzaa* ba*shaa*-iru li**l**nn*aa*si wahudan wara*h*matun liqawmin yuuqinuun**a**

(Al-Qur'an) ini adalah pedoman bagi manusia, petunjuk dan rahmat bagi kaum yang meyakini.

45:21

# اَمْ حَسِبَ الَّذِيْنَ اجْتَرَحُوا السَّيِّاٰتِ اَنْ نَّجْعَلَهُمْ كَالَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِ سَوَاۤءً مَّحْيَاهُمْ وَمَمَاتُهُمْ ۗسَاۤءَ مَا يَحْكُمُوْنَ ࣖࣖ

am *h*asiba **al**la*dz*iina ijtara*h*uu **al**ssayyi-*aa*ti an naj'alahum ka**a**lla*dz*iina *aa*manuu wa'amiluu **al***shshaa*li*haa*ti saw*aa*-an ma*h*

*Apakah orang-orang yang melakukan kejahatan itu mengira bahwa Kami akan memperlakukan mereka seperti orang-orang yang beriman dan yang mengerjakan kebajikan, yaitu sama dalam kehidupan dan kematian mere-ka? Alangkah buruknya penilaian mereka itu.*









45:22

# وَخَلَقَ اللّٰهُ السَّمٰوٰتِ وَالْاَرْضَ بِالْحَقِّ وَلِتُجْزٰى كُلُّ نَفْسٍۢ بِمَا كَسَبَتْ وَهُمْ لَا يُظْلَمُوْنَ

wakhalaqa **al**l*aa*hu **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*a bi**a**l*h*aqqi walitujz*aa* kullu nafsin bim*aa* kasabat wahum l*aa* yu*zh*lamuun**a**

**Dan Allah menciptakan langit dan bumi dengan tujuan yang benar dan agar setiap jiwa diberi balasan sesuai dengan apa yang dikerjakannya, dan mereka tidak akan dirugikan.**









45:23

# اَفَرَءَيْتَ مَنِ اتَّخَذَ اِلٰهَهٗ هَوٰىهُ وَاَضَلَّهُ اللّٰهُ عَلٰى عِلْمٍ وَّخَتَمَ عَلٰى سَمْعِهٖ وَقَلْبِهٖ وَجَعَلَ عَلٰى بَصَرِهٖ غِشٰوَةًۗ فَمَنْ يَّهْدِيْهِ مِنْۢ بَعْدِ اللّٰهِ ۗ اَفَلَا تَذَكَّرُوْنَ

afara-ayta mani ittakha*dz*a il*aa*hahu haw*aa*hu wa-a*dh*allahu **al**l*aa*hu 'al*aa* 'ilmin wakhatama 'al*aa* sam'ihi waqalbihi waja'ala 'al*aa* ba*sh*arihi ghisy*aa*watan faman yahdiihi min

Maka pernahkah kamu melihat orang yang menjadikan hawa nafsunya sebagai tuhannya dan Allah membiarkannya sesat dengan sepengetahuan-Nya, dan Allah telah mengunci pendengaran dan hatinya serta meletakkan tutup atas penglihatannya? Maka siapa yang mampu mem

45:24

# وَقَالُوْا مَا هِيَ اِلَّا حَيَاتُنَا الدُّنْيَا نَمُوْتُ وَنَحْيَا وَمَا يُهْلِكُنَآ اِلَّا الدَّهْرُۚ وَمَا لَهُمْ بِذٰلِكَ مِنْ عِلْمٍۚ اِنْ هُمْ اِلَّا يَظُنُّوْنَ

waq*aa*luu m*aa* hiya ill*aa* *h*ay*aa*tun*aa* **al**dduny*aa* namuutu wana*h*y*aa* wam*aa* yuhlikun*aa* ill*aa* **al**ddahru wam*aa* lahum bi*dzaa*lika min 'i

Dan mereka berkata, “Kehidupan ini tidak lain hanyalah kehidupan di dunia saja, kita mati dan kita hidup, dan tidak ada yang membinasakan kita selain masa.” Tetapi mereka tidak mempunyai ilmu tentang itu, mereka hanyalah menduga-duga saja.

45:25

# وَاِذَا تُتْلٰى عَلَيْهِمْ اٰيٰتُنَا بَيِّنٰتٍ مَّا كَانَ حُجَّتَهُمْ اِلَّآ اَنْ قَالُوا ائْتُوْا بِاٰبَاۤىِٕنَآ اِنْ كُنْتُمْ صٰدِقِيْنَ

wa-i*dzaa* tutl*aa* 'alayhim *aa*y*aa*tun*aa* bayyin*aa*tin m*aa* k*aa*na *h*ujjatahum ill*aa* an q*aa*luu i/tuu bi-*aa*b*aa*-in*aa* in kuntum *shaa*diqiin**a**

Dan apabila kepada mereka dibacakan ayat-ayat Kami yang jelas, tidak ada bantahan mereka selain mengatakan, “Hidupkanlah kembali nenek moyang kami, jika kamu orang yang benar.”

45:26

# قُلِ اللّٰهُ يُحْيِيْكُمْ ثُمَّ يُمِيْتُكُمْ ثُمَّ يَجْمَعُكُمْ اِلٰى يَوْمِ الْقِيٰمَةِ لَارَيْبَ فِيْهِ وَلٰكِنَّ اَكْثَرَ النَّاسِ لَا يَعْلَمُوْنَ ࣖ

quli **al**l*aa*hu yu*h*yiikum tsumma yumiitukum tsumma yajma'ukum il*aa* yawmi **a**lqiy*aa*mati l*aa* rayba fiihi wal*aa*kinna aktsara **al**nn*aa*si l*aa* ya'lamuun**a**

Katakanlah, “Allah yang menghidupkan kemudian mematikan kamu, setelah itu mengumpulkan kamu pada hari Kiamat yang tidak diragukan lagi; tetapi kebanyakan manusia tidak mengetahui.”







45:27

# وَلِلّٰهِ مُلْكُ السَّمٰوٰتِ وَالْاَرْضِۗ وَيَوْمَ تَقُوْمُ السَّاعَةُ يَوْمَىِٕذٍ يَّخْسَرُ الْمُبْطِلُوْنَ

walill*aa*hi mulku **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i wayawma taquumu **al**ss*aa*'atu yawma-i*dz*in yakhsaru **a**lmub*th*iluun**a**

Dan milik Allah kerajaan langit dan bumi. Dan pada hari terjadinya Kiamat, akan rugilah pada hari itu orang-orang yang mengerjakan kebatilan (dosa).

45:28

# وَتَرٰى كُلَّ اُمَّةٍ جَاثِيَةً ۗ كُلُّ اُمَّةٍ تُدْعٰٓى اِلٰى كِتٰبِهَاۗ اَلْيَوْمَ تُجْزَوْنَ مَا كُنْتُمْ تَعْمَلُوْنَ

watar*aa* kulla ummatin j*aa*tsiyatan kullu ummatin tud'*aa* il*aa* kit*aa*bih*aa* **a**lyawma tujzawna m*aa* kuntum ta'maluun**a**

Dan (pada hari itu) engkau akan melihat setiap umat berlutut. Setiap umat dipanggil untuk (melihat) buku catatan amalnya. Pada hari itu kamu diberi balasan atas apa yang telah kamu kerjakan.

45:29

# هٰذَا كِتٰبُنَا يَنْطِقُ عَلَيْكُمْ بِالْحَقِّ ۗاِنَّا كُنَّا نَسْتَنْسِخُ مَا كُنْتُمْ تَعْمَلُوْنَ

h*aadzaa* kit*aa*bun*aa* yan*th*iqu 'alaykum bi**a**l*h*aqqi inn*aa* kunn*aa* nastansikhu m*aa* kuntum ta'maluun**a**

(Allah berfirman), “Inilah Kitab (catatan) Kami yang menuturkan kepadamu dengan sebenar-benarnya. Sesungguhnya Kami telah menyuruh mencatat apa yang telah kamu kerjakan.”

45:30

# فَاَمَّا الَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِ فَيُدْخِلُهُمْ رَبُّهُمْ فِيْ رَحْمَتِهٖۗ ذٰلِكَ هُوَ الْفَوْزُ الْمُبِيْنُ

fa-amm*aa* **al**la*dz*iina *aa*manuu wa'amiluu **al***shshaa*li*haa*ti fayudkhiluhum rabbuhum fii ra*h*matihi *dzaa*lika huwa **a**lfawzu **a**lmubiin**u**

**Maka adapun orang-orang yang beriman dan mengerjakan kebajikan, maka Tuhan memasukkan mereka ke dalam rahmat-Nya (surga). Demikian itulah kemenangan yang nyata.**









45:31

# وَاَمَّا الَّذِيْنَ كَفَرُوْاۗ اَفَلَمْ تَكُنْ اٰيٰتِيْ تُتْلٰى عَلَيْكُمْ فَاسْتَكْبَرْتُمْ وَكُنْتُمْ قَوْمًا مُّجْرِمِيْنَ

wa-amm*aa* **al**la*dz*iina kafaruu afalam takun *aa*y*aa*tii tutl*aa* 'alaykum fa**i**stakbartum wakuntum qawman mujrimiin**a**

Dan adapun (kepada) orang-orang yang kafir (difirmankan), “Bukankah ayat-ayat-Ku telah dibacakan kepadamu tetapi kamu menyombongkan diri dan kamu menjadi orang-orang yang berbuat dosa?”

45:32

# وَاِذَا قِيْلَ اِنَّ وَعْدَ اللّٰهِ حَقٌّ وَّالسَّاعَةُ لَا رَيْبَ فِيْهَا قُلْتُمْ مَّا نَدْرِيْ مَا السَّاعَةُۙ اِنْ نَّظُنُّ اِلَّا ظَنًّا وَّمَا نَحْنُ بِمُسْتَيْقِنِيْنَ

wa-i*dzaa* qiila inna wa'da **al**l*aa*hi *h*aqqun wa**al**ss*aa*'atu l*aa* rayba fiih*aa* qultum m*aa* nadrii m*aa* **al**ss*aa*'atu in na*zh*unnu ill*aa* *zh*

Dan apabila dikatakan (kepadamu), “Sungguh, janji Allah itu benar, dan hari Kiamat itu tidak diragukan adanya,” kamu menjawab, “Kami tidak tahu apakah hari Kiamat itu, kami hanyalah menduga-duga saja, dan kami tidak yakin.”







45:33

# وَبَدَا لَهُمْ سَيِّاٰتُ مَا عَمِلُوْا وَحَاقَ بِهِمْ مَّا كَانُوْا بِهٖ يَسْتَهْزِءُوْنَ

wabad*aa* lahum sayyi-*aa*tu m*aa* 'amiluu wa*haa*qa bihim m*aa* k*aa*nuu bihi yastahzi-uun**a**

Dan nyatalah bagi mereka keburukan-keburukan yang mereka kerjakan, dan berlakulah (azab) terhadap mereka yang dahulu mereka perolok-olokkan.

45:34

# وَقِيْلَ الْيَوْمَ نَنْسٰىكُمْ كَمَا نَسِيْتُمْ لِقَاۤءَ يَوْمِكُمْ هٰذَاۙ وَمَأْوٰىكُمُ النَّارُ وَمَا لَكُمْ مِّنْ نّٰصِرِيْنَ

waqiila **a**lyawma nans*aa*kum kam*aa* nasiitum liq*aa*-a yawmikum h*aadzaa* wama/w*aa*kumu **al**nn*aa*ru wam*aa* lakum min n*aas*iriin**a**

Dan kepada mereka dikatakan, “Pada hari ini Kami melupakan kamu sebagaimana kamu telah melupakan pertemuan (dengan) harimu ini; dan tempat kembalimu ialah neraka dan se-kali-kali tidak akan ada penolong bagimu.

45:35

# ذٰلِكُمْ بِاَنَّكُمُ اتَّخَذْتُمْ اٰيٰتِ اللّٰهِ هُزُوًا وَّغَرَّتْكُمُ الْحَيٰوةُ الدُّنْيَا ۚفَالْيَوْمَ لَا يُخْرَجُوْنَ مِنْهَا وَلَا هُمْ يُسْتَعْتَبُوْنَ

*dzaa*likum bi-annakumu ittakha*dz*tum *aa*y*aa*ti **al**l*aa*hi huzuwan wagharratkumu **a**l*h*ay*aa*tu **al**dduny*aa* fa**a**lyawma l*aa* yukhrajuuna minh<

Yang demikian itu, karena sesungguhnya kamu telah menjadikan ayat-ayat Allah sebagai olok-olokan dan kamu telah ditipu oleh kehidupan dunia.” Maka pada hari ini mereka tidak dikeluarkan dari neraka dan tidak pula mereka diberi kesempatan untuk bertobat.

45:36

# فَلِلّٰهِ الْحَمْدُ رَبِّ السَّمٰوٰتِ وَرَبِّ الْاَرْضِ رَبِّ الْعٰلَمِيْنَ

falill*aa*hi **a**l*h*amdu rabbi **al**ssam*aa*w*aa*ti warabbi **a**l-ar*dh*i rabbi **a**l'*aa*lamiin**a**

Segala puji hanya bagi Allah, Tuhan (pemilik) langit dan bumi, Tuhan seluruh alam.

45:37

# وَلَهُ الْكِبْرِيَاۤءُ فِى السَّمٰوٰتِ وَالْاَرْضِ ۗوَهُوَ الْعَزِيْزُ الْحَكِيْمُ ࣖ ۔

walahu **a**lkibriy*aa*u fii **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i wahuwa **a**l'aziizu **a**l*h*akiim**u**

Dan hanya bagi-Nya segala keagungan di langit dan di bumi, dan Dialah Yang Mahaperkasa, Mahabijaksana.

<!--EndFragment-->