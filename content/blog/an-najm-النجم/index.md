---
title: (53) An-Najm - النجم
date: 2021-10-27T04:12:08.433Z
ayat: 53
description: "Jumlah Ayat: 62 / Arti: Bintang"
---
<!--StartFragment-->

53:1

# وَالنَّجْمِ اِذَا هَوٰىۙ

wa**al**nnajmi i*dzaa* haw*aa*

Demi bintang ketika terbenam,

53:2

# مَا ضَلَّ صَاحِبُكُمْ وَمَا غَوٰىۚ

m*aa* *dh*alla *shaah*ibukum wam*aa* ghaw*aa*

kawanmu (Muhammad) tidak sesat dan tidak (pula) keliru,

53:3

# وَمَا يَنْطِقُ عَنِ الْهَوٰى

wam*aa* yan*th*iqu 'ani **a**lhaw*aa*

dan tidaklah yang diucapkannya itu (Al-Qur'an) menurut keinginannya.

53:4

# اِنْ هُوَ اِلَّا وَحْيٌ يُّوْحٰىۙ

in huwa ill*aa* wa*h*yun yuu*haa*

Tidak lain (Al-Qur'an itu) adalah wahyu yang diwahyukan (kepadanya),

53:5

# عَلَّمَهٗ شَدِيْدُ الْقُوٰىۙ

'allamahu syadiidu **a**lquw*aa*

yang diajarkan kepadanya oleh (Jibril) yang sangat kuat,

53:6

# ذُوْ مِرَّةٍۗ فَاسْتَوٰىۙ

*dz*uu mirratin fa**i**staw*aa*

yang mempunyai keteguhan; maka (Jibril itu) menampakkan diri dengan rupa yang asli (rupa yang bagus dan perkasa)

53:7

# وَهُوَ بِالْاُفُقِ الْاَعْلٰىۗ

wahuwa bi**a**l-ufuqi **a**l-a'l*aa*

Sedang dia berada di ufuk yang tinggi.

53:8

# ثُمَّ دَنَا فَتَدَلّٰىۙ

tsumma dan*aa* fatadall*aa*

Kemudian dia mendekat (pada Muhammad), lalu bertambah dekat,

53:9

# فَكَانَ قَابَ قَوْسَيْنِ اَوْ اَدْنٰىۚ

fak*aa*na q*aa*ba qawsayni aw adn*aa*

sehingga jaraknya (sekitar) dua busur panah atau lebih dekat (lagi).

53:10

# فَاَوْحٰىٓ اِلٰى عَبْدِهٖ مَآ اَوْحٰىۗ

fa-aw*haa* il*aa* 'abdihi m*aa* aw*haa*

Lalu disampaikannya wahyu kepada hamba-Nya (Muhammad) apa yang telah diwahyukan Allah.

53:11

# مَا كَذَبَ الْفُؤَادُ مَا رَاٰى

m*aa* ka*dz*aba **a**lfu-*aa*du m*aa* ra*aa*

Hatinya tidak mendustakan apa yang telah dilihatnya.

53:12

# اَفَتُمٰرُوْنَهٗ عَلٰى مَا يَرٰى

afatum*aa*ruunahu 'al*aa* m*aa* yar*aa*

Maka apakah kamu (musyrikin Mekah) hendak membantahnya tentang apa yang dilihatnya itu?

53:13

# وَلَقَدْ رَاٰهُ نَزْلَةً اُخْرٰىۙ

walaqad ra*aa*hu nazlatan ukhr*aa*

Dan sungguh, dia (Muhammad) telah melihatnya (dalam rupanya yang asli) pada waktu yang lain,

53:14

# عِنْدَ سِدْرَةِ الْمُنْتَهٰى

'inda sidrati **a**lmuntah*aa*

(yaitu) di Sidratul Muntaha,

53:15

# عِنْدَهَا جَنَّةُ الْمَأْوٰىۗ

'indah*aa* jannatu **a**lma/w*aa*

di dekatnya ada surga tempat tinggal,

53:16

# اِذْ يَغْشَى السِّدْرَةَ مَا يَغْشٰىۙ

i*dz* yaghsy*aa* **al**ssidrata m*aa* yaghsy*aa*

(Muhammad melihat Jibril) ketika Sidratil muntaha diliputi oleh sesuatu yang meliputinya,

53:17

# مَا زَاغَ الْبَصَرُ وَمَا طَغٰى

m*aa* z*aa*gha **a**lba*sh*aru wam*aa* *th*agh*aa*

penglihatannya (Muhammad) tidak menyimpang dari yang dilihatnya itu dan tidak (pula) melampauinya.

53:18

# لَقَدْ رَاٰى مِنْ اٰيٰتِ رَبِّهِ الْكُبْرٰى

laqad ra*aa* min *aa*y*aa*ti rabbihi **a**lkubr*aa*

Sungguh, dia telah melihat sebagian tanda-tanda (kebesaran) Tuhannya yang paling besar.

53:19

# اَفَرَءَيْتُمُ اللّٰتَ وَالْعُزّٰى

afara-aytumu **al**l*aa*ta wa**a**l'uzz*aa*

Maka apakah patut kamu (orang-orang musyrik) menganggap (berhala) Al-Lata dan Al-‘Uzza,

53:20

# وَمَنٰوةَ الثَّالِثَةَ الْاُخْرٰى

waman*aa*ta **al**tsts*aa*litsata **a**l-ukhr*aa*

dan Manat, yang ketiga (yang) kemudian (sebagai anak perempuan Allah).

53:21

# اَلَكُمُ الذَّكَرُ وَلَهُ الْاُنْثٰى

alakumu **al***dzdz*akaru walahu **a**l-unts*aa*

Apakah (pantas) untuk kamu yang laki-laki dan untuk-Nya yang perempuan?

53:22

# تِلْكَ اِذًا قِسْمَةٌ ضِيْزٰى

tilka i*dz*an qismatun *dh*iiz*aa*

Yang demikian itu tentulah suatu pembagian yang tidak adil.

53:23

# اِنْ هِيَ اِلَّآ اَسْمَاۤءٌ سَمَّيْتُمُوْهَآ اَنْتُمْ وَاٰبَاۤؤُكُمْ مَّآ اَنْزَلَ اللّٰهُ بِهَا مِنْ سُلْطٰنٍۗ اِنْ يَّتَّبِعُوْنَ اِلَّا الظَّنَّ وَمَا تَهْوَى الْاَنْفُسُۚ وَلَقَدْ جَاۤءَهُمْ مِّنْ رَّبِّهِمُ الْهُدٰىۗ

in hiya ill*aa* asm*aa*un sammaytumuuh*aa* antum wa*aa*b*aa*ukum m*aa* anzala **al**l*aa*hu bih*aa* min sul*thaa*nin in yattabi'uuna ill*aa* **al***zhzh*anna wam*aa<*

Itu tidak lain hanyalah nama-nama yang kamu dan nenek moyangmu mengada-adakannya; Allah tidak menurunkan suatu keterangan apa pun untuk (menyembah)nya. Mereka hanya mengikuti dugaan, dan apa yang diingini oleh keinginannya. Padahal sungguh, telah datang p







53:24

# اَمْ لِلْاِنْسَانِ مَا تَمَنّٰىۖ

am lil-ins*aa*ni m*aa* tamann*aa*

Atau apakah manusia akan mendapat segala yang dicita-citakannya?

53:25

# فَلِلّٰهِ الْاٰخِرَةُ وَالْاُوْلٰى ࣖ

falill*aa*hi **a**l-*aa*khiratu wa**a**l-uul*aa*

(Tidak!) Maka milik Allah-lah kehidupan akhirat dan kehidupan dunia.

53:26

# وَكَمْ مِّنْ مَّلَكٍ فِى السَّمٰوٰتِ لَا تُغْنِيْ شَفَاعَتُهُمْ شَيْـًٔا اِلَّا مِنْۢ بَعْدِ اَنْ يَّأْذَنَ اللّٰهُ لِمَنْ يَّشَاۤءُ وَيَرْضٰى

wakam min malakin fii **al**ssam*aa*w*aa*ti l*aa* tughnii syaf*aa*'atuhum syay-an ill*aa* min ba'di an ya/*dz*ana **al**l*aa*hu liman yasy*aa*u wayar*daa*

Dan betapa banyak malaikat di langit, syafaat (pertolongan) mereka sedikit pun tidak berguna kecuali apabila Allah telah mengizinkan (dan hanya) bagi siapa yang Dia kehendaki dan Dia ridai.

53:27

# اِنَّ الَّذِيْنَ لَا يُؤْمِنُوْنَ بِالْاٰخِرَةِ لَيُسَمُّوْنَ الْمَلٰۤىِٕكَةَ تَسْمِيَةَ الْاُنْثٰى

inna **al**la*dz*iina l*aa* yu/minuuna bi**a**l-*aa*khirati layusammuuna **a**lmal*aa*-ikata tasmiyata **a**l-unts*aa*

Sesungguhnya orang-orang yang tidak beriman kepada kehidupan akhirat, sungguh mereka menamakan para malaikat dengan nama perempuan.

53:28

# وَمَا لَهُمْ بِهٖ مِنْ عِلْمٍۗ اِنْ يَّتَّبِعُوْنَ اِلَّا الظَّنَّ وَاِنَّ الظَّنَّ لَا يُغْنِيْ مِنَ الْحَقِّ شَيْـًٔاۚ

wam*aa* lahum bihi min 'ilmin in yattabi'uuna ill*aa* **al***zhzh*anna wa-inna **al***zhzh*anna l*aa* yughnii mina **a**l*h*aqqi syay-*aa***n**

Dan mereka tidak mempunyai ilmu tentang itu. Mereka tidak lain hanyalah mengikuti dugaan, dan sesungguhnya dugaan itu tidak berfaedah sedikit pun terhadap kebenaran.

53:29

# فَاَعْرِضْ عَنْ مَّنْ تَوَلّٰىۙ عَنْ ذِكْرِنَا وَلَمْ يُرِدْ اِلَّا الْحَيٰوةَ الدُّنْيَاۗ

fa-a'ri*dh* 'an man tawall*aa* 'an *dz*ikrin*aa* walam yurid ill*aa* **a**l*h*ay*aa*ta **al**dduny*aa*

Maka tinggalkanlah (Muhammad) orang yang berpaling dari peringatan Kami, dan dia hanya mengingini kehidupan dunia.

53:30

# ذٰلِكَ مَبْلَغُهُمْ مِّنَ الْعِلْمِۗ اِنَّ رَبَّكَ هُوَ اَعْلَمُ بِمَنْ ضَلَّ عَنْ سَبِيْلِهٖۙ وَهُوَ اَعْلَمُ بِمَنِ اهْتَدٰى

*dzaa*lika mablaghuhum mina **a**l'ilmi inna rabbaka huwa a'lamu biman *dh*alla 'an sabiilihi wahuwa a'lamu bimani ihtad*aa*

Itulah kadar ilmu mereka. Sungguh, Tuhanmu, Dia lebih mengetahui siapa yang tersesat dari jalan-Nya dan Dia pula yang mengetahui siapa yang mendapat petunjuk.

53:31

# وَلِلّٰهِ مَا فِى السَّمٰوٰتِ وَمَا فِى الْاَرْضِۗ لِيَجْزِيَ الَّذِيْنَ اَسَاۤءُوْا بِمَا عَمِلُوْا وَيَجْزِيَ الَّذِيْنَ اَحْسَنُوْا بِالْحُسْنٰىۚ

walill*aa*hi m*aa* fii **al**ssam*aa*w*aa*ti wam*aa* fii **a**l-ar*dh*i liyajziya **al**la*dz*iina as*aa*uu bim*aa* 'amiluu wayajziya **al**la*dz*iina a<

Dan milik Allah-lah apa yang ada di langit dan apa yang ada di bumi. (Dengan demikian) Dia akan memberi balasan kepada orang-orang yang berbuat jahat sesuai dengan apa yang telah mereka kerjakan dan Dia akan memberi balasan kepada orang-orang yang berbuat

53:32

# اَلَّذِيْنَ يَجْتَنِبُوْنَ كَبٰۤىِٕرَ الْاِثْمِ وَالْفَوَاحِشَ اِلَّا اللَّمَمَۙ اِنَّ رَبَّكَ وَاسِعُ الْمَغْفِرَةِۗ هُوَ اَعْلَمُ بِكُمْ اِذْ اَنْشَاَكُمْ مِّنَ الْاَرْضِ وَاِذْ اَنْتُمْ اَجِنَّةٌ فِيْ بُطُوْنِ اُمَّهٰتِكُمْۗ فَلَا تُزَكُّوْٓا اَنْفُسَك

**al**la*dz*iina yajtanibuuna kab*aa*-ira **a**l-itsmi wa**a**lfaw*aah*isya ill*aa* **al**lamama inna rabbaka w*aa*si'u **a**lmaghfirati huwa a'lamu bikum i*dz*

Yaitu) mereka yang menjauhi dosa-dosa besar dan perbuatan keji, kecuali kesalahan-kesalahan kecil. Sungguh, Tuhanmu Mahaluas ampunan-Nya. Dia mengetahui tentang kamu, sejak Dia menjadikan kamu dari tanah lalu ketika kamu masih janin dalam perut ibumu. Mak

53:33

# اَفَرَءَيْتَ الَّذِيْ تَوَلّٰىۙ

afara-ayta **al**la*dz*ii tawall*aa*

Maka tidakkah engkau melihat orang yang berpaling (dari Al-Qur'an)?

53:34

# وَاَعْطٰى قَلِيْلًا وَّاَكْدٰى

wa-a'*thaa* qaliilan wa-akd*aa*

dan dia memberikan sedikit (dari apa yang dijanjikan) lalu menahan sisanya.

53:35

# اَعِنْدَهٗ عِلْمُ الْغَيْبِ فَهُوَ يَرٰى

a'indahu 'ilmu **a**lghaybi fahuwa yar*aa*

Apakah dia mempunyai ilmu tentang yang gaib sehingga dia dapat melihat(nya)?

53:36

# اَمْ لَمْ يُنَبَّأْ بِمَا فِيْ صُحُفِ مُوْسٰى

am lam yunabba/ bim*aa* fii *sh*u*h*ufi muus*aa*

Ataukah belum diberitakan (kepadanya) apa yang ada dalam lembaran-lembaran (Kitab Suci yang diturunkan kepada) Musa?

53:37

# وَاِبْرٰهِيْمَ الَّذِيْ وَفّٰىٓ ۙ

wa-ibr*aa*hiima **al**la*dz*ii waff*aa*

Dan (lembaran-lembaran) Ibrahim yang selalu menyempurnakan janji?

53:38

# اَلَّا تَزِرُ وَازِرَةٌ وِّزْرَ اُخْرٰىۙ

**al**l*aa* taziru w*aa*ziratun wizra ukhr*aa*

(yaitu) bahwa seseorang yang berdosa tidak akan memikul dosa orang lain,

53:39

# وَاَنْ لَّيْسَ لِلْاِنْسَانِ اِلَّا مَا سَعٰىۙ

wa-an laysa lil-ins*aa*ni ill*aa* m*aa* sa'*aa*

dan bahwa manusia hanya memperoleh apa yang telah diusahakannya,

53:40

# وَاَنَّ سَعْيَهٗ سَوْفَ يُرٰىۖ

wa-anna sa'yahu sawfa yur*aa*

dan sesungguhnya usahanya itu kelak akan diperlihatkan (kepadanya),

53:41

# ثُمَّ يُجْزٰىهُ الْجَزَاۤءَ الْاَوْفٰىۙ

tsumma yujz*aa*hu **a**ljaz*aa*-a **a**l-awf*aa*

kemudian akan diberi balasan kepadanya dengan balasan yang paling sempurna,

53:42

# وَاَنَّ اِلٰى رَبِّكَ الْمُنْتَهٰىۙ

wa-anna il*aa* rabbika **a**lmuntah*aa*

dan sesungguhnya kepada Tuhanmulah kesudahannya (segala sesuatu),

53:43

# وَاَنَّهٗ هُوَ اَضْحَكَ وَاَبْكٰى

wa-annahu huwa a*dh*aka wa-abk*aa*

dan sesungguhnya Dialah yang menjadikan orang tertawa dan menangis,

53:44

# وَاَنَّهٗ هُوَ اَمَاتَ وَاَحْيَاۙ

wa-annahu huwa am*aa*ta wa-a*h*y*aa*

dan sesungguhnya Dialah yang mematikan dan menghidupkan,

53:45

# وَاَنَّهٗ خَلَقَ الزَّوْجَيْنِ الذَّكَرَ وَالْاُنْثٰى

wa-annahu khalaqa **al**zzawjayni **al***dzdz*akara wa**a**l-unts*aa*

dan sesungguhnya Dialah yang men-ciptakan pasangan laki-laki dan perempuan,

53:46

# مِنْ نُّطْفَةٍ اِذَا تُمْنٰىۙ

min nu*th*fatin i*dzaa* tumn*aa*

dari mani, apabila dipancarkan,

53:47

# وَاَنَّ عَلَيْهِ النَّشْاَةَ الْاُخْرٰىۙ

wa-anna 'alayhi **al**nnasy-ata **a**l-ukhr*aa*

dan sesungguhnya Dialah yang menetapkan penciptaan yang lain (kebangkitan setelah mati),

53:48

# وَاَنَّهٗ هُوَ اَغْنٰى وَاَقْنٰىۙ

wa-annahu huwa aghn*aa* wa-aqn*aa*

dan sesungguhnya Dialah yang memberikan kekayaan dan kecukupan.

53:49

# وَاَنَّهٗ هُوَ رَبُّ الشِّعْرٰىۙ

wa-annahu huwa rabbu **al**sysyi'r*aa*

dan sesungguhnya Dialah Tuhan (yang memiliki) bintang Syi‘ra,

53:50

# وَاَنَّهٗٓ اَهْلَكَ عَادًا ۨالْاُوْلٰىۙ

wa-annahu ahlaka '*aa*dan **a**l-uul*aa*

dan sesungguhnya Dialah yang telah membinasakan kaum ‘Ad dahulu kala,

53:51

# وَثَمُوْدَا۟ فَمَآ اَبْقٰىۙ

watsamuuda fam*aa* abq*aa*

dan kaum Samud, tidak seorang pun yang ditinggalkan-Nya (hidup),

53:52

# وَقَوْمَ نُوْحٍ مِّنْ قَبْلُۗ اِنَّهُمْ كَانُوْا هُمْ اَظْلَمَ وَاَطْغٰىۗ

waqawma nuu*h*in min qablu innahum k*aa*nuu hum a*zh*lama wa-a*th*gh*aa*

dan (juga) kaum Nuh sebelum itu. Sungguh, mereka adalah orang-orang yang paling zalim dan paling durhaka.

53:53

# وَالْمُؤْتَفِكَةَ اَهْوٰىۙ

wa**a**lmu/tafikata ahw*aa*

Dan prahara angin telah meruntuhkan (negeri kaum Lut),

53:54

# فَغَشّٰىهَا مَا غَشّٰىۚ

faghasysy*aa*h*aa* m*aa* ghasysy*aa*

lalu menimbuni negeri itu (sebagai azab) dengan (puing-puing) yang menimpanya.

53:55

# فَبِاَيِّ اٰلَاۤءِ رَبِّكَ تَتَمَارٰى

fabi-ayyi *aa*l*aa*-i rabbika tatam*aa*r*aa*

Maka terhadap nikmat Tuhanmu yang manakah yang masih kamu ragukan?

53:56

# هٰذَا نَذِيْرٌ مِّنَ النُّذُرِ الْاُوْلٰى

h*aatsa* na*dz*iirun mina **al**nnu*dz*uri **a**l-uul*aa*

Ini (Muhammad) salah seorang pemberi peringatan di antara para pemberi peringatan yang telah terdahulu.

53:57

# اَزِفَتِ الْاٰزِفةُ ۚ

azifati **a**l-*aa*zifat**u**

Yang dekat (hari Kiamat) telah makin mendekat.

53:58

# لَيْسَ لَهَا مِنْ دُوْنِ اللّٰهِ كَاشِفَةٌ ۗ

laysa lah*aa* min duuni **al**l*aa*hi k*aa*syifat**un**

Tidak ada yang akan dapat mengungkapkan (terjadinya hari itu) selain Allah.

53:59

# اَفَمِنْ هٰذَا الْحَدِيْثِ تَعْجَبُوْنَۙ

afamin h*aadzaa* **a**l*h*adiitsi ta'jabuun**a**

Maka apakah kamu merasa heran terhadap pemberitaan ini?

53:60

# وَتَضْحَكُوْنَ وَلَا تَبْكُوْنَۙ

wata*dh*akuuna wal*aa* tabkuun**a**

dan kamu tertawakan dan tidak menangis,

53:61

# وَاَنْتُمْ سَامِدُوْنَ

wa-antum s*aa*miduun**a**

sedang kamu lengah (darinya).

53:62

# فَاسْجُدُوْا لِلّٰهِ وَاعْبُدُوْا ࣖ ۩

fa**u**sjuduu lill*aa*hi wa**u**'buduu

Maka bersujudlah kepada Allah dan sembahlah (Dia).

<!--EndFragment-->