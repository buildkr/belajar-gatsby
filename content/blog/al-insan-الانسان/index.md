---
title: (76) Al-Insan - الانسان
date: 2021-10-27T04:07:46.367Z
ayat: 76
description: "Jumlah Ayat: 31 / Arti: Manusia"
---
<!--StartFragment-->

76:1

# هَلْ اَتٰى عَلَى الْاِنْسَانِ حِيْنٌ مِّنَ الدَّهْرِ لَمْ يَكُنْ شَيْـًٔا مَّذْكُوْرًا

hal at*aa* 'al*aa* **a**l-ins*aa*ni *h*iinun mina **al**ddahri lam yakun syay-an ma*dz*kuur*aa***n**

Bukankah pernah datang kepada manusia waktu dari masa, yang ketika itu belum merupakan sesuatu yang dapat disebut?

76:2

# اِنَّا خَلَقْنَا الْاِنْسَانَ مِنْ نُّطْفَةٍ اَمْشَاجٍۖ نَّبْتَلِيْهِ فَجَعَلْنٰهُ سَمِيْعًاۢ بَصِيْرًا

inn*aa* khalaqn*aa* **a**l-ins*aa*na min nu*th*fatin amsy*aa*jin nabtaliihi faja'aln*aa*hu samii'an ba*sh*iir*aa***n**

Sungguh, Kami telah menciptakan manusia dari setetes mani yang bercampur yang Kami hendak mengujinya (dengan perintah dan larangan), karena itu Kami jadikan dia mendengar dan melihat.

76:3

# اِنَّا هَدَيْنٰهُ السَّبِيْلَ اِمَّا شَاكِرًا وَّاِمَّا كَفُوْرًا

inn*aa* hadayn*aa*hu **al**ssabiila imm*aa* sy*aa*kiran wa-imm*aa* kafuur*aa***n**

Sungguh, Kami telah menunjukkan kepadanya jalan yang lurus; ada yang bersyukur dan ada pula yang kufur.

76:4

# اِنَّآ اَعْتَدْنَا لِلْكٰفِرِيْنَ سَلٰسِلَا۟ وَاَغْلٰلًا وَّسَعِيْرًا

inn*aa* a'tadn*aa* lilk*aa*firiina sal*aa*sila wa-aghl*aa*lan wasa'iir*aa***n**

Sungguh, Kami telah menyediakan bagi orang-orang kafir rantai, belenggu dan neraka yang menyala-nyala.

76:5

# اِنَّ الْاَبْرَارَ يَشْرَبُوْنَ مِنْ كَأْسٍ كَانَ مِزَاجُهَا كَافُوْرًاۚ

inna **a**l-abr*aa*ra yasyrabuuna min ka/sin k*aa*na miz*aa*juh*aa* k*aa*fuur*aa***n**

Sungguh, orang-orang yang berbuat kebajikan akan minum dari gelas (berisi minuman) yang campurannya adalah air kafur,

76:6

# عَيْنًا يَّشْرَبُ بِهَا عِبَادُ اللّٰهِ يُفَجِّرُوْنَهَا تَفْجِيْرًا

'aynan yasyrabu bih*aa* 'ib*aa*du **al**l*aa*hi yufajjiruunah*aa* tafjiir*aa***n**

(yaitu) mata air (dalam surga) yang diminum oleh hamba-hamba Allah dan mereka dapat memancarkannya dengan sebaik-baiknya.

76:7

# يُوْفُوْنَ بِالنَّذْرِ وَيَخَافُوْنَ يَوْمًا كَانَ شَرُّهٗ مُسْتَطِيْرًا

yuufuuna bi**al**nna*dz*ri wayakh*aa*fuuna yawman k*aa*na syarruhu musta*th*iir*aa***n**

Mereka memenuhi nazar dan takut akan suatu hari yang azabnya merata di mana-mana.

76:8

# وَيُطْعِمُوْنَ الطَّعَامَ عَلٰى حُبِّهٖ مِسْكِيْنًا وَّيَتِيْمًا وَّاَسِيْرًا

wayu*th*'imuna **al***ththh*a'*aa*ma 'al*aa* *h*ubbihi miskiinan wayatiiman wa-asiir*aa***n**

Dan mereka memberikan makanan yang disukainya kepada orang miskin, anak yatim dan orang yang ditawan,

76:9

# اِنَّمَا نُطْعِمُكُمْ لِوَجْهِ اللّٰهِ لَا نُرِيْدُ مِنْكُمْ جَزَاۤءً وَّلَا شُكُوْرًا

innam*aa* nu*th*'imukum liwajhi **al**l*aa*hi l*aa* nuriidu minkum jaz*aa*-an wal*aa* syukuur*aa***n**

(sambil berkata), “Sesungguhnya kami memberi makanan kepadamu hanyalah karena mengharapkan keridaan Allah, kami tidak mengharap balasan dan terima kasih dari kamu.

76:10

# اِنَّا نَخَافُ مِنْ رَّبِّنَا يَوْمًا عَبُوْسًا قَمْطَرِيْرًا

innam*aa* nu*th*'imukum liwajhi **al**l*aa*hi l*aa* nuriidu minkum jaz*aa*-an wal*aa* syukuur*aa***n**

Sungguh, kami takut akan (azab) Tuhan pada hari (ketika) orang-orang berwajah masam penuh kesulitan.”

76:11

# فَوَقٰىهُمُ اللّٰهُ شَرَّ ذٰلِكَ الْيَوْمِ وَلَقّٰىهُمْ نَضْرَةً وَّسُرُوْرًاۚ

fawaq*aa*humu **al**l*aa*hu syarra *dzaa*lika **a**lyawmi walaqq*aa*hum na*dh*ratan wasuruur*aa***n**

Maka Allah melindungi mereka dari kesusahan hari itu, dan memberikan kepada mereka keceriaan dan kegembiraan.

76:12

# وَجَزٰىهُمْ بِمَا صَبَرُوْا جَنَّةً وَّحَرِيْرًاۙ

wajaz*aa*hum bim*aa* *sh*abaruu jannatan wa*h*ariir*aa***n**

Dan Dia memberi balasan kepada mereka karena kesabarannya (berupa) surga dan (pakaian) sutera.

76:13

# مُّتَّكِـِٕيْنَ فِيْهَا عَلَى الْاَرَاۤىِٕكِۚ لَا يَرَوْنَ فِيْهَا شَمْسًا وَّلَا زَمْهَرِيْرًاۚ

muttaki-iina fiih*aa* 'al*aa* **a**l-ar*aa*-iki l*aa* yarawna fiih*aa* syamsan wal*aa* zamhariir*aa***n**

Di sana mereka duduk bersandar di atas dipan, di sana mereka tidak melihat (merasakan teriknya) matahari dan tidak pula dingin yang berlebihan.

76:14

# وَدَانِيَةً عَلَيْهِمْ ظِلٰلُهَا وَذُلِّلَتْ قُطُوْفُهَا تَذْلِيْلًا

wad*aa*niyatan 'alayhim *zh*il*aa*luh*aa* wa*dz*ullilat qu*th*uufuh*aa* ta*dz*liil*aa***n**

Dan naungan (pepohonan)nya dekat di atas mereka dan dimudahkan semudah-mudahnya untuk memetik (buah)nya.

76:15

# وَيُطَافُ عَلَيْهِمْ بِاٰنِيَةٍ مِّنْ فِضَّةٍ وَّاَكْوَابٍ كَانَتْ قَوَارِيْرَا۠

wayu*thaa*fu 'alayhim bi-*aa*niyatin min fi*dhdh*atin wa-akw*aa*bin k*aa*nat qaw*aa*riir*aa*

Dan kepada mereka diedarkan bejana-bejana dari perak dan piala-piala yang bening laksana kristal,

76:16

# قَوَارِيْرَا۟ مِنْ فِضَّةٍ قَدَّرُوْهَا تَقْدِيْرًا

qaw*aa*riira min fi*dhdh*atin qaddaruuh*aa* taqdiir*aa***n**

kristal yang jernih terbuat dari perak, mereka tentukan ukurannya yang sesuai (dengan kehendak mereka).

76:17

# وَيُسْقَوْنَ فِيْهَا كَأْسًا كَانَ مِزَاجُهَا زَنْجَبِيْلًاۚ

wayusqawna fiih*aa* ka/san k*aa*na miz*aa*juh*aa* zanjabiil*aa***n**

Dan di sana mereka diberi segelas minuman bercampur jahe.

76:18

# عَيْنًا فِيْهَا تُسَمّٰى سَلْسَبِيْلًا

'aynan fiih*aa* tusamm*aa* salsabiil*aa***n**

(Yang didatangkan dari) sebuah mata air (di surga) yang dinamakan Salsabil.

76:19

# ۞ وَيَطُوْفُ عَلَيْهِمْ وِلْدَانٌ مُّخَلَّدُوْنَۚ اِذَا رَاَيْتَهُمْ حَسِبْتَهُمْ لُؤْلُؤًا مَّنْثُوْرًا

waya*th*uufu 'alayhim wild*aa*nun mukhalladuuna i*dzaa* ra-aytahum *h*asibtahum lu/lu-an mantsuur*aa***n**

Dan mereka dikelilingi oleh para pemuda yang tetap muda. Apabila kamu melihatnya, akan kamu kira mereka, mutiara yang bertaburan.

76:20

# وَاِذَا رَاَيْتَ ثَمَّ رَاَيْتَ نَعِيْمًا وَّمُلْكًا كَبِيْرًا

wa-i*dzaa* ra-ayta tsamma ra-ayta na'iiman wamulkan kabiir*aa***n**

Dan apabila engkau melihat (keadaan) di sana (surga), niscaya engkau akan melihat berbagai macam kenikmatan dan kerajaan yang besar.

76:21

# عٰلِيَهُمْ ثِيَابُ سُنْدُسٍ خُضْرٌ وَّاِسْتَبْرَقٌۖ وَّحُلُّوْٓا اَسَاوِرَ مِنْ فِضَّةٍۚ وَسَقٰىهُمْ رَبُّهُمْ شَرَابًا طَهُوْرًا

'*aa*liyahum tsiy*aa*bu sundusin khu*dh*run wa-istabraqun wa*h*ulluu as*aa*wira min fi*dhdh*atin wasaq*aa*hum rabbuhum syar*aa*ban *th*ahuur*aa***n**

Mereka berpakaian sutera halus yang hijau dan sutera tebal dan memakai gelang terbuat dari perak, dan Tuhan memberikan kepada mereka minuman yang bersih (dan suci).

76:22

# اِنَّ هٰذَا كَانَ لَكُمْ جَزَاۤءً وَّكَانَ سَعْيُكُمْ مَّشْكُوْرًا ࣖ

inna h*aadzaa* k*aa*na lakum jaz*aa*-an wak*aa*na sa'yukum masykuur*aa***n**

Inilah balasan untukmu, dan segala usahamu diterima dan diakui (Allah).

76:23

# اِنَّا نَحْنُ نَزَّلْنَا عَلَيْكَ الْقُرْاٰنَ تَنْزِيْلًاۚ

inn*aa* na*h*nu nazzaln*aa* 'alayka **a**lqur-*aa*na tanziil*aa***n**

Sesungguhnya Kamilah yang menurunkan Al-Qur'an kepadamu (Muhammad) secara berangsur-angsur.

76:24

# فَاصْبِرْ لِحُكْمِ رَبِّكَ وَلَا تُطِعْ مِنْهُمْ اٰثِمًا اَوْ كَفُوْرًاۚ

fa**i***sh*bir li*h*ukmi rabbika wal*aa* tu*th*i' minhum *aa*tsiman aw kafuur*aa***n**

Maka bersabarlah untuk (melaksanakan) ketetapan Tuhanmu, dan janganlah engkau ikuti orang yang berdosa dan orang yang kafir di antara mereka.

76:25

# وَاذْكُرِ اسْمَ رَبِّكَ بُكْرَةً وَّاَصِيْلًاۚ

wa**u***dz*kuri isma rabbika bukratan wa-a*sh*iil*aa***n**

Dan sebutlah nama Tuhanmu pada (waktu) pagi dan petang.

76:26

# وَمِنَ الَّيْلِ فَاسْجُدْ لَهٗ وَسَبِّحْهُ لَيْلًا طَوِيْلًا

wamina **al**layli fa**u**sjud lahu wasabbi*h*hu laylan *th*awiil*aa***n**

Dan pada sebagian dari malam, maka bersujudlah kepada-Nya dan bertasbihlah kepada-Nya pada bagian yang panjang di malam hari.

76:27

# اِنَّ هٰٓؤُلَاۤءِ يُحِبُّوْنَ الْعَاجِلَةَ وَيَذَرُوْنَ وَرَاۤءَهُمْ يَوْمًا ثَقِيْلًا

inna h*aa*ul*aa*-i yu*h*ibbuuna **a**l'*aa*jilata waya*dz*aruuna war*aa*-ahum yawman tsaqiil*aa***n**

Sesungguhnya mereka (orang kafir) itu mencintai kehidupan (dunia) dan meninggalkan hari yang berat (hari akhirat) di belakangnya.

76:28

# نَحْنُ خَلَقْنٰهُمْ وَشَدَدْنَآ اَسْرَهُمْۚ وَاِذَا شِئْنَا بَدَّلْنَآ اَمْثَالَهُمْ تَبْدِيْلًا

na*h*nu khalaqn*aa*hum wasyadadn*aa* asrahum wa-i*dzaa* syi/n*aa* baddaln*aa* amts*aa*lahum tabdiil*aa***n**

Kami telah menciptakan mereka dan menguatkan persendian tubuh mereka. Tetapi, jika Kami menghendaki, Kami dapat mengganti dengan yang serupa mereka.

76:29

# اِنَّ هٰذِهٖ تَذْكِرَةٌ ۚ فَمَنْ شَاۤءَ اتَّخَذَ اِلٰى رَبِّهٖ سَبِيْلًا

inna h*aadz*ihi ta*dz*kiratun faman sy*aa*-a ittakha*dz*a il*aa* rabbihi sabiil*aa***n**

Sungguh, (ayat-ayat) ini adalah peringatan, maka barangsiapa menghendaki (kebaikan bagi dirinya) tentu dia mengambil jalan menuju kepada Tuhannya.

76:30

# وَمَا تَشَاۤءُوْنَ اِلَّآ اَنْ يَّشَاۤءَ اللّٰهُ ۗاِنَّ اللّٰهَ كَانَ عَلِيْمًا حَكِيْمًاۖ

wam*aa* tasy*aa*uuna ill*aa* an yasy*aa*-a **al**l*aa*hu inna **al**l*aa*ha k*aa*na 'aliiman *h*akiim*aa***n**

Tetapi kamu tidak mampu (menempuh jalan itu), kecuali apabila Allah kehendaki Allah. Sungguh, Allah Maha Mengetahui, Mahabijaksana.

76:31

# يُّدْخِلُ مَنْ يَّشَاۤءُ فِيْ رَحْمَتِهٖۗ وَالظّٰلِمِيْنَ اَعَدَّ لَهُمْ عَذَابًا اَلِيْمًا ࣖ

yudkhilu man yasy*aa*u fii ra*h*matihi wa**al***zhzhaa*limiina a'adda lahum 'a*dzaa*ban **a**liim*aa***n**

Dia memasukkan siapa pun yang Dia kehendaki ke dalam rahmat-Nya (surga). Adapun bagi orang-orang zalim disediakan-Nya azab yang pedih.

<!--EndFragment-->