---
title: (4) An-Nisa' - النساۤء
date: 2021-10-27T03:11:45.526Z
ayat: 4
description: "Jumlah Ayat: 176 / Arti: Wanita"
---
<!--StartFragment-->

4:1

# يٰٓاَيُّهَا النَّاسُ اتَّقُوْا رَبَّكُمُ الَّذِيْ خَلَقَكُمْ مِّنْ نَّفْسٍ وَّاحِدَةٍ وَّخَلَقَ مِنْهَا زَوْجَهَا وَبَثَّ مِنْهُمَا رِجَالًا كَثِيْرًا وَّنِسَاۤءً ۚ وَاتَّقُوا اللّٰهَ الَّذِيْ تَسَاۤءَلُوْنَ بِهٖ وَالْاَرْحَامَ ۗ اِنَّ اللّٰهَ كَانَ عَلَي

y*aa* ayyuh*aa* **al**nn*aa*su ittaquu rabbakumu **al**la*dz*ii khalaqakum min nafsin w*aah*idatin wakhalaqa minh*aa* zawjah*aa* wabatstsa minhum*aa* rij*aa*lan katsiiran wanis*aa*

*Wahai manusia! Bertakwalah kepada Tuhanmu yang telah menciptakan kamu dari diri yang satu (Adam), dan (Allah) menciptakan pasangannya (Hawa) dari (diri)-nya; dan dari keduanya Allah memperkembangbiakkan laki-laki dan perempuan yang banyak. Bertakwalah kep*

4:2

# وَاٰتُوا الْيَتٰمٰىٓ اَمْوَالَهُمْ وَلَا تَتَبَدَّلُوا الْخَبِيْثَ بِالطَّيِّبِ ۖ وَلَا تَأْكُلُوْٓا اَمْوَالَهُمْ اِلٰٓى اَمْوَالِكُمْ ۗ اِنَّهٗ كَانَ حُوْبًا كَبِيْرًا

wa*aa*tuu **a**lyat*aa*m*aa* amw*aa*lahum wal*aa* tatabaddaluu **a**lkhabiitsa bi**al***ththh*ayyibi wal*aa *ta/kuluu amw*aa*lahum il*aa *amw*aa*likum innahu k*aa*

*Dan berikanlah kepada anak-anak yatim (yang sudah dewasa) harta mereka, janganlah kamu menukar yang baik dengan yang buruk, dan janganlah kamu makan harta mereka bersama hartamu. Sungguh, (tindakan menukar dan memakan) itu adalah dosa yang besar.*

4:3

# وَاِنْ خِفْتُمْ اَلَّا تُقْسِطُوْا فِى الْيَتٰمٰى فَانْكِحُوْا مَا طَابَ لَكُمْ مِّنَ النِّسَاۤءِ مَثْنٰى وَثُلٰثَ وَرُبٰعَ ۚ فَاِنْ خِفْتُمْ اَلَّا تَعْدِلُوْا فَوَاحِدَةً اَوْ مَا مَلَكَتْ اَيْمَانُكُمْ ۗ ذٰلِكَ اَدْنٰٓى اَلَّا تَعُوْلُوْاۗ

wa-in khiftum **al**l*aa* tuqsi*th*uu fii **a**lyat*aa*m*aa* fa**i**nki*h*uu m*aa* *thaa*ba lakum mina **al**nnis*aa*-i matsn*aa* watsul*aa*tsa wa

Dan jika kamu khawatir tidak akan mampu berlaku adil terhadap (hak-hak) perempuan yatim (bilamana kamu menikahinya), maka nikahilah perempuan (lain) yang kamu senangi: dua, tiga atau empat. Tetapi jika kamu khawatir tidak akan mampu berlaku adil, maka (ni

4:4

# وَاٰتُوا النِّسَاۤءَ صَدُقٰتِهِنَّ نِحْلَةً ۗ فَاِنْ طِبْنَ لَكُمْ عَنْ شَيْءٍ مِّنْهُ نَفْسًا فَكُلُوْهُ هَنِيْۤـًٔا مَّرِيْۤـًٔا

wa*aa*tuu **al**nnis*aa*-a *sh*aduq*aa*tihinna ni*h*latan fa-in *th*ibna lakum 'an syay-in minhu nafsan fakuluuhu hanii-an marii-*aa**\*n**

Dan berikanlah maskawin (mahar) kepada perempuan (yang kamu nikahi) sebagai pemberian yang penuh kerelaan. Kemudian, jika mereka menyerahkan kepada kamu sebagian dari (maskawin) itu dengan senang hati, maka terimalah dan nikmatilah pemberian itu dengan se

4:5

# وَلَا تُؤْتُوا السُّفَهَاۤءَ اَمْوَالَكُمُ الَّتِيْ جَعَلَ اللّٰهُ لَكُمْ قِيٰمًا وَّارْزُقُوْهُمْ فِيْهَا وَاكْسُوْهُمْ وَقُوْلُوْا لَهُمْ قَوْلًا مَّعْرُوْفًا

wal*aa* tu/tuu **al**ssufah*aa*-a amw*aa*lakumu **al**latii ja'ala **al**l*aa*hu lakum qiy*aa*man wa**u**rzuquuhum fiih*aa* wa**u**ksuuhum waquuluu lahum qawlan

Dan janganlah kamu serahkan kepada orang yang belum sempurna akalnya, harta (mereka yang ada dalam kekuasaan) kamu yang dijadikan Allah sebagai pokok kehidupan. Berilah mereka belanja dan pakaian (dari hasil harta itu) dan ucapkanlah kepada mereka perkata

4:6

# وَابْتَلُوا الْيَتٰمٰى حَتّٰىٓ اِذَا بَلَغُوا النِّكَاحَۚ فَاِنْ اٰنَسْتُمْ مِّنْهُمْ رُشْدًا فَادْفَعُوْٓا اِلَيْهِمْ اَمْوَالَهُمْ ۚ وَلَا تَأْكُلُوْهَآ اِسْرَافًا وَّبِدَارًا اَنْ يَّكْبَرُوْا ۗ وَمَنْ كَانَ غَنِيًّا فَلْيَسْتَعْفِفْ ۚ وَمَنْ كَانَ فَ

wa**i**btaluu **a**lyat*aa*m*aa* *h*att*aa* i*dzaa* balaghuu **al**nnik*aah*a fa-in *aa*nastum minhum rusydan fa**i**dfa'uu ilayhim amw*aa*lahum wal*aa* ta/ku

Dan ujilah anak-anak yatim itu sampai mereka cukup umur untuk menikah. Kemudian jika menurut pendapatmu mereka telah cerdas (pandai memelihara harta), maka serahkanlah kepada mereka hartanya. Dan janganlah kamu memakannya (harta anak yatim) melebihi batas

4:7

# لِلرِّجَالِ نَصِيْبٌ مِّمَّا تَرَكَ الْوَالِدٰنِ وَالْاَقْرَبُوْنَۖ وَلِلنِّسَاۤءِ نَصِيْبٌ مِّمَّا تَرَكَ الْوَالِدٰنِ وَالْاَقْرَبُوْنَ مِمَّا قَلَّ مِنْهُ اَوْ كَثُرَ ۗ نَصِيْبًا مَّفْرُوْضًا

li**l**rrij*aa*li na*sh*iibun mimm*aa* taraka **a**lw*aa*lid*aa*ni wa**a**l-aqrabuuna wali**l**nnis*aa*-i na*sh*iibun mimm*aa* taraka **a**lw*aa*

Bagi laki-laki ada hak bagian dari harta peninggalan kedua orang tua dan kerabatnya, dan bagi perempuan ada hak bagian (pula) dari harta peninggalan kedua orang tua dan kerabatnya, baik sedikit atau banyak menurut bagian yang telah ditetapkan.

4:8

# وَاِذَا حَضَرَ الْقِسْمَةَ اُولُوا الْقُرْبٰى وَالْيَتٰمٰى وَالْمَسٰكِيْنُ فَارْزُقُوْهُمْ مِّنْهُ وَقُوْلُوْا لَهُمْ قَوْلًا مَّعْرُوْفًا

wa-i*dzaa* *h*a*dh*ara **a**lqismata uluu **a**lqurb*aa* wa**a**lyat*aa*m*aa* wa**a**lmas*aa*kiinu fa**u**rzuquuhum minhu waquuluu lahum qawlan ma'ruuf*a*

Dan apabila sewaktu pembagian itu hadir beberapa kerabat, anak-anak yatim dan orang-orang miskin, maka berilah mereka dari harta itu (sekedarnya) dan ucapkanlah kepada mereka perkataan yang baik.

4:9

# وَلْيَخْشَ الَّذِيْنَ لَوْ تَرَكُوْا مِنْ خَلْفِهِمْ ذُرِّيَّةً ضِعٰفًا خَافُوْا عَلَيْهِمْۖ فَلْيَتَّقُوا اللّٰهَ وَلْيَقُوْلُوْا قَوْلًا سَدِيْدًا

walyakhsya **al**la*dz*iina law tarakuu min khalfihim *dz*urriyyatan *dh*i'*aa*fan kh*aa*fuu 'alayhim falyattaquu **al**l*aa*ha walyaquuluu qawlan sadiid*aa**\*n**

Dan hendaklah takut (kepada Allah) orang-orang yang sekiranya mereka meninggalkan keturunan yang lemah di belakang mereka yang mereka khawatir terhadap (kesejahteraan)nya. Oleh sebab itu, hendaklah mereka bertakwa kepada Allah, dan hendaklah mereka berbic

4:10

# اِنَّ الَّذِيْنَ يَأْكُلُوْنَ اَمْوَالَ الْيَتٰمٰى ظُلْمًا اِنَّمَا يَأْكُلُوْنَ فِيْ بُطُوْنِهِمْ نَارًا ۗ وَسَيَصْلَوْنَ سَعِيْرًا ࣖ

inna **al**la*dz*iina ya/kuluuna amw*aa*la **a**lyat*aa*m*aa* *zh*ulman innam*aa* ya/kuluuna fii bu*th*uunihim n*aa*ran wasaya*sh*lawna sa'iir*aa**\*n**

Sesungguhnya orang-orang yang memakan harta anak yatim secara zalim, sebenarnya mereka itu menelan api dalam perutnya dan mereka akan masuk ke dalam api yang menyala-nyala (neraka).

4:11

# يُوْصِيْكُمُ اللّٰهُ فِيْٓ اَوْلَادِكُمْ لِلذَّكَرِ مِثْلُ حَظِّ الْاُنْثَيَيْنِ ۚ فَاِنْ كُنَّ نِسَاۤءً فَوْقَ اثْنَتَيْنِ فَلَهُنَّ ثُلُثَا مَا تَرَكَ ۚ وَاِنْ كَانَتْ وَاحِدَةً فَلَهَا النِّصْفُ ۗ وَلِاَبَوَيْهِ لِكُلِّ وَاحِدٍ مِّنْهُمَا السُّدُسُ مِم

yuu*sh*iikumu **al**l*aa*hu fii awl*aa*dikum li**l***dzdz*akari mitslu* h*a*zhzh*i **a**luntsayayni fa-in kunna nis*aa*\-an fawqa itsnatayni falahunna tsuluts*aa *m*aa* tar

Allah mensyariatkan (mewajibkan) kepadamu tentang (pembagian warisan untuk) anak-anakmu, (yaitu) bagian seorang anak laki-laki sama dengan bagian dua orang anak perempuan. Dan jika anak itu semuanya perempuan yang jumlahnya lebih dari dua, maka bagian mer

4:12

# ۞ وَلَكُمْ نِصْفُ مَا تَرَكَ اَزْوَاجُكُمْ اِنْ لَّمْ يَكُنْ لَّهُنَّ وَلَدٌ ۚ فَاِنْ كَانَ لَهُنَّ وَلَدٌ فَلَكُمُ الرُّبُعُ مِمَّا تَرَكْنَ مِنْۢ بَعْدِ وَصِيَّةٍ يُّوْصِيْنَ بِهَآ اَوْ دَيْنٍ ۗ وَلَهُنَّ الرُّبُعُ مِمَّا تَرَكْتُمْ اِنْ لَّمْ يَكُنْ ل

walakum ni*sh*fu m*aa* taraka azw*aa*jukum in lam yakun lahunna waladun fa-in k*aa*na lahunna waladun falakumu **al**rrubu'u mimm*aa* tarakna min ba'di wa*sh*iyyatin yuu*sh*iina bih*aa* aw daynin walahu

Dan bagianmu (suami-suami) adalah seperdua dari harta yang ditinggalkan oleh istri-istrimu, jika mereka tidak mempunyai anak. Jika mereka (istri-istrimu) itu mempunyai anak, maka kamu mendapat seperempat dari harta yang ditinggalkannya setelah (dipenuhi)

4:13

# تِلْكَ حُدُوْدُ اللّٰهِ ۗ وَمَنْ يُّطِعِ اللّٰهَ وَرَسُوْلَهٗ يُدْخِلْهُ جَنّٰتٍ تَجْرِيْ مِنْ تَحْتِهَا الْاَنْهٰرُ خٰلِدِيْنَ فِيْهَا ۗ وَذٰلِكَ الْفَوْزُ الْعَظِيْمُ

tilka *h*uduudu **al**l*aa*hi waman yu*th*i'i **al**l*aa*ha warasuulahu yudkhilhu jann*aa*tin tajrii min ta*h*tih*aa* **a**l-anh*aa*ru kh*aa*lidiina fiih*aa* wa*dza*

Itulah batas-batas (hukum) Allah. Barangsiapa taat kepada Allah dan Rasul-Nya, Dia akan memasukkannya ke dalam surga-surga yang mengalir di bawahnya sungai-sungai, mereka kekal di dalamnya. Dan itulah kemenangan yang agung.

4:14

# وَمَنْ يَّعْصِ اللّٰهَ وَرَسُوْلَهٗ وَيَتَعَدَّ حُدُوْدَهٗ يُدْخِلْهُ نَارًا خَالِدًا فِيْهَاۖ وَلَهٗ عَذَابٌ مُّهِيْنٌ ࣖ

waman ya'*sh*i **al**l*aa*ha warasuulahu wayata'adda *h*uduudahu yudkhilhu n*aa*ran kh*aa*lidan fiih*aa* walahu 'a*dzaa*bun muhiin**un**

Dan barangsiapa mendurhakai Allah dan Rasul-Nya dan melanggar batas-batas hukum-Nya, niscaya Allah memasukkannya ke dalam api neraka, dia kekal di dalamnya dan dia akan mendapat azab yang menghinakan.

4:15

# وَالّٰتِيْ يَأْتِيْنَ الْفَاحِشَةَ مِنْ نِّسَاۤىِٕكُمْ فَاسْتَشْهِدُوْا عَلَيْهِنَّ اَرْبَعَةً مِّنْكُمْ ۚ فَاِنْ شَهِدُوْا فَاَمْسِكُوْهُنَّ فِى الْبُيُوْتِ حَتّٰى يَتَوَفّٰىهُنَّ الْمَوْتُ اَوْ يَجْعَلَ اللّٰهُ لَهُنَّ سَبِيْلًا

wa**a**ll*aa*tii ya/tiina **a**lf*aah*isyata min nis*aa*-ikum fa**i**stasyhiduu 'alayhinna arba'atan minkum fa-in syahiduu fa-amsikuuhunna fii **a**lbuyuuti *h*att*aa* yatawaff

Dan para perempuan yang melakukan perbuatan keji di antara perempuan-perempuan kamu, hendaklah terhadap mereka ada empat orang saksi di antara kamu (yang menyaksikannya). Apabila mereka telah memberi kesaksian, maka kurunglah mereka (perempuan itu) dalam

4:16

# وَالَّذٰنِ يَأْتِيٰنِهَا مِنْكُمْ فَاٰذُوْهُمَا ۚ فَاِنْ تَابَا وَاَصْلَحَا فَاَعْرِضُوْا عَنْهُمَا ۗ اِنَّ اللّٰهَ كَانَ تَوَّابًا رَّحِيْمًا

wa**a**lla*dzaa*ni ya/tiy*aa*nih*aa* minkum fa*aadz*uuhum*aa* fa-in t*aa*b*aa* wa-a*sh*la*haa* fa-a'ri*dh*uu 'anhum*aa* inna **al**l*aa*ha k*aa*na taww*aa*ban

Dan terhadap dua orang yang melakukan perbuatan keji di antara kamu, maka berilah hukuman kepada keduanya. Jika keduanya tobat dan memperbaiki diri, maka biarkanlah mereka. Sungguh, Allah Maha Penerima tobat, Maha Penyayang.

4:17

# اِنَّمَا التَّوْبَةُ عَلَى اللّٰهِ لِلَّذِيْنَ يَعْمَلُوْنَ السُّوْۤءَ بِجَهَالَةٍ ثُمَّ يَتُوْبُوْنَ مِنْ قَرِيْبٍ فَاُولٰۤىِٕكَ يَتُوْبُ اللّٰهُ عَلَيْهِمْ ۗ وَكَانَ اللّٰهُ عَلِيْمًا حَكِيْمًا

innam*aa* **al**ttawbatu 'al*aa* **al**l*aa*hi lilla*dz*iina ya'maluuna **al**ssuu-a bijah*aa*latin tsumma yatuubuuna min qariibin faul*aa*-ika yatuubu **al**l*aa*hu 'a

Sesungguhnya bertobat kepada Allah itu hanya (pantas) bagi mereka yang melakukan kejahatan karena tidak mengerti, kemudian segera bertobat. Tobat mereka itulah yang diterima Allah. Allah Maha Mengetahui, Mahabijaksana.

4:18

# وَلَيْسَتِ التَّوْبَةُ لِلَّذِيْنَ يَعْمَلُوْنَ السَّيِّاٰتِۚ حَتّٰىٓ اِذَا حَضَرَ اَحَدَهُمُ الْمَوْتُ قَالَ اِنِّيْ تُبْتُ الْـٰٔنَ وَلَا الَّذِيْنَ يَمُوْتُوْنَ وَهُمْ كُفَّارٌ ۗ اُولٰۤىِٕكَ اَعْتَدْنَا لَهُمْ عَذَابًا اَلِيْمًا

walaysati **al**ttawbatu lilla*dz*iina ya'maluuna **al**ssayyi-*aa*ti *h*att*aa* i*dzaa* *h*a*dh*ara a*h*adahumu **a**lmawtu q*aa*la innii tubtu **a**l-*aa<*

Dan tobat itu tidaklah (diterima Allah) dari mereka yang melakukan kejahatan hingga apabila datang ajal kepada seseorang di antara mereka, (barulah) dia mengatakan, “Saya benar-benar bertobat sekarang.” Dan tidak (pula diterima tobat) dari orang-orang yan

4:19

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْا لَا يَحِلُّ لَكُمْ اَنْ تَرِثُوا النِّسَاۤءَ كَرْهًا ۗ وَلَا تَعْضُلُوْهُنَّ لِتَذْهَبُوْا بِبَعْضِ مَآ اٰتَيْتُمُوْهُنَّ اِلَّآ اَنْ يَّأْتِيْنَ بِفَاحِشَةٍ مُّبَيِّنَةٍ ۚ وَعَاشِرُوْهُنَّ بِالْمَعْرُوْفِ ۚ فَاِنْ كَرِه

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu l*aa* ya*h*illu lakum an taritsuu **al**nnis*aa*-a karhan wal*aa* ta'*dh*uluuhunna lita*dz*habuu biba'*dh*i m*aa* *aa*tay

Wahai orang-orang beriman! Tidak halal bagi kamu mewarisi perempuan dengan jalan paksa dan janganlah kamu menyusahkan mereka karena hendak mengambil kembali sebagian dari apa yang telah kamu berikan kepadanya, kecuali apabila mereka melakukan perbuatan ke

4:20

# وَاِنْ اَرَدْتُّمُ اسْتِبْدَالَ زَوْجٍ مَّكَانَ زَوْجٍۙ وَّاٰتَيْتُمْ اِحْدٰىهُنَّ قِنْطَارًا فَلَا تَأْخُذُوْا مِنْهُ شَيْـًٔا ۗ اَتَأْخُذُوْنَهٗ بُهْتَانًا وَّاِثْمًا مُّبِيْنًا

wa-in aradtumu istibd*aa*la zawjin mak*aa*na zawjin wa*aa*taytum i*h*d*aa*hunna qin*thaa*ran fal*aa* ta/khu*dz*uu minhu syay-an ata/khu*dz*uunahu buht*aa*nan wa-itsman mubiin*aa**\*n**

**Dan jika kamu ingin mengganti istrimu dengan istri yang lain, sedang kamu telah memberikan kepada seorang di antara mereka harta yang banyak, maka janganlah kamu mengambil kembali sedikit pun darinya. Apakah kamu akan mengambilnya kembali dengan jalan tud**

4:21

# وَكَيْفَ تَأْخُذُوْنَهٗ وَقَدْ اَفْضٰى بَعْضُكُمْ اِلٰى بَعْضٍ وَّاَخَذْنَ مِنْكُمْ مِّيْثَاقًا غَلِيْظًا

wakayfa ta/khu*dz*uunahu waqad af*daa* ba'*dh*ukum il*aa* ba'*dh*in wa-akha*dz*na minkum miits*aa*qan ghalii*zhaa**\*n**

Dan bagaimana kamu akan mengambilnya kembali, padahal kamu telah bergaul satu sama lain (sebagai suami-istri). Dan mereka (istri-istrimu) telah mengambil perjanjian yang kuat (ikatan pernikahan) dari kamu.

4:22

# وَلَا تَنْكِحُوْا مَا نَكَحَ اٰبَاۤؤُكُمْ مِّنَ النِّسَاۤءِ اِلَّا مَا قَدْ سَلَفَ ۗ اِنَّهٗ كَانَ فَاحِشَةً وَّمَقْتًاۗ وَسَاۤءَ سَبِيْلًا ࣖ

wal*aa* tanki*h*uu m*aa* naka*h*a *aa*b*aa*ukum mina **al**nnis*aa*-i ill*aa* m*aa* qad salafa innahu k*aa*na f*aah*isyatan wamaqtan was*aa*-a sabiil*aa**\*n**

Dan janganlah kamu menikahi perempuan-perempuan yang telah dinikahi oleh ayahmu, kecuali (kejadian pada masa) yang telah lampau. Sungguh, perbuatan itu sangat keji dan dibenci (oleh Allah) dan seburuk-buruk jalan (yang ditempuh).

4:23

# حُرِّمَتْ عَلَيْكُمْ اُمَّهٰتُكُمْ وَبَنٰتُكُمْ وَاَخَوٰتُكُمْ وَعَمّٰتُكُمْ وَخٰلٰتُكُمْ وَبَنٰتُ الْاَخِ وَبَنٰتُ الْاُخْتِ وَاُمَّهٰتُكُمُ الّٰتِيْٓ اَرْضَعْنَكُمْ وَاَخَوٰتُكُمْ مِّنَ الرَّضَاعَةِ وَاُمَّهٰتُ نِسَاۤىِٕكُمْ وَرَبَاۤىِٕبُكُمُ الّٰتِيْ ف

*h*urrimat 'alaykum ummah*aa*tukum waban*aa*tukum wa-akhaw*aa*tukum wa'amm*aa*tukum wakh*aa*l*aa*tukum waban*aa*tu **a**l-akhi waban*aa*tu **a**l-ukhti waummah*aa*tukumu **a**

Diharamkan atas kamu (menikahi) ibu-ibumu, anak-anakmu yang perempuan, saudara-saudaramu yang perempuan, saudara-saudara ayahmu yang perempuan, saudara-saudara ibumu yang perempuan, anak-anak perempuan dari saudara-saudaramu yang laki-laki, anak-anak pere

4:24

# ۞ وَالْمُحْصَنٰتُ مِنَ النِّسَاۤءِ اِلَّا مَا مَلَكَتْ اَيْمَانُكُمْ ۚ كِتٰبَ اللّٰهِ عَلَيْكُمْ ۚ وَاُحِلَّ لَكُمْ مَّا وَرَاۤءَ ذٰلِكُمْ اَنْ تَبْتَغُوْا بِاَمْوَالِكُمْ مُّحْصِنِيْنَ غَيْرَ مُسَافِحِيْنَ ۗ فَمَا اسْتَمْتَعْتُمْ بِهٖ مِنْهُنَّ فَاٰتُوْ

wa**a**lmu*hs*an*aa*tu mina **al**nnis*aa*-i ill*aa* m*aa* malakat aym*aa*nukum kit*aa*ba **al**l*aa*hi 'alaykum wau*h*illa lakum m*aa* war*aa*-a *dzaa*liku

Dan (diharamkan juga kamu menikahi) perempuan yang bersuami, kecuali hamba sahaya perempuan (tawanan perang) yang kamu miliki sebagai ketetapan Allah atas kamu. Dan dihalalkan bagimu selain (perempuan-perempuan) yang demikian itu jika kamu berusaha dengan

4:25

# وَمَنْ لَّمْ يَسْتَطِعْ مِنْكُمْ طَوْلًا اَنْ يَّنْكِحَ الْمُحْصَنٰتِ الْمُؤْمِنٰتِ فَمِنْ مَّا مَلَكَتْ اَيْمَانُكُمْ مِّنْ فَتَيٰتِكُمُ الْمُؤْمِنٰتِۗ وَاللّٰهُ اَعْلَمُ بِاِيْمَانِكُمْ ۗ بَعْضُكُمْ مِّنْۢ بَعْضٍۚ فَانْكِحُوْهُنَّ بِاِذْنِ اَهْلِهِنَّ و

waman lam yasta*th*i' minkum *th*awlan an yanki*h*a **a**lmu*hs*an*aa*ti **a**lmu/min*aa*ti famin m*aa* malakat aym*aa*nukum min fatay*aa*tikumu **a**lmu/min*aa*ti wa

Dan barangsiapa di antara kamu tidak mempunyai biaya untuk menikahi perempuan merdeka yang beriman, maka (dihalalkan menikahi perempuan) yang beriman dari hamba sahaya yang kamu miliki. Allah mengetahui keimananmu. Sebagian dari kamu adalah dari sebagian

4:26

# يُرِيْدُ اللّٰهُ لِيُبَيِّنَ لَكُمْ وَيَهْدِيَكُمْ سُنَنَ الَّذِيْنَ مِنْ قَبْلِكُمْ وَيَتُوْبَ عَلَيْكُمْ ۗ وَاللّٰهُ عَلِيْمٌ حَكِيْمٌ

yuriidu **al**l*aa*hu liyubayyina lakum wayahdiyakum sunana **al**la*dz*iina min qablikum wayatuuba 'alaykum wa**al**l*aa*hu 'aliimun *h*akiim**un**

Allah hendak menerangkan (syariat-Nya) kepadamu, dan menunjukkan jalan-jalan (kehidupan) orang yang sebelum kamu (para nabi dan orang-orang saleh) dan Dia menerima tobatmu. Allah Maha Mengetahui, Mahabijaksana.

4:27

# وَاللّٰهُ يُرِيْدُ اَنْ يَّتُوْبَ عَلَيْكُمْ ۗ وَيُرِيْدُ الَّذِيْنَ يَتَّبِعُوْنَ الشَّهَوٰتِ اَنْ تَمِيْلُوْا مَيْلًا عَظِيْمًا

wa**al**l*aa*hu yuriidu an yatuuba 'alaykum wayuriidu **al**la*dz*iina yattabi'uuna **al**sysyahaw*aa*ti an tamiiluu maylan 'a*zh*iim**an**v

Dan Allah hendak menerima tobatmu, sedang orang-orang yang mengikuti keinginannya menghendaki agar kamu berpaling sejauh-jauhnya (dari kebenaran).

4:28

# يُرِيْدُ اللّٰهُ اَنْ يُّخَفِّفَ عَنْكُمْ ۚ وَخُلِقَ الْاِنْسَانُ ضَعِيْفًا

yuriidu **al**l*aa*hu an yukhaffifa 'ankum wakhuliqa **a**l-ins*aa*nu *dh*a'iif*aa**\*n**

Allah hendak memberikan keringanan kepadamu, karena manusia diciptakan (bersifat) lemah.

4:29

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْا لَا تَأْكُلُوْٓا اَمْوَالَكُمْ بَيْنَكُمْ بِالْبَاطِلِ اِلَّآ اَنْ تَكُوْنَ تِجَارَةً عَنْ تَرَاضٍ مِّنْكُمْ ۗ وَلَا تَقْتُلُوْٓا اَنْفُسَكُمْ ۗ اِنَّ اللّٰهَ كَانَ بِكُمْ رَحِيْمًا

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu l*aa* ta/kuluu amw*aa*lakum baynakum bi**a**lb*aath*ili ill*aa* an takuuna tij*aa*ratan 'an tar*aad*in minkum wal*aa* taqtuluu anfu

Wahai orang-orang yang beriman! Janganlah kamu saling memakan harta sesamamu dengan jalan yang batil (tidak benar), kecuali dalam perdagangan yang berlaku atas dasar suka sama suka di antara kamu. Dan janganlah kamu membunuh dirimu. Sungguh, Allah Maha Pe

4:30

# وَمَنْ يَّفْعَلْ ذٰلِكَ عُدْوَانًا وَّظُلْمًا فَسَوْفَ نُصْلِيْهِ نَارًا ۗوَكَانَ ذٰلِكَ عَلَى اللّٰهِ يَسِيْرًا

waman yaf'al *dzaa*lika 'udw*aa*nan wa*zh*ulman fasawfa nu*sh*liihi n*aa*ran wak*aa*na *dzaa*lika 'al*aa* **al**l*aa*hi yasiir*aa**\*n**

Dan barangsiapa berbuat demikian dengan cara melanggar hukum dan zalim, akan Kami masukkan dia ke dalam neraka. Yang demikian itu mudah bagi Allah.

4:31

# اِنْ تَجْتَنِبُوْا كَبَاۤىِٕرَ مَا تُنْهَوْنَ عَنْهُ نُكَفِّرْ عَنْكُمْ سَيِّاٰتِكُمْ وَنُدْخِلْكُمْ مُّدْخَلًا كَرِيْمًا

in tajtanibuu kab*aa*-ira m*aa* tunhawna 'anhu nukaffir 'ankum sayyi-*aa*tikum wanudkhilkum mudkhalan kariim*aa**\*n**

Jika kamu menjauhi dosa-dosa besar di antara dosa-dosa yang dilarang mengerjakannya, niscaya Kami hapus kesalahan-kesalahanmu dan akan Kami masukkan kamu ke tempat yang mulia (surga).

4:32

# وَلَا تَتَمَنَّوْا مَا فَضَّلَ اللّٰهُ بِهٖ بَعْضَكُمْ عَلٰى بَعْضٍ ۗ لِلرِّجَالِ نَصِيْبٌ مِّمَّا اكْتَسَبُوْا ۗ وَلِلنِّسَاۤءِ نَصِيْبٌ مِّمَّا اكْتَسَبْنَ ۗوَسْـَٔلُوا اللّٰهَ مِنْ فَضْلِهٖ ۗ اِنَّ اللّٰهَ كَانَ بِكُلِّ شَيْءٍ عَلِيْمًا

wal*aa* tatamannaw m*aa* fa*dhdh*ala **al**l*aa*hu bihi ba'*dh*akum 'al*aa* ba'*dh*in li**l**rrij*aa*li na*sh*iibun mimm*aa* iktasabuu wali**l**nnis*aa*-i na*s*

Dan janganlah kamu iri hati terhadap karunia yang telah dilebihkan Allah kepada sebagian kamu atas sebagian yang lain. (Karena) bagi laki-laki ada bagian dari apa yang mereka usahakan, dan bagi perempuan (pun) ada bagian dari apa yang mereka usahakan. Moh

4:33

# وَلِكُلٍّ جَعَلْنَا مَوَالِيَ مِمَّا تَرَكَ الْوَالِدٰنِ وَالْاَقْرَبُوْنَ ۗ وَالَّذِيْنَ عَقَدَتْ اَيْمَانُكُمْ فَاٰتُوْهُمْ نَصِيْبَهُمْ ۗ اِنَّ اللّٰهَ كَانَ عَلٰى كُلِّ شَيْءٍ شَهِيْدًا ࣖ

walikullin ja'aln*aa* maw*aa*liya mimm*aa* taraka **a**lw*aa*lid*aa*ni wa**a**l-aqrabuuna wa**a**lla*dz*iina 'aqadat aym*aa*nukum fa*aa*tuuhum na*sh*iibahum inna **a**

Dan untuk masing-masing (laki-laki dan perempuan) Kami telah menetapkan para ahli waris atas apa yang ditinggalkan oleh kedua orang tuanya dan karib kerabatnya. Dan orang-orang yang kamu telah bersumpah setia dengan mereka, maka berikanlah kepada mereka b

4:34

# اَلرِّجَالُ قَوَّامُوْنَ عَلَى النِّسَاۤءِ بِمَا فَضَّلَ اللّٰهُ بَعْضَهُمْ عَلٰى بَعْضٍ وَّبِمَآ اَنْفَقُوْا مِنْ اَمْوَالِهِمْ ۗ فَالصّٰلِحٰتُ قٰنِتٰتٌ حٰفِظٰتٌ لِّلْغَيْبِ بِمَا حَفِظَ اللّٰهُ ۗوَالّٰتِيْ تَخَافُوْنَ نُشُوْزَهُنَّ فَعِظُوْهُنَّ وَاهْج

a**l**rrij*aa*lu qaww*aa*muuna 'al*aa* **al**nnis*aa*-i bim*aa* fa*dhdh*ala **al**l*aa*hu ba'*dh*ahum 'al*aa* ba'*dh*in wabim*aa* anfaquu min amw*aa*lihim f

Laki-laki (suami) itu pelindung bagi perempuan (istri), karena Allah telah melebihkan sebagian mereka (laki-laki) atas sebagian yang lain (perempuan), dan karena mereka (laki-laki) telah memberikan nafkah dari hartanya. Maka perempuan-perempuan yang saleh

4:35

# وَاِنْ خِفْتُمْ شِقَاقَ بَيْنِهِمَا فَابْعَثُوْا حَكَمًا مِّنْ اَهْلِهٖ وَحَكَمًا مِّنْ اَهْلِهَا ۚ اِنْ يُّرِيْدَآ اِصْلَاحًا يُّوَفِّقِ اللّٰهُ بَيْنَهُمَا ۗ اِنَّ اللّٰهَ كَانَ عَلِيْمًا خَبِيْرًا

wa-in khiftum syiq*aa*qa baynihim*aa* fa**i**b'atsuu *h*akaman min ahlihi wa*h*akaman min ahlih*aa* in yuriid*aa* i*sh*l*aah*an yuwaffiqi **al**l*aa*hu baynahum*aa* inna **a**

Dan jika kamu khawatir terjadi persengketaan antara keduanya, maka kirimlah seorang juru damai dari keluarga laki-laki dan seorang juru damai dari keluarga perempuan. Jika keduanya (juru damai itu) bermaksud mengadakan perbaikan, niscaya Allah memberi tau

4:36

# ۞ وَاعْبُدُوا اللّٰهَ وَلَا تُشْرِكُوْا بِهٖ شَيْـًٔا وَّبِالْوَالِدَيْنِ اِحْسَانًا وَّبِذِى الْقُرْبٰى وَالْيَتٰمٰى وَالْمَسٰكِيْنِ وَالْجَارِ ذِى الْقُرْبٰى وَالْجَارِ الْجُنُبِ وَالصَّاحِبِ بِالْجَنْۢبِ وَابْنِ السَّبِيْلِۙ وَمَا مَلَكَتْ اَيْمَانُكُم

wa**u**'buduu **al**l*aa*ha wal*aa* tusyrikuu bihi syay-an wabi**a**lw*aa*lidayni i*h*s*aa*nan wabi*dz*ii **a**lqurb*aa* wa**a**lyat*aa*m*aa* wa<

Dan sembahlah Allah dan janganlah kamu mempersekutukan-Nya dengan sesuatu apa pun. Dan berbuat-baiklah kepada kedua orang tua, karib-kerabat, anak-anak yatim, orang-orang miskin, tetangga dekat dan tetangga jauh, teman sejawat, ibnu sabil dan hamba sahaya

4:37

# ۨالَّذِيْنَ يَبْخَلُوْنَ وَيَأْمُرُوْنَ النَّاسَ بِالْبُخْلِ وَيَكْتُمُوْنَ مَآ اٰتٰىهُمُ اللّٰهُ مِنْ فَضْلِهٖۗ وَاَعْتَدْنَا لِلْكٰفِرِيْنَ عَذَابًا مُّهِيْنًاۚ

**al**la*dz*iina yabkhaluuna waya/muruuna **al**nn*aa*sa bi**a**lbukhli wayaktumuuna m*aa* *aa*t*aa*humu **al**l*aa*hu min fa*dh*lihi wa-a'tadn*aa* lilk*aa*fi

(yaitu) orang yang kikir, dan menyuruh orang lain berbuat kikir, dan menyembunyikan karunia yang telah diberikan Allah kepadanya. Kami telah menyediakan untuk orang-orang kafir azab yang menghinakan.

4:38

# وَالَّذِيْنَ يُنْفِقُوْنَ اَمْوَالَهُمْ رِئَاۤءَ النَّاسِ وَلَا يُؤْمِنُوْنَ بِاللّٰهِ وَلَا بِالْيَوْمِ الْاٰخِرِ ۗ وَمَنْ يَّكُنِ الشَّيْطٰنُ لَهٗ قَرِيْنًا فَسَاۤءَ قَرِيْنًا

wa**a**lla*dz*iina yunfiquuna amw*aa*lahum ri-*aa*-a **al**nn*aa*si wal*aa* yu/minuuna bi**al**l*aa*hi wal*aa* bi**a**lyawmi **a**l-*aa*khiri waman ya

Dan (juga) orang-orang yang menginfakkan hartanya karena ria dan kepada orang lain (ingin dilihat dan dipuji), dan orang-orang yang tidak beriman kepada Allah dan kepada hari kemudian. Barangsiapa menjadikan setan sebagai temannya, maka (ketahuilah) dia (

4:39

# وَمَاذَا عَلَيْهِمْ لَوْ اٰمَنُوْا بِاللّٰهِ وَالْيَوْمِ الْاٰخِرِ وَاَنْفَقُوْا مِمَّا رَزَقَهُمُ اللّٰهُ ۗوَكَانَ اللّٰهُ بِهِمْ عَلِيْمًا

wam*aatsaa* 'alayhim law *aa*manuu bi**al**l*aa*hi wa**a**lyawmi **a**l-*aa*khiri wa-anfaquu mimm*aa* razaqahumu **al**l*aa*hu wak*aa*na **al**l*aa*hu

Dan apa (keberatan) bagi mereka jika mereka beriman kepada Allah dan hari kemudian dan menginfakkan sebagian rezeki yang telah diberikan Allah kepadanya? Dan Allah Maha Mengetahui keadaan mereka.

4:40

# اِنَّ اللّٰهَ لَا يَظْلِمُ مِثْقَالَ ذَرَّةٍ ۚوَاِنْ تَكُ حَسَنَةً يُّضٰعِفْهَا وَيُؤْتِ مِنْ لَّدُنْهُ اَجْرًا عَظِيْمًا

inna **al**l*aa*ha l*aa* ya*zh*limu mitsq*aa*la *dz*arratin wa-in taku *h*asanatan yu*daa*'ifh*aa* wayu/ti min ladunhu ajran 'a*zh*iim*aa**\*n**

Sungguh, Allah tidak akan menzalimi seseorang walaupun sebesar dzarrah, dan jika ada kebajikan (sekecil dzarrah), niscaya Allah akan melipatgandakannya dan memberikan pahala yang besar dari sisi-Nya.

4:41

# فَكَيْفَ اِذَا جِئْنَا مِنْ كُلِّ اُمَّةٍۢ بِشَهِيْدٍ وَّجِئْنَا بِكَ عَلٰى هٰٓؤُلَاۤءِ شَهِيْدًاۗ

fakayfa i*dzaa* ji/n*aa* min kulli ummatin bisyahiidin waji/n*aa* bika 'al*aa* h*aa*ul*aa*-i syahiid*aa**\*n**

Dan bagaimanakah (keadaan orang kafir nanti), jika Kami mendatangkan seorang saksi (Rasul) dari setiap umat dan Kami mendatangkan engkau (Muhammad) sebagai saksi atas mereka.

4:42

# يَوْمَىِٕذٍ يَّوَدُّ الَّذِيْنَ كَفَرُوْا وَعَصَوُا الرَّسُوْلَ لَوْ تُسَوّٰى بِهِمُ الْاَرْضُۗ وَلَا يَكْتُمُوْنَ اللّٰهَ حَدِيْثًا ࣖ

yawma-i*dz*in yawaddu **al**la*dz*iina kafaruu wa'a*sh*awuu **al**rrasuula law tusaww*aa* bihimu **a**l-ar*dh*u wal*aa* yaktumuuna **al**l*aa*ha *h*adiits*aa*

Pada hari itu, orang yang kafir dan orang yang mendurhakai Rasul (Muhammad), berharap sekiranya mereka diratakan dengan tanah (dikubur atau hancur luluh menjadi tanah), padahal mereka tidak dapat menyembunyikan sesuatu kejadian apa pun dari Allah.

4:43

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْا لَا تَقْرَبُوا الصَّلٰوةَ وَاَنْتُمْ سُكَارٰى حَتّٰى تَعْلَمُوْا مَا تَقُوْلُوْنَ وَلَا جُنُبًا اِلَّا عَابِرِيْ سَبِيْلٍ حَتّٰى تَغْتَسِلُوْا ۗوَاِنْ كُنْتُمْ مَّرْضٰٓى اَوْ عَلٰى سَفَرٍ اَوْ جَاۤءَ اَحَدٌ مِّنْكُمْ مِّنَ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu l*aa* taqrabuu **al***shsh*al*aa*ta wa-antum suk*aa*r*aa* *h*att*aa* ta'lamuu m*aa* taquuluuna wal*aa* junuban ill*aa*

*Wahai orang yang beriman! Janganlah kamu mendekati salat ketika kamu dalam keadaan mabuk, sampai kamu sadar apa yang kamu ucapkan, dan jangan pula (kamu hampiri masjid ketika kamu) dalam keadaan junub kecuali sekedar melewati jalan saja, sebelum kamu mand*

4:44

# اَلَمْ تَرَ اِلَى الَّذِيْنَ اُوْتُوْا نَصِيْبًا مِّنَ الْكِتٰبِ يَشْتَرُوْنَ الضَّلٰلَةَ وَيُرِيْدُوْنَ اَنْ تَضِلُّوا السَّبِيْلَۗ

alam tara il*aa* **al**la*dz*iina uutuu na*sh*iiban mina **a**lkit*aa*bi yasytaruuna **al***dhdh*al*aa*lata wayuriiduuna an ta*dh*illuu **al**ssabiil**a**

Tidakkah kamu memperhatikan orang yang telah diberi bagian Kitab (Taurat)? Mereka membeli kesesatan dan mereka menghendaki agar kamu tersesat (menyimpang) dari jalan (yang benar).

4:45

# وَاللّٰهُ اَعْلَمُ بِاَعْدَاۤىِٕكُمْ ۗوَكَفٰى بِاللّٰهِ وَلِيًّا ۙوَّكَفٰى بِاللّٰهِ نَصِيْرًا

wa**al**l*aa*hu a'lamu bi-a'd*aa*-ikum wakaf*aa* bi**al**l*aa*hi waliyyan wakaf*aa* bi**al**l*aa*hi na*sh*iir*aa**\*n**

Dan Allah lebih mengetahui tentang musuh-musuhmu. Cukuplah Allah menjadi pelindung dan cukuplah Allah menjadi penolong (bagimu).

4:46

# مِنَ الَّذِيْنَ هَادُوْا يُحَرِّفُوْنَ الْكَلِمَ عَنْ مَّوَاضِعِهٖ وَيَقُوْلُوْنَ سَمِعْنَا وَعَصَيْنَا وَاسْمَعْ غَيْرَ مُسْمَعٍ وَّرَاعِنَا لَيًّاۢ بِاَلْسِنَتِهِمْ وَطَعْنًا فِى الدِّيْنِۗ وَلَوْ اَنَّهُمْ قَالُوْا سَمِعْنَا وَاَطَعْنَا وَاسْمَعْ وَانْ

mina **al**la*dz*iina h*aa*duu yu*h*arrifuuna **a**lkalima 'an maw*aad*i'ihi wayaquuluuna sami'n*aa* wa'a*sh*ayn*aa* wa**i**sma' ghayra musma'in war*aa*'in*aa* layyan bi-a

(Yaitu) di antara orang Yahudi, yang mengubah perkataan dari tempat-tempatnya. Dan mereka berkata, “Kami mendengar, tetapi kami tidak mau menurutinya.” Dan (mereka mengatakan pula), “Dengarlah,” sedang (engkau Muhammad sebenarnya) tidak mendengar apa pun.

4:47

# يٰٓاَيُّهَا الَّذِيْنَ اُوْتُوا الْكِتٰبَ اٰمِنُوْا بِمَا نَزَّلْنَا مُصَدِّقًا لِّمَا مَعَكُمْ مِّنْ قَبْلِ اَنْ نَّطْمِسَ وُجُوْهًا فَنَرُدَّهَا عَلٰٓى اَدْبَارِهَآ اَوْ نَلْعَنَهُمْ كَمَا لَعَنَّآ اَصْحٰبَ السَّبْتِ ۗ وَكَانَ اَمْرُ اللّٰهِ مَفْعُوْل

y*aa* ayyuh*aa* **al**la*dz*iina uutuu **a**lkit*aa*ba *aa*minuu bim*aa* nazzaln*aa* mu*sh*addiqan lim*aa* ma'akum min qabli an na*th*misa wujuuhan fanaruddah*aa* 'al*aa*

*Wahai orang-orang yang telah diberi Kitab! Berimanlah kamu kepada apa yang telah Kami turunkan (Al-Qur'an) yang membenarkan Kitab yang ada pada kamu, sebelum Kami mengubah wajah-wajah(mu), lalu Kami putar ke belakang atau Kami laknat mereka sebagaimana Ka*

4:48

# اِنَّ اللّٰهَ لَا يَغْفِرُ اَنْ يُّشْرَكَ بِهٖ وَيَغْفِرُ مَا دُوْنَ ذٰلِكَ لِمَنْ يَّشَاۤءُ ۚ وَمَنْ يُّشْرِكْ بِاللّٰهِ فَقَدِ افْتَرٰٓى اِثْمًا عَظِيْمًا

inna **al**l*aa*ha l*aa* yaghfiru an yusyraka bihi wayaghfiru m*aa* duuna *dzaa*lika liman yasy*aa*u waman yusyrik bi**al**l*aa*hi faqadi iftar*aa* itsman 'a*zh*iim*aa**\*n**

**Sesungguhnya Allah tidak akan mengampuni (dosa) karena mempersekutukan-Nya (syirik), dan Dia mengampuni apa (dosa) yang selain (syirik) itu bagi siapa yang Dia kehendaki. Barangsiapa mempersekutukan Allah, maka sungguh, dia telah berbuat dosa yang besar.**

4:49

# اَلَمْ تَرَ اِلَى الَّذِيْنَ يُزَكُّوْنَ اَنْفُسَهُمْ ۗ بَلِ اللّٰهُ يُزَكِّيْ مَنْ يَّشَاۤءُ وَلَا يُظْلَمُوْنَ فَتِيْلًا

alam tara il*aa* **al**la*dz*iina yuzakkuuna anfusahum bali **al**l*aa*hu yuzakkii man yasy*aa*u wal*aa* yu*zh*lamuuna fatiil*aa**\*n**

Tidakkah engkau memperhatikan orang-orang yang menganggap dirinya suci (orang Yahudi dan Nasrani)? Sebenarnya Allah menyucikan siapa yang Dia kehendaki dan mereka tidak dizalimi sedikit pun.

4:50

# اُنْظُرْ كَيْفَ يَفْتَرُوْنَ عَلَى اللّٰهِ الْكَذِبَۗ وَكَفٰى بِهٖٓ اِثْمًا مُّبِيْنًا ࣖ

un*zh*ur kayfa yaftaruuna 'al*aa* **al**l*aa*hi **a**lka*dz*iba wakaf*aa* bihi itsman mubiin*aa**\*n**

Perhatikanlah, betapa mereka mengada-adakan dusta terhadap Allah! Dan cukuplah perbuatan itu menjadi dosa yang nyata (bagi mereka).

4:51

# اَلَمْ تَرَ اِلَى الَّذِيْنَ اُوْتُوْا نَصِيْبًا مِّنَ الْكِتٰبِ يُؤْمِنُوْنَ بِالْجِبْتِ وَالطَّاغُوْتِ وَيَقُوْلُوْنَ لِلَّذِيْنَ كَفَرُوْا هٰٓؤُلَاۤءِ اَهْدٰى مِنَ الَّذِيْنَ اٰمَنُوْا سَبِيْلًا

alam tara il*aa* **al**la*dz*iina uutuu na*sh*iiban mina **a**lkit*aa*bi yu/minuuna bi**a**ljibti wa**al***ththaa*ghuuti wayaquuluuna lilla*dz*iina kafaruu h*aa*ul*aa*

Tidakkah kamu memperhatikan orang-orang yang diberi bagian dari Kitab (Taurat)? Mereka percaya kepada Jibt dan thaghut, dan mengatakan kepada orang-orang kafir (musyrik Mekah), bahwa mereka itu lebih benar jalannya daripada orang-orang yang beriman.

4:52

# اُولٰۤىِٕكَ الَّذِيْنَ لَعَنَهُمُ اللّٰهُ ۗوَمَنْ يَّلْعَنِ اللّٰهُ فَلَنْ تَجِدَ لَهٗ نَصِيْرًا

ul*aa*-ika **al**la*dz*iina la'anahumu **al**l*aa*hu waman yal'ani **al**l*aa*hu falan tajida lahu na*sh*iir*aa**\*n**

Mereka itulah orang-orang yang dilaknat Allah. Dan barangsiapa dilaknat Allah, niscaya engkau tidak akan mendapatkan penolong baginya.

4:53

# اَمْ لَهُمْ نَصِيْبٌ مِّنَ الْمُلْكِ فَاِذًا لَّا يُؤْتُوْنَ النَّاسَ نَقِيْرًاۙ

am lahum na*sh*iibun mina **a**lmulki fa-i*dz*an l*aa* yu/tuuna **al**nn*aa*sa naqiir*aa**\*n**

Ataukah mereka mempunyai bagian dari kerajaan (kekuasaan), meskipun mereka tidak akan memberikan sedikit pun (kebajikan) kepada manusia,

4:54

# اَمْ يَحْسُدُوْنَ النَّاسَ عَلٰى مَآ اٰتٰىهُمُ اللّٰهُ مِنْ فَضْلِهٖۚ فَقَدْ اٰتَيْنَآ اٰلَ اِبْرٰهِيْمَ الْكِتٰبَ وَالْحِكْمَةَ وَاٰتَيْنٰهُمْ مُّلْكًا عَظِيْمًا

am ya*h*suduuna **al**nn*aa*sa 'al*aa* m*aa* *aa*t*aa*humu **al**l*aa*hu min fa*dh*lihi faqad *aa*tayn*aa* *aa*la ibr*aa*hiima **a**lkit*aa*ba wa

ataukah mereka dengki kepada manusia (Muhammad) karena karunia yang telah diberikan Allah kepadanya? Sungguh, Kami telah memberikan Kitab dan Hikmah kepada keluarga Ibrahim, dan Kami telah memberikan kepada mereka kerajaan (kekuasaan) yang besar.

4:55

# فَمِنْهُمْ مَّنْ اٰمَنَ بِهٖ وَمِنْهُمْ مَّنْ صَدَّ عَنْهُ ۗ وَكَفٰى بِجَهَنَّمَ سَعِيْرًا

faminhum man *aa*mana bihi waminhum man *sh*adda 'anhu wakaf*aa* bijahannama sa'iir*aa**\*n**

Maka di antara mereka (yang dengki itu), ada yang beriman kepadanya dan ada pula yang menghalangi (manusia beriman) kepadanya. Cukuplah (bagi mereka) neraka Jahanam yang menyala-nyala apinya.

4:56

# اِنَّ الَّذِيْنَ كَفَرُوْا بِاٰيٰتِنَا سَوْفَ نُصْلِيْهِمْ نَارًاۗ كُلَّمَا نَضِجَتْ جُلُوْدُهُمْ بَدَّلْنٰهُمْ جُلُوْدًا غَيْرَهَا لِيَذُوْقُوا الْعَذَابَۗ اِنَّ اللّٰهَ كَانَ عَزِيْزًا حَكِيْمًا

inna **al**la*dz*iina kafaruu bi-*aa*y*aa*tin*aa* sawfa nu*sh*liihim n*aa*ran kullam*aa* na*dh*ijat juluuduhum baddaln*aa*hum juluudan ghayrah*aa* liya*dz*uuquu **a**l'a*dz*

Sungguh, orang-orang yang kafir kepada ayat-ayat Kami, kelak akan Kami masukkan ke dalam neraka. Setiap kali kulit mereka hangus, Kami ganti dengan kulit yang lain, agar mereka merasakan azab. Sungguh, Allah Maha-perkasa, Mahabijaksana.

4:57

# وَالَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِ سَنُدْخِلُهُمْ جَنّٰتٍ تَجْرِيْ مِنْ تَحْتِهَا الْاَنْهٰرُ خٰلِدِيْنَ فِيْهَآ اَبَدًاۗ لَهُمْ فِيْهَآ اَزْوَاجٌ مُّطَهَّرَةٌ ۙ وَّنُدْخِلُهُمْ ظِلًّا ظَلِيْلًا

wa**a**lla*dz*iina *aa*manuu wa'amiluu **al***shshaa*li*haa*ti sanudkhiluhum jann*aa*tin tajrii min ta*h*tih*aa* **a**l-anh*aa*ru kh*aa*lidiina fiih*aa* abadan lahum

Adapun orang-orang yang beriman dan mengerjakan kebajikan, kelak akan Kami masukkan ke dalam surga yang mengalir di bawahnya sungai-sungai, mereka kekal di dalamnya selama-lamanya. Di sana mereka mempunyai pasangan-pasangan yang suci, dan Kami masukkan me

4:58

# ۞ اِنَّ اللّٰهَ يَأْمُرُكُمْ اَنْ تُؤَدُّوا الْاَمٰنٰتِ اِلٰٓى اَهْلِهَاۙ وَاِذَا حَكَمْتُمْ بَيْنَ النَّاسِ اَنْ تَحْكُمُوْا بِالْعَدْلِ ۗ اِنَّ اللّٰهَ نِعِمَّا يَعِظُكُمْ بِهٖ ۗ اِنَّ اللّٰهَ كَانَ سَمِيْعًاۢ بَصِيْرًا

inna **al**l*aa*ha ya/murukum an tu-adduu **a**l-am*aa*n*aa*ti il*aa* ahlih*aa* wa-i*dzaa* *h*akamtum bayna **al**nn*aa*si an ta*h*kumuu bi**a**l'adli inna

Sungguh, Allah menyuruhmu menyampaikan amanat kepada yang berhak menerimanya, dan apabila kamu menetapkan hukum di antara manusia hendaknya kamu menetapkannya dengan adil. Sungguh, Allah sebaik-baik yang memberi pengajaran kepadamu. Sungguh, Allah Maha Me

4:59

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْٓا اَطِيْعُوا اللّٰهَ وَاَطِيْعُوا الرَّسُوْلَ وَاُولِى الْاَمْرِ مِنْكُمْۚ فَاِنْ تَنَازَعْتُمْ فِيْ شَيْءٍ فَرُدُّوْهُ اِلَى اللّٰهِ وَالرَّسُوْلِ اِنْ كُنْتُمْ تُؤْمِنُوْنَ بِاللّٰهِ وَالْيَوْمِ الْاٰخِرِۗ ذٰلِكَ خَيْرٌ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu a*th*ii'uu **al**l*aa*ha wa-a*th*ii'uu **al**rrasuula waulii **a**l-amri minkum fa-in tan*aa*za'tum fii syay-in farudduu

Wahai orang-orang yang beriman! Taatilah Allah dan taatilah Rasul (Muhammad), dan Ulil Amri (pemegang kekuasaan) di antara kamu. Kemudian, jika kamu berbeda pendapat tentang sesuatu, maka kembalikanlah kepada Allah (Al-Qur'an) dan Rasul (sunnahnya), jika

4:60

# اَلَمْ تَرَ اِلَى الَّذِيْنَ يَزْعُمُوْنَ اَنَّهُمْ اٰمَنُوْا بِمَآ اُنْزِلَ اِلَيْكَ وَمَآ اُنْزِلَ مِنْ قَبْلِكَ يُرِيْدُوْنَ اَنْ يَّتَحَاكَمُوْٓا اِلَى الطَّاغُوْتِ وَقَدْ اُمِرُوْٓا اَنْ يَّكْفُرُوْا بِهٖ ۗوَيُرِيْدُ الشَّيْطٰنُ اَنْ يُّضِلَّهُمْ ض

alam tara il*aa* **al**la*dz*iina yaz'umuuna annahum *aa*manuu bim*aa* unzila ilayka wam*aa* unzila min qablika yuriiduuna an yata*haa*kamuu il*aa* **al***ththaa*ghuuti waqad umiruu an yakf

Tidakkah engkau (Muhammad) memperhatikan orang-orang yang mengaku bahwa mereka telah beriman kepada apa yang diturunkan kepadamu dan kepada apa yang diturunkan sebelummu? Tetapi mereka masih menginginkan ketetapan hukum kepada thaghut, padahal mereka tela

4:61

# وَاِذَا قِيْلَ لَهُمْ تَعَالَوْا اِلٰى مَآ اَنْزَلَ اللّٰهُ وَاِلَى الرَّسُوْلِ رَاَيْتَ الْمُنٰفِقِيْنَ يَصُدُّوْنَ عَنْكَ صُدُوْدًاۚ

wa-i*dzaa* qiila lahum ta'*aa*law il*aa* m*aa* anzala **al**l*aa*hu wa-il*aa* **al**rrasuuli ra-ayta **a**lmun*aa*fiqiina ya*sh*udduuna 'anka *sh*uduud*aa**\*n**

**Dan apabila dikatakan kepada mereka, “Marilah (patuh) kepada apa yang telah diturunkan Allah dan (patuh) kepada Rasul,” (niscaya) engkau (Muhammad) melihat orang munafik menghalangi dengan keras darimu.**

4:62

# فَكَيْفَ اِذَآ اَصَابَتْهُمْ مُّصِيْبَةٌ ۢبِمَا قَدَّمَتْ اَيْدِيْهِمْ ثُمَّ جَاۤءُوْكَ يَحْلِفُوْنَ بِاللّٰهِ ۖاِنْ اَرَدْنَآ اِلَّآ اِحْسَانًا وَّتَوْفِيْقًا

fakayfa i*dzaa* a*shaa*bat-hum mu*sh*iibatun bim*aa* qaddamat aydiihim tsumma j*aa*uuka ya*h*lifuuna bi**al**l*aa*hi in aradn*aa* ill*aa* i*h*s*aa*nan watawfiiq*aa**\*n**

Maka bagaimana halnya apabila (kelak) musibah menimpa mereka (orang munafik) disebabkan perbuatan tangannya sendiri, kemudian mereka datang kepadamu (Muhammad) sambil bersumpah, “Demi Allah, kami sekali-kali tidak menghendaki selain kebaikan dan kedamaian

4:63

# اُولٰۤىِٕكَ الَّذِيْنَ يَعْلَمُ اللّٰهُ مَا فِيْ قُلُوْبِهِمْ فَاَعْرِضْ عَنْهُمْ وَعِظْهُمْ وَقُلْ لَّهُمْ فِيْٓ اَنْفُسِهِمْ قَوْلًا ۢ بَلِيْغًا

ul*aa*-ika **al**la*dz*iina ya'lamu **al**l*aa*hu m*aa* fii quluubihim fa-a'ri*dh* 'anhum wa'i*zh*hum waqul lahum fii anfusihim qawlan baliigh*aa**\*n**

Mereka itu adalah orang-orang yang (sesungguhnya) Allah mengetahui apa yang ada di dalam hatinya. Karena itu berpalinglah kamu dari mereka, dan berilah mereka nasihat, dan katakanlah kepada mereka perkataan yang membekas pada jiwanya.

4:64

# وَمَآ اَرْسَلْنَا مِنْ رَّسُوْلٍ اِلَّا لِيُطَاعَ بِاِذْنِ اللّٰهِ ۗوَلَوْ اَنَّهُمْ اِذْ ظَّلَمُوْٓا اَنْفُسَهُمْ جَاۤءُوْكَ فَاسْتَغْفَرُوا اللّٰهَ وَاسْتَغْفَرَ لَهُمُ الرَّسُوْلُ لَوَجَدُوا اللّٰهَ تَوَّابًا رَّحِيْمًا

wam*aa* arsaln*aa* min rasuulin ill*aa* liyu*thaa*'a bi-i*dz*ni **al**l*aa*hi walaw annahum i*dz* *zh*alamuu anfusahum j*aa*uuka fa**i**staghfaruu **al**l*aa*h

Dan Kami tidak mengutus seorang rasul melainkan untuk ditaati dengan izin Allah. Dan sungguh, sekiranya mereka setelah menzalimi dirinya datang kepadamu (Muhammad), lalu memohon ampunan kepada Allah, dan Rasul pun memohonkan ampunan untuk mereka, niscaya

4:65

# فَلَا وَرَبِّكَ لَا يُؤْمِنُوْنَ حَتّٰى يُحَكِّمُوْكَ فِيْمَا شَجَرَ بَيْنَهُمْ ثُمَّ لَا يَجِدُوْا فِيْٓ اَنْفُسِهِمْ حَرَجًا مِّمَّا قَضَيْتَ وَيُسَلِّمُوْا تَسْلِيْمًا

fal*aa* warabbika l*aa* yu/minuuna *h*att*aa* yu*h*akkimuuka fiim*aa* syajara baynahum tsumma l*aa* yajiduu fii anfusihim *h*arajan mimm*aa* qa*dh*ayta wayusallimuu tasliim*aa**\*n**

Maka demi Tuhanmu, mereka tidak beriman sebelum mereka menjadikan engkau (Muhammad) sebagai hakim dalam perkara yang mereka perselisihkan, (sehingga) kemudian tidak ada rasa keberatan dalam hati mereka terhadap putusan yang engkau berikan, dan mereka mene

4:66

# وَلَوْ اَنَّا كَتَبْنَا عَلَيْهِمْ اَنِ اقْتُلُوْٓا اَنْفُسَكُمْ اَوِ اخْرُجُوْا مِنْ دِيَارِكُمْ مَّا فَعَلُوْهُ اِلَّا قَلِيْلٌ مِّنْهُمْ ۗوَلَوْ اَنَّهُمْ فَعَلُوْا مَا يُوْعَظُوْنَ بِهٖ لَكَانَ خَيْرًا لَّهُمْ وَاَشَدَّ تَثْبِيْتًاۙ

walaw ann*aa* katabn*aa* 'alayhim ani uqtuluu anfusakum awi ukhrujuu min diy*aa*rikum m*aa* fa'aluuhu ill*aa* qaliilun minhum walaw annahum fa'aluu m*aa* yuu'a*zh*uuna bihi lak*aa*na khayran lahum wa-asyadda tatsbii

Dan sekalipun telah Kami perintahkan kepada mereka, “Bunuhlah dirimu atau keluarlah kamu dari kampung halamanmu,” ternyata mereka tidak akan melakukannya, kecuali sebagian kecil dari mereka. Dan sekiranya mereka benar-benar melaksanakan perintah yang dibe

4:67

# وَّاِذًا لَّاٰ تَيْنٰهُمْ مِّنْ لَّدُنَّآ اَجْرًا عَظِيْمًاۙ

wa-i*dz*an la*aa*tayn*aa*hum min ladunn*aa* ajran 'a*zh*iim*aa**\*n**

dan dengan demikian, pasti Kami berikan kepada mereka pahala yang besar dari sisi Kami,

4:68

# وَّلَهَدَيْنٰهُمْ صِرَاطًا مُّسْتَقِيْمًا

walahadayn*aa*hum *sh*ir*aath*an mustaqiim*aa**\*n**

dan pasti Kami tunjukkan kepada mereka jalan yang lurus.

4:69

# وَمَنْ يُّطِعِ اللّٰهَ وَالرَّسُوْلَ فَاُولٰۤىِٕكَ مَعَ الَّذِيْنَ اَنْعَمَ اللّٰهُ عَلَيْهِمْ مِّنَ النَّبِيّٖنَ وَالصِّدِّيْقِيْنَ وَالشُّهَدَاۤءِ وَالصّٰلِحِيْنَ ۚ وَحَسُنَ اُولٰۤىِٕكَ رَفِيْقًا

waman yu*th*i'i **al**l*aa*ha wa**al**rrasuula faul*aa*-ika ma'a **al**la*dz*iina an'ama **al**l*aa*hu 'alayhim mina **al**nnabiyyiina wa**al***shsh*

*Dan barangsiapa menaati Allah dan Rasul (Muhammad), maka mereka itu akan bersama-sama dengan orang yang diberikan nikmat oleh Allah, (yaitu) para nabi, para pencinta kebenaran, orang-orang yang mati syahid dan orang-orang saleh. Mereka itulah teman yang s*

4:70

# ذٰلِكَ الْفَضْلُ مِنَ اللّٰهِ ۗوَكَفٰى بِاللّٰهِ عَلِيْمًا ࣖ

*dzaa*lika **a**lfa*dh*lu mina **al**l*aa*hi wakaf*aa* bi**al**l*aa*hi 'aliim*aa**\*n**

Yang demikian itu adalah karunia dari Allah, dan cukuplah Allah yang Maha Mengetahui.

4:71

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْا خُذُوْا حِذْرَكُمْ فَانْفِرُوْا ثُبَاتٍ اَوِ انْفِرُوْا جَمِيْعًا

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu khu*dz*uu *h*i*dz*rakum fa**i**nfiruu tsub*aa*tin awi infiruu jamii'*aa**\*n**

Wahai orang-orang yang beriman! Bersiapsiagalah kamu, dan majulah (ke medan pertempuran) secara berkelompok, atau majulah bersama-sama (serentak).

4:72

# وَاِنَّ مِنْكُمْ لَمَنْ لَّيُبَطِّئَنَّۚ فَاِنْ اَصَابَتْكُمْ مُّصِيْبَةٌ قَالَ قَدْ اَنْعَمَ اللّٰهُ عَلَيَّ اِذْ لَمْ اَكُنْ مَّعَهُمْ شَهِيْدًا

wa-inna minkum laman layuba*ththh*i-anna fa-in a*shaa*batkum mu*sh*iibatun q*aa*la qad an'ama **al**l*aa*hu 'alayya i*dz* lam akun ma'ahum syahiid*aa**\*n**wala-in a*shaa*bakum fa*dh*lu

Dan sesungguhnya di antara kamu pasti ada orang yang sangat enggan (ke medan pertempuran). Lalu jika kamu ditimpa musibah dia berkata, “Sungguh, Allah telah memberikan nikmat kepadaku karena aku tidak ikut berperang bersama mereka.”

4:73

# وَلَىِٕنْ اَصَابَكُمْ فَضْلٌ مِّنَ اللّٰهِ لَيَقُوْلَنَّ كَاَنْ لَّمْ تَكُنْۢ بَيْنَكُمْ وَبَيْنَهٗ مَوَدَّةٌ يّٰلَيْتَنِيْ كُنْتُ مَعَهُمْ فَاَفُوْزَ فَوْزًا عَظِيْمًا

wala-in a*shaa*bakum fa*dh*lun mina **al**l*aa*hi layaquulanna ka-an lam takun baynakum wabaynahu mawaddatun y*aa* laytanii kuntu ma'ahum fa-afuuza fawzan 'a*zh*iim*aa**\*n**

Dan sungguh, jika kamu mendapat karunia (kemenangan) dari Allah, tentulah dia mengatakan seakan-akan belum pernah ada hubungan kasih sayang antara kamu dengan dia, “Wahai, sekiranya aku bersama mereka, tentu aku akan memperoleh kemenangan yang agung (pula

4:74

# ۞ فَلْيُقَاتِلْ فِيْ سَبِيْلِ اللّٰهِ الَّذِيْنَ يَشْرُوْنَ الْحَيٰوةَ الدُّنْيَا بِالْاٰخِرَةِ ۗ وَمَنْ يُّقَاتِلْ فِيْ سَبِيْلِ اللّٰهِ فَيُقْتَلْ اَوْ يَغْلِبْ فَسَوْفَ نُؤْتِيْهِ اَجْرًا عَظِيْمًا

falyuq*aa*til fii sabiili **al**l*aa*hi **al**la*dz*iina yasyruuna **a**l*h*ay*aa*ta **al**dduny*aa* bi**a**l-*aa*khirati waman yuq*aa*til fii sabiili

Karena itu, hendaklah orang-orang yang menjual kehidupan dunia untuk (kehidupan) akhirat berperang di jalan Allah. Dan barangsiapa berperang di jalan Allah, lalu gugur atau memperoleh kemenangan maka akan Kami berikan pahala yang besar kepadanya.

4:75

# وَمَا لَكُمْ لَا تُقَاتِلُوْنَ فِيْ سَبِيْلِ اللّٰهِ وَالْمُسْتَضْعَفِيْنَ مِنَ الرِّجَالِ وَالنِّسَاۤءِ وَالْوِلْدَانِ الَّذِيْنَ يَقُوْلُوْنَ رَبَّنَآ اَخْرِجْنَا مِنْ هٰذِهِ الْقَرْيَةِ الظَّالِمِ اَهْلُهَاۚ وَاجْعَلْ لَّنَا مِنْ لَّدُنْكَ وَلِيًّاۚ و

wam*aa* lakum l*aa* tuq*aa*tiluuna fii sabiili **al**l*aa*hi wa**a**lmusta*dh*'afiina mina **al**rrij*aa*li wa**al**nnis*aa*-i wa**a**lwild*aa*ni

Dan mengapa kamu tidak mau berperang di jalan Allah dan (membela) orang yang lemah, baik laki-laki, perempuan maupun anak-anak yang berdoa, “Ya Tuhan kami, keluarkanlah kami dari negeri ini (Mekah) yang penduduknya zalim. Berilah kami pelindung dari sisi

4:76

# اَلَّذِيْنَ اٰمَنُوْا يُقَاتِلُوْنَ فِيْ سَبِيْلِ اللّٰهِ ۚ وَالَّذِيْنَ كَفَرُوْا يُقَاتِلُوْنَ فِيْ سَبِيْلِ الطَّاغُوْتِ فَقَاتِلُوْٓا اَوْلِيَاۤءَ الشَّيْطٰنِ ۚ اِنَّ كَيْدَ الشَّيْطٰنِ كَانَ ضَعِيْفًا ۚ ࣖ

**al**la*dz*iina *aa*manuu yuq*aa*tiluuna fii sabiili **al**l*aa*hi wa**a**lla*dz*iina kafaruu yuq*aa*tiluuna fii sabiili **al***ththaa*ghuuti faq*aa*tiluu awliy

Orang-orang yang beriman berperang di jalan Allah, dan orang-orang yang kafir berperang di jalan thaghut, maka perangilah kawan-kawan setan itu, (karena) sesungguhnya tipu daya setan itu lemah.

4:77

# اَلَمْ تَرَ اِلَى الَّذِيْنَ قِيْلَ لَهُمْ كُفُّوْٓا اَيْدِيَكُمْ وَاَقِيْمُوا الصَّلٰوةَ وَاٰتُوا الزَّكٰوةَۚ فَلَمَّا كُتِبَ عَلَيْهِمُ الْقِتَالُ اِذَا فَرِيْقٌ مِّنْهُمْ يَخْشَوْنَ النَّاسَ كَخَشْيَةِ اللّٰهِ اَوْ اَشَدَّ خَشْيَةً ۚ وَقَالُوْا رَبَّنَ

alam tara il*aa* **al**la*dz*iina qiila lahum kuffuu aydiyakum wa-aqiimuu **al***shsh*al*aa*ta wa*aa*tuu **al**zzak*aa*ta falamm*aa *kutiba 'alayhimu **a**lqit*aa*

Tidakkah engkau memperhatikan orang-orang yang dikatakan kepada mereka, ”Tahanlah tanganmu (dari berperang), laksanakanlah salat dan tunaikanlah zakat!” Ketika mereka diwajibkan berperang, tiba-tiba sebagian mereka (golongan munafik) takut kepada manusia

4:78

# اَيْنَمَا تَكُوْنُوْا يُدْرِكْكُّمُ الْمَوْتُ وَلَوْ كُنْتُمْ فِيْ بُرُوْجٍ مُّشَيَّدَةٍ ۗ وَاِنْ تُصِبْهُمْ حَسَنَةٌ يَّقُوْلُوْا هٰذِهٖ مِنْ عِنْدِ اللّٰهِ ۚ وَاِنْ تُصِبْهُمْ سَيِّئَةٌ يَّقُوْلُوْا هٰذِهٖ مِنْ عِنْدِكَ ۗ قُلْ كُلٌّ مِّنْ عِنْدِ اللّٰهِ

aynam*aa* takuunuu yudrikkumu **a**lmawtu walaw kuntum fii buruujin musyayyadatin wa-in tu*sh*ibhum *h*asanatun yaquuluu h*aadz*ihi min 'indi **al**l*aa*hi wa-in tu*sh*ibhum sayyi-atun yaquuluu h*a*

Di manapun kamu berada, kematian akan mendapatkan kamu, kendatipun kamu berada di dalam benteng yang tinggi dan kukuh. Jika mereka memperoleh kebaikan, mereka mengatakan, “Ini dari sisi Allah,” dan jika mereka ditimpa suatu keburukan, mereka mengatakan, “

4:79

# مَآ اَصَابَكَ مِنْ حَسَنَةٍ فَمِنَ اللّٰهِ ۖ وَمَآ اَصَابَكَ مِنْ سَيِّئَةٍ فَمِنْ نَّفْسِكَ ۗ وَاَرْسَلْنٰكَ لِلنَّاسِ رَسُوْلًا ۗ وَكَفٰى بِاللّٰهِ شَهِيْدًا

m*aa* a*shaa*baka min *h*asanatin famina **al**l*aa*hi wam*aa* a*shaa*baka min sayyi-atin famin nafsika wa-arsaln*aa*ka li**l**nn*aa*si rasuulan wakaf*aa* bi**al**l*aa*

*Kebajikan apa pun yang kamu peroleh, adalah dari sisi Allah, dan keburukan apa pun yang menimpamu, itu dari (kesalahan) dirimu sendiri. Kami mengutusmu (Muhammad) menjadi Rasul kepada (seluruh) manusia. Dan cukuplah Allah yang menjadi saksi.*

4:80

# مَنْ يُّطِعِ الرَّسُوْلَ فَقَدْ اَطَاعَ اللّٰهَ ۚ وَمَنْ تَوَلّٰى فَمَآ اَرْسَلْنٰكَ عَلَيْهِمْ حَفِيْظًا ۗ

man yu*th*i'i **al**rrasuula faqad a*thaa*'a **al**l*aa*ha waman tawall*aa* fam*aa* arsaln*aa*ka 'alayhim *h*afii*zhaa**\*n**

Barangsiapa menaati Rasul (Muhammad), maka sesungguhnya dia telah menaati Allah. Dan barangsiapa berpaling (dari ketaatan itu), maka (ketahuilah) Kami tidak mengutusmu (Muhammad) untuk menjadi pemelihara mereka.

4:81

# وَيَقُوْلُوْنَ طَاعَةٌ ۖ فَاِذَا بَرَزُوْا مِنْ عِنْدِكَ بَيَّتَ طَاۤىِٕفَةٌ مِّنْهُمْ غَيْرَ الَّذِيْ تَقُوْلُ ۗ وَاللّٰهُ يَكْتُبُ مَا يُبَيِّتُوْنَ ۚ فَاَعْرِضْ عَنْهُمْ وَتَوَكَّلْ عَلَى اللّٰهِ ۗ وَكَفٰى بِاللّٰهِ وَكِيْلًا

wayaquuluuna *thaa*'atun fa-i*dzaa* barazuu min 'indika bayyata *thaa*-ifatun minhum ghayra **al**la*dz*ii taquulu wa**al**l*aa*hu yaktubu m*aa* yubayyituuna fa-a'ri*dh* 'anhum wat

Dan mereka (orang-orang munafik) mengatakan, “(Kewajiban kami hanyalah) taat.” Tetapi, apabila mereka telah pergi dari sisimu (Muhammad), sebagian dari mereka mengatur siasat di malam hari (mengambil keputusan) lain dari yang telah mereka katakan tadi. Al

4:82

# اَفَلَا يَتَدَبَّرُوْنَ الْقُرْاٰنَ ۗ وَلَوْ كَانَ مِنْ عِنْدِ غَيْرِ اللّٰهِ لَوَجَدُوْا فِيْهِ اخْتِلَافًا كَثِيْرًا

afal*aa* yatadabbaruuna **a**lqur-*aa*na walaw k*aa*na min 'indi ghayri **al**l*aa*hi lawajaduu fiihi ikhtil*aa*fan katsiir*aa**\*n**

Maka tidakkah mereka menghayati (mendalami) Al-Qur'an? Sekiranya (Al-Qur'an) itu bukan dari Allah, pastilah mereka menemukan banyak hal yang bertentangan di dalamnya.

4:83

# وَاِذَا جَاۤءَهُمْ اَمْرٌ مِّنَ الْاَمْنِ اَوِ الْخَوْفِ اَذَاعُوْا بِهٖ ۗ وَلَوْ رَدُّوْهُ اِلَى الرَّسُوْلِ وَاِلٰٓى اُولِى الْاَمْرِ مِنْهُمْ لَعَلِمَهُ الَّذِيْنَ يَسْتَنْۢبِطُوْنَهٗ مِنْهُمْ ۗ وَلَوْلَا فَضْلُ اللّٰهِ عَلَيْكُمْ وَرَحْمَتُهٗ لَاتَّبَ

wa-i*dzaa* j*aa*-ahum amrun mina **a**l-amni awi **a**lkhawfi a*dzaa*'uu bihi walaw radduuhu il*aa* **al**rrasuuli wa-il*aa* ulii **a**l-amri minhum la'alimahu **al**

**Dan apabila sampai kepada mereka suatu berita tentang keamanan ataupun ketakutan, mereka (langsung) menyiarkannya. (Padahal) apabila mereka menyerahkannya kepada Rasul dan Ulil Amri di antara mereka, tentulah orang-orang yang ingin mengetahui kebenarannya**

4:84

# فَقَاتِلْ فِيْ سَبِيْلِ اللّٰهِ ۚ لَا تُكَلَّفُ اِلَّا نَفْسَكَ وَحَرِّضِ الْمُؤْمِنِيْنَ ۚ عَسَى اللّٰهُ اَنْ يَّكُفَّ بَأْسَ الَّذِيْنَ كَفَرُوْا ۗوَاللّٰهُ اَشَدُّ بَأْسًا وَّاَشَدُّ تَنْكِيْلًا

faq*aa*til fii sabiili **al**l*aa*hi l*aa* tukallafu ill*aa* nafsaka wa*h*arri*dh*i **a**lmu/miniina 'as*aa* **al**l*aa*hu an yakuffa ba/sa **al**la*dz*iina k

Maka berperanglah engkau (Muhammad) di jalan Allah, engkau tidaklah dibebani melainkan atas dirimu sendiri. Kobarkanlah (semangat) orang-orang beriman (untuk berperang). Mudah-mudahan Allah menolak (mematahkan) serangan orang-orang yang kafir itu. Allah s

4:85

# مَنْ يَّشْفَعْ شَفَاعَةً حَسَنَةً يَّكُنْ لَّهٗ نَصِيْبٌ مِّنْهَا ۚ وَمَنْ يَّشْفَعْ شَفَاعَةً سَيِّئَةً يَّكُنْ لَّهٗ كِفْلٌ مِّنْهَا ۗ وَكَانَ اللّٰهُ عَلٰى كُلِّ شَيْءٍ مُّقِيْتًا

man yasyfa' syaf*aa*'atan *h*asanatan yakun lahu na*sh*iibun minh*aa* waman yasyfa' syaf*aa*'atan sayyi-atan yakun lahu kiflun minh*aa* wak*aa*na **al**l*aa*hu 'al*aa* kulli syay-in muqiit*aa*

Barangsiapa memberi pertolongan dengan pertolongan yang baik, niscaya dia akan memperoleh bagian dari (pahala)nya. Dan barangsiapa memberi pertolongan dengan pertolongan yang buruk, niscaya dia akan memikul bagian dari (dosa)nya. Allah Mahakuasa atas sega

4:86

# وَاِذَا حُيِّيْتُمْ بِتَحِيَّةٍ فَحَيُّوْا بِاَحْسَنَ مِنْهَآ اَوْ رُدُّوْهَا ۗ اِنَّ اللّٰهَ كَانَ عَلٰى كُلِّ شَيْءٍ حَسِيْبًا

wa-i*dzaa* *h*uyyiitum bita*h*iyyatin fa*h*ayyuu bi-a*h*sana minh*aa* aw rudduuh*aa* inna **al**l*aa*ha k*aa*na 'al*aa* kulli syay-in *h*asiib*aa**\*n**

Dan apabila kamu dihormati dengan suatu (salam) penghormatan, maka balaslah penghormatan itu dengan yang lebih baik, atau balaslah (penghormatan itu, yang sepadan) dengannya. Sungguh, Allah memperhitungkan segala sesuatu.

4:87

# اَللّٰهُ لَآ اِلٰهَ اِلَّا هُوَۗ لَيَجْمَعَنَّكُمْ اِلٰى يَوْمِ الْقِيٰمَةِ لَا رَيْبَ فِيْهِ ۗ وَمَنْ اَصْدَقُ مِنَ اللّٰهِ حَدِيْثًا ࣖ

**al**l*aa*hu l*aa* il*aa*ha ill*aa* huwa layajma'annakum il*aa* yawmi **a**lqiy*aa*mati l*aa* rayba fiihi waman a*sh*daqu mina **al**l*aa*hi *h*adiits*aa*

Allah, tidak ada tuhan selain Dia. Dia pasti akan mengumpulkan kamu pada hari Kiamat yang tidak diragukan terjadinya. Siapakah yang lebih benar perkataan(nya) daripada Allah?

4:88

# ۞ فَمَا لَكُمْ فِى الْمُنٰفِقِيْنَ فِئَتَيْنِ وَاللّٰهُ اَرْكَسَهُمْ بِمَا كَسَبُوْا ۗ اَتُرِيْدُوْنَ اَنْ تَهْدُوْا مَنْ اَضَلَّ اللّٰهُ ۗوَمَنْ يُّضْلِلِ اللّٰهُ فَلَنْ تَجِدَ لَهٗ سَبِيْلًا

fam*aa* lakum fii **a**lmun*aa*fiqiina fi-atayni wa**al**l*aa*hu arkasahum bim*aa* kasabuu aturiiduuna an tahduu man a*dh*alla **al**l*aa*hu waman yu*dh*lili **al**l

Maka mengapa kamu (terpecah) menjadi dua golongan dalam (menghadapi) orang-orang munafik, padahal Allah telah mengembalikan mereka (kepada kekafiran), disebabkan usaha mereka sendiri? Apakah kamu bermaksud memberi petunjuk kepada orang yang telah dibiarka

4:89

# وَدُّوْا لَوْ تَكْفُرُوْنَ كَمَا كَفَرُوْا فَتَكُوْنُوْنَ سَوَاۤءً فَلَا تَتَّخِذُوْا مِنْهُمْ اَوْلِيَاۤءَ حَتّٰى يُهَاجِرُوْا فِيْ سَبِيْلِ اللّٰهِ ۗ فَاِنْ تَوَلَّوْا فَخُذُوْهُمْ وَاقْتُلُوْهُمْ حَيْثُ وَجَدْتُّمُوْهُمْ ۖ وَلَا تَتَّخِذُوْا مِنْهُمْ و

wadduu law takfuruuna kam*aa* kafaruu fatakuunuuna saw*aa*-an fal*aa* tattakhi*dz*uu minhum awliy*aa*-a *h*att*aa* yuh*aa*jiruu fii sabiili **al**l*aa*hi fa-in tawallaw fakhu*dz*uuhum wa

Mereka ingin agar kamu menjadi kafir sebagaimana mereka telah menjadi kafir, sehingga kamu menjadi sama (dengan mereka). Janganlah kamu jadikan dari antara mereka sebagai teman-teman(mu), sebelum mereka berpindah pada jalan Allah. Apabila mereka berpaling

4:90

# اِلَّا الَّذِيْنَ يَصِلُوْنَ اِلٰى قَوْمٍۢ بَيْنَكُمْ وَبَيْنَهُمْ مِّيْثَاقٌ اَوْ جَاۤءُوْكُمْ حَصِرَتْ صُدُوْرُهُمْ اَنْ يُّقَاتِلُوْكُمْ اَوْ يُقَاتِلُوْا قَوْمَهُمْ ۗ وَلَوْ شَاۤءَ اللّٰهُ لَسَلَّطَهُمْ عَلَيْكُمْ فَلَقَاتَلُوْكُمْ ۚ فَاِنِ اعْتَزَلُو

ill*aa* **al**la*dz*iina ya*sh*iluuna il*aa* qawmin baynakum wabaynahum miits*aa*qun aw j*aa*uukum *h*a*sh*irat *sh*uduuruhum an yuq*aa*tiluukum aw yuq*aa*tiluu qawmahum walaw sy*aa*

kecuali orang-orang yang meminta perlindungan kepada suatu kaum, yang antara kamu dan kaum itu telah ada perjanjian (damai) atau orang yang datang kepadamu sedang hati mereka merasa keberatan untuk memerangi kamu atau memerangi kaumnya. Sekiranya Allah me

4:91

# سَتَجِدُوْنَ اٰخَرِيْنَ يُرِيْدُوْنَ اَنْ يَّأْمَنُوْكُمْ وَيَأْمَنُوْا قَوْمَهُمْ ۗ كُلَّ مَا رُدُّوْٓا اِلَى الْفِتْنَةِ اُرْكِسُوْا فِيْهَا ۚ فَاِنْ لَّمْ يَعْتَزِلُوْكُمْ وَيُلْقُوْٓا اِلَيْكُمُ السَّلَمَ وَيَكُفُّوْٓا اَيْدِيَهُمْ فَخُذُوْهُمْ وَاقْت

satajiduuna *aa*khariina yuriiduuna an ya/manuukum waya/manuu qawmahum kulla m*aa* rudduu il*aa* **a**lfitnati urkisuu fiih*aa* fa-in lam ya'taziluukum wayulquu ilaykumu **al**ssalama wayakuffuu aydiyahum fakh

Kelak akan kamu dapati (golongan-golongan) yang lain, yang menginginkan agar mereka hidup aman bersamamu dan aman (pula) bersama kaumnya. Setiap kali mereka diajak kembali kepada fitnah (syirik), mereka pun terjun ke dalamnya. Karena itu jika mereka tidak

4:92

# وَمَا كَانَ لِمُؤْمِنٍ اَنْ يَّقْتُلَ مُؤْمِنًا اِلَّا خَطَـًٔا ۚ وَمَنْ قَتَلَ مُؤْمِنًا خَطَـًٔا فَتَحْرِيْرُ رَقَبَةٍ مُّؤْمِنَةٍ وَّدِيَةٌ مُّسَلَّمَةٌ اِلٰٓى اَهْلِهٖٓ اِلَّآ اَنْ يَّصَّدَّقُوْا ۗ فَاِنْ كَانَ مِنْ قَوْمٍ عَدُوٍّ لَّكُمْ وَهُوَ م

wam*aa* k*aa*na limu/minin an yaqtula mu/minan ill*aa* kha*th*a-an waman qatala mu/minan kha*th*a-an fata*h*riiru raqabatin mu/minatin wadiyatun musallamatun il*aa* ahlihi ill*aa* an ya*shsh*addaquu fa-in k*a*

Dan tidak patut bagi seorang yang beriman membunuh seorang yang beriman (yang lain), kecuali karena tersalah (tidak sengaja). Barangsiapa membunuh seorang yang beriman karena tersalah (hendaklah) dia memerdekakan seorang hamba sahaya yang beriman serta (m

4:93

# وَمَنْ يَّقْتُلْ مُؤْمِنًا مُّتَعَمِّدًا فَجَزَاۤؤُهٗ جَهَنَّمُ خَالِدًا فِيْهَا وَغَضِبَ اللّٰهُ عَلَيْهِ وَلَعَنَهٗ وَاَعَدَّ لَهٗ عَذَابًا عَظِيْمًا

waman yaqtul mu/minan muta'ammidan fajaz*aa*uhu jahannamu kh*aa*lidan fiih*aa* wagha*dh*iba **al**l*aa*hu 'alayhi wala'anahu wa-a'adda lahu 'a*dzaa*ban 'a*zh*iim*aa**\*n**

Dan barangsiapa membunuh seorang yang beriman dengan sengaja, maka balasannya ialah neraka Jahanam, dia kekal di dalamnya. Allah murka kepadanya, dan melaknatnya serta menyediakan azab yang besar baginya.

4:94

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْٓا اِذَا ضَرَبْتُمْ فِيْ سَبِيْلِ اللّٰهِ فَتَبَيَّنُوْا وَلَا تَقُوْلُوْا لِمَنْ اَلْقٰىٓ اِلَيْكُمُ السَّلٰمَ لَسْتَ مُؤْمِنًاۚ تَبْتَغُوْنَ عَرَضَ الْحَيٰوةِ الدُّنْيَا ۖفَعِنْدَ اللّٰهِ مَغَانِمُ كَثِيْرَةٌ ۗ كَذٰلِكَ كُ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu i*dzaa* *dh*arabtum fii sabiili **al**l*aa*hi fatabayyanuu wal*aa* taquuluu liman **a**lq*aa* ilaykumu **al**ssal

Wahai orang-orang yang beriman! Apabila kamu pergi (berperang) di jalan Allah, maka telitilah (carilah keterangan) dan janganlah kamu mengatakan kepada orang yang mengucapkan ”salam” kepadamu, ”Kamu bukan seorang yang beriman,” (lalu kamu membunuhnya), de

4:95

# لَا يَسْتَوِى الْقَاعِدُوْنَ مِنَ الْمُؤْمِنِيْنَ غَيْرُ اُولِى الضَّرَرِ وَالْمُجَاهِدُوْنَ فِيْ سَبِيْلِ اللّٰهِ بِاَمْوَالِهِمْ وَاَنْفُسِهِمْۗ فَضَّلَ اللّٰهُ الْمُجٰهِدِيْنَ بِاَمْوَالِهِمْ وَاَنْفُسِهِمْ عَلَى الْقٰعِدِيْنَ دَرَجَةً ۗ وَكُلًّا وَّع

l*aa* yastawii **a**lq*aa*'iduuna mina **a**lmu/miniina ghayru ulii **al***dhdh*arari wa**a**lmuj*aa*hiduuna fii sabiili **al**l*aa*hi bi-amw*aa*lihim wa-anfusi

Tidaklah sama antara orang beriman yang duduk (yang tidak turut berperang) tanpa mempunyai uzur (halangan) dengan orang yang berjihad di jalan Allah dengan harta dan jiwanya. Allah melebihkan derajat orang-orang yang berjihad dengan harta dan jiwanya atas

4:96

# دَرَجٰتٍ مِّنْهُ وَمَغْفِرَةً وَّرَحْمَةً ۗوَكَانَ اللّٰهُ غَفُوْرًا رَّحِيْمًا ࣖ

daraj*aa*tin minhu wamaghfiratan wara*h*matan wak*aa*na **al**l*aa*hu ghafuuran ra*h*iim*aa*

(yaitu) beberapa derajat daripada-Nya, serta ampunan dan rahmat. Allah Ma-ha Pengampun, Maha Penyayang.

4:97

# اِنَّ الَّذِيْنَ تَوَفّٰىهُمُ الْمَلٰۤىِٕكَةُ ظَالِمِيْٓ اَنْفُسِهِمْ قَالُوْا فِيْمَ كُنْتُمْ ۗ قَالُوْا كُنَّا مُسْتَضْعَفِيْنَ فِى الْاَرْضِۗ قَالُوْٓا اَلَمْ تَكُنْ اَرْضُ اللّٰهِ وَاسِعَةً فَتُهَاجِرُوْا فِيْهَا ۗ فَاُولٰۤىِٕكَ مَأْوٰىهُمْ جَهَنَّمُ

inna **al**la*dz*iina tawaff*aa*humu almal*aa*-ikatu *zhaa*limii anfusihim q*aa*luu fiima kuntum q*aa*luu kunn*aa* musta*dh*'afiina fii al-ar*dh*i q*aa*luu alam takun ar*dh*u **al**

**Sesungguhnya orang-orang yang dicabut nyawanya oleh malaikat dalam keadaan menzalimi sendiri, mereka (para malaikat) bertanya, “Bagaimana kamu ini?” Mereka menjawab, “Kami orang-orang yang tertindas di bumi (Mekah).” Mereka (para malaikat) bertanya, “Buka**

4:98

# اِلَّا الْمُسْتَضْعَفِيْنَ مِنَ الرِّجَالِ وَالنِّسَاۤءِ وَالْوِلْدَانِ لَا يَسْتَطِيْعُوْنَ حِيْلَةً وَّلَا يَهْتَدُوْنَ سَبِيْلًاۙ

ill*aa* **a**lmusta*dh*'afiina mina **al**rrij*aa*li wa**al**nnis*aa*-i wa**a**lwild*aa*ni l*aa* yasta*th*ii'uuna *h*iilatan wal*aa* yahtaduuna sabiil*aa*

*kecuali mereka yang tertindas baik laki-laki atau perempuan dan anak-anak yang tidak berdaya dan tidak mengetahui jalan (untuk berhijrah),*

4:99

# فَاُولٰۤىِٕكَ عَسَى اللّٰهُ اَنْ يَّعْفُوَ عَنْهُمْ ۗ وَكَانَ اللّٰهُ عَفُوًّا غَفُوْرًا

faul*aa*-ika 'as*aa* **al**l*aa*hu an ya'fuwa 'anhum wak*aa*na **al**l*aa*hu 'afuwwan ghafuur*aa**\*n**

maka mereka itu, mudah-mudahan Allah memaafkannya. Allah Maha Pemaaf, Maha Pengampun.

4:100

# ۞ وَمَنْ يُّهَاجِرْ فِيْ سَبِيْلِ اللّٰهِ يَجِدْ فِى الْاَرْضِ مُرَاغَمًا كَثِيْرًا وَّسَعَةً ۗوَمَنْ يَّخْرُجْ مِنْۢ بَيْتِهٖ مُهَاجِرًا اِلَى اللّٰهِ وَرَسُوْلِهٖ ثُمَّ يُدْرِكْهُ الْمَوْتُ فَقَدْ وَقَعَ اَجْرُهٗ عَلَى اللّٰهِ ۗوَكَانَ اللّٰهُ غَفُوْرًا

waman yuh*aa*jir fii sabiili **al**l*aa*hi yajid fii **a**l-ar*dh*i mur*aa*ghaman katsiiran wasa'atan waman yakhruj min baytihi muh*aa*jiran il*aa* **al**l*aa*hi warasuulihi tsumma

Dan barangsiapa berhijrah di jalan Allah, niscaya mereka akan mendapatkan di bumi ini tempat hijrah yang luas dan (rezeki) yang banyak. Barangsiapa keluar dari rumahnya dengan maksud berhijrah karena Allah dan Rasul-Nya, kemudian kematian menimpanya (sebe

4:101

# وَاِذَا ضَرَبْتُمْ فِى الْاَرْضِ فَلَيْسَ عَلَيْكُمْ جُنَاحٌ اَنْ تَقْصُرُوْا مِنَ الصَّلٰوةِ ۖ اِنْ خِفْتُمْ اَنْ يَّفْتِنَكُمُ الَّذِيْنَ كَفَرُوْاۗ اِنَّ الْكٰفِرِيْنَ كَانُوْا لَكُمْ عَدُوًّا مُّبِيْنًا

wa-i*dzaa* *dh*arabtum fii **a**l-ar*dh*i falaysa 'alaykum jun*aah*un an taq*sh*uruu mina **al***shsh*al*aa*ti in khiftum an yaftinakumu **al**la*dz*iina kafaruu inna **a**

Dan apabila kamu bepergian di bumi, maka tidaklah berdosa kamu meng-qasar salat, jika kamu takut diserang orang kafir. Sesungguhnya orang kafir itu adalah musuh yang nyata bagimu.

4:102

# وَاِذَا كُنْتَ فِيْهِمْ فَاَقَمْتَ لَهُمُ الصَّلٰوةَ فَلْتَقُمْ طَاۤىِٕفَةٌ مِّنْهُمْ مَّعَكَ وَلْيَأْخُذُوْٓا اَسْلِحَتَهُمْ ۗ فَاِذَا سَجَدُوْا فَلْيَكُوْنُوْا مِنْ وَّرَاۤىِٕكُمْۖ وَلْتَأْتِ طَاۤىِٕفَةٌ اُخْرٰى لَمْ يُصَلُّوْا فَلْيُصَلُّوْا مَعَكَ وَ

wa-i*dzaa* kunta fiihim fa-aqamta lahumu **al***shsh*al*aa*ta faltaqum* thaa*\-ifatun minhum ma'aka walya/khu*dz*uu asli*h*atahum fa-i*dzaa *sajaduu falyakuunuu min war*aa*\-ikum walta/ti* th*

Dan apabila engkau (Muhammad) berada di tengah-tengah mereka (sahabatmu) lalu engkau hendak melaksanakan salat bersama-sama mereka, maka hendaklah segolongan dari mereka berdiri (salat) besertamu dan menyandang senjata mereka, kemudian apabila mereka (yan

4:103

# فَاِذَا قَضَيْتُمُ الصَّلٰوةَ فَاذْكُرُوا اللّٰهَ قِيَامًا وَّقُعُوْدًا وَّعَلٰى جُنُوْبِكُمْ ۚ فَاِذَا اطْمَأْنَنْتُمْ فَاَقِيْمُوا الصَّلٰوةَ ۚ اِنَّ الصَّلٰوةَ كَانَتْ عَلَى الْمُؤْمِنِيْنَ كِتٰبًا مَّوْقُوْتًا

fa-i*dzaa* qa*dh*aytumu **al***shsh*al*aa*ta fa**u***dz*kuruu **al**l*aa*ha qiy*aa*man waqu'uudan wa'al*aa *junuubikum fa-i*dzaa *i*th*ma/nantum fa-aqiimuu **al**

Selanjutnya, apabila kamu telah menyelesaikan salat(mu), ingatlah Allah ketika kamu berdiri, pada waktu duduk dan ketika berbaring. Kemudian, apabila kamu telah merasa aman, maka laksanakanlah salat itu (sebagaimana biasa). Sungguh, salat itu adalah kewaj

4:104

# وَلَا تَهِنُوْا فِى ابْتِغَاۤءِ الْقَوْمِ ۗ اِنْ تَكُوْنُوْا تَأْلَمُوْنَ فَاِنَّهُمْ يَأْلَمُوْنَ كَمَا تَأْلَمُوْنَ ۚوَتَرْجُوْنَ مِنَ اللّٰهِ مَا لَا يَرْجُوْنَ ۗوَكَانَ اللّٰهُ عَلِيْمًا حَكِيْمًا ࣖ

wal*aa* tahinuu fii ibtigh*aa*-i **a**lqawmi in takuunuu ta/lamuuna fa-innahum ya/lamuuna kam*aa* ta/lamuuna watarjuuna mina **al**l*aa*hi m*aa* l*aa* yarjuuna wak*aa*na **al**l*a*

Dan janganlah kamu berhati lemah dalam mengejar mereka (musuhmu). Jika kamu menderita kesakitan, maka ketahuilah mereka pun menderita kesakitan (pula), sebagaimana kamu rasakan, sedang kamu masih dapat mengharapkan dari Allah apa yang tidak dapat mereka h

4:105

# اِنَّآ اَنْزَلْنَآ اِلَيْكَ الْكِتٰبَ بِالْحَقِّ لِتَحْكُمَ بَيْنَ النَّاسِ بِمَآ اَرٰىكَ اللّٰهُ ۗوَلَا تَكُنْ لِّلْخَاۤىِٕنِيْنَ خَصِيْمًا ۙ

inn*aa* anzaln*aa* ilayka **a**lkit*aa*ba bi**a**l*h*aqqi lita*h*kuma bayna **al**nn*aa*si bim*aa* ar*aa*ka **al**l*aa*hu wal*aa* takun lilkh*aa*-in

Sungguh, Kami telah menurunkan Kitab (Al-Qur'an) kepadamu (Muhammad) membawa kebenaran, agar engkau mengadili antara manusia dengan apa yang telah diajarkan Allah kepadamu, dan janganlah engkau menjadi penentang (orang yang tidak bersalah), karena (membel

4:106

# وَّاسْتَغْفِرِ اللّٰهَ ۗاِنَّ اللّٰهَ كَانَ غَفُوْرًا رَّحِيْمًاۚ

wa**i**staghfiri **al**l*aa*ha inna **al**l*aa*ha k*aa*na ghafuuran ra*h*iim*aa**\*n**

dan mohonkanlah ampunan kepada Allah. Sungguh, Allah Maha Pengampun, Maha Penyayang.

4:107

# وَلَا تُجَادِلْ عَنِ الَّذِيْنَ يَخْتَانُوْنَ اَنْفُسَهُمْ ۗ اِنَّ اللّٰهَ لَا يُحِبُّ مَنْ كَانَ خَوَّانًا اَثِيْمًاۙ

wal*aa* tuj*aa*dil 'ani **al**la*dz*iina yakht*aa*nuuna anfusahum inna **al**l*aa*ha l*aa* yu*h*ibbu man k*aa*na khaww*aa*nan atsiim*aa**\*n**

Dan janganlah kamu berdebat untuk (membela) orang-orang yang mengkhianati dirinya. Sungguh, Allah tidak menyukai orang-orang yang selalu berkhianat dan bergelimang dosa,

4:108

# يَّسْتَخْفُوْنَ مِنَ النَّاسِ وَلَا يَسْتَخْفُوْنَ مِنَ اللّٰهِ وَهُوَ مَعَهُمْ اِذْ يُبَيِّتُوْنَ مَا لَا يَرْضٰى مِنَ الْقَوْلِ ۗ وَكَانَ اللّٰهُ بِمَا يَعْمَلُوْنَ مُحِيْطًا

yastakhfuuna mina **al**nn*aa*si wal*aa* yastakhfuuna mina **al**l*aa*hi wahuwa ma'ahum i*dz* yubayyituuna m*aa* l*aa* yar*daa* mina **a**lqawli wak*aa*na **al**l<

mereka dapat bersembunyi dari manusia, tetapi mereka tidak dapat bersembunyi dari Allah, karena Allah beserta mereka, ketika pada suatu malam mereka menetapkan keputusan rahasia yang tidak diridai-Nya. Dan Allah Ma-ha Meliputi terhadap apa yang mereka ker

4:109

# هٰٓاَنْتُمْ هٰٓؤُلَاۤءِ جَادَلْتُمْ عَنْهُمْ فِى الْحَيٰوةِ الدُّنْيَاۗ فَمَنْ يُّجَادِلُ اللّٰهَ عَنْهُمْ يَوْمَ الْقِيٰمَةِ اَمْ مَّنْ يَّكُوْنُ عَلَيْهِمْ وَكِيْلًا

h*aa*-antum h*aa*ul*aa*-i j*aa*daltum 'anhum fii **a**l*h*ay*aa*ti **al**dduny*aa* faman yuj*aa*dilu **al**l*aa*ha 'anhum yawma **a**lqiy*aa*mati am man

Itulah kamu! Kamu berdebat untuk (membela) mereka dalam kehidupan dunia ini, tetapi siapa yang akan menentang Allah untuk (membela) mereka pada hari Kiamat? Atau siapakah yang menjadi pelindung mereka (terhadap azab Allah)?

4:110

# وَمَنْ يَّعْمَلْ سُوْۤءًا اَوْ يَظْلِمْ نَفْسَهٗ ثُمَّ يَسْتَغْفِرِ اللّٰهَ يَجِدِ اللّٰهَ غَفُوْرًا رَّحِيْمًا

waman ya'mal suu-an aw ya*zh*lim nafsahu tsumma yastaghfiri **al**l*aa*ha yajidi **al**l*aa*ha ghafuuran ra*h*iim*aa**\*n**waman ya'mal suu-an aw ya*zh*lim nafsahu tsumma yastaghfiri

Dan barangsiapa berbuat kejahatan dan menganiaya dirinya, kemudian dia memohon ampunan kepada Allah, niscaya dia akan mendapatkan Allah Maha Pengampun, Maha Penyayang.

4:111

# وَمَنْ يَّكْسِبْ اِثْمًا فَاِنَّمَا يَكْسِبُهٗ عَلٰى نَفْسِهٖ ۗ وَكَانَ اللّٰهُ عَلِيْمًا حَكِيْمًا

waman yaksib itsman fa-innam*aa* yaksibuhu 'al*aa* nafsihi wak*aa*na **al**l*aa*hu 'aliiman *h*akiim*aa**\*n**

Dan barangsiapa berbuat dosa, maka sesungguhnya dia mengerjakannya untuk (kesulitan) dirinya sendiri. Dan Allah Maha Mengetahui, Mahabijak-sana.

4:112

# وَمَنْ يَّكْسِبْ خَطِيْۤـَٔةً اَوْ اِثْمًا ثُمَّ يَرْمِ بِهٖ بَرِيْۤـًٔا فَقَدِ احْتَمَلَ بُهْتَانًا وَّاِثْمًا مُّبِيْنًا ࣖ

waman yaksib kha*th*ii-atan aw itsman tsumma yarmi bihi barii-an faqadi i*h*tamala buht*aa*nan wa-itsman mubiin*aa**\*n**

Dan barangsiapa berbuat kesalahan atau dosa, kemudian dia tuduhkan kepada orang yang tidak bersalah, maka sungguh, dia telah memikul suatu kebohongan dan dosa yang nyata.

4:113

# وَلَوْلَا فَضْلُ اللّٰهِ عَلَيْكَ وَرَحْمَتُهٗ لَهَمَّتْ طَّاۤىِٕفَةٌ مِّنْهُمْ اَنْ يُّضِلُّوْكَۗ وَمَا يُضِلُّوْنَ اِلَّآ اَنْفُسَهُمْ وَمَا يَضُرُّوْنَكَ مِنْ شَيْءٍ ۗ وَاَنْزَلَ اللّٰهُ عَلَيْكَ الْكِتٰبَ وَالْحِكْمَةَ وَعَلَّمَكَ مَا لَمْ تَكُنْ تَع

walawl*aa* fa*dh*lu **al**l*aa*hi 'alayka wara*h*matuhu lahammat *thaa*-ifatun minhum an yu*dh*illuuka wam*aa* yu*dh*illuuna ill*aa* anfusahum wam*aa* ya*dh*urruunaka min syay-in w

Dan kalau bukan karena karunia Allah dan rahmat-Nya kepadamu (Muhammad), tentulah segolongan dari mereka berkeinginan keras untuk menyesatkanmu. Tetapi mereka hanya menyesatkan dirinya sendiri, dan tidak membahayakanmu sedikit pun. Dan (juga karena) Allah

4:114

# ۞ لَا خَيْرَ فِيْ كَثِيْرٍ مِّنْ نَّجْوٰىهُمْ اِلَّا مَنْ اَمَرَ بِصَدَقَةٍ اَوْ مَعْرُوْفٍ اَوْ اِصْلَاحٍۢ بَيْنَ النَّاسِۗ وَمَنْ يَّفْعَلْ ذٰلِكَ ابْتِغَاۤءَ مَرْضَاتِ اللّٰهِ فَسَوْفَ نُؤْتِيْهِ اَجْرًا عَظِيْمًا

l*aa* khayra fii katsiirin min najw*aa*hum ill*aa* man amara bi*sh*adaqatin aw ma'ruufin aw i*sh*l*aah*in bayna **al**nn*aa*si waman yaf'al *dzaa*lika ibtigh*aa*-a mar*daa*ti **al**

**Tidak ada kebaikan dari banyak pembicaraan rahasia mereka, kecuali pembicaraan rahasia dari orang yang menyuruh (orang) bersedekah, atau berbuat kebaikan, atau mengadakan perdamaian di antara manusia. Barangsiapa berbuat demikian karena mencari keridaan A**

4:115

# وَمَنْ يُّشَاقِقِ الرَّسُوْلَ مِنْۢ بَعْدِ مَا تَبَيَّنَ لَهُ الْهُدٰى وَيَتَّبِعْ غَيْرَ سَبِيْلِ الْمُؤْمِنِيْنَ نُوَلِّهٖ مَا تَوَلّٰى وَنُصْلِهٖ جَهَنَّمَۗ وَسَاۤءَتْ مَصِيْرًا ࣖ

waman yusy*aa*qiqi **al**rrasuula min ba'di m*aa* tabayyana lahu **a**lhud*aa* wayattabi' ghayra sabiili **a**lmu/miniina nuwallihi m*aa* tawall*aa* wanu*sh*lihi jahannama was*aa*-a

Dan barangsiapa menentang Rasul (Muhammad) setelah jelas kebenaran baginya, dan mengikuti jalan yang bukan jalan orang-orang mukmin, Kami biarkan dia dalam kesesatan yang telah dilakukannya itu dan akan Kami masukkan dia ke dalam neraka Jahanam, dan itu s

4:116

# اِنَّ اللّٰهَ لَا يَغْفِرُ اَنْ يُّشْرَكَ بِهٖ وَيَغْفِرُ مَا دُوْنَ ذٰلِكَ لِمَنْ يَّشَاۤءُ ۗ وَمَنْ يُّشْرِكْ بِاللّٰهِ فَقَدْ ضَلَّ ضَلٰلًا ۢ بَعِيْدًا

inna **al**l*aa*ha l*aa* yaghfiru an yusyraka bihi wayaghfiru m*aa* duuna *dzaa*lika liman yasy*aa*u waman yusyrik bi**al**l*aa*hi faqad *dh*alla *dh*al*aa*lan ba'iid*aa**\*n**

Allah tidak akan mengampuni dosa syirik (mempersekutukan Allah dengan sesuatu), dan Dia mengampuni dosa selain itu bagi siapa yang Dia kehendaki. Dan barangsiapa mempersekutukan (sesuatu) dengan Allah, maka sungguh, dia telah tersesat jauh sekali.

4:117

# اِنْ يَّدْعُوْنَ مِنْ دُوْنِهٖٓ اِلَّآ اِنَاثًاۚ وَاِنْ يَّدْعُوْنَ اِلَّا شَيْطٰنًا مَّرِيْدًاۙ

in yad'uuna min duunihi ill*aa* in*aa*tsan wa-in yad'uuna ill*aa* syay*thaa*nan mariid*aa**\*n**

Yang mereka sembah selain Allah itu tidak lain hanyalah (berhala), dan mereka tidak lain hanyalah menyembah setan yang durhaka,

4:118

# لَّعَنَهُ اللّٰهُ ۘ وَقَالَ لَاَتَّخِذَنَّ مِنْ عِبَادِكَ نَصِيْبًا مَّفْرُوْضًاۙ

la'anahu **al**l*aa*hu waq*aa*la la-attakhi*dz*anna min 'ib*aa*dika na*sh*iiban mafruu*daa**\*n**

yang dilaknati Allah, dan (setan) itu mengatakan, “Aku pasti akan mengambil bagian tertentu dari hamba-hamba-Mu,

4:119

# وَّلَاُضِلَّنَّهُمْ وَلَاُمَنِّيَنَّهُمْ وَلَاٰمُرَنَّهُمْ فَلَيُبَتِّكُنَّ اٰذَانَ الْاَنْعَامِ وَلَاٰمُرَنَّهُمْ فَلَيُغَيِّرُنَّ خَلْقَ اللّٰهِ ۚ وَمَنْ يَّتَّخِذِ الشَّيْطٰنَ وَلِيًّا مِّنْ دُوْنِ اللّٰهِ فَقَدْ خَسِرَ خُسْرَانًا مُّبِيْنًا

walau*dh*illannahum walaumanniyannahum wala*aa*murannahum falayubattikunna *aatsaa*na **a**l-an'*aa*mi wala*aa*murannahum falayughayyirunna khalqa **al**l*aa*hi waman yattakhi*dz*i **al**

**dan pasti kusesatkan mereka, dan akan kubangkitkan angan-angan kosong pada mereka dan akan kusuruh mereka memotong telinga-telinga binatang ternak, (lalu mereka benar-benar memotongnya), dan akan aku suruh mereka mengubah ciptaan Allah, (lalu mereka benar**

4:120

# يَعِدُهُمْ وَيُمَنِّيْهِمْۗ وَمَا يَعِدُهُمُ الشَّيْطٰنُ اِلَّا غُرُوْرًا

ya'iduhum wayumanniihim wam*aa* ya'iduhumu **al**sysyay*thaa*nu ill*aa* ghuruur*aa**\*n**

(Setan itu) memberikan janji-janji kepada mereka dan membangkitkan angan-angan kosong pada mereka, padahal setan itu hanya menjanjikan tipuan belaka kepada mereka.

4:121

# اُولٰۤىِٕكَ مَأْوٰىهُمْ جَهَنَّمُۖ وَلَا يَجِدُوْنَ عَنْهَا مَحِيْصًا

ul*aa*-ika ma/w*aa*hum jahannamu wal*aa* yajiduuna 'anh*aa* ma*h*ii*shaa**\*n**

Mereka (yang tertipu) itu tempatnya di neraka Jahanam dan mereka tidak akan mendapat tempat (lain untuk) lari darinya.

4:122

# وَالَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِ سَنُدْخِلُهُمْ جَنّٰتٍ تَجْرِيْ مِنْ تَحْتِهَا الْاَنْهٰرُ خٰلِدِيْنَ فِيْهَآ اَبَدًاۗ وَعْدَ اللّٰهِ حَقًّا ۗوَمَنْ اَصْدَقُ مِنَ اللّٰهِ قِيْلًا

wa**a**lla*dz*iina *aa*manuu wa'amiluu **al***shshaa*li*haa*ti sanudkhiluhum jann*aa*tin tajrii min ta*h*tih*aa* **a**l-anh*aa*ru kh*aa*lidiina fiih*aa* abadan wa'da

Dan orang yang beriman dan mengerjakan amal kebajikan, kelak akan Kami masukkan ke dalam surga yang mengalir di bawahnya sungai-sungai, mereka kekal di dalamnya selama-lamanya. Dan janji Allah itu benar. Siapakah yang lebih benar perkataannya daripada All

4:123

# لَيْسَ بِاَمَانِيِّكُمْ وَلَآ اَمَانِيِّ اَهْلِ الْكِتٰبِ ۗ مَنْ يَّعْمَلْ سُوْۤءًا يُّجْزَ بِهٖۙ وَلَا يَجِدْ لَهٗ مِنْ دُوْنِ اللّٰهِ وَلِيًّا وَّلَا نَصِيْرًا

laysa bi-am*aa*niyyikum wal*aa* am*aa*niyyi ahli **a**lkit*aa*bi man ya'mal suu-an yujza bihi wal*aa* yajid lahu min duuni **al**l*aa*hi waliyyan wal*aa* na*sh*iir*aa**\*n**

**(Pahala dari Allah) itu bukanlah angan-anganmu dan bukan (pula) angan-angan Ahli Kitab. Barangsiapa mengerjakan kejahatan, niscaya akan dibalas sesuai dengan kejahatan itu, dan dia tidak akan mendapatkan pelindung dan penolong selain Allah.**

4:124

# وَمَنْ يَّعْمَلْ مِنَ الصّٰلِحٰتِ مِنْ ذَكَرٍ اَوْ اُنْثٰى وَهُوَ مُؤْمِنٌ فَاُولٰۤىِٕكَ يَدْخُلُوْنَ الْجَنَّةَ وَلَا يُظْلَمُوْنَ نَقِيْرًا

waman ya'mal mina **al***shshaa*li*haa*ti min* dz*akarin aw unts*aa *wahuwa mu/minun faul*aa*\-ika yadkhuluuna **a**ljannata wal*aa *yu*zh*lamuuna naqiir*aa**\*n**

Dan barangsiapa mengerjakan amal kebajikan, baik laki-laki maupun perempuan sedang dia beriman, maka mereka itu akan masuk ke dalam surga dan mereka tidak dizalimi sedikit pun.

4:125

# وَمَنْ اَحْسَنُ دِيْنًا مِّمَّنْ اَسْلَمَ وَجْهَهٗ لِلّٰهِ وَهُوَ مُحْسِنٌ وَّاتَّبَعَ مِلَّةَ اِبْرٰهِيْمَ حَنِيْفًا ۗوَاتَّخَذَ اللّٰهُ اِبْرٰهِيْمَ خَلِيْلًا

waman a*h*sanu diinan mimman aslama wajhahu lill*aa*hi wahuwa mu*h*sinun wa**i**ttaba'a millata ibr*aa*hiima *h*aniifan wa**i**ttakha*dz*a **al**l*aa*hu ibr*aa*hiima khaliil

Dan siapakah yang lebih baik agamanya daripada orang yang dengan ikhlas berserah diri kepada Allah, sedang dia mengerjakan kebaikan, dan mengikuti agama Ibrahim yang lurus? Dan Allah telah memilih Ibrahim menjadi kesayangan(-Nya).

4:126

# وَلِلّٰهِ مَا فِى السَّمٰوٰتِ وَمَا فِى الْاَرْضِۗ وَكَانَ اللّٰهُ بِكُلِّ شَيْءٍ مُّحِيْطًا ࣖ

walill*aa*hi m*aa* fii **al**ssam*aa*w*aa*ti wam*aa* fii **a**l-ar*dh*i wak*aa*na **al**l*aa*hu bikulli syay-in mu*h*ii*thaa**\*n**

Dan milik Allah-lah apa yang ada di langit dan apa yang ada di bumi, dan (pengetahuan) Allah meliputi segala sesuatu.

4:127

# وَيَسْتَفْتُوْنَكَ فِى النِّسَاۤءِۗ قُلِ اللّٰهُ يُفْتِيْكُمْ فِيْهِنَّ ۙوَمَا يُتْلٰى عَلَيْكُمْ فِى الْكِتٰبِ فِيْ يَتٰمَى النِّسَاۤءِ الّٰتِيْ لَا تُؤْتُوْنَهُنَّ مَا كُتِبَ لَهُنَّ وَتَرْغَبُوْنَ اَنْ تَنْكِحُوْهُنَّ وَالْمُسْتَضْعَفِيْنَ مِنَ الْوِل

wayastaftuunaka fii **al**nnis*aa*-i quli **al**l*aa*hu yuftiikum fiihinna wam*aa* yutl*aa* 'alaykum fii **a**lkit*aa*bi fii yat*aa*m*aa* **al**nnis*aa*-i

Dan mereka meminta fatwa kepadamu tentang perempuan. Katakanlah, “Allah memberi fatwa kepadamu tentang mereka, dan apa yang dibacakan kepadamu dalam Al-Qur'an (juga memfatwakan) tentang para perempuan yatim yang tidak kamu berikan sesuatu (maskawin) yang

4:128

# وَاِنِ امْرَاَةٌ خَافَتْ مِنْۢ بَعْلِهَا نُشُوْزًا اَوْ اِعْرَاضًا فَلَا جُنَاحَ عَلَيْهِمَآ اَنْ يُّصْلِحَا بَيْنَهُمَا صُلْحًا ۗوَالصُّلْحُ خَيْرٌ ۗوَاُحْضِرَتِ الْاَنْفُسُ الشُّحَّۗ وَاِنْ تُحْسِنُوْا وَتَتَّقُوْا فَاِنَّ اللّٰهَ كَانَ بِمَا تَعْمَلُو

wa-ini imra-atun kh*aa*fat min ba'lih*aa* nusyuuzan aw i'r*aad*an fal*aa* jun*aah*a 'alayhim*aa* an yu*sh*li*haa* baynahum*aa* *sh*ul*h*an wa**al***shsh*ul*h*u khayrun wau*hd<*

Dan jika seorang perempuan khawatir suaminya akan nusyuz atau bersikap tidak acuh, maka keduanya dapat mengadakan perdamaian yang sebenarnya, dan perdamaian itu lebih baik (bagi mereka) walaupun manusia itu menurut tabiatnya kikir. Dan jika kamu memperbai

4:129

# وَلَنْ تَسْتَطِيْعُوْٓا اَنْ تَعْدِلُوْا بَيْنَ النِّسَاۤءِ وَلَوْ حَرَصْتُمْ فَلَا تَمِيْلُوْا كُلَّ الْمَيْلِ فَتَذَرُوْهَا كَالْمُعَلَّقَةِ ۗوَاِنْ تُصْلِحُوْا وَتَتَّقُوْا فَاِنَّ اللّٰهَ كَانَ غَفُوْرًا رَّحِيْمًا

walan tasta*th*ii'uu an ta'diluu bayna **al**nnis*aa*-i walaw *h*ara*sh*tum fal*aa* tamiiluu kulla **a**lmayli fata*dz*aruuh*aa* ka**a**lmu'allaqati wa-in tu*sh*li*h*uu wa

Dan kamu tidak akan dapat berlaku adil di antara istri-istri(mu), walaupun kamu sangat ingin berbuat demikian, karena itu janganlah kamu terlalu cenderung (kepada yang kamu cintai), sehingga kamu biarkan yang lain terkatung-katung. Dan jika kamu mengadaka

4:130

# وَاِنْ يَّتَفَرَّقَا يُغْنِ اللّٰهُ كُلًّا مِّنْ سَعَتِهٖۗ وَكَانَ اللّٰهُ وَاسِعًا حَكِيْمًا

wa-in yatafarraq*aa* yughni **al**l*aa*hu kullan min sa'atihi wak*aa*na **al**l*aa*hu w*aa*si'an *h*akiim*aa**\*n**

Dan jika keduanya bercerai, maka Allah akan memberi kecukupan kepada masing-masing dari karunia-Nya. Dan Allah Mahaluas (karunia-Nya), Mahabijaksana.

4:131

# وَلِلّٰهِ مَا فِى السَّمٰوٰتِ وَمَا فِى الْاَرْضِۗ وَلَقَدْ وَصَّيْنَا الَّذِيْنَ اُوْتُوا الْكِتٰبَ مِنْ قَبْلِكُمْ وَاِيَّاكُمْ اَنِ اتَّقُوا اللّٰهَ ۗوَاِنْ تَكْفُرُوْا فَاِنَّ لِلّٰهِ مَا فِى السَّمٰوٰتِ وَمَا فِى الْاَرْضِۗ وَكَانَ اللّٰهُ غَنِيًّا ح

walill*aa*hi m*aa* fii **al**ssam*aa*w*aa*ti wam*aa* fii **a**l-ar*dh*i walaqad wa*shsh*ayn*aa* **al**la*dz*iina uutuu **a**lkit*aa*ba min qablikum wa-iy

Dan milik Allah-lah apa yang ada di langit dan apa yang ada di bumi, dan sungguh, Kami telah memerintahkan kepada orang yang diberi kitab suci sebelum kamu dan (juga) kepadamu agar bertakwa kepada Allah. Tetapi jika kamu ingkar, maka (ketahuilah), milik A

4:132

# وَلِلّٰهِ مَا فِى السَّمٰوٰتِ وَمَا فِى الْاَرْضِ ۗوَكَفٰى بِاللّٰهِ وَكِيْلًا

walill*aa*hi m*aa* fii **al**ssam*aa*w*aa*ti wam*aa* fii **a**l-ar*dh*i wakaf*aa* bi**al**l*aa*hi wakiil*aa**\*n**

Dan milik Allah-lah apa yang ada di langit dan apa yang ada di bumi. Cukuplah Allah sebagai pemeliharanya.

4:133

# اِنْ يَّشَأْ يُذْهِبْكُمْ اَيُّهَا النَّاسُ وَيَأْتِ بِاٰخَرِيْنَۗ وَكَانَ اللّٰهُ عَلٰى ذٰلِكَ قَدِيْرًا

in yasya/ yu*dz*hibkum ayyuh*aa* **al**nn*aa*su waya/ti bi-*aa*khariina wak*aa*na **al**l*aa*hu 'al*aa* *dzaa*lika qadiir*aa**\*n**

Kalau Allah menghendaki, niscaya dimusnahkan-Nya kamu semua wahai manusia! Kemudian Dia datangkan (umat) yang lain (sebagai penggantimu). Dan Allah Mahakuasa berbuat demikian.

4:134

# مَنْ كَانَ يُرِيْدُ ثَوَابَ الدُّنْيَا فَعِنْدَ اللّٰهِ ثَوَابُ الدُّنْيَا وَالْاٰخِرَةِ ۗوَكَانَ اللّٰهُ سَمِيْعًاۢ بَصِيْرًا ࣖ

man k*aa*na yuriidu tsaw*aa*ba **al**dduny*aa* fa'inda **al**l*aa*hi tsaw*aa*bu **al**dduny*aa* wa**a**l-*aa*khirati wak*aa*na **al**l*aa*hu sami

Barangsiapa menghendaki pahala di dunia maka ketahuilah bahwa di sisi Allah ada pahala dunia dan akhirat. Dan Allah Maha Mendengar, Maha Melihat.

4:135

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْا كُوْنُوْا قَوَّامِيْنَ بِالْقِسْطِ شُهَدَاۤءَ لِلّٰهِ وَلَوْ عَلٰٓى اَنْفُسِكُمْ اَوِ الْوَالِدَيْنِ وَالْاَقْرَبِيْنَ ۚ اِنْ يَّكُنْ غَنِيًّا اَوْ فَقِيْرًا فَاللّٰهُ اَوْلٰى بِهِمَاۗ فَلَا تَتَّبِعُوا الْهَوٰٓى اَنْ تَعْ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu kuunuu qaww*aa*miina bi**a**lqis*th*i syuhad*aa*-a lill*aa*hi walaw 'al*aa* anfusikum awi **a**lw*aa*lidayni wa**a**

**Wahai orang-orang yang beriman! Jadilah kamu penegak keadilan, menjadi saksi karena Allah, walaupun terhadap dirimu sendiri atau terhadap ibu bapak dan kaum kerabatmu. Jika dia (yang terdakwa) kaya ataupun miskin, maka Allah lebih tahu kemaslahatan (kebai**

4:136

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْٓا اٰمِنُوْا بِاللّٰهِ وَرَسُوْلِهٖ وَالْكِتٰبِ الَّذِيْ نَزَّلَ عَلٰى رَسُوْلِهٖ وَالْكِتٰبِ الَّذِيْٓ اَنْزَلَ مِنْ قَبْلُ ۗوَمَنْ يَّكْفُرْ بِاللّٰهِ وَمَلٰۤىِٕكَتِهٖ وَكُتُبِهٖ وَرُسُلِهٖ وَالْيَوْمِ الْاٰخِرِ فَقَدْ ضَل

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu *aa*minuu bi**al**l*aa*hi warasuulihi wa**a**lkit*aa*bi **al**la*dz*ii nazzala 'al*aa* rasuulihi wa**a**

**Wahai orang-orang yang beriman! Tetaplah beriman kepada Allah dan Rasul-Nya (Muhammad) dan kepada Kitab (Al-Qur'an) yang diturunkan kepada Rasul-Nya, serta kitab yang diturunkan sebelumnya. Barangsiapa ingkar kepada Allah, malaikat-malaikat-Nya, kitab-kit**

4:137

# اِنَّ الَّذِيْنَ اٰمَنُوْا ثُمَّ كَفَرُوْا ثُمَّ اٰمَنُوْا ثُمَّ كَفَرُوْا ثُمَّ ازْدَادُوْا كُفْرًا لَّمْ يَكُنِ اللّٰهُ لِيَغْفِرَ لَهُمْ وَلَا لِيَهْدِيَهُمْ سَبِيْلًاۗ

inna **al**la*dz*iina *aa*manuu tsumma kafaruu tsumma *aa*manuu tsumma kafaruu tsumma izd*aa*duu kufran lam yakuni **al**l*aa*hu liyaghfira lahum wal*aa* liyahdiyahum sabiil*aa**\*n**

**Sesungguhnya orang-orang yang beriman lalu kafir, kemudian beriman (lagi), kemudian kafir lagi, lalu bertambah kekafirannya, maka Allah tidak akan mengampuni mereka, dan tidak (pula) menunjukkan kepada mereka jalan (yang lurus).**

4:138

# بَشِّرِ الْمُنٰفِقِيْنَ بِاَنَّ لَهُمْ عَذَابًا اَلِيْمًاۙ

basysyiri **a**lmun*aa*fiqiina bi-anna lahum 'a*dzaa*ban **a**liim*aa**\*n**

Kabarkanlah kepada orang-orang munafik bahwa mereka akan mendapat siksaan yang pedih,

4:139

# ۨالَّذِيْنَ يَتَّخِذُوْنَ الْكٰفِرِيْنَ اَوْلِيَاۤءَ مِنْ دُوْنِ الْمُؤْمِنِيْنَ ۗ اَيَبْتَغُوْنَ عِنْدَهُمُ الْعِزَّةَ فَاِنَّ الْعِزَّةَ لِلّٰهِ جَمِيْعًاۗ

**al**la*dz*iina yattakhi*dz*uuna **a**lk*aa*firiina awliy*aa*-a min duuni **a**lmu/miniina ayabtaghuuna 'indahumu **a**l'izzata fa-inna **a**l'izzata lill*aa*hi jam

(yaitu) orang-orang yang menjadikan orang-orang kafir sebagai pemimpin dengan meninggalkan orang-orang mukmin. Apakah mereka mencari kekuatan di sisi orang kafir itu? Ketahuilah bahwa semua kekuatan itu milik Allah.

4:140

# وَقَدْ نَزَّلَ عَلَيْكُمْ فِى الْكِتٰبِ اَنْ اِذَا سَمِعْتُمْ اٰيٰتِ اللّٰهِ يُكْفَرُ بِهَا وَيُسْتَهْزَاُ بِهَا فَلَا تَقْعُدُوْا مَعَهُمْ حَتّٰى يَخُوْضُوْا فِيْ حَدِيْثٍ غَيْرِهٖٓ ۖ اِنَّكُمْ اِذًا مِّثْلُهُمْ ۗ اِنَّ اللّٰهَ جَامِعُ الْمُنٰفِقِيْنَ و

waqad nazzala 'alaykum fii **a**lkit*aa*bi an i*dzaa* sami'tum *aa*y*aa*ti **al**l*aa*hi yukfaru bih*aa* wayustahzau bih*aa* fal*aa* taq'uduu ma'ahum *h*att*aa* yakhuu*dh*uu

Dan sungguh, Allah telah menurunkan (ketentuan) bagimu di dalam Kitab (Al-Qur'an) bahwa apabila kamu mendengar ayat-ayat Allah diingkari dan diperolok-olokkan (oleh orang-orang kafir), maka janganlah kamu duduk bersama mereka, sebelum mereka memasuki pemb

4:141

# ۨالَّذِيْنَ يَتَرَبَّصُوْنَ بِكُمْۗ فَاِنْ كَانَ لَكُمْ فَتْحٌ مِّنَ اللّٰهِ قَالُوْٓا اَلَمْ نَكُنْ مَّعَكُمْ ۖ وَاِنْ كَانَ لِلْكٰفِرِيْنَ نَصِيْبٌ قَالُوْٓا اَلَمْ نَسْتَحْوِذْ عَلَيْكُمْ وَنَمْنَعْكُمْ مِّنَ الْمُؤْمِنِيْنَ ۗ فَاللّٰهُ يَحْكُمُ بَيْن

**al**la*dz*iina yatarabba*sh*uuna bikum fa-in k*aa*na lakum fat*h*un mina **al**l*aa*hi q*aa*luu alam nakun ma'akum wa-in k*aa*na lilk*aa*firiina na*sh*iibun q*aa*luu alam nasta<

(yaitu) orang yang menunggu-nunggu (peristiwa) yang akan terjadi pada dirimu. Apabila kamu mendapat kemenangan dari Allah mereka berkata, “Bukankah kami (turut berperang) bersama kamu?” Dan jika orang kafir mendapat bagian, mereka berkata, “Bukankah kami

4:142

# اِنَّ الْمُنٰفِقِيْنَ يُخٰدِعُوْنَ اللّٰهَ وَهُوَ خَادِعُهُمْۚ وَاِذَا قَامُوْٓا اِلَى الصَّلٰوةِ قَامُوْا كُسَالٰىۙ يُرَاۤءُوْنَ النَّاسَ وَلَا يَذْكُرُوْنَ اللّٰهَ اِلَّا قَلِيْلًاۖ

inna **a**lmun*aa*fiqiina yukh*aa*di'uuna **al**l*aa*ha wahuwa kh*aa*di'uhum wa-i*dzaa* q*aa*muu il*aa* **al***shsh*al*aa*ti q*aa*muu kus*aa*l*aa *yur*aa<*

Sesungguhnya orang munafik itu hendak menipu Allah, tetapi Allah-lah yang menipu mereka. Apabila mereka berdiri untuk salat, mereka lakukan dengan malas. Mereka bermaksud ria (ingin dipuji) di hadapan manusia. Dan mereka tidak mengingat Allah kecuali sedi

4:143

# مُّذَبْذَبِيْنَ بَيْنَ ذٰلِكَۖ لَآ اِلٰى هٰٓؤُلَاۤءِ وَلَآ اِلٰى هٰٓؤُلَاۤءِ ۗ وَمَنْ يُّضْلِلِ اللّٰهُ فَلَنْ تَجِدَ لَهٗ سَبِيْلًا

mu*dz*ab*dz*abiina bayna *dzaa*lika l*aa* il*aa* h*aa*ul*aa*-i wal*aa* il*aa* h*aa*ul*aa*-i waman yu*dh*lili **al**l*aa*hu falan tajida lahu sabiil*aa**\*n**

Mereka dalam keadaan ragu antara yang demikian (iman atau kafir), tidak termasuk kepada golongan ini (orang beriman) dan tidak (pula) kepada golongan itu (orang kafir). Barangsiapa dibiarkan sesat oleh Allah, maka kamu tidak akan mendapatkan jalan (untuk

4:144

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْا لَا تَتَّخِذُوا الْكٰفِرِيْنَ اَوْلِيَاۤءَ مِنْ دُوْنِ الْمُؤْمِنِيْنَ ۚ اَتُرِيْدُوْنَ اَنْ تَجْعَلُوْا لِلّٰهِ عَلَيْكُمْ سُلْطٰنًا مُّبِيْنًا

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu l*aa* tattakhi*dz*uu **a**lk*aa*firiina awliy*aa*-a min duuni **a**lmu/miniina aturiiduuna an taj'aluu lill*aa*hi 'alaykum sul

Wahai orang-orang yang beriman! Janganlah kamu menjadikan orang-orang kafir sebagai pemimpin selain dari orang-orang mukmin. Apakah kamu ingin memberi alasan yang jelas bagi Allah (untuk menghukummu)?

4:145

# اِنَّ الْمُنٰفِقِيْنَ فِى الدَّرْكِ الْاَسْفَلِ مِنَ النَّارِۚ وَلَنْ تَجِدَ لَهُمْ نَصِيْرًاۙ

inna **a**lmun*aa*fiqiina fii **al**ddarki **a**l-asfali mina **al**nn*aa*ri walan tajida lahum na*sh*iir*aa**\*n**

Sungguh, orang-orang munafik itu (ditempatkan) pada tingkatan yang paling bawah dari neraka. Dan kamu tidak akan mendapat seorang penolong pun bagi mereka.

4:146

# اِلَّا الَّذِيْنَ تَابُوْا وَاَصْلَحُوْا وَاعْتَصَمُوْا بِاللّٰهِ وَاَخْلَصُوْا دِيْنَهُمْ لِلّٰهِ فَاُولٰۤىِٕكَ مَعَ الْمُؤْمِنِيْنَۗ وَسَوْفَ يُؤْتِ اللّٰهُ الْمُؤْمِنِيْنَ اَجْرًا عَظِيْمًا

ill*aa* **al**la*dz*iina t*aa*buu wa-a*sh*la*h*uu wa**i**'ta*sh*amuu bi**al**l*aa*hi wa-akhla*sh*uu diinahum lill*aa*hi faul*aa*-ika ma'a **a**lmu/miniin

Kecuali orang-orang yang bertobat dan memperbaiki diri dan berpegang teguh pada (agama) Allah dan dengan tulus ikhlas (menjalankan) agama mereka karena Allah. Maka mereka itu bersama-sama orang-orang yang beriman dan kelak Allah akan memberikan pahala yan

4:147

# مَا يَفْعَلُ اللّٰهُ بِعَذَابِكُمْ اِنْ شَكَرْتُمْ وَاٰمَنْتُمْۗ وَكَانَ اللّٰهُ شَاكِرًا عَلِيْمًا ۔

m*aa* yaf'alu **al**l*aa*hu bi'a*dzaa*bikum in syakartum wa*aa*mantum wak*aa*na **al**l*aa*hu sy*aa*kiran 'aliim*aa**\*n**

Allah tidak akan menyiksamu, jika kamu bersyukur dan beriman. Dan Allah Maha Mensyukuri, Maha Mengetahui.

4:148

# ۞ لَا يُحِبُّ اللّٰهُ الْجَهْرَ بِالسُّوْۤءِ مِنَ الْقَوْلِ اِلَّا مَنْ ظُلِمَ ۗ وَكَانَ اللّٰهُ سَمِيْعًا عَلِيْمًا

l*aa* yu*h*ibbu **al**l*aa*hu **a**ljahra bi**al**ssuu-i mina **a**lqawli ill*aa* man *zh*ulima wak*aa*na **al**l*aa*hu samii'an 'aliim*aa**\*n**

**Allah tidak menyukai perkataan buruk, (yang diucapkan) secara terus terang kecuali oleh orang yang dizalimi. Dan Allah Maha Mendengar, Maha Mengetahui.**

4:149

# اِنْ تُبْدُوْا خَيْرًا اَوْ تُخْفُوْهُ اَوْ تَعْفُوْا عَنْ سُوْۤءٍ فَاِنَّ اللّٰهَ كَانَ عَفُوًّا قَدِيْرًا

in tubduu khayran aw tukhfuuhu aw ta'fuu 'an suu-in fa-inna **al**l*aa*ha k*aa*na 'afuwwan qadiir*aa**\*n**

Jika kamu menyatakan sesuatu kebajikan, menyembunyikannya atau memaafkan suatu kesalahan (orang lain), maka sungguh, Allah Maha Pemaaf, Mahakuasa.

4:150

# اِنَّ الَّذِيْنَ يَكْفُرُوْنَ بِاللّٰهِ وَرُسُلِهٖ وَيُرِيْدُوْنَ اَنْ يُّفَرِّقُوْا بَيْنَ اللّٰهِ وَرُسُلِهٖ وَيَقُوْلُوْنَ نُؤْمِنُ بِبَعْضٍ وَّنَكْفُرُ بِبَعْضٍۙ وَّيُرِيْدُوْنَ اَنْ يَّتَّخِذُوْا بَيْنَ ذٰلِكَ سَبِيْلًاۙ

inna **al**la*dz*iina yakfuruuna bi**al**l*aa*hi warusulihi wayuriiduuna an yufarriquu bayna **al**l*aa*hi warusulihi wayaquuluuna nu/minu biba'*dh*in wanakfuru biba'*dh*in wayuriiduuna an ya

Sesungguhnya orang-orang yang ingkar kepada Allah dan rasul-rasul-Nya, dan bermaksud membeda-bedakan antara (keimanan kepada) Allah dan rasul-rasul-Nya, dengan mengatakan, “Kami beriman kepada sebagian dan kami mengingkari sebagian (yang lain),” serta ber

4:151

# اُولٰۤىِٕكَ هُمُ الْكٰفِرُوْنَ حَقًّا ۚوَاَعْتَدْنَا لِلْكٰفِرِيْنَ عَذَابًا مُّهِيْنًا

ul*aa*-ika humu **a**lk*aa*firuuna *h*aqqan wa-a'tadn*aa* lilk*aa*firiina 'a*dzaa*ban muhiin*aa**\*n**

merekalah orang-orang kafir yang sebenarnya. Dan Kami sediakan untuk orang-orang kafir itu azab yang menghinakan.

4:152

# وَالَّذِيْنَ اٰمَنُوْا بِاللّٰهِ وَرُسُلِهٖ وَلَمْ يُفَرِّقُوْا بَيْنَ اَحَدٍ مِّنْهُمْ اُولٰۤىِٕكَ سَوْفَ يُؤْتِيْهِمْ اُجُوْرَهُمْ ۗوَكَانَ اللّٰهُ غَفُوْرًا رَّحِيْمًا ࣖ

wa**a**lla*dz*iina *aa*manuu bi**al**l*aa*hi warusulihi walam yufarriquu bayna a*h*adin minhum ul*aa*-ika sawfa yu/tiihim ujuurahum wak*aa*na **al**l*aa*hu ghafuuran ra*h*iim

Adapun orang-orang yang beriman kepada Allah dan rasul-rasul-Nya dan tidak membeda-bedakan di antara mereka (para rasul), kelak Allah akan memberikan pahala kepada mereka. Allah Maha Pengampun, Maha Penyayang.

4:153

# يَسْـَٔلُكَ اَهْلُ الْكِتٰبِ اَنْ تُنَزِّلَ عَلَيْهِمْ كِتٰبًا مِّنَ السَّمَاۤءِ فَقَدْ سَاَلُوْا مُوْسٰٓى اَكْبَرَ مِنْ ذٰلِكَ فَقَالُوْٓا اَرِنَا اللّٰهَ جَهْرَةً فَاَخَذَتْهُمُ الصَّاعِقَةُ بِظُلْمِهِمْۚ ثُمَّ اتَّخَذُوا الْعِجْلَ مِنْۢ بَعْدِ مَا جَا

yas-aluka ahlu **a**lkit*aa*bi an tunazzila 'alayhim kit*aa*ban mina **al**ssam*aa*-i faqad sa-aluu muus*aa* akbara min *dzaa*lika faq*aa*luu arin*aa* **al**l*aa*ha jahratan fa

(Orang-orang) Ahli Kitab meminta kepadamu (Muhammad) agar engkau menurunkan sebuah kitab dari langit kepada mereka. Sesungguhnya mereka telah meminta kepada Musa yang lebih besar dari itu. Mereka berkata, “Perlihatkanlah Allah kepada kami secara nyata.” M

4:154

# وَرَفَعْنَا فَوْقَهُمُ الطُّوْرَ بِمِيْثَاقِهِمْ وَقُلْنَا لَهُمُ ادْخُلُوا الْبَابَ سُجَّدًا وَّقُلْنَا لَهُمْ لَا تَعْدُوْا فِى السَّبْتِ وَاَخَذْنَا مِنْهُمْ مِّيْثَاقًا غَلِيْظًا

warafa'n*aa* fawqahumu **al***ththh*uura bimiits*aa*qihim waquln*aa *lahumu udkhuluu **a**lb*aa*ba sujjadan waquln*aa *lahum l*aa *ta'duu fii **al**ssabti wa-akha*dz*n*aa*

Dan Kami angkat gunung (Sinai) di atas mereka untuk (menguatkan) perjanjian mereka. Dan Kami perintahkan kepada mereka, “Masukilah pintu gerbang (Baitulmaqdis) itu sambil bersujud,” dan Kami perintahkan (pula) kepada mereka, “Janganlah kamu melanggar pera

4:155

# فَبِمَا نَقْضِهِمْ مِّيْثَاقَهُمْ وَكُفْرِهِمْ بِاٰيٰتِ اللّٰهِ وَقَتْلِهِمُ الْاَنْۢبِيَاۤءَ بِغَيْرِ حَقٍّ وَّقَوْلِهِمْ قُلُوْبُنَا غُلْفٌ ۗ بَلْ طَبَعَ اللّٰهُ عَلَيْهَا بِكُفْرِهِمْ فَلَا يُؤْمِنُوْنَ اِلَّا قَلِيْلًاۖ

fabim*aa* naq*dh*ihim miits*aa*qahum wakufrihim bi-*aa*y*aa*ti **al**l*aa*hi waqatlihimu **a**l-anbiy*aa*-a bighayri *h*aqqin waqawlihim quluubun*aa* ghulfun bal *th*aba'a

Maka (Kami hukum mereka), karena mereka melanggar perjanjian itu, karena kekafiran mereka terhadap keterangan-keterangan Allah, dan karena mereka telah membunuh nabi-nabi tanpa hak (alasan yang benar) dan karena mereka mengatakan, “Hati kami tertutup.” Se

4:156

# وَّبِكُفْرِهِمْ وَقَوْلِهِمْ عَلٰى مَرْيَمَ بُهْتَانًا عَظِيْمًاۙ

wabikufrihim waqawlihim 'al*aa* maryama buht*aa*nan 'a*zh*iim*aa**\*n**

dan (Kami hukum juga) karena kekafiran mereka (terhadap Isa), dan tuduhan mereka yang sangat keji terhadap Maryam,

4:157

# وَّقَوْلِهِمْ اِنَّا قَتَلْنَا الْمَسِيْحَ عِيْسَى ابْنَ مَرْيَمَ رَسُوْلَ اللّٰهِۚ وَمَا قَتَلُوْهُ وَمَا صَلَبُوْهُ وَلٰكِنْ شُبِّهَ لَهُمْ ۗوَاِنَّ الَّذِيْنَ اخْتَلَفُوْا فِيْهِ لَفِيْ شَكٍّ مِّنْهُ ۗمَا لَهُمْ بِهٖ مِنْ عِلْمٍ اِلَّا اتِّبَاعَ الظَّن

waqawlihim inn*aa* qataln*aa* **a**lmasii*h*a 'iis*aa* ibna maryama rasuula **al**l*aa*hi wam*aa* qataluuhu wam*aa* *sh*alabuuhu wal*aa*kin syubbiha lahum wa-inna **al**la

dan (Kami hukum juga) karena ucapan mereka, “Sesungguhnya kami telah membunuh Al-Masih, Isa putra Maryam, Rasul Allah,” padahal mereka tidak membunuhnya dan tidak (pula) menyalibnya, tetapi (yang mereka bunuh adalah) orang yang diserupakan dengan Isa. Ses

4:158

# بَلْ رَّفَعَهُ اللّٰهُ اِلَيْهِ ۗوَكَانَ اللّٰهُ عَزِيْزًا حَكِيْمًا

bal rafa'ahu **al**l*aa*hu ilayhi wak*aa*na **al**l*aa*hu 'aziizan *h*akiim*aa**\*n**

Tetapi Allah telah mengangkat Isa ke hadirat-Nya. Allah Mahaperkasa, Mahabijaksana.

4:159

# وَاِنْ مِّنْ اَهْلِ الْكِتٰبِ اِلَّا لَيُؤْمِنَنَّ بِهٖ قَبْلَ مَوْتِهٖ ۚوَيَوْمَ الْقِيٰمَةِ يَكُوْنُ عَلَيْهِمْ شَهِيْدًاۚ

wa-in min ahli **a**lkit*aa*bi ill*aa* layu/minanna bihi qabla mawtihi wayawma **a**lqiy*aa*mati yakuunu 'alayhim syahiid*aa**\*n**

Tidak ada seorang pun di antara Ahli Kitab yang tidak beriman kepadanya (Isa) menjelang kematiannya. Dan pada hari Kiamat dia (Isa) akan menjadi saksi mereka.

4:160

# فَبِظُلْمٍ مِّنَ الَّذِيْنَ هَادُوْا حَرَّمْنَا عَلَيْهِمْ طَيِّبٰتٍ اُحِلَّتْ لَهُمْ وَبِصَدِّهِمْ عَنْ سَبِيْلِ اللّٰهِ كَثِيْرًاۙ

fabi*zh*ulmin mina **al**la*dz*iina h*aa*duu *h*arramn*aa* 'alayhim *th*ayyib*aa*tin u*h*illat lahum wabi*sh*addihim 'an sabiili **al**l*aa*hi katsiir*aa**\*n**

Karena kezaliman orang-orang Yahudi, Kami haramkan bagi mereka makanan yang baik-baik yang (dahulu) pernah dihalalkan; dan karena mereka sering menghalangi (orang lain) dari jalan Allah,

4:161

# وَّاَخْذِهِمُ الرِّبٰوا وَقَدْ نُهُوْا عَنْهُ وَاَكْلِهِمْ اَمْوَالَ النَّاسِ بِالْبَاطِلِ ۗوَاَعْتَدْنَا لِلْكٰفِرِيْنَ مِنْهُمْ عَذَابًا اَلِيْمًا

wa-akh*dz*ihimu **al**rrib*aa* waqad nuhuu 'anhu wa-aklihim amw*aa*la **al**nn*aa*si bi**a**lb*aath*ili wa-a'tadn*aa* lilk*aa*firiina minhum 'a*dzaa*ban **a**liim<

dan karena mereka menjalankan riba, padahal sungguh mereka telah dilarang darinya, dan karena mereka memakan harta orang dengan cara tidak sah (batil). Dan Kami sediakan untuk orang-orang kafir di antara mereka azab yang pedih.

4:162

# لٰكِنِ الرَّاسِخُوْنَ فِى الْعِلْمِ مِنْهُمْ وَالْمُؤْمِنُوْنَ يُؤْمِنُوْنَ بِمَآ اُنْزِلَ اِلَيْكَ وَمَآ اُنْزِلَ مِنْ قَبْلِكَ وَالْمُقِيْمِيْنَ الصَّلٰوةَ وَالْمُؤْتُوْنَ الزَّكٰوةَ وَالْمُؤْمِنُوْنَ بِاللّٰهِ وَالْيَوْمِ الْاٰخِرِۗ اُولٰۤىِٕكَ سَنُؤ

l*aa*kini **al**rr*aa*sikhuuna fii **a**l'ilmi minhum wa**a**lmu/minuuna yu/minuuna bim*aa* unzila ilayka wam*aa* unzila min qablika wa**a**lmuqiimiina **al***shsh*a

Tetapi orang-orang yang ilmunya mendalam di antara mereka, dan orang-orang yang beriman, mereka beriman kepada (Al-Qur'an) yang diturunkan kepadamu (Muhammad), dan kepada (kitab-kitab) yang diturunkan sebelummu, begitu pula mereka yang melaksanakan salat

4:163

# ۞ اِنَّآ اَوْحَيْنَآ اِلَيْكَ كَمَآ اَوْحَيْنَآ اِلٰى نُوْحٍ وَّالنَّبِيّٖنَ مِنْۢ بَعْدِهٖۚ وَاَوْحَيْنَآ اِلٰٓى اِبْرٰهِيْمَ وَاِسْمٰعِيْلَ وَاِسْحٰقَ وَيَعْقُوْبَ وَالْاَسْبَاطِ وَعِيْسٰى وَاَيُّوْبَ وَيُوْنُسَ وَهٰرُوْنَ وَسُلَيْمٰنَ ۚوَاٰتَيْنَا

inn*aa* aw*h*ayn*aa* ilayka kam*aa* aw*h*ayn*aa* il*aa* nuu*h*in wa**al**nnabiyyiina min ba'dihi wa-aw*h*ayn*aa* il*aa* ibr*aa*hiima wa-ism*aa*'iila wa-is*haa*qa waya'quuba

Sesungguhnya Kami mewahyukan kepadamu (Muhammad) sebagaimana Kami telah mewahyukan kepada Nuh dan nabi-nabi setelahnya, dan Kami telah mewahyukan (pula) kepada Ibrahim, Ismail, Ishak, Yakub dan anak cucunya; Isa, Ayyub, Yunus, Harun dan Sulaiman. Dan Kami

4:164

# وَرُسُلًا قَدْ قَصَصْنٰهُمْ عَلَيْكَ مِنْ قَبْلُ وَرُسُلًا لَّمْ نَقْصُصْهُمْ عَلَيْكَ ۗوَكَلَّمَ اللّٰهُ مُوْسٰى تَكْلِيْمًاۚ

warusulan qad qa*sh*a*sh*n*aa*hum 'alayka min qablu warusulan lam naq*sh*u*sh*hum 'alayka wakallama **al**l*aa*hu muus*aa* takliim*aa**\*n**

Dan ada beberapa rasul yang telah Kami kisahkan mereka kepadamu sebelumnya dan ada beberapa rasul (la-in) yang tidak Kami kisahkan mereka kepadamu. Dan kepada Musa, Allah berfirman langsung.

4:165

# رُسُلًا مُّبَشِّرِيْنَ وَمُنْذِرِيْنَ لِئَلَّا يَكُوْنَ لِلنَّاسِ عَلَى اللّٰهِ حُجَّةٌ ۢ بَعْدَ الرُّسُلِ ۗوَكَانَ اللّٰهُ عَزِيْزًا حَكِيْمًا

rusulan mubasysyiriina wamun*dz*iriina li-all*aa* yakuuna li**l**nn*aa*si 'al*aa* **al**l*aa*hi *h*ujjatun ba'da **al**rrusuli wak*aa*na **al**l*aa*hu 'aziizan

Rasul-rasul itu adalah sebagai pembawa berita gembira dan pemberi peringatan, agar tidak ada alasan bagi manusia untuk membantah Allah setelah rasul-rasul itu diutus. Allah Mahaperkasa, Mahabijaksana.

4:166

# لٰكِنِ اللّٰهُ يَشْهَدُ بِمَآ اَنْزَلَ اِلَيْكَ اَنْزَلَهٗ بِعِلْمِهٖ ۚوَالْمَلٰۤىِٕكَةُ يَشْهَدُوْنَ ۗوَكَفٰى بِاللّٰهِ شَهِيْدًاۗ

l*aa*kini **al**l*aa*hu yasyhadu bim*aa* anzala ilayka anzalahu bi'ilmihi wa**a**lmal*aa*-ikatu yasyhaduuna wakaf*aa* bi**al**l*aa*hi syahiid*aa**\*n**

Tetapi Allah menjadi saksi atas (Al-Qur'an) yang diturunkan-Nya kepadamu (Muhammad). Dia menurunkannya dengan ilmu-Nya, dan para malaikat pun menyaksikan. Dan cukuplah Allah yang menjadi saksi.

4:167

# اِنَّ الَّذِيْنَ كَفَرُوْا وَصَدُّوْا عَنْ سَبِيْلِ اللّٰهِ قَدْ ضَلُّوْا ضَلٰلًا ۢ بَعِيْدًا

inna **al**la*dz*iina kafaruu wa*sh*adduu 'an sabiili **al**l*aa*hi qad *dh*alluu *dh*al*aa*lan ba'iid*aa**\*n**

Sesungguhnya orang-orang yang kafir dan menghalang-halangi (orang lain) dari jalan Allah, benar-benar telah sesat sejauh-jauhnya.

4:168

# اِنَّ الَّذِيْنَ كَفَرُوْا وَظَلَمُوْا لَمْ يَكُنِ اللّٰهُ لِيَغْفِرَ لَهُمْ وَلَا لِيَهْدِيَهُمْ طَرِيْقًاۙ

inna **al**la*dz*iina kafaruu wa*zh*alamuu lam yakuni **al**l*aa*hu liyaghfira lahum wal*aa* liyahdiyahum *th*ariiq*aa**\*n**

Sesungguhnya orang-orang yang kafir dan melakukan kezaliman, Allah tidak akan mengampuni mereka, dan tidak (pula) akan menunjukkan kepada mereka jalan (yang lurus),

4:169

# اِلَّا طَرِيْقَ جَهَنَّمَ خٰلِدِيْنَ فِيْهَآ اَبَدًا ۗوَكَانَ ذٰلِكَ عَلَى اللّٰهِ يَسِيْرًا

ill*aa* *th*ariiqa jahannama kh*aa*lidiina fiih*aa* abadan wak*aa*na *dzaa*lika 'al*aa* **al**l*aa*hi yasiir*aa**\*n**

kecuali jalan ke neraka Jahanam; mereka kekal di dalamnya selama-lamanya. Dan hal itu (sangat) mudah bagi Allah.

4:170

# يٰٓاَيُّهَا النَّاسُ قَدْ جَاۤءَكُمُ الرَّسُوْلُ بِالْحَقِّ مِنْ رَّبِّكُمْ فَاٰمِنُوْا خَيْرًا لَّكُمْ ۗوَاِنْ تَكْفُرُوْا فَاِنَّ لِلّٰهِ مَا فِى السَّمٰوٰتِ وَالْاَرْضِۗ وَكَانَ اللّٰهُ عَلِيْمًا حَكِيْمًا

y*aa* ayyuh*aa* **al**nn*aa*su qad j*aa*-akumu **al**rrasuulu bi**a**l*h*aqqi min rabbikum fa*aa*minuu khayran lakum wa-in takfuruu fa-inna lill*aa*hi m*aa* fii **al**

**Wahai manusia! Sungguh, telah datang Rasul (Muhammad) kepadamu dengan (membawa) kebenaran dari Tuhanmu, maka berimanlah (kepadanya), itu lebih baik bagimu. Dan jika kamu kafir, (itu tidak merugikan Allah sedikit pun) karena sesungguhnya milik Allah-lah ap**

4:171

# يٰٓاَهْلَ الْكِتٰبِ لَا تَغْلُوْا فِيْ دِيْنِكُمْ وَلَا تَقُوْلُوْا عَلَى اللّٰهِ اِلَّا الْحَقَّۗ اِنَّمَا الْمَسِيْحُ عِيْسَى ابْنُ مَرْيَمَ رَسُوْلُ اللّٰهِ وَكَلِمَتُهٗ ۚ اَلْقٰهَآ اِلٰى مَرْيَمَ وَرُوْحٌ مِّنْهُ ۖفَاٰمِنُوْا بِاللّٰهِ وَرُسُلِهٖۗ وَ

y*aa* ahla **a**lkit*aa*bi l*aa* taghluu fii diinikum wal*aa* taquuluu 'al*aa* **al**l*aa*hi ill*aa* **a**l*h*aqqa innam*aa* **a**lmasii*h*u 'iis*aa*

*Wahai Ahli Kitab! Janganlah kamu melampaui batas dalam agamamu, dan janganlah kamu mengatakan terhadap Allah kecuali yang benar. Sungguh, Al-Masih Isa putra Maryam itu adalah utusan Allah dan (yang diciptakan dengan) kalimat-Nya yang disampaikan-Nya kepad*

4:172

# لَنْ يَّسْتَنْكِفَ الْمَسِيْحُ اَنْ يَّكُوْنَ عَبْدًا لِّلّٰهِ وَلَا الْمَلٰۤىِٕكَةُ الْمُقَرَّبُوْنَۗ وَمَنْ يَّسْتَنْكِفْ عَنْ عِبَادَتِهٖ وَيَسْتَكْبِرْ فَسَيَحْشُرُهُمْ اِلَيْهِ جَمِيْعًا

lan yastankifa **a**lmasii*h*u an yakuuna 'abdan lill*aa*hi wal*aa* **a**lmal*aa*-ikatu **a**lmuqarrabuuna waman yastankif 'an 'ib*aa*datihi wayastakbir fasaya*h*syuruhum ilayhi jamii'

Al-Masih sama sekali tidak enggan menjadi hamba Allah, dan begitu pula para malaikat yang terdekat (kepada Allah). Dan barangsiapa enggan menyembah-Nya dan menyombongkan diri, maka Allah akan mengumpulkan mereka semua kepada-Nya.

4:173

# فَاَمَّا الَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِ فَيُوَفِّيْهِمْ اُجُوْرَهُمْ وَيَزِيْدُهُمْ مِّنْ فَضْلِهٖۚ وَاَمَّا الَّذِيْنَ اسْتَنْكَفُوْا وَاسْتَكْبَرُوْا فَيُعَذِّبُهُمْ عَذَابًا اَلِيْمًاۙ وَّلَا يَجِدُوْنَ لَهُمْ مِّنْ دُوْنِ اللّٰهِ وَلِيًّا

fa-amm*aa* **al**la*dz*iina *aa*manuu wa'amiluu **al***shshaa*li*haa*ti fayuwaffiihim ujuurahum wayaziiduhum min fa*dh*lihi wa-amm*aa* **al**la*dz*iina istankafuu wa**i**

**Adapun orang-orang yang beriman dan mengerjakan kebajikan, Allah akan menyempurnakan pahala bagi mereka dan menambah sebagian dari karunia-Nya. Sedangkan orang-orang yang enggan (menyembah Allah) dan menyombongkan diri, maka Allah akan mengazab mereka den**

4:174

# يٰٓاَيُّهَا النَّاسُ قَدْ جَاۤءَكُمْ بُرْهَانٌ مِّنْ رَّبِّكُمْ وَاَنْزَلْنَآ اِلَيْكُمْ نُوْرًا مُّبِيْنًا

y*aa* ayyuh*aa* **al**nn*aa*su qad j*aa*-akum burh*aa*nun min rabbikum wa-anzaln*aa* ilaykum nuuran mubiin*aa**\*n**

Wahai manusia! Sesungguhnya telah sampai kepadamu bukti kebenaran dari Tuhanmu, (Muhammad dengan mukjizatnya) dan telah Kami turunkan kepadamu cahaya yang terang benderang (Al-Qur'an).

4:175

# فَاَمَّا الَّذِيْنَ اٰمَنُوْا بِاللّٰهِ وَاعْتَصَمُوْا بِهٖ فَسَيُدْخِلُهُمْ فِيْ رَحْمَةٍ مِّنْهُ وَفَضْلٍۙ وَّيَهْدِيْهِمْ اِلَيْهِ صِرَاطًا مُّسْتَقِيْمًاۗ

fa-amm*aa* **al**la*dz*iina *aa*manuu bi**al**l*aa*hi wa**i**'ta*sh*amuu bihi fasayudkhiluhum fii ra*h*matin minhu wafa*dh*lin wayahdiihim ilayhi *sh*ir*aath*an mustaqiim<

Adapun orang-orang yang beriman kepada Allah dan berpegang teguh kepada (agama)-Nya, maka Allah akan memasukkan mereka ke dalam rahmat dan karunia dari-Nya (surga), dan menunjukkan mereka jalan yang lurus kepada-Nya.

4:176

# يَسْتَفْتُوْنَكَۗ قُلِ اللّٰهُ يُفْتِيْكُمْ فِى الْكَلٰلَةِ ۗاِنِ امْرُؤٌا هَلَكَ لَيْسَ لَهٗ وَلَدٌ وَّلَهٗٓ اُخْتٌ فَلَهَا نِصْفُ مَا تَرَكَۚ وَهُوَ يَرِثُهَآ اِنْ لَّمْ يَكُنْ لَّهَا وَلَدٌ ۚ فَاِنْ كَانَتَا اثْنَتَيْنِ فَلَهُمَا الثُّلُثٰنِ مِمَّا تَ

yastaftuunaka quli **al**l*aa*hu yuftiikum fii **a**lkal*aa*lati ini imruun halaka laysa lahu waladun walahu ukhtun falah*aa* ni*sh*fu m*aa* taraka wahuwa yaritsuh*aa* in lam yakun lah*aa* walad

Mereka meminta fatwa kepadamu (tentang kalalah). Katakanlah: “Allah memberi fatwa kepadamu tentang kalalah (yaitu): jika seorang meninggal dunia, dan ia tidak mempunyai anak dan mempunyai saudara perempuan, maka bagi saudaranya yang perempuan itu seperdua dari harta yang ditinggalkannya, dan saudaranya yang laki-laki mempusakai (seluruh harta saudara perempuan), jika ia tidak mempunyai anak; tetapi jika saudara perempuan itu dua orang, maka bagi keduanya dua pertiga dari harta yang ditinggalkan oleh yang meninggal. Dan jika mereka (ahli waris itu terdiri dari) saudara-saudara laki dan perempuan, maka bahagian seorang saudara laki-laki sebanyak bahagian dua orang saudara perempuan. Allah menerangkan (hukum ini) kepadamu, supaya kamu tidak sesat. Dan Allah Maha Mengetahui segala sesuatu.

<!--EndFragment-->