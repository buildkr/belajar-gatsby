---
title: (1) Al-Fatihah - الفاتحة
date: 2021-10-26T08:20:03.020Z
ayat: 1
description: "Jumlah Ayat: 7 / Arti: Pembukaan"
---
<!--StartFragment-->

<br>1:1

# بِسْمِ اللّٰهِ الرَّحْمٰنِ الرَّحِيْمِ



<br><strong> bismi **al**l*aa*hi **al**rra*h*m*aa*ni **al**rra*h*iim**i** </strong>

Dengan nama Allah Yang Maha Pengasih, Maha Penyayang.

<br>



1:2

# اَلْحَمْدُ لِلّٰهِ رَبِّ الْعٰلَمِيْنَۙ

al*h*amdu lill*aa*hi rabbi **a**l'*aa*lamiin**a**

Segala puji bagi Allah, Tuhan seluruh alam,

<br>

1:3

# الرَّحْمٰنِ الرَّحِيْمِۙ

a**l**rra*h*m*aa*ni **al**rra*h*iim**i**

Yang Maha Pengasih, Maha Penyayang,

<br>

1:4

# مٰلِكِ يَوْمِ الدِّيْنِۗ

m*aa*liki yawmi **al**ddiin**i**

Pemilik hari pembalasan.

<br>

1:5

# اِيَّاكَ نَعْبُدُ وَاِيَّاكَ نَسْتَعِيْنُۗ

iyy*aa*ka na'budu wa-iyy*aa*ka nasta'iin**u**

Hanya kepada Engkaulah kami menyembah dan hanya kepada Engkaulah kami mohon pertolongan.

<br>

1:6

# اِهْدِنَا الصِّرَاطَ الْمُسْتَقِيْمَ ۙ

ihdin*aa* **al***shsh*ir*aath*a **a**lmustaqiim**a**

Tunjukilah kami jalan yang lurus,

<br>

1:7

# صِرَاطَ الَّذِيْنَ اَنْعَمْتَ عَلَيْهِمْ ەۙ غَيْرِ الْمَغْضُوْبِ عَلَيْهِمْ وَلَا الضَّاۤلِّيْنَ ࣖ

*sh*ir*aath*a **al**la*dz*iina an'amta 'alayhim ghayri **a**lmagh*dh*uubi 'alayhim wal*aa* **al***dhdhaa*lliin**a**

(yaitu) jalan orang-orang yang telah Engkau beri nikmat kepadanya; bukan (jalan) mereka yang dimurkai, dan bukan (pula jalan) mereka yang sesat.

<!--EndFragment-->