---
title: (109) Al-Kafirun - الكٰفرون
date: 2021-10-27T04:18:12.795Z
ayat: 109
description: "Jumlah Ayat: 6 / Arti: Orang-Orang kafir"
---
<!--StartFragment-->

109:1

# قُلْ يٰٓاَيُّهَا الْكٰفِرُوْنَۙ

qul y*aa* ayyuh*aa* **a**lk*aa*firuun**a**

Katakanlah (Muhammad), “Wahai orang-orang kafir!

109:2

# لَآ اَعْبُدُ مَا تَعْبُدُوْنَۙ

l*aa* a'budu m*aa* ta'buduun**a**

aku tidak akan menyembah apa yang kamu sembah,

109:3

# وَلَآ اَنْتُمْ عٰبِدُوْنَ مَآ اَعْبُدُۚ

wal*aa* antum '*aa*biduuna m*aa* a'bud**u**

dan kamu bukan penyembah apa yang aku sembah,

109:4

# وَلَآ اَنَا۠ عَابِدٌ مَّا عَبَدْتُّمْۙ

wal*aa* an*aa* '*aa*bidun m*aa* 'abadtum

dan aku tidak pernah menjadi penyembah apa yang kamu sembah,

109:5

# وَلَآ اَنْتُمْ عٰبِدُوْنَ مَآ اَعْبُدُۗ

wal*aa* antum '*aa*biduuna m*aa* a'bud**u**

dan kamu tidak pernah (pula) menjadi penyembah apa yang aku sembah.

109:6

# لَكُمْ دِيْنُكُمْ وَلِيَ دِيْنِ ࣖ

lakum diinukum waliya diin**i**

Untukmu agamamu, dan untukku agamaku.”

<!--EndFragment-->