---
title: (42) Asy-Syura - الشورى
date: 2021-10-27T04:03:12.141Z
ayat: 42
description: "Jumlah Ayat: 53 / Arti: Musyawarah"
---
<!--StartFragment-->

42:1

# حٰمۤ ۚ

*haa*-miim

Ha Mim

42:2

# عۤسۤقۤ ۗ

'ayn-siin-q*aa*f

Ain Sin Qaf

42:3

# كَذٰلِكَ يُوْحِيْٓ اِلَيْكَ وَاِلَى الَّذِيْنَ مِنْ قَبْلِكَۙ اللّٰهُ الْعَزِيْزُ الْحَكِيْمُ

ka*dzaa*lika yuu*h*ii ilayka wa-il*aa* **al**la*dz*iina min qablika **al**l*aa*hu **a**l'aziizu **a**l*h*akiim**u**

Demikianlah Allah Yang Mahaperkasa, Mahabijaksana mewahyukan kepadamu (Muhammad) dan kepada orang-orang yang sebelummu.

42:4

# لَهٗ مَا فِى السَّمٰوٰتِ وَمَا فِى الْاَرْضِۗ وَهُوَ الْعَلِيُّ الْعَظِيْمُ

lahu m*aa* fii **al**ssam*aa*w*aa*ti wam*aa* fii **a**l-ar*dh*i wahuwa **a**l'aliyyu **a**l'a*zh*iim**u**

Milik-Nyalah apa yang ada di langit dan apa yang ada di bumi. Dan Dialah Yang Mahaagung, Mahabesar.

42:5

# تَكَادُ السَّمٰوٰتُ يَتَفَطَّرْنَ مِنْ فَوْقِهِنَّ وَالْمَلٰۤىِٕكَةُ يُسَبِّحُوْنَ بِحَمْدِ رَبِّهِمْ وَيَسْتَغْفِرُوْنَ لِمَنْ فِى الْاَرْضِۗ اَلَآ اِنَّ اللّٰهَ هُوَ الْغَفُوْرُ الرَّحِيْمُ

tak*aa*du **al**ssam*aa*w*aa*tu yatafa*ththh*arna min fawqihinna wa**a**lmal*aa*-ikatu yusabbi*h*uuna bi*h*amdi rabbihim wayastaghfiruuna liman fii **a**l-ar*dh*i **a**

**Hampir saja langit itu pecah dari sebelah atasnya (karena kebesaran Allah) dan malaikat-malaikat bertasbih memuji Tuhannya dan memohonkan ampunan untuk orang yang ada di bumi. Ingatlah, sesungguhnya Allah Dialah Yang Maha Pengampun, Maha Penyayang.**









42:6

# وَالَّذِيْنَ اتَّخَذُوْا مِنْ دُوْنِهٖٓ اَوْلِيَاۤءَ اللّٰهُ حَفِيْظٌ عَلَيْهِمْۖ وَمَآ اَنْتَ عَلَيْهِمْ بِوَكِيْلٍ

wa**a**lla*dz*iina ittakha*dz*uu min duunihi awliy*aa*-a **al**l*aa*hu *h*afii*zh*un 'alayhim wam*aa* anta 'alayhim biwakiil**in**

Dan orang-orang yang mengambil pelindung-pelindung selain Allah, Allah mengawasi (perbuatan) mereka; adapun engkau (Muhammad) bukanlah orang yang diserahi mengawasi mereka.

42:7

# وَكَذٰلِكَ اَوْحَيْنَآ اِلَيْكَ قُرْاٰنًا عَرَبِيًّا لِّتُنْذِرَ اُمَّ الْقُرٰى وَمَنْ حَوْلَهَا وَتُنْذِرَ يَوْمَ الْجَمْعِ لَا رَيْبَ فِيْهِ ۗفَرِيْقٌ فِى الْجَنَّةِ وَفَرِيْقٌ فِى السَّعِيْرِ

waka*dzaa*lika aw*h*ayn*aa* ilayka qur-*aa*nan 'arabiyyan litun*dz*ira umma **a**lqur*aa* waman *h*awlah*aa* watun*dz*ira yawma **a**ljam'i l*aa* rayba fiihi fariiqun fii

Dan demikianlah Kami wahyukan Al-Qur'an kepadamu dalam bahasa Arab, agar engkau memberi peringatan kepada penduduk ibukota (Mekah) dan penduduk (negeri-negeri) di sekelilingnya serta memberi peringatan tentang hari berkumpul (Kiamat) yang tidak diragukan







42:8

# وَلَوْ شَاۤءَ اللّٰهُ لَجَعَلَهُمْ اُمَّةً وَّاحِدَةً وَّلٰكِنْ يُّدْخِلُ مَنْ يَّشَاۤءُ فِيْ رَحْمَتِهٖۗ وَالظّٰلِمُوْنَ مَا لَهُمْ مِّنْ وَّلِيٍّ وَّلَا نَصِيْرٍ

walaw sy*aa*-a **al**l*aa*hu laja'alahum ummatan w*aah*idatan wal*aa*kin yudkhilu man yasy*aa*u fii ra*h*matihi wa**al***zhzhaa*limuuna m*aa* lahum min waliyyin wal*aa* na*sh*iir<

Dan sekiranya Allah menghendaki niscaya Dia jadikan mereka satu umat, tetapi Dia memasukkan orang-orang yang Dia kehendaki ke dalam rahmat-Nya. Dan orang-orang yang zalim tidak ada bagi mereka pelindung dan penolong.

42:9

# اَمِ اتَّخَذُوْا مِنْ دُوْنِهٖٓ اَوْلِيَاۤءَۚ فَاللّٰهُ هُوَ الْوَلِيُّ وَهُوَ يُحْيِ الْمَوْتٰى ۖوَهُوَ عَلٰى كُلِّ شَيْءٍ قَدِيْرٌ ࣖ

ami ittakha*dz*uu min duunihi awliy*aa*-a fa**al**l*aa*hu huwa **a**lwaliyyu wahuwa yu*h*yii **a**lmawt*aa* wahuwa 'al*aa* kulli syay-in qadiir**un**

Atau mereka mengambil pelindung-pelindung selain Dia? Padahal Allah, Dialah pelindung (yang sebenarnya). Dan Dia menghidupkan orang yang mati, dan Dia Mahakuasa atas segala sesuatu.

42:10

# وَمَا اخْتَلَفْتُمْ فِيْهِ مِنْ شَيْءٍ فَحُكْمُهٗٓ اِلَى اللّٰهِ ۗذٰلِكُمُ اللّٰهُ رَبِّيْ عَلَيْهِ تَوَكَّلْتُۖ وَاِلَيْهِ اُنِيْبُ

wam*aa* ikhtalaftum fiihi min syay-in fa*h*ukmuhu il*aa* **al**l*aa*hi *dzaa*likumu **al**l*aa*hu rabbii 'alayhi tawakkaltu wa-ilayhi uniib**u**

Dan apa pun yang kamu perselisihkan padanya tentang sesuatu, keputusannya (terserah) kepada Allah. (Yang memiliki sifat-sifat demikian) itulah Allah Tuhanku. Kepada-Nya aku bertawakal dan kepada-Nya aku kembali.

42:11

# فَاطِرُ السَّمٰوٰتِ وَالْاَرْضِۗ جَعَلَ لَكُمْ مِّنْ اَنْفُسِكُمْ اَزْوَاجًا وَّمِنَ الْاَنْعَامِ اَزْوَاجًاۚ يَذْرَؤُكُمْ فِيْهِۗ لَيْسَ كَمِثْلِهٖ شَيْءٌ ۚوَهُوَ السَّمِيْعُ الْبَصِيْرُ

f*aath*iru **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i ja'ala lakum min anfusikum azw*aa*jan wamina **a**l-an'*aa*mi azw*aa*jan ya*dz*raukum fiihi laysa kamitslihi syay-un wahuwa

(Allah) Pencipta langit dan bumi. Dia menjadikan bagi kamu pasangan-pasangan dari jenis kamu sendiri, dan dari jenis hewan ternak pasangan-pasangan (juga). Dijadikan-Nya kamu berkembang biak dengan jalan itu. Tidak ada sesuatu pun yang serupa dengan Dia.

42:12

# لَهٗ مَقَالِيْدُ السَّمٰوٰتِ وَالْاَرْضِۚ يَبْسُطُ الرِّزْقَ لِمَنْ يَّشَاۤءُ وَيَقْدِرُ ۚاِنَّهٗ بِكُلِّ شَيْءٍ عَلِيْمٌ

lahu maq*aa*liidu **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i yabsu*th*u **al**rrizqa liman yasy*aa*u wayaqdiru innahu bikulli syay-in 'aliim**un**

Milik-Nyalah perbendaharaan langit dan bumi; Dia melapangkan rezeki dan membatasinya bagi siapa yang Dia kehendaki. Sungguh, Dia Maha Mengetahui segala sesuatu.

42:13

# ۞ شَرَعَ لَكُمْ مِّنَ الدِّيْنِ مَا وَصّٰى بِهٖ نُوْحًا وَّالَّذِيْٓ اَوْحَيْنَآ اِلَيْكَ وَمَا وَصَّيْنَا بِهٖٓ اِبْرٰهِيْمَ وَمُوْسٰى وَعِيْسٰٓى اَنْ اَقِيْمُوا الدِّيْنَ وَلَا تَتَفَرَّقُوْا فِيْهِۗ كَبُرَ عَلَى الْمُشْرِكِيْنَ مَا تَدْعُوْهُمْ اِلَي

syara'a lakum mina **al**ddiini m*aa* wa*shshaa* bihi nuu*h*an wa**a**lla*dz*ii aw*h*ayn*aa* ilayka wam*aa* wa*shsh*ayn*aa* bihi ibr*aa*hiima wamuus*aa* wa'iis*aa* an aq

diwasiatkan-Nya kepada Nuh dan apa yang telah Kami wahyukan kepadamu (Muhammad) dan apa yang telah Kami wasiatkan kepada Ibrahim, Musa dan Isa yaitu tegakkanlah agama (keimanan dan ketakwaan) dan janganlah kamu berpecah belah di dalamnya. Sangat berat bag

42:14

# وَمَا تَفَرَّقُوْٓا اِلَّا مِنْۢ بَعْدِ مَا جَاۤءَهُمُ الْعِلْمُ بَغْيًاۢ بَيْنَهُمْۗ وَلَوْلَا كَلِمَةٌ سَبَقَتْ مِنْ رَّبِّكَ اِلٰٓى اَجَلٍ مُّسَمًّى لَّقُضِيَ بَيْنَهُمْۗ وَاِنَّ الَّذِيْنَ اُوْرِثُوا الْكِتٰبَ مِنْۢ بَعْدِهِمْ لَفِيْ شَكٍّ مِّنْهُ مُر

wam*aa* tafarraquu ill*aa* min ba'di m*aa* j*aa*-ahumu **a**l'ilmu baghyan baynahum walawl*aa* kalimatun sabaqat min rabbika il*aa* ajalin musamman laqu*dh*iya baynahum wa-inna **al**la*dz*

Dan mereka (Ahli Kitab) tidak berpecah belah kecuali setelah datang kepada mereka ilmu (kebenaran yang disampaikan oleh para nabi) karena kedengkian antara sesama mereka. Jika tidaklah karena suatu ketetapan yang telah ada dahulunya dari Tuhanmu (untuk me

42:15

# فَلِذٰلِكَ فَادْعُ ۚوَاسْتَقِمْ كَمَآ اُمِرْتَۚ وَلَا تَتَّبِعْ اَهْوَاۤءَهُمْۚ وَقُلْ اٰمَنْتُ بِمَآ اَنْزَلَ اللّٰهُ مِنْ كِتٰبٍۚ وَاُمِرْتُ لِاَعْدِلَ بَيْنَكُمْ ۗ اَللّٰهُ رَبُّنَا وَرَبُّكُمْ ۗ لَنَآ اَعْمَالُنَا وَلَكُمْ اَعْمَالُكُمْ ۗ لَاحُجَّة

fali*dzaa*lika fa**u**d'u wa**i**staqim kam*aa* umirta wal*aa* tattabi' ahw*aa*-ahum waqul *aa*mantu bim*aa* anzala **al**l*aa*hu min kit*aa*bin waumirtu li-a'dila baynakum

Karena itu, serulah (mereka beriman) dan tetaplah (beriman dan berdakwah) sebagaimana diperintahkan kepadamu (Muhammad) dan janganlah mengikuti keinginan mereka dan katakanlah, “Aku beriman kepada Kitab yang diturunkan Allah dan aku diperintahkan agar ber

42:16

# وَالَّذِيْنَ يُحَاۤجُّوْنَ فِى اللّٰهِ مِنْۢ بَعْدِ مَا اسْتُجِيْبَ لَهٗ حُجَّتُهُمْ دَاحِضَةٌ عِنْدَ رَبِّهِمْ وَعَلَيْهِمْ غَضَبٌ وَّلَهُمْ عَذَابٌ شَدِيْدٌ

wa**a**lla*dz*iina yu*haa*jjuuna fii **al**l*aa*hi min ba'di m*aa* istujiiba lahu *h*ujjatuhum d*aah*i*dh*atun 'inda rabbihim wa'alayhim gha*dh*abun walahum 'a*dzaa*bun syadiid

Dan orang-orang yang berbantah-bantah tentang (agama) Allah setelah (agama itu) diterima, perbantahan mereka itu sia-sia di sisi Tuhan mereka. Mereka mendapat kemurkaan (Allah) dan mereka mendapat azab yang sangat keras.







42:17

# اَللّٰهُ الَّذِيْٓ اَنْزَلَ الْكِتٰبَ بِالْحَقِّ وَالْمِيْزَانَ ۗوَمَا يُدْرِيْكَ لَعَلَّ السَّاعَةَ قَرِيْبٌ

**al**l*aa*hu **al**la*dz*ii anzala **a**lkit*aa*ba bi**a**l*h*aqqi wa**a**lmiiz*aa*ni wam*aa* yudriika la'alla **al**ss*aa*'ata qariib

Allah yang menurunkan Kitab (Al-Qur'an) dengan (membawa) kebenaran dan neraca (keadilan). Dan tahukah kamu, boleh jadi hari Kiamat itu sudah dekat?

42:18

# يَسْتَعْجِلُ بِهَا الَّذِيْنَ لَا يُؤْمِنُوْنَ بِهَاۚ وَالَّذِيْنَ اٰمَنُوْا مُشْفِقُوْنَ مِنْهَاۙ وَيَعْلَمُوْنَ اَنَّهَا الْحَقُّ ۗ اَلَآ اِنَّ الَّذِيْنَ يُمَارُوْنَ فِى السَّاعَةِ لَفِيْ ضَلٰلٍۢ بَعِيْدٍ

yasta'jilu bih*aa* **al**la*dz*iina l*aa* yu/minuuna bih*aa* wa**a**lla*dz*iina *aa*manuu musyfiquuna minh*aa* waya'lamuuna annah*aa* **a**l*h*aqqu **a**l*a*

Orang-orang yang tidak percaya adanya hari Kiamat meminta agar hari itu segera terjadi, dan orang-orang yang beriman merasa takut kepadanya dan mereka yakin bahwa Kiamat itu adalah benar (akan terjadi). Ketahuilah bahwa sesungguhnya orang-orang yang memba







42:19

# اَللّٰهُ لَطِيْفٌۢ بِعِبَادِهٖ يَرْزُقُ مَنْ يَّشَاۤءُ ۚوَهُوَ الْقَوِيُّ الْعَزِيْزُ ࣖ

**al**l*aa*hu la*th*iifun bi'ib*aa*dihi yarzuqu man yasy*aa*u wahuwa **a**lqawiyyu **a**l'aziiz**u**

Allah Mahalembut terhadap hamba-hamba-Nya; Dia memberi rezeki kepada siapa yang Dia kehendaki dan Dia Mahakuat, Mahaperkasa.

42:20

# مَنْ كَانَ يُرِيْدُ حَرْثَ الْاٰخِرَةِ نَزِدْ لَهٗ فِيْ حَرْثِهٖۚ وَمَنْ كَانَ يُرِيْدُ حَرْثَ الدُّنْيَا نُؤْتِهٖ مِنْهَاۙ وَمَا لَهٗ فِى الْاٰخِرَةِ مِنْ نَّصِيْبٍ

man k*aa*na yuriidu *h*artsa **a**l-*aa*khirati nazid lahu fii *h*artsihi waman k*aa*na yuriidu *h*artsa **al**dduny*aa* nu/tihi minh*aa* wam*aa* lahu fii **a**l-*aa*

Barangsiapa menghendaki keuntungan di akhirat akan Kami tambahkan keuntungan itu baginya dan barangsiapa menghendaki keuntungan di dunia Kami berikan kepadanya sebagian darinya (keuntungan dunia), tetapi dia tidak akan mendapat bagian di akhirat.

42:21

# اَمْ لَهُمْ شُرَكٰۤؤُا شَرَعُوْا لَهُمْ مِّنَ الدِّيْنِ مَا لَمْ يَأْذَنْۢ بِهِ اللّٰهُ ۗوَلَوْلَا كَلِمَةُ الْفَصْلِ لَقُضِيَ بَيْنَهُمْ ۗوَاِنَّ الظّٰلِمِيْنَ لَهُمْ عَذَابٌ اَلِيْمٌ

am lahum syurak*aa*u syara'uu lahum mina **al**ddiini m*aa* lam ya/*dz*an bihi **al**l*aa*hu walawl*aa* kalimatu **a**lfa*sh*li laqu*dh*iya baynahum wa-inna **al***zh*

Apakah mereka mempunyai sesembahan selain Allah yang menetapkan aturan agama bagi mereka yang tidak diizinkan (diridai) Allah? Dan sekiranya tidak ada ketetapan yang menunda (hukuman dari Allah) tentulah hukuman di antara mereka telah dilaksanakan. Dan su







42:22

# تَرَى الظّٰلِمِيْنَ مُشْفِقِيْنَ مِمَّا كَسَبُوْا وَهُوَ وَاقِعٌۢ بِهِمْ ۗوَالَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِ فِيْ رَوْضٰتِ الْجَنّٰتِۚ لَهُمْ مَّا يَشَاۤءُوْنَ عِنْدَ رَبِّهِمْ ۗذٰلِكَ هُوَ الْفَضْلُ الْكَبِيْرُ

tar*aa* **al***zhzhaa*limiina musyfiqiina mimm*aa* kasabuu wahuwa w*aa*qi'un bihim wa**a**lla*dz*iina *aa*manuu wa'amiluu **al***shshaa*li*haa*ti fii raw*daa*ti **a<**

Kamu akan melihat orang-orang zalim itu sangat ketakutan karena (kejahatan-kejahatan) yang telah mereka lakukan, dan (azab) menimpa mereka. Dan orang-orang yang beriman dan mengerjakan kebajikan (berada) di dalam taman-taman surga, mereka memperoleh apa y







42:23

# ذٰلِكَ الَّذِيْ يُبَشِّرُ اللّٰهُ عِبَادَهُ الَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِۗ قُلْ لَّآ اَسْـَٔلُكُمْ عَلَيْهِ اَجْرًا اِلَّا الْمَوَدَّةَ فِى الْقُرْبٰىۗ وَمَنْ يَّقْتَرِفْ حَسَنَةً نَّزِدْ لَهٗ فِيْهَا حُسْنًا ۗاِنَّ اللّٰهَ غَفُوْرٌ شَكُوْر

*dzaa*lika **al**la*dz*ii yubasysyiru **al**l*aa*hu 'ib*aa*dahu **al**la*dz*iina *aa*manuu wa'amiluu **al***shshaa*li*haa*ti qul l*aa* as-alukum 'alayhi ajran

Itulah (karunia) yang diberitahukan Allah untuk menggembirakan hamba-hamba-Nya yang beriman dan mengerjakan kebajikan. Katakanlah (Muhammad), “Aku tidak meminta kepadamu sesuatu imbalan pun atas seruanku kecuali kasih sayang dalam kekeluargaan.” Dan baran

42:24

# اَمْ يَقُوْلُوْنَ افْتَرٰى عَلَى اللّٰهِ كَذِبًاۚ فَاِنْ يَّشَاِ اللّٰهُ يَخْتِمْ عَلٰى قَلْبِكَ ۗوَيَمْحُ اللّٰهُ الْبَاطِلَ وَيُحِقُّ الْحَقَّ بِكَلِمٰتِهٖ ۗاِنَّهٗ عَلِيْمٌ ۢبِذَاتِ الصُّدُوْرِ

am yaquuluuna iftar*aa* 'al*aa* **al**l*aa*hi ka*dz*iban fa-in yasya-i **al**l*aa*hu yakhtim 'al*aa* qalbika wayam*h*u **al**l*aa*hu **a**lb*aath*ila wayu*h*

Ataukah mereka mengatakan, “Dia (Muhammad) telah mengada-adakan kebohongan tentang Allah.” Sekiranya Allah menghendaki niscaya Dia kunci hatimu. Dan Allah menghapus yang batil dan membenarkan yang benar dengan firman-Nya (Al-Qur'an). Sungguh, Dia Maha Men







42:25

# وَهُوَ الَّذِيْ يَقْبَلُ التَّوْبَةَ عَنْ عِبَادِهٖ وَيَعْفُوْا عَنِ السَّيِّاٰتِ وَيَعْلَمُ مَا تَفْعَلُوْنَۙ

wahuwa **al**la*dz*ii yaqbalu **al**ttawbata 'an 'ib*aa*dihi waya'fuu 'ani **al**ssayyi-ati waya'lamu m*aa* taf'aluun**a**

Dan Dialah yang menerima tobat dari hamba-hamba-Nya dan memaafkan kesalahan-kesalahan dan mengetahui apa yang kamu kerjakan,

42:26

# وَيَسْتَجِيْبُ الَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِ وَيَزِيْدُهُمْ مِّنْ فَضْلِهٖ ۗوَالْكٰفِرُوْنَ لَهُمْ عَذَابٌ شَدِيْدٌ

wayastajiibu **al**la*dz*iina *aa*manuu wa'amiluu **al***shshaa*li*haa*ti wayaziiduhum min fa*dh*lihi wa**a**lk*aa*firuuna lahum 'a*dzaa*bun syadiid**un**

dan Dia memperkenankan (doa) orang-orang yang beriman dan mengerjakan kebajikan serta menambah (pahala) kepada mereka dari karunia-Nya. Orang-orang yang ingkar akan mendapat azab yang sangat keras.

42:27

# ۞ وَلَوْ بَسَطَ اللّٰهُ الرِّزْقَ لِعِبَادِهٖ لَبَغَوْا فِى الْاَرْضِ وَلٰكِنْ يُنَزِّلُ بِقَدَرٍ مَّا يَشَاۤءُ ۗاِنَّهٗ بِعِبَادِهٖ خَبِيْرٌۢ بَصِيْرٌ

walaw basa*th*a **al**l*aa*hu **al**rrizqa li'ib*aa*dihi labaghaw fii **a**l-ar*dh*i wal*aa*kin yunazzilu biqadarin m*aa* yasy*aa*u innahu bi'ib*aa*dihi khabiirun ba*sh*ii

Dan sekiranya Allah melapangkan rezeki kepada hamba-hamba-Nya niscaya mereka akan berbuat melampaui batas di bumi, tetapi Dia menurunkan dengan ukuran yang Dia kehendaki. Sungguh, Dia Mahateliti terhadap (keadaan) hamba-hamba-Nya, Maha Melihat.

42:28

# وَهُوَ الَّذِيْ يُنَزِّلُ الْغَيْثَ مِنْۢ بَعْدِ مَا قَنَطُوْا وَيَنْشُرُ رَحْمَتَهٗ ۗوَهُوَ الْوَلِيُّ الْحَمِيْدُ

wahuwa **al**la*dz*ii yunazzilu **a**lghaytsa min ba'di m*aa* qana*th*uu wayansyuru ra*h*matahu wahuwa **a**lwaliyyu **a**l*h*amiid**u**

Dan Dialah yang menurunkan hujan setelah mereka berputus asa dan menyebarkan rahmat-Nya. Dan Dialah Maha Pelindung, Maha Terpuji.

42:29

# وَمِنْ اٰيٰتِهٖ خَلْقُ السَّمٰوٰتِ وَالْاَرْضِ وَمَا بَثَّ فِيْهِمَا مِنْ دَاۤبَّةٍ ۗوَهُوَ عَلٰى جَمْعِهِمْ اِذَا يَشَاۤءُ قَدِيْرٌ ࣖ

wamin *aa*y*aa*tihi khalqu **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i wam*aa* batstsa fiihim*aa* min d*aa*bbatin wahuwa 'al*aa* jam'ihim i*dzaa* yasy*aa*u qadiir**un**

**Dan di antara tanda-tanda (kebesaran)-Nya adalah penciptaan langit dan bumi dan makhluk-makhluk yang melata yang Dia sebarkan pada keduanya. Dan Dia Mahakuasa mengumpulkan semuanya apabila Dia kehendaki.**









42:30

# وَمَآ اَصَابَكُمْ مِّنْ مُّصِيْبَةٍ فَبِمَا كَسَبَتْ اَيْدِيْكُمْ وَيَعْفُوْا عَنْ كَثِيْرٍۗ

wam*aa* a*shaa*bakum min mu*sh*iibatin fabim*aa* kasabat aydiikum waya'fuu 'an katsiir**in**

Dan musibah apa pun yang menimpa kamu adalah disebabkan oleh perbuatan tanganmu sendiri, dan Allah memaafkan banyak (dari kesalahan-kesalahanmu).

42:31

# وَمَآ اَنْتُمْ بِمُعْجِزِيْنَ فِى الْاَرْضِۚ وَمَا لَكُمْ مِّنْ دُوْنِ اللّٰهِ مِنْ وَّلِيٍّ وَّلَا نَصِيْرٍ

wam*aa* antum bimu'jiziina fii **a**l-ar*dh*i wam*aa* lakum min duuni **al**l*aa*hi min waliyyin wal*aa* na*sh*iir**in**

Dan kamu tidak dapat melepaskan diri (dari siksaan Allah) di bumi, dan kamu tidak memperoleh pelindung atau penolong selain Allah.

42:32

# وَمِنْ اٰيٰتِهِ الْجَوَارِ فِى الْبَحْرِ كَالْاَعْلَامِ ۗ

wamin *aa*y*aa*tihi **a**ljaw*aa*ri fii **a**lba*h*ri ka**a**l-a'l*aa*m**i**

Dan di antara tanda-tanda (kebesaran)-Nya ialah kapal-kapal (yang berlayar) di laut seperti gunung-gunung.

42:33

# اِنْ يَّشَأْ يُسْكِنِ الرِّيْحَ فَيَظْلَلْنَ رَوَاكِدَ عَلٰى ظَهْرِهٖۗ اِنَّ فِيْ ذٰلِكَ لَاٰيٰتٍ لِّكُلِّ صَبَّارٍ شَكُوْرٍۙ

in yasya/ yuskini **al**rrii*h*a faya*zh*lalna raw*aa*kida 'al*aa* *zh*ahrihi inna fii *dzaa*lika la*aa*y*aa*tin likulli *sh*abb*aa*rin syakuur**in**

Jika Dia menghendaki, Dia akan menghentikan angin, sehingga jadilah (kapal-kapal) itu terhenti di permukaan laut. Sungguh, pada yang demikian itu terdapat tanda-tanda (kekuasaan Allah) bagi orang yang selalu bersabar dan banyak bersyukur,

42:34

# اَوْ يُوْبِقْهُنَّ بِمَا كَسَبُوْا وَيَعْفُ عَنْ كَثِيْرٍۙ

aw yuubiqhunna bim*aa* kasabuu waya'fu 'an katsiir**in**

atau (Dia akan) menghancurkan kapal-kapal itu karena perbuatan (dosa) mereka, dan Dia memaafkan banyak (dari mereka),

42:35

# وَّيَعْلَمَ الَّذِيْنَ يُجَادِلُوْنَ فِيْٓ اٰيٰتِنَاۗ مَا لَهُمْ مِّنْ مَّحِيْصٍ

waya'lama **al**la*dz*iina yuj*aa*diluuna fii *aa*y*aa*tin*aa* m*aa* lahum min ma*h*ii*sh***in**

dan agar orang-orang yang membantah tanda-tanda (kekuasaan) Kami mengetahui bahwa mereka tidak akan memperoleh jalan keluar (dari siksaan).

42:36

# فَمَآ اُوْتِيْتُمْ مِّنْ شَيْءٍ فَمَتَاعُ الْحَيٰوةِ الدُّنْيَا ۚوَمَا عِنْدَ اللّٰهِ خَيْرٌ وَّاَبْقٰى لِلَّذِيْنَ اٰمَنُوْا وَعَلٰى رَبِّهِمْ يَتَوَكَّلُوْنَۚ

fam*aa* uutiitum min syay-in famat*aa*'u **a**l*h*ay*aa*ti **al**dduny*aa* wam*aa* 'inda **al**l*aa*hi khayrun wa-abq*aa* lilla*dz*iina *aa*manuu wa'al*aa* rabbi

Apa pun (kenikmatan) yang diberikan kepadamu, maka itu adalah kesenangan hidup di dunia. Sedangkan apa (kenikmatan) yang ada di sisi Allah lebih baik dan lebih kekal bagi orang-orang yang beriman, dan hanya kepada Tuhan mereka bertawakal,

42:37

# وَالَّذِيْنَ يَجْتَنِبُوْنَ كَبٰۤىِٕرَ الْاِثْمِ وَالْفَوَاحِشَ وَاِذَا مَا غَضِبُوْا هُمْ يَغْفِرُوْنَ ۚ

wa**a**lla*dz*iina yajtanibuuna kab*aa*-ira **a**l-itsmi wa**a**lfaw*aah*isya wa-i*dzaa* m*aa* gha*dh*ibuu hum yaghfiruun**a**

dan juga (bagi) orang-orang yang menjauhi dosa-dosa besar dan perbuatan-perbuatan keji, dan apabila mereka marah segera memberi maaf,

42:38

# وَالَّذِيْنَ اسْتَجَابُوْا لِرَبِّهِمْ وَاَقَامُوا الصَّلٰوةَۖ وَاَمْرُهُمْ شُوْرٰى بَيْنَهُمْۖ وَمِمَّا رَزَقْنٰهُمْ يُنْفِقُوْنَ ۚ

wa**a**lla*dz*iina istaj*aa*buu lirabbihim wa-aq*aa*muu **al***shsh*al*aa*ta wa-amruhum syuur*aa* baynahum wamimm*aa* razaqn*aa*hum yunfiquun**a**

dan (bagi) orang-orang yang menerima (mematuhi) seruan Tuhan dan melaksanakan salat, sedang urusan mereka (diputuskan) dengan musyawarah antara mereka; dan mereka menginfakkan sebagian dari rezeki yang Kami berikan kepada mereka,

42:39

# وَالَّذِيْنَ اِذَآ اَصَابَهُمُ الْبَغْيُ هُمْ يَنْتَصِرُوْنَ

wa**a**lla*dz*iina i*dzaa* a*shaa*bahumu **a**lbaghyu hum yanta*sh*iruun**a**

dan (bagi) orang-orang yang apabila mereka diperlakukan dengan zalim, mereka membela diri.

42:40

# وَجَزٰۤؤُا سَيِّئَةٍ سَيِّئَةٌ مِّثْلُهَا ۚفَمَنْ عَفَا وَاَصْلَحَ فَاَجْرُهٗ عَلَى اللّٰهِ ۗاِنَّهٗ لَا يُحِبُّ الظّٰلِمِيْنَ

wajaz*aa*u sayyi-atin sayyi-atun mitsluh*aa* faman 'af*aa* wa-a*sh*la*h*a fa-ajruhu 'al*aa* **al**l*aa*hi innahu l*aa* yu*h*ibbu **al***zhzhaa*limiin**a**

Dan balasan suatu kejahatan adalah kejahatan yang setimpal, tetapi barangsiapa memaafkan dan berbuat baik (kepada orang yang berbuat jahat) maka pahalanya dari Allah. Sungguh, Dia tidak menyukai orang-orang zalim.

42:41

# وَلَمَنِ انْتَصَرَ بَعْدَ ظُلْمِهٖ فَاُولٰۤىِٕكَ مَا عَلَيْهِمْ مِّنْ سَبِيْلٍۗ

walamani inta*sh*ara ba'da *zh*ulmihi faul*aa*-ika m*aa* 'alayhim min sabiil**in**

Tetapi orang-orang yang membela diri setelah dizalimi, tidak ada alasan untuk menyalahkan mereka.

42:42

# اِنَّمَا السَّبِيْلُ عَلَى الَّذِيْنَ يَظْلِمُوْنَ النَّاسَ وَيَبْغُوْنَ فِى الْاَرْضِ بِغَيْرِ الْحَقِّۗ اُولٰۤىِٕكَ لَهُمْ عَذَابٌ اَلِيْمٌ

innam*aa* **al**ssabiilu 'al*aa* **al**la*dz*iina ya*zh*limuuna **al**nn*aa*sa wayabghuuna fii **a**l-ar*dh*i bighayri **a**l*h*aqqi ul*aa*-ika lahum

Sesungguhnya kesalahan hanya ada pada orang-orang yang berbuat zalim kepada manusia dan melampaui batas di bumi tanpa (mengindahkan) kebenaran. Mereka itu mendapat siksa yang pedih.

42:43

# وَلَمَنْ صَبَرَ وَغَفَرَ اِنَّ ذٰلِكَ لَمِنْ عَزْمِ الْاُمُوْرِ ࣖ

walaman *sh*abara waghafara inna *dzaa*lika lamin 'azmi **a**l-umuur**i**

Tetapi barangsiapa bersabar dan memaafkan, sungguh yang demikian itu termasuk perbuatan yang mulia.

42:44

# وَمَنْ يُّضْلِلِ اللّٰهُ فَمَا لَهٗ مِنْ وَّلِيٍّ مِّنْۢ بَعْدِهٖ ۗوَتَرَى الظّٰلِمِيْنَ لَمَّا رَاَوُا الْعَذَابَ يَقُوْلُوْنَ هَلْ اِلٰى مَرَدٍّ مِّنْ سَبِيْلٍۚ

waman yu*dh*lili **al**l*aa*hu fam*aa* lahu min waliyyin min ba'dihi watar*aa* **al***zhzhaa*limiina lamm*aa* ra-awuu **a**l'a*dzaa*ba yaquuluuna hal il*aa* maraddin min sabiil

Dan barangsiapa dibiarkan sesat oleh Allah, maka tidak ada baginya pelindung setelah itu. Kamu akan melihat orang-orang zalim ketika mereka melihat azab berkata, “Adakah kiranya jalan untuk kembali (ke dunia)?”

42:45

# وَتَرٰىهُمْ يُعْرَضُوْنَ عَلَيْهَا خٰشِعِيْنَ مِنَ الذُّلِّ يَنْظُرُوْنَ مِنْ طَرْفٍ خَفِيٍّۗ وَقَالَ الَّذِيْنَ اٰمَنُوْٓا اِنَّ الْخٰسِرِيْنَ الَّذِيْنَ خَسِرُوْٓا اَنْفُسَهُمْ وَاَهْلِيْهِمْ يَوْمَ الْقِيٰمَةِ ۗ اَلَآ اِنَّ الظّٰلِمِيْنَ فِيْ عَذَابٍ

watar*aa*hum yu'ra*dh*uuna 'alayh*aa* kh*aa*syi'iina mina **al***dzdz*ulli yan*zh*uruuna min *th*arfin khafiyyin waq*aa*la **al**la*dz*iina *aa*manuu inna **a**lkh

Dan kamu akan melihat mereka dihadapkan ke neraka dalam keadaan tertunduk karena (merasa) hina, mereka melihat dengan pandangan yang lesu. Dan orang-orang yang beriman berkata, “Sesungguhnya orang-orang yang rugi ialah orang-orang yang merugikan diri mere







42:46

# وَمَا كَانَ لَهُمْ مِّنْ اَوْلِيَاۤءَ يَنْصُرُوْنَهُمْ مِّنْ دُوْنِ اللّٰهِ ۗوَمَنْ يُّضْلِلِ اللّٰهُ فَمَا لَهٗ مِنْ سَبِيْلٍ ۗ

wam*aa* k*aa*na lahum min awliy*aa*-a yan*sh*uruunahum min duuni **al**l*aa*hi waman yu*dh*lili **al**l*aa*hu fam*aa* lahu min sabiil**in**

Dan mereka tidak akan mempunyai pelindung yang dapat menolong mereka selain Allah. Barangsiapa dibiarkan sesat oleh Allah tidak akan ada jalan keluar baginya (untuk mendapat petunjuk).

42:47

# اِسْتَجِيْبُوْا لِرَبِّكُمْ مِّنْ قَبْلِ اَنْ يَّأْتِيَ يَوْمٌ لَّا مَرَدَّ لَهٗ مِنَ اللّٰهِ ۗمَا لَكُمْ مِّنْ مَّلْجَاٍ يَّوْمَىِٕذٍ وَّمَا لَكُمْ مِّنْ نَّكِيْرٍ

istajiibuu lirabbikum min qabli an ya/tiya yawmun l*aa* maradda lahu mina **al**l*aa*hi m*aa* lakum min malja-in yawma-i*dz*in wam*aa* lakum min nakiir**in**

Patuhilah seruan Tuhanmu sebelum datang dari Allah suatu hari yang tidak dapat ditolak (atas perintah dari Allah). Pada hari itu kamu tidak memperoleh tempat berlindung dan tidak (pula) dapat mengingkari (dosa-dosamu).

42:48

# فَاِنْ اَعْرَضُوْا فَمَآ اَرْسَلْنٰكَ عَلَيْهِمْ حَفِيْظًا ۗاِنْ عَلَيْكَ اِلَّا الْبَلٰغُ ۗوَاِنَّآ اِذَآ اَذَقْنَا الْاِنْسَانَ مِنَّا رَحْمَةً فَرِحَ بِهَا ۚوَاِنْ تُصِبْهُمْ سَيِّئَةٌ ۢبِمَا قَدَّمَتْ اَيْدِيْهِمْ فَاِنَّ الْاِنْسَانَ كَفُوْرٌ

fa-in a'ra*dh*uu fam*aa* arsaln*aa*ka 'alayhim *h*afii*zh*an in 'alayka ill*aa* **a**lbal*aa*ghu wa-inn*aa* i*dzaa* a*dz*aqn*aa* **a**l-ins*aa*na minn*aa* ra*h*

*Jika mereka berpaling, maka (ingatlah) Kami tidak mengutus engkau sebagai pengawas bagi mereka. Kewajibanmu tidak lain hanyalah menyampaikan (risalah). Dan sungguh, apabila Kami merasakan kepada manusia suatu rahmat dari Kami, dia menyambutnya dengan gemb*









42:49

# لِلّٰهِ مُلْكُ السَّمٰوٰتِ وَالْاَرْضِۗ يَخْلُقُ مَا يَشَاۤءُ ۗيَهَبُ لِمَنْ يَّشَاۤءُ اِنَاثًا وَّيَهَبُ لِمَنْ يَّشَاۤءُ الذُّكُوْرَ ۙ

lill*aa*hi mulku **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i yakhluqu m*aa* yasy*aa*u yahabu liman yasy*aa*u in*aa*tsan wayahabu liman yasy*aa*u **al***dzdz*ukuur**a**

Milik Allah-lah kerajaan langit dan bumi; Dia menciptakan apa yang Dia kehendaki, memberikan anak perempuan kepada siapa yang Dia kehendaki dan memberikan anak laki-laki kepada siapa yang Dia kehendaki,







42:50

# اَوْ يُزَوِّجُهُمْ ذُكْرَانًا وَّاِنَاثًا ۚوَيَجْعَلُ مَنْ يَّشَاۤءُ عَقِيْمًا ۗاِنَّهٗ عَلِيْمٌ قَدِيْرٌ

aw yuzawwijuhum *dz*ukr*aa*nan wa-in*aa*tsan wayaj'alu man yasy*aa*u 'aqiiman innahu 'aliimun qadiir**un**

atau Dia menganugerahkan jenis laki-laki dan perempuan, dan menjadikan mandul siapa yang Dia kehendaki. Dia Maha Mengetahui, Mahakuasa.

42:51

# ۞ وَمَا كَانَ لِبَشَرٍ اَنْ يُّكَلِّمَهُ اللّٰهُ اِلَّا وَحْيًا اَوْ مِنْ وَّرَاۤئِ حِجَابٍ اَوْ يُرْسِلَ رَسُوْلًا فَيُوْحِيَ بِاِذْنِهٖ مَا يَشَاۤءُ ۗاِنَّهٗ عَلِيٌّ حَكِيْمٌ

wam*aa* k*aa*na libasyarin an yukallimahu **al**l*aa*hu ill*aa* wa*h*yan aw min war*aa*-i *h*ij*aa*bin aw yursila rasuulan fayuu*h*iya bi-i*dz*nihi m*aa* yasy*aa*u innahu 'aliyyun

Dan tidaklah patut bagi seorang manusia bahwa Allah akan berbicara kepadanya kecuali dengan perantaraan wahyu atau dari belakang tabir atau dengan mengutus utusan (malaikat) lalu diwahyukan kepadanya dengan izin-Nya apa yang Dia kehendaki. Sungguh, Dia Ma

42:52

# وَكَذٰلِكَ اَوْحَيْنَآ اِلَيْكَ رُوْحًا مِّنْ اَمْرِنَا ۗمَا كُنْتَ تَدْرِيْ مَا الْكِتٰبُ وَلَا الْاِيْمَانُ وَلٰكِنْ جَعَلْنٰهُ نُوْرًا نَّهْدِيْ بِهٖ مَنْ نَّشَاۤءُ مِنْ عِبَادِنَا ۗوَاِنَّكَ لَتَهْدِيْٓ اِلٰى صِرَاطٍ مُّسْتَقِيْمٍۙ

waka*dzaa*lika aw*h*ayn*aa* ilayka ruu*h*an min amrin*aa* m*aa* kunta tadrii m*aa* **a**lkit*aa*bu wal*aa* **a**l-iim*aa*nu wal*aa*kin ja'aln*aa*hu nuuran nahdii bihi ma

Dan demikianlah Kami wahyukan kepadamu (Muhammad) ruh (Al-Qur'an) dengan perintah Kami. Sebelumnya engkau tidaklah mengetahui apakah Kitab (Al-Qur'an) dan apakah iman itu, tetapi Kami jadikan Al-Qur'an itu cahaya, dengan itu Kami memberi petunjuk siapa y

42:53

# صِرَاطِ اللّٰهِ الَّذِيْ لَهٗ مَا فِى السَّمٰوٰتِ وَمَا فِى الْاَرْضِۗ اَلَآ اِلَى اللّٰهِ تَصِيْرُ الْاُمُوْرُ ࣖ

*sh*ir*aath*i **al**l*aa*hi **al**la*dz*ii lahu m*aa* fii **al**ssam*aa*w*aa*ti wam*aa* fii **a**l-ar*dh*i **a**l*aa* il*aa* **al**

(Yaitu) jalan Allah yang milik-Nyalah apa yang ada di langit dan apa yang ada di bumi. Ingatlah, segala urusan kembali kepada Allah.

<!--EndFragment-->