---
title: (10) Yunus - يونس
date: 2021-10-27T03:28:56.599Z
ayat: 10
description: "Jumlah Ayat: 109 / Arti: Yunus"
---
<!--StartFragment-->

10:1

# الۤرٰ ۗتِلْكَ اٰيٰتُ الْكِتٰبِ الْحَكِيْمِ

alif-l*aa*m-r*aa* tilka *aa*y*aa*tu **a**lkit*aa*bi **a**l*h*akiim**i**

Alif Lam Ra. Inilah ayat-ayat Al-Qur'an yang penuh hikmah.

10:2

# اَكَانَ لِلنَّاسِ عَجَبًا اَنْ اَوْحَيْنَآ اِلٰى رَجُلٍ مِّنْهُمْ اَنْ اَنْذِرِ النَّاسَ وَبَشِّرِ الَّذِيْنَ اٰمَنُوْٓا اَنَّ لَهُمْ قَدَمَ صِدْقٍ عِنْدَ رَبِّهِمْ ۗ قَالَ الْكٰفِرُوْنَ اِنَّ هٰذَا لَسٰحِرٌ مُّبِيْنٌ

ak*aa*na li**l**nn*aa*si 'ajaban an aw*h*ayn*aa* il*aa* rajulin minhum an an*dz*iri **al**nn*aa*sa wabasysyiri **al**la*dz*iina *aa*manuu anna lahum qadama *sh*idqin

Pantaskah manusia menjadi heran bahwa Kami memberi wahyu kepada seorang laki-laki di antara mereka, “Berilah peringatan kepada manusia dan gembirakanlah orang-orang beriman bahwa mereka mempunyai kedudukan yang tinggi di sisi Tuhan.” Orang-orang kafir ber

10:3

# اِنَّ رَبَّكُمُ اللّٰهُ الَّذِيْ خَلَقَ السَّمٰوٰتِ وَالْاَرْضَ فِيْ سِتَّةِ اَيَّامٍ ثُمَّ اسْتَوٰى عَلَى الْعَرْشِ يُدَبِّرُ الْاَمْرَۗ مَا مِنْ شَفِيْعٍ اِلَّا مِنْۢ بَعْدِ اِذْنِهٖۗ ذٰلِكُمُ اللّٰهُ رَبُّكُمْ فَاعْبُدُوْهُۗ اَفَلَا تَذَكَّرُوْنَ

inna rabbakumu **al**l*aa*hu **al**la*dz*ii khalaqa **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*a fii sittati ayy*aa*min tsumma istaw*aa* 'al*aa* **a**l'arsy

Sesungguhnya Tuhan kamu Dialah Allah yang menciptakan langit dan bumi dalam enam masa, kemudian Dia bersemayam di atas ‘Arsy (singgasana) untuk mengatur segala urusan. Tidak ada yang dapat memberi syafaat kecuali setelah ada izin-Nya. Itulah Allah, Tuhanm

10:4

# اِلَيْهِ مَرْجِعُكُمْ جَمِيْعًاۗ وَعْدَ اللّٰهِ حَقًّاۗ اِنَّهٗ يَبْدَؤُا الْخَلْقَ ثُمَّ يُعِيْدُهٗ لِيَجْزِيَ الَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِ بِالْقِسْطِۗ وَالَّذِيْنَ كَفَرُوْا لَهُمْ شَرَابٌ مِّنْ حَمِيْمٍ وَّعَذَابٌ اَلِيْمٌ ۢبِمَا كَانُو

ilayhi marji'ukum jamii'an wa'da **al**l*aa*hi *h*aqqan innahu yabdau **a**lkhalqa tsumma yu'iiduhu liyajziya **al**la*dz*iina *aa*manuu wa'amiluu **al***shshaa*li*haa*ti bi

Hanya kepada-Nya kamu semua akan kembali. Itu merupakan janji Allah yang benar dan pasti. Sesungguhnya Dialah yang memulai penciptaan makhluk kemudian mengulanginya (menghidupkannya kembali setelah berbangkit), agar Dia memberi balasan kepada orang-orang

10:5

# هُوَ الَّذِيْ جَعَلَ الشَّمْسَ ضِيَاۤءً وَّالْقَمَرَ نُوْرًا وَّقَدَّرَهٗ مَنَازِلَ لِتَعْلَمُوْا عَدَدَ السِّنِيْنَ وَالْحِسَابَۗ مَا خَلَقَ اللّٰهُ ذٰلِكَ اِلَّا بِالْحَقِّۗ يُفَصِّلُ الْاٰيٰتِ لِقَوْمٍ يَّعْلَمُوْنَ

huwa **al**la*dz*ii ja'ala **al**sysyamsa *dh*iy*aa*-an wa**a**lqamara nuuran waqaddarahu man*aa*zila lita'lamuu 'adada **al**ssiniina wa**a**l*h*is*aa*ba m*a*

Dialah yang menjadikan matahari bersinar dan bulan bercahaya, dan Dialah yang menetapkan tempat-tempat orbitnya, agar kamu mengetahui bilangan tahun, dan perhitungan (waktu). Allah tidak menciptakan demikian itu melainkan dengan benar. Dia menjelaskan tan

10:6

# اِنَّ فِى اخْتِلَافِ الَّيْلِ وَالنَّهَارِ وَمَا خَلَقَ اللّٰهُ فِى السَّمٰوٰتِ وَالْاَرْضِ لَاٰيٰتٍ لِّقَوْمٍ يَّتَّقُوْنَ

inna fii ikhtil*aa*fi **al**layli wa**al**nnah*aa*ri wam*aa* khalaqa **al**l*aa*hu fii **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i la*aa*y*aa*tin liqa

Sesungguhnya pada pergantian malam dan siang dan pada apa yang diciptakan Allah di langit dan di bumi, pasti terdapat tanda-tanda (kebesaran-Nya) bagi orang-orang yang bertakwa.

10:7

# اِنَّ الَّذِيْنَ لَا يَرْجُوْنَ لِقَاۤءَنَا وَرَضُوْا بِالْحَيٰوةِ الدُّنْيَا وَاطْمَـَٔنُّوْا بِهَا وَالَّذِيْنَ هُمْ عَنْ اٰيٰتِنَا غٰفِلُوْنَۙ

inna **al**la*dz*iina l*aa* yarjuuna liq*aa*-an*aa* wara*dh*uu bi**a**l*h*ay*aa*ti **al**dduny*aa* wa**i***th*ma-annuu bih*aa* wa**a**lla

Sesungguhnya orang-orang yang tidak mengharapkan (tidak percaya akan) pertemuan dengan Kami, dan merasa puas dengan kehidupan dunia serta merasa tenteram dengan (kehidupan) itu, dan orang-orang yang melalaikan ayat-ayat Kami,

10:8

# اُولٰۤىِٕكَ مَأْوٰىهُمُ النَّارُ بِمَا كَانُوْا يَكْسِبُوْنَ

ul*aa*-ika ma/w*aa*humu **al**nn*aa*ru bim*aa* k*aa*nuu yaksibuun**a**

mereka itu tempatnya di neraka, karena apa yang telah mereka lakukan.

10:9

# اِنَّ الَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِ يَهْدِيْهِمْ رَبُّهُمْ بِاِيْمَانِهِمْۚ تَجْرِيْ مِنْ تَحْتِهِمُ الْاَنْهٰرُ فِيْ جَنّٰتِ النَّعِيْمِ

inna **al**la*dz*iina *aa*manuu wa'amiluu **al***shshaa*li*haa*ti yahdiihim rabbuhum bi-iim*aa*nihim tajrii min ta*h*tihimu **a**l-anh*aa*ru fii jann*aa*ti **al**n

Sesungguhnya orang-orang yang beriman dan mengerjakan kebajikan, niscaya diberi petunjuk oleh Tuhan karena keimanannya. Mereka di dalam surga yang penuh kenikmatan, mengalir di bawahnya sungai-sungai.

10:10

# دَعْوٰىهُمْ فِيْهَا سُبْحٰنَكَ اللهم وَتَحِيَّتُهُمْ فِيْهَا سَلٰمٌۚ وَاٰخِرُ دَعْوٰىهُمْ اَنِ الْحَمْدُ لِلّٰهِ رَبِّ الْعٰلَمِيْنَ ࣖ

da'w*aa*hum fiih*aa* sub*haa*naka **al**l*aa*humma wata*h*iyyatuhum fiih*aa* sal*aa*mun wa*aa*khiru da'w*aa*hum ani **a**l*h*amdu lill*aa*hi rabbi **a**l'*aa*

*Doa mereka di dalamnya ialah, “Subhanakallahumma” (Mahasuci Engkau, ya Tuhan kami), dan salam penghormatan mereka ialah, “Salam” (salam sejahtera). Dan penutup doa mereka ialah, “Al-hamdu lillahi Rabbil ‘alamin” (segala puji bagi Allah Tuhan seluruh alam)*

10:11

# ۞ وَلَوْ يُعَجِّلُ اللّٰهُ لِلنَّاسِ الشَّرَّ اسْتِعْجَالَهُمْ بِالْخَيْرِ لَقُضِيَ اِلَيْهِمْ اَجَلُهُمْۗ فَنَذَرُ الَّذِيْنَ لَا يَرْجُوْنَ لِقَاۤءَنَا فِيْ طُغْيَانِهِمْ يَعْمَهُوْنَ

walaw yu'ajjilu **al**l*aa*hu li**l**nn*aa*si **al**sysyarra isti'j*aa*lahum bi**a**lkhayri laqu*dh*iya ilayhim ajaluhum fana*dz*aru **al**la*dz*iina l*aa*

Dan kalau Allah menyegerakan keburukan bagi manusia seperti permintaan mereka untuk menyegerakan kebaikan, pasti diakhiri umur mereka. Namun Kami biarkan orang-orang yang tidak mengharapkan pertemuan dengan Kami, bingung di dalam kesesatan mereka.

10:12

# وَاِذَا مَسَّ الْاِنْسَانَ الضُّرُّ دَعَانَا لِجَنْۢبِهٖٓ اَوْ قَاعِدًا اَوْ قَاۤىِٕمًا ۚفَلَمَّا كَشَفْنَا عَنْهُ ضُرَّهٗ مَرَّ كَاَنْ لَّمْ يَدْعُنَآ اِلٰى ضُرٍّ مَّسَّهٗۗ كَذٰلِكَ زُيِّنَ لِلْمُسْرِفِيْنَ مَا كَانُوْا يَعْمَلُوْنَ

wa-i*dzaa* massa **a**l-ins*aa*na **al***dhdh*urru da'*aa*n*aa *lijanbihi aw q*aa*'idan aw q*aa*\-iman falamm*aa *kasyafn*aa *'anhu* dh*urrahu marra ka-an lam yad'un*aa* il

Dan apabila manusia ditimpa bahaya dia berdoa kepada Kami dalam keadaan berbaring, duduk atau berdiri, tetapi setelah Kami hilangkan bahaya itu darinya, dia kembali (ke jalan yang sesat), seolah-olah dia tidak pernah berdoa kepada Kami untuk (menghilangka

10:13

# وَلَقَدْ اَهْلَكْنَا الْقُرُوْنَ مِنْ قَبْلِكُمْ لَمَّا ظَلَمُوْاۙ وَجَاۤءَتْهُمْ رُسُلُهُمْ بِالْبَيِّنٰتِ وَمَا كَانُوْا لِيُؤْمِنُوْا ۗ كَذٰلِكَ نَجْزِى الْقَوْمَ الْمُجْرِمِيْنَ

walaqad ahlakn*aa* **a**lquruuna min qablikum lamm*aa* *zh*alamuu waj*aa*-at-hum rusuluhum bi**a**lbayyin*aa*ti wam*aa* k*aa*nuu liyu/minuu ka*dzaa*lika najzii **a**lqawma

Dan sungguh, Kami telah membinasakan umat-umat sebelum kamu, ketika mereka berbuat zalim, padahal para rasul mereka telah datang membawa keterangan-keterangan (yang nyata), tetapi mereka sama sekali tidak mau beriman. Demikianlah Kami memberi balasan kep

10:14

# ثُمَّ جَعَلْنٰكُمْ خَلٰۤىِٕفَ فِى الْاَرْضِ مِنْۢ بَعْدِهِمْ لِنَنْظُرَ كَيْفَ تَعْمَلُوْنَ

tsumma ja'aln*aa*kum khal*aa*-ifa fii **a**l-ar*dh*i min ba'dihim linan*zh*ura kayfa ta'maluun**a**

Kemudian Kami jadikan kamu sebagai pengganti-pengganti (mereka) di bumi setelah mereka, untuk Kami lihat bagaimana kamu berbuat.

10:15

# وَاِذَا تُتْلٰى عَلَيْهِمْ اٰيَاتُنَا بَيِّنٰتٍۙ قَالَ الَّذِيْنَ لَا يَرْجُوْنَ لِقَاۤءَنَا ائْتِ بِقُرْاٰنٍ غَيْرِ هٰذَآ اَوْ بَدِّلْهُ ۗ قُلْ مَا يَكُوْنُ لِيْٓ اَنْ اُبَدِّلَهٗ مِنْ تِلْقَاۤئِ نَفْسِيْ ۚاِنْ اَتَّبِعُ اِلَّا مَا يُوْحٰٓى اِلَيَّ ۚ اِ

wa-i*dzaa* tutl*aa* 'alayhim *aa*y*aa*tun*aa* bayyin*aa*tin q*aa*la **al**la*dz*iina l*aa* yarjuuna liq*aa*-an*aa* i/ti biqur-*aa*nin ghayri h*aadzaa* aw baddilhu qul m*aa*

Dan apabila dibacakan kepada mereka ayat-ayat Kami dengan jelas, orang-orang yang tidak mengharapkan pertemuan dengan Kami berkata, “Datangkanlah kitab selain Al-Qur'an ini atau gantilah.” Katakanlah (Muhammad), “Tidaklah pantas bagiku menggantinya atas k

10:16

# قُلْ لَّوْ شَاۤءَ اللّٰهُ مَا تَلَوْتُهٗ عَلَيْكُمْ وَلَآ اَدْرٰىكُمْ بِهٖ ۖفَقَدْ لَبِثْتُ فِيْكُمْ عُمُرًا مِّنْ قَبْلِهٖۗ اَفَلَا تَعْقِلُوْنَ

qul law sy*aa*-a **al**l*aa*hu m*aa* talawtuhu 'alaykum wal*aa* adr*aa*kum bihi faqad labitstu fiikum 'umuran min qablihi afal*aa* ta'qiluun**a**

Katakanlah (Muhammad), “Jika Allah menghendaki, niscaya aku tidak membacakannya kepadamu dan Allah tidak (pula) memberitahukannya kepadamu.” Aku telah tinggal bersamamu beberapa lama sebelumnya (sebelum turun Al-Qur'an). Apakah kamu tidak mengerti?

10:17

# فَمَنْ اَظْلَمُ مِمَّنِ افْتَرٰى عَلَى اللّٰهِ كَذِبًا اَوْ كَذَّبَ بِاٰيٰتِهٖۗ اِنَّهٗ لَا يُفْلِحُ الْمُجْرِمُوْنَ

faman a*zh*lamu mimmani iftar*aa* 'al*aa* **al**l*aa*hi ka*dz*iban aw ka*dzdz*aba bi-*aa*y*aa*tihi innahu l*aa* yufli*h*u **a**lmujrimuun**a**

Maka siapakah yang lebih zalim daripada orang yang mengada-adakan kebohongan terhadap Allah atau mendustakan ayat-ayat-Nya? Sesungguhnya orang-orang yang berbuat dosa itu tidak akan beruntung.

10:18

# وَيَعْبُدُوْنَ مِنْ دُوْنِ اللّٰهِ مَا لَا يَضُرُّهُمْ وَلَا يَنْفَعُهُمْ وَيَقُوْلُوْنَ هٰٓؤُلَاۤءِ شُفَعَاۤؤُنَا عِنْدَ اللّٰهِ ۗقُلْ اَتُنَبِّـُٔوْنَ اللّٰهَ بِمَا لَا يَعْلَمُ فِى السَّمٰوٰتِ وَلَا فِى الْاَرْضِۗ سُبْحٰنَهٗ وَتَعٰلٰى عَمَّا يُشْرِكُوْ

waya'buduuna min duuni **al**l*aa*hi m*aa* l*aa* ya*dh*urruhum wal*aa* yanfa'uhum wayaquuluuna h*aa*ul*aa*-i syufa'*aa*un*aa* 'inda **al**l*aa*hi qul atunabbi-uuna **al**

**Dan mereka menyembah selain Allah, sesuatu yang tidak dapat mendatangkan bencana kepada mereka dan tidak (pula) memberi manfaat, dan mereka berkata, “Mereka itu adalah pemberi syafaat kami di hadapan Allah.” Katakanlah, “Apakah kamu akan memberitahu kepad**

10:19

# وَمَا كَانَ النَّاسُ اِلَّآ اُمَّةً وَّاحِدَةً فَاخْتَلَفُوْاۗ وَلَوْلَا كَلِمَةٌ سَبَقَتْ مِنْ رَّبِّكَ لَقُضِيَ بَيْنَهُمْ فِيْمَا فِيْهِ يَخْتَلِفُوْنَ

wam*aa* k*aa*na **al**nn*aa*su ill*aa* ummatan w*aah*idatan fa**i**khtalafuu walawl*aa* kalimatun sabaqat min rabbika laqu*dh*iya baynahum fiim*aa* fiihi yakhtalifuun**a**

Dan manusia itu dahulunya hanyalah satu umat, kemudian mereka berselisih. Kalau tidak karena suatu ketetapan yang telah ada dari Tuhanmu, pastilah telah diberi keputusan (di dunia) di antara mereka, tentang apa yang mereka perselisihkan itu.

10:20

# وَيَقُوْلُوْنَ لَوْلَآ اُنْزِلَ عَلَيْهِ اٰيَةٌ مِّنْ رَّبِّهٖۚ فَقُلْ اِنَّمَا الْغَيْبُ لِلّٰهِ فَانْتَظِرُوْاۚ اِنِّيْ مَعَكُمْ مِّنَ الْمُنْتَظِرِيْنَ ࣖ

wayaquuluuna lawl*aa* unzila 'alayhi *aa*yatun min rabbihi faqul innam*aa* **a**lghaybu lill*aa*hi fa**i**nta*zh*iruu innii ma'akum mina **a**lmunta*zh*iriin**a**

Dan mereka berkata, “Mengapa tidak diturunkan kepadanya (Muhammad) suatu bukti (mukjizat) dari Tuhannya?” Katakanlah, “Sungguh, segala yang gaib itu hanya milik Allah; sebab itu tunggu (sajalah) olehmu. Ketahuilah aku juga menunggu bersama kamu.”

10:21

# وَاِذَآ اَذَقْنَا النَّاسَ رَحْمَةً مِّنْۢ بَعْدِ ضَرَّاۤءَ مَسَّتْهُمْ اِذَا لَهُمْ مَّكْرٌ فِيْٓ اٰيٰتِنَاۗ قُلِ اللّٰهُ اَسْرَعُ مَكْرًاۗ اِنَّ رُسُلَنَا يَكْتُبُوْنَ مَا تَمْكُرُوْنَ

wa-i*dzaa* a*dz*aqn*aa* **al**nn*aa*sa ra*h*matan min ba'di *dh*arr*aa*-a massat-hum i*dzaa* lahum makrun fii *aa*y*aa*tin*aa* quli **al**l*aa*hu asra'u makran inna rusu

Dan apabila Kami memberikan suatu rahmat kepada manusia, setelah mereka ditimpa bencana, mereka segera melakukan segala tipu daya (menentang) ayat-ayat Kami. Katakanlah, “Allah lebih cepat pembalasannya (atas tipu daya itu).” Sesungguhnya malaikat-malaika

10:22

# هُوَ الَّذِيْ يُسَيِّرُكُمْ فِى الْبَرِّ وَالْبَحْرِۗ حَتّٰٓى اِذَا كُنْتُمْ فِىْ الْفُلْكِۚ وَجَرَيْنَ بِهِمْ بِرِيْحٍ طَيِّبَةٍ وَّفَرِحُوْا بِهَا جَاۤءَتْهَا رِيْحٌ عَاصِفٌ وَّجَاۤءَهُمُ الْمَوْجُ مِنْ كُلِّ مَكَانٍ وَّظَنُّوْٓا اَنَّهُمْ اُحِيْطَ بِهِ

huwa **al**la*dz*ii yusayyirukum fii **a**lbarri wa**a**lba*h*ri *h*att*aa* i*dzaa* kuntum fii **a**lfulki wajarayna bihim birii*h*in *th*ayyibatin wafari*h*uu bih

Dialah Tuhan yang menjadikan kamu dapat berjalan di daratan, (dan berlayar) di lautan. Sehingga ketika kamu berada di dalam kapal, dan meluncurlah (kapal) itu membawa mereka (orang-orang yang ada di dalamnya) dengan tiupan angin yang baik, dan mereka berg

10:23

# فَلَمَّآ اَنْجٰىهُمْ اِذَا هُمْ يَبْغُوْنَ فِى الْاَرْضِ بِغَيْرِ الْحَقِّ ۗيٰٓاَيُّهَا النَّاسُ اِنَّمَا بَغْيُكُمْ عَلٰٓى اَنْفُسِكُمْ مَّتَاعَ الْحَيٰوةِ الدُّنْيَاۖ ثُمَّ اِلَيْنَا مَرْجِعُكُمْ فَنُنَبِّئُكُمْ بِمَا كُنْتُمْ تَعْمَلُوْنَ

falamm*aa* anj*aa*hum i*dzaa* hum yabghuuna fii **a**l-ar*dh*i bighayri **a**l*h*aqqi y*aa* ayyuh*aa* **al**nn*aa*su innam*aa* baghyukum 'al*aa* anfusikum mat*aa*

*Tetapi ketika Allah menyelamatkan mereka, malah mereka berbuat kezaliman di bumi tanpa (alasan) yang benar. Wahai manusia! Sesungguhnya kezalimanmu bahayanya akan menimpa dirimu sendiri; itu hanya kenikmatan hidup duniawi, selanjutnya kepada Kamilah kemba*

10:24

# اِنَّمَا مَثَلُ الْحَيٰوةِ الدُّنْيَا كَمَاۤءٍ اَنْزَلْنٰهُ مِنَ السَّمَاۤءِ فَاخْتَلَطَ بِهٖ نَبَاتُ الْاَرْضِ مِمَّا يَأْكُلُ النَّاسُ وَالْاَنْعَامُ ۗحَتّٰٓى اِذَآ اَخَذَتِ الْاَرْضُ زُخْرُفَهَا وَازَّيَّنَتْ وَظَنَّ اَهْلُهَآ اَنَّهُمْ قٰدِرُوْنَ عَ

innam*aa* matsalu **a**l*h*ay*aa*ti **al**dduny*aa* kam*aa*-in anzaln*aa*hu mina **al**ssam*aa*-i fa**i**khtala*th*a bihi nab*aa*tu **a**l-ar*d*

Sesungguhnya perumpamaan kehidupan duniawi itu, hanya seperti air (hujan) yang Kami turunkan dari langit, lalu tumbuhlah tanaman-tanaman bumi dengan subur (karena air itu), di antaranya ada yang dimakan manusia dan hewan ternak. Hingga apabila bumi itu te

10:25

# وَاللّٰهُ يَدْعُوْٓ اِلٰى دَارِ السَّلٰمِ ۚوَيَهْدِيْ مَنْ يَّشَاۤءُ اِلٰى صِرَاطٍ مُّسْتَقِيْمٍ

wa**al**l*aa*hu yad'uu il*aa* d*aa*ri **al**ssal*aa*mi wayahdii man yasy*aa*u il*aa* *sh*ir*aath*in mustaqiim**in**

Dan Allah menyeru (manusia) ke Darus-salam (surga), dan memberikan petunjuk kepada orang yang Dia kehendaki ke jalan yang lurus (Islam).

10:26

# ۞ لِلَّذِيْنَ اَحْسَنُوا الْحُسْنٰى وَزِيَادَةٌ ۗوَلَا يَرْهَقُ وُجُوْهَهُمْ قَتَرٌ وَّلَا ذِلَّةٌ ۗاُولٰۤىِٕكَ اَصْحٰبُ الْجَنَّةِ هُمْ فِيْهَا خٰلِدُوْنَ

lilla*dz*iina a*h*sanuu **a**l*h*usn*aa* waziy*aa*datun wal*aa* yarhaqu wujuuhahum qatarun wal*aa* *dz*illatun ul*aa*-ika a*sh*-*haa*bu **a**ljannati hum fiih*aa* kh*a*

Bagi orang-orang yang berbuat baik, ada pahala yang terbaik (surga) dan tambahannya (kenikmatan melihat Allah). Dan wajah mereka tidak ditutupi debu hitam dan tidak (pula) dalam kehinaan. Mereka itulah penghuni surga, mereka kekal di dalamnya.

10:27

# وَالَّذِيْنَ كَسَبُوا السَّيِّاٰتِ جَزَاۤءُ سَيِّئَةٍ ۢبِمِثْلِهَاۙ وَتَرْهَقُهُمْ ذِلَّةٌ ۗمَا لَهُمْ مِّنَ اللّٰهِ مِنْ عَاصِمٍۚ كَاَنَّمَآ اُغْشِيَتْ وُجُوْهُهُمْ قِطَعًا مِّنَ الَّيْلِ مُظْلِمًاۗ اُولٰۤىِٕكَ اَصْحٰبُ النَّارِ ۚهُمْ فِيْهَا خٰلِدُوْ

wa**a**lla*dz*iina kasabuu **al**ssayyi-*aa*ti jaz*aa*u sayyi-atin bimitslih*aa* watarhaquhum *dz*illatun m*aa* lahum mina **al**l*aa*hi min '*aas*imin ka-annam*aa* ughsyi

Adapun orang-orang yang berbuat kejahatan (akan mendapat) balasan kejahatan yang setimpal dan mereka diselubungi kehinaan. Tidak ada bagi mereka seorang pelindung pun dari (azab) Allah, seakan-akan wajah mereka ditutupi dengan kepingan-kepingan malam yang

10:28

# وَيَوْمَ نَحْشُرُهُمْ جَمِيْعًا ثُمَّ نَقُوْلُ لِلَّذِيْنَ اَشْرَكُوْا مَكَانَكُمْ اَنْتُمْ وَشُرَكَاۤؤُكُمْۚ فَزَيَّلْنَا بَيْنَهُمْ وَقَالَ شُرَكَاۤؤُهُمْ مَّا كُنْتُمْ اِيَّانَا تَعْبُدُوْنَ

wayawma na*h*syuruhum jamii'an tsumma naquulu lilla*dz*iina asyrakuu mak*aa*nakum antum wasyurak*aa*ukum fazayyaln*aa* baynahum waq*aa*la syurak*aa*uhum m*aa* kuntum iyy*aa*n*aa* ta'buduun**a**

**Dan (ingatlah) pada hari (ketika) itu Kami mengumpulkan mereka semuanya, kemudian Kami berkata kepada orang yang mempersekutukan (Allah), “Tetaplah di tempatmu, kamu dan para sekutumu.” Lalu Kami pisahkan mereka dan berkatalah sekutu-sekutu mereka, “Kamu**

10:29

# فَكَفٰى بِاللّٰهِ شَهِيْدًاۢ بَيْنَنَا وَبَيْنَكُمْ اِنْ كُنَّا عَنْ عِبَادَتِكُمْ لَغٰفِلِيْنَ

fakaf*aa* bi**al**l*aa*hi syahiidan baynan*aa* wabaynakum in kunn*aa* 'an 'ib*aa*datikum lagh*aa*filiin**a**

Maka cukuplah Allah menjadi saksi antara kami dengan kamu, sebab kami tidak tahu-menahu tentang penyembahan kamu (kepada kami).”

10:30

# هُنَالِكَ تَبْلُوْا كُلُّ نَفْسٍ مَّآ اَسْلَفَتْ وَرُدُّوْٓا اِلَى اللّٰهِ مَوْلٰىهُمُ الْحَقِّ وَضَلَّ عَنْهُمْ مَّا كَانُوْا يَفْتَرُوْنَ ࣖ

hun*aa*lika tabluu kullu nafsin m*aa* aslafat warudduu il*aa* **al**l*aa*hi mawl*aa*humu **a**l*h*aqqi wa*dh*alla 'anhum m*aa* k*aa*nuu yaftaruun**a**

Di tempat itu (padang Mahsyar), setiap jiwa merasakan pembalasan dari apa yang telah dikerjakannya (dahulu) dan mereka dikembalikan kepada Allah, pelindung mereka yang sebenarnya, dan lenyaplah dari mereka apa (pelindung palsu) yang mereka ada-adakan.

10:31

# قُلْ مَنْ يَّرْزُقُكُمْ مِّنَ السَّمَاۤءِ وَالْاَرْضِ اَمَّنْ يَّمْلِكُ السَّمْعَ وَالْاَبْصَارَ وَمَنْ يُّخْرِجُ الْحَيَّ مِنَ الْمَيِّتِ وَيُخْرِجُ الْمَيِّتَ مِنَ الْحَيِّ وَمَنْ يُّدَبِّرُ الْاَمْرَۗ فَسَيَقُوْلُوْنَ اللّٰهُ ۚفَقُلْ اَفَلَا تَتَّقُوْن

qul man yarzuqukum mina **al**ssam*aa*-i wa**a**l-ar*dh*i amman yamliku **al**ssam'a wa**a**l-ab*shaa*ra waman yukhriju **a**l*h*ayya mina **a**lmayyiti wayu

Katakanlah (Muhammad), “Siapakah yang memberi rezeki kepadamu dari langit dan bumi, atau siapakah yang kuasa (menciptakan) pendengaran dan penglihatan, dan siapakah yang mengeluarkan yang hidup dari yang mati dan mengeluarkan yang mati dari yang hidup, da

10:32

# فَذٰلِكُمُ اللّٰهُ رَبُّكُمُ الْحَقُّۚ فَمَاذَا بَعْدَ الْحَقِّ اِلَّا الضَّلٰلُ ۖفَاَنّٰى تُصْرَفُوْنَ

fa*dzaa*likumu **al**l*aa*hu rabbukumu **a**l*h*aqqu fam*aatsaa* ba'da **a**l*h*aqqi ill*aa* **al***dhdh*al*aa*lu fa-ann*aa *tu*sh*rafuun**a**

**Maka itulah Allah, Tuhan kamu yang sebenarnya; maka tidak ada setelah kebenaran itu melainkan kesesatan. Maka mengapa kamu berpaling (dari kebenaran)?**

10:33

# كَذٰلِكَ حَقَّتْ كَلِمَتُ رَبِّكَ عَلَى الَّذِيْنَ فَسَقُوْٓا اَنَّهُمْ لَا يُؤْمِنُوْنَ

ka*dzaa*lika *h*aqqat kalimatu rabbika 'al*aa* **al**la*dz*iina fasaquu annahum l*aa* yu/minuun**a**

Demikianlah telah tetap (hukuman) Tuhanmu terhadap orang-orang yang fasik, karena sesungguhnya mereka tidak beriman.

10:34

# قُلْ هَلْ مِنْ شُرَكَاۤىِٕكُمْ مَّنْ يَّبْدَؤُا الْخَلْقَ ثُمَّ يُعِيْدُهٗۗ قُلِ اللّٰهُ يَبْدَؤُا الْخَلْقَ ثُمَّ يُعِيْدُهٗ فَاَنّٰى تُؤْفَكُوْنَ

qul hal min syurak*aa*-ikum man yabdau **a**lkhalqa tsumma yu'iiduhu quli **al**l*aa*hu yabdau **a**lkhalqa tsumma yu'iiduhu fa-ann*aa* tu/fakuun**a**

Katakanlah, “Adakah di antara sekutumu yang dapat memulai penciptaan (makhluk), kemudian mengulanginya (menghidupkannya) kembali?” Katakanlah, “Allah memulai (penciptaan) makhluk, kemudian mengulanginya. Maka bagaimana kamu dipalingkan (menyembah selain A

10:35

# قُلْ هَلْ مِنْ شُرَكَاۤىِٕكُمْ مَّنْ يَّهْدِيْٓ اِلَى الْحَقِّۗ قُلِ اللّٰهُ يَهْدِيْ لِلْحَقِّۗ اَفَمَنْ يَّهْدِيْٓ اِلَى الْحَقِّ اَحَقُّ اَنْ يُّتَّبَعَ اَمَّنْ لَّا يَهِدِّيْٓ اِلَّآ اَنْ يُّهْدٰىۚ فَمَا لَكُمْۗ كَيْفَ تَحْكُمُوْنَ

qul hal min syurak*aa*-ikum man yahdii il*aa* **a**l*h*aqqi quli **al**l*aa*hu yahdii lil*h*aqqi afaman yahdii il*aa* **a**l*h*aqqi a*h*aqqu an yuttaba'a amman l*aa* yahid

Katakanlah, “Apakah di antara sekutumu ada yang membimbing kepada kebenaran?” Katakanlah, “Allah-lah yang membimbing kepada kebenaran.” Maka manakah yang lebih berhak diikuti, Tuhan yang membimbing kepada kebenaran itu, ataukah orang yang tidak mampu memb

10:36

# وَمَا يَتَّبِعُ اَكْثَرُهُمْ اِلَّا ظَنًّاۗ اِنَّ الظَّنَّ لَا يُغْنِيْ مِنَ الْحَقِّ شَيْـًٔاۗ اِنَّ اللّٰهَ عَلِيْمٌ ۢبِمَا يَفْعَلُوْنَ

wam*aa* yattabi'u aktsaruhum ill*aa* *zh*annan inna **al***zhzh*anna l*aa *yughnii mina **a**l*h*aqqi syay-an inna **al**l*aa*ha 'aliimun bim*aa* yaf'aluun**a**

Dan kebanyakan mereka hanya mengikuti dugaan. Sesungguhnya dugaan itu tidak sedikit pun berguna untuk melawan kebenaran. Sungguh, Allah Maha Mengetahui apa yang mereka kerjakan.

10:37

# وَمَا كَانَ هٰذَا الْقُرْاٰنُ اَنْ يُّفْتَرٰى مِنْ دُوْنِ اللّٰهِ وَلٰكِنْ تَصْدِيْقَ الَّذِيْ بَيْنَ يَدَيْهِ وَتَفْصِيْلَ الْكِتٰبِ لَا رَيْبَ فِيْهِ مِنْ رَّبِّ الْعٰلَمِيْنَۗ

wam*aa* k*aa*na h*aadzaa* **a**lqur-*aa*nu an yuftar*aa* min duuni **al**l*aa*hi wal*aa*kin ta*sh*diiqa **al**la*dz*ii bayna yadayhi wataf*sh*iila **a**l

Dan tidak mungkin Al-Qur'an ini dibuat-buat oleh selain Allah; tetapi (Al-Qur'an) membenarkan (kitab-kitab) yang sebelumnya dan menjelaskan hukum-hukum yang telah ditetapkannya, tidak ada keraguan di dalamnya, (diturunkan) dari Tuhan seluruh alam.

10:38

# اَمْ يَقُوْلُوْنَ افْتَرٰىهُ ۗ قُلْ فَأْتُوْا بِسُوْرَةٍ مِّثْلِهٖ وَادْعُوْا مَنِ اسْتَطَعْتُمْ مِّنْ دُوْنِ اللّٰهِ اِنْ كُنْتُمْ صٰدِقِيْنَ

am yaquuluuna iftar*aa*hu qul fa/tuu bisuuratin mitslihi wa**u**d'uu mani ista*th*a'tum min duuni **al**l*aa*hi in kuntum *shaa*diqiin**a**

Apakah pantas mereka mengatakan dia (Muhammad) yang telah membuat-buatnya? Katakanlah, “Buatlah sebuah surah yang semisal dengan surah (Al-Qur'an), dan ajaklah siapa saja di antara kamu orang yang mampu (membuatnya) selain Allah, jika kamu orang-orang yan

10:39

# بَلْ كَذَّبُوْا بِمَا لَمْ يُحِيْطُوْا بِعِلْمِهٖ وَلَمَّا يَأْتِهِمْ تَأْوِيْلُهٗۗ كَذٰلِكَ كَذَّبَ الَّذِيْنَ مِنْ قَبْلِهِمْ فَانْظُرْ كَيْفَ كَانَ عَاقِبَةُ الظّٰلِمِيْنَ

bal ka*dzdz*abuu bim*aa* lam yu*h*ii*th*uu bi'ilmihi walamm*aa* ya/tihim ta/wiiluhu ka*dzaa*lika ka*dzdz*aba **al**la*dz*iina min qablihim fa**u**n*zh*ur kayfa k*aa*na '*aa*q

Bahkan (yang sebenarnya), mereka mendustakan apa yang mereka belum mengetahuinya dengan sempurna dan belum mereka peroleh penjelasannya. Demikianlah halnya umat-umat yang ada sebelum mereka telah mendustakan (rasul). Maka perhatikanlah bagaimana akibat or

10:40

# وَمِنْهُمْ مَّنْ يُّؤْمِنُ بِهٖ وَمِنْهُمْ مَّنْ لَّا يُؤْمِنُ بِهٖۗ وَرَبُّكَ اَعْلَمُ بِالْمُفْسِدِيْنَ ࣖ

waminhum man yu/minu bihi waminhum man l*aa* yu/minu bihi warabbuka a'lamu bi**a**lmufsidiin**a**

Dan di antara mereka ada orang-orang yang beriman kepadanya (Al-Qur'an), dan di antaranya ada (pula) orang-orang yang tidak beriman kepadanya. Sedangkan Tuhanmu lebih mengetahui tentang orang-orang yang berbuat kerusakan.

10:41

# وَاِنْ كَذَّبُوْكَ فَقُلْ لِّيْ عَمَلِيْ وَلَكُمْ عَمَلُكُمْۚ اَنْتُمْ بَرِيْۤـُٔوْنَ مِمَّآ اَعْمَلُ وَاَنَا۠ بَرِيْۤءٌ مِّمَّا تَعْمَلُوْنَ

wa-in ka*dzdz*abuuka faqul lii 'amalii walakum 'amalukum antum barii-uuna mimm*aa* a'malu wa-an*aa* barii-un mimm*aa* ta'maluun**a**

Dan jika mereka (tetap) mendustakanmu (Muhammad), maka katakanlah, “Bagiku pekerjaanku dan bagimu pekerjaanmu. Kamu tidak bertanggung jawab terhadap apa yang aku kerjakan dan aku pun tidak bertanggung jawab terhadap apa yang kamu kerjakan.”

10:42

# وَمِنْهُمْ مَّنْ يَّسْتَمِعُوْنَ اِلَيْكَۗ اَفَاَنْتَ تُسْمِعُ الصُّمَّ وَلَوْ كَانُوْا لَا يَعْقِلُوْنَ

waminhum man yastami'uuna ilayka afa-anta tusmi'u **al***shsh*umma walaw k*aa*nuu l*aa* ya'qiluun**a**

Dan di antara mereka ada yang mendengarkan engkau (Muhammad). Tetapi apakah engkau dapat menjadikan orang yang tuli itu mendengar walaupun mereka tidak mengerti?

10:43

# وَمِنْهُمْ مَّنْ يَّنْظُرُ اِلَيْكَۗ اَفَاَنْتَ تَهْدِى الْعُمْيَ وَلَوْ كَانُوْا لَا يُبْصِرُوْنَ

waminhum man yan*zh*uru ilayka afa-anta tahdii **a**l'umya walaw k*aa*nuu l*aa* yub*sh*iruun**a**

Dan di antara mereka ada yang melihat kepada engkau. Tetapi apakah engkau dapat memberi petunjuk kepada orang yang buta, walaupun mereka tidak memperhatikan?

10:44

# اِنَّ اللّٰهَ لَا يَظْلِمُ النَّاسَ شَيْـًٔا وَّلٰكِنَّ النَّاسَ اَنْفُسَهُمْ يَظْلِمُوْنَ

inna **al**l*aa*ha l*aa* ya*zh*limu **al**nn*aa*sa syay-an wal*aa*kinna **al**nn*aa*sa anfusahum ya*zh*limuun**a**

Sesungguhnya Allah tidak menzalimi manusia sedikit pun, tetapi manusia itulah yang menzalimi dirinya sendiri.

10:45

# وَيَوْمَ يَحْشُرُهُمْ كَاَنْ لَّمْ يَلْبَثُوْٓا اِلَّا سَاعَةً مِّنَ النَّهَارِ يَتَعَارَفُوْنَ بَيْنَهُمْۗ قَدْ خَسِرَ الَّذِيْنَ كَذَّبُوْا بِلِقَاۤءِ اللّٰهِ وَمَا كَانُوْا مُهْتَدِيْنَ

wayawma ya*h*syuruhum ka-an lam yalbatsuu ill*aa* s*aa*'atan mina **al**nnah*aa*ri yata'*aa*rafuuna baynahum qad khasira **al**la*dz*iina ka*dzdz*abuu biliq*aa*-i **al**l*aa<*

Dan (ingatlah) pada hari (ketika) Allah mengumpulkan mereka, (mereka merasa) seakan-akan tidak pernah berdiam (di dunia) kecuali sesaat saja pada siang hari, (pada waktu) mereka saling berkenalan. Sungguh rugi orang yang mendustakan pertemuan mereka denga

10:46

# وَاِمَّا نُرِيَنَّكَ بَعْضَ الَّذِيْ نَعِدُهُمْ اَوْ نَتَوَفَّيَنَّكَ فَاِلَيْنَا مَرْجِعُهُمْ ثُمَّ اللّٰهُ شَهِيْدٌ عَلٰى مَا يَفْعَلُوْنَ

wa-imm*aa* nuriyannaka ba'*dh*a **al**la*dz*ii na'iduhum aw natawaffayannaka fa-ilayn*aa* marji'uhum tsumma **al**l*aa*hu syahiidun 'al*aa* m*aa* yaf'aluun**a**

Dan jika Kami perlihatkan kepadamu (Muhammad) sebagian dari (siksaan) yang Kami janjikan kepada mereka, (tentulah engkau akan melihatnya) atau (jika) Kami wafatkan engkau (sebelum itu), maka kepada Kami (jualah) mereka kembali, dan Allah menjadi saksi ata

10:47

# وَلِكُلِّ اُمَّةٍ رَّسُوْلٌ ۚفَاِذَا جَاۤءَ رَسُوْلُهُمْ قُضِيَ بَيْنَهُمْ بِالْقِسْطِ وَهُمْ لَا يُظْلَمُوْنَ

walikulli ummatin rasuulun fa-i*dzaa* j*aa*-a rasuuluhum qu*dh*iya baynahum bi**a**lqis*th*i wahum l*aa* yu*zh*lamuun**a**

Dan setiap umat (mempunyai) rasul. Maka apabila rasul mereka telah datang, diberlakukanlah hukum bagi mereka dengan adil dan (sedikit pun) tidak dizalimi.

10:48

# وَيَقُوْلُوْنَ مَتٰى هٰذَا الْوَعْدُ اِنْ كُنْتُمْ صٰدِقِيْنَ

wayaquuluuna mat*aa* h*aadzaa* **a**lwa'du in kuntum *shaa*diqiin**a**

Dan mereka mengatakan, ”Bilakah (datangnya) ancaman itu, jika kamu orang-orang yang benar?”

10:49

# قُلْ لَّآ اَمْلِكُ لِنَفْسِيْ ضَرًّا وَّلَا نَفْعًا اِلَّا مَا شَاۤءَ اللّٰهُ ۗ لِكُلِّ اُمَّةٍ اَجَلٌ ۚاِذَا جَاۤءَ اَجَلُهُمْ فَلَا يَسْتَأْخِرُوْنَ سَاعَةً وَّلَا يَسْتَقْدِمُوْنَ

qul l*aa* amliku linafsii *dh*arran wal*aa* naf'an ill*aa* m*aa* sy*aa*-a **al**l*aa*hu likulli ummatin ajalun i*dzaa* j*aa*-a ajaluhum fal*aa* yasta/khiruuna s*aa*'atan wal*aa* yast

Katakanlah (Muhammad), “Aku tidak kuasa menolak mudarat maupun mendatangkan manfaat kepada diriku, kecuali apa yang Allah kehendaki.” Bagi setiap umat mempunyai ajal (batas waktu). Apabila ajalnya tiba, mereka tidak dapat meminta penundaan atau percepatan

10:50

# قُلْ اَرَءَيْتُمْ اِنْ اَتٰىكُمْ عَذَابُهٗ بَيَاتًا اَوْ نَهَارًا مَّاذَا يَسْتَعْجِلُ مِنْهُ الْمُجْرِمُوْنَ

qul ara-aytum in at*aa*kum 'a*dzaa*buhu bay*aa*tan aw nah*aa*ran m*aatsaa* yasta'jilu minhu **a**lmujrimuun**a**

Katakanlah, “Terangkanlah kepadaku, jika datang kepada kamu siksaan-Nya pada waktu malam atau siang hari, manakah yang diminta untuk disegerakan orang-orang yang berdosa itu?”

10:51

# اَثُمَّ اِذَا مَا وَقَعَ اٰمَنْتُمْ بِهٖۗ اٰۤلْـٰٔنَ وَقَدْ كُنْتُمْ بِهٖ تَسْتَعْجِلُوْنَ

atsumma i*dzaa* m*aa* waqa'a *aa*mantum bihi *aa*l-*aa*na waqad kuntum bihi tasta'jiluun**a**

Kemudian apakah setelah azab itu terjadi, kamu baru mempercayainya? Apakah (baru) sekarang, padahal sebelumnya kamu selalu meminta agar disegerakan?

10:52

# ثُمَّ قِيْلَ لِلَّذِيْنَ ظَلَمُوْا ذُوْقُوْا عَذَابَ الْخُلْدِۚ هَلْ تُجْزَوْنَ اِلَّا بِمَا كُنْتُمْ تَكْسِبُوْنَ

tsumma qiila lilla*dz*iina *zh*alamuu *dz*uuquu 'a*dzaa*ba **a**lkhuldi hal tujzawna ill*aa* bim*aa* kuntum taksibuun**a**

Kemudian dikatakan kepada orang-orang yang zalim itu, “Rasakanlah olehmu siksaan yang kekal. Kamu tidak diberi balasan, melainkan (sesuai) dengan apa yang telah kamu lakukan.”

10:53

# وَيَسْتَنْۢبِـُٔوْنَكَ اَحَقٌّ هُوَ ۗ قُلْ اِيْ وَرَبِّيْٓ اِنَّهٗ لَحَقٌّ ۗوَمَآ اَنْتُمْ بِمُعْجِزِيْنَ ࣖ

wayastanbi-uunaka a*h*aqqun huwa qul ii warabbii innahu la*h*aqqun wam*aa* antum bimu'jiziin**a**

Dan mereka menanyakan kepadamu (Muhammad), “Benarkah (azab yang dijanjikan) itu?” Katakanlah, “Ya, demi Tuhanku, sesungguhnya (azab) itu pasti benar dan kamu sekali-kali tidak dapat menghindar.”

10:54

# وَلَوْ اَنَّ لِكُلِّ نَفْسٍ ظَلَمَتْ مَا فِى الْاَرْضِ لَافْتَدَتْ بِهٖۗ وَاَسَرُّوا النَّدَامَةَ لَمَّا رَاَوُا الْعَذَابَۚ وَقُضِيَ بَيْنَهُمْ بِالْقِسْطِ وَهُمْ لَا يُظْلَمُوْنَ

walaw anna likulli nafsin *zh*alamat m*aa* fii **a**l-ar*dh*i la**i**ftadat bihi wa-asarruu **al**nnad*aa*mata lamm*aa* ra-awuu **a**l'a*dzaa*ba waqu*dh*iya baynahum bi

Dan kalau setiap orang yang zalim itu (mempunyai) segala yang ada di bumi, tentu dia menebus dirinya dengan itu, dan mereka menyembunyikan penyesalannya ketika mereka telah menyaksikan azab itu. Kemudian diberi keputusan di antara mereka dengan adil, dan

10:55

# اَلَآ اِنَّ لِلّٰهِ مَا فِى السَّمٰوٰتِ وَالْاَرْضِۗ اَلَآ اِنَّ وَعْدَ اللّٰهِ حَقٌّ وَّلٰكِنَّ اَكْثَرَهُمْ لَا يَعْلَمُوْنَ

al*aa* inna lill*aa*hi m*aa* fii **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i **a**l*aa* inna wa'da **al**l*aa*hi *h*aqqun wal*aa*kinna aktsarahum l*aa* y

Ketahuilah sesungguhnya milik Allah-lah apa yang ada di langit dan di bumi. Bukankah janji Allah itu benar? Tetapi kebanyakan mereka tidak mengetahui.

10:56

# هُوَ يُحْيٖ وَيُمِيْتُ وَاِلَيْهِ تُرْجَعُوْنَ

huwa yu*h*yii wayumiitu wa-ilayhi turja'uun**a**

Dialah yang menghidupkan dan mematikan dan hanya kepada-Nyalah kamu dikembalikan.

10:57

# يٰٓاَيُّهَا النَّاسُ قَدْ جَاۤءَتْكُمْ مَّوْعِظَةٌ مِّنْ رَّبِّكُمْ وَشِفَاۤءٌ لِّمَا فِى الصُّدُوْرِۙ وَهُدًى وَّرَحْمَةٌ لِّلْمُؤْمِنِيْنَ

y*aa* ayyuh*aa* **al**nn*aa*su qad j*aa*-atkum maw'i*zh*atun min rabbikum wasyif*aa*un lim*aa* fii **al***shsh*uduuri wahudan wara*h*matun lilmu/miniin**a**

Wahai manusia! Sungguh, telah datang kepadamu pelajaran (Al-Qur'an) dari Tuhanmu, penyembuh bagi penyakit yang ada dalam dada dan petunjuk serta rahmat bagi orang yang beriman.

10:58

# قُلْ بِفَضْلِ اللّٰهِ وَبِرَحْمَتِهٖ فَبِذٰلِكَ فَلْيَفْرَحُوْاۗ هُوَ خَيْرٌ مِّمَّا يَجْمَعُوْنَ

qul bifa*dh*li **al**l*aa*hi wabira*h*matihi fabi*dzaa*lika falyafra*h*uu huwa khayrun mimm*aa* yajma'uun**a**

Katakanlah (Muhammad), “Dengan karunia Allah dan rahmat-Nya, hendaklah dengan itu mereka bergembira. Itu lebih baik daripada apa yang mereka kumpulkan.”

10:59

# قُلْ اَرَءَيْتُمْ مَّآ اَنْزَلَ اللّٰهُ لَكُمْ مِّنْ رِّزْقٍ فَجَعَلْتُمْ مِّنْهُ حَرَامًا وَّحَلٰلًا ۗ قُلْ اٰۤللّٰهُ اَذِنَ لَكُمْ اَمْ عَلَى اللّٰهِ تَفْتَرُوْنَ

qul ara-aytum m*aa* anzala **al**l*aa*hu lakum min rizqin faja'altum minhu *h*ar*aa*man wa*h*al*aa*lan qul *aa*ll*aa*hu a*dz*ina lakum am 'al*aa* **al**l*aa*hi taftaruun

Katakanlah (Muhammad), “Terangkanlah kepadaku tentang rezeki yang diturunkan Allah kepadamu, lalu kamu jadikan sebagiannya haram dan sebagiannya halal.” Katakanlah, “Apakah Allah telah memberikan izin kepadamu (ten-tang ini) ataukah kamu mengada-ada atas

10:60

# وَمَا ظَنُّ الَّذِيْنَ يَفْتَرُوْنَ عَلَى اللّٰهِ الْكَذِبَ يَوْمَ الْقِيٰمَةِ ۗاِنَّ اللّٰهَ لَذُوْ فَضْلٍ عَلَى النَّاسِ وَلٰكِنَّ اَكْثَرَهُمْ لَا يَشْكُرُوْنَ ࣖ

wam*aa* *zh*annu **al**la*dz*iina yaftaruuna 'al*aa* **al**l*aa*hi **a**lka*dz*iba yawma **a**lqiy*aa*mati inna **al**l*aa*ha la*dz*uu fa*dh*

*Dan apakah dugaan orang-orang yang mengada-adakan kebohongan terhadap Allah pada hari Kiamat? Sesungguhnya Allah benar-benar mempunyai karunia (yang dilimpahkan) kepada manusia, tetapi kebanyakan mereka tidak bersyukur.*

10:61

# وَمَا تَكُوْنُ فِيْ شَأْنٍ وَّمَا تَتْلُوْا مِنْهُ مِنْ قُرْاٰنٍ وَّلَا تَعْمَلُوْنَ مِنْ عَمَلٍ اِلَّا كُنَّا عَلَيْكُمْ شُهُوْدًا اِذْ تُفِيْضُوْنَ فِيْهِۗ وَمَا يَعْزُبُ عَنْ رَّبِّكَ مِنْ مِّثْقَالِ ذَرَّةٍ فِى الْاَرْضِ وَلَا فِى السَّمَاۤءِ وَلَآ ا

wam*aa* takuunu fii sya/nin wam*aa* tatluu minhu min qur-*aa*nin wal*aa* ta'maluuna min 'amalin ill*aa* kunn*aa* 'alaykum syuhuudan i*dz* tufii*dh*uuna fiihi wam*aa* ya'zubu 'an rabbika min mitsq*aa*li

Dan tidakkah engkau (Muhammad) berada dalam suatu urusan, dan tidak membaca suatu ayat Al-Qur'an serta tidak pula kamu melakukan suatu pekerjaan, melainkan Kami menjadi saksi atasmu ketika kamu melakukannya. Tidak lengah sedikit pun dari pengetahuan Tuhan

10:62

# اَلَآ اِنَّ اَوْلِيَاۤءَ اللّٰهِ لَا خَوْفٌ عَلَيْهِمْ وَلَا هُمْ يَحْزَنُوْنَۚ

al*aa* inna awliy*aa*-a **al**l*aa*hi l*aa* khawfun 'alayhim wal*aa* hum ya*h*zanuun**a**

Ingatlah wali-wali Allah itu, tidak ada rasa takut pada mereka dan mereka tidak bersedih hati.

10:63

# اَلَّذِيْنَ اٰمَنُوْا وَكَانُوْا يَتَّقُوْنَۗ

**al**la*dz*iina *aa*manuu wak*aa*nuu yattaquun**a**

(Yaitu) orang-orang yang beriman dan senantiasa bertakwa.

10:64

# لَهُمُ الْبُشْرٰى فِى الْحَيٰوةِ الدُّنْيَا وَفِى الْاٰخِرَةِۗ لَا تَبْدِيْلَ لِكَلِمٰتِ اللّٰهِ ۗذٰلِكَ هُوَ الْفَوْزُ الْعَظِيْمُۗ

lahumu **a**lbusyr*aa* fii **a**l*h*ay*aa*ti **al**dduny*aa* wafii **a**l-*aa*khirati l*aa* tabdiila likalim*aa*ti **al**l*aa*hi *dzaa*lika huwa

Bagi mereka berita gembira di dalam kehidupan di dunia dan di akhirat. Tidak ada perubahan bagi janji-janji Allah. Demikian itulah kemenangan yang agung.

10:65

# وَلَا يَحْزُنْكَ قَوْلُهُمْۘ اِنَّ الْعِزَّةَ لِلّٰهِ جَمِيْعًاۗ هُوَ السَّمِيْعُ الْعَلِيْمُ

wal*aa* ya*h*zunka qawluhum inna **a**l'izzata lill*aa*hi jamii'an huwa **al**ssamii'u **a**l'aliim**u**

Dan janganlah engkau (Muhammad) sedih oleh perkataan mereka. Sungguh, kekuasaan itu seluruhnya milik Allah. Dia Maha Mendengar, Maha Mengetahui.

10:66

# اَلَآ اِنَّ لِلّٰهِ مَنْ فِى السَّمٰوٰتِ وَمَنْ فِى الْاَرْضِۗ وَمَا يَتَّبِعُ الَّذِيْنَ يَدْعُوْنَ مِنْ دُوْنِ اللّٰهِ شُرَكَاۤءَ ۗاِنْ يَّتَّبِعُوْنَ اِلَّا الظَّنَّ وَاِنْ هُمْ اِلَّا يَخْرُصُوْنَ

al*aa* inna lill*aa*hi man fii **al**ssam*aa*w*aa*ti waman fii **a**l-ar*dh*i wam*aa* yattabi'u **al**la*dz*iina yad'uuna min duuni **al**l*aa*hi syurak*aa*-a

Ingatlah, milik Allah meliputi siapa yang ada di langit dan siapa yang ada di bumi. Dan orang-orang yang menyeru sekutu-sekutu selain Allah, tidaklah mengikuti (suatu keyakinan). Mereka hanya mengikuti persangkaan belaka, dan mereka hanyalah menduga-duga.

10:67

# هُوَ الَّذِيْ جَعَلَ لَكُمُ الَّيْلَ لِتَسْكُنُوْا فِيْهِ وَالنَّهَارَ مُبْصِرًا ۗاِنَّ فِيْ ذٰلِكَ لَاٰيٰتٍ لِّقَوْمٍ يَّسْمَعُوْنَ

huwa **al**la*dz*ii ja'ala lakumu **al**layla litaskunuu fiihi wa**al**nnah*aa*ra mub*sh*iran inna fii *dzaa*lika la*aa*y*aa*tin liqawmin yasma'uun**a**

Dialah yang menjadikan malam bagimu agar kamu beristirahat padanya dan menjadikan siang terang benderang. Sungguh, yang demikian itu terdapat tanda-tanda (kekuasaan Allah) bagi orang-orang yang mendengar.

10:68

# قَالُوا اتَّخَذَ اللّٰهُ وَلَدًا سُبْحٰنَهٗ ۗ هُوَ الْغَنِيُّ ۗ لَهٗ مَا فِى السَّمٰوٰتِ وَمَا فِى الْاَرْضِۗ اِنْ عِنْدَكُمْ مِّنْ سُلْطٰنٍۢ بِهٰذَاۗ اَتَقُوْلُوْنَ عَلَى اللّٰهِ مَا لَا تَعْلَمُوْنَ

q*aa*luu ittakha*dz*a **al**l*aa*hu waladan sub*haa*nahu huwa **a**lghaniyyu lahu m*aa* fii **al**ssam*aa*w*aa*ti wam*aa* fii **a**l-ar*dh*i in 'indakum min s

Mereka (orang-orang Yahudi dan Nasrani) berkata, “Allah mempunyai anak.” Mahasuci Dia, Dialah Yang Mahakaya; milik-Nyalah apa yang ada di langit dan apa yang ada di bumi. Kamu tidak mempunyai alasan kuat tentang ini. Pantaskah kamu mengatakan tentang Alla

10:69

# قُلْ اِنَّ الَّذِيْنَ يَفْتَرُوْنَ عَلَى اللّٰهِ الْكَذِبَ لَا يُفْلِحُوْنَۗ

qul inna **al**la*dz*iina yaftaruuna 'al*aa* **al**l*aa*hi **a**lka*dz*iba l*aa* yufli*h*uun**a**

Katakanlah, “Sesungguhnya orang-orang yang mengada-adakan kebohongan terhadap Allah tidak akan beruntung.”

10:70

# مَتَاعٌ فِى الدُّنْيَا ثُمَّ اِلَيْنَا مَرْجِعُهُمْ ثُمَّ نُذِيْقُهُمُ الْعَذَابَ الشَّدِيْدَ بِمَا كَانُوْا يَكْفُرُوْنَ ࣖ

mat*aa*'un fii **al**dduny*aa* tsumma ilayn*aa* marji'uhum tsumma nu*dz*iiquhumu **a**l'a*dzaa*ba **al**sysyadiida bim*aa* k*aa*nuu yakfuruun**a**

(Bagi mereka) kesenangan (sesaat) ketika di dunia, selanjutnya kepada Kamilah mereka kembali, kemudian Kami rasakan kepada mereka azab yang berat, karena kekafiran mereka.

10:71

# ۞ وَاتْلُ عَلَيْهِمْ نَبَاَ نُوْحٍۘ اِذْ قَالَ لِقَوْمِهٖ يٰقَوْمِ اِنْ كَانَ كَبُرَ عَلَيْكُمْ مَّقَامِيْ وَتَذْكِيْرِيْ بِاٰيٰتِ اللّٰهِ فَعَلَى اللّٰهِ تَوَكَّلْتُ فَاَجْمِعُوْٓا اَمْرَكُمْ وَشُرَكَاۤءَكُمْ ثُمَّ لَا يَكُنْ اَمْرُكُمْ عَلَيْكُمْ غُمَّة

wa**u**tlu 'alayhim naba-a nuu*h*in i*dz* q*aa*la liqawmihi y*aa* qawmi in k*aa*na kabura 'alaykum maq*aa*mii wata*dz*kiirii bi-*aa*y*aa*ti **al**l*aa*hi fa'al*aa* **al**

Dan bacakanlah kepada mereka berita penting (tentang) Nuh ketika (dia) berkata kepada kaumnya, “Wahai kaumku! Jika terasa berat bagimu aku tinggal (bersamamu) dan peringatanku dengan ayat-ayat Allah, maka kepada Allah aku bertawakal. Karena itu bulatkanla

10:72

# فَاِنْ تَوَلَّيْتُمْ فَمَا سَاَلْتُكُمْ مِّنْ اَجْرٍۗ اِنْ اَجْرِيَ اِلَّا عَلَى اللّٰهِ ۙوَاُمِرْتُ اَنْ اَكُوْنَ مِنَ الْمُسْلِمِيْنَ

fa-in tawallaytum fam*aa* sa-altukum min ajrin in ajriya ill*aa* 'al*aa* **al**l*aa*hi waumirtu an akuuna mina **a**lmuslimiin**a**

Maka jika kamu berpaling (dari peringatanku), aku tidak meminta imbalan sedikit pun darimu. Imbalanku tidak lain hanyalah dari Allah, dan aku diperintah agar aku termasuk golongan orang-orang Muslim (berserah diri).”

10:73

# فَكَذَّبُوْهُ فَنَجَّيْنٰهُ وَمَنْ مَّعَهٗ فِى الْفُلْكِ وَجَعَلْنٰهُمْ خَلٰۤىِٕفَ وَاَغْرَقْنَا الَّذِيْنَ كَذَّبُوْا بِاٰيٰتِنَاۚ فَانْظُرْ كَيْفَ كَانَ عَاقِبَةُ الْمُنْذَرِيْنَ

faka*dzdz*abuuhu fanajjayn*aa*hu waman ma'ahu fii **a**lfulki waja'aln*aa*hum khal*aa*-ifa wa-aghraqn*aa* **al**la*dz*iina ka*dzdz*abuu bi-*aa*y*aa*tin*aa* fa**u**n

Kemudian mereka mendustakannya (Nuh), lalu Kami selamatkan dia dan orang yang bersamanya di dalam kapal, dan Kami jadikan mereka itu khalifah dan Kami tenggelamkan orang yang mendustakan ayat-ayat Kami. Maka perhatikanlah bagaimana kesudahan orang-orang y

10:74

# ثُمَّ بَعَثْنَا مِنْۢ بَعْدِهٖ رُسُلًا اِلٰى قَوْمِهِمْ فَجَاۤءُوْهُمْ بِالْبَيِّنٰتِ فَمَا كَانُوْا لِيُؤْمِنُوْا بِمَا كَذَّبُوْا بِهٖ مِنْ قَبْلُ ۗ كَذٰلِكَ نَطْبَعُ عَلٰى قُلُوْبِ الْمُعْتَدِيْنَ

tsumma ba'atsn*aa* min ba'dihi rusulan il*aa* qawmihim faj*aa*uuhum bi**a**lbayyin*aa*ti fam*aa* k*aa*nuu liyu/minuu bim*aa* ka*dzdz*abuu bihi min qablu ka*dzaa*lika na*th*ba'u 'al*aa* qu

Kemudian setelahnya (Nuh), Kami utus beberapa rasul kepada kaum mereka (masing-masing), maka rasul-rasul itu datang kepada mereka dengan membawa keterangan yang jelas, tetapi mereka tidak mau beriman karena mereka dahulu telah (biasa) mendustakannya. Demi

10:75

# ثُمَّ بَعَثْنَا مِنْۢ بَعْدِهِمْ مُّوْسٰى وَهٰرُوْنَ اِلٰى فِرْعَوْنَ وَمَلَا۟ىِٕهٖ بِاٰيٰتِنَا فَاسْتَكْبَرُوْا وَكَانُوْا قَوْمًا مُّجْرِمِيْنَ

tsumma ba'atsn*aa* min ba'dihim muus*aa* wah*aa*ruuna il*aa* fir'awna wamala-ihi bi-*aa*y*aa*tin*aa* fa**i**stakbaruu wak*aa*nuu qawman mujrimiin**a**

Kemudian setelah mereka, Kami utus Musa dan Harun kepada Fir‘aun dan para pemuka kaumnya, dengan membawa tanda-tanda (kekuasaan) Kami. Ternyata mereka menyombongkan diri dan mereka adalah orang-orang yang berdosa.

10:76

# فَلَمَّا جَاۤءَهُمُ الْحَقُّ مِنْ عِنْدِنَا قَالُوْٓا اِنَّ هٰذَا لَسِحْرٌ مُّبِيْنٌ

falamm*aa* j*aa*-ahumu **a**l*h*aqqu min 'indin*aa* q*aa*luu inna h*aadzaa* lasi*h*run mubiin**un**

Maka ketika telah datang kepada mereka kebenaran dari sisi Kami, mereka berkata, “Ini benar-benar sihir yang nyata.”

10:77

# قَالَ مُوْسٰٓى اَتَقُوْلُوْنَ لِلْحَقِّ لَمَّا جَاۤءَكُمْ ۗ اَسِحْرٌ هٰذَاۗ وَلَا يُفْلِحُ السَّاحِرُوْنَ

q*aa*la muus*aa* ataquuluuna lil*h*aqqi lamm*aa* j*aa*-akum asi*h*run h*aadzaa* wal*aa* yufli*h*u **al**ss*aah*iruun**a**

Musa berkata, “Pantaskah kamu mengatakan terhadap kebenaran ketika ia datang kepadamu, ‘sihirkah ini?’ Padahal para pesihir itu tidaklah mendapat kemenangan.”

10:78

# قَالُوْٓا اَجِئْتَنَا لِتَلْفِتَنَا عَمَّا وَجَدْنَا عَلَيْهِ اٰبَاۤءَنَا وَتَكُوْنَ لَكُمَا الْكِبْرِيَاۤءُ فِى الْاَرْضِۗ وَمَا نَحْنُ لَكُمَا بِمُؤْمِنِيْنَ

q*aa*luu aji/tan*aa* litalfitan*aa* 'amm*aa* wajadn*aa* 'alayhi *aa*b*aa*-an*aa* watakuuna lakum*aa* **a**lkibriy*aa*u fii **a**l-ar*dh*i wam*aa* na*h*nu lakum*aa*

Mereka berkata, “Apakah engkau datang kepada kami untuk memalingkan kami dari apa (kepercayaan) yang kami dapati nenek moyang kami mengerjakannya (menyembah berhala), dan agar kamu berdua mempunyai kekuasaan di bumi (negeri Mesir)? Kami tidak akan memperc

10:79

# وَقَالَ فِرْعَوْنُ ائْتُوْنِيْ بِكُلِّ سٰحِرٍ عَلِيْمٍ

waq*aa*la fir'awnu i/tuunii bikulli s*aah*irin 'aliim**in**

Dan Fir‘aun berkata (kepada pemuka kaumnya), “Datangkanlah kepadaku semua pesihir yang ulung!”

10:80

# فَلَمَّا جَاۤءَ السَّحَرَةُ قَالَ لَهُمْ مُّوْسٰٓى اَلْقُوْا مَآ اَنْتُمْ مُّلْقُوْنَ

falamm*aa* j*aa*-a **al**ssa*h*aratu q*aa*la lahum muus*aa* **a**lquu m*aa* antum mulquun**a**

Maka ketika para pesihir itu datang, Musa berkata kepada mereka, “Lemparkanlah apa yang hendak kamu lemparkan!”

10:81

# فَلَمَّآ اَلْقَوْا قَالَ مُوْسٰى مَا جِئْتُمْ بِهِ ۙالسِّحْرُۗ اِنَّ اللّٰهَ سَيُبْطِلُهٗۗ اِنَّ اللّٰهَ لَا يُصْلِحُ عَمَلَ الْمُفْسِدِيْنَ ࣖ

falamm*aa* **a**lqaw q*aa*la muus*aa* m*aa* ji/tum bihi **al**ssi*h*ru inna **al**l*aa*ha sayub*th*iluhu inna **al**l*aa*ha l*aa* yu*sh*li*h*u 'amal

Setelah mereka melemparkan, Musa berkata, “Apa yang kamu lakukan itu, itulah sihir, sesungguhnya Allah akan menampakkan kepalsuan sihir itu. Sungguh, Allah tidak akan membiarkan terus berlangsungnya pekerjaan orang yang berbuat kerusakan.”

10:82

# وَيُحِقُّ اللّٰهُ الْحَقَّ بِكَلِمٰتِهٖ وَلَوْ كَرِهَ الْمُجْرِمُوْنَ

wayu*h*iqqu **al**l*aa*hu **a**l*h*aqqa bikalim*aa*tihi walaw kariha **a**lmujrimuun**a**

Dan Allah akan mengukuhkan yang benar dengan ketetapan-Nya, walaupun orang-orang yang berbuat dosa tidak menyukainya.

10:83

# فَمَآ اٰمَنَ لِمُوْسٰىٓ اِلَّا ذُرِّيَّةٌ مِّنْ قَوْمِهٖ عَلٰى خَوْفٍ مِّنْ فِرْعَوْنَ وَمَلَا۟ىِٕهِمْ اَنْ يَّفْتِنَهُمْ ۗوَاِنَّ فِرْعَوْنَ لَعَالٍ فِى الْاَرْضِۚ وَاِنَّهٗ لَمِنَ الْمُسْرِفِيْنَ

fam*aa* *aa*mana limuus*aa* ill*aa* *dz*urriyyatun min qawmihi 'al*aa* khawfin min fir'awna wamala-ihim an yaftinahum wa-inna fir'awna la'*aa*lin fii **a**l-ar*dh*i wa-innahu lamina **a**lm

Maka tidak ada yang beriman kepada Musa, selain keturunan dari kaumnya dalam keadaan takut bahwa Fir‘aun dan para pemuka (kaum)nya akan menyiksa mereka. Dan sungguh, Fir‘aun itu benar-benar telah berbuat sewenang-wenang di bumi, dan benar-benar termasuk o

10:84

# وَقَالَ مُوْسٰى يٰقَوْمِ اِنْ كُنْتُمْ اٰمَنْتُمْ بِاللّٰهِ فَعَلَيْهِ تَوَكَّلُوْٓا اِنْ كُنْتُمْ مُّسْلِمِيْنَ

waq*aa*la muus*aa* y*aa* qawmi in kuntum *aa*mantum bi**al**l*aa*hi fa'alayhi tawakkaluu in kuntum muslimiin**a**

Dan Musa berkata, “Wahai kaumku! Apabila kamu beriman kepada Allah, maka bertawakallah kepada-Nya, jika kamu benar-benar orang Muslim (berserah diri).”

10:85

# فَقَالُوْا عَلَى اللّٰهِ تَوَكَّلْنَا ۚرَبَّنَا لَا تَجْعَلْنَا فِتْنَةً لِّلْقَوْمِ الظّٰلِمِيْنَ

faq*aa*luu 'al*aa* **al**l*aa*hi tawakkaln*aa* rabban*aa* l*aa* taj'aln*aa* fitnatan lilqawmi **al***zhzhaa*limiin**a**

Lalu mereka berkata, “Kepada Allah-lah kami bertawakal. Ya Tuhan kami, janganlah Engkau jadikan kami (sasaran) fitnah bagi kaum yang zalim,

10:86

# وَنَجِّنَا بِرَحْمَتِكَ مِنَ الْقَوْمِ الْكٰفِرِيْنَ

wanajjin*aa* bira*h*matika mina **a**lqawmi **a**lk*aa*firiin**a**

dan selamatkanlah kami dengan rahmat-Mu dari orang-orang kafir.”

10:87

# وَاَوْحَيْنَآ اِلٰى مُوْسٰى وَاَخِيْهِ اَنْ تَبَوَّاٰ لِقَوْمِكُمَا بِمِصْرَ بُيُوْتًا وَّاجْعَلُوْا بُيُوْتَكُمْ قِبْلَةً وَّاَقِيْمُوا الصَّلٰوةَۗ وَبَشِّرِ الْمُؤْمِنِيْنَ

wa-aw*h*ayn*aa* il*aa* muus*aa* wa-akhiihi an tabawwa*aa* liqawmikum*aa* bimi*sh*ra buyuutan wa**i**j'aluu buyuutakum qiblatan wa-aqiimuu **al***shsh*al*aa*ta wabasysyiri **a**

**Dan Kami wahyukan kepada Musa dan saudaranya, “Ambillah beberapa rumah di Mesir untuk (tempat tinggal) kaummu dan jadikanlah rumah-rumahmu itu tempat ibadah dan laksanakanlah salat serta gembirakanlah orang-orang mukmin.”**

10:88

# وَقَالَ مُوْسٰى رَبَّنَآ اِنَّكَ اٰتَيْتَ فِرْعَوْنَ وَمَلَاَهٗ زِيْنَةً وَّاَمْوَالًا فِى الْحَيٰوةِ الدُّنْيَاۗ رَبَّنَا لِيُضِلُّوْا عَنْ سَبِيْلِكَ ۚرَبَّنَا اطْمِسْ عَلٰٓى اَمْوَالِهِمْ وَاشْدُدْ عَلٰى قُلُوْبِهِمْ فَلَا يُؤْمِنُوْا حَتّٰى يَرَوُا ا

waq*aa*la muus*aa* rabban*aa* innaka *aa*tayta fir'awna wamala-ahu ziinatan wa-amw*aa*lan fii **a**l*h*ay*aa*ti **al**dduny*aa* rabban*aa* liyu*dh*illuu 'an sabiilika rabban*aa<*

Dan Musa berkata, “Ya Tuhan kami, Engkau telah memberikan kepada Fir‘aun dan para pemuka kaumnya perhiasan dan harta kekayaan dalam kehidupan dunia. Ya Tuhan kami, (akibatnya) mereka menyesatkan (manusia) dari jalan-Mu. Ya Tuhan, binasakanlah harta mereka

10:89

# قَالَ قَدْ اُجِيْبَتْ دَّعْوَتُكُمَا فَاسْتَقِيْمَا وَلَا تَتَّبِعٰۤنِّ سَبِيْلَ الَّذِيْنَ لَا يَعْلَمُوْنَ

q*aa*la qad ujiibat da'watukum*aa* fa**i**staqiim*aa* wal*aa* tattabi'*aa*nni sabiila **al**la*dz*iina l*aa* ya'lamuun**a**

Dia Allah berfirman, “Sungguh, telah diperkenankan permohonan kamu berdua, sebab itu tetaplah kamu berdua pada jalan yang lurus dan jangan sekali-kali kamu mengikuti jalan orang yang tidak mengetahui.”

10:90

# ۞ وَجَاوَزْنَا بِبَنِيْٓ اِسْرَاۤءِيْلَ الْبَحْرَ فَاَتْبَعَهُمْ فِرْعَوْنُ وَجُنُوْدُهٗ بَغْيًا وَّعَدْوًا ۗحَتّٰىٓ اِذَآ اَدْرَكَهُ الْغَرَقُ قَالَ اٰمَنْتُ اَنَّهٗ لَآ اِلٰهَ اِلَّا الَّذِيْٓ اٰمَنَتْ بِهٖ بَنُوْٓا اِسْرَاۤءِيْلَ وَاَنَا۠ مِنَ الْمُس

waj*aa*wazn*aa* bibanii isr*aa*-iila **a**lba*h*ra fa-atba'ahum fir'awnu wajunuuduhu baghyan wa'adwan *h*att*aa* i*dzaa* adrakahu **a**lgharaqu q*aa*la *aa*mantu annahu l*aa* il

Dan Kami selamatkan Bani Israil melintasi laut, kemudian Fir‘aun dan bala tentaranya mengikuti mereka, untuk menzalimi dan menindas (mereka). Sehingga ketika Fir‘aun hampir tenggelam dia berkata, “Aku percaya bahwa tidak ada tuhan melainkan Tuhan yang dip

10:91

# اٰۤلْـٰٔنَ وَقَدْ عَصَيْتَ قَبْلُ وَكُنْتَ مِنَ الْمُفْسِدِيْنَ

*aa*l-*aa*na waqad 'a*sh*ayta qablu wakunta mina **a**lmufsidiin**a**

Mengapa baru sekarang (kamu beriman), padahal sesungguhnya engkau telah durhaka sejak dahulu, dan engkau termasuk orang yang berbuat kerusakan.

10:92

# فَالْيَوْمَ نُنَجِّيْكَ بِبَدَنِكَ لِتَكُوْنَ لِمَنْ خَلْفَكَ اٰيَةً ۗوَاِنَّ كَثِيْرًا مِّنَ النَّاسِ عَنْ اٰيٰتِنَا لَغٰفِلُوْنَ

fa**a**lyawma nunajjiika bibadanika litakuuna liman khalfaka *aa*yatan wa-inna katsiiran mina **al**nn*aa*si 'an *aa*y*aa*tin*aa* lagh*aa*filuun**a**

Maka pada hari ini Kami selamatkan jasadmu agar engkau dapat menjadi pelajaran bagi orang-orang yang datang setelahmu, tetapi kebanyakan manusia tidak mengindahkan tanda-tanda (kekuasaan) Kami.

10:93

# وَلَقَدْ بَوَّأْنَا بَنِيْٓ اِسْرَاۤءِيْلَ مُبَوَّاَ صِدْقٍ وَّرَزَقْنٰهُمْ مِّنَ الطَّيِّبٰتِ ۚفَمَا اخْتَلَفُوْا حَتّٰى جَاۤءَهُمُ الْعِلْمُ ۗاِنَّ رَبَّكَ يَقْضِيْ بَيْنَهُمْ يَوْمَ الْقِيٰمَةِ فِيْمَا كَانُوْا فِيْهِ يَخْتَلِفُوْنَ

walaqad bawwa/n*aa* banii isr*aa*-iila mubawwa-a *sh*idqin warazaqn*aa*hum mina **al***ththh*ayyib*aa*ti fam*aa *ikhtalafuu* h*att*aa *j*aa*\-ahumu **a**l'ilmu inna rabbaka yaq*d*

Dan sungguh, Kami telah menempatkan Bani Israil di tempat kediaman yang bagus dan Kami beri mereka rezeki yang baik. Maka mereka tidak berselisih, kecuali setelah datang kepada mereka pengetahuan (yang tersebut dalam Taurat). Sesungguhnya Tuhan kamu akan

10:94

# فَاِنْ كُنْتَ فِيْ شَكٍّ مِّمَّآ اَنْزَلْنَآ اِلَيْكَ فَسْـَٔلِ الَّذِيْنَ يَقْرَءُوْنَ الْكِتٰبَ مِنْ قَبْلِكَ ۚ لَقَدْ جَاۤءَكَ الْحَقُّ مِنْ رَّبِّكَ فَلَا تَكُوْنَنَّ مِنَ الْمُمْتَرِيْنَۙ

fa-in kunta fii syakkin mimm*aa* anzalnn*aa* ilayka fa**i**s-ali **al**la*dz*iina yaqrauuna **a**lkit*aa*ba min qablika laqad j*aa*-aka **a**l*h*aqqu min rabbika fal*aa*

*Maka jika engkau (Muhammad) berada dalam keragu-raguan tentang apa yang Kami turunkan kepadamu, maka tanyakanlah kepada orang yang membaca kitab sebelummu. Sungguh, telah datang kebenaran kepadamu dari Tuhanmu, maka janganlah sekali-kali engkau termasuk o*

10:95

# وَلَا تَكُوْنَنَّ مِنَ الَّذِيْنَ كَذَّبُوْا بِاٰيٰتِ اللّٰهِ فَتَكُوْنَ مِنَ الْخٰسِرِيْنَ

wal*aa* takuunanna mina **al**la*dz*iina ka*dzdz*abuu bi-*aa*y*aa*ti **al**l*aa*hi fatakuuna mina **a**lkh*aa*siriin**a**

Dan janganlah sekali-kali engkau termasuk orang yang mendustakan ayat-ayat Allah, nanti engkau termasuk orang yang rugi.

10:96

# اِنَّ الَّذِيْنَ حَقَّتْ عَلَيْهِمْ كَلِمَةُ رَبِّكَ لَا يُؤْمِنُوْنَ

inna **al**la*dz*iina *h*aqqat 'alayhim kalimatu rabbika l*aa* yu/minuun**a**

Sungguh, orang-orang yang telah dipastikan mendapat ketetapan Tuhanmu, tidaklah akan beriman,

10:97

# وَلَوْ جَاۤءَتْهُمْ كُلُّ اٰيَةٍ حَتّٰى يَرَوُا الْعَذَابَ الْاَلِيْمَ

walaw j*aa*-at-hum kullu *aa*yatin *h*att*aa* yarawuu **a**l'a*dzaa*ba **a**l-aliim**a**

meskipun mereka mendapat tanda-tanda (kebesaran Allah), hingga mereka menyaksikan azab yang pedih.

10:98

# فَلَوْلَا كَانَتْ قَرْيَةٌ اٰمَنَتْ فَنَفَعَهَآ اِيْمَانُهَآ اِلَّا قَوْمَ يُوْنُسَۗ لَمَّآ اٰمَنُوْا كَشَفْنَا عَنْهُمْ عَذَابَ الْخِزْيِ فِى الْحَيٰوةِ الدُّنْيَا وَمَتَّعْنٰهُمْ اِلٰى حِيْنٍ

falawl*aa* k*aa*nat qaryatun *aa*manat fanafa'ah*aa* iim*aa*nuh*aa* ill*aa* qawma yuunusa lamm*aa* *aa*manuu kasyafn*aa* 'anhum 'a*dzaa*ba **a**lkhizyi fii **a**l*h*ay

Maka mengapa tidak ada (penduduk) suatu negeri pun yang beriman, lalu imannya itu bermanfaat kepadanya selain kaum Yunus? Ketika mereka (kaum Yunus itu) beriman, Kami hilangkan dari mereka azab yang menghinakan dalam kehidupan dunia, dan Kami beri kesenan

10:99

# وَلَوْ شَاۤءَ رَبُّكَ لَاٰمَنَ مَنْ فِى الْاَرْضِ كُلُّهُمْ جَمِيْعًاۗ اَفَاَنْتَ تُكْرِهُ النَّاسَ حَتّٰى يَكُوْنُوْا مُؤْمِنِيْنَ

walaw sy*aa*-a rabbuka la*aa*mana man fii **a**l-ar*dh*i kulluhum jamii'an afa-anta tukrihu **al**nn*aa*sa *h*att*aa* yakuunuu mu/miniin**a**

Dan jika Tuhanmu menghendaki, tentulah beriman semua orang di bumi seluruhnya. Tetapi apakah kamu (hendak) memaksa manusia agar mereka menjadi orang-orang yang beriman?

10:100

# وَمَا كَانَ لِنَفْسٍ اَنْ تُؤْمِنَ اِلَّا بِاِذْنِ اللّٰهِ ۗوَيَجْعَلُ الرِّجْسَ عَلَى الَّذِيْنَ لَا يَعْقِلُوْنَ

wam*aa* k*aa*na linafsin an tu/mina ill*aa* bi-i*dz*ni **al**l*aa*hi wayaj'alu **al**rrijsa 'al*aa* **al**la*dz*iina l*aa* ya'qiluun**a**

Dan tidak seorang pun akan beriman kecuali dengan izin Allah, dan Allah menimpakan azab kepada orang yang tidak mengerti.

10:101

# قُلِ انْظُرُوْا مَاذَا فِى السَّمٰوٰتِ وَالْاَرْضِ ۗوَمَا تُغْنِى الْاٰيٰتُ وَالنُّذُرُ عَنْ قَوْمٍ لَّا يُؤْمِنُوْنَ

quli un*zh*uruu m*aatsaa* fii **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i wam*aa* tughnii **a**l-*aa*y*aa*tu wa**al**nnu*dz*uru 'an qawmin l*aa* yu/minuun

Katakanlah, “Perhatikanlah apa yang ada di langit dan di bumi!” Tidaklah bermanfaat tanda-tanda (kebesaran Allah) dan rasul-rasul yang memberi peringatan bagi orang yang tidak beriman.

10:102

# فَهَلْ يَنْتَظِرُوْنَ اِلَّا مِثْلَ اَيَّامِ الَّذِيْنَ خَلَوْا مِنْ قَبْلِهِمْۗ قُلْ فَانْتَظِرُوْٓا اِنِّيْ مَعَكُمْ مِّنَ الْمُنْتَظِرِيْنَ

fahal yanta*zh*iruuna ill*aa* mitsla ayy*aa*mi **al**la*dz*iina khalaw min qablihim qul fa**i**nta*zh*iruu innii ma'akum mina **a**lmunta*zh*iriin**a**

Maka mereka tidak menunggu-nunggu kecuali (kejadian-kejadian) yang sama dengan kejadian-kejadian (yang menimpa) orang-orang terdahulu sebelum mereka. Katakanlah, “Maka tunggulah, aku pun termasuk orang yang menunggu bersama kamu.”

10:103

# ثُمَّ نُنَجِّيْ رُسُلَنَا وَالَّذِيْنَ اٰمَنُوْا كَذٰلِكَ ۚحَقًّا عَلَيْنَا نُنْجِ الْمُؤْمِنِيْنَ ࣖ

tsumma nunajjii rusulan*aa* wa**a**lla*dz*iina *aa*manuu ka*dzaa*lika *h*aqqan 'alayn*aa* nunjii **a**lmu/miniin**a**

Kemudian Kami selamatkan rasul-rasul Kami dan orang-orang yang beri-man, demikianlah menjadi kewajiban Kami menyelamatkan orang yang beriman.

10:104

# قُلْ يٰٓاَيُّهَا النَّاسُ اِنْ كُنْتُمْ فِيْ شَكٍّ مِّنْ دِيْنِيْ فَلَآ اَعْبُدُ الَّذِيْنَ تَعْبُدُوْنَ مِنْ دُوْنِ اللّٰهِ وَلٰكِنْ اَعْبُدُ اللّٰهَ الَّذِيْ يَتَوَفّٰىكُمْ ۖ وَاُمِرْتُ اَنْ اَكُوْنَ مِنَ الْمُؤْمِنِيْنَ

qul y*aa* ayyuh*aa* **al**nn*aa*su in kuntum fii syakkin min diinii fal*aa* a'budu **al**la*dz*iina ta'buduuna min duuni **al**l*aa*hi wal*aa*kin a'budu **al**l*aa*

*Katakanlah (Muhammad), “Wahai manusia! Jika kamu masih dalam keragu-raguan tentang agamaku, maka (ketahuilah) aku tidak menyembah yang kamu sembah selain Allah, tetapi aku menyembah Allah yang akan mematikan kamu dan aku telah diperintah agar termasuk ora*

10:105

# وَاَنْ اَقِمْ وَجْهَكَ لِلدِّيْنِ حَنِيْفًاۚ وَلَا تَكُوْنَنَّ مِنَ الْمُشْرِكِيْنَ

wa-an aqim wajhaka li**l**ddiini *h*aniifan wal*aa* takuunanna mina **a**lmusyrikiin**a**

dan (aku telah diperintah), “Hadapkanlah wajahmu kepada agama dengan tulus dan ikhlas dan jangan sekali-kali engkau termasuk orang yang musyrik.

10:106

# وَلَا تَدْعُ مِنْ دُوْنِ اللّٰهِ مَا لَا يَنْفَعُكَ وَلَا يَضُرُّكَ ۚفَاِنْ فَعَلْتَ فَاِنَّكَ اِذًا مِّنَ الظّٰلِمِيْنَ

wal*aa* tad'u min duuni **al**l*aa*hi m*aa* l*aa* yanfa'uka wal*aa* ya*dh*urruka fa-in fa'alta fa-innaka i*dz*an mina **al***zhzhaa*limiin**a**

Dan jangan engkau menyembah sesuatu yang tidak memberi manfaat dan tidak (pula) memberi bencana kepadamu selain Allah, sebab jika engkau lakukan (yang demikian), maka sesungguhnya engkau termasuk orang-orang zalim.”

10:107

# وَاِنْ يَّمْسَسْكَ اللّٰهُ بِضُرٍّ فَلَا كَاشِفَ لَهٗ ٓاِلَّا هُوَ ۚوَاِنْ يُّرِدْكَ بِخَيْرٍ فَلَا رَاۤدَّ لِفَضْلِهٖۗ يُصِيْبُ بِهٖ مَنْ يَّشَاۤءُ مِنْ عِبَادِهٖ ۗوَهُوَ الْغَفُوْرُ الرَّحِيْمُ

wa-in yamsaska **al**l*aa*hu bi*dh*urrin fal*aa* k*aa*syifa lahu ill*aa* huwa wa-in yuridka bikhayrin fal*aa* r*aa*dda lifa*dh*lihi yu*sh*iibu bihi man yasy*aa*u min 'ib*aa*dihi wahuwa

Dan jika Allah menimpakan suatu bencana kepadamu, maka tidak ada yang dapat menghilangkannya kecuali Dia. Dan jika Allah menghendaki kebaikan bagi kamu, maka tidak ada yang dapat menolak karunia-Nya. Dia memberikan kebaikan kepada siapa saja yang Dia kehe

10:108

# قُلْ يٰٓاَيُّهَا النَّاسُ قَدْ جَاۤءَكُمُ الْحَقُّ مِنْ رَّبِّكُمْ ۚفَمَنِ اهْتَدٰى فَاِنَّمَا يَهْتَدِيْ لِنَفْسِهٖ ۚوَمَنْ ضَلَّ فَاِنَّمَا يَضِلُّ عَلَيْهَا ۚوَمَآ اَنَا۠ عَلَيْكُمْ بِوَكِيْلٍۗ

qul y*aa* ayyuh*aa* **al**nn*aa*su qad j*aa*-akumu **a**l*h*aqqu min rabbikum famani ihtad*aa* fa-innam*aa* yahtadii linafsihi waman *dh*alla fa-innam*aa* ya*dh*illu 'alayh*aa*

*Katakanlah (Muhammad), “Wahai manusia! Telah datang kepadamu kebenaran (Al-Qur'an) dari Tuhanmu, sebab itu barang siapa mendapat petunjuk, maka sebenarnya (petunjuk itu) untuk (kebaikan) dirinya sendiri. Dan barang siapa sesat, sesungguhnya kesesatannya i*

10:109

# وَاتَّبِعْ مَا يُوْحٰىٓ اِلَيْكَ وَاصْبِرْ حَتّٰى يَحْكُمَ اللّٰهُ ۚوَهُوَ خَيْرُ الْحٰكِمِيْنَ ࣖ

wa**i**ttabi' m*aa* yuu*haa* ilayka wa**i***sh*bir* h*att*aa *ya*h*kuma **al**l*aa*hu wahuwa khayru **a**l*haa*kimiin**a**

Dan ikutilah apa yang diwahyukan kepadamu, dan bersabarlah hingga Allah memberi keputusan. Dialah hakim yang sebaik-baiknya.

<!--EndFragment-->