---
title: (13) Ar-Ra'd - الرّعد
date: 2021-10-27T03:34:30.970Z
ayat: 13
description: "Jumlah Ayat: 43 / Arti: Guruh"
---
<!--StartFragment-->

13:1

# الۤمّۤرٰۗ تِلْكَ اٰيٰتُ الْكِتٰبِۗ وَالَّذِيْٓ اُنْزِلَ اِلَيْكَ مِنْ رَّبِّكَ الْحَقُّ وَلٰكِنَّ اَكْثَرَ النَّاسِ لَا يُؤْمِنُوْنَ

alif-l*aa*m-miim-r*aa* tilka *aa*y*aa*tu **a**lkit*aa*bi wa**a**lla*dz*ii unzila ilayka min rabbika **a**l*h*aqqu wal*aa*kinna aktsara **al**nn*aa*si l*aa*

*Alif Lam Mim Ra. Ini adalah ayat-ayat Kitab (Al-Qur'an). Dan (Kitab) yang diturunkan kepadamu (Muhammad) dari Tuhanmu itu adalah benar; tetapi kebanyakan manusia tidak beriman (kepadanya).*









13:2

# اَللّٰهُ الَّذِيْ رَفَعَ السَّمٰوٰتِ بِغَيْرِ عَمَدٍ تَرَوْنَهَا ثُمَّ اسْتَوٰى عَلَى الْعَرْشِ وَسَخَّرَ الشَّمْسَ وَالْقَمَرَۗ كُلٌّ يَّجْرِيْ لِاَجَلٍ مُّسَمًّىۗ يُدَبِّرُ الْاَمْرَ يُفَصِّلُ الْاٰيٰتِ لَعَلَّكُمْ بِلِقَاۤءِ رَبِّكُمْ تُوْقِنُوْنَ

**al**l*aa*hu **al**la*dz*ii rafa'a **al**ssam*aa*w*aa*ti bighayri 'amadin tarawnah*aa* tsumma istaw*aa* 'al*aa* **a**l'arsyi wasakhkhara **al**sysyamsa wa

Allah yang meninggikan langit tanpa tiang (sebagaimana) yang kamu lihat, kemudian Dia bersemayam di atas ‘Arsy. Dia menundukkan matahari dan bulan; masing-masing beredar menurut waktu yang telah ditentukan. Dia mengatur urusan (makhluk-Nya), dan menjelask

13:3

# وَهُوَ الَّذِيْ مَدَّ الْاَرْضَ وَجَعَلَ فِيْهَا رَوَاسِيَ وَاَنْهٰرًا ۗوَمِنْ كُلِّ الثَّمَرٰتِ جَعَلَ فِيْهَا زَوْجَيْنِ اثْنَيْنِ يُغْشِى الَّيْلَ النَّهَارَۗ اِنَّ فِيْ ذٰلِكَ لَاٰيٰتٍ لِّقَوْمٍ يَّتَفَكَّرُوْنَ

wahuwa **al**la*dz*ii madda **a**l-ar*dh*a waja'ala fiih*aa* raw*aa*siya wa-anh*aa*ran wamin kulli **al**tstsamar*aa*ti ja'ala fiih*aa* zawjayni itsnayni yughsyii **al**

**Dan Dia yang menghamparkan bumi dan menjadikan gunung-gunung dan sungai-sungai di atasnya. Dan padanya Dia menjadikan semua buah-buahan berpasang-pasangan; Dia menutupkan malam kepada siang. Sungguh, pada yang demikian itu terdapat tanda-tanda (kebesaran**









13:4

# وَفِى الْاَرْضِ قِطَعٌ مُّتَجٰوِرٰتٌ وَّجَنّٰتٌ مِّنْ اَعْنَابٍ وَّزَرْعٌ وَّنَخِيْلٌ صِنْوَانٌ وَّغَيْرُ صِنْوَانٍ يُّسْقٰى بِمَاۤءٍ وَّاحِدٍۙ وَّنُفَضِّلُ بَعْضَهَا عَلٰى بَعْضٍ فِى الْاُكُلِۗ اِنَّ فِيْ ذٰلِكَ لَاٰيٰتٍ لِّقَوْمٍ يَّعْقِلُوْنَ

wafii **a**l-ar*dh*i qi*th*a'un mutaj*aa*wir*aa*tun wajann*aa*tun min a'n*aa*bin wazar'un wanakhiilun *sh*inw*aa*nun waghayru *sh*inw*aa*nin yusq*aa* bim*aa*-in w*aah*idin wanuf

Dan di bumi terdapat bagian-bagian yang berdampingan, kebun-kebun anggur, tanaman-tanaman, pohon kurma yang bercabang, dan yang tidak bercabang; disirami dengan air yang sama, tetapi Kami lebihkan tanaman yang satu dari yang lainnya dalam hal rasanya. Sun

13:5

# ۞ وَاِنْ تَعْجَبْ فَعَجَبٌ قَوْلُهُمْ ءَاِذَا كُنَّا تُرَابًا ءَاِنَّا لَفِيْ خَلْقٍ جَدِيْدٍ ەۗ اُولٰۤىِٕكَ الَّذِيْنَ كَفَرُوْا بِرَبِّهِمْۚ وَاُولٰۤىِٕكَ الْاَغْلٰلُ فِيْٓ اَعْنَاقِهِمْۚ وَاُولٰۤىِٕكَ اَصْحٰبُ النَّارِۚ هُمْ فِيْهَا خٰلِدُوْنَ

wa-in ta'jab fa'ajabun qawluhum a-i*dzaa* kunn*aa* tur*aa*ban a-inn*aa* lafii khalqin jadiidin ul*aa*-ika **al**la*dz*iina kafaruu birabbihim waul*aa*-ika **a**l-aghl*aa*lu fii a'n*aa*

Dan jika engkau merasa heran, maka yang mengherankan adalah ucapan mereka, “Apabila kami telah menjadi tanah, apakah kami akan (dikembalikan) menjadi makhluk yang baru?” Mereka itulah yang ingkar kepada Tuhannya; dan mereka itulah (yang dilekatkan) beleng

13:6

# وَيَسْتَعْجِلُوْنَكَ بِالسَّيِّئَةِ قَبْلَ الْحَسَنَةِ وَقَدْ خَلَتْ مِنْ قَبْلِهِمُ الْمَثُلٰتُۗ وَاِنَّ رَبَّكَ لَذُوْ مَغْفِرَةٍ لِّلنَّاسِ عَلٰى ظُلْمِهِمْۚ وَاِنَّ رَبَّكَ لَشَدِيْدُ الْعِقَابِ

wayasta'jiluunaka bi**al**ssayyi-ati qabla **a**l*h*asanati waqad khalat min qablihimu **a**lmatsul*aa*tu wa-inna rabbaka la*dz*uu maghfiratin li**l**nn*aa*si 'al*aa* *zh*ul

Dan mereka meminta kepadamu agar dipercepat (datangnya) siksaan, sebelum (mereka meminta) kebaikan, padahal telah terjadi bermacam-macam contoh siksaan sebelum mereka. Sungguh, Tuhanmu benar-benar memiliki ampunan bagi manusia atas kezaliman mereka, dan s

13:7

# وَيَقُوْلُ الَّذِيْنَ كَفَرُوْا لَوْلَآ اُنْزِلَ عَلَيْهِ اٰيَةٌ مِّنْ رَّبِّهٖۗ اِنَّمَآ اَنْتَ مُنْذِرٌ وَّلِكُلِّ قَوْمٍ هَادٍ ࣖ

wayaquulu **al**la*dz*iina kafaruu lawl*aa* unzila 'alayhi *aa*yatun min rabbihi innam*aa* anta mun*dz*irun walikulli qawmin h*aa*d**in**

Dan orang-orang kafir berkata, “Mengapa tidak diturunkan kepadanya (Muhammad) suatu tanda (mukjizat) dari Tuhannya?” Sesungguhnya engkau hanyalah seorang pemberi peringatan; dan bagi setiap kaum ada orang yang memberi petunjuk.

13:8

# اَللّٰهُ يَعْلَمُ مَا تَحْمِلُ كُلُّ اُنْثٰى وَمَا تَغِيْضُ الْاَرْحَامُ وَمَا تَزْدَادُ ۗوَكُلُّ شَيْءٍ عِنْدَهٗ بِمِقْدَارٍ

**al**l*aa*hu ya'lamu m*aa* ta*h*milu kullu unts*aa* wam*aa* taghii*dh*u **a**l-ar*haa*mu wam*aa* tazd*aa*du wakullu syay-in 'indahu bimiqd*aa*r**in**

Allah mengetahui apa yang dikandung oleh setiap perempuan, apa yang kurang sempurna dan apa yang bertambah dalam rahim. Dan segala sesuatu ada ukuran di sisi-Nya.

13:9

# عٰلِمُ الْغَيْبِ وَالشَّهَادَةِ الْكَبِيْرُ الْمُتَعَالِ

'*aa*limu **a**lghaybi wa**al**sysyah*aa*dati **a**lkabiiru **a**lmuta'*aa*l**i**

(Allah) Yang mengetahui semua yang gaib dan yang nyata; Yang Mahabesar, Mahatinggi.

13:10

# سَوَاۤءٌ مِّنْكُمْ مَّنْ اَسَرَّ الْقَوْلَ وَمَنْ جَهَرَ بِهٖ وَمَنْ هُوَ مُسْتَخْفٍۢ بِالَّيْلِ وَسَارِبٌۢ بِالنَّهَارِ

saw*aa*un minkum man asarra **a**lqawla waman jahara bihi waman huwa mustakhfin bi**a**llayli was*aa*ribun bi**al**nnah*aa*r**i**

Sama saja (bagi Allah), siapa di antaramu yang merahasiakan ucapannya dan siapa yang berterus terang dengannya; dan siapa yang bersembunyi pada malam hari dan yang berjalan pada siang hari.

13:11

# لَهٗ مُعَقِّبٰتٌ مِّنْۢ بَيْنِ يَدَيْهِ وَمِنْ خَلْفِهٖ يَحْفَظُوْنَهٗ مِنْ اَمْرِ اللّٰهِ ۗاِنَّ اللّٰهَ لَا يُغَيِّرُ مَا بِقَوْمٍ حَتّٰى يُغَيِّرُوْا مَا بِاَنْفُسِهِمْۗ وَاِذَآ اَرَادَ اللّٰهُ بِقَوْمٍ سُوْۤءًا فَلَا مَرَدَّ لَهٗ ۚوَمَا لَهُمْ مِّنْ

lahu mu'aqqib*aa*tun min bayni yadayhi wamin khalfihi ya*h*fa*zh*uunahu min amri **al**l*aa*hi inna **al**l*aa*ha l*aa* yughayyiru m*aa* biqawmin *h*att*aa* yughayyiruu m*aa* bi-a

Baginya (manusia) ada malaikat-malaikat yang selalu menjaganya bergiliran, dari depan dan belakangnya. Mereka menjaganya atas perintah Allah. Sesungguhnya Allah tidak akan mengubah keadaan suatu kaum sebelum mereka mengubah keadaan diri mereka sendiri. Da

13:12

# هُوَ الَّذِيْ يُرِيْكُمُ الْبَرْقَ خَوْفًا وَّطَمَعًا وَّيُنْشِئُ السَّحَابَ الثِّقَالَۚ

huwa **al**la*dz*ii yuriikumu **a**lbarqa khawfan wa*th*ama'an wayunsyi-u **al**ssa*haa*ba **al**tstsiq*aa*l**a**

Dialah yang memperlihatkan kilat kepadamu, yang menimbulkan ketakutan dan harapan, dan Dia menjadikan mendung.

13:13

# وَيُسَبِّحُ الرَّعْدُ بِحَمْدِهٖ وَالْمَلٰۤىِٕكَةُ مِنْ خِيْفَتِهٖۚ وَيُرْسِلُ الصَّوَاعِقَ فَيُصِيْبُ بِهَا مَنْ يَّشَاۤءُ وَهُمْ يُجَادِلُوْنَ فِى اللّٰهِ ۚوَهُوَ شَدِيْدُ الْمِحَالِۗ

wayusabbi*h*u **al**rra'du bi*h*amdihi wa**a**lmal*aa*-ikatu min khiifatihi wayursilu **al***shsh*aw*aa*'iqa fayu*sh*iibu bih*aa* man yasy*aa*u wahum yuj*aa*diluuna fii

Dan guruh bertasbih memuji-Nya, (demikian pula) para malaikat karena takut kepada-Nya, dan Allah melepaskan halilintar, lalu menimpakannya kepada siapa yang Dia kehendaki, sementara mereka berbantah-bantahan tentang Allah, dan Dia Mahakeras siksaan-Nya.

13:14

# لَهٗ دَعْوَةُ الْحَقِّۗ وَالَّذِيْنَ يَدْعُوْنَ مِنْ دُوْنِهٖ لَا يَسْتَجِيْبُوْنَ لَهُمْ بِشَيْءٍ اِلَّا كَبَاسِطِ كَفَّيْهِ اِلَى الْمَاۤءِ لِيَبْلُغَ فَاهُ وَمَا هُوَ بِبَالِغِهٖۗ وَمَا دُعَاۤءُ الْكٰفِرِيْنَ اِلَّا فِيْ ضَلٰلٍ

lahu da'watu **a**l*h*aqqi wa**a**lla*dz*iina yad'uuna min duunihi l*aa* yastajiibuuna lahum bisyay-in ill*aa* kab*aa*si*th*i kaffayhi il*aa* **a**lm*aa*-i liyablugha f*aa*

*Hanya kepada Allah doa yang benar. Berhala-berhala yang mereka sembah selain Allah tidak dapat mengabulkan apa pun bagi mereka, tidak ubahnya seperti orang yang membukakan kedua telapak tangannya ke dalam air agar (air) sampai ke mulutnya. Padahal air itu*









13:15

# وَلِلّٰهِ يَسْجُدُ مَنْ فِى السَّمٰوٰتِ وَالْاَرْضِ طَوْعًا وَّكَرْهًا وَّظِلٰلُهُمْ بِالْغُدُوِّ وَالْاٰصَالِ ۩

walill*aa*hi yasjudu man fii **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i *th*aw'an wakarhan wa*zh*il*aa*luhum bi**a**lghuduwwi wa**a**l-*aasaa*l**i**

Dan semua sujud kepada Allah baik yang di langit maupun yang di bumi, baik dengan kemauan sendiri maupun terpaksa (dan sujud pula) bayang-bayang mereka, pada waktu pagi dan petang hari.

13:16

# قُلْ مَنْ رَّبُّ السَّمٰوٰتِ وَالْاَرْضِۗ قُلِ اللّٰهُ ۗقُلْ اَفَاتَّخَذْتُمْ مِّنْ دُوْنِهٖٓ اَوْلِيَاۤءَ لَا يَمْلِكُوْنَ لِاَنْفُسِهِمْ نَفْعًا وَّلَا ضَرًّاۗ قُلْ هَلْ يَسْتَوِى الْاَعْمٰى وَالْبَصِيْرُ ەۙ اَمْ هَلْ تَسْتَوِى الظُّلُمٰتُ وَالنُّوْرُ ە

qul man rabbu **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i quli **al**l*aa*hu qul afa**i**ttakha*dz*tum min duunihi awliy*aa*-a l*aa* yamlikuuna li-anfusihim naf'an wal*aa<*

Katakanlah (Muhammad), “Siapakah Tuhan langit dan bumi?” Katakanlah, “Allah.” Katakanlah, “Pantaskah kamu mengambil pelindung-pelindung selain Allah, padahal mereka tidak kuasa mendatangkan manfaat maupun menolak mudarat bagi dirinya sendiri?” Katakanla







13:17

# اَنْزَلَ مِنَ السَّمَاۤءِ مَاۤءً فَسَالَتْ اَوْدِيَةٌ ۢ بِقَدَرِهَا فَاحْتَمَلَ السَّيْلُ زَبَدًا رَّابِيًا ۗوَمِمَّا يُوْقِدُوْنَ عَلَيْهِ فِى النَّارِ ابْتِغَاۤءَ حِلْيَةٍ اَوْ مَتَاعٍ زَبَدٌ مِّثْلُهٗ ۗ كَذٰلِكَ يَضْرِبُ اللّٰهُ الْحَقَّ وَالْبَاطِلَ

anzala mina **al**ssam*aa*-i m*aa*-an fas*aa*lat awdiyatun biqadarih*aa* fa**i***h*tamala **al**ssaylu zabadan r*aa*biyan wamimm*aa* yuuqiduuna 'alayhi fii **al**nn*a*

Allah telah menurunkan air (hujan) dari langit, maka mengalirlah ia (air) di lembah-lembah menurut ukurannya, maka arus itu membawa buih yang mengambang. Dan dari apa (logam) yang mereka lebur dalam api untuk membuat perhiasan atau alat-alat, ada (pula) b







13:18

# لِلَّذِيْنَ اسْتَجَابُوْا لِرَبِّهِمُ الْحُسْنٰىۗ وَالَّذِيْنَ لَمْ يَسْتَجِيْبُوْا لَهٗ لَوْ اَنَّ لَهُمْ مَّا فِى الْاَرْضِ جَمِيْعًا وَّمِثْلَهٗ مَعَهٗ لَافْتَدَوْا بِهٖ ۗ اُولٰۤىِٕكَ لَهُمْ سُوْۤءُ الْحِسَابِ ەۙ وَمَأْوٰىهُمْ جَهَنَّمُ ۗوَبِئْسَ الْمِ

lilla*dz*iina istaj*aa*buu lirabbihimu **a**l*h*usn*aa* wa**a**lla*dz*iina lam yastajiibuu lahu law anna lahum m*aa* fii **a**l-ar*dh*i jamii'an wamitslahu ma'ahu la**i**

**Bagi orang-orang yang memenuhi seruan Tuhan, mereka (disediakan) balasan yang baik. Dan orang-orang yang tidak memenuhi seruan-Nya, sekiranya mereka memiliki semua yang ada di bumi dan (ditambah) sebanyak itu lagi, niscaya mereka akan menebus dirinya deng**









13:19

# ۞ اَفَمَنْ يَّعْلَمُ اَنَّمَآ اُنْزِلَ اِلَيْكَ مِنْ رَّبِّكَ الْحَقُّ كَمَنْ هُوَ اَعْمٰىۗ اِنَّمَا يَتَذَكَّرُ اُولُوا الْاَلْبَابِۙ

afaman ya'lamu annam*aa* unzila ilayka min rabbika **a**l*h*aqqu kaman huwa a'm*aa* innam*aa* yata*dz*akkaru uluu **a**l-alb*aa*b**i**

Maka apakah orang yang mengetahui bahwa apa yang diturunkan Tuhan kepadamu adalah kebenaran, sama dengan orang yang buta? Hanya orang berakal saja yang dapat mengambil pelajaran,

13:20

# الَّذِيْنَ يُوْفُوْنَ بِعَهْدِ اللّٰهِ وَلَا يَنْقُضُوْنَ الْمِيْثَاقَۙ

**al**la*dz*iina yuufuuna bi'ahdi **al**l*aa*hi wal*aa* yanqu*dh*uuna **a**lmiits*aa*q**a**

(yaitu) orang yang memenuhi janji Allah dan tidak melanggar perjanjian,

13:21

# وَالَّذِيْنَ يَصِلُوْنَ مَآ اَمَرَ اللّٰهُ بِهٖٓ اَنْ يُّوْصَلَ وَيَخْشَوْنَ رَبَّهُمْ وَيَخَافُوْنَ سُوْۤءَ الْحِسَابِ ۗ

wa**a**lla*dz*iina ya*sh*iluuna m*aa* amara **al**l*aa*hu bihi an yuu*sh*ala wayakhsyawna rabbahum wayakh*aa*fuuna suu-a **a**l*h*is*aa*b**i**

dan orang-orang yang menghubungkan apa yang diperintahkan Allah agar dihubungkan, dan mereka takut kepada Tuhannya dan takut kepada hisab yang buruk.

13:22

# وَالَّذِيْنَ صَبَرُوا ابْتِغَاۤءَ وَجْهِ رَبِّهِمْ وَاَقَامُوا الصَّلٰوةَ وَاَنْفَقُوْا مِمَّا رَزَقْنٰهُمْ سِرًّا وَّعَلَانِيَةً وَّيَدْرَءُوْنَ بِالْحَسَنَةِ السَّيِّئَةَ اُولٰۤىِٕكَ لَهُمْ عُقْبَى الدَّارِۙ

wa**a**lla*dz*iina *sh*abaruu ibtigh*aa*-a wajhi rabbihim wa-aq*aa*muu **al***shsh*al*aa*ta wa-anfaquu mimm*aa* razaqn*aa*hum sirran wa'al*aa*niyatan wayadrauuna bi**a**l<

Dan orang yang sabar karena mengharap keridaan Tuhannya, melaksanakan salat, dan menginfakkan sebagian rezeki yang Kami berikan kepada mereka, secara sembunyi atau terang-terangan serta menolak kejahatan dengan kebaikan; orang itulah yang men-dapat tempat

13:23

# جَنّٰتُ عَدْنٍ يَّدْخُلُوْنَهَا وَمَنْ صَلَحَ مِنْ اٰبَاۤىِٕهِمْ وَاَزْوَاجِهِمْ وَذُرِّيّٰتِهِمْ وَالْمَلٰۤىِٕكَةُ يَدْخُلُوْنَ عَلَيْهِمْ مِّنْ كُلِّ بَابٍۚ

jann*aa*tu 'adnin yadkhuluunah*aa* waman *sh*ala*h*a min *aa*b*aa*-ihim wa-azw*aa*jihim wa*dz*urriyy*aa*tihim wa**a**lmal*aa*-ikatu yadkhuluuna 'alayhim min kulli b*aa*b**in**

**(yaitu) surga-surga ‘Adn, mereka masuk ke dalamnya bersama dengan orang yang saleh dari nenek moyangnya, pasangan-pasangannya, dan anak cucunya, sedang para malaikat masuk ke tempat-tempat mereka dari semua pintu;**









13:24

# سَلٰمٌ عَلَيْكُمْ بِمَا صَبَرْتُمْ فَنِعْمَ عُقْبَى الدَّارِۗ

sal*aa*mun 'alaykum bim*aa* *sh*abartum fani'ma 'uqb*aa* **al**dd*aa*r**i**

(sambil mengucapkan), “Selamat sejahtera atasmu karena kesabaranmu.” Maka alangkah nikmatnya tempat kesudahan itu.

13:25

# وَالَّذِيْنَ يَنْقُضُوْنَ عَهْدَ اللّٰهِ مِنْ ۢ بَعْدِ مِيْثَاقِهٖ وَيَقْطَعُوْنَ مَآ اَمَرَ اللّٰهُ بِهٖٓ اَنْ يُّوْصَلَ وَيُفْسِدُوْنَ فِى الْاَرْضِۙ اُولٰۤىِٕكَ لَهُمُ اللَّعْنَةُ وَلَهُمْ سُوْۤءُ الدَّارِ

wa**a**lla*dz*iina yanqu*dh*uuna 'ahda **al**l*aa*hi min ba'di miits*aa*qihi wayaq*th*a'uuna m*aa* amara **al**l*aa*hu bihi an yuu*sh*ala wayufsiduuna fii **a**l-a

Dan orang-orang yang melanggar janji Allah setelah diikrarkannya, dan memutuskan apa yang diperintahkan Allah agar disambungkan dan berbuat kerusakan di bumi; mereka itu memperoleh kutukan dan tempat kediaman yang buruk (Jahanam).

13:26

# اَللّٰهُ يَبْسُطُ الرِّزْقَ لِمَنْ يَّشَاۤءُ وَيَقْدِرُ ۗوَفَرِحُوْا بِالْحَيٰوةِ الدُّنْيَاۗ وَمَا الْحَيٰوةُ الدُّنْيَا فِى الْاٰخِرَةِ اِلَّا مَتَاعٌ ࣖ

**al**l*aa*hu yabsu*th*u **al**rrizqa liman yasy*aa*u wayaqdiru wafari*h*uu bi**a**l*h*ay*aa*ti **al**dduny*aa* wam*aa* **a**l*h*ay*aa*tu

Allah melapangkan rezeki bagi siapa yang Dia kehendaki dan membatasi (bagi siapa yang Dia kehendaki). Mereka bergembira dengan kehidupan dunia, padahal kehidupan dunia hanyalah kesenangan (yang sedikit) dibanding kehidupan akhirat.

13:27

# وَيَقُوْلُ الَّذِيْنَ كَفَرُوْا لَوْلَآ اُنْزِلَ عَلَيْهِ اٰيَةٌ مِّنْ رَّبِّهٖۗ قُلْ اِنَّ اللّٰهَ يُضِلُّ مَنْ يَّشَاۤءُ وَيَهْدِيْٓ اِلَيْهِ مَنْ اَنَابَۖ

wayaquulu **al**la*dz*iina kafaruu lawl*aa* unzila 'alayhi *aa*yatun min rabbihi qul inna **al**l*aa*ha yu*dh*illu man yasy*aa*u wayahdii ilayhi man an*aa*b**a**

Dan orang-orang kafir berkata, “Mengapa tidak diturunkan kepadanya (Muhammad) tanda (mukjizat) dari Tuhannya?” Katakanlah (Muhammad), “Sesungguhnya Allah menyesatkan siapa yang Dia kehendaki dan memberi petunjuk orang yang bertobat kepada-Nya,”

13:28

# الَّذِيْنَ اٰمَنُوْا وَتَطْمَىِٕنُّ قُلُوْبُهُمْ بِذِكْرِ اللّٰهِ ۗ اَلَا بِذِكْرِ اللّٰهِ تَطْمَىِٕنُّ الْقُلُوْبُ ۗ

**al**la*dz*iina *aa*manuu wata*th*ma-innu quluubuhum bi*dz*ikri **al**l*aa*hi **a**l*aa* bi*dz*ikri **al**l*aa*hi ta*th*ma-innu **a**lquluub

(yaitu) orang-orang yang beriman dan hati mereka menjadi tenteram dengan mengingat Allah. Ingatlah, hanya dengan mengingat Allah hati menjadi tenteram.

13:29

# اَلَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِ طُوْبٰى لَهُمْ وَحُسْنُ مَاٰبٍ

**al**la*dz*iina *aa*manuu wa'amiluu **al***shshaa*li*haa*ti *th*uub*aa* lahum wa*h*usnu ma*aa*b**in**

Orang-orang yang beriman dan mengerjakan kebajikan, mereka mendapat kebahagiaan dan tempat kembali yang baik.

13:30

# كَذٰلِكَ اَرْسَلْنٰكَ فِيْٓ اُمَّةٍ قَدْ خَلَتْ مِنْ قَبْلِهَآ اُمَمٌ لِّتَتْلُوَا۟ عَلَيْهِمُ الَّذِيْٓ اَوْحَيْنَآ اِلَيْكَ وَهُمْ يَكْفُرُوْنَ بِالرَّحْمٰنِۗ قُلْ هُوَ رَبِّيْ لَآ اِلٰهَ اِلَّا هُوَۚ عَلَيْهِ تَوَكَّلْتُ وَاِلَيْهِ مَتَابِ

ka*dzaa*lika arsaln*aa*ka fii ummatin qad khalat min qablih*aa* umamun litatluwa 'alayhimu **al**la*dz*ii aw*h*ayn*aa* ilayka wahum yakfuruuna bi**al**rra*h*m*aa*ni qul huwa rabbii l*aa*

*Demikianlah, Kami telah mengutus engkau (Muhammad) kepada suatu umat yang sungguh sebelumnya telah berlalu beberapa umat, agar engkau bacakan kepada mereka (Al-Qur'an) yang Kami wahyukan kepadamu, padahal mereka ingkar kepada Tuhan Yang Maha Pengasih. Kat*









13:31

# وَلَوْ اَنَّ قُرْاٰنًا سُيِّرَتْ بِهِ الْجِبَالُ اَوْ قُطِّعَتْ بِهِ الْاَرْضُ اَوْ كُلِّمَ بِهِ الْمَوْتٰىۗ بَلْ لِّلّٰهِ الْاَمْرُ جَمِيْعًاۗ اَفَلَمْ يَا۟يْـَٔسِ الَّذِيْنَ اٰمَنُوْٓا اَنْ لَّوْ يَشَاۤءُ اللّٰهُ لَهَدَى النَّاسَ جَمِيْعًاۗ وَلَا يَزَال

walaw anna qur-*aa*nan suyyirat bihi **a**ljib*aa*lu aw qu*ththh*i'at bihi **a**l-ar*dh*u aw kullima bihi **a**lmawt*aa* bal lill*aa*hi **a**l-amru jamii'an afalam yay-asi <

Dan sekiranya ada suatu bacaan (Kitab Suci) yang dengan itu gunung-gunung dapat digoncangkan, atau bumi jadi terbelah, atau orang yang sudah mati dapat berbicara, (itulah Al-Qur'an). Sebenarnya segala urusan itu milik Allah. Maka tidakkah orang-orang yang

13:32

# وَلَقَدِ اسْتُهْزِئَ بِرُسُلٍ مِّنْ قَبْلِكَ فَاَمْلَيْتُ لِلَّذِيْنَ كَفَرُوْا ثُمَّ اَخَذْتُهُمْ فَكَيْفَ كَانَ عِقَابِ

walaqadi istuhzi-a birusulin min qablika fa-amlaytu lilla*dz*iina kafaruu tsumma akha*dz*tuhum fakayfa k*aa*na 'iq*aa*b**i**

Dan sesungguhnya beberapa rasul sebelum engkau (Muhammad) telah diperolok-olokkan, maka Aku beri tenggang waktu kepada orang-orang kafir itu, kemudian Aku binasakan mereka. Maka alangkah hebatnya siksaan-Ku itu!

13:33

# اَفَمَنْ هُوَ قَاۤىِٕمٌ عَلٰى كُلِّ نَفْسٍۢ بِمَا كَسَبَتْۚ وَجَعَلُوْا لِلّٰهِ شُرَكَاۤءَ ۗ قُلْ سَمُّوْهُمْۗ اَمْ تُنَبِّـُٔوْنَهٗ بِمَا لَا يَعْلَمُ فِى الْاَرْضِ اَمْ بِظَاهِرٍ مِّنَ الْقَوْلِ ۗبَلْ زُيِّنَ لِلَّذِيْنَ كَفَرُوْا مَكْرُهُمْ وَصُدُّوْا

afaman huwa q*aa*-imun 'al*aa* kulli nafsin bim*aa* kasabat waja'aluu lill*aa*hi syurak*aa*-a qul sammuuhum am tunabbi-uunahu bim*aa* l*aa* ya'lamu fii **a**l-ar*dh*i am bi*zhaa*hirin mina

Maka apakah Tuhan yang menjaga setiap jiwa terhadap apa yang diperbuatnya (sama dengan yang lain)? Mereka menjadikan sekutu-sekutu bagi Allah. Katakanlah, “Sebutkanlah sifat-sifat mereka itu.” Atau apakah kamu hendak memberitahukan kepada Allah apa yang t







13:34

# لَهُمْ عَذَابٌ فِى الْحَيٰوةِ الدُّنْيَا وَلَعَذَابُ الْاٰخِرَةِ اَشَقُّۚ وَمَا لَهُمْ مِّنَ اللّٰهِ مِنْ وَّاقٍ

lahum 'a*dzaa*bun fii **a**l*h*ay*aa*ti **al**dduny*aa* wala'a*dzaa*bu **a**l-*aa*khirati asyaqqu wam*aa* lahum mina **al**l*aa*hi min w*aa*q**in**

**Mereka mendapat siksaan dalam kehidupan dunia, dan azab akhirat pasti lebih keras. Tidak ada seorang pun yang melindungi mereka dari (azab) Allah.**









13:35

# ۞ مَثَلُ الْجَنَّةِ الَّتِيْ وُعِدَ الْمُتَّقُوْنَۗ تَجْرِيْ مِنْ تَحْتِهَا الْاَنْهٰرُۗ اُكُلُهَا دَاۤىِٕمٌ وَّظِلُّهَاۗ تِلْكَ عُقْبَى الَّذِيْنَ اتَّقَوْا ۖوَّعُقْبَى الْكٰفِرِيْنَ النَّارُ

matsalu **a**ljannati **al**latii wu'ida **a**lmuttaquuna tajrii min ta*h*tih*aa* **a**l-anh*aa*ru ukuluh*aa* d*aa*-imun wa*zh*illuh*aa* tilka 'uqb*aa* **al<**

Perumpamaan surga yang dijanjikan kepada orang yang bertakwa (ialah seperti taman), mengalir di bawahnya sungai-sungai; senantiasa berbuah dan teduh. Itulah tempat kesudahan bagi orang yang bertakwa; sedang tempat kesudahan bagi orang yang ingkar kepada T







13:36

# وَالَّذِيْنَ اٰتَيْنٰهُمُ الْكِتٰبَ يَفْرَحُوْنَ بِمَآ اُنْزِلَ اِلَيْكَ وَمِنَ الْاَحْزَابِ مَنْ يُّنْكِرُ بَعْضَهٗ ۗ قُلْ اِنَّمَآ اُمِرْتُ اَنْ اَعْبُدَ اللّٰهَ وَلَآ اُشْرِكَ بِهٖ ۗاِلَيْهِ اَدْعُوْا وَاِلَيْهِ مَاٰبِ

wa**a**lla*dz*iina *aa*tayn*aa*humu **a**lkit*aa*ba yafra*h*uuna bim*aa* unzila ilayka wamina **a**l-a*h*z*aa*bi man yunkiru ba'*dh*ahu qul innam*aa* umirtu an a'buda

Dan orang yang telah Kami berikan kitab kepada mereka bergembira dengan apa (kitab) yang diturunkan kepadamu (Muhammad), dan ada di antara golongan (Yahudi dan Nasrani), yang mengingkari sebagiannya. Katakanlah, “Aku hanya diperintah untuk menyembah Allah

13:37

# وَكَذٰلِكَ اَنْزَلْنٰهُ حُكْمًا عَرَبِيًّاۗ وَلَىِٕنِ اتَّبَعْتَ اَهْوَاۤءَهُمْ بَعْدَمَا جَاۤءَكَ مِنَ الْعِلْمِۙ مَا لَكَ مِنَ اللّٰهِ مِنْ وَّلِيٍّ وَّلَا وَاقٍ ࣖ

waka*dzaa*lika anzaln*aa*hu *h*ukman 'arabiyyan wala-ini ittaba'ta ahw*aa*-ahum ba'da m*aa* j*aa*-aka mina **a**l'ilmi m*aa* laka mina **al**l*aa*hi min waliyyin wal*aa* w*aa*q

Dan demikianlah Kami telah menurunkannya (Al-Qur'an) sebagai peraturan (yang benar) dalam bahasa Arab. Sekiranya engkau mengikuti keinginan mereka setelah datang pengetahuan kepadamu, maka tidak ada yang melindungi dan yang menolong engkau dari (siksaan)

13:38

# وَلَقَدْ اَرْسَلْنَا رُسُلًا مِّنْ قَبْلِكَ وَجَعَلْنَا لَهُمْ اَزْوَاجًا وَّذُرِّيَّةً ۗوَمَا كَانَ لِرَسُوْلٍ اَنْ يَّأْتِيَ بِاٰيَةٍ اِلَّا بِاِذْنِ اللّٰهِ ۗلِكُلِّ اَجَلٍ كِتَابٌ

walaqad arsaln*aa* rusulan min qablika waja'aln*aa* lahum azw*aa*jan wa*dz*urriyyatan wam*aa* k*aa*na lirasuulin an ya/tiya bi-*aa*yatin ill*aa* bi-i*dz*ni **al**l*aa*hi likulli ajalin kit*a*

Dan sungguh, Kami telah mengutus beberapa rasul sebelum engkau (Muhammad) dan Kami berikan kepada mereka istri-istri dan keturunan. Tidak ada hak bagi seorang rasul mendatangkan sesuatu bukti (mukjizat) melainkan dengan izin Allah. Untuk setiap masa ada K







13:39

# يَمْحُوا اللّٰهُ مَا يَشَاۤءُ وَيُثْبِتُ ۚوَعِنْدَهٗٓ اُمُّ الْكِتٰبِ

yam*h*uu **al**l*aa*hu m*aa* yasy*aa*u wayutsbitu wa'indahu ummu **a**lkit*aa*b**i**

Allah menghapus dan menetapkan apa yang Dia kehendaki. Dan di sisi-Nya terdapat Ummul-Kitab (Lauh Mahfuzh).

13:40

# وَاِنْ مَّا نُرِيَنَّكَ بَعْضَ الَّذِيْ نَعِدُهُمْ اَوْ نَتَوَفَّيَنَّكَ فَاِنَّمَا عَلَيْكَ الْبَلٰغُ وَعَلَيْنَا الْحِسَابُ

wa-in m*aa* nuriyannaka ba'*dh*a **al**la*dz*ii na'iduhum aw natawaffayannaka fa-innam*aa* 'alayka **a**lbal*aa*ghu wa'alayn*aa* **a**l*h*is*aa*b**u**

Dan sungguh jika Kami perlihatkan kepadamu (Muhammad) sebagian (siksaan) yang Kami ancamkan kepada mereka atau Kami wafatkan engkau, maka sesungguhnya tugasmu hanya menyampaikan saja, dan Kamilah yang memperhitungkan (amal mereka).

13:41

# اَوَلَمْ يَرَوْا اَنَّا نَأْتِى الْاَرْضَ نَنْقُصُهَا مِنْ اَطْرَافِهَاۗ وَاللّٰهُ يَحْكُمُ لَا مُعَقِّبَ لِحُكْمِهٖۗ وَهُوَ سَرِيْعُ الْحِسَابِ

awa lam yaraw ann*aa* na/tii **a**l-ar*dh*a nanqu*sh*uh*aa* min a*th*r*aa*fih*aa* wa**al**l*aa*hu ya*h*kumu l*aa* mu'aqqiba li*h*ukmihi wahuwa sarii'u **a**l*h*

Dan apakah mereka tidak melihat bahwa Kami mendatangi daerah-daerah (orang yang ingkar kepada Allah), lalu Kami kurangi (daerah-daerah) itu (sedikit demi sedikit) dari tepi-tepinya? Dan Allah menetapkan hukum (menurut kehendak-Nya), tidak ada yang dapat m







13:42

# وَقَدْ مَكَرَ الَّذِيْنَ مِنْ قَبْلِهِمْ فَلِلّٰهِ الْمَكْرُ جَمِيْعًا ۗيَعْلَمُ مَا تَكْسِبُ كُلُّ نَفْسٍۗ وَسَيَعْلَمُ الْكُفّٰرُ لِمَنْ عُقْبَى الدَّارِ

waqad makara **al**la*dz*iina min qablihim falill*aa*hi **a**lmakru jamii'an ya'lamu m*aa* taksibu kullu nafsin wasaya'lamu **a**lkuff*aa*ru liman 'uqb*aa* **al**dd*aa*r

Dan sungguh, orang sebelum mereka (kafir Mekah) telah mengadakan tipu daya, tetapi semua tipu daya itu dalam kekuasaan Allah. Dia mengetahui apa yang diusahakan oleh setiap orang, dan orang yang ingkar kepada Tuhan akan mengetahui untuk siapa tempat kesud

13:43

# وَيَقُوْلُ الَّذِيْنَ كَفَرُوْا لَسْتَ مُرْسَلًا ۗ قُلْ كَفٰى بِاللّٰهِ شَهِيْدًاۢ بَيْنِيْ وَبَيْنَكُمْۙ وَمَنْ عِنْدَهٗ عِلْمُ الْكِتٰبِ ࣖ

wayaquulu **al**la*dz*iina kafaruu lasta mursalan qul kaf*aa* bi**al**l*aa*hi syahiidan baynii wabaynakum waman 'indahu 'ilmu **a**lkit*aa*b**i**

Dan orang-orang kafir berkata, “Engkau (Muhammad) bukanlah seorang Rasul.” Katakanlah, “Cukuplah Allah dan orang yang menguasai ilmu Al-Kitab menjadi saksi antara aku dan kamu.”

<!--EndFragment-->