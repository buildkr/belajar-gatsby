---
title: (43) Az-Zukhruf - الزخرف
date: 2021-10-27T04:03:42.296Z
ayat: 43
description: "Jumlah Ayat: 89 / Arti: Perhiasan"
---
<!--StartFragment-->

43:1

# حٰمۤ ۚ

*haa*-miim

Ha Mim

43:2

# وَالْكِتٰبِ الْمُبِيْنِ ۙ

wa**a**lkit*aa*bi **a**lmubiin**i**

Demi Kitab (Al-Qur'an) yang jelas.

43:3

# اِنَّا جَعَلْنٰهُ قُرْاٰنًا عَرَبِيًّا لَّعَلَّكُمْ تَعْقِلُوْنَۚ

inn*aa* ja'aln*aa*hu qur-*aa*nan 'arabiyyan la'allakum ta'qiluunaa

Kami menjadikan Al-Qur'an dalam bahasa Arab agar kamu mengerti.

43:4

# وَاِنَّهٗ فِيْٓ اُمِّ الْكِتٰبِ لَدَيْنَا لَعَلِيٌّ حَكِيْمٌ ۗ

wa-innahu fii ummi **a**lkit*aa*bi ladayn*aa* la'aliyyun *h*akiim**un**

Dan sesungguhnya Al-Qur'an itu dalam Ummul Kitab (Lauh Mahfuzh) di sisi Kami, benar-benar (bernilai) tinggi dan penuh hikmah.

43:5

# اَفَنَضْرِبُ عَنْكُمُ الذِّكْرَ صَفْحًا اَنْ كُنْتُمْ قَوْمًا مُّسْرِفِيْنَ

afana*dh*ribu 'ankumu **al***dzdz*ikra *sh*af*h*an an kuntum qawman musrifiin**a**

Maka apakah Kami akan berhenti menurunkan ayat-ayat (sebagai peringatan) Al-Qur'an kepadamu, karena kamu kaum yang melampaui batas?

43:6

# وَكَمْ اَرْسَلْنَا مِنْ نَّبِيٍّ فِى الْاَوَّلِيْنَ

wakam arsaln*aa* min nabiyyin fii **a**l-awwaliin**a**

Dan betapa banyak nabi-nabi yang telah Kami utus kepada umat-umat yang terdahulu.

43:7

# وَمَا يَأْتِيْهِمْ مِّنْ نَّبِيٍّ اِلَّا كَانُوْا بِهٖ يَسْتَهْزِءُوْنَ

wam*aa* ya/tiihim min nabiyyin ill*aa* k*aa*nuu bihi yastahzi-uun**a**

Dan setiap kali seorang nabi datang kepada mereka, mereka selalu memperolok-olokkannya.

43:8

# فَاَهْلَكْنَٓا اَشَدَّ مِنْهُمْ بَطْشًا وَّمَضٰى مَثَلُ الْاَوَّلِيْنَ

fa-ahlakn*aa* asyadda minhum ba*th*syan wama*daa* matsalu **a**l-awwaliin**a**

Karena itu Kami binasakan orang-orang yang lebih besar kekuatannya di antara mereka dan telah berlalu contoh umat-umat terdahulu.

43:9

# وَلَىِٕنْ سَاَلْتَهُمْ مَّنْ خَلَقَ السَّمٰوٰتِ وَالْاَرْضَ لَيَقُوْلُنَّ خَلَقَهُنَّ الْعَزِيْزُ الْعَلِيْمُۙ

wala-in sa-altahum man khalaqa **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*a layaquulunna khalaqahunna **a**l'aziizu **a**l'aliim**u**

Dan jika kamu tanyakan kepada mereka, “Siapakah yang menciptakan langit dan bumi?” Pastilah mereka akan menjawab, “Semuanya diciptakan oleh Yang Mahaperkasa, Maha Mengetahui.”

43:10

# الَّذِيْ جَعَلَ لَكُمُ الْاَرْضَ مَهْدًا وَّجَعَلَ لَكُمْ فِيْهَا سُبُلًا لَّعَلَّكُمْ تَهْتَدُوْنَ ۚ

**al**la*dz*ii ja'ala lakumu **a**l-ar*dh*a mahdan waja'ala lakum fiih*aa* subulan la'allakum tahtaduun**a**

Yang menjadikan bumi sebagai tempat menetap bagimu dan Dia menjadikan jalan-jalan di atas bumi untukmu agar kamu mendapat petunjuk.

43:11

# وَالَّذِيْ نَزَّلَ مِنَ السَّمَاۤءِ مَاۤءًۢ بِقَدَرٍۚ فَاَنْشَرْنَا بِهٖ بَلْدَةً مَّيْتًا ۚ كَذٰلِكَ تُخْرَجُوْنَ

wa**a**lla*dz*ii nazzala mina **al**ssam*aa*-i m*aa*-an biqadarin fa-ansyarn*aa* bihi baldatan maytan ka*dzaa*lika tukhrajuun**a**

Dan yang menurunkan air dari langit menurut ukuran (yang diperlukan) lalu dengan air itu Kami hidupkan negeri yang mati (tandus). Seperti itulah kamu akan dikeluarkan (dari kubur).

43:12

# وَالَّذِيْ خَلَقَ الْاَزْوَاجَ كُلَّهَا وَجَعَلَ لَكُمْ مِّنَ الْفُلْكِ وَالْاَنْعَامِ مَا تَرْكَبُوْنَۙ

wa**a**lla*dz*ii khalaqa **a**l-azw*aa*ja kullah*aa* waja'ala lakum mina **a**lfulki wa**a**l-an'*aa*mi m*aa* tarkabuun**a**

Dan yang menciptakan semua berpasang-pasangan dan menjadikan kapal untukmu dan hewan ternak yang kamu tunggangi.

43:13

# لِتَسْتَوٗا عَلٰى ظُهُوْرِهٖ ثُمَّ تَذْكُرُوْا نِعْمَةَ رَبِّكُمْ اِذَا اسْتَوَيْتُمْ عَلَيْهِ وَتَقُوْلُوْا سُبْحٰنَ الَّذِيْ سَخَّرَ لَنَا هٰذَا وَمَا كُنَّا لَهٗ مُقْرِنِيْنَۙ

litastawuu 'al*aa* *zh*uhuurihi tsumma ta*dz*kuruu ni'mata rabbikum i*dzaa* istawaytum 'alayhi wataquuluu sub*haa*na **al**la*dz*ii sakhkhara lan*aa* h*aadzaa* wam*aa* kunn*aa* lahu muqriniin<

Agar kamu duduk di atas punggungnya kemudian kamu ingat nikmat Tuhanmu apabila kamu telah duduk di atasnya; dan agar kamu mengucapkan, “Maha-suci (Allah) yang telah menundukkan semua ini bagi kami padahal kami sebelumnya tidak mampu menguasainya,

43:14

# وَاِنَّآ اِلٰى رَبِّنَا لَمُنْقَلِبُوْنَ

wa-inn*aa* il*aa* rabbin*aa* lamunqalibuun**a**

dan sesungguhnya kami akan kembali kepada Tuhan kami.”

43:15

# وَجَعَلُوْا لَهٗ مِنْ عِبَادِهٖ جُزْءًا ۗاِنَّ الْاِنْسَانَ لَكَفُوْرٌ مُّبِيْنٌ ۗ ࣖ

waja'aluu lahu min 'ib*aa*dihi juz-an inna **a**l-ins*aa*na lakafuurun mubiin**un**

Dan mereka menjadikan sebagian dari hamba-hamba-Nya sebagai bagian dari-Nya. Sungguh, manusia itu pengingkar (nikmat Tuhan) yang nyata.

43:16

# اَمِ اتَّخَذَ مِمَّا يَخْلُقُ بَنٰتٍ وَّاَصْفٰىكُمْ بِالْبَنِيْنَ ۗ

ami ittakha*dz*a mimm*aa* yakhluqu ban*aa*tin wa-a*sh*f*aa*kum bi**a**lbaniin**a**

Pantaskah Dia mengambil anak perempuan dari yang diciptakan-Nya dan memberikan anak laki-laki kepadamu?

43:17

# وَاِذَا بُشِّرَ اَحَدُهُمْ بِمَا ضَرَبَ لِلرَّحْمٰنِ مَثَلًا ظَلَّ وَجْهُهٗ مُسْوَدًّا وَّهُوَ كَظِيْمٌ

wa-i*dzaa* busysyira a*h*aduhum bim*aa* *dh*araba li**l**rra*h*m*aa*ni matsalan *zh*alla wajhuhu muswaddan wahuwa ka*zh*iim**un**

Dan apabila salah seorang di antara mereka diberi kabar gembira dengan apa (kelahiran anak perempuan) yang dijadikan sebagai perumpamaan bagi (Allah) Yang Maha Pengasih, jadilah wajahnya hitam pekat, karena menahan sedih (dan marah).

43:18

# اَوَمَنْ يُّنَشَّؤُا فِى الْحِلْيَةِ وَهُوَ فِى الْخِصَامِ غَيْرُ مُبِيْنٍ

awaman yunasysyau fii **a**l*h*ilyati wahuwa fii **a**lkhi*shaa*mi ghayru mubiin**in**

Dan apakah patut (menjadi anak Allah) orang yang dibesarkan sebagai perhiasan sedang dia tidak mampu memberi alasan yang tegas dan jelas dalam pertengkaran.

43:19

# وَجَعَلُوا الْمَلٰۤىِٕكَةَ الَّذِيْنَ هُمْ عِبٰدُ الرَّحْمٰنِ اِنَاثًا ۗ اَشَهِدُوْا خَلْقَهُمْ ۗسَتُكْتَبُ شَهَادَتُهُمْ وَيُسْٔـَلُوْنَ

waja'aluu **a**lmal*aa*-ikata **al**la*dz*iina hum 'ib*aa*du **al**rra*h*m*aa*ni in*aa*tsan asyahiduu khalqahum satuktabu syah*aa*datuhum wayus-aluun**a**

Dan mereka menjadikan malaikat-malaikat hamba-hamba (Allah) Yang Maha Pengasih itu sebagai jenis perempuan. Apakah mereka menyaksikan penciptaan (malaikat-malaikat itu)? Kelak akan dituliskan kesaksian mereka dan akan dimintakan pertanggungjawaban.

43:20

# وَقَالُوْا لَوْ شَاۤءَ الرَّحْمٰنُ مَا عَبَدْنٰهُمْ ۗمَا لَهُمْ بِذٰلِكَ مِنْ عِلْمٍ اِنْ هُمْ اِلَّا يَخْرُصُوْنَۗ

waq*aa*luu law sy*aa*-a **al**rra*h*m*aa*nu m*aa* 'abadn*aa*hum m*aa* lahum bi*dzaa*lika min 'ilmin in hum ill*aa* yakhru*sh*uun**a**

Dan mereka berkata, “Sekiranya (Allah) Yang Maha Pengasih menghendaki, tentulah kami tidak menyembah mereka (malaikat).” Mereka tidak mempunyai ilmu sedikit pun tentang itu. Tidak lain mereka hanyalah menduga-duga belaka.

43:21

# اَمْ اٰتَيْنٰهُمْ كِتٰبًا مِّنْ قَبْلِهٖ فَهُمْ بِهٖ مُسْتَمْسِكُوْنَ

am *aa*tayn*aa*hum kit*aa*ban min qablihi fahum bihi mustamsikuun**a**

Atau apakah pernah Kami berikan sebuah kitab kepada mereka sebelumnya, lalu mereka berpegang (pada kitab itu)?

43:22

# بَلْ قَالُوْٓا اِنَّا وَجَدْنَآ اٰبَاۤءَنَا عَلٰٓى اُمَّةٍ وَّاِنَّا عَلٰٓى اٰثٰرِهِمْ مُّهْتَدُوْنَ

bal q*aa*luu inn*aa* wajadn*aa* *aa*b*aa*-an*aa* 'al*aa* ummatin wa-inn*aa* 'al*aa* *aa*ts*aa*rihim muhtaduun**a**

Bahkan mereka berkata, “Sesungguhnya kami mendapati nenek moyang kami menganut suatu agama, dan kami mendapat petunjuk untuk mengikuti jejak mereka.”

43:23

# وَكَذٰلِكَ مَآ اَرْسَلْنَا مِنْ قَبْلِكَ فِيْ قَرْيَةٍ مِّنْ نَّذِيْرٍۙ اِلَّا قَالَ مُتْرَفُوْهَآ ۙاِنَّا وَجَدْنَآ اٰبَاۤءَنَا عَلٰٓى اُمَّةٍ وَّاِنَّا عَلٰٓى اٰثٰرِهِمْ مُّقْتَدُوْنَ

waka*dzaa*lika m*aa* arsaln*aa* min qablika fii qaryatin min na*dz*iirin ill*aa* q*aa*la mutrafuuh*aa* inn*aa* wajadn*aa* *aa*b*aa*-an*aa* 'al*aa* ummatin wa-inn*aa* 'al*aa* *aa<*

Dan demikian juga ketika Kami mengutus seorang pemberi peringatan sebelum engkau (Muhammad) dalam suatu negeri, orang-orang yang hidup mewah (di negeri itu) selalu berkata, “Sesungguhnya kami mendapati nenek moyang kami menganut suatu (agama) dan sesunggu







43:24

# ۞ قٰلَ اَوَلَوْ جِئْتُكُمْ بِاَهْدٰى مِمَّا وَجَدْتُّمْ عَلَيْهِ اٰبَاۤءَكُمْۗ قَالُوْٓا اِنَّا بِمَآ اُرْسِلْتُمْ بِهٖ كٰفِرُوْنَ

q*aa*la awa law ji/tukum bi-ahd*aa* mimm*aa* wajadtum 'alayhi *aa*b*aa*-akum q*aa*luu inn*aa* bim*aa* ursiltum bihi k*aa*firuun**a**

(Rasul itu) berkata, “Apakah (kamu akan mengikutinya juga) sekalipun aku membawa untukmu (agama) yang lebih baik daripada apa yang kamu peroleh dari (agama) yang dianut nenek moyangmu.” Mereka menjawab, “Sesungguhnya kami mengingkari (agama) yang kamu dip

43:25

# فَانْتَقَمْنَا مِنْهُمْ فَانْظُرْ كَيْفَ كَانَ عَاقِبَةُ الْمُكَذِّبِيْنَ ࣖ

fa**i**ntaqamn*aa* minhum fa**u**n*zh*ur kayfa k*aa*na '*aa*qibatu **a**lmuka*dzdz*ibiin**a**

Lalu Kami binasakan mereka, maka perhatikanlah bagaimana kesudahan orang-orang yang mendustakan (kebenaran).

43:26

# وَاِذْ قَالَ اِبْرٰهِيْمُ لِاَبِيْهِ وَقَوْمِهٖٓ اِنَّنِيْ بَرَاۤءٌ مِّمَّا تَعْبُدُوْنَۙ

wa-i*dz* q*aa*la ibr*aa*hiimu li-abiihi waqawmihi innanii bar*aa*un mimm*aa* ta'buduun**a**

Dan (ingatlah) ketika Ibrahim berkata kepada ayahnya dan kaumnya, “Sesungguhnya aku berlepas diri dari apa yang kamu sembah,

43:27

# اِلَّا الَّذِيْ فَطَرَنِيْ فَاِنَّهٗ سَيَهْدِيْنِ

ill*aa* **al**la*dz*ii fa*th*aranii fa-innahu sayahdiin**i**

kecuali (kamu menyembah) Allah yang menciptakanku; karena sungguh, Dia akan memberi petunjuk kepadaku.”

43:28

# وَجَعَلَهَا كَلِمَةً ۢ بَاقِيَةً فِيْ عَقِبِهٖ لَعَلَّهُمْ يَرْجِعُوْنَۗ

waja'alah*aa* kalimatan b*aa*qiyatan fii 'aqibihi la'allahum yarji'uun**a**

Dan (Ibrahim) menjadikan (kalimat tauhid) itu kalimat yang kekal pada keturunannya agar mereka kembali (kepada kalimat tauhid itu).

43:29

# بَلْ مَتَّعْتُ هٰٓؤُلَاۤءِ وَاٰبَاۤءَهُمْ حَتّٰى جَاۤءَهُمُ الْحَقُّ وَرَسُوْلٌ مُّبِيْنٌ

bal matta'tu h*aa*ul*aa*-i wa*aa*b*aa*-ahum *h*att*aa* j*aa*-ahumu **a**l*h*aqqu warasuulun mubiin**un**

Bahkan Aku telah memberikan kenikmatan hidup kepada mereka dan nenek moyang mereka sampai kebenaran (Al-Qur'an) datang kepada mereka beserta seorang Rasul yang memberi penjelasan.

43:30

# وَلَمَّا جَاۤءَهُمُ الْحَقُّ قَالُوْا هٰذَا سِحْرٌ وَّاِنَّا بِهٖ كٰفِرُوْنَ

walamm*aa* j*aa*-ahumu **a**l*h*aqqu q*aa*luu h*aadzaa* si*h*run wa-inn*aa* bihi k*aa*firuun**a**

Tetapi ketika kebenaran (Al-Qur'an) itu datang kepada mereka, mereka berkata, “Ini adalah sihir dan sesungguhnya kami mengingkarinya.”

43:31

# وَقَالُوْا لَوْلَا نُزِّلَ هٰذَا الْقُرْاٰنُ عَلٰى رَجُلٍ مِّنَ الْقَرْيَتَيْنِ عَظِيْمٍ

waq*aa*luu lawl*aa* nuzzila h*aadzaa* **a**lqur-*aa*nu 'al*aa* rajulin mina **a**lqaryatayni 'a*zh*iim**un**

Dan mereka (juga) berkata, “Mengapa Al-Qur'an ini tidak diturunkan kepada orang besar (kaya dan berpengaruh) dari salah satu dua negeri ini (Mekah dan Taif)?”

43:32

# اَهُمْ يَقْسِمُوْنَ رَحْمَتَ رَبِّكَۗ نَحْنُ قَسَمْنَا بَيْنَهُمْ مَّعِيْشَتَهُمْ فِى الْحَيٰوةِ الدُّنْيَاۙ وَرَفَعْنَا بَعْضَهُمْ فَوْقَ بَعْضٍ دَرَجٰتٍ لِّيَتَّخِذَ بَعْضُهُمْ بَعْضًا سُخْرِيًّا ۗوَرَحْمَتُ رَبِّكَ خَيْرٌ مِّمَّا يَجْمَعُوْنَ

ahum yaqsimuuna ra*h*mata rabbika na*h*nu qasamn*aa* baynahum ma'iisyatahum fii **a**l*h*ay*aa*ti **al**dduny*aa* warafa'n*aa* ba'*dh*ahum fawqa ba'*dh*in daraj*aa*tin liyattakhi<

Apakah mereka yang membagi-bagi rahmat Tuhanmu? Kamilah yang menentukan penghidupan mereka dalam kehidupan dunia, dan Kami telah meninggikan sebagian mereka atas sebagian yang lain beberapa derajat, agar sebagian mereka dapat memanfaatkan sebagian yang la

43:33

# وَلَوْلَآ اَنْ يَّكُوْنَ النَّاسُ اُمَّةً وَّاحِدَةً لَّجَعَلْنَا لِمَنْ يَّكْفُرُ بِالرَّحْمٰنِ لِبُيُوْتِهِمْ سُقُفًا مِّنْ فِضَّةٍ وَّمَعَارِجَ عَلَيْهَا يَظْهَرُوْنَۙ

walawl*aa* an yakuuna **al**nn*aa*su ummatan w*aah*idatan laja'aln*aa* liman yakfuru bi**al**rra*h*m*aa*ni libuyuutihim suqufan min fi*dhdh*atin wama'*aa*rija 'alayh*aa* ya*zh*har

Dan sekiranya bukan karena menghindarkan manusia menjadi umat yang satu (dalam kekafiran), pastilah sudah Kami buatkan bagi orang-orang yang kafir kepada (Allah) Yang Maha Pengasih, loteng-loteng rumah mereka dari perak, demikian pula tangga-tangga yang m

43:34

# وَلِبُيُوْتِهِمْ اَبْوَابًا وَّسُرُرًا عَلَيْهَا يَتَّكِـُٔوْنَۙ

walibuyuutihim abw*aa*ban wasururan 'alayh*aa* yattaki-uun**a**

dan (Kami buatkan pula) pintu-pintu (perak) bagi rumah-rumah mereka, dan (begitu pula) dipan-dipan tempat mereka bersandar,

43:35

# وَزُخْرُفًاۗ وَاِنْ كُلُّ ذٰلِكَ لَمَّا مَتَاعُ الْحَيٰوةِ الدُّنْيَا ۗوَالْاٰخِرَةُ عِنْدَ رَبِّكَ لِلْمُتَّقِيْنَ ࣖ

wazukhrufan wa-in kullu *dzaa*lika lamm*aa* mat*aa*'u **a**l*h*ay*aa*ti **al**dduny*aa* wa**a**l-*aa*khiratu 'inda rabbika lilmuttaqiin**a**

dan (Kami buatkan pula) perhiasan-perhiasan dari emas. Dan semuanya itu tidak lain hanyalah kesenangan kehidupan dunia, sedangkan kehidupan akhirat di sisi Tuhanmu disediakan bagi orang-orang yang bertakwa.

43:36

# وَمَنْ يَّعْشُ عَنْ ذِكْرِ الرَّحْمٰنِ نُقَيِّضْ لَهٗ شَيْطٰنًا فَهُوَ لَهٗ قَرِيْنٌ

waman ya'syu 'an *dz*ikri **al**rra*h*m*aa*ni nuqayyi*dh* lahu syay*thaa*nan fahuwa lahu qariin**un**

Dan barangsiapa berpaling dari pengajaran Allah Yang Maha Pengasih (Al-Qur'an), Kami biarkan setan (menyesatkannya) dan menjadi teman karibnya.

43:37

# وَاِنَّهُمْ لَيَصُدُّوْنَهُمْ عَنِ السَّبِيْلِ وَيَحْسَبُوْنَ اَنَّهُمْ مُّهْتَدُوْنَ

wa-innahum laya*sh*udduunahum 'ani **al**ssabiili waya*h*sabuuna annahum muhtaduun**a**

Dan sungguh, mereka (setan-setan itu) benar-benar menghalang-halangi mereka dari jalan yang benar, sedang mereka menyangka bahwa mereka mendapat petunjuk.

43:38

# حَتّٰىٓ اِذَا جَاۤءَنَا قَالَ يٰلَيْتَ بَيْنِيْ وَبَيْنَكَ بُعْدَ الْمَشْرِقَيْنِ فَبِئْسَ الْقَرِيْنُ

*h*att*aa* i*dzaa* j*aa*-an*aa* q*aa*la y*aa* layta baynii wabaynaka bu'da **a**lmasyriqayni fabi/sa **a**lqariin**u**

Sehingga apabila orang–orang yang berpaling itu datang kepada Kami (pada hari Kiamat) dia berkata, “Wahai! Sekiranya (jarak) antara aku dan kamu seperti jarak antara timur dan barat! Memang (setan itu) teman yang paling jahat (bagi manusia).”

43:39

# وَلَنْ يَّنْفَعَكُمُ الْيَوْمَ اِذْ ظَّلَمْتُمْ اَنَّكُمْ فِى الْعَذَابِ مُشْتَرِكُوْنَ

walan yanfa'akumu **a**lyawma i*dz* *zh*alamtum annakum fii **a**l'a*dzaa*bi musytarikuun**a**

Dan (harapanmu itu) sekali-kali tidak akan memberi manfaat kepadamu pada hari itu karena kamu telah menzalimi (dirimu sendiri). Sesungguhnya kamu pantas bersama-sama dalam azab itu.

43:40

# اَفَاَنْتَ تُسْمِعُ الصُّمَّ اَوْ تَهْدِى الْعُمْيَ وَمَنْ كَانَ فِيْ ضَلٰلٍ مُّبِيْنٍ

afa-anta tusmi'u **al***shsh*umma aw tahdii **a**l'umya waman k*aa*na fii *dh*al*aa*lin mubiin**in**

Maka apakah engkau (Muhammad) dapat menjadikan orang yang tuli bisa mendengar, atau (dapatkah) engkau memberi petunjuk kepada orang yang buta (hatinya) dan kepada orang yang tetap dalam kesesatan yang nyata?

43:41

# فَاِمَّا نَذْهَبَنَّ بِكَ فَاِنَّا مِنْهُمْ مُّنْتَقِمُوْنَۙ

fa-imm*aa* na*dz*habanna bika fa-inn*aa* minhum muntaqimuun**a**

Maka sungguh, sekiranya Kami mewafatkanmu (sebelum engkau mencapai kemenangan), maka sesungguhnya Kami akan tetap memberikan azab kepada mereka (di akhirat),

43:42

# اَوْ نُرِيَنَّكَ الَّذِيْ وَعَدْنٰهُمْ فَاِنَّا عَلَيْهِمْ مُّقْتَدِرُوْنَ

aw nuriyannaka **al**la*dz*ii wa'adn*aa*hum fa-inn*aa* 'alayhim muqtadiruun**a**

atau Kami perlihatkan kepadamu (azab) yang telah Kami ancamkan kepada mereka. Maka sungguh, Kami berkuasa atas mereka.

43:43

# فَاسْتَمْسِكْ بِالَّذِيْٓ اُوْحِيَ اِلَيْكَ ۚاِنَّكَ عَلٰى صِرَاطٍ مُّسْتَقِيْمٍ

fa**i**stamsik bi**a**lla*dz*ii uu*h*iya ilayka innaka 'al*aa* *sh*ir*aath*in mustaqiim**in**

Maka berpegang teguhlah engkau kepada (agama) yang telah diwahyukan kepadamu. Sungguh, engkau berada di jalan yang lurus.

43:44

# وَاِنَّهٗ لَذِكْرٌ لَّكَ وَلِقَوْمِكَ ۚوَسَوْفَ تُسْٔـَلُوْنَ

wa-innahu la*dz*ikrun laka waliqawmika wasawfa tus-aluun**a**

Dan sungguh, Al-Qur'an itu benar-benar suatu peringatan bagimu dan bagi kaummu, dan kelak kamu akan diminta pertanggungjawaban.

43:45

# وَسْٔـَلْ مَنْ اَرْسَلْنَا مِنْ قَبْلِكَ مِنْ رُّسُلِنَآ ۖ اَجَعَلْنَا مِنْ دُوْنِ الرَّحْمٰنِ اٰلِهَةً يُّعْبَدُوْنَ ࣖ

wa**i**s-al man arsaln*aa* min qablika min rusulin*aa* aja'aln*aa* min duuni **al**rra*h*m*aa*ni *aa*lihatan yu'baduun**a**

Dan tanyakanlah (Muhammad) kepada rasul-rasul Kami yang telah Kami utus sebelum engkau, “Apakah Kami menentukan tuhan-tuhan selain (Allah) Yang Maha Pengasih untuk disembah?”

43:46

# وَلَقَدْ اَرْسَلْنَا مُوْسٰى بِاٰيٰتِنَآ اِلٰى فِرْعَوْنَ وَمَلَا۟ىِٕهٖ فَقَالَ اِنِّيْ رَسُوْلُ رَبِّ الْعٰلَمِيْنَ

walaqad arsaln*aa* muus*aa* bi-*aa*y*aa*tin*aa* il*aa* fir'awna wamala-ihi faq*aa*la innii rasuulu rabbi **a**l'*aa*lamiin**a**

Dan sungguh, Kami telah mengutus Musa dengan membawa mukjizat-mukjizat Kami kepada Fir’aun dan pemuka-pemuka kaumnya. Maka dia (Musa) berkata, “Sesungguhnya aku adalah utusan dari Tuhan seluruh alam.”

43:47

# فَلَمَّا جَاۤءَهُمْ بِاٰيٰتِنَآ اِذَا هُمْ مِّنْهَا يَضْحَكُوْنَ

falamm*aa* j*aa*-ahum bi-*aa*y*aa*tin*aa* i*dzaa* hum minh*aa* ya*dh*akuun**a**

Maka ketika dia (Musa) datang kepada mereka membawa mukjizat-mukjizat Kami, seketika itu mereka menertawakannya.

43:48

# وَمَا نُرِيْهِمْ مِّنْ اٰيَةٍ اِلَّا هِيَ اَكْبَرُ مِنْ اُخْتِهَاۗ وَاَخَذْنٰهُمْ بِالْعَذَابِ لَعَلَّهُمْ يَرْجِعُوْنَ

wam*aa* nuriihim min *aa*yatin ill*aa* hiya akbaru min ukhtih*aa* wa-akha*dz*n*aa*hum bi**a**l'a*dzaa*bi la'allahum yarji'uun**a**

Dan tidaklah Kami perlihatkan suatu mukjizat kepada mereka kecuali (mukjizat itu) lebih besar dari mukjizat-mukjizat (yang sebelumnya). Dan Kami timpakan kepada mereka azab agar mereka kembali (ke jalan yang benar).

43:49

# وَقَالُوْا يٰٓاَيُّهَ السّٰحِرُ ادْعُ لَنَا رَبَّكَ بِمَا عَهِدَ عِنْدَكَۚ اِنَّنَا لَمُهْتَدُوْنَ

waq*aa*luu y*aa* ayyuh*aa* **al**s*aah*iru ud'u lan*aa* rabbaka bim*aa* 'ahida 'indaka innan*aa* lamuhtaduun**a**

Dan mereka berkata, “Wahai pesihir! Berdoalah kepada Tuhanmu untuk (melepaskan) kami sesuai dengan apa yang telah dijanjikan-Nya kepadamu; sesungguhnya kami (jika doamu dikabulkan) akan menjadi orang yang mendapat petunjuk.”

43:50

# فَلَمَّا كَشَفْنَا عَنْهُمُ الْعَذَابَ اِذَا هُمْ يَنْكُثُوْنَ

falamm*aa* kasyafn*aa* 'anhumu **a**l'a*dzaa*ba i*dzaa* hum yankutsuun**a**

Maka ketika Kami hilangkan azab itu dari mereka, seketika itu (juga) mereka ingkar janji.

43:51

# وَنَادٰى فِرْعَوْنُ فِيْ قَوْمِهٖ قَالَ يٰقَوْمِ اَلَيْسَ لِيْ مُلْكُ مِصْرَ وَهٰذِهِ الْاَنْهٰرُ تَجْرِيْ مِنْ تَحْتِيْۚ اَفَلَا تُبْصِرُوْنَۗ

wan*aa*d*aa* fir'awnu fii qawmihi q*aa*la y*aa* qawmi alaysa lii mulku mi*sh*ra wah*aadz*ihi al-anh*aa*ru tajrii min ta*h*tii afal*aa* tub*sh*iruun**a**

Dan Fir‘aun berseru kepada kaumnya (seraya) berkata, “Wahai kaumku! Bukankah kerajaan Mesir itu milikku dan (bukankah) sungai-sungai ini mengalir di bawahku; apakah kamu tidak melihat?

43:52

# اَمْ اَنَا۠ خَيْرٌ مِّنْ هٰذَا الَّذِيْ هُوَ مَهِيْنٌ ەۙ وَّلَا يَكَادُ يُبِيْنُ

am an*aa* khayrun min h*aadzaa* **al**la*dz*ii huwa mahiinun wal*aa* yak*aa*du yubiin**u**

Bukankah aku lebih baik dari orang (Musa) yang hina ini dan yang hampir tidak dapat menjelaskan (perkataannya)?

43:53

# فَلَوْلَٓا اُلْقِيَ عَلَيْهِ اَسْوِرَةٌ مِّنْ ذَهَبٍ اَوْ جَاۤءَ مَعَهُ الْمَلٰۤىِٕكَةُ مُقْتَرِنِيْنَ

falawl*aa* ulqiya 'alayhi aswiratun min *dz*ahabin aw j*aa*-a ma'ahu **a**lmal*aa*-ikatu muqtariniin**a**

Maka mengapa dia (Musa) tidak dipakaikan gelang dari emas atau malaikat datang bersama-sama dia untuk mengiringkannya?”

43:54

# فَاسْتَخَفَّ قَوْمَهٗ فَاَطَاعُوْهُ ۗاِنَّهُمْ كَانُوْا قَوْمًا فٰسِقِيْنَ

fa**i**stakhaffa qawmahu fa-a*thaa*'uuhu innahum k*aa*nuu qawman f*aa*siqiin**a**

Maka (Fir‘aun) dengan perkataan itu telah mempengaruhi kaumnya, sehingga mereka patuh kepadanya. Sungguh, mereka adalah kaum yang fasik.

43:55

# فَلَمَّآ اٰسَفُوْنَا انْتَقَمْنَا مِنْهُمْ فَاَغْرَقْنٰهُمْ اَجْمَعِيْنَۙ

falamm*aa* *aa*safuun*aa* intaqamn*aa* minhum fa-aghraqn*aa*hum ajma'iin**a**

Maka ketika mereka membuat Kami murka, Kami hukum mereka, lalu Kami tenggelamkan mereka semuanya (di laut),

43:56

# فَجَعَلْنٰهُمْ سَلَفًا وَّمَثَلًا لِّلْاٰخِرِيْنَ ࣖ

faja'aln*aa*hum salafan wamatsalan lil-*aa*khiriin**a**

maka Kami jadikan mereka sebagai (kaum) terdahulu dan pelajaran bagi orang-orang yang kemudian.

43:57

# وَلَمَّا ضُرِبَ ابْنُ مَرْيَمَ مَثَلًا اِذَا قَوْمُكَ مِنْهُ يَصِدُّوْنَ

walamm*aa* *dh*uriba ibnu maryama matsalan i*dzaa* qawmuka minhu ya*sh*idduun**a**

Dan ketika putra Maryam (Isa) dijadikan perumpamaan, tiba-tiba kaummu (Suku Quraisy) bersorak karenanya.

43:58

# وَقَالُوْٓا ءَاٰلِهَتُنَا خَيْرٌ اَمْ هُوَ ۗمَا ضَرَبُوْهُ لَكَ اِلَّا جَدَلًا ۗبَلْ هُمْ قَوْمٌ خَصِمُوْنَ

waq*aa*luu a*aa*lihatun*aa* khayrun am huwa m*aa* *dh*arabuuhu laka ill*aa* jadalan bal hum qawmun kha*sh*imuun**a**

Dan mereka berkata, “Manakah yang lebih baik tuhan-tuhan kami atau dia (Isa)?” Mereka tidak memberikan (perumpamaan itu) kepadamu melainkan dengan maksud membantah saja, sebenarnya mereka adalah kaum yang suka bertengkar.

43:59

# اِنْ هُوَ اِلَّا عَبْدٌ اَنْعَمْنَا عَلَيْهِ وَجَعَلْنٰهُ مَثَلًا لِّبَنِيْٓ اِسْرَاۤءِيْلَ ۗ

in huwa ill*aa* 'abdun an'amn*aa* 'alayhi waja'aln*aa*hu matsalan libanii isr*aa*-iil**a**

Dia (Isa) tidak lain hanyalah seorang hamba yang Kami berikan nikmat (kenabian) kepadanya dan Kami jadikan dia sebagai contoh pelajaran bagi Bani Israil.

43:60

# وَلَوْ نَشَاۤءُ لَجَعَلْنَا مِنْكُمْ مَّلٰۤىِٕكَةً فِى الْاَرْضِ يَخْلُفُوْنَ

walaw nasy*aa*u laja'aln*aa* minkum mal*aa*-ikatan fii **a**l-ar*dh*i yakhlufuun**a**

Dan sekiranya Kami menghendaki, niscaya ada di antara kamu yang Kami jadikan malaikat-malaikat (yang turun temurun) sebagai pengganti kamu di bumi.

43:61

# وَاِنَّهٗ لَعِلْمٌ لِّلسَّاعَةِ فَلَا تَمْتَرُنَّ بِهَا وَاتَّبِعُوْنِۗ هٰذَا صِرَاطٌ مُّسْتَقِيْمٌ

wa-innahu la'ilmun li**l**ss*aa*'ati fal*aa* tamtarunna bih*aa* wa**i**ttabi'uuni h*aadzaa* *sh*ir*aath*un mustaqiim**un**

Dan sungguh, dia (Isa) itu benar-benar menjadi pertanda akan datangnya hari Kiamat. Karena itu, janganlah kamu ragu-ragu tentang (Kiamat) itu dan ikutilah Aku. Inilah jalan yang lurus.

43:62

# وَلَا يَصُدَّنَّكُمُ الشَّيْطٰنُۚ اِنَّهٗ لَكُمْ عَدُوٌّ مُّبِيْنٌ

wal*aa* ya*sh*uddannakumu **al**sysyay*thaa*nu innahu lakum 'aduwwun mubiin**un**

Dan janganlah kamu sekali-kali dipalingkan oleh setan; sungguh, setan itu musuh yang nyata bagimu.

43:63

# وَلَمَّا جَاۤءَ عِيْسٰى بِالْبَيِّنٰتِ قَالَ قَدْ جِئْتُكُمْ بِالْحِكْمَةِ وَلِاُبَيِّنَ لَكُمْ بَعْضَ الَّذِيْ تَخْتَلِفُوْنَ فِيْهِۚ فَاتَّقُوا اللّٰهَ وَاَطِيْعُوْنِ

walamm*aa* j*aa*-a 'iis*aa* bi**a**lbayyin*aa*ti q*aa*la qad ji/tukum bi**a**l*h*ikmati wali-ubayyina lakum ba'*dh*a **al**la*dz*ii takhtalifuuna fiihi fa**i**ttaq

Dan ketika Isa datang membawa keterangan, dia berkata, “Sungguh, aku datang kepadamu dengan membawa hikmah dan untuk menjelaskan kepadamu sebagian dari apa yang kamu perselisihkan, maka bertakwalah kepada Allah dan taatlah kepadaku.

43:64

# اِنَّ اللّٰهَ هُوَ رَبِّيْ وَرَبُّكُمْ فَاعْبُدُوْهُۗ هٰذَا صِرَاطٌ مُّسْتَقِيْمٌ

inna **al**l*aa*ha huwa rabbii warabbukum fa**u**'buduuhu h*aadzaa* *sh*ir*aath*un mustaqiim**un**

Sungguh Allah, Dia Tuhanku dan Tuhanmu, maka sembahlah Dia. Ini adalah jalan yang lurus.”

43:65

# فَاخْتَلَفَ الْاَحْزَابُ مِنْۢ بَيْنِهِمْ ۚفَوَيْلٌ لِّلَّذِيْنَ ظَلَمُوْا مِنْ عَذَابِ يَوْمٍ اَلِيْمٍ

fa**i**khtalafa **a**l-a*h*z*aa*bu min baynihim fawaylun lilla*dz*iina *zh*alamuu min 'a*dzaa*bi yawmin **a**liim**in**

Tetapi golongan-golongan (yang ada) saling berselisih di antara mereka; maka celakalah orang-orang yang zalim karena azab pada hari yang pedih (Kiamat).

43:66

# هَلْ يَنْظُرُوْنَ اِلَّا السَّاعَةَ اَنْ تَأْتِيَهُمْ بَغْتَةً وَّهُمْ لَا يَشْعُرُوْنَ

hal yan*zh*uruuna ill*aa* **al**ss*aa*'ata an ta/tiyahum baghtatan wahum l*aa* yasy'uruun**a**

Apakah mereka hanya menunggu saja kedatangan hari Kiamat yang datang kepada mereka secara mendadak sedang mereka tidak menyadarinya?

43:67

# اَلْاَخِلَّاۤءُ يَوْمَىِٕذٍۢ بَعْضُهُمْ لِبَعْضٍ عَدُوٌّ اِلَّا الْمُتَّقِيْنَ ۗ ࣖ

al-akhill*aa*u yawma-i*dz*in ba'*dh*uhum liba'*dh*in 'aduwwun ill*aa* **a**lmuttaqiin**a**

Teman-teman karib pada hari itu saling bermusuhan satu sama lain, kecuali mereka yang bertakwa.

43:68

# يٰعِبَادِ لَاخَوْفٌ عَلَيْكُمُ الْيَوْمَ وَلَآ اَنْتُمْ تَحْزَنُوْنَۚ

y*aa* 'ib*aa*di l*aa* khawfun 'alaykumu **a**lyawma wal*aa* antum ta*h*zanuun**a**

”Wahai hamba-hamba-Ku! Tidak ada ketakutan bagimu pada hari itu dan tidak pula kamu bersedih hati.

43:69

# اَلَّذِيْنَ اٰمَنُوْا بِاٰيٰتِنَا وَكَانُوْا مُسْلِمِيْنَۚ

**al**la*dz*iina *aa*manuu bi-*aa*y*aa*tin*aa* wak*aa*nuu muslimiin**a**

(Yaitu) orang-orang yang beriman kepada ayat-ayat Kami dan mereka berserah diri.

43:70

# اُدْخُلُوا الْجَنَّةَ اَنْتُمْ وَاَزْوَاجُكُمْ تُحْبَرُوْنَ

udkhuluu **a**ljannata antum wa-azw*aa*jukum tu*h*baruun**a**

Masuklah kamu ke dalam surga, kamu dan pasanganmu akan digembirakan.”

43:71

# يُطَافُ عَلَيْهِمْ بِصِحَافٍ مِّنْ ذَهَبٍ وَّاَكْوَابٍ ۚوَفِيْهَا مَا تَشْتَهِيْهِ الْاَنْفُسُ وَتَلَذُّ الْاَعْيُنُ ۚوَاَنْتُمْ فِيْهَا خٰلِدُوْنَۚ

yu*thaa*fu 'alayhim bi*sh*i*haa*fin min *dz*ahabin wa-akw*aa*bin wafiih*aa* m*aa* tasytahiihi **a**l-anfusu watala*dzdz*u **a**l-a'yunu wa-antum fiih*aa* kh*aa*liduun

Kepada mereka diedarkan piring-piring dan gelas-gelas dari emas, dan di dalam surga itu terdapat apa yang diingini oleh hati dan segala yang sedap (dipandang) mata. Dan kamu kekal di dalamnya.

43:72

# وَتِلْكَ الْجَنَّةُ الَّتِيْٓ اُوْرِثْتُمُوْهَا بِمَا كُنْتُمْ تَعْمَلُوْنَ

watilka **a**ljannatu **al**latii uuritstumuuh*aa* bim*aa* kuntum ta'maluun**a**

Dan itulah surga yang diwariskan kepada kamu disebabkan amal perbuatan yang telah kamu kerjakan.

43:73

# لَكُمْ فِيْهَا فَاكِهَةٌ كَثِيْرَةٌ مِّنْهَا تَأْكُلُوْنَ

lakum fiih*aa* f*aa*kihatun katsiiratun minh*aa* ta/kuluun**a**

Di dalam surga itu terdapat banyak buah-buahan untukmu yang sebagiannya kamu makan.

43:74

# اِنَّ الْمُجْرِمِيْنَ فِيْ عَذَابِ جَهَنَّمَ خٰلِدُوْنَۖ

inna **a**lmujrimiina fii 'a*dzaa*bi jahannama kh*aa*liduun**a**

Sungguh, orang-orang yang berdosa itu kekal di dalam azab neraka Jahanam.

43:75

# لَا يُفَتَّرُ عَنْهُمْ وَهُمْ فِيْهِ مُبْلِسُوْنَ ۚ

l*aa* yufattaru 'anhum wahum fiihi mublisuun**a**

Tidak diringankan (azab) itu dari mereka, dan mereka berputus asa di dalamnya.

43:76

# وَمَا ظَلَمْنٰهُمْ وَلٰكِنْ كَانُوْا هُمُ الظّٰلِمِيْنَ

wam*aa* *zh*alamn*aa*hum wal*aa*kin k*aa*nuu humu **al***zhzhaa*limiin**a**

Dan tidaklah Kami menzalimi mereka, tetapi merekalah yang menzalimi diri mereka sendiri.

43:77

# وَنَادَوْا يٰمٰلِكُ لِيَقْضِ عَلَيْنَا رَبُّكَۗ قَالَ اِنَّكُمْ مَّاكِثُوْنَ

wan*aa*daw y*aa* m*aa*liku liyaq*dh*i 'alayn*aa* rabbuka q*aa*la innakum m*aa*kitsuun**a**

Dan mereka berseru, “Wahai (Malaikat) Malik! Biarlah Tuhanmu mematikan kami saja.” Dia menjawab, “Sungguh, kamu akan tetap tinggal (di neraka ini).”

43:78

# لَقَدْ جِئْنٰكُمْ بِالْحَقِّ وَلٰكِنَّ اَكْثَرَكُمْ لِلْحَقِّ كٰرِهُوْنَ

laqad ji/n*aa*kum bi**a**l*h*aqqi wal*aa*kinna aktsarakum lil*h*aqqi k*aa*rihuun**a**

Sungguh, Kami telah datang membawa kebenaran kepada kamu tetapi kebanyakan di antara kamu benci pada kebenaran itu.

43:79

# اَمْ اَبْرَمُوْٓا اَمْرًا فَاِنَّا مُبْرِمُوْنَۚ

am abramuu amran fa-inn*aa* mubrimuun**a**

Ataukah mereka telah merencanakan suatu tipu daya (jahat), maka sesungguhnya Kami telah berencana (mengatasi tipu daya mereka).

43:80

# اَمْ يَحْسَبُوْنَ اَنَّا لَا نَسْمَعُ سِرَّهُمْ وَنَجْوٰىهُمْ ۗ بَلٰى وَرُسُلُنَا لَدَيْهِمْ يَكْتُبُوْنَ

am ya*h*sabuuna ann*aa* l*aa* nasma'u sirrahum wanajw*aa*hum bal*aa* warusulun*aa* ladayhim yaktubuun**a**

Ataukah mereka mengira, bahwa Kami tidak mendengar rahasia dan bisikan-bisikan mereka? Sebenarnya (Kami mendengar), dan utusan-utusan Kami (malaikat) selalu mencatat di sisi mereka.

43:81

# قُلْ اِنْ كَانَ لِلرَّحْمٰنِ وَلَدٌ ۖفَاَنَا۠ اَوَّلُ الْعٰبِدِيْنَ

qul in k*aa*na li**l**rra*h*m*aa*ni waladun fa-an*aa* awwalu **a**l'*aa*bidiin**a**

Katakanlah (Muhammad), “Jika benar Tuhan Yang Maha Pengasih mempunyai anak, maka akulah orang yang mula-mula memuliakan (anak itu).

43:82

# سُبْحٰنَ رَبِّ السَّمٰوٰتِ وَالْاَرْضِ رَبِّ الْعَرْشِ عَمَّا يَصِفُوْنَ

sub*haa*na rabbi **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i rabbi **a**l'arsyi 'amm*aa* ya*sh*ifuun**a**

Mahasuci Tuhan pemilik langit dan bumi, Tuhan pemilik ‘Arsy, dari apa yang mereka sifatkan itu.”

43:83

# فَذَرْهُمْ يَخُوْضُوْا وَيَلْعَبُوْا حَتّٰى يُلٰقُوْا يَوْمَهُمُ الَّذِيْ يُوْعَدُوْنَ

fa*dz*arhum yakhuu*dh*uu wayal'abuu *h*att*aa* yul*aa*quu yawmahumu **al**la*dz*ii yuu'aduun**a**

Maka biarkanlah mereka tenggelam (dalam kesesatan) dan bermain-main sampai mereka menemui hari yang dijanjikan kepada mereka.

43:84

# وَهُوَ الَّذِيْ فِى السَّمَاۤءِ اِلٰهٌ وَّ فِى الْاَرْضِ اِلٰهٌ ۗوَهُوَ الْحَكِيْمُ الْعَلِيْمُ

wahuwa **al**la*dz*ii fii **al**ssam*aa*-i il*aa*hun wafii **a**l-ar*dh*i il*aa*hun wahuwa **a**l*h*akiimu **a**l'aliim**u**

Dan Dialah Tuhan (yang disembah) di langit dan Tuhan (yang disembah) di bumi dan Dialah Yang Mahabijaksana, Maha Mengetahui.

43:85

# وَتَبٰرَكَ الَّذِيْ لَهٗ مُلْكُ السَّمٰوٰتِ وَالْاَرْضِ وَمَا بَيْنَهُمَا ۚوَعِنْدَهٗ عِلْمُ السَّاعَةِۚ وَاِلَيْهِ تُرْجَعُوْنَ

watab*aa*raka **al**la*dz*ii lahu mulku **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i wam*aa* baynahum*aa* wa'indahu 'ilmu **al**ss*aa*'ati wa-ilayhi turja'uun**a**

**Dan Mahasuci (Allah) yang memiliki kerajaan langit dan bumi, dan apa yang ada di antara keduanya; dan di sisi-Nyalah ilmu tentang hari Kiamat dan hanya kepada-Nyalah kamu dikembalikan.**









43:86

# وَلَا يَمْلِكُ الَّذِيْنَ يَدْعُوْنَ مِنْ دُوْنِهِ الشَّفَاعَةَ اِلَّا مَنْ شَهِدَ بِالْحَقِّ وَهُمْ يَعْلَمُوْنَ

wal*aa* yamliku **al**la*dz*iina yad'uuna min duunihi **al**sysyaf*aa*'ata ill*aa* man syahida bi**a**l*h*aqqi wahum ya'lamuun**a**

Dan orang-orang yang menyeru kepada selain Allah tidak mendapat syafaat (pertolongan di akhirat); kecuali orang yang mengakui yang hak (tauhid) dan mereka meyakini.

43:87

# وَلَىِٕنْ سَاَلْتَهُمْ مَّنْ خَلَقَهُمْ لَيَقُوْلُنَّ اللّٰهُ فَاَنّٰى يُؤْفَكُوْنَۙ

wala-in sa-altahum man khalaqahum layaquulunna **al**l*aa*hu fa-ann*aa* yu/fakuun**a**

Dan jika engkau bertanya kepada mereka, “Siapakah yang menciptakan mereka, niscaya mereka menjawab, “Allah,” jadi bagaimana mereka dapat dipalingkan (dari menyembah Allah),”

43:88

# وَقِيْلِهٖ يٰرَبِّ اِنَّ هٰٓؤُلَاۤءِ قَوْمٌ لَّا يُؤْمِنُوْنَۘ

waqiilihi y*aa* rabbi inna h*aa*ul*aa*-i qawmun l*aa* yu/minuun**a**

dan (Allah mengetahui) ucapannya (Muhammad), “Ya Tuhanku, sesungguhnya mereka itu adalah kaum yang tidak beriman.”

43:89

# فَاصْفَحْ عَنْهُمْ وَقُلْ سَلٰمٌۗ فَسَوْفَ يَعْلَمُوْنَ ࣖ

fa**i***sh*fa*h* 'anhum waqul sal*aa*mun fasawfa ya'lamuun**a**

Maka berpalinglah dari mereka dan katakanlah, “Salam (selamat tinggal).” Kelak mereka akan mengetahui (nasib mereka yang buruk).

<!--EndFragment-->