---
title: (95) At-Tin - التين
date: 2021-10-27T04:31:18.550Z
ayat: 95
description: "Jumlah Ayat: 8 / Arti: Buah Tin"
---
<!--StartFragment-->

95:1

# وَالتِّيْنِ وَالزَّيْتُوْنِۙ

wa**al**ttiini wa**al**zzaytuun**i**

Demi (buah) Tin dan (buah) Zaitun,

95:2

# وَطُوْرِ سِيْنِيْنَۙ

wa*th*uuri siiniin**a**

demi gunung Sinai,

95:3

# وَهٰذَا الْبَلَدِ الْاَمِيْنِۙ

wah*aadzaa* **a**lbaladi **a**l-amiin**i**

dan demi negeri (Mekah) yang aman ini.

95:4

# لَقَدْ خَلَقْنَا الْاِنْسَانَ فِيْٓ اَحْسَنِ تَقْوِيْمٍۖ

laqad khalaqn*aa* **a**l-ins*aa*na fii a*h*sani taqwiim**in**

Sungguh, Kami telah menciptakan manusia dalam bentuk yang sebaik-baiknya,

95:5

# ثُمَّ رَدَدْنٰهُ اَسْفَلَ سَافِلِيْنَۙ

tsumma radadn*aa*hu asfala s*aa*filiin**a**

kemudian Kami kembalikan dia ke tempat yang serendah-rendahnya,

95:6

# اِلَّا الَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِ فَلَهُمْ اَجْرٌ غَيْرُ مَمْنُوْنٍۗ

ill*aa* **al**la*dz*iina *aa*manuu wa'amiluu **al***shshaa*li*haa*ti falahum ajrun ghayru mamnuun**in**

kecuali orang-orang yang beriman dan mengerjakan kebajikan; maka mereka akan mendapat pahala yang tidak ada putus-putusnya.

95:7

# فَمَا يُكَذِّبُكَ بَعْدُ بِالدِّيْنِۗ

fam*aa* yuka*dzdz*ibuka ba'du bi**al**ddiin**i**

Maka apa yang menyebabkan (mereka) mendustakanmu (tentang) hari pembalasan setelah (adanya keterangan-keterangan) itu?

95:8

# اَلَيْسَ اللّٰهُ بِاَحْكَمِ الْحٰكِمِيْنَ ࣖ

alaysa **al**l*aa*hu bi-a*h*kami **a**l*haa*kimiin**a**

Bukankah Allah hakim yang paling adil?

<!--EndFragment-->