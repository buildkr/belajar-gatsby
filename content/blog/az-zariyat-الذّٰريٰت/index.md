---
title: (51) Az-Zariyat - الذّٰريٰت
date: 2021-10-27T04:10:41.945Z
ayat: 51
description: "Jumlah Ayat: 60 / Arti: Angin yang Menerbangkan"
---
<!--StartFragment-->

51:1

# وَالذّٰرِيٰتِ ذَرْوًاۙ

wa**al***dzdzaa*riy*aa*ti *dz*arw*aa***n**

Demi (angin) yang menerbangkan debu,

51:2

# فَالْحٰمِلٰتِ وِقْرًاۙ

fa**a**l*haa*mil*aa*ti wiqr*aa***n**

dan awan yang mengandung (hujan),

51:3

# فَالْجٰرِيٰتِ يُسْرًاۙ

fa**a**lj*aa*riy*aa*ti yusr*aa***n**

dan (kapal-kapal) yang berlayar dengan mudah,

51:4

# فَالْمُقَسِّمٰتِ اَمْرًاۙ

fa**a**lmuqassim*aa*ti amr*aa***n**

dan (malaikat-malaikat) yang membagi-bagi urusan,

51:5

# اِنَّمَا تُوْعَدُوْنَ لَصَادِقٌۙ

innam*aa* tuu'aduuna la*shaa*diq**un**

sungguh, apa yang dijanjikan kepadamu pasti benar,

51:6

# وَّاِنَّ الدِّيْنَ لَوَاقِعٌۗ

wa-inna **al**ddiina law*aa*qi'**un**

dan sungguh, (hari) pembalasan pasti terjadi.

51:7

# وَالسَّمَاۤءِ ذَاتِ الْحُبُكِۙ

wa**al**ssam*aa*-i *dzaa*ti **a**l*h*ubuk**i**

Demi langit yang mempunyai jalan-jalan,

51:8

# اِنَّكُمْ لَفِيْ قَوْلٍ مُّخْتَلِفٍۙ

innakum lafii qawlin mukhtalif**in**

sungguh, kamu benar-benar dalam keadaan berbeda-beda pendapat,

51:9

# يُّؤْفَكُ عَنْهُ مَنْ اُفِكَۗ

yu/faku 'anhu man ufik**a**

dipalingkan darinya (Al-Qur'an dan Rasul) orang yang dipalingkan.

51:10

# قُتِلَ الْخَرَّاصُوْنَۙ

qutila **a**lkharr*aas*uun**a**

Terkutuklah orang-orang yang banyak berdusta,

51:11

# الَّذِيْنَ هُمْ فِيْ غَمْرَةٍ سَاهُوْنَۙ

**al**la*dz*iina hum fii ghamratin s*aa*huun**a**

(yaitu) orang-orang yang terbenam dalam kebodohan dan kelalaian,

51:12

# يَسْـَٔلُوْنَ اَيَّانَ يَوْمُ الدِّيْنِۗ

yas-aluuna ayy*aa*na yawmu **al**ddiin**i**

mereka bertanya, “Kapankah hari pembalasan itu?”

51:13

# يَوْمَ هُمْ عَلَى النَّارِ يُفْتَنُوْنَ

yawma hum 'al*aa* **al**nn*aa*ri yuftanuun**a**

(Hari pembalasan itu ialah) pada hari (ketika) mereka diazab di dalam api neraka.

51:14

# ذُوْقُوْا فِتْنَتَكُمْۗ هٰذَا الَّذِيْ كُنْتُمْ بِهٖ تَسْتَعْجِلُوْنَ

*dz*uuquu fitnatakum h*aadzaa* **al**la*dz*ii kuntum bihi tasta'jiluun**a**

(Dikatakan kepada mereka), “Rasakanlah azabmu ini. Inilah azab yang dahulu kamu minta agar disegerakan.”

51:15

# اِنَّ الْمُتَّقِيْنَ فِيْ جَنّٰتٍ وَّعُيُوْنٍۙ

inna **a**lmuttaqiina fii jann*aa*tin wa'uyuun**in**

Sesungguhnya orang-orang yang bertakwa berada di dalam taman-taman (surga) dan mata air,

51:16

# اٰخِذِيْنَ مَآ اٰتٰىهُمْ رَبُّهُمْ ۗ اِنَّهُمْ كَانُوْا قَبْلَ ذٰلِكَ مُحْسِنِيْنَۗ

*aa*khi*dz*iina m*aa* *aa*t*aa*hum rabbuhum innahum k*aa*nuu qabla *dzaa*lika mu*h*siniin**a**

mereka mengambil apa yang diberikan Tuhan kepada mereka. Sesungguhnya mereka sebelum itu (di dunia) adalah orang-orang yang berbuat baik;

51:17

# كَانُوْا قَلِيْلًا مِّنَ الَّيْلِ مَا يَهْجَعُوْنَ

k*aa*nuu qaliilan mina **al**layli m*aa* yahja'uun**a**

mereka sedikit sekali tidur pada waktu malam;

51:18

# وَبِالْاَسْحَارِ هُمْ يَسْتَغْفِرُوْنَ

wabi**a**l-as*haa*ri hum yastaghfiruun**a**

dan pada akhir malam mereka memohon ampunan (kepada Allah).

51:19

# وَفِيْٓ اَمْوَالِهِمْ حَقٌّ لِّلسَّاۤىِٕلِ وَالْمَحْرُوْمِ

wafii amw*aa*lihim *h*aqqun li**l**ss*aa*-ili wa**a**lma*h*ruum**i**

Dan pada harta benda mereka ada hak untuk orang miskin yang meminta dan orang miskin yang tidak meminta.

51:20

# وَفِى الْاَرْضِ اٰيٰتٌ لِّلْمُوْقِنِيْنَۙ

wafii **a**l-ar*dh*i *aa*y*aa*tun lilmuuqiniin**a**

Dan di bumi terdapat tanda-tanda (kebesaran Allah) bagi orang-orang yang yakin,

51:21

# وَفِيْٓ اَنْفُسِكُمْ ۗ اَفَلَا تُبْصِرُوْنَ

wafii anfusikum afal*aa* tub*sh*iruun**a**

dan (juga) pada dirimu sendiri. Maka apakah kamu tidak memperhatikan?

51:22

# وَفِى السَّمَاۤءِ رِزْقُكُمْ وَمَا تُوْعَدُوْنَ

wafii **al**ssam*aa*-i rizqukum wam*aa* tuu'aduun**a**

Dan di langit terdapat (sebab-sebab) rezekimu dan apa yang dijanjikan kepadamu.

51:23

# فَوَرَبِّ السَّمَاۤءِ وَالْاَرْضِ اِنَّهٗ لَحَقٌّ مِّثْلَ مَآ اَنَّكُمْ تَنْطِقُوْنَ ࣖ

fawarabbi **al**ssam*aa*-i wa**a**l-ar*dh*i innahu la*h*aqqun mitsla m*aa* annakum tan*th*iquun**a**

Maka demi Tuhan langit dan bumi, sungguh, apa yang dijanjikan itu pasti terjadi seperti apa yang kamu ucapkan.

51:24

# هَلْ اَتٰىكَ حَدِيْثُ ضَيْفِ اِبْرٰهِيْمَ الْمُكْرَمِيْنَۘ

hal at*aa*ka *h*adiitsu *dh*ayfi ibr*aa*hiima **a**lmukramiin**a**

Sudahkah sampai kepadamu (Muhammad) cerita tamu Ibrahim (malaikat-malaikat) yang dimuliakan?

51:25

# اِذْ دَخَلُوْا عَلَيْهِ فَقَالُوْا سَلٰمًا ۗقَالَ سَلٰمٌۚ قَوْمٌ مُّنْكَرُوْنَ

i*dz* dakhaluu 'alayhi faq*aa*luu sal*aa*man q*aa*la sal*aa*mun qawmun munkaruun**a**

(Ingatlah) ketika mereka masuk ke tempatnya lalu mengucapkan, “Salaman” (salam), Ibrahim menjawab, “Salamun” (salam). (Mereka itu) orang-orang yang belum dikenalnya.

51:26

# فَرَاغَ اِلٰٓى اَهْلِهٖ فَجَاۤءَ بِعِجْلٍ سَمِيْنٍۙ

far*aa*gha il*aa* ahlihi faj*aa*-a bi'ijlin samiin**in**

Maka diam-diam dia (Ibrahim) pergi menemui keluarganya, kemudian dibawanya daging anak sapi gemuk (yang dibakar),

51:27

# فَقَرَّبَهٗٓ اِلَيْهِمْۚ قَالَ اَلَا تَأْكُلُوْنَ

faqarrabahu ilayhim q*aa*la **a**l*aa* ta/kuluun**a**

lalu dihidangkannya kepada mereka (tetapi mereka tidak mau makan). Ibrahim berkata, “Mengapa tidak kamu makan.”

51:28

# فَاَوْجَسَ مِنْهُمْ خِيْفَةً ۗقَالُوْا لَا تَخَفْۗ وَبَشَّرُوْهُ بِغُلٰمٍ عَلِيْمٍ

fa-awjasa minhum khiifatan q*aa*luu l*aa* takhaf wabasysyaruuhu bighul*aa*min 'aliim**in**

Maka dia (Ibrahim) merasa takut terhadap mereka. Mereka berkata, “Janganlah kamu takut,” dan mereka memberi kabar gembira kepadanya dengan (kelahiran) seorang anak yang alim (Ishak).

51:29

# فَاَقْبَلَتِ امْرَاَتُهٗ فِيْ صَرَّةٍ فَصَكَّتْ وَجْهَهَا وَقَالَتْ عَجُوْزٌ عَقِيْمٌ

fa-aqbalati imra-atuhu fii *sh*arratin fa*sh*akkat wajhah*aa* waq*aa*lat 'ajuuzun 'aqiim**un**

Kemudian istrinya datang memekik (tercengang) lalu menepuk wajahnya sendiri seraya berkata, “(Aku ini) seorang perempuan tua yang mandul.”

51:30

# قَالُوْا كَذٰلِكِۙ قَالَ رَبُّكِ ۗاِنَّهٗ هُوَ الْحَكِيْمُ الْعَلِيْمُ ۔

q*aa*luu ka*dzaa*liki q*aa*la rabbuki innahu huwa **a**l*h*akiimu **a**l'aliim**u**

Mereka berkata, “Demikianlah Tuhanmu berfirman. Sungguh, Dialah Yang Mahabijaksana, Maha Mengetahui.”

51:31

# قَالَ فَمَا خَطْبُكُمْ اَيُّهَا الْمُرْسَلُوْنَۚ

q*aa*la fam*aa* kha*th*bukum ayyuh*aa* **a**lmursaluun**a**

Dia (Ibrahim) berkata, “Apakah urusanmu yang penting wahai para utusan?”

51:32

# قَالُوْٓ اِنَّآ اُرْسِلْنَآ اِلٰى قَوْمٍ مُّجْرِمِيْنَۙ

q*aa*luu inn*aa* ursiln*aa* il*aa* qawmin mujrimiin**a**

Mereka menjawab, “Sesungguhnya kami diutus kepada kaum yang berdosa (kaum Lut),

51:33

# لِنُرْسِلَ عَلَيْهِمْ حِجَارَةً مِّنْ طِيْنٍۙ

linursila 'alayhim *h*ij*aa*ratan min *th*iin**in**

agar Kami menimpa mereka dengan batu-batu dari tanah (yang keras),

51:34

# مُّسَوَّمَةً عِنْدَ رَبِّكَ لِلْمُسْرِفِيْنَ

musawwamatan 'inda rabbika lilmusrifiin**a**

yang ditandai dari Tuhanmu untuk (membinasakan) orang-orang yang melampaui batas.”

51:35

# فَاَخْرَجْنَا مَنْ كَانَ فِيْهَا مِنَ الْمُؤْمِنِيْنَۚ

fa-akhrajn*aa* man k*aa*na fiih*aa* mina **a**lmu/miniin**a**

Lalu Kami keluarkan orang-orang yang beriman yang berada di dalamnya (negeri kaum Lut) itu.

51:36

# فَمَا وَجَدْنَا فِيْهَا غَيْرَ بَيْتٍ مِّنَ الْمُسْلِمِيْنَۚ

fam*aa* wajadn*aa* fiih*aa* ghayra baytin mina **a**lmuslimiin**a**

Maka Kami tidak mendapati di dalamnya (negeri itu), kecuali sebuah rumah dari orang-orang Muslim (Lut).

51:37

# وَتَرَكْنَا فِيْهَآ اٰيَةً لِّلَّذِيْنَ يَخَافُوْنَ الْعَذَابَ الْاَلِيْمَۗ

watarakn*aa* fiih*aa* *aa*yatan lilla*dz*iina yakh*aa*fuuna **a**l'a*dzaa*ba **a**l-aliim**a**

Dan Kami tinggalkan padanya (negeri itu) suatu tanda bagi orang-orang yang takut kepada azab yang pedih.

51:38

# وَفِيْ مُوْسٰىٓ اِذْ اَرْسَلْنٰهُ اِلٰى فِرْعَوْنَ بِسُلْطٰنٍ مُّبِيْنٍ

wafii muus*aa* i*dz* arsa**l**n*aa*hu il*aa* fir'awna bisul*thaa*nin mubiin**in**

Dan pada Musa (terdapat tanda-tanda kekuasaan Allah) ketika Kami mengutusnya kepada Fir‘aun dengan membawa mukjizat yang nyata.

51:39

# فَتَوَلّٰى بِرُكْنِهٖ وَقَالَ سٰحِرٌ اَوْ مَجْنُوْنٌ

fatawall*aa* biruknihi waq*aa*la s*aah*irun aw majnuun**un**

Tetapi dia (Fir‘aun) bersama bala tentaranya berpaling dan berkata, “Dia adalah seorang pesihir atau orang gila.”

51:40

# فَاَخَذْنٰهُ وَجُنُوْدَهٗ فَنَبَذْنٰهُمْ فِى الْيَمِّ وَهُوَ مُلِيْمٌۗ

fa-akha*dz*n*aa*hu wajunuudahu fanaba*dz*n*aa*hum fii **a**lyammi wahuwa muliim**un**

Maka Kami siksa dia beserta bala tentaranya, lalu Kami lemparkan mereka ke dalam laut, dalam keadaan tercela.

51:41

# وَفِيْ عَادٍ اِذْ اَرْسَلْنَا عَلَيْهِمُ الرِّيْحَ الْعَقِيْمَۚ

wafii '*aa*din i*dz* arsaln*aa* 'alayhimu **al**rrii*h*a **a**l'aqiim**a**

Dan (juga) pada (kisah kaum) ‘Ad, ketika Kami kirimkan kepada mereka angin yang membinasakan,

51:42

# مَا تَذَرُ مِنْ شَيْءٍ اَتَتْ عَلَيْهِ اِلَّا جَعَلَتْهُ كَالرَّمِيْمِۗ

m*aa* ta*dz*aru min syay-in atat 'alayhi ill*aa* ja'alat-hu ka**al**rramiim**i**

(angin itu) tidak membiarkan suatu apa pun yang dilandanya, bahkan dijadikannya seperti serbuk.

51:43

# وَفِيْ ثَمُوْدَ اِذْ قِيْلَ لَهُمْ تَمَتَّعُوْا حَتّٰى حِيْنٍ

wafii tsamuuda i*dz* qiila lahum tamatta'uu *h*att*aa* *h*iin**in**

Dan pada (kisah kaum) Samud, ketika dikatakan kepada mereka, “Bersenang-senanglah kamu sampai waktu yang ditentukan.”

51:44

# فَعَتَوْا عَنْ اَمْرِ رَبِّهِمْ فَاَخَذَتْهُمُ الصّٰعِقَةُ وَهُمْ يَنْظُرُوْنَ

fa'ataw 'an amri rabbihim fa-akha*dz*at-humu **al***shshaa*'iqatu wahum yan*zh*uruun**a**

Lalu mereka berlaku angkuh terhadap perintah Tuhannya, maka mereka disambar petir sedang mereka melihatnya.

51:45

# فَمَا اسْتَطَاعُوْا مِنْ قِيَامٍ وَّمَا كَانُوْا مُنْتَصِرِيْنَۙ

fam*aa* ista*thaa*'uu min qiy*aa*min wam*aa* k*aa*nuu munta*sh*iriin**a**

Maka mereka tidak mampu bangun dan juga tidak mendapat pertolongan,

51:46

# وَقَوْمَ نُوْحٍ مِّنْ قَبْلُ ۗ اِنَّهُمْ كَانُوْا قَوْمًا فٰسِقِيْنَ ࣖ

waqawma nuu*h*in min qablu innahum k*aa*nuu qawman f*aa*siqiin**a**

dan sebelum itu (telah Kami binasakan) kaum Nuh. Sungguh, mereka adalah kaum yang fasik.

51:47

# وَالسَّمَاۤءَ بَنَيْنٰهَا بِاَيْىدٍ وَّاِنَّا لَمُوْسِعُوْنَ

wa**al**ssam*aa*-a banayn*aa*h*aa* bi-aydin wa-inn*aa* lamuusi'uun**a**

Dan langit Kami bangun dengan kekuasaan (Kami), dan Kami benar-benar meluaskannya.

51:48

# وَالْاَرْضَ فَرَشْنٰهَا فَنِعْمَ الْمَاهِدُوْنَ

wa**a**l-ar*dh*a farasyn*aa*h*aa* fani'ma **a**lm*aa*hiduun**a**

Dan bumi Kami hamparkan; maka (Kami) sebaik-baik yang telah menghamparkan.

51:49

# وَمِنْ كُلِّ شَيْءٍ خَلَقْنَا زَوْجَيْنِ لَعَلَّكُمْ تَذَكَّرُوْنَ

wamin kulli syay-in khalaqn*aa* zawjayni la'allakum ta*dz*akkaruun**a**

Dan segala sesuatu Kami ciptakan berpasang-pasangan agar kamu mengingat (kebesaran Allah).

51:50

# فَفِرُّوْٓا اِلَى اللّٰهِ ۗاِنِّيْ لَكُمْ مِّنْهُ نَذِيْرٌ مُّبِيْنٌۚ

fafirruu il*aa* **al**l*aa*hi innii lakum minhu na*dz*iirun mubiin**un**

Maka segeralah kembali kepada (menaati) Allah. Sungguh, aku seorang pemberi peringatan yang jelas dari Allah untukmu.

51:51

# وَلَا تَجْعَلُوْا مَعَ اللّٰهِ اِلٰهًا اٰخَرَۗ اِنِّيْ لَكُمْ مِّنْهُ نَذِيْرٌ مُّبِيْنٌ

wal*aa* taj'aluu ma'a **al**l*aa*hi il*aa*han *aa*khara innii lakum minhu na*dz*iirun mubiin**un**

Dan janganlah kamu mengadakan tuhan yang lain selain Allah. Sungguh, aku seorang pemberi peringatan yang jelas dari Allah untukmu.

51:52

# كَذٰلِكَ مَآ اَتَى الَّذِيْنَ مِنْ قَبْلِهِمْ مِّنْ رَّسُوْلٍ اِلَّا قَالُوْا سَاحِرٌ اَوْ مَجْنُوْنٌ

ka*dzaa*lika m*aa* at*aa* **al**la*dz*iina min qablihim min rasuulin ill*aa* q*aa*luu s*aah*irun aw majnuun**un**

Demikianlah setiap kali seorang Rasul yang datang kepada orang-orang yang sebelum mereka, mereka (kaumnya) pasti mengatakan, “Dia itu pesihir atau orang gila.”

51:53

# اَتَوَاصَوْا بِهٖۚ بَلْ هُمْ قَوْمٌ طَاغُوْنَۚ

ataw*aas*aw bihi bal hum qawmun *thaa*ghuun**a**

Apakah mereka saling berpesan tentang apa yang dikatakan itu. Sebenarnya mereka adalah kaum yang melampaui batas.

51:54

# فَتَوَلَّ عَنْهُمْ فَمَآ اَنْتَ بِمَلُوْمٍ

fatawalla 'anhum fam*aa* anta bimaluum**in**

Maka berpalinglah engkau dari mereka, dan engkau sama sekali tidak tercela.

51:55

# وَذَكِّرْ فَاِنَّ الذِّكْرٰى تَنْفَعُ الْمُؤْمِنِيْنَ

wa*dz*akkir fa-inna **al***dzdz*ikr*aa* tanfa'u **a**lmu/miniin**a**

Dan tetaplah memberi peringatan, karena sesungguhnya peringatan itu bermanfaat bagi orang-orang mukmin.

51:56

# وَمَا خَلَقْتُ الْجِنَّ وَالْاِنْسَ اِلَّا لِيَعْبُدُوْنِ

wam*aa* khalaqtu **a**ljinna wa**a**l-insa ill*aa* liya'buduun**i**

Aku tidak menciptakan jin dan manusia melainkan agar mereka beribadah kepada-Ku.

51:57

# مَآ اُرِيْدُ مِنْهُمْ مِّنْ رِّزْقٍ وَّمَآ اُرِيْدُ اَنْ يُّطْعِمُوْنِ

m*aa* uriidu minhum min rizqin wam*aa* uriidu an yu*th*'imuun**i**

Aku tidak menghendaki rezeki sedikit pun dari mereka dan Aku tidak menghendaki agar mereka memberi makan kepada-Ku.

51:58

# اِنَّ اللّٰهَ هُوَ الرَّزَّاقُ ذُو الْقُوَّةِ الْمَتِيْنُ

inna **al**l*aa*ha huwa **al**rrazz*aa*qu *dz*uu **a**lquwwati **a**lmatiin**u**

Sungguh Allah, Dialah Pemberi rezeki Yang Mempunyai Kekuatan lagi Sangat Kokoh.

51:59

# فَاِنَّ لِلَّذِيْنَ ظَلَمُوْا ذَنُوْبًا مِّثْلَ ذَنُوْبِ اَصْحٰبِهِمْ فَلَا يَسْتَعْجِلُوْنِ

fa-inna lilla*dz*iina *zh*alamuu *dz*anuuban mitsla *dz*anuubi a*sh*-*haa*bihim fal*aa* yasta'jiluun**a**

Maka sungguh, untuk orang-orang yang zalim ada bagian (azab) seperti bagian teman-teman mereka (dahulu); maka janganlah mereka meminta kepada-Ku untuk menyegerakannya.

51:60

# فَوَيْلٌ لِّلَّذِيْنَ كَفَرُوْا مِنْ يَّوْمِهِمُ الَّذِيْ يُوْعَدُوْنَ ࣖ

fawaylun lilla*dz*iina kafaruu min yawmihimu **al**la*dz*ii yuu'aduun**a**

Maka celakalah orang-orang yang kafir pada hari yang telah dijanjikan kepada mereka (hari Kiamat).

<!--EndFragment-->