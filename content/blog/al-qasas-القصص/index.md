---
title: (28) Al-Qasas - القصص
date: 2021-10-27T03:54:11.113Z
ayat: 28
description: "Jumlah Ayat: 88 / Arti: Kisah-Kisah"
---
<!--StartFragment-->

28:1

# طٰسۤمّۤ

*thaa*-siin-miim

Tha Sin Mim

28:2

# تِلْكَ اٰيٰتُ الْكِتٰبِ الْمُبِيْنِ

tilka *aa*y*aa*tu **a**lkit*aa*bi **a**lmubiin**i**

Ini ayat-ayat Kitab (Al-Qur'an) yang jelas (dari Allah).

28:3

# نَتْلُوْا عَلَيْكَ مِنْ نَّبَاِ مُوْسٰى وَفِرْعَوْنَ بِالْحَقِّ لِقَوْمٍ يُّؤْمِنُوْنَ

natluu 'alayka min naba-i muus*aa* wafir'awna bi**a**l*h*aqqi liqawmin yu/minuun**a**

Kami membacakan kepadamu sebagian dari kisah Musa dan Fir‘aun dengan sebenarnya untuk orang-orang yang beriman.

28:4

# اِنَّ فِرْعَوْنَ عَلَا فِى الْاَرْضِ وَجَعَلَ اَهْلَهَا شِيَعًا يَّسْتَضْعِفُ طَاۤىِٕفَةً مِّنْهُمْ يُذَبِّحُ اَبْنَاۤءَهُمْ وَيَسْتَحْيٖ نِسَاۤءَهُمْ ۗاِنَّهٗ كَانَ مِنَ الْمُفْسِدِيْنَ

inna fir'awna 'al*aa* fii **a**l-ar*dh*i waja'ala ahlah*aa* syiya'an yasta*dh*'ifu *thaa*-ifatan minhum yu*dz*abbi*h*u abn*aa*-ahum wayasta*h*yii nis*aa*-ahum innahu k*aa*na mina <

Sungguh, Fir‘aun telah berbuat sewenang-wenang di bumi dan menjadikan penduduknya berpecah belah, dia menindas segolongan dari mereka (Bani Israil), dia menyembelih anak laki-laki mereka dan membiarkan hidup anak perempuan mereka. Sungguh, dia (Fir‘aun) t

28:5

# وَنُرِيْدُ اَنْ نَّمُنَّ عَلَى الَّذِيْنَ اسْتُضْعِفُوْا فِى الْاَرْضِ وَنَجْعَلَهُمْ اَىِٕمَّةً وَّنَجْعَلَهُمُ الْوٰرِثِيْنَ ۙ

wanuriidu an namunna 'al*aa* **al**la*dz*iina istu*dh*'ifuu fii **a**l-ar*dh*i wanaj'alahum a-immatan wanaj'alahumu **a**lw*aa*ritsiin**a**

Dan Kami hendak memberi karunia kepada orang-orang yang tertindas di bumi (Mesir) itu, dan hendak menjadikan mereka pemimpin dan menjadikan mereka orang-orang yang mewarisi (bumi),

28:6

# وَنُمَكِّنَ لَهُمْ فِى الْاَرْضِ وَنُرِيَ فِرْعَوْنَ وَهَامٰنَ وَجُنُوْدَهُمَا مِنْهُمْ مَّا كَانُوْا يَحْذَرُوْنَ

wanumakkina lahum fii **a**l-ar*dh*i wanuriya fir'awna wah*aa*m*aa*na wajunuudahum*aa* minhum m*aa* k*aa*nuu ya*hts*aruun**a**

dan Kami teguhkan kedudukan mereka di bumi dan Kami perlihatkan kepada Fir‘aun dan Haman bersama bala tentaranya apa yang selalu mereka takutkan dari mereka.

28:7

# وَاَوْحَيْنَآ اِلٰٓى اُمِّ مُوْسٰٓى اَنْ اَرْضِعِيْهِۚ فَاِذَا خِفْتِ عَلَيْهِ فَاَلْقِيْهِ فِى الْيَمِّ وَلَا تَخَافِيْ وَلَا تَحْزَنِيْ ۚاِنَّا رَاۤدُّوْهُ اِلَيْكِ وَجَاعِلُوْهُ مِنَ الْمُرْسَلِيْنَ

wa-aw*h*ayn*aa* il*aa* ummi muus*aa* an ar*dh*i'iihi fa-i*dzaa* khifti 'alayhi fa-alqiihi fii **a**lyammi wal*aa* takh*aa*fii wal*aa* ta*h*zanii inn*aa* r*aa*dduuhu ilayki waj*aa*

*Dan Kami ilhamkan kepada ibunya Musa, “Susuilah dia (Musa), dan apabila engkau khawatir terhadapnya maka hanyutkanlah dia ke sungai (Nil). Dan janganlah engkau takut dan jangan (pula) bersedih hati, sesungguhnya Kami akan mengembalikannya kepadamu, dan me*









28:8

# فَالْتَقَطَهٗٓ اٰلُ فِرْعَوْنَ لِيَكُوْنَ لَهُمْ عَدُوًّا وَّحَزَنًاۗ اِنَّ فِرْعَوْنَ وَهَامٰنَ وَجُنُوْدَهُمَا كَانُوْا خٰطِـِٕيْنَ

fa**i**ltaqa*th*ahu *aa*lu fir'awna liyakuuna lahum 'aduwwan wa*h*azanan inna fir'awna wah*aa*m*aa*na wajunuudahum*aa* k*aa*nuu kh*aath*i-iin**a**

Maka dia dipungut oleh keluarga Fir‘aun agar (kelak) dia menjadi musuh dan kesedihan bagi mereka. Sungguh, Fir‘aun dan Haman bersama bala tentaranya adalah orang-orang yang bersalah.

28:9

# وَقَالَتِ امْرَاَتُ فِرْعَوْنَ قُرَّتُ عَيْنٍ لِّيْ وَلَكَۗ لَا تَقْتُلُوْهُ ۖعَسٰٓى اَنْ يَّنْفَعَنَآ اَوْ نَتَّخِذَهٗ وَلَدًا وَّهُمْ لَا يَشْعُرُوْنَ

waq*aa*lati imra-atu fir'awna qurratu 'aynin lii walaka l*aa* taqtuluuhu 'as*aa* an yanfa'an*aa* aw nattakhi*dz*ahu waladan wahum l*aa* yasy'uruun**a**

Dan istri Fir‘aun berkata, “(Dia) adalah penyejuk mata hati bagiku dan bagimu. Janganlah kamu membunuhnya, mudah-mudahan dia bermanfaat kepada kita atau kita ambil dia menjadi anak,” sedang mereka tidak menyadari.

28:10

# وَاَصْبَحَ فُؤَادُ اُمِّ مُوْسٰى فٰرِغًاۗ اِنْ كَادَتْ لَتُبْدِيْ بِهٖ لَوْلَآ اَنْ رَّبَطْنَا عَلٰى قَلْبِهَا لِتَكُوْنَ مِنَ الْمُؤْمِنِيْنَ

wa-a*sh*ba*h*a fu-*aa*du ummi muus*aa* f*aa*righan in k*aa*dat latubdii bihi lawl*aa* an raba*th*n*aa* 'al*aa* qalbih*aa* litakuuna mina **a**lmu/miniin**a**

Dan hati ibu Musa menjadi kosong. Sungguh, hampir saja dia menyatakannya (rahasia tentang Musa), seandainya tidak Kami teguhkan hatinya, agar dia termasuk orang-orang yang beriman (kepada janji Allah).

28:11

# وَقَالَتْ لِاُخْتِهٖ قُصِّيْهِۗ فَبَصُرَتْ بِهٖ عَنْ جُنُبٍ وَّهُمْ لَا يَشْعُرُوْنَ ۙ

waq*aa*lat li-ukhtihi qu*shsh*iihi faba*sh*urat bihi 'an junubin wahum l*aa* yasy'uruun**a**

Dan dia (ibunya Musa) berkata kepada saudara perempuan Musa, “Ikutilah dia (Musa).” Maka kelihatan olehnya (Musa) dari jauh, sedang mereka tidak menyadarinya.

28:12

# ۞ وَحَرَّمْنَا عَلَيْهِ الْمَرَاضِعَ مِنْ قَبْلُ فَقَالَتْ هَلْ اَدُلُّكُمْ عَلٰٓى اَهْلِ بَيْتٍ يَّكْفُلُوْنَهٗ لَكُمْ وَهُمْ لَهٗ نَاصِحُوْنَ

wa*h*arramn*aa* 'alayhi **a**lmar*aad*i'a min qablu faq*aa*lat hal adullukum 'al*aa* ahli baytin yakfuluunahu lakum wahum lahu n*aas*i*h*uun**a**

Dan Kami cegah dia (Musa) menyusu kepada perempuan-perempuan yang mau menyusui(nya) sebelum itu; maka berkatalah dia (saudaranya Musa), “Maukah aku tunjukkan kepadamu, keluarga yang akan memeliharanya untukmu dan mereka dapat berlaku baik padanya?”

28:13

# فَرَدَدْنٰهُ اِلٰٓى اُمِّهٖ كَيْ تَقَرَّ عَيْنُهَا وَلَا تَحْزَنَ وَلِتَعْلَمَ اَنَّ وَعْدَ اللّٰهِ حَقٌّ وَّلٰكِنَّ اَكْثَرَهُمْ لَا يَعْلَمُوْنَ ࣖ

faradadn*aa*hu il*aa* ummihi kay taqarra 'aynuh*aa* wal*aa* ta*h*zana walita'lama anna wa'da **al**l*aa*hi *h*aqqun wal*aa*kinna aktsarahum l*aa* ya'lamuun**a**

Maka Kami kembalikan dia (Musa) kepada ibunya, agar senang hatinya dan tidak bersedih hati, dan agar dia mengetahui bahwa janji Allah adalah benar, tetapi kebanyakan mereka tidak mengetahuinya.

28:14

# وَلَمَّا بَلَغَ اَشُدَّهٗ وَاسْتَوٰىٓ اٰتَيْنٰهُ حُكْمًا وَّعِلْمًاۗ وَكَذٰلِكَ نَجْزِى الْمُحْسِنِيْنَ

walamm*aa* balagha asyuddahu wa**i**staw*aa* *aa*tayn*aa*hu *h*ukman wa'ilman waka*dzaa*lika najzii **a**lmu*h*siniin**a**

Dan setelah dia (Musa) dewasa dan sempurna akalnya, Kami anugerahkan kepadanya hikmah (kenabian) dan pengetahuan. Dan demikianlah Kami memberi balasan kepada orang-orang yang berbuat baik.

28:15

# وَدَخَلَ الْمَدِيْنَةَ عَلٰى حِيْنِ غَفْلَةٍ مِّنْ اَهْلِهَا فَوَجَدَ فِيْهَا رَجُلَيْنِ يَقْتَتِلٰنِۖ هٰذَا مِنْ شِيْعَتِهٖ وَهٰذَا مِنْ عَدُوِّهٖۚ فَاسْتَغَاثَهُ الَّذِيْ مِنْ شِيْعَتِهٖ عَلَى الَّذِيْ مِنْ عَدُوِّهٖ ۙفَوَكَزَهٗ مُوْسٰى فَقَضٰى عَلَيْهِ

wadakhala **a**lmadiinata 'al*aa* *h*iini ghaflatin min ahlih*aa* fawajada fiih*aa* rajulayni yaqtatil*aa*ni h*aadzaa* min syii'atihi wah*aadzaa* min 'aduwwihi fa**i**stagh*aa*tsahu

Dan dia (Musa) masuk ke kota (Memphis) ketika penduduknya sedang lengah, maka dia mendapati di dalam kota itu dua orang laki-laki sedang berkelahi; yang seorang dari golongannya (Bani Israil) dan yang seorang (lagi) dari pihak musuhnya (kaum Fir‘aun). Ora

28:16

# قَالَ رَبِّ اِنِّيْ ظَلَمْتُ نَفْسِيْ فَاغْفِرْ لِيْ فَغَفَرَ لَهٗ ۗاِنَّهٗ هُوَ الْغَفُوْرُ الرَّحِيْمُ

q*aa*la rabbi innii *zh*alamtu nafsii fa**i**ghfir lii faghafara lahu innahu huwa **a**lghafuuru **al**rra*h*iim**u**

Dia (Musa) berdoa, “Ya Tuhanku, sesungguhnya aku telah menzalimi diriku sendiri, maka ampunilah aku.” Maka Dia (Allah) mengampuninya. Sungguh, Allah, Dialah Yang Maha Pengampun, Maha Penyayang.

28:17

# قَالَ رَبِّ بِمَآ اَنْعَمْتَ عَلَيَّ فَلَنْ اَكُوْنَ ظَهِيْرًا لِّلْمُجْرِمِيْنَ

q*aa*la rabbi bim*aa* an'amta 'alayya falan akuuna *zh*ahiiran lilmujrimiin**a**

Dia (Musa) berkata, “Ya Tuhanku! Demi nikmat yang telah Engkau anugerahkan kepadaku, maka aku tidak akan menjadi penolong bagi orang-orang yang berdosa.”

28:18

# فَاَصْبَحَ فِى الْمَدِيْنَةِ خَاۤىِٕفًا يَّتَرَقَّبُ فَاِذَا الَّذِى اسْتَنْصَرَهٗ بِالْاَمْسِ يَسْتَصْرِخُهٗ ۗقَالَ لَهٗ مُوْسٰٓى اِنَّكَ لَغَوِيٌّ مُّبِيْنٌ

fa-a*sh*ba*h*a fii **a**lmadiinati kh*aa*-ifan yataraqqabu fa-i*dzaa* **al**la*dz*ii istan*sh*arahu bi**a**l-amsi yasta*sh*rikhuhu q*aa*la lahu muus*aa* innaka laghawiyyun

Karena itu, dia (Musa) menjadi ketakutan berada di kota itu sambil menunggu (akibat perbuatannya), tiba-tiba orang yang kemarin meminta pertolongan berteriak meminta pertolongan kepadanya. Musa berkata kepadanya, “Engkau sungguh, orang yang nyata-nyata se

28:19

# فَلَمَّآ اَنْ اَرَادَ اَنْ يَّبْطِشَ بِالَّذِيْ هُوَ عَدُوٌّ لَّهُمَاۙ قَالَ يٰمُوْسٰٓى اَتُرِيْدُ اَنْ تَقْتُلَنِيْ كَمَا قَتَلْتَ نَفْسًاۢ بِالْاَمْسِۖ اِنْ تُرِيْدُ اِلَّآ اَنْ تَكُوْنَ جَبَّارًا فِى الْاَرْضِ وَمَا تُرِيْدُ اَنْ تَكُوْنَ مِنَ الْمُص

falamm*aa* an ar*aa*da an yab*th*isya bi**a**lla*dz*ii huwa 'aduwwun lahum*aa* q*aa*la y*aa* muus*aa* aturiidu an taqtulanii kam*aa* qatalta nafsan bi**a**l-amsi in turiidu ill*aa*

*Maka ketika dia (Musa) hendak memukul dengan keras orang yang menjadi musuh mereka berdua, dia (musuhnya) berkata, “Wahai Musa! Apakah engkau bermaksud membunuhku, sebagaimana kemarin engkau membunuh seseorang? Engkau hanya bermaksud menjadi orang yang be*









28:20

# وَجَاۤءَ رَجُلٌ مِّنْ اَقْصَى الْمَدِيْنَةِ يَسْعٰىۖ قَالَ يٰمُوْسٰٓى اِنَّ الْمَلَاَ يَأْتَمِرُوْنَ بِكَ لِيَقْتُلُوْكَ فَاخْرُجْ اِنِّيْ لَكَ مِنَ النّٰصِحِيْنَ

waj*aa*-a rajulun min aq*shaa* **a**lmadiinati yas'*aa* q*aa*la y*aa* muus*aa* inna **a**lmala-a ya/tamiruuna bika liyaqtuluuka fa**u**khruj innii laka mina **al**nn*aas*

*Dan seorang laki-laki datang bergegas dari ujung kota seraya berkata, “Wahai Musa! Sesungguhnya para pembesar negeri sedang berunding tentang engkau untuk membunuhmu, maka keluarlah (dari kota ini), sesungguhnya aku termasuk orang-orang yang memberi nasih*









28:21

# فَخَرَجَ مِنْهَا خَاۤىِٕفًا يَّتَرَقَّبُ ۖقَالَ رَبِّ نَجِّنِيْ مِنَ الْقَوْمِ الظّٰلِمِيْنَ ࣖ

fakharaja minh*aa* kh*aa*-ifan yataraqqabu q*aa*la rabbi najjinii mina **a**lqawmi **al***zhzhaa*limiin**a**

Maka keluarlah dia (Musa) dari kota itu dengan rasa takut, waspada (kalau ada yang menyusul atau menangkapnya), dia berdoa, “Ya Tuhanku, selamatkanlah aku dari orang-orang yang zalim itu.”

28:22

# وَلَمَّا تَوَجَّهَ تِلْقَاۤءَ مَدْيَنَ قَالَ عَسٰى رَبِّيْٓ اَنْ يَّهْدِيَنِيْ سَوَاۤءَ السَّبِيْلِ

walamm*aa* tawajjaha tilq*aa*-a madyana q*aa*la 'as*aa* rabbii an yahdiyanii saw*aa*-a **al**ssabiil**i**

Dan ketika dia menuju ke arah negeri Madyan dia berdoa lagi, “Mudah-mudahan Tuhanku memimpin aku ke jalan yang benar.”

28:23

# وَلَمَّا وَرَدَ مَاۤءَ مَدْيَنَ وَجَدَ عَلَيْهِ اُمَّةً مِّنَ النَّاسِ يَسْقُوْنَ ەۖ وَوَجَدَ مِنْ دُوْنِهِمُ امْرَاَتَيْنِ تَذُوْدٰنِۚ قَالَ مَا خَطْبُكُمَا ۗقَالَتَا لَا نَسْقِيْ حَتّٰى يُصْدِرَ الرِّعَاۤءُ وَاَبُوْنَا شَيْخٌ كَبِيْرٌ

walamm*aa* warada m*aa*-a madyana wajada 'alayhi ummatan mina **al**nn*aa*si yasquuna wawajada min duunihimu imra-atayni ta*dz*uud*aa*ni q*aa*la m*aa* kha*th*bukum*aa* q*aa*lat*aa* l*aa*

Dan ketika dia sampai di sumber air negeri Madyan, dia menjumpai di sana sekumpulan orang yang sedang memberi minum (ternaknya), dan dia menjumpai di belakang orang banyak itu, dua orang perempuan sedang menghambat (ternaknya). Dia (Musa) berkata, “Apakah







28:24

# فَسَقٰى لَهُمَا ثُمَّ تَوَلّٰىٓ اِلَى الظِّلِّ فَقَالَ رَبِّ اِنِّيْ لِمَآ اَنْزَلْتَ اِلَيَّ مِنْ خَيْرٍ فَقِيْرٌ

fasaq*aa* lahum*aa* tsumma tawall*aa* il*aa* **al***zhzh*illi faq*aa*la rabbi innii lim*aa* anzalta ilayya min khayrin faqiir**un**

Maka dia (Musa) memberi minum (ternak) kedua perempuan itu, kemudian dia kembali ke tempat yang teduh lalu berdoa, “Ya Tuhanku, sesungguhnya aku sangat memerlukan sesuatu kebaikan (makanan) yang Engkau turunkan kepadaku.”

28:25

# فَجَاۤءَتْهُ اِحْدٰىهُمَا تَمْشِيْ عَلَى اسْتِحْيَاۤءٍ ۖقَالَتْ اِنَّ اَبِيْ يَدْعُوْكَ لِيَجْزِيَكَ اَجْرَ مَا سَقَيْتَ لَنَاۗ فَلَمَّا جَاۤءَهٗ وَقَصَّ عَلَيْهِ الْقَصَصَۙ قَالَ لَا تَخَفْۗ نَجَوْتَ مِنَ الْقَوْمِ الظّٰلِمِيْنَ

faj*aa*-at-hu i*h*d*aa*hum*aa* tamsyii 'al*aa* isti*h*y*aa*-in q*aa*lat inna abii yad'uuka liyajziyaka ajra m*aa* saqayta lan*aa* falamm*aa* j*aa*-ahu waqa*shsh*a 'alayhi **a**l

Kemudian datanglah kepada Musa salah seorang dari kedua perempuan itu berjalan dengan malu-malu, dia berkata, “Sesungguhnya ayahku mengundangmu untuk memberi balasan sebagai imbalan atas (kebaikan)mu memberi minum (ternak) kami.” Ketika (Musa) mendatangi

28:26

# قَالَتْ اِحْدٰىهُمَا يٰٓاَبَتِ اسْتَأْجِرْهُ ۖاِنَّ خَيْرَ مَنِ اسْتَأْجَرْتَ الْقَوِيُّ الْاَمِيْنُ

q*aa*lat i*h*d*aa*hum*aa* y*aa* abati ista/jirhu inna khayra mani ista/jarta **a**lqawiyyu **a**l-amiin**u**

Dan salah seorang dari kedua (perempuan) itu berkata, “Wahai ayahku! Jadikanlah dia sebagai pekerja (pada kita), sesungguhnya orang yang paling baik yang engkau ambil sebagai pekerja (pada kita) ialah orang yang kuat dan dapat dipercaya.”

28:27

# قَالَ اِنِّيْٓ اُرِيْدُ اَنْ اُنْكِحَكَ اِحْدَى ابْنَتَيَّ هٰتَيْنِ عَلٰٓى اَنْ تَأْجُرَنِيْ ثَمٰنِيَ حِجَجٍۚ فَاِنْ اَتْمَمْتَ عَشْرًا فَمِنْ عِنْدِكَۚ وَمَآ اُرِيْدُ اَنْ اَشُقَّ عَلَيْكَۗ سَتَجِدُنِيْٓ اِنْ شَاۤءَ اللّٰهُ مِنَ الصّٰلِحِيْنَ

q*aa*la innii uriidu an unki*h*aka i*h*d*aa* ibnatayya h*aa*tayni 'al*aa* an ta/juranii tsam*aa*niya *h*ijajin fa-in atmamta 'asyran famin 'indika wam*aa* uriidu an asyuqqa 'alayka satajidunii in sy*aa*-a

Dia (Syekh Madyan) berkata, “Sesungguhnya aku bermaksud ingin menikahkan engkau dengan salah seorang dari kedua anak perempuanku ini, dengan ketentuan bahwa engkau bekerja padaku selama delapan tahun dan jika engkau sempurnakan sepuluh tahun maka itu adal

28:28

# قَالَ ذٰلِكَ بَيْنِيْ وَبَيْنَكَۗ اَيَّمَا الْاَجَلَيْنِ قَضَيْتُ فَلَا عُدْوَانَ عَلَيَّۗ وَاللّٰهُ عَلٰى مَا نَقُوْلُ وَكِيْلٌ ࣖ

q*aa*la *dzaa*lika baynii wabaynaka ayyam*aa* **a**l-ajalayni qa*dh*aytu fal*aa* 'udw*aa*na 'alayya wa**al**l*aa*hu 'al*aa* m*aa* naquulu wakiil**un**

Dia (Musa) berkata, “Itu (perjanjian) antara aku dan engkau. Yang mana saja dari kedua waktu yang ditentukan itu yang aku sempurnakan, maka tidak ada tuntutan (tambahan) atas diriku (lagi). Dan Allah menjadi saksi atas apa yang kita ucapkan.”

28:29

# ۞ فَلَمَّا قَضٰى مُوْسَى الْاَجَلَ وَسَارَ بِاَهْلِهٖٓ اٰنَسَ مِنْ جَانِبِ الطُّوْرِ نَارًاۗ قَالَ لِاَهْلِهِ امْكُثُوْٓا اِنِّيْٓ اٰنَسْتُ نَارًا لَّعَلِّيْٓ اٰتِيْكُمْ مِّنْهَا بِخَبَرٍ اَوْ جَذْوَةٍ مِّنَ النَّارِ لَعَلَّكُمْ تَصْطَلُوْنَ

falamm*aa* qa*daa* muus*aa* **a**l-ajala was*aa*ra bi-ahlihi *aa*nasa min j*aa*nibi **al***ththh*uuri n*aa*ran q*aa*la li-ahlihi umkutsuu innii *aa*nastu n*aa*ran la'allii

Maka ketika Musa telah menyelesaikan waktu yang ditentukan itu dan dia berangkat dengan keluarganya, dia melihat api di lereng gunung. Dia berkata kepada keluarganya, “Tunggulah (di sini), sesungguhnya aku melihat api, mudah-mudahan aku dapat membawa suat

28:30

# فَلَمَّآ اَتٰىهَا نُوْدِيَ مِنْ شَاطِئِ الْوَادِ الْاَيْمَنِ فِى الْبُقْعَةِ الْمُبٰرَكَةِ مِنَ الشَّجَرَةِ اَنْ يّٰمُوْسٰٓى اِنِّيْٓ اَنَا اللّٰهُ رَبُّ الْعٰلَمِيْنَ ۙ

falamm*aa* at*aa*h*aa* nuudiya min sy*aath*i-i **a**lw*aa*di **a**l-aymani fii **a**lbuq'ati **a**lmub*aa*rakati mina **al**sysyajarati an y*aa* muus*aa<*

Maka ketika dia (Musa) sampai ke (tempat) api itu, dia diseru dari (arah) pinggir sebelah kanan lembah, dari sebatang pohon, di sebidang tanah yang diberkahi, “Wahai Musa! Sungguh, Aku adalah Allah, Tuhan seluruh alam!







28:31

# وَاَنْ اَلْقِ عَصَاكَ ۗفَلَمَّا رَاٰهَا تَهْتَزُّ كَاَنَّهَا جَاۤنٌّ وَّلّٰى مُدْبِرًا وَّلَمْ يُعَقِّبْۗ يٰمُوْسٰٓى اَقْبِلْ وَلَا تَخَفْۗ اِنَّكَ مِنَ الْاٰمِنِيْنَ

wa-an **a**lqi 'a*shaa*ka falamm*aa* ra*aa*h*aa* tahtazzu ka-annah*aa* j*aa*nnun wall*aa* mudbiran walam yu'aqqib y*aa* muus*aa* aqbil wal*aa* takhaf innaka mina **a**l-*aa*m

Dan lemparkanlah tongkatmu.” Maka ketika dia (Musa) melihatnya bergerak-gerak seakan-akan seekor ular yang (gesit), dia lari berbalik ke belakang tanpa menoleh. (Allah berfirman), “Wahai Musa! Kemarilah dan jangan takut. Sesungguhnya engkau termasuk orang

28:32

# اُسْلُكْ يَدَكَ فِيْ جَيْبِكَ تَخْرُجْ بَيْضَاۤءَ مِنْ غَيْرِ سُوْۤءٍ ۖوَّاضْمُمْ اِلَيْكَ جَنَاحَكَ مِنَ الرَّهْبِ فَذٰنِكَ بُرْهَانٰنِ مِنْ رَّبِّكَ اِلٰى فِرْعَوْنَ وَمَلَا۟ىِٕهٖۗ اِنَّهُمْ كَانُوْا قَوْمًا فٰسِقِيْنَ

usluk yadaka fii jaybika takhruj bay*dhaa*-a min ghayri suu-in wa**u***dh*mum ilayka jan*aah*aka mina **al**rrahbi fa*dzaa*nika burh*aa*n*aa*ni min rabbika il*aa* fir'awna wamala-ihi innahum k

Masukkanlah tanganmu ke leher bajumu, dia akan keluar putih (bercahaya) tanpa cacat, dan dekapkanlah kedua tanganmu ke dadamu apabila ketakutan. Itulah dua mukjizat dari Tuhanmu (yang akan engkau pertunjukkan) kepada Fir‘aun dan para pembesarnya. Sungguh,

28:33

# قَالَ رَبِّ اِنِّيْ قَتَلْتُ مِنْهُمْ نَفْسًا فَاَخَافُ اَنْ يَّقْتُلُوْنِ

q*aa*la rabbi innii qataltu minhum nafsan fa-akh*aa*fu an yaqtuluun**i**

Dia (Musa) berkata, “Ya Tuhanku, sungguh aku telah membunuh seorang dari golongan mereka, sehingga aku takut mereka akan membunuhku.

28:34

# وَاَخِيْ هٰرُوْنُ هُوَ اَفْصَحُ مِنِّيْ لِسَانًا فَاَرْسِلْهُ مَعِيَ رِدْءًا يُّصَدِّقُنِيْٓ ۖاِنِّيْٓ اَخَافُ اَنْ يُّكَذِّبُوْنِ

wa-akhii h*aa*ruunu huwa af*sh*a*h*u minnii lis*aa*nan fa-arsilhu ma'iya rid-an yu*sh*addiqunii innii akh*aa*fu an yuka*dzdz*ibuun**i**

Dan saudaraku Harun, dia lebih fasih lidahnya daripada aku, maka utuslah dia bersamaku sebagai pembantuku untuk membenarkan (perkataan)ku; sungguh, aku takut mereka akan mendustakanku.”

28:35

# قَالَ سَنَشُدُّ عَضُدَكَ بِاَخِيْكَ وَنَجْعَلُ لَكُمَا سُلْطٰنًا فَلَا يَصِلُوْنَ اِلَيْكُمَا ۛبِاٰيٰتِنَا ۛ اَنْتُمَا وَمَنِ اتَّبَعَكُمَا الْغٰلِبُوْنَ

q*aa*la sanasyuddu 'a*dh*udaka bi-akhiika wanaj'alu lakum*aa* sul*thaa*nan fal*aa* ya*sh*iluuna ilaykum*aa* bi-*aa*y*aa*tin*aa* antum*aa* wamani ittaba'akum*aa* **a**lgh*aa<*

Dia (Allah) berfirman, “Kami akan menguatkan engkau (membantumu) dengan saudaramu, dan Kami berikan kepadamu berdua kekuasaan yang besar, maka mereka tidak akan dapat mencapaimu; (berangkatlah kamu berdua) dengan membawa mukjizat Kami, kamu berdua dan ora







28:36

# فَلَمَّا جَاۤءَهُمْ مُّوْسٰى بِاٰيٰتِنَا بَيِّنٰتٍ قَالُوْا مَا هٰذَآ اِلَّا سِحْرٌ مُّفْتَرًىۙ وَّمَا سَمِعْنَا بِهٰذَا فِيْٓ اٰبَاۤىِٕنَا الْاَوَّلِيْنَ

falamm*aa* j*aa*-ahum muus*aa* bi-*aa*y*aa*tin*aa* bayyin*aa*tin q*aa*luu m*aa* h*aadzaa* ill*aa* si*h*run muftaran wam*aa* sami'n*aa* bih*aadzaa* fii *aa*b*aa*-in*aa*

*Maka ketika Musa datang kepada mereka dengan (membawa) mukjizat Kami yang nyata, mereka berkata, “Ini hanyalah sihir yang dibuat-buat, dan kami tidak pernah mendengar (yang seperti) ini pada nenek moyang kami dahulu.”*









28:37

# وَقَالَ مُوْسٰى رَبِّيْٓ اَعْلَمُ بِمَنْ جَاۤءَ بِالْهُدٰى مِنْ عِنْدِهٖ وَمَنْ تَكُوْنُ لَهٗ عَاقِبَةُ الدَّارِۗ اِنَّهٗ لَا يُفْلِحُ الظّٰلِمُوْنَ

waq*aa*la muus*aa* rabbii a'lamu biman j*aa*-a bi**a**lhud*aa* min 'indihi waman takuunu lahu '*aa*qibatu **al**dd*aa*ri innahu l*aa* yufli*h*u **al***zhzhaa*limuun

Dan dia (Musa) menjawab, “Tuhanku lebih mengetahui siapa yang (pantas) membawa petunjuk dari sisi-Nya dan siapa yang akan mendapat kesudahan (yang baik) di akhirat. Sesungguhnya orang-orang yang zalim tidak akan mendapat kemenangan.”

28:38

# وَقَالَ فِرْعَوْنُ يٰٓاَيُّهَا الْمَلَاُ مَا عَلِمْتُ لَكُمْ مِّنْ اِلٰهٍ غَيْرِيْۚ فَاَوْقِدْ لِيْ يٰهَامٰنُ عَلَى الطِّيْنِ فَاجْعَلْ لِّيْ صَرْحًا لَّعَلِّيْٓ اَطَّلِعُ اِلٰٓى اِلٰهِ مُوْسٰىۙ وَاِنِّيْ لَاَظُنُّهٗ مِنَ الْكٰذِبِيْنَ

waq*aa*la fir'awnu y*aa* ayyuh*aa* **a**lmalau m*aa* 'alimtu lakum min il*aa*hin ghayrii fa-awqid lii y*aa* h*aa*m*aa*nu 'al*aa* **al***ththh*iini fa**i**j'al lii

Dan Fir‘aun berkata, “Wahai para pembesar kaumku! Aku tidak mengetahui ada Tuhan bagimu selain aku. Maka bakarlah tanah liat untukku wahai Haman (untuk membuat batu bata), kemudian buatkanlah bangunan yang tinggi untukku agar aku dapat naik melihat Tuhann







28:39

# وَاسْتَكْبَرَ هُوَ وَجُنُوْدُهٗ فِى الْاَرْضِ بِغَيْرِ الْحَقِّ وَظَنُّوْٓا اَنَّهُمْ اِلَيْنَا لَا يُرْجَعُوْنَ

wa**i**stakbara huwa wajunuuduhu fii **a**l-ar*dh*i bighayri **a**l*h*aqqi wa*zh*annuu annahum ilayn*aa* l*aa* yurja'uun**a**

Dan dia (Fir‘aun) dan bala tentaranya berlaku sombong, di bumi tanpa alasan yang benar, dan mereka mengira bahwa mereka tidak akan dikembalikan kepada Kami.

28:40

# فَاَخَذْنٰهُ وَجُنُوْدَهٗ فَنَبَذْنٰهُمْ فِى الْيَمِّ ۚفَانْظُرْ كَيْفَ كَانَ عَاقِبَةُ الظّٰلِمِيْنَ

fa-akha*dz*n*aa*hu wajunuudahu fanaba*dz*n*aa*hum fii **a**lyammi fa**u**n*zh*ur kayfa k*aa*na '*aa*qibatu **al***zhzhaa*limiin**a**

Maka Kami siksa dia (Fir‘aun) dan bala tentaranya, lalu Kami lemparkan mereka ke dalam laut. Maka perhatikanlah bagaimana kesudahan orang yang zalim.

28:41

# وَجَعَلْنٰهُمْ اَىِٕمَّةً يَّدْعُوْنَ اِلَى النَّارِۚ وَيَوْمَ الْقِيٰمَةِ لَا يُنْصَرُوْنَ

waja'aln*aa*hum a-immatan yad'uuna il*aa* **al**nn*aa*ri wayawma **a**lqiy*aa*mati l*aa* yun*sh*aruun**a**

Dan Kami jadikan mereka para pemimpin yang mengajak (manusia) ke neraka dan pada hari Kiamat mereka tidak akan ditolong.

28:42

# وَاَتْبَعْنٰهُمْ فِيْ هٰذِهِ الدُّنْيَا لَعْنَةً ۚوَيَوْمَ الْقِيٰمَةِ هُمْ مِّنَ الْمَقْبُوْحِيْنَ ࣖ

wa-atba'n*aa*hum fii h*aadz*ihi **al**dduny*aa* la'natan wayawma **a**lqiy*aa*mati hum mina **a**lmaqbuu*h*iin**a**

Dan Kami susulkan laknat kepada mereka di dunia ini; sedangkan pada hari Kiamat mereka termasuk orang-orang yang dijauhkan (dari rahmat Allah).

28:43

# وَلَقَدْ اٰتَيْنَا مُوْسَى الْكِتٰبَ مِنْۢ بَعْدِ مَآ اَهْلَكْنَا الْقُرُوْنَ الْاُوْلٰى بَصَاۤىِٕرَ لِلنَّاسِ وَهُدًى وَّرَحْمَةً لَّعَلَّهُمْ يَتَذَكَّرُوْنَ

walaqad *aa*tayn*aa* muus*aa* **a**lkit*aa*ba min ba'di m*aa* ahlakn*aa* **a**lquruuna **a**l-uul*aa* ba*shaa*-ira li**l**nn*aa*si wahudan wara*h*matan l

Dan sungguh, telah Kami berikan kepada Musa Kitab (Taurat) setelah Kami binasakan umat-umat terdahulu, untuk menjadi pelita bagi manusia dan petunjuk serta rahmat, agar mereka mendapat pelajaran.

28:44

# وَمَا كُنْتَ بِجَانِبِ الْغَرْبِيِّ اِذْ قَضَيْنَآ اِلٰى مُوْسَى الْاَمْرَ وَمَا كُنْتَ مِنَ الشّٰهِدِيْنَ ۙ

wam*aa* kunta bij*aa*nibi **a**lgharbiyyi i*dz* qa*dh*ayn*aa* il*aa* muus*aa* **a**l-amra wam*aa* kunta mina **al**sysy*aa*hidiin**a**

Dan engkau (Muhammad) tidak berada di sebelah barat (lembah suci Tuwa) ketika Kami menyampaikan perintah kepada Musa, dan engkau tidak (pula) termasuk orang-orang yang menyaksikan (kejadian itu).

28:45

# وَلٰكِنَّآ اَنْشَأْنَا قُرُوْنًا فَتَطَاوَلَ عَلَيْهِمُ الْعُمُرُۚ وَمَا كُنْتَ ثَاوِيًا فِيْٓ اَهْلِ مَدْيَنَ تَتْلُوْا عَلَيْهِمْ اٰيٰتِنَاۙ وَلٰكِنَّا كُنَّا مُرْسِلِيْنَ

wal*aa*kinn*aa* ansya-n*aa* quruunan fata*thaa*wala 'alayhimu **a**l'umuru wam*aa* kunta ts*aa*wiyan fii ahli madyana tatluu 'alayhim *aa*y*aa*tin*aa* wal*aa*kinn*aa* kunn*aa*

Tetapi Kami telah menciptakan beberapa umat, dan telah berlalu atas mereka masa yang panjang, dan engkau (Muhammad) tidak tinggal bersama-sama penduduk Madyan dengan membacakan ayat-ayat Kami kepada mereka, tetapi Kami telah mengutus rasul-rasul.

28:46

# وَمَا كُنْتَ بِجَانِبِ الطُّوْرِ اِذْ نَادَيْنَا وَلٰكِنْ رَّحْمَةً مِّنْ رَّبِّكَ لِتُنْذِرَ قَوْمًا مَّآ اَتٰىهُمْ مِّنْ نَّذِيْرٍ مِّنْ قَبْلِكَ لَعَلَّهُمْ يَتَذَكَّرُوْنَ

wam*aa* kunta bij*aa*nibi **al***ththh*uuri i*dz* n*aa*dayn*aa* wal*aa*kin ra*h*matan min rabbika litun*dz*ira qawman m*aa* at*aa*hum min na*dz*iirin min qablika la'allahum yata*dz*

Dan engkau (Muhammad) tidak berada di dekat Tur (gunung) ketika Kami menyeru (Musa), tetapi (Kami utus engkau) sebagai rahmat dari Tuhanmu, agar engkau memberi peringatan kepada kaum (Quraisy) yang tidak didatangi oleh pemberi peringatan sebelum engkau ag







28:47

# وَلَوْلَآ اَنْ تُصِيْبَهُمْ مُّصِيْبَةٌ ۢبِمَا قَدَّمَتْ اَيْدِيْهِمْ فَيَقُوْلُوْا رَبَّنَا لَوْلَآ اَرْسَلْتَ اِلَيْنَا رَسُوْلًا فَنَتَّبِعَ اٰيٰتِكَ وَنَكُوْنَ مِنَ الْمُؤْمِنِيْنَ

walawl*aa* an tu*sh*iibahum mu*sh*iibatun bim*aa* qaddamat aydiihim fayaquuluu rabban*aa* lawl*aa* arsalta ilayn*aa* rasuulan fanattabi'a *aa*y*aa*tika wanakuuna mina **a**lmu/miniin**a**

**Dan agar mereka tidak mengatakan ketika azab menimpa mereka disebabkan apa yang mereka kerjakan, “Ya Tuhan kami, mengapa Engkau tidak mengutus seorang rasul kepada kami, agar kami mengikuti ayat-ayat Engkau dan termasuk orang mukmin.”**









28:48

# فَلَمَّا جَاۤءَهُمُ الْحَقُّ مِنْ عِنْدِنَا قَالُوْا لَوْلَآ اُوْتِيَ مِثْلَ مَآ اُوْتِيَ مُوْسٰىۗ اَوَلَمْ يَكْفُرُوْا بِمَآ اُوْتِيَ مُوْسٰى مِنْ قَبْلُۚ قَالُوْا سِحْرٰنِ تَظَاهَرَاۗ وَقَالُوْٓا اِنَّا بِكُلٍّ كٰفِرُوْنَ

falamm*aa* j*aa*-ahumu **a**l*h*aqqu min 'indin*aa* q*aa*luu lawl*aa* uutiya mitsla m*aa* uutiya muus*aa* awa lam yakfuruu bim*aa* uutiya muus*aa* min qablu q*aa*luu si*h*r*aa*n

Maka ketika telah datang kepada mereka kebenaran (Al-Qur'an) dari sisi Kami, mereka berkata, “Mengapa tidak diberikan kepadanya (Muhammad) seperti apa yang telah diberikan kepada Musa dahulu?” Bukankah mereka itu telah ingkar (juga) kepada apa yang diberi

28:49

# قُلْ فَأْتُوْا بِكِتٰبٍ مِّنْ عِنْدِ اللّٰهِ هُوَ اَهْدٰى مِنْهُمَآ اَتَّبِعْهُ اِنْ كُنْتُمْ صٰدِقِيْنَ

qul fa/tuu bikit*aa*bin min 'indi **al**l*aa*hi huwa ahd*aa* minhum*aa* attabi'hu in kuntum *shaa*diqiin**a**

Katakanlah (Muhammad), “Datangkanlah olehmu sebuah kitab dari sisi Allah yang kitab itu lebih memberi petunjuk daripada keduanya (Taurat dan Al-Qur'an), niscaya aku mengikutinya, jika kamu orang yang benar.”

28:50

# فَاِنْ لَّمْ يَسْتَجِيْبُوْا لَكَ فَاعْلَمْ اَنَّمَا يَتَّبِعُوْنَ اَهْوَاۤءَهُمْۗ وَمَنْ اَضَلُّ مِمَّنِ اتَّبَعَ هَوٰىهُ بِغَيْرِ هُدًى مِّنَ اللّٰهِ ۗاِنَّ اللّٰهَ لَا يَهْدِى الْقَوْمَ الظّٰلِمِيْنَ ࣖ

fa-in lam yastajiibuu laka fa**i**'lam annam*aa* yattabi'uuna ahw*aa*-ahum waman a*dh*allu mimmani ittaba'a haw*aa*hu bighayri hudan mina **al**l*aa*hi inna **al**l*aa*ha l*aa* yahd

Maka jika mereka tidak menjawab (tantanganmu), maka ketahuilah bahwa mereka hanyalah mengikuti keinginan mereka. Dan siapakah yang lebih sesat daripada orang yang mengikuti keinginannya tanpa mendapat petunjuk dari Allah sedikit pun? Sungguh, Allah tidak

28:51

# ۞ وَلَقَدْ وَصَّلْنَا لَهُمُ الْقَوْلَ لَعَلَّهُمْ يَتَذَكَّرُوْنَ ۗ

walaqad wa*shsh*aln*aa* lahumu **a**lqawla la'allahum yata*dz*akkaruun**a**

Dan sungguh, Kami telah menyampaikan perkataan ini (Al-Qur'an) kepada mereka agar mereka selalu mengingatnya.

28:52

# اَلَّذِيْنَ اٰتَيْنٰهُمُ الْكِتٰبَ مِنْ قَبْلِهٖ هُمْ بِهٖ يُؤْمِنُوْنَ

**al**la*dz*iina *aa*tayn*aa*humu **a**lkit*aa*ba min qablihi hum bihi yu/minuun**a**

Orang-orang yang telah Kami berikan kepada mereka Al-Kitab sebelum Al-Qur'an, mereka beriman (pula) kepadanya (Al-Qur'an).

28:53

# وَاِذَا يُتْلٰى عَلَيْهِمْ قَالُوْٓا اٰمَنَّا بِهٖٓ اِنَّهُ الْحَقُّ مِنْ رَّبِّنَآ اِنَّا كُنَّا مِنْ قَبْلِهٖ مُسْلِمِيْنَ

wa-i*dzaa* yutl*aa* 'alayhim q*aa*luu *aa*mann*aa* bihi innahu **a**l*h*aqqu min rabbin*aa* inn*aa* kunn*aa* min qablihi muslimiin**a**

Dan apabila (Al-Qur'an) dibacakan kepada mereka, mereka berkata, “Kami beriman kepadanya, sesungguhnya (Al-Qur'an) itu adalah suatu kebenaran dari Tuhan kami. Sungguh, sebelumnya kami adalah orang muslim.”

28:54

# اُولٰۤىِٕكَ يُؤْتَوْنَ اَجْرَهُمْ مَّرَّتَيْنِ بِمَا صَبَرُوْا وَيَدْرَءُوْنَ بِالْحَسَنَةِ السَّيِّئَةَ وَمِمَّا رَزَقْنٰهُمْ يُنْفِقُوْنَ

ul*aa*-ika yu/tawna ajrahum marratayni bim*aa* *sh*abaruu wayadrauuna bi**a**l*h*asanati **al**ssayyi-ata wamimm*aa* razaqn*aa*hum yunfiquun**a**

Mereka itu diberi pahala dua kali (karena beriman kepada Taurat dan Al-Qur'an) disebabkan kesabaran mereka, dan mereka menolak kejahatan dengan kebaikan, dan menginfakkan sebagian dari rezeki yang telah Kami berikan kepada mereka.

28:55

# وَاِذَا سَمِعُوا اللَّغْوَ اَعْرَضُوْا عَنْهُ وَقَالُوْا لَنَآ اَعْمَالُنَا وَلَكُمْ اَعْمَالُكُمْ ۖسَلٰمٌ عَلَيْكُمْ ۖ لَا نَبْتَغِى الْجٰهِلِيْنَ

wa-i*dzaa* sami'uu **al**laghwa a'ra*dh*uu 'anhu waq*aa*luu lan*aa* a'm*aa*lun*aa* walakum a'm*aa*lukum sal*aa*mun 'alaykum l*aa* nabtaghii **a**lj*aa*hiliin**a**

Dan apabila mereka mendengar perkataan yang buruk, mereka berpaling darinya dan berkata, “Bagi kami amal-amal kami dan bagimu amal-amal kamu, semoga selamatlah kamu, kami tidak ingin (bergaul) dengan orang-orang bodoh.”

28:56

# اِنَّكَ لَا تَهْدِيْ مَنْ اَحْبَبْتَ وَلٰكِنَّ اللّٰهَ يَهْدِيْ مَنْ يَّشَاۤءُ ۚوَهُوَ اَعْلَمُ بِالْمُهْتَدِيْنَ

innaka l*aa* tahdii man a*h*babta wal*aa*kinna **al**l*aa*ha yahdii man yasy*aa*u wahuwa a'lamu bi**a**lmuhtadiin**a**

Sungguh, engkau (Muhammad) tidak dapat memberi petunjuk kepada orang yang engkau kasihi, tetapi Allah memberi petunjuk kepada orang yang Dia kehendaki, dan Dia lebih mengetahui orang-orang yang mau menerima petunjuk.

28:57

# وَقَالُوْٓا اِنْ نَّتَّبِعِ الْهُدٰى مَعَكَ نُتَخَطَّفْ مِنْ اَرْضِنَاۗ اَوَلَمْ نُمَكِّنْ لَّهُمْ حَرَمًا اٰمِنًا يُّجْبٰٓى اِلَيْهِ ثَمَرٰتُ كُلِّ شَيْءٍ رِّزْقًا مِّنْ لَّدُنَّا وَلٰكِنَّ اَكْثَرَهُمْ لَا يَعْلَمُوْنَ

waq*aa*luu in nattabi'i **a**lhud*aa* ma'aka nutakha*ththh*af min ar*dh*in*aa* awa lam numakkin lahum *h*araman *aa*minan yujb*aa* ilayhi tsamar*aa*tu kulli syay-in rizqan min ladunn*aa* wal

Dan mereka berkata, “Jika kami mengikuti petunjuk bersama engkau, niscaya kami akan diusir dari negeri kami.” (Allah berfirman) Bukankah Kami telah meneguhkan kedudukan mereka dalam tanah haram (tanah suci) yang aman, yang didatangkan ke tempat itu buah-b

28:58

# وَكَمْ اَهْلَكْنَا مِنْ قَرْيَةٍ ۢ بَطِرَتْ مَعِيْشَتَهَا ۚفَتِلْكَ مَسٰكِنُهُمْ لَمْ تُسْكَنْ مِّنْۢ بَعْدِهِمْ اِلَّا قَلِيْلًاۗ وَكُنَّا نَحْنُ الْوَارِثِيْنَ

wakam ahlakn*aa* min qaryatin ba*th*irat ma'iisyatah*aa* fatilka mas*aa*kinuhum lam tuskan min ba'dihim ill*aa* qaliilan wakunn*aa* na*h*nu **a**lw*aa*ritsiin**a**

Dan betapa banyak (penduduk) negeri yang sudah bersenang-senang dalam kehidupannya yang telah Kami binasakan, maka itulah tempat kediaman mereka yang tidak didiami (lagi) setelah mereka, kecuali sebagian kecil. Dan Kamilah yang mewarisinya.”

28:59

# وَمَا كَانَ رَبُّكَ مُهْلِكَ الْقُرٰى حَتّٰى يَبْعَثَ فِيْٓ اُمِّهَا رَسُوْلًا يَّتْلُوْا عَلَيْهِمْ اٰيٰتِنَاۚ وَمَا كُنَّا مُهْلِكِى الْقُرٰىٓ اِلَّا وَاَهْلُهَا ظٰلِمُوْنَ

wam*aa* k*aa*na rabbuka muhlika **a**lqur*aa* *h*att*aa* yab'atsa fii ummih*aa* rasuulan yatluu 'alayhim *aa*y*aa*tin*aa* wam*aa* kunn*aa* muhlikii **a**lqur*aa* ill*a*

Dan Tuhanmu tidak akan membinasakan negeri-negeri, sebelum Dia mengutus seorang rasul di ibukotanya yang membacakan ayat-ayat Kami kepada mereka; dan tidak pernah (pula) Kami membinasakan (penduduk) negeri; kecuali penduduknya melakukan kezaliman.







28:60

# وَمَآ اُوْتِيْتُمْ مِّنْ شَيْءٍ فَمَتَاعُ الْحَيٰوةِ الدُّنْيَا وَزِيْنَتُهَا ۚوَمَا عِنْدَ اللّٰهِ خَيْرٌ وَّاَبْقٰىۗ اَفَلَا تَعْقِلُوْنَ ࣖ

wam*aa* uutiitum min syay-in famat*aa*'u **a**l*h*ay*aa*ti **al**dduny*aa* waziinatuh*aa* wam*aa* 'inda **al**l*aa*hi khayrun wa-abq*aa* afal*aa* ta'qiluun**a**

**Dan apa saja (kekayaan, jabatan, keturunan) yang diberikan kepada kamu, maka itu adalah kesenangan hidup duniawi dan perhiasannya; sedang apa yang di sisi Allah adalah lebih baik dan lebih kekal. Tidakkah kamu mengerti?**









28:61

# اَفَمَنْ وَّعَدْنٰهُ وَعْدًا حَسَنًا فَهُوَ لَاقِيْهِ كَمَنْ مَّتَّعْنٰهُ مَتَاعَ الْحَيٰوةِ الدُّنْيَا ثُمَّ هُوَ يَوْمَ الْقِيٰمَةِ مِنَ الْمُحْضَرِيْنَ

afaman wa'adn*aa*hu wa'dan *h*asanan fahuwa l*aa*qiihi kaman matta'n*aa*hu mat*aa*'a **a**l*h*ay*aa*ti **al**dduny*aa* tsumma huwa yawma **a**lqiy*aa*mati mina **a**

**Maka apakah sama orang yang Kami janjikan kepadanya suatu janji yang baik (surga) lalu dia memperolehnya, dengan orang yang Kami berikan kepadanya kesenangan hidup duniawi; kemudian pada hari Kiamat dia termasuk orang-orang yang diseret (ke dalam neraka)?**









28:62

# وَيَوْمَ يُنَادِيْهِمْ فَيَقُوْلُ اَيْنَ شُرَكَاۤءِيَ الَّذِيْنَ كُنْتُمْ تَزْعُمُوْنَ

wayawma yun*aa*diihim fayaquulu ayna syurak*aa*-iya **al**la*dz*iina kuntum taz'umuun**a**

Dan (ingatlah) pada hari ketika Dia (Allah) menyeru mereka dan berfirman, “Di manakah sekutu-sekutu-Ku yang dahulu kamu sangka?”

28:63

# قَالَ الَّذِيْنَ حَقَّ عَلَيْهِمُ الْقَوْلُ رَبَّنَا هٰٓؤُلَاۤءِ الَّذِيْنَ اَغْوَيْنَاۚ اَغْوَيْنٰهُمْ كَمَا غَوَيْنَاۚ تَبَرَّأْنَآ اِلَيْكَ مَا كَانُوْٓا اِيَّانَا يَعْبُدُوْنَ

q*aa*la **al**la*dz*iina *h*aqqa 'alayhimu **a**lqawlu rabban*aa* h*aa*ul*aa*-i **al**la*dz*iina aghwayn*aa* aghwayn*aa*hum kam*aa* ghawayn*aa* tabarra/n*aa*

*Orang-orang yang sudah pasti akan mendapatkan hukuman berkata, “Ya Tuhan kami, mereka inilah orang-orang yang kami sesatkan itu; kami telah menyesatkan mereka sebagaimana kami (sendiri) sesat, kami menyatakan kepada Engkau berlepas diri (dari mereka), mer*









28:64

# وَقِيْلَ ادْعُوْا شُرَكَاۤءَكُمْ فَدَعَوْهُمْ فَلَمْ يَسْتَجِيْبُوْا لَهُمْ ۗوَرَاَوُا الْعَذَابَۚ لَوْ اَنَّهُمْ كَانُوْا يَهْتَدُوْنَ

waqiila ud'uu syurak*aa*-akum fada'awhum falam yastajiibuu lahum wara-awuu **a**l'a*dzaa*ba law annahum k*aa*nuu yahtaduun**a**

Dan dikatakan (kepada mereka), “Serulah sekutu-sekutumu,” lalu mereka menyerunya, tetapi yang diseru tidak menyambutnya, dan mereka melihat azab. (Mereka itu berkeinginan) sekiranya mereka dahulu menerima petunjuk.

28:65

# وَيَوْمَ يُنَادِيْهِمْ فَيَقُوْلُ مَاذَآ اَجَبْتُمُ الْمُرْسَلِيْنَ

wayawma yun*aa*diihim fayaquulu m*aatsaa* ajabtumu **a**lmursaliin**a**

Dan (ingatlah) pada hari ketika Dia (Allah) menyeru mereka, dan berfirman, “Apakah jawabanmu terhadap para rasul?”

28:66

# فَعَمِيَتْ عَلَيْهِمُ الْاَنْۢبَاۤءُ يَوْمَىِٕذٍ فَهُمْ لَا يَتَسَاۤءَلُوْنَ

fa'amiyat 'alayhimu **a**l-anb*aa*u yawma-i*dz*in fahum l*aa* yatas*aa*-aluun**a**

Maka gelaplah bagi mereka segala macam alasan pada hari itu, karena itu mereka tidak saling bertanya.

28:67

# فَاَمَّا مَنْ تَابَ وَاٰمَنَ وَعَمِلَ صَالِحًا فَعَسٰٓى اَنْ يَّكُوْنَ مِنَ الْمُفْلِحِيْنَ

fa-amm*aa* man t*aa*ba wa*aa*mana wa'amila *shaa*li*h*an fa'as*aa* an yakuuna mina **a**lmufli*h*iin**a**

Maka adapun orang yang bertobat dan beriman, serta mengerjakan kebajikan, maka mudah-mudahan dia termasuk orang yang beruntung.

28:68

# وَرَبُّكَ يَخْلُقُ مَا يَشَاۤءُ وَيَخْتَارُ ۗمَا كَانَ لَهُمُ الْخِيَرَةُ ۗسُبْحٰنَ اللّٰهِ وَتَعٰلٰى عَمَّا يُشْرِكُوْنَ

warabbuka yakhluqu m*aa* yasy*aa*u wayakht*aa*ru m*aa* k*aa*na lahumu **a**lkhiyaratu sub*haa*na **al**l*aa*hi wata'*aa*l*aa* 'amm*aa* yusyrikuun**a**

Dan Tuhanmu menciptakan dan memilih apa yang Dia kehendaki. Bagi mereka (manusia) tidak ada pilihan. Mahasuci Allah dan Mahatinggi Dia dari apa yang mereka persekutukan.

28:69

# وَرَبُّكَ يَعْلَمُ مَا تُكِنُّ صُدُوْرُهُمْ وَمَا يُعْلِنُوْنَ

warabbuka ya'lamu m*aa* tukinnu *sh*uduuruhum wam*aa* yu'linuun**a**

Dan Tuhanmu mengetahui apa yang disembunyikan dalam dada mereka dan apa yang mereka nyatakan.

28:70

# وَهُوَ اللّٰهُ لَآ اِلٰهَ اِلَّا هُوَ ۗ لَهُ الْحَمْدُ فِى الْاُوْلٰى وَالْاٰخِرَةِ ۖوَلَهُ الْحُكْمُ وَاِلَيْهِ تُرْجَعُوْنَ

wahuwa **al**l*aa*hu l*aa* il*aa*ha ill*aa* huwa lahu **a**l*h*amdu fii **a**l-uul*aa* wa**a**l-*aa*khirati walahu **a**l*h*ukmu wa-ilayhi turja'uun

Dan Dialah Allah, tidak ada tuhan (yang berhak disembah) selain Dia, segala puji bagi-Nya di dunia dan di akhirat, dan bagi-Nya segala penentuan dan kepada-Nya kamu dikembalikan.

28:71

# قُلْ اَرَءَيْتُمْ اِنْ جَعَلَ اللّٰهُ عَلَيْكُمُ الَّيْلَ سَرْمَدًا اِلٰى يَوْمِ الْقِيٰمَةِ مَنْ اِلٰهٌ غَيْرُ اللّٰهِ يَأْتِيْكُمْ بِضِيَاۤءٍ ۗ اَفَلَا تَسْمَعُوْنَ

qul ara-aytum in ja'ala **al**l*aa*hu 'alaykumu **al**layla sarmadan il*aa* yawmi **a**lqiy*aa*mati man il*aa*hun ghayru **al**l*aa*hi ya/tiikum bi*dh*iy*aa*-in afal

Katakanlah (Muhammad), “Bagaimana pendapatmu, jika Allah menjadikan untukmu malam itu terus-menerus sampai hari Kiamat. Siapakah tuhan selain Allah yang akan mendatangkan sinar terang kepadamu? Apakah kamu tidak mendengar?”







28:72

# قُلْ اَرَءَيْتُمْ اِنْ جَعَلَ اللّٰهُ عَلَيْكُمُ النَّهَارَ سَرْمَدًا اِلٰى يَوْمِ الْقِيٰمَةِ مَنْ اِلٰهٌ غَيْرُ اللّٰهِ يَأْتِيْكُمْ بِلَيْلٍ تَسْكُنُوْنَ فِيْهِ ۗ اَفَلَا تُبْصِرُوْنَ

qul ara-aytum in ja'ala **al**l*aa*hu 'alaykumu **al**nnah*aa*ra sarmadan il*aa* yawmi **a**lqiy*aa*mati man il*aa*hun ghayru **al**l*aa*hi ya/tiikum bilaylin taskunuuna fii

Katakanlah (Muhammad), “Bagaimana pendapatmu, jika Allah menjadikan untukmu siang itu terus-menerus sampai hari Kiamat. Siapakah tuhan selain Allah yang akan mendatangkan malam kepadamu sebagai waktu istirahatmu? Apakah kamu tidak memperhatikan?”

28:73

# وَمِنْ رَّحْمَتِهٖ جَعَلَ لَكُمُ الَّيْلَ وَالنَّهَارَ لِتَسْكُنُوْا فِيْهِ وَلِتَبْتَغُوْا مِنْ فَضْلِهٖ وَلَعَلَّكُمْ تَشْكُرُوْنَ

wamin ra*h*matihi ja'ala lakumu **al**layla wa**al**nnah*aa*ra litaskunuu fiihi walitabtaghuu min fa*dh*lihi wala'allakum tasykuruun**a**

Dan adalah karena rahmat-Nya, Dia jadikan untukmu malam dan siang, agar kamu beristirahat pada malam hari dan agar kamu mencari sebagian karunia-Nya (pada siang hari) dan agar kamu bersyukur kepada-Nya.

28:74

# وَيَوْمَ يُنَادِيْهِمْ فَيَقُوْلُ اَيْنَ شُرَكَاۤءِيَ الَّذِيْنَ كُنْتُمْ تَزْعُمُوْنَ

wayawma yun*aa*diihim fayaquulu ayna syurak*aa*-iya **al**la*dz*iina kuntum taz'umuun**a**

Dan (ingatlah) pada hari ketika Dia (Allah) menyeru mereka, dan berfirman, “Di manakah sekutu-sekutu-Ku yang dahulu kamu sangka?”

28:75

# وَنَزَعْنَا مِنْ كُلِّ اُمَّةٍ شَهِيْدًا فَقُلْنَا هَاتُوْا بُرْهَانَكُمْ فَعَلِمُوْٓا اَنَّ الْحَقَّ لِلّٰهِ وَضَلَّ عَنْهُمْ مَّا كَانُوْا يَفْتَرُوْنَ ࣖ

wanaza'n*aa* min kulli ummatin syahiidan faquln*aa* h*aa*tuu burh*aa*nakum fa'alimuu anna **a**l*h*aqqa lill*aa*hi wa*dh*alla 'anhum m*aa* k*aa*nuu yaftaruun**a**

Dan Kami datangkan dari setiap umat seorang saksi, lalu Kami katakan, “Kemukakanlah bukti kebenaranmu,” maka tahulah mereka bahwa yang hak (kebenaran) itu milik Allah dan lenyaplah dari mereka apa yang dahulu mereka ada-adakan.

28:76

# ۞ اِنَّ قَارُوْنَ كَانَ مِنْ قَوْمِ مُوْسٰى فَبَغٰى عَلَيْهِمْ ۖوَاٰتَيْنٰهُ مِنَ الْكُنُوْزِ مَآ اِنَّ مَفَاتِحَهٗ لَتَنُوْۤاُ بِالْعُصْبَةِ اُولِى الْقُوَّةِ اِذْ قَالَ لَهٗ قَوْمُهٗ لَا تَفْرَحْ اِنَّ اللّٰهَ لَا يُحِبُّ الْفَرِحِيْنَ

inna q*aa*ruuna k*aa*na min qawmi muus*aa* fabagh*aa* 'alayhim wa*aa*tayn*aa*hu mina **a**lkunuuzi m*aa* inna maf*aa*ti*h*ahu latanuu-u bi**a**l'u*sh*bati ulii **a**l

Sesungguhnya Karun termasuk kaum Musa, tetapi dia berlaku zalim terhadap mereka, dan Kami telah menganugerahkan kepadanya perbendaharaan harta yang kunci-kuncinya sungguh berat dipikul oleh sejumlah orang yang kuat-kuat. (Ingatlah) ketika kaumnya berkata

28:77

# وَابْتَغِ فِيْمَآ اٰتٰىكَ اللّٰهُ الدَّارَ الْاٰخِرَةَ وَلَا تَنْسَ نَصِيْبَكَ مِنَ الدُّنْيَا وَاَحْسِنْ كَمَآ اَحْسَنَ اللّٰهُ اِلَيْكَ وَلَا تَبْغِ الْفَسَادَ فِى الْاَرْضِ ۗاِنَّ اللّٰهَ لَا يُحِبُّ الْمُفْسِدِيْنَ

wa**i**btaghi fiim*aa* *aa*t*aa*ka **al**l*aa*hu **al**dd*aa*ra **a**l-*aa*khirata wal*aa* tansa na*sh*iibaka mina **al**dduny*aa* wa-a*h*si

Dan carilah (pahala) negeri akhirat dengan apa yang telah dianugerahkan Allah kepadamu, tetapi janganlah kamu lupakan bagianmu di dunia dan berbuatbaiklah (kepada orang lain) sebagaimana Allah telah berbuat baik kepadamu, dan janganlah kamu berbuat kerusa

28:78

# قَالَ اِنَّمَآ اُوْتِيْتُهٗ عَلٰى عِلْمٍ عِنْدِيْۗ اَوَلَمْ يَعْلَمْ اَنَّ اللّٰهَ قَدْ اَهْلَكَ مِنْ قَبْلِهٖ مِنَ الْقُرُوْنِ مَنْ هُوَ اَشَدُّ مِنْهُ قُوَّةً وَّاَكْثَرُ جَمْعًا ۗوَلَا يُسْـَٔلُ عَنْ ذُنُوْبِهِمُ الْمُجْرِمُوْنَ

q*aa*la innam*aa* uutiituhu 'al*aa* 'ilmin 'indii awa lam ya'lam anna **al**l*aa*ha qad ahlaka min qablihi mina **a**lquruuni man huwa asyaddu minhu quwwatan wa-aktsaru jam'an wal*aa* yus-alu 'an *dz*

*Dia (Karun) berkata, “Sesungguhnya aku diberi (harta itu), semata-mata karena ilmu yang ada padaku.” Tidakkah dia tahu, bahwa Allah telah membinasakan umat-umat sebelumnya yang lebih kuat daripadanya, dan lebih banyak mengumpulkan harta? Dan orang-orang y*









28:79

# فَخَرَجَ عَلٰى قَوْمِهٖ فِيْ زِيْنَتِهٖ ۗقَالَ الَّذِيْنَ يُرِيْدُوْنَ الْحَيٰوةَ الدُّنْيَا يٰلَيْتَ لَنَا مِثْلَ مَآ اُوْتِيَ قَارُوْنُۙ اِنَّهٗ لَذُوْ حَظٍّ عَظِيْمٍ

fakharaja 'al*aa* qawmihi fii ziinatihi q*aa*la **al**la*dz*iina yuriiduuna **a**l*h*ay*aa*ta **al**dduny*aa* y*aa* layta lan*aa* mitsla m*aa* uutiya q*aa*ruunu innah

Maka keluarlah dia (Karun) kepada kaumnya dengan kemegahannya. Orang-orang yang menginginkan kehidupan dunia berkata, “Mudah-mudahan kita mempunyai harta kekayaan seperti apa yang telah diberikan kepada Karun, sesungguhnya dia benar-benar mempunyai keberu

28:80

# وَقَالَ الَّذِيْنَ اُوْتُوا الْعِلْمَ وَيْلَكُمْ ثَوَابُ اللّٰهِ خَيْرٌ لِّمَنْ اٰمَنَ وَعَمِلَ صَالِحًا ۚوَلَا يُلَقّٰىهَآ اِلَّا الصّٰبِرُوْنَ

waq*aa*la **al**la*dz*iina uutuu **a**l'ilma waylakum tsaw*aa*bu **al**l*aa*hi khayrun liman *aa*mana wa'amila *shaa*li*h*an wal*aa* yulaqq*aa*h*aa* ill*aa*

Tetapi orang-orang yang dianugerahi ilmu berkata, “Celakalah kamu! Ketahuilah, pahala Allah lebih baik bagi orang-orang yang beriman dan mengerjakan kebajikan, dan (pahala yang besar) itu hanya diperoleh oleh orang-orang yang sabar.”

28:81

# فَخَسَفْنَا بِهٖ وَبِدَارِهِ الْاَرْضَ ۗفَمَا كَانَ لَهٗ مِنْ فِئَةٍ يَّنْصُرُوْنَهٗ مِنْ دُوْنِ اللّٰهِ ۖوَمَا كَانَ مِنَ الْمُنْتَصِرِيْنَ

fakhasafn*aa* bihi wabid*aa*rihi **a**l-ar*dh*a fam*aa* k*aa*na lahu min fi-atin yan*sh*uruunahu min duuni **al**l*aa*hi wam*aa* k*aa*na mina **a**lmunta*sh*iriin

Maka Kami benamkan dia (Karun) bersama rumahnya ke dalam bumi. Maka tidak ada baginya satu golongan pun yang akan menolongnya selain Allah, dan dia tidak termasuk orang-orang yang dapat membela diri.

28:82

# وَاَصْبَحَ الَّذِيْنَ تَمَنَّوْا مَكَانَهٗ بِالْاَمْسِ يَقُوْلُوْنَ وَيْكَاَنَّ اللّٰهَ يَبْسُطُ الرِّزْقَ لِمَنْ يَّشَاۤءُ مِنْ عِبَادِهٖ وَيَقْدِرُۚ لَوْلَآ اَنْ مَّنَّ اللّٰهُ عَلَيْنَا لَخَسَفَ بِنَا ۗوَيْكَاَنَّهٗ لَا يُفْلِحُ الْكٰفِرُوْنَ ࣖ

wa-a*sh*ba*h*a **al**la*dz*iina tamannaw mak*aa*nahu bi**a**l-amsi yaquuluuna wayka-anna **al**l*aa*ha yabsu*th*u **al**rrizqa liman yasy*aa*u min 'ib*aa*dihi waya

Dan orang-orang yang kemarin mengangan-angankan kedudukannya (Karun) itu berkata, “Aduhai, benarlah kiranya Allah yang melapangkan rezeki bagi siapa yang Dia kehendaki di antara hamba-hamba-Nya dan membatasi (bagi siapa yang Dia kehendaki di antara hamba-

28:83

# تِلْكَ الدَّارُ الْاٰخِرَةُ نَجْعَلُهَا لِلَّذِيْنَ لَا يُرِيْدُوْنَ عُلُوًّا فِى الْاَرْضِ وَلَا فَسَادًا ۗوَالْعَاقِبَةُ لِلْمُتَّقِيْنَ

tilka **al**dd*aa*ru **a**l-*aa*khiratu naj'aluh*aa* lilla*dz*iina l*aa* yuriiduuna 'uluwwan fii **a**l-ar*dh*i wal*aa* fas*aa*dan wa**a**l'*aa*qibatu lilmutt

Negeri akhirat itu Kami jadikan bagi orang-orang yang tidak menyombongkan diri dan tidak berbuat kerusakan di bumi. Dan kesudahan (yang baik) itu bagi orang-orang yang bertakwa.

28:84

# مَنْ جَاۤءَ بِالْحَسَنَةِ فَلَهٗ خَيْرٌ مِّنْهَاۚ وَمَنْ جَاۤءَ بِالسَّيِّئَةِ فَلَا يُجْزَى الَّذِيْنَ عَمِلُوا السَّيِّاٰتِ اِلَّا مَا كَانُوْا يَعْمَلُوْنَ

man j*aa*-a bi**a**l*h*asanati falahu khayrun minh*aa* waman j*aa*-a bi**al**ssayyi-ati fal*aa* yujz*aa* **al**la*dz*iina 'amiluu **al**ssayyi-*aa*ti ill*aa*

Barangsiapa datang dengan (membawa) kebaikan, maka dia akan mendapat (pahala) yang lebih baik daripada kebaikannya itu; dan barang siapa datang dengan (membawa) kejahatan, maka orang-orang yang telah mengerjakan kejahatan itu hanya diberi balasan (seimban

28:85

# اِنَّ الَّذِيْ فَرَضَ عَلَيْكَ الْقُرْاٰنَ لَرَاۤدُّكَ اِلٰى مَعَادٍ ۗقُلْ رَّبِّيْٓ اَعْلَمُ مَنْ جَاۤءَ بِالْهُدٰى وَمَنْ هُوَ فِيْ ضَلٰلٍ مُّبِيْنٍ

inna **al**la*dz*ii fara*dh*a 'alayka **a**lqur-*aa*na lar*aa*dduka il*aa* ma'*aa*din qul rabbii a'lamu man j*aa*-a bi**a**lhud*aa* waman huwa fii *dh*al*aa*lin mubii

Sesungguhnya (Allah) yang mewajibkan engkau (Muhammad) untuk (melaksanakan hukum-hukum) Al-Qur'an, benar-benar akan mengembalikanmu ke tempat kembali. Katakanlah (Muhammad), “Tuhanku mengetahui orang yang membawa petunjuk dan orang yang berada dalam keses

28:86

# وَمَا كُنْتَ تَرْجُوْٓا اَنْ يُّلْقٰٓى اِلَيْكَ الْكِتٰبُ اِلَّا رَحْمَةً مِّنْ رَّبِّكَ فَلَا تَكُوْنَنَّ ظَهِيْرًا لِّلْكٰفِرِيْنَ ۖ

wam*aa* kunta tarjuu an yulq*aa* ilayka **a**lkit*aa*bu ill*aa* ra*h*matan min rabbika fal*aa* takuunanna *zh*ahiiran lilk*aa*firiin**a**

Dan engkau (Muhammad) tidak pernah mengharap agar Kitab (Al-Qur'an) itu diturunkan kepadamu, tetapi ia (diturunkan) sebagai rahmat dari Tuhanmu, sebab itu janganlah sekali-kali engkau menjadi penolong bagi orang-orang kafir,

28:87

# وَلَا يَصُدُّنَّكَ عَنْ اٰيٰتِ اللّٰهِ بَعْدَ اِذْ اُنْزِلَتْ اِلَيْكَ وَادْعُ اِلٰى رَبِّكَ وَلَا تَكُوْنَنَّ مِنَ الْمُشْرِكِيْنَ ۚ

wal*aa* ya*sh*uddunnaka 'an *aa*y*aa*ti **al**l*aa*hi ba'da i*dz* unzilat ilayka wa**u**d'u il*aa* rabbika wal*aa* takuunanna mina **a**lmusyrikiin**a**

dan jangan sampai mereka menghalang-halangi engkau (Muhammad) untuk (menyampaikan) ayat-ayat Allah, setelah ayat-ayat itu diturunkan kepadamu, dan serulah (manusia) agar (beriman) kepada Tuhanmu, dan janganlah engkau termasuk orang-orang musyrik.

28:88

# وَلَا تَدْعُ مَعَ اللّٰهِ اِلٰهًا اٰخَرَۘ لَآ اِلٰهَ اِلَّا هُوَۗ كُلُّ شَيْءٍ هَالِكٌ اِلَّا وَجْهَهٗ ۗ لَهُ الْحُكْمُ وَاِلَيْهِ تُرْجَعُوْنَ ࣖ

wal*aa* tad'u ma'a **al**l*aa*hi il*aa*han *aa*khara l*aa* il*aa*ha ill*aa* huwa kullu syay-in h*aa*likun ill*aa* wajhahu lahu **a**l*h*ukmu wa-ilayhi turja'uun**a**

Dan jangan (pula) engkau sembah tuhan yang lain selain Allah. Tidak ada tuhan (yang berhak disembah) selain Dia. Segala sesuatu pasti binasa, kecuali Allah. Segala keputusan menjadi wewenang-Nya, dan hanya kepada-Nya kamu dikembalikan.

<!--EndFragment-->