---
title: (63) Al-Munafiqun - المنٰفقون
date: 2021-10-27T04:03:13.683Z
ayat: 63
description: "Jumlah Ayat: 11 / Arti: Orang-Orang Munafik"
---
<!--StartFragment-->

63:1

# اِذَا جَاۤءَكَ الْمُنٰفِقُوْنَ قَالُوْا نَشْهَدُ اِنَّكَ لَرَسُوْلُ اللّٰهِ ۘوَاللّٰهُ يَعْلَمُ اِنَّكَ لَرَسُوْلُهٗ ۗوَاللّٰهُ يَشْهَدُ اِنَّ الْمُنٰفِقِيْنَ لَكٰذِبُوْنَۚ

i*dzaa* j*aa*-aka **a**lmun*aa*fiquuna q*aa*luu nasyhadu innaka larasuulu **al**l*aa*hi wa**al**l*aa*hu ya'lamu innaka larasuuluhu wa**al**l*aa*hu yasyhadu inna

Apabila orang-orang munafik datang kepadamu (Muhammad), mereka berkata, “Kami mengakui, bahwa engkau adalah Rasul Allah.” Dan Allah mengetahui bahwa engkau benar-benar Rasul-Nya; dan Allah menyaksikan bahwa orang-orang munafik itu benar-benar pendusta.

63:2

# اِتَّخَذُوْٓا اَيْمَانَهُمْ جُنَّةً فَصَدُّوْا عَنْ سَبِيْلِ اللّٰهِ ۗاِنَّهُمْ سَاۤءَ مَا كَانُوْا يَعْمَلُوْنَ

ittakha*dz*uu aym*aa*nahum junnatan fa*sh*adduu 'an sabiili **al**l*aa*hi innahum s*aa*-a m*aa* k*aa*nuu ya'maluun**a**

Mereka menjadikan sumpah-sumpah mereka sebagai perisai, lalu mereka menghalang-halangi (manusia) dari jalan Allah. Sungguh, betapa buruknya apa yang telah mereka kerjakan.

63:3

# ذٰلِكَ بِاَنَّهُمْ اٰمَنُوْا ثُمَّ كَفَرُوْا فَطُبِعَ عَلٰى قُلُوْبِهِمْ فَهُمْ لَا يَفْقَهُوْنَ

*dzaa*lika bi-annahum *aa*manuu tsumma kafaruu fa*th*ubi'a 'al*aa* quluubihim fahum l*aa* yafqahuun**a**

Yang demikian itu karena sesungguhnya mereka telah beriman, kemudian menjadi kafir, maka hati mereka dikunci, sehingga mereka tidak dapat mengerti.

63:4

# ۞ وَاِذَا رَاَيْتَهُمْ تُعْجِبُكَ اَجْسَامُهُمْۗ وَاِنْ يَّقُوْلُوْا تَسْمَعْ لِقَوْلِهِمْۗ كَاَنَّهُمْ خُشُبٌ مُّسَنَّدَةٌ ۗيَحْسَبُوْنَ كُلَّ صَيْحَةٍ عَلَيْهِمْۗ هُمُ الْعَدُوُّ فَاحْذَرْهُمْۗ قَاتَلَهُمُ اللّٰهُ ۖاَنّٰى يُؤْفَكُوْنَ

wa-i*dzaa* ra-aytahum tu'jibuka ajs*aa*muhum wa-in yaquuluu tasma' liqawlihim ka-annahum khusyubun musannadatun ya*h*sabuuna kulla *sh*ay*h*atin 'alayhim humu **a**l'aduwwu fa**i***hts*arhum q*aa*

*Dan apabila engkau melihat mereka, tubuh mereka mengagumkanmu. Dan jika mereka berkata, engkau mendengarkan tutur-katanya. Mereka seakan-akan kayu yang tersandar. Mereka mengira bahwa setiap teriakan ditujukan kepada mereka. Mereka itulah musuh (yang sebe*









63:5

# وَاِذَا قِيْلَ لَهُمْ تَعَالَوْا يَسْتَغْفِرْ لَكُمْ رَسُوْلُ اللّٰهِ لَوَّوْا رُءُوْسَهُمْ وَرَاَيْتَهُمْ يَصُدُّوْنَ وَهُمْ مُّسْتَكْبِرُوْنَ

wa-i*dzaa* qiila lahum ta'*aa*law yastaghfir lakum rasuulu **al**l*aa*hi lawwaw ruuusahum wara-aytahum ya*sh*udduuna wahum mustakbiruun**a**

Dan apabila dikatakan kepada mereka, “Marilah (beriman), agar Rasulullah memohonkan ampunan bagimu,” mereka membuang muka dan engkau lihat mereka berpaling dengan menyombongkan diri.

63:6

# سَوَاۤءٌ عَلَيْهِمْ اَسْتَغْفَرْتَ لَهُمْ اَمْ لَمْ تَسْتَغْفِرْ لَهُمْۗ لَنْ يَّغْفِرَ اللّٰهُ لَهُمْۗ اِنَّ اللّٰهَ لَا يَهْدِى الْقَوْمَ الْفٰسِقِيْنَ

saw*aa*un 'alayhim astaghfarta lahum am lam tastaghfir lahum lan yaghfira **al**l*aa*hu lahum inna **al**l*aa*ha l*aa* yahdii **a**lqawma **a**lf*aa*siqiin**a**

Sama saja bagi mereka, engkau (Muhammad) mohonkan ampunan untuk mereka atau tidak engkau mohonkan ampunan bagi mereka, Allah tidak akan mengampuni mereka; sesungguhnya Allah tidak akan memberi petunjuk kepada orang-orang yang fasik.

63:7

# هُمُ الَّذِيْنَ يَقُوْلُوْنَ لَا تُنْفِقُوْا عَلٰى مَنْ عِنْدَ رَسُوْلِ اللّٰهِ حَتّٰى يَنْفَضُّوْاۗ وَلِلّٰهِ خَزَاۤىِٕنُ السَّمٰوٰتِ وَالْاَرْضِۙ وَلٰكِنَّ الْمُنٰفِقِيْنَ لَا يَفْقَهُوْنَ

humu **al**la*dz*iina yaquuluuna l*aa* tunfiquu 'al*aa* man 'inda rasuuli **al**l*aa*hi *h*att*aa* yanfa*dhdh*uu walill*aa*hi khaz*aa*-inu **al**ssam*aa*w*aa*ti

Mereka yang berkata (kepada orang-orang Ansar), “Janganlah kamu bersedekah kepada orang-orang (Muhajirin) yang ada di sisi Rasulullah sampai mereka bubar (meninggalkan Rasulullah).” Padahal milik Allah-lah perbendaharaan langit dan bumi, tetapi orang-oran

63:8

# يَقُوْلُوْنَ لَىِٕنْ رَّجَعْنَآ اِلَى الْمَدِيْنَةِ لَيُخْرِجَنَّ الْاَعَزُّ مِنْهَا الْاَذَلَّ ۗوَلِلّٰهِ الْعِزَّةُ وَلِرَسُوْلِهٖ وَلِلْمُؤْمِنِيْنَ وَلٰكِنَّ الْمُنٰفِقِيْنَ لَا يَعْلَمُوْنَ ࣖ

yaquuluuna la-in raja'n*aa* il*aa* **a**lmadiinati layukhrijanna **a**l-a'azzu minh*aa* **a**l-a*dz*alla walill*aa*hi **a**l'izzatu walirasuulihi walilmu/miniina wal*aa*kinn

Mereka berkata, “Sungguh, jika kita kembali ke Madinah (kembali dari perang Bani Mustalik), pastilah orang yang kuat akan mengusir orang-orang yang lemah dari sana.” Padahal kekuatan itu hanyalah bagi Allah, Rasul-Nya dan bagi orang-orang mukmin, tetapi o

63:9

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْا لَا تُلْهِكُمْ اَمْوَالُكُمْ وَلَآ اَوْلَادُكُمْ عَنْ ذِكْرِ اللّٰهِ ۚوَمَنْ يَّفْعَلْ ذٰلِكَ فَاُولٰۤىِٕكَ هُمُ الْخٰسِرُوْنَ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu l*aa* tulhikum amw*aa*lukum wal*aa* awl*aa*dukum 'an *dz*ikri **al**l*aa*hi waman yaf'al *dzaa*lika faul*aa*-ika humu

Wahai orang-orang yang beriman! Janganlah harta bendamu dan anak-anakmu melalaikan kamu dari mengingat Allah. Dan barangsiapa berbuat demikian, maka mereka itulah orang-orang yang rugi.







63:10

# وَاَنْفِقُوْا مِنْ مَّا رَزَقْنٰكُمْ مِّنْ قَبْلِ اَنْ يَّأْتِيَ اَحَدَكُمُ الْمَوْتُ فَيَقُوْلَ رَبِّ لَوْلَآ اَخَّرْتَنِيْٓ اِلٰٓى اَجَلٍ قَرِيْبٍۚ فَاَصَّدَّقَ وَاَكُنْ مِّنَ الصّٰلِحِيْنَ

wa-anfiquu min m*aa* razaqn*aa*kum min qabli an ya/tiya a*h*adakumu **a**lmawtu fayaquula rabbi lawl*aa* akhkhartanii il*aa* ajalin qariibin fa-a*shsh*addaqa wa-akun mina **al***shshaa*li*h*

*Dan infakkanlah sebagian dari apa yang telah Kami berikan kepadamu sebelum kematian datang kepada salah seorang di antara kamu; lalu dia berkata (menyesali), “Ya Tuhanku, sekiranya Engkau berkenan menunda (kematian)ku sedikit waktu lagi, maka aku dapat be*









63:11

# وَلَنْ يُّؤَخِّرَ اللّٰهُ نَفْسًا اِذَا جَاۤءَ اَجَلُهَاۗ وَاللّٰهُ خَبِيْرٌۢ بِمَا تَعْمَلُوْنَ ࣖ

walan yu-akhkhira **al**l*aa*hu nafsan i*dzaa* j*aa*-a ajaluh*aa* wa**al**l*aa*hu khabiirun bim*aa* ta'maluun**a**

Dan Allah tidak akan menunda (kematian) seseorang apabila waktu kematiannya telah datang. Dan Allah Mahateliti apa yang kamu kerjakan.

<!--EndFragment-->