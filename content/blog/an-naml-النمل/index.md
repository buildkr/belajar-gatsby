---
title: (27) An-Naml - النمل
date: 2021-10-27T03:53:35.576Z
ayat: 27
description: "Jumlah Ayat: 93 / Arti: Semut-semut"
---
<!--StartFragment-->

27:1

# طٰسۤ ۚ تِلْكَ اٰيٰتُ الْقُرْاٰنِ وَكِتَابٍ مُّبِيْنٍ ۙ

*thaa*-siin tilka *aa*y*aa*tu **a**lqur-*aa*ni wakit*aa*bin mubiin**in**

Tha Sin. Inilah ayat-ayat Al-Qur'an, dan Kitab yang jelas,

27:2

# هُدًى وَّبُشْرٰى لِلْمُؤْمِنِيْنَ ۙ

hudan wabusyr*aa* lilmu/miniin**a**

petunjuk dan berita gembira bagi orang-orang yang beriman,

27:3

# الَّذِيْنَ يُقِيْمُوْنَ الصَّلٰوةَ وَيُؤْتُوْنَ الزَّكٰوةَ وَهُمْ بِالْاٰخِرَةِ هُمْ يُوْقِنُوْنَ

**al**la*dz*iina yuqiimuuna **al***shsh*al*aa*ta wayu/tuuna **al**zzak*aa*ta wahum bi**a**l-*aa*khirati hum yuuqinuun**a**

(yaitu) orang-orang yang melaksanakan salat dan menunaikan zakat, dan mereka meyakini adanya akhirat.

27:4

# اِنَّ الَّذِيْنَ لَا يُؤْمِنُوْنَ بِالْاٰخِرَةِ زَيَّنَّا لَهُمْ اَعْمَالَهُمْ فَهُمْ يَعْمَهُوْنَ ۗ

inna **al**la*dz*iina l*aa* yu/minuuna bi**a**l-*aa*khirati zayyann*aa* lahum a'm*aa*lahum fahum ya'mahuun**a**

Sesungguhnya orang-orang yang tidak beriman kepada akhirat, Kami jadikan terasa indah bagi mereka perbuatan-perbuatan mereka (yang buruk), sehingga mereka bergelimang dalam kesesatan.

27:5

# اُولٰۤىِٕكَ الَّذِيْنَ لَهُمْ سُوْۤءُ الْعَذَابِ وَهُمْ فِى الْاٰخِرَةِ هُمُ الْاَخْسَرُوْنَ

ul*aa*-ika **al**la*dz*iina lahum suu-u **a**l'a*dzaa*bi wahum fii **a**l-*aa*khirati humu **a**l-akhsaruun**a**

Mereka itulah orang-orang yang akan mendapat siksaan buruk (di dunia) dan mereka di akhirat adalah orang-orang yang paling rugi.

27:6

# وَاِنَّكَ لَتُلَقَّى الْقُرْاٰنَ مِنْ لَّدُنْ حَكِيْمٍ عَلِيْمٍ

wa-innaka latulaqq*aa* **a**lqur-*aa*na min ladun *h*akiimin 'aliim**in**

Dan sesungguhnya engkau (Muhammad) benar-benar telah diberi Al-Qur'an dari sisi (Allah) Yang Mahabijaksana, Maha Mengetahui.

27:7

# اِذْ قَالَ مُوْسٰى لِاَهْلِهٖٓ اِنِّيْٓ اٰنَسْتُ نَارًاۗ سَاٰتِيْكُمْ مِّنْهَا بِخَبَرٍ اَوْ اٰتِيْكُمْ بِشِهَابٍ قَبَسٍ لَّعَلَّكُمْ تَصْطَلُوْنَ

i*dz* q*aa*la muus*aa* li-ahlihi innii *aa*nastu n*aa*ran sa*aa*tiikum minh*aa* bikhabarin aw *aa*tiikum bisyih*aa*bin qabasin la'allakum ta*sth*aluun**a**

(Ingatlah) ketika Musa berkata kepada keluarganya, “Sungguh, aku melihat api. Aku akan membawa kabar tentang itu kepadamu, atau aku akan membawa suluh api (obor) kepadamu agar kamu dapat berdiang (menghangatkan badan dekat api).”

27:8

# فَلَمَّا جَاۤءَهَا نُوْدِيَ اَنْۢ بُوْرِكَ مَنْ فِى النَّارِ وَمَنْ حَوْلَهَاۗ وَسُبْحٰنَ اللّٰهِ رَبِّ الْعٰلَمِيْنَ

falamm*aa* j*aa*-ah*aa* nuudiya an buurika man fii **al**nn*aa*ri waman *h*awlah*aa* wasub*haa*na **al**l*aa*hi rabbi **a**l'*aa*lamiin**a**

Maka ketika dia tiba di sana (tempat api itu), dia diseru, “Telah diberkahi orang-orang yang berada di dekat api, dan orang-orang yang berada di sekitarnya. Mahasuci Allah, Tuhan seluruh alam.”

27:9

# يٰمُوْسٰٓى اِنَّهٗٓ اَنَا اللّٰهُ الْعَزِيْزُ الْحَكِيْمُ ۙ

y*aa* muus*aa* innahu an*aa* **al**l*aa*hu **a**l'aziizu **a**l*h*akiim**u**

(Allah berfirman), “Wahai Musa! Sesungguhnya Aku adalah Allah, Yang Mahaperkasa, Mahabijaksana.

27:10

# وَاَلْقِ عَصَاكَ ۗفَلَمَّا رَاٰهَا تَهْتَزُّ كَاَنَّهَا جَاۤنٌّ وَّلّٰى مُدْبِرًا وَّلَمْ يُعَقِّبْۗ يٰمُوْسٰى لَا تَخَفْۗ اِنِّيْ لَا يَخَافُ لَدَيَّ الْمُرْسَلُوْنَ ۖ

wa-alqi 'a*shaa*ka falamm*aa* ra*aa*h*aa* tahtazzu ka-annah*aa* j*aa*nnun wall*aa* mudbiran walam yu'aqqib y*aa* muus*aa* l*aa* takhaf innii l*aa* yakh*aa*fu ladayya **a**lmursaluun<

Dan lemparkanlah tongkatmu!” Maka ketika (tongkat itu menjadi ular dan) Musa melihatnya bergerak-gerak seperti seekor ular yang gesit, larilah dia berbalik ke belakang tanpa menoleh. ”Wahai Musa! Jangan takut! Sesungguhnya di hadapan-Ku, para rasul tidak

27:11

# اِلَّا مَنْ ظَلَمَ ثُمَّ بَدَّلَ حُسْنًاۢ بَعْدَ سُوْۤءٍ فَاِنِّيْ غَفُوْرٌ رَّحِيْمٌ

ill*aa* man *zh*alama tsumma baddala *h*usnan ba'da suu-in fa-innii ghafuurun ra*h*iim**un**

kecuali orang yang berlaku zalim yang kemudian mengubah (dirinya) dengan kebaikan setelah kejahatan (bertobat); maka sungguh, Aku Maha Pengampun, Maha Penyayang.

27:12

# وَاَدْخِلْ يَدَكَ فِيْ جَيْبِكَ تَخْرُجْ بَيْضَاۤءَ مِنْ غَيْرِ سُوْۤءٍۙ فِيْ تِسْعِ اٰيٰتٍ اِلٰى فِرْعَوْنَ وَقَوْمِهٖۚ اِنَّهُمْ كَانُوْا قَوْمًا فٰسِقِيْنَ

wa-adkhil yadaka fii jaybika takhruj bay*dhaa*-a min ghayri suu-in fii tis'i *aa*y*aa*tin il*aa* fir'awna waqawmihi innahum k*aa*nuu qawman f*aa*siqiin**a**

Dan masukkanlah tanganmu ke leher bajumu, niscaya ia akan keluar menjadi putih (bersinar) tanpa cacat. (Kedua mukjizat ini) termasuk sembilan macam mukjizat (yang akan dikemukakan) kepada Fir‘aun dan kaumnya. Mereka benar-benar orang-orang yang fasik.”

27:13

# فَلَمَّا جَاۤءَتْهُمْ اٰيٰتُنَا مُبْصِرَةً قَالُوْا هٰذَا سِحْرٌ مُّبِيْنٌ ۚ

falamm*aa* j*aa*-at-hum *aa*y*aa*tun*aa* mub*sh*iratan q*aa*luu h*aadzaa* si*h*run mubiin**un**

Maka ketika mukjizat-mukjizat Kami yang terang itu sampai kepada mereka, mereka berkata, “Ini sihir yang nyata.”

27:14

# وَجَحَدُوْا بِهَا وَاسْتَيْقَنَتْهَآ اَنْفُسُهُمْ ظُلْمًا وَّعُلُوًّاۗ فَانْظُرْ كَيْفَ كَانَ عَاقِبَةُ الْمُفْسِدِيْنَ ࣖ

waja*h*aduu bih*aa* wa**i**stayqanat-h*aa* anfusuhum *zh*ulman wa'uluwwan fa**u**n*zh*ur kayfa k*aa*na '*aa*qibatu **a**lmufsidiin**a**

Dan mereka mengingkarinya karena kezaliman dan kesombongannya, padahal hati mereka meyakini (kebenaran)nya. Maka perhatikanlah bagaimana kesudahan orang-orang yang berbuat kerusakan.

27:15

# وَلَقَدْ اٰتَيْنَا دَاوٗدَ وَسُلَيْمٰنَ عِلْمًاۗ وَقَالَا الْحَمْدُ لِلّٰهِ الَّذِيْ فَضَّلَنَا عَلٰى كَثِيْرٍ مِّنْ عِبَادِهِ الْمُؤْمِنِيْنَ

walaqad *aa*tayn*aa* d*aa*wuuda wasulaym*aa*na 'ilman waq*aa*l*aa* **a**l*h*amdu lill*aa*hi **al**la*dz*ii fa*dhdh*alan*aa* 'al*aa* katsiirin min 'ib*aa*dihi

Dan sungguh, Kami telah memberikan ilmu kepada Dawud dan Sulaiman; dan keduanya berkata, “Segala puji bagi Allah yang melebihkan kami dari banyak hamba-hamba-Nya yang beriman.”

27:16

# وَوَرِثَ سُلَيْمٰنُ دَاوٗدَ وَقَالَ يٰٓاَيُّهَا النَّاسُ عُلِّمْنَا مَنْطِقَ الطَّيْرِ وَاُوْتِيْنَا مِنْ كُلِّ شَيْءٍۗ اِنَّ هٰذَا لَهُوَ الْفَضْلُ الْمُبِيْنُ

wawaritsa sulaym*aa*nu d*aa*wuuda waq*aa*la y*aa* ayyuh*aa* **al**nn*aa*su 'ullimn*aa* man*th*iqa **al***ththh*ayri wauutiin*aa* min kulli syay-in inna h*aadzaa* lahuwa

Dan Sulaiman telah mewarisi Dawud, dan dia (Sulaiman) berkata, “Wahai manusia! Kami telah diajari bahasa burung dan kami diberi segala sesuatu. Sungguh, (semua) ini benar-benar karunia yang nyata.”

27:17

# وَحُشِرَ لِسُلَيْمٰنَ جُنُوْدُهٗ مِنَ الْجِنِّ وَالْاِنْسِ وَالطَّيْرِ فَهُمْ يُوْزَعُوْنَ

wa*h*usyira lisulaym*aa*na junuuduhu mina **a**ljinni wa**a**l-insi wa**al***ththh*ayri fahum yuuza'uun**a**

Dan untuk Sulaiman dikumpulkan bala tentaranya dari jin, manusia dan burung, lalu mereka berbaris dengan tertib.

27:18

# حَتّٰىٓ اِذَآ اَتَوْا عَلٰى وَادِ النَّمْلِۙ قَالَتْ نَمْلَةٌ يّٰٓاَيُّهَا النَّمْلُ ادْخُلُوْا مَسٰكِنَكُمْۚ لَا يَحْطِمَنَّكُمْ سُلَيْمٰنُ وَجُنُوْدُهٗۙ وَهُمْ لَا يَشْعُرُوْنَ

*h*att*aa* i*dzaa* ataw 'al*aa* w*aa*di **al**nnamli q*aa*lat namlatun y*aa* ayyuh*aa* **al**nnamlu udkhuluu mas*aa*kinakum l*aa* ya*hth*imannakum sulaym*aa*nu wajunuudu

Hingga ketika mereka sampai di lembah semut, berkatalah seekor semut, “Wahai semut-semut! Masuklah ke dalam sarang-sarangmu, agar kamu tidak diinjak oleh Sulaiman dan bala tentaranya, sedangkan mereka tidak menyadari.”

27:19

# فَتَبَسَّمَ ضَاحِكًا مِّنْ قَوْلِهَا وَقَالَ رَبِّ اَوْزِعْنِيْٓ اَنْ اَشْكُرَ نِعْمَتَكَ الَّتِيْٓ اَنْعَمْتَ عَلَيَّ وَعَلٰى وَالِدَيَّ وَاَنْ اَعْمَلَ صَالِحًا تَرْضٰىهُ وَاَدْخِلْنِيْ بِرَحْمَتِكَ فِيْ عِبَادِكَ الصّٰلِحِيْنَ

fatabassama *dah*ikan min qawlih*aa* waq*aa*la rabbi awzi'nii an asykura ni'mataka **al**latii an'amta 'alayya wa'al*aa* w*aa*lidayya wa-an a'mala *shaa*li*h*an tar*daa*hu wa-adkhilnii bira*h*matik

Maka dia (Sulaiman) tersenyum lalu tertawa karena (mendengar) perkataan semut itu. Dan dia berdoa, “Ya Tuhanku, anugerahkanlah aku ilham untuk tetap mensyukuri nikmat-Mu yang telah Engkau anugerahkan kepadaku dan kepada kedua orang tuaku dan agar aku meng

27:20

# وَتَفَقَّدَ الطَّيْرَ فَقَالَ مَا لِيَ لَآ اَرَى الْهُدْهُدَۖ اَمْ كَانَ مِنَ الْغَاۤىِٕبِيْنَ

watafaqqada **al***ththh*ayra faq*aa*la m*aa* liya l*aa* ar*aa* **a**lhudhuda am k*aa*na mina **a**lgh*aa*-ibiin**a**

Dan dia memeriksa burung-burung lalu berkata, “Mengapa aku tidak melihat Hud-hud, apakah ia termasuk yang tidak hadir?

27:21

# لَاُعَذِّبَنَّهٗ عَذَابًا شَدِيْدًا اَوْ لَاَا۟ذْبَحَنَّهٗٓ اَوْ لَيَأْتِيَنِّيْ بِسُلْطٰنٍ مُّبِيْنٍ

lau'a*dzdz*ibannahu 'a*dzaa*ban syadiidan aw la-a*dz*ba*h*annahu aw laya/tiyannii bisul*thaa*nin mubiin**in**

Pasti akan kuhukum ia dengan hukuman yang berat atau kusembelih ia, kecuali jika ia datang kepadaku dengan alasan yang jelas.”

27:22

# فَمَكَثَ غَيْرَ بَعِيْدٍ فَقَالَ اَحَطْتُّ بِمَا لَمْ تُحِطْ بِهٖ وَجِئْتُكَ مِنْ سَبَاٍ ۢبِنَبَاٍ يَّقِيْنٍ

famakatsa ghayra ba'iidin faq*aa*la a*hath*tu bim*aa* lam tu*h*i*th* bihi waji/tuka min saba-in binaba-in yaqiin**in**

Maka tidak lama kemudian (datanglah Hud-hud), lalu ia berkata, “Aku telah mengetahui sesuatu yang belum engkau ketahui. Aku datang kepadamu dari negeri Saba' membawa suatu berita yang meyakinkan.

27:23

# اِنِّيْ وَجَدْتُّ امْرَاَةً تَمْلِكُهُمْ وَاُوْتِيَتْ مِنْ كُلِّ شَيْءٍ وَّلَهَا عَرْشٌ عَظِيْمٌ

innii wajadtu imra-atan tamlikuhum wauutiyat min kulli syay-in walah*aa* 'arsyun 'a*zh*iim**un**

Sungguh, kudapati ada seorang perempuan yang memerintah mereka, dan dia dianugerahi segala sesuatu serta memiliki singgasana yang besar.

27:24

# وَجَدْتُّهَا وَقَوْمَهَا يَسْجُدُوْنَ لِلشَّمْسِ مِنْ دُوْنِ اللّٰهِ وَزَيَّنَ لَهُمُ الشَّيْطٰنُ اَعْمَالَهُمْ فَصَدَّهُمْ عَنِ السَّبِيْلِ فَهُمْ لَا يَهْتَدُوْنَۙ

wajadtuh*aa* waqawmah*aa* yasjuduuna li**l**sysyamsi min duuni **al**l*aa*hi wazayyana lahumu **al**sysyay*thaa*nu a'm*aa*lahum fa*sh*addahum 'ani **al**ssabiili fahu

Aku (burung Hud) dapati dia dan kaumnya menyembah matahari, bukan kepada Allah; dan setan telah menjadikan terasa indah bagi mereka perbuatan-perbuatan (buruk) mereka, sehingga menghalangi mereka dari jalan (Allah), maka mereka tidak mendapat petunjuk,

27:25

# اَلَّا يَسْجُدُوْا لِلّٰهِ الَّذِيْ يُخْرِجُ الْخَبْءَ فِى السَّمٰوٰتِ وَالْاَرْضِ وَيَعْلَمُ مَا تُخْفُوْنَ وَمَا تُعْلِنُوْنَ

**al**l*aa* yasjuduu lill*aa*hi **al**la*dz*ii yukhriju **a**lkhaba-a fii **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i waya'lamu m*aa* tukhfuuna wam*aa* tu'

mereka (juga) tidak menyembah Allah yang mengeluarkan apa yang terpendam di langit dan di bumi dan yang mengetahui apa yang kamu sembunyikan dan yang kamu nyatakan.

27:26

# اَللّٰهُ لَآ اِلٰهَ اِلَّا هُوَۙ رَبُّ الْعَرْشِ الْعَظِيْمِ ۩

**al**l*aa*hu l*aa* il*aa*ha ill*aa* huwa rabbu **a**l'arsyi **a**l'a*zh*iim**i**

Allah, tidak ada tuhan melainkan Dia, Tuhan yang mempunyai ‘Arsy yang agung.”

27:27

# ۞ قَالَ سَنَنْظُرُ اَصَدَقْتَ اَمْ كُنْتَ مِنَ الْكٰذِبِيْنَ

q*aa*la sanan*zh*uru a*sh*adaqta am kunta mina **a**lk*aadz*ibiin**a**

Dia (Sulaiman) berkata, “Akan kami lihat, apa kamu benar, atau termasuk yang berdusta.

27:28

# اِذْهَبْ بِّكِتٰبِيْ هٰذَا فَاَلْقِهْ اِلَيْهِمْ ثُمَّ تَوَلَّ عَنْهُمْ فَانْظُرْ مَاذَا يَرْجِعُوْنَ

i*dz*hab bikit*aa*bii h*aadzaa* fa-alqih ilayhim tsumma tawalla 'anhum fa**u**n*zh*ur m*aatsaa* yarji'uun**a**

Pergilah dengan (membawa) suratku ini, lalu jatuhkanlah kepada mereka, kemudian berpalinglah dari mereka, lalu perhatikanlah apa yang mereka bicarakan.”

27:29

# قَالَتْ يٰٓاَيُّهَا الْمَلَؤُا اِنِّيْٓ اُلْقِيَ اِلَيَّ كِتٰبٌ كَرِيْمٌ

q*aa*lat y*aa* ayyuh*aa* **a**lmalau innii ulqiya ilayya kit*aa*bun kariim**un**

Dia (Balqis) berkata, “Wahai para pembesar! Sesungguhnya telah disampaikan kepadaku sebuah surat yang mulia.”

27:30

# اِنَّهٗ مِنْ سُلَيْمٰنَ وَاِنَّهٗ بِسْمِ اللّٰهِ الرَّحْمٰنِ الرَّحِيْمِ ۙ

innahu min sulaym*aa*na wa-innahu bismi **al**l*aa*hi **al**rra*h*m*aa*ni **al**rra*h*iim**i**

Sesungguhnya (surat) itu dari Sulaiman yang isinya, “Dengan nama Allah Yang Maha Pengasih, Maha Penyayang,

27:31

# اَلَّا تَعْلُوْا عَلَيَّ وَأْتُوْنِيْ مُسْلِمِيْنَ ࣖ

**al**l*aa* ta'luu 'alayya wa/tuunii muslimiin**a**

janganlah engkau berlaku sombong terhadapku dan datanglah kepadaku sebagai orang-orang yang berserah diri.”

27:32

# قَالَتْ يٰٓاَيُّهَا الْمَلَؤُا اَفْتُوْنِيْ فِيْٓ اَمْرِيْۚ مَا كُنْتُ قَاطِعَةً اَمْرًا حَتّٰى تَشْهَدُوْنِ

q*aa*lat y*aa* ayyuh*aa* **a**lmalau aftuunii fii amrii m*aa* kuntu q*aath*i'atan amran *h*att*aa* tasyhaduun**a**

Dia (Balqis) berkata, “Wahai para pembesar! Berilah aku pertimbangan dalam perkaraku (ini). Aku tidak pernah memutuskan suatu perkara sebelum kamu hadir dalam majelis(ku).”

27:33

# قَالُوْا نَحْنُ اُولُوْا قُوَّةٍ وَّاُولُوْا بَأْسٍ شَدِيْدٍ ەۙ وَّالْاَمْرُ اِلَيْكِ فَانْظُرِيْ مَاذَا تَأْمُرِيْنَ

q*aa*luu na*h*nu uluu quwwatin wauluu ba/sin syadiidin wa**a**l-amru ilayki fa**u**n*zh*urii m*aatsaa* ta/muriin**a**

Mereka menjawab, “Kita memiliki kekuatan dan keberanian yang luar biasa (untuk berperang), tetapi keputusan berada di tanganmu; maka pertimbangkanlah apa yang akan engkau perintahkan.”

27:34

# قَالَتْ اِنَّ الْمُلُوْكَ اِذَا دَخَلُوْا قَرْيَةً اَفْسَدُوْهَا وَجَعَلُوْٓا اَعِزَّةَ اَهْلِهَآ اَذِلَّةً ۚوَكَذٰلِكَ يَفْعَلُوْنَ

q*aa*lat inna **a**lmuluuka i*dzaa* dakhaluu qaryatan afsaduuh*aa* waja'aluu a'izzata ahlih*aa* a*dz*illatan waka*dzaa*lika yaf'aluun**a**

Dia (Balqis) berkata, “Sesungguhnya raja-raja apabila menaklukkan suatu negeri, mereka tentu membinasakannya, dan menjadikan penduduknya yang mulia jadi hina; dan demikian yang akan mereka perbuat.

27:35

# وَاِنِّيْ مُرْسِلَةٌ اِلَيْهِمْ بِهَدِيَّةٍ فَنٰظِرَةٌ ۢبِمَ يَرْجِعُ الْمُرْسَلُوْنَ

wa-innii mursilatun ilayhim bihadiyyatin fan*aats*iratun bima yarji'u **a**lmursaluun**a**

Dan sungguh, aku akan mengirim utusan kepada mereka dengan (membawa) hadiah, dan (aku) akan menunggu apa yang akan dibawa kembali oleh para utusan itu.”

27:36

# فَلَمَّا جَاۤءَ سُلَيْمٰنَ قَالَ اَتُمِدُّوْنَنِ بِمَالٍ فَمَآ اٰتٰىنِ َۧ اللّٰهُ خَيْرٌ مِّمَّآ اٰتٰىكُمْۚ بَلْ اَنْتُمْ بِهَدِيَّتِكُمْ تَفْرَحُوْنَ

falamm*aa* j*aa*-a sulaym*aa*na q*aa*la atumidduunani bim*aa*lin fam*aa* *aa*t*aa*niya **al**l*aa*hu khayrun mimm*aa* *aa*t*aa*kum bal antum bihadiyyatikum tafra*h*uun**a<**

Maka ketika para (utusan itu) sampai kepada Sulaiman, dia (Sulaiman) berkata, “Apakah kamu akan memberi harta kepadaku? Apa yang Allah berikan kepadaku lebih baik daripada apa yang Allah berikan kepadamu; tetapi kamu merasa bangga dengan hadiahmu.







27:37

# اِرْجِعْ اِلَيْهِمْ فَلَنَأْتِيَنَّهُمْ بِجُنُوْدٍ لَّا قِبَلَ لَهُمْ بِهَا وَلَنُخْرِجَنَّهُمْ مِّنْهَآ اَذِلَّةً وَّهُمْ صَاغِرُوْنَ

falamm*aa* j*aa*-a sulaym*aa*na q*aa*la atumidduunani bim*aa*lin fam*aa* *aa*t*aa*niya **al**l*aa*hu khayrun mimm*aa* *aa*t*aa*kum bal antum bihadiyyatikum tafra*h*uun**a<**

Kembalilah kepada mereka! Sungguh, Kami pasti akan mendatangi mereka dengan bala tentara yang mereka tidak mampu melawannya, dan akan kami usir mereka dari negeri itu (Saba') secara terhina dan mereka akan menjadi (tawanan) yang hina dina.”







27:38

# قَالَ يٰٓاَيُّهَا الْمَلَؤُا اَيُّكُمْ يَأْتِيْنِيْ بِعَرْشِهَا قَبْلَ اَنْ يَّأْتُوْنِيْ مُسْلِمِيْنَ

q*aa*la y*aa* ayyuh*aa* **a**lmalau ayyukum ya/tiinii bi'arsyih*aa* qabla an ya/tuunii muslimiin**a**

Dia (Sulaiman) berkata, “Wahai para pembesar! Siapakah di antara kamu yang sanggup membawa singgasananya kepadaku sebelum mereka datang kepadaku menyerahkan diri?”

27:39

# قَالَ عِفْرِيْتٌ مِّنَ الْجِنِّ اَنَا۠ اٰتِيْكَ بِهٖ قَبْلَ اَنْ تَقُوْمَ مِنْ مَّقَامِكَۚ وَاِنِّيْ عَلَيْهِ لَقَوِيٌّ اَمِيْنٌ

q*aa*la 'ifriitun mina **a**ljinni an*aa* *aa*tiika bihi qabla an taquuma min maq*aa*mika wa-inne 'alayhi laqawiyyun amiin**un**

‘Ifrit dari golongan jin berkata, “Akulah yang akan membawanya kepadamu sebelum engkau berdiri dari tempat dudukmu; dan sungguh, aku kuat melakukannya dan dapat dipercaya.”

27:40

# قَالَ الَّذِيْ عِنْدَهٗ عِلْمٌ مِّنَ الْكِتٰبِ اَنَا۠ اٰتِيْكَ بِهٖ قَبْلَ اَنْ يَّرْتَدَّ اِلَيْكَ طَرْفُكَۗ فَلَمَّا رَاٰهُ مُسْتَقِرًّا عِنْدَهٗ قَالَ هٰذَا مِنْ فَضْلِ رَبِّيْۗ لِيَبْلُوَنِيْٓ ءَاَشْكُرُ اَمْ اَكْفُرُۗ وَمَنْ شَكَرَ فَاِنَّمَا يَشْكُ

q*aa*la **al**la*dz*ii 'indahu 'ilmun mina **a**lkit*aa*bi an*aa* *aa*tiika bihi qabla an yartadda ilayka *th*arfuka falamm*aa* ra*aa*hu mustaqirran 'indahu q*aa*la h*aadzaa* min

Seorang yang mempunyai ilmu dari Kitab berkata, “Aku akan membawa singgasana itu kepadamu sebelum matamu berkedip.” Maka ketika dia (Sulaiman) melihat singgasana itu terletak di hadapannya, dia pun berkata, “Ini termasuk karunia Tuhanku untuk mengujiku, a

27:41

# قَالَ نَكِّرُوْا لَهَا عَرْشَهَا نَنْظُرْ اَتَهْتَدِيْٓ اَمْ تَكُوْنُ مِنَ الَّذِيْنَ لَا يَهْتَدُوْنَ

q*aa*la nakkiruu lah*aa* 'arsyah*aa* nan*zh*ur atahtadii am takuunu mina **al**la*dz*iina l*aa* yahtaduun**a**

Dia (Sulaiman) berkata, “Ubahlah untuknya singgasananya; kita akan melihat apakah dia (Balqis) mengenal; atau tidak mengenalnya lagi.”

27:42

# فَلَمَّا جَاۤءَتْ قِيْلَ اَهٰكَذَا عَرْشُكِۗ قَالَتْ كَاَنَّهٗ هُوَۚ وَاُوْتِيْنَا الْعِلْمَ مِنْ قَبْلِهَا وَكُنَّا مُسْلِمِيْنَ

falamm*aa* j*aa*-at qiila ah*aa*ka*dzaa* 'arsyuki q*aa*lat ka-annahu huwa wauutiin*aa* **a**l'ilma min qablih*aa* wakunn*aa* muslimiin**a**

Maka ketika dia (Balqis) datang, ditanyakanlah (kepadanya), “Serupa inikah singgasanamu?” Dia (Balqis) menjawab, “Seakan-akan itulah dia.” (Dan dia Balqis berkata), “Kami telah diberi pengetahuan sebelumnya dan kami adalah orang-orang yang berserah diri (

27:43

# وَصَدَّهَا مَا كَانَتْ تَّعْبُدُ مِنْ دُوْنِ اللّٰهِ ۗاِنَّهَا كَانَتْ مِنْ قَوْمٍ كٰفِرِيْنَ

wa*sh*addah*aa* m*aa* k*aa*nat ta'budu min duuni **al**l*aa*hi innah*aa* k*aa*nat min qawmin k*aa*firiin**a**

Dan kebiasaannya menyembah selain Allah mencegahnya (untuk melahirkan keislamannya), sesungguhnya dia (Balqis) dahulu termasuk orang-orang kafir.

27:44

# قِيْلَ لَهَا ادْخُلِى الصَّرْحَۚ فَلَمَّا رَاَتْهُ حَسِبَتْهُ لُجَّةً وَّكَشَفَتْ عَنْ سَاقَيْهَاۗ قَالَ اِنَّهٗ صَرْحٌ مُّمَرَّدٌ مِّنْ قَوَارِيْرَ ەۗ قَالَتْ رَبِّ اِنِّيْ ظَلَمْتُ نَفْسِيْ وَاَسْلَمْتُ مَعَ سُلَيْمٰنَ لِلّٰهِ رَبِّ الْعٰلَمِيْنَ ࣖ

qiila lah*aa* udkhulii **al***shsh*ar*h*a falamm*aa* ra-at-hu *h*asibat-hu lujjatan wakasyafat 'an s*aa*qayh*aa* q*aa*la innahu *sh*ar*h*un mumarradun min qaw*aa*riira q*aa*lat rabbi

Dikatakan kepadanya (Balqis), “Masuklah ke dalam istana.” Maka ketika dia (Balqis) melihat (lantai istana) itu, dikiranya kolam air yang besar, dan disingkapkannya (penutup) kedua betisnya. Dia (Sulaiman) berkata, “Sesungguhnya ini hanyalah lantai istana

27:45

# وَلَقَدْ اَرْسَلْنَآ اِلٰى ثَمُوْدَ اَخَاهُمْ صٰلِحًا اَنِ اعْبُدُوا اللّٰهَ فَاِذَا هُمْ فَرِيْقٰنِ يَخْتَصِمُوْنَ

walaqad arsaln*aa* il*aa* tsamuuda akh*aa*hum *shaa*li*h*an ani u'buduu **al**l*aa*ha fa-i*dzaa* hum fariiq*aa*ni yakhta*sh*imuun**a**

Dan sungguh, Kami telah mengutus kepada (kaum) Samud saudara mereka yaitu Saleh (yang menyeru), “Sembahlah Allah!” Tetapi tiba-tiba mereka (menjadi) dua golongan yang bermusuhan.

27:46

# قَالَ يٰقَوْمِ لِمَ تَسْتَعْجِلُوْنَ بِالسَّيِّئَةِ قَبْلَ الْحَسَنَةِۚ لَوْلَا تَسْتَغْفِرُوْنَ اللّٰهَ لَعَلَّكُمْ تُرْحَمُوْنَ

q*aa*la y*aa* qawmi lima tasta'jiluuna bi**al**ssayyi-ati qabla **a**l*h*asanati lawl*aa* tastaghfiruuna **al**l*aa*ha la'allakum tur*h*amuun**a**

Dia (Saleh) berkata, “Wahai kaumku! Mengapa kamu meminta disegerakan keburukan sebelum (kamu meminta) kebaikan? Mengapa kamu tidak memohon ampunan kepada Allah, agar kamu mendapat rahmat?”

27:47

# قَالُوا اطَّيَّرْنَا بِكَ وَبِمَنْ مَّعَكَۗ قَالَ طٰۤىِٕرُكُمْ عِنْدَ اللّٰهِ بَلْ اَنْتُمْ قَوْمٌ تُفْتَنُوْنَ

q*aa*luu i*ththh*ayyarn*aa* bika wabiman ma'aka q*aa*la *thaa*-irukum 'inda **al**l*aa*hi bal antum qawmun tuftanuun**a**

Mereka menjawab, “Kami mendapat nasib yang malang disebabkan oleh kamu dan orang-orang yang bersamamu.” Dia (Saleh) berkata, “Nasibmu ada pada Allah (bukan kami yang menjadi sebab), tetapi kamu adalah kaum yang sedang diuji.”

27:48

# وَكَانَ فِى الْمَدِيْنَةِ تِسْعَةُ رَهْطٍ يُّفْسِدُوْنَ فِى الْاَرْضِ وَلَا يُصْلِحُوْنَ

wak*aa*na fii **a**lmadiinati tis'atu rah*th*in yufsiduuna fii **a**l-ar*dh*i wal*aa* yu*sh*li*h*uun**a**

Dan di kota itu ada sembilan orang laki-laki yang berbuat kerusakan di bumi, mereka tidak melakukan perbaikan.

27:49

# قَالُوْا تَقَاسَمُوْا بِاللّٰهِ لَنُبَيِّتَنَّهٗ وَاَهْلَهٗ ثُمَّ لَنَقُوْلَنَّ لِوَلِيِّهٖ مَا شَهِدْنَا مَهْلِكَ اَهْلِهٖ وَاِنَّا لَصٰدِقُوْنَ

q*aa*luu taq*aa*samuu bi**al**l*aa*hi lanubayyitannahu wa-ahlahu tsumma lanaquulanna liwaliyyihi m*aa* syahidn*aa* mahlika ahlihi wa-inn*aa* la*shaa*diquun**a**

Mereka berkata, “Bersumpahlah kamu dengan (nama) Allah, bahwa kita pasti akan menyerang dia bersama keluarganya pada malam hari, kemudian kita akan mengatakan kepada ahli warisnya (bahwa) kita tidak menyaksikan kebinasaan keluarganya itu, dan sungguh, kit

27:50

# وَمَكَرُوْا مَكْرًا وَّمَكَرْنَا مَكْرًا وَّهُمْ لَا يَشْعُرُوْنَ

wamakaruu makran wamakarn*aa* makran wahum l*aa* yasy'uruun**a**

Dan mereka membuat tipu daya, dan Kami pun menyusun tipu daya, sedang mereka tidak menyadari.

27:51

# فَانْظُرْ كَيْفَ كَانَ عَاقِبَةُ مَكْرِهِمْ اَنَّا دَمَّرْنٰهُمْ وَقَوْمَهُمْ اَجْمَعِيْنَ

fa**u**n*zh*ur kayfa k*aa*na '*aa*qibatu makrihim ann*aa* dammarn*aa*hum waqawmahum ajma'iin**a**

Maka perhatikanlah bagaimana akibat dari tipu daya mereka, bahwa Kami membinasakan mereka dan kaum mereka semuanya.

27:52

# فَتِلْكَ بُيُوْتُهُمْ خَاوِيَةً ۢبِمَا ظَلَمُوْاۗ اِنَّ فِيْ ذٰلِكَ لَاٰيَةً لِّقَوْمٍ يَّعْلَمُوْنَ

fatilka buyuutuhum kh*aa*wiyatan bim*aa* *zh*alamuu inna fii *dzaa*lika la*aa*yatan liqawmin ya'lamuun**a**

Maka itulah rumah-rumah mereka yang runtuh karena kezaliman mereka. Sungguh, pada yang demikian itu benar-benar terdapat tanda (kekuasaan Allah) bagi orang-orang yang mengetahui.

27:53

# وَاَنْجَيْنَا الَّذِيْنَ اٰمَنُوْا وَكَانُوْا يَتَّقُوْنَ

wa-anjayn*aa* **al**la*dz*iina *aa*manuu wak*aa*nuu yattaquun**a**

Dan Kami selamatkan orang-orang yang beriman dan mereka selalu bertakwa.

27:54

# وَلُوْطًا اِذْ قَالَ لِقَوْمِهٖٓ اَتَأْتُوْنَ الْفَاحِشَةَ وَاَنْتُمْ تُبْصِرُوْنَ

waluu*th*an i*dz* q*aa*la liqawmihi ata/tuuna **a**lf*aah*isyata wa-antum tub*sh*iruun**a**

Dan (ingatlah kisah) Lut, ketika dia berkata kepada kaumnya, “Mengapa kamu mengerjakan perbuatan fahisyah (keji), padahal kamu melihatnya (kekejian perbuatan maksiat itu)?”

27:55

# اَىِٕنَّكُمْ لَتَأْتُوْنَ الرِّجَالَ شَهْوَةً مِّنْ دُوْنِ النِّسَاۤءِ ۗبَلْ اَنْتُمْ قَوْمٌ تَجْهَلُوْنَ

a-innakum lata/tuuna **al**rrij*aa*la syahwatan min duuni **al**nnis*aa*-i bal antum qawmun tajhaluun**a**

Mengapa kamu mendatangi laki-laki untuk (memenuhi) syahwat(mu), bukan (mendatangi) perempuan? Sungguh, kamu adalah kaum yang tidak mengetahui (akibat perbuatanmu).

27:56

# ۞ فَمَا كَانَ جَوَابَ قَوْمِهٖٓ اِلَّآ اَنْ قَالُوْٓا اَخْرِجُوْٓا اٰلَ لُوْطٍ مِّنْ قَرْيَتِكُمْۙ اِنَّهُمْ اُنَاسٌ يَّتَطَهَّرُوْنَ

fam*aa* k*aa*na jaw*aa*ba qawmihi ill*aa* an q*aa*luu akhrijuu *aa*la luu*th*in min qaryatikum innahum un*aa*sun yata*th*ahharuun**a**

Jawaban kaumnya tidak lain hanya dengan mengatakan, “Usirlah Lut dan keluarganya dari negerimu; sesungguhnya mereka adalah orang-orang yang (menganggap dirinya) suci.”

27:57

# فَاَنْجَيْنٰهُ وَاَهْلَهٗٓ اِلَّا امْرَاَتَهٗ قَدَّرْنٰهَا مِنَ الْغٰبِرِيْنَ

fa-anjayn*aa*hu wa-ahlahu ill*aa* imra-atahu qaddarn*aa*h*aa* mina **a**lgh*aa*biriin**a**

Maka Kami selamatkan dia dan keluarganya, kecuali istrinya. Kami telah menentukan dia termasuk orang-orang yang tertinggal (dibinasakan).

27:58

# وَاَمْطَرْنَا عَلَيْهِمْ مَّطَرًاۚ فَسَاۤءَ مَطَرُ الْمُنْذَرِيْنَ ࣖ

wa-am*th*arn*aa* 'alayhim ma*th*aran fas*aa*-a ma*th*aru **a**lmun*dz*ariin**a**

Dan Kami hujani mereka dengan hujan (batu), maka sangat buruklah hujan (yang ditimpakan) pada orang-orang yang diberi peringatan itu (tetapi tidak mengindahkan).

27:59

# قُلِ الْحَمْدُ لِلّٰهِ وَسَلٰمٌ عَلٰى عِبَادِهِ الَّذِيْنَ اصْطَفٰىۗ ءٰۤاللّٰهُ خَيْرٌ اَمَّا يُشْرِكُوْنَ ۔

quli **a**l*h*amdu lill*aa*hi wasal*aa*mun 'al*aa* 'ib*aa*dihi **al**la*dz*iina i*sth*af*aa* *aa*ll*aa*hu khayrun amm*aa* yusyrikuun**a**

Katakanlah (Muhammad), “Segala puji bagi Allah dan salam sejahtera atas hamba-hamba-Nya yang dipilih-Nya. Apakah Allah yang lebih baik, ataukah apa yang mereka persekutukan (dengan Dia)?”

27:60

# اَمَّنْ خَلَقَ السَّمٰوٰتِ وَالْاَرْضَ وَاَنْزَلَ لَكُمْ مِّنَ السَّمَاۤءِ مَاۤءً فَاَنْۢبَتْنَا بِهٖ حَدَاۤىِٕقَ ذَاتَ بَهْجَةٍۚ مَا كَانَ لَكُمْ اَنْ تُنْۢبِتُوْا شَجَرَهَاۗ ءَاِلٰهٌ مَّعَ اللّٰهِ ۗبَلْ هُمْ قَوْمٌ يَّعْدِلُوْنَ ۗ

amman khalaqa **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*a wa-anzala lakum mina **al**ssam*aa*-i m*aa*-an fa-anbatn*aa* bihi *h*ad*aa*-iqa *dzaa*ta bahjatin m*aa* k*aa*

*Bukankah Dia (Allah) yang menciptakan langit dan bumi dan yang menurunkan air dari langit untukmu, lalu Kami tumbuhkan dengan air itu kebun-kebun yang berpemandangan indah? Kamu tidak akan mampu menumbuhkan pohon-pohonnya. Apakah di samping Allah ada tuha*









27:61

# اَمَّنْ جَعَلَ الْاَرْضَ قَرَارًا وَّجَعَلَ خِلٰلَهَآ اَنْهٰرًا وَّجَعَلَ لَهَا رَوَاسِيَ وَجَعَلَ بَيْنَ الْبَحْرَيْنِ حَاجِزًاۗ ءَاِلٰهٌ مَّعَ اللّٰهِ ۗبَلْ اَكْثَرُهُمْ لَا يَعْلَمُوْنَ ۗ

amman ja'ala **a**l-ar*dh*a qar*aa*ran waja'ala khil*aa*lah*aa* anh*aa*ran waja'ala lah*aa* raw*aa*siya waja'ala bayna **a**lba*h*rayni *haa*jizan a-il*aa*hun ma'a **al**

**Bukankah Dia (Allah) yang telah menjadikan bumi sebagai tempat berdiam, yang menjadikan sungai-sungai di celah-celahnya, yang menjadikan gunung-gunung untuk (mengokohkan)nya dan yang menjadikan suatu pemisah antara dua laut? Apakah di samping Allah ada tu**









27:62

# اَمَّنْ يُّجِيْبُ الْمُضْطَرَّ اِذَا دَعَاهُ وَيَكْشِفُ السُّوْۤءَ وَيَجْعَلُكُمْ خُلَفَاۤءَ الْاَرْضِۗ ءَاِلٰهٌ مَّعَ اللّٰهِ ۗقَلِيْلًا مَّا تَذَكَّرُوْنَۗ

amman yujiibu **a**lmu*dth*arra i*dzaa* da'*aa*hu wayaksyifu **al**ssuu-a wayaj'alukum khulaf*aa*-a **a**l-ar*dh*i a-il*aa*hun ma'a **al**l*aa*hi qaliilan m*aa* ta

Bukankah Dia (Allah) yang memperkenankan (doa) orang yang dalam kesulitan apabila dia berdoa kepada-Nya, dan menghilangkan kesusahan dan menjadikan kamu (manusia) sebagai khalifah (pemimpin) di bumi? Apakah di samping Allah ada tuhan (yang lain)? Sedikit

27:63

# اَمَّنْ يَّهْدِيْكُمْ فِيْ ظُلُمٰتِ الْبَرِّ وَالْبَحْرِ وَمَنْ يُّرْسِلُ الرِّيٰحَ بُشْرًا ۢ بَيْنَ يَدَيْ رَحْمَتِهٖۗ ءَاِلٰهٌ مَّعَ اللّٰهِ ۗتَعٰلَى اللّٰهُ عَمَّا يُشْرِكُوْنَ

amman yahdiikum fii *zh*ulum*aa*ti **a**lbarri wa**a**lba*h*ri waman yursilu **al**rriy*aah*a busyran bayna yaday ra*h*matihi a-il*aa*hun ma'a **al**l*aa*hi ta'*aa*

*Bukankah Dia (Allah) yang memberi petunjuk kepada kamu dalam kegelapan di daratan dan lautan dan yang mendatangkan angin sebagai kabar gembira sebelum (kedatangan) rahmat-Nya? Apakah di samping Allah ada tuhan (yang lain)? Mahatinggi Allah terhadap apa ya*









27:64

# اَمَّنْ يَّبْدَؤُا الْخَلْقَ ثُمَّ يُعِيْدُهٗ وَمَنْ يَّرْزُقُكُمْ مِّنَ السَّمَاۤءِ وَالْاَرْضِۗ ءَاِلٰهٌ مَّعَ اللّٰهِ ۗقُلْ هَاتُوْا بُرْهَانَكُمْ اِنْ كُنْتُمْ صٰدِقِيْنَ

amman yabdau **a**lkhalqa tsumma yu'iiduhu waman yarzuqukum mina **al**ssam*aa*-i wa**a**l-ar*dh*i a-il*aa*hun ma'a **al**l*aa*hi qul h*aa*tuu burh*aa*nakum in kuntum *sha*

Bukankah Dia (Allah) yang menciptakan (makhluk) dari permulaannya, kemudian mengulanginya (lagi) dan yang memberikan rezeki kepadamu dari langit dan bumi? Apakah di samping Allah ada tuhan (yang lain)? Katakanlah, “Kemukakanlah bukti kebenaranmu, jika kam







27:65

# قُلْ لَّا يَعْلَمُ مَنْ فِى السَّمٰوٰتِ وَالْاَرْضِ الْغَيْبَ اِلَّا اللّٰهُ ۗوَمَا يَشْعُرُوْنَ اَيَّانَ يُبْعَثُوْنَ

qul l*aa* ya'lamu man fii **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i **a**lghayba ill*aa* **al**l*aa*hu wam*aa* yasy'uruuna ayy*aa*na yub'atsuun**a**

Katakanlah (Muhammad), “Tidak ada sesuatu pun di langit dan di bumi yang mengetahui perkara yang gaib, kecuali Allah. Dan mereka tidak mengetahui kapan mereka akan dibangkitkan.”

27:66

# بَلِ ادّٰرَكَ عِلْمُهُمْ فِى الْاٰخِرَةِۗ بَلْ هُمْ فِيْ شَكٍّ مِّنْهَاۗ بَلْ هُمْ مِّنْهَا عَمُوْنَ ࣖ

bali idd*aa*raka 'ilmuhum fii **a**l-*aa*khirati bal hum fii syakkin minh*aa* bal hum minh*aa* 'amuun**a**

Bahkan pengetahuan mereka tentang akhirat tidak sampai (ke sana). Bahkan mereka ragu-ragu tentangnya (akhirat itu). Bahkan mereka buta tentang itu.

27:67

# وَقَالَ الَّذِيْنَ كَفَرُوْٓا ءَاِذَا كُنَّا تُرَابًا وَّاٰبَاۤؤُنَآ اَىِٕنَّا لَمُخْرَجُوْنَ

waq*aa*la **al**la*dz*iina kafaruu a-i*dzaa* kunn*aa* tur*aa*ban wa*aa*b*aa*un*aa* a-inn*aa* lamukhrajuun**a**

Dan orang-orang yang kafir berkata, “Setelah kita menjadi tanah dan (begitu pula) nenek moyang kita, apakah benar kita akan dikeluarkan (dari kubur)?

27:68

# لَقَدْ وُعِدْنَا هٰذَا نَحْنُ وَاٰبَاۤؤُنَا مِنْ قَبْلُۙ اِنْ هٰذَآ اِلَّآ اَسَاطِيْرُ الْاَوَّلِيْنَ

laqad wu'idn*aa* h*aadzaa* na*h*nu wa*aa*b*aa*un*aa* min qablu in h*aadzaa* ill*aa* as*aath*iiru **a**l-awwaliin**a**

Sejak dahulu kami telah diberi ancaman dengan ini (hari kebangkitan); kami dan nenek moyang kami. Sebenarnya ini hanyalah dongeng orang-orang terdahulu.”

27:69

# قُلْ سِيْرُوْا فِى الْاَرْضِ فَانْظُرُوْا كَيْفَ كَانَ عَاقِبَةُ الْمُجْرِمِيْنَ

qul siiruu fii **a**l-ar*dh*i fa**u**n*zh*uruu kayfa k*aa*na '*aa*qibatu **a**lmujrimiin**a**

Katakanlah (Muhammad), “Berjalanlah kamu di bumi, lalu perhatikanlah bagaimana kesudahan orang-orang yang berdosa.

27:70

# وَلَا تَحْزَنْ عَلَيْهِمْ وَلَا تَكُنْ فِيْ ضَيْقٍ مِّمَّا يَمْكُرُوْنَ

wal*aa* ta*h*zan 'alayhim wal*aa* takun fii *dh*ayqin mimm*aa* yamkuruun**a**

Dan janganlah engkau bersedih hati terhadap mereka, dan janganlah (dadamu) merasa sempit terhadap upaya tipu daya mereka.”

27:71

# وَيَقُوْلُوْنَ مَتٰى هٰذَا الْوَعْدُ اِنْ كُنْتُمْ صٰدِقِيْنَ

wayaquuluuna mat*aa* h*aadzaa* **a**lwa'du in kuntum *shaa*diqiin**a**

Dan mereka (orang kafir) berkata, “Kapankah datangnya janji (azab) itu, jika kamu orang yang benar.”

27:72

# قُلْ عَسٰٓى اَنْ يَّكُوْنَ رَدِفَ لَكُمْ بَعْضُ الَّذِيْ تَسْتَعْجِلُوْنَ

qul 'as*aa* an yakuuna radifa lakum ba'*dh*u **al**la*dz*ii tasta'jiluun**a**

Katakanlah (Muhammad), “Boleh jadi sebagian dari (azab) yang kamu minta disegerakan itu telah hampir sampai kepadamu.”

27:73

# وَاِنَّ رَبَّكَ لَذُوْ فَضْلٍ عَلَى النَّاسِ وَلٰكِنَّ اَكْثَرَهُمْ لَا يَشْكُرُوْنَ

wa-inna rabbaka la*dz*uu fa*dh*lin 'al*aa* **al**nn*aa*si wal*aa*kinna aktsarahum l*aa* yasykuruun**a**

Dan sungguh, Tuhanmu benar-benar memiliki karunia (yang diberikan-Nya) kepada manusia, tetapi kebanyakan mereka tidak mensyukuri(nya).

27:74

# وَاِنَّ رَبَّكَ لَيَعْلَمُ مَا تُكِنُّ صُدُوْرُهُمْ وَمَا يُعْلِنُوْنَ

wa-inna rabbaka laya'lamu m*aa* tukinnu *sh*uduuruhum wam*aa* yu'linuun**a**

Dan sungguh, Tuhanmu mengetahui apa yang disembunyikan dalam dada mereka dan apa yang mereka nyatakan.

27:75

# وَمَا مِنْ غَاۤىِٕبَةٍ فِى السَّمَاۤءِ وَالْاَرْضِ اِلَّا فِيْ كِتٰبٍ مُّبِيْنٍ

wam*aa* min gh*aa*-ibatin fii **al**ssam*aa*-i wa**a**l-ar*dh*i ill*aa* fii kit*aa*bin mubiin**in**

Dan tidak ada sesuatu pun yang tersembunyi di langit dan di bumi, melainkan (tercatat) dalam Kitab yang jelas (Lauh Mahfuzh).

27:76

# اِنَّ هٰذَا الْقُرْاٰنَ يَقُصُّ عَلٰى بَنِيْٓ اِسْرَاۤءِيْلَ اَكْثَرَ الَّذِيْ هُمْ فِيْهِ يَخْتَلِفُوْنَ

inna h*aadzaa* **a**lqur-*aa*na yaqu*shsh*u 'al*aa* banii isr*aa*-iila aktsara **al**la*dz*ii hum fiihi yakhtalifuun**a**

Sungguh, Al-Qur'an ini menjelaskan kepada Bani Israil sebagian besar dari (perkara) yang mereka perselisihkan.

27:77

# وَاِنَّهٗ لَهُدًى وَّرَحْمَةٌ لِّلْمُؤْمِنِيْنَ

wa-innahu lahudan wara*h*matun lilmu/miniin**a**

Dan sungguh, (Al-Qur'an) itu benar-benar menjadi petunjuk dan rahmat bagi orang-orang yang beriman.

27:78

# اِنَّ رَبَّكَ يَقْضِيْ بَيْنَهُمْ بِحُكْمِهٖۚ وَهُوَ الْعَزِيْزُ الْعَلِيْمُۚ

inna rabbaka yaq*dh*ii baynahum bi*h*ukmihi wahuwa **a**l'aziizu **a**l'aliim**u**

Sungguh, Tuhanmu akan menyelesaikan (perkara) di antara mereka dengan hukum-Nya, dan Dia Mahaperkasa, Maha Mengetahui.

27:79

# فَتَوَكَّلْ عَلَى اللّٰهِ ۗاِنَّكَ عَلَى الْحَقِّ الْمُبِيْنِ

fatawakkal 'al*aa* **al**l*aa*hi innaka 'al*aa* **a**l*h*aqqi **a**lmubiin**i**

Maka bertawakallah kepada Allah, sungguh engkau (Muhammad) berada di atas kebenaran yang nyata.

27:80

# اِنَّكَ لَا تُسْمِعُ الْمَوْتٰى وَلَا تُسْمِعُ الصُّمَّ الدُّعَاۤءَ اِذَا وَلَّوْا مُدْبِرِيْنَ

innaka l*aa* tusmi'u **a**lmawt*aa* wal*aa* tusmi'u **al***shsh*umma **al**ddu'*aa*-a i*dzaa* wallaw mudbiriin**a**

Sungguh, engkau tidak dapat menjadikan orang yang mati dapat mendengar dan (tidak pula) menjadikan orang yang tuli dapat mendengar seruan, apabila mereka telah berpaling ke belakang.

27:81

# وَمَآ اَنْتَ بِهٰدِى الْعُمْيِ عَنْ ضَلٰلَتِهِمْۗ اِنْ تُسْمِعُ اِلَّا مَنْ يُّؤْمِنُ بِاٰيٰتِنَا فَهُمْ مُّسْلِمُوْنَ

wam*aa* anta bih*aa*dii **a**l'umyi 'an *dh*al*aa*latihim in tusmi'u ill*aa* man yu/minu bi-*aa*y*aa*tin*aa* fahum muslimuun**a**

Dan engkau tidak akan dapat memberi petunjuk orang buta dari kesesatannya. Engkau tidak dapat menjadikan (seorang pun) mendengar, kecuali orang-orang yang beriman kepada ayat-ayat Kami, lalu mereka berserah diri.

27:82

# ۞ وَاِذَا وَقَعَ الْقَوْلُ عَلَيْهِمْ اَخْرَجْنَا لَهُمْ دَاۤبَّةً مِّنَ الْاَرْضِ تُكَلِّمُهُمْ اَنَّ النَّاسَ كَانُوْا بِاٰيٰتِنَا لَا يُوْقِنُوْنَ ࣖ

wa-i*dzaa* waqa'a **a**lqawlu 'alayhim akhrajn*aa* lahum d*aa*bbatan mina **a**l-ar*dh*i tukallimuhum anna **al**nn*aa*sa k*aa*nuu bi-*aa*y*aa*tin*aa* l*aa* yuuqinuun

Dan apabila perkataan (ketentuan masa kehancuran alam) telah berlaku atas mereka, Kami keluarkan makhluk bergerak yang bernyawa dari bumi yang akan mengatakan kepada mereka bahwa manusia dahulu tidak yakin kepada ayat-ayat Kami.

27:83

# وَيَوْمَ نَحْشُرُ مِنْ كُلِّ اُمَّةٍ فَوْجًا مِّمَّنْ يُّكَذِّبُ بِاٰيٰتِنَا فَهُمْ يُوْزَعُوْنَ

wayawma na*h*syuru min kulli ummatin fawjan mimman yuka*dzdz*ibu bi-*aa*y*aa*tin*aa* fahum yuuza'uun**a**

Dan (ingatlah) pada hari (ketika) Kami mengumpulkan dari setiap umat, segolongan orang yang mendustakan ayat-ayat Kami, lalu mereka dibagi-bagi (dalam kelompok-kelompok).

27:84

# حَتّٰٓى اِذَا جَاۤءُوْ قَالَ اَكَذَّبْتُمْ بِاٰيٰتِيْ وَلَمْ تُحِيْطُوْا بِهَا عِلْمًا اَمَّاذَا كُنْتُمْ تَعْمَلُوْنَ

*h*att*aa* i*dzaa* j*aa*uu q*aa*la aka*dzdz*abtum bi-*aa*y*aa*tii walam tu*h*ii*th*uu bih*aa* 'ilman amm*aatsaa* kuntum ta'maluun**a**

Hingga apabila mereka datang, Dia (Allah) berfirman, “Mengapa kamu telah mendustakan ayat-ayat-Ku, pada-hal kamu tidak mempunyai pengetahuan tentang itu, atau apakah yang telah kamu kerjakan?”

27:85

# وَوَقَعَ الْقَوْلُ عَلَيْهِمْ بِمَا ظَلَمُوْا فَهُمْ لَا يَنْطِقُوْنَ

wawaqa'a **a**lqawlu 'alayhim bim*aa* *zh*alamuu fahum l*aa* yan*th*iquun**a**

Dan berlakulah perkataan (janji azab) atas mereka karena kezaliman mereka, maka mereka tidak dapat berkata.

27:86

# اَلَمْ يَرَوْا اَنَّا جَعَلْنَا الَّيْلَ لِيَسْكُنُوْا فِيْهِ وَالنَّهَارَ مُبْصِرًاۗ اِنَّ فِيْ ذٰلِكَ لَاٰيٰتٍ لِّقَوْمٍ يُّؤْمِنُوْنَ

alam yaraw ann*aa* ja'aln*aa* **al**layla liyaskunuu fiihi wa**al**nnah*aa*ra mub*sh*iran inna fii *dzaa*lika la*aa*y*aa*tin liqawmin yu/minuun**a**

Apakah mereka tidak memperhatikan bahwa Kami telah menjadikan malam agar mereka beristirahat padanya dan (menjadikan) siang yang menerangi? Sungguh, pada yang demikian itu terdapat tanda-tanda (kebesaran Allah) bagi orang-orang yang beriman.

27:87

# وَيَوْمَ يُنْفَخُ فِى الصُّوْرِ فَفَزِعَ مَنْ فِى السَّمٰوٰتِ وَمَنْ فِى الْاَرْضِ اِلَّا مَنْ شَاۤءَ اللّٰهُ ۗوَكُلٌّ اَتَوْهُ دَاخِرِيْنَ

wayawma yunfakhu fii **al***shsh*uuri fafazi'a man fii **al**ssam*aa*w*aa*ti waman fii **a**l-ar*dh*i ill*aa* man sy*aa*-a **al**l*aa*hu wakullun atawhu d*aa*khiri

Dan (ingatlah) pada hari (ketika) sangkakala ditiup, maka terkejutlah apa yang ada di langit dan apa yang ada di bumi, kecuali siapa yang dikehendaki Allah. Dan semua mereka datang menghadap-Nya dengan merendahkan diri.

27:88

# وَتَرَى الْجِبَالَ تَحْسَبُهَا جَامِدَةً وَّهِيَ تَمُرُّ مَرَّ السَّحَابِۗ صُنْعَ اللّٰهِ الَّذِيْٓ اَتْقَنَ كُلَّ شَيْءٍۗ اِنَّهٗ خَبِيْرٌ ۢبِمَا تَفْعَلُوْنَ

watar*aa* **a**ljib*aa*la ta*h*sabuh*aa* j*aa*midatan wahiya tamurru marra **al**ssa*haa*bi *sh*un'a **al**l*aa*hi **al**la*dz*ii atqana kulla syay-in innahu

Dan engkau akan melihat gunung-gunung, yang engkau kira tetap di tempatnya, padahal ia berjalan (seperti) awan berjalan. (Itulah) ciptaan Allah yang mencipta dengan sempurna segala sesuatu. Sungguh, Dia Mahateliti apa yang kamu kerjakan.

27:89

# مَنْ جَاۤءَ بِالْحَسَنَةِ فَلَهٗ خَيْرٌ مِّنْهَاۚ وَهُمْ مِّنْ فَزَعٍ يَّوْمَىِٕذٍ اٰمِنُوْنَ

man j*aa*-a bi**a**l*h*asanati falahu khayrun minh*aa* wahum min faza'in yawma-i*dz*in *aa*minuun**a**

Barangsiapa membawa kebaikan, maka dia memperoleh (balasan) yang lebih baik daripadanya, sedang mereka merasa aman dari kejutan (yang dahsyat) pada hari itu.

27:90

# وَمَنْ جَاۤءَ بِالسَّيِّئَةِ فَكُبَّتْ وُجُوْهُهُمْ فِى النَّارِۗ هَلْ تُجْزَوْنَ اِلَّا مَا كُنْتُمْ تَعْمَلُوْنَ

waman j*aa*-a bi**al**ssayyi-ati fakubbat wujuuhuhum fii **al**nn*aa*ri hal tujzawna ill*aa* m*aa* kuntum ta'maluun**a**

Dan barangsiapa membawa kejahatan, maka disungkurkanlah wajah mereka ke dalam neraka. Kamu tidak diberi balasan, melainkan (setimpal) dengan apa yang telah kamu kerjakan.

27:91

# اِنَّمَآ اُمِرْتُ اَنْ اَعْبُدَ رَبَّ هٰذِهِ الْبَلْدَةِ الَّذِيْ حَرَّمَهَا وَلَهٗ كُلُّ شَيْءٍ وَّاُمِرْتُ اَنْ اَكُوْنَ مِنَ الْمُسْلِمِيْنَ ۙ

innam*aa* umirtu an a'buda rabba h*aadz*ihi **a**lbaldati **al**la*dz*ii *h*arramah*aa* walahu kullu syay-in waumirtu an akuuna mina **a**lmuslimiin**a**

Aku (Muhammad) hanya diperintahkan menyembah Tuhan negeri ini (Mekah) yang Dia telah menjadikan suci padanya dan segala sesuatu adalah milik-Nya. Dan aku diperintahkan agar aku termasuk orang Muslim,

27:92

# وَاَنْ اَتْلُوَا الْقُرْاٰنَ ۚفَمَنِ اهْتَدٰى فَاِنَّمَا يَهْتَدِيْ لِنَفْسِهٖۚ وَمَنْ ضَلَّ فَقُلْ اِنَّمَآ اَنَا۠ مِنَ الْمُنْذِرِيْنَ

wa-an atluwa **a**lqur-*aa*na famani ihtad*aa* fa-innam*aa* yahtadii linafsihi waman *dh*alla faqul innam*aa* an*aa* mina **a**lmun*dz*iriin**a**

dan agar aku membacakan Al-Qur'an (kepada manusia). Maka barangsiapa mendapat petunjuk maka sesungguhnya dia mendapat petunjuk untuk (kebaikan) dirinya, dan barangsiapa sesat, maka katakanlah, “Sesungguhnya aku (ini) tidak lain hanyalah salah seorang pemb

27:93

# وَقُلِ الْحَمْدُ لِلّٰهِ سَيُرِيْكُمْ اٰيٰتِهٖ فَتَعْرِفُوْنَهَاۗ وَمَا رَبُّكَ بِغَافِلٍ عَمَّا تَعْمَلُوْنَ ࣖ

waquli **a**l*h*amdu lill*aa*hi sayuriikum *aa*y*aa*tihi fata'rifuunah*aa* wam*aa* rabbuka bigh*aa*filin 'amm*aa* ta'maluun**a**

Dan katakanlah (Muhammad), “Segala puji bagi Allah, Dia akan memperlihatkan kepadamu tanda-tanda (kebesaran)-Nya, maka kamu akan mengetahuinya. Dan Tuhanmu tidak lengah terhadap apa yang kamu kerjakan.”

<!--EndFragment-->