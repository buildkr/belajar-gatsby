---
title: (22) Al-Hajj - الحج
date: 2021-10-27T03:49:48.421Z
ayat: 22
description: "Jumlah Ayat: 78 / Arti: Haji"
---
<!--StartFragment-->

22:1

# يٰٓاَيُّهَا النَّاسُ اتَّقُوْا رَبَّكُمْۚ اِنَّ زَلْزَلَةَ السَّاعَةِ شَيْءٌ عَظِيْمٌ

y*aa* ayyuh*aa* **al**nn*aa*su ittaquu rabbakum inna zalzalata **al**ss*aa*'ati syay-un 'a*zh*iim**un**

Wahai manusia! Bertakwalah kepada Tuhanmu; sungguh, guncangan (hari) Kiamat itu adalah suatu (kejadian) yang sangat besar.

22:2

# يَوْمَ تَرَوْنَهَا تَذْهَلُ كُلُّ مُرْضِعَةٍ عَمَّآ اَرْضَعَتْ وَتَضَعُ كُلُّ ذَاتِ حَمْلٍ حَمْلَهَا وَتَرَى النَّاسَ سُكٰرٰى وَمَا هُمْ بِسُكٰرٰى وَلٰكِنَّ عَذَابَ اللّٰهِ شَدِيْدٌ

yawma tarawnah*aa* ta*dz*halu kullu mur*dh*i'atin 'amm*aa* ar*dh*a'at wata*dh*a'u kullu *dzaa*ti *h*amlin *h*amlah*aa* watar*aa* **al**nn*aa*sa suk*aa*r*aa* wam*aa* hu

(Ingatlah) pada hari ketika kamu melihatnya (goncangan itu), semua perempuan yang menyusui anaknya akan lalai terhadap anak yang disusuinya, dan setiap perempuan yang hamil akan keguguran kandungannya, dan kamu melihat manusia dalam keadaan mabuk, padahal

22:3

# وَمِنَ النَّاسِ مَنْ يُّجَادِلُ فِى اللّٰهِ بِغَيْرِ عِلْمٍ وَّيَتَّبِعُ كُلَّ شَيْطٰنٍ مَّرِيْدٍۙ

wamina **al**nn*aa*si man yuj*aa*dilu fii **al**l*aa*hi bighayri 'ilmin wayattabi'u kulla syay*thaa*nin mariid**in**

Dan di antara manusia ada yang berbantahan tentang Allah tanpa ilmu dan hanya mengikuti para setan yang sangat jahat.

22:4

# كُتِبَ عَلَيْهِ اَنَّهٗ مَنْ تَوَلَّاهُ فَاَنَّهٗ يُضِلُّهٗ وَيَهْدِيْهِ اِلٰى عَذَابِ السَّعِيْرِ

kutiba 'alayhi annahu man tawall*aa*hu fa-annahu yu*dh*illuhu wayahdiihi il*aa* 'a*dzaa*bi **al**ssa'iir**i**

(Tentang setan), telah ditetapkan bahwa siapa yang berkawan dengan dia, maka dia akan menyesatkannya, dan membawanya ke azab neraka.

22:5

# يٰٓاَيُّهَا النَّاسُ اِنْ كُنْتُمْ فِيْ رَيْبٍ مِّنَ الْبَعْثِ فَاِنَّا خَلَقْنٰكُمْ مِّنْ تُرَابٍ ثُمَّ مِنْ نُّطْفَةٍ ثُمَّ مِنْ عَلَقَةٍ ثُمَّ مِنْ مُّضْغَةٍ مُّخَلَّقَةٍ وَّغَيْرِ مُخَلَّقَةٍ لِّنُبَيِّنَ لَكُمْۗ وَنُقِرُّ فِى الْاَرْحَامِ مَا نَشَاۤء

y*aa* ayyuh*aa* **al**nn*aa*su in kuntum fii raybin mina **a**lba'tsi fa-inn*aa* khalaqn*aa*kum min tur*aa*bin tsumma min nu*th*fatin tsumma min 'alaqatin tsumma min mu*dh*ghatin mukhallaqa

Wahai manusia! Jika kamu meragukan (hari) kebangkitan, maka sesungguhnya Kami telah menjadikan kamu dari tanah, kemudian dari setetes mani, kemudian dari segumpal darah, kemudian dari segumpal daging yang sempurna kejadiannya dan yang tidak sempurna, agar

22:6

# ذٰلِكَ بِاَنَّ اللّٰهَ هُوَ الْحَقُّ وَاَنَّهٗ يُحْيِ الْمَوْتٰى وَاَنَّهٗ عَلٰى كُلِّ شَيْءٍ قَدِيْرٌ ۙ

*dzaa*lika bi-anna **al**l*aa*ha huwa **a**l*h*aqqu wa-annahu yu*h*yii **a**lmawt*aa* wa-annahu 'al*aa* kulli syay-in qadiir**un**

Yang demikian itu karena sungguh, Allah, Dialah yang hak dan sungguh, Dialah yang menghidupkan segala yang telah mati, dan sungguh, Dia Mahakuasa atas segala sesuatu.

22:7

# وَّاَنَّ السَّاعَةَ اٰتِيَةٌ لَّا رَيْبَ فِيْهَاۙ وَاَنَّ اللّٰهَ يَبْعَثُ مَنْ فِى الْقُبُوْرِ

wa-anna **al**ss*aa*'ata *aa*tiyatun l*aa* rayba fiih*aa* wa-anna **al**l*aa*ha yab'atsu man fii **a**lqubuur**i**

Dan sungguh, (hari) Kiamat itu pasti datang, tidak ada keraguan padanya; dan sungguh, Allah akan membangkitkan siapa pun yang di dalam kubur.

22:8

# وَمِنَ النَّاسِ مَنْ يُّجَادِلُ فِى اللّٰهِ بِغَيْرِ عِلْمٍ وَّلَا هُدًى وَّلَا كِتٰبٍ مُّنِيْرٍ ۙ

wamina **al**nn*aa*si man yuj*aa*dilu fii **al**l*aa*hi bighayri 'ilmin wal*aa* hudan wal*aa* kit*aa*bin muniir**in**

Dan di antara manusia ada yang berbantahan tentang Allah tanpa ilmu, tanpa petunjuk dan tanpa kitab (wahyu) yang memberi penerangan.

22:9

# ثَانِيَ عِطْفِهٖ لِيُضِلَّ عَنْ سَبِيْلِ اللّٰهِ ۗ لَهٗ فِى الدُّنْيَا خِزْيٌ وَّنُذِيْقُهٗ يَوْمَ الْقِيٰمَةِ عَذَابَ الْحَرِيْقِ

ts*aa*niya 'i*th*fihi liyu*dh*illa 'an sabiili **al**l*aa*hi lahu fii **al**dduny*aa* khizyun wanu*dz*iiquhu yawma **a**lqiy*aa*mati 'a*dzaa*ba **a**l*h*ariiq

Sambil memalingkan lambungnya (dengan congkak) untuk menyesatkan manusia dari jalan Allah. Dia mendapat kehinaan di dunia, dan pada hari Kiamat Kami berikan kepadanya rasa azab neraka yang membakar.

22:10

# ذٰلِكَ بِمَا قَدَّمَتْ يَدٰكَ وَاَنَّ اللّٰهَ لَيْسَ بِظَلَّامٍ لِّلْعَبِيْدِ ࣖ

*dzaa*lika bim*aa* qaddamat yad*aa*ka wa-anna **al**l*aa*ha laysa bi*zh*all*aa*min lil'abiid**i**

(Akan dikatakan kepadanya), “Itu karena perbuatan yang dilakukan dahulu oleh kedua tanganmu, dan Allah sekali-kali tidak menzalimi hamba-hamba-Nya.

22:11

# وَمِنَ النَّاسِ مَنْ يَّعْبُدُ اللّٰهَ عَلٰى حَرْفٍۚ فَاِنْ اَصَابَهٗ خَيْرُ ِۨاطْمَـَٔنَّ بِهٖۚ وَاِنْ اَصَابَتْهُ فِتْنَةُ ِۨانْقَلَبَ عَلٰى وَجْهِهٖۗ خَسِرَ الدُّنْيَا وَالْاٰخِرَةَۗ ذٰلِكَ هُوَ الْخُسْرَانُ الْمُبِيْنُ

wamina **al**nn*aa*si man ya'budu **al**l*aa*ha 'al*aa* *h*arfin fa-in a*shaa*bahu khayrun i*th*ma-anna bihi wa-in a*shaa*bat-hu fitnatun inqalaba 'al*aa* wajhihi khasira **al**

**Dan di antara manusia ada yang menyembah Allah hanya di tepi; maka jika dia memperoleh kebajikan, dia merasa puas, dan jika dia ditimpa suatu cobaan, dia berbalik ke belakang. Dia rugi di dunia dan di akhirat. Itulah kerugian yang nyata.**

22:12

# يَدْعُوْا مِنْ دُوْنِ اللّٰهِ مَا لَا يَضُرُّهٗ وَمَا لَا يَنْفَعُهٗۗ ذٰلِكَ هُوَ الضَّلٰلُ الْبَعِيْدُ ۚ

yad'uu min duuni **al**l*aa*hi m*aa* l*aa* ya*dh*urruhu wam*aa* l*aa* yanfa'uhu *dzaa*lika huwa **al***dhdh*al*aa*lu **a**lba'iid**u**

Dia menyeru kepada selain Allah sesuatu yang tidak dapat mendatangkan bencana dan tidak (pula) memberi manfaat kepadanya. Itulah kesesatan yang jauh.

22:13

# يَدْعُوْا لَمَنْ ضَرُّهٗٓ اَقْرَبُ مِنْ نَّفْعِهٖۗ لَبِئْسَ الْمَوْلٰى وَلَبِئْسَ الْعَشِيْرُ

yad'uu laman *dh*arruhu aqrabu min naf'ihi labi/sa **a**lmawl*aa* walabi/sa **a**l'asyiir**u**

Dia menyeru kepada sesuatu yang (sebenarnya) bencananya lebih dekat daripada manfaatnya. Sungguh, itu seburuk-buruk penolong dan sejahat-jahat kawan.

22:14

# اِنَّ اللّٰهَ يُدْخِلُ الَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِ جَنّٰتٍ تَجْرِيْ مِنْ تَحْتِهَا الْاَنْهٰرُۗ اِنَّ اللّٰهَ يَفْعَلُ مَا يُرِيْدُ

inna **al**l*aa*ha yudkhilu **al**la*dz*iina *aa*manuu wa'amiluu **al***shshaa*li*haa*ti jann*aa*tin tajrii min ta*h*tih*aa* **a**l-anh*aa*ru inna **al<**

(Sungguh,) Allah akan memasukkan orang-orang yang beriman dan mengerjakan kebajikan ke dalam surga-surga yang mengalir di bawahnya sungai-sungai. Sungguh, Allah berbuat apa yang Dia kehendaki.

22:15

# مَنْ كَانَ يَظُنُّ اَنْ لَّنْ يَّنْصُرَهُ اللّٰهُ فِى الدُّنْيَا وَالْاٰخِرَةِ فَلْيَمْدُدْ بِسَبَبٍ اِلَى السَّمَاۤءِ ثُمَّ لْيَقْطَعْ فَلْيَنْظُرْ هَلْ يُذْهِبَنَّ كَيْدُهٗ مَا يَغِيْظُ

man k*aa*na ya*zh*unnu an lan yan*sh*urahu **al**l*aa*hu fii **al**dduny*aa* wa**a**l-*aa*khirati falyamdud bisababin il*aa* **al**ssam*aa*-i tsumma liyaq*th*

Barangsiapa menyangka bahwa Allah tidak akan menolongnya (Muhammad) di dunia dan di akhirat, maka hendaklah dia merentangkan tali ke langit-langit, ) lalu menggantung (diri), kemudian pikirkanlah apakah tipu dayanya itu dapat melenyapkan apa yang menyakit

22:16

# وَكَذٰلِكَ اَنْزَلْنٰهُ اٰيٰتٍۢ بَيِّنٰتٍۙ وَّاَنَّ اللّٰهَ يَهْدِيْ مَنْ يُّرِيْدُ

waka*dzaa*lika anzaln*aa*hu *aa*y*aa*tin bayyin*aa*tin wa-anna **al**l*aa*ha yahdii man yuriid**u**

Dan demikianlah Kami telah menurunkannya (Al-Qur'an) yang merupakan ayat-ayat yang nyata; sesungguhnya Allah memberikan petunjuk kepada siapa yang Dia kehendaki.

22:17

# اِنَّ الَّذِيْنَ اٰمَنُوْا وَالَّذِيْنَ هَادُوْا وَالصَّابِـِٕيْنَ وَالنَّصٰرٰى وَالْمَجُوْسَ وَالَّذِيْنَ اَشْرَكُوْٓا ۖاِنَّ اللّٰهَ يَفْصِلُ بَيْنَهُمْ يَوْمَ الْقِيٰمَةِۗ اِنَّ اللّٰهَ عَلٰى كُلِّ شَيْءٍ شَهِيْدٌ

inna **al**la*dz*iina *aa*manuu wa**a**lla*dz*iina h*aa*duu wa**al***shshaa*bi-iina wa**al**nna*shaa*r*aa *wa**a**lmajuusa wa**a**lla*dz*

Sesungguhnya orang-orang beriman, orang Yahudi, orang Sabiin, orang Nasrani, orang Majusi dan orang musyrik, Allah pasti memberi keputusan di antara mereka pada hari Kiamat. Sungguh, Allah menjadi saksi atas segala sesuatu.

22:18

# اَلَمْ تَرَ اَنَّ اللّٰهَ يَسْجُدُ لَهٗ مَنْ فِى السَّمٰوٰتِ وَمَنْ فِى الْاَرْضِ وَالشَّمْسُ وَالْقَمَرُ وَالنُّجُوْمُ وَالْجِبَالُ وَالشَّجَرُ وَالدَّوَاۤبُّ وَكَثِيْرٌ مِّنَ النَّاسِۗ وَكَثِيْرٌ حَقَّ عَلَيْهِ الْعَذَابُۗ وَمَنْ يُّهِنِ اللّٰهُ فَمَا ل

alam tara anna **al**l*aa*ha yasjudu lahu man fii **al**ssam*aa*w*aa*ti waman fii **a**l-ar*dh*i wa**al**sysyamsu wa**a**lqamaru wa**al**nnujuumu wa**a**

Tidakkah engkau tahu bahwa siapa yang ada di langit dan siapa yang ada di bumi bersujud kepada Allah, juga matahari, bulan, bintang, gunung-gunung, pohon-pohon, hewan-hewan yang melata dan banyak di antara manusia? Tetapi banyak (manusia) yang pantas mend

22:19

# ۞ هٰذَانِ خَصْمٰنِ اخْتَصَمُوْا فِيْ رَبِّهِمْ فَالَّذِيْنَ كَفَرُوْا قُطِّعَتْ لَهُمْ ثِيَابٌ مِّنْ نَّارٍۗ يُصَبُّ مِنْ فَوْقِ رُءُوْسِهِمُ الْحَمِيْمُ ۚ

h*aadzaa*ni kha*sh*m*aa*ni ikhta*sh*amuu fii rabbihim fa**a**lla*dz*iina kafaruu qu*ththh*i'at lahum tsiy*aa*bun min n*aa*rin yu*sh*abbu min fawqi ruuusihimu **a**l*h*amiim

Inilah dua golongan (golongan mukmin dan kafir) yang bertengkar, mereka bertengkar mengenai Tuhan mereka. Maka bagi orang kafir akan dibuatkan pakaian-pakaian dari api (neraka) untuk mereka. Ke atas kepala mereka akan disiramkan air yang mendidih.

22:20

# يُصْهَرُ بِهٖ مَا فِيْ بُطُوْنِهِمْ وَالْجُلُوْدُ ۗ

yu*sh*haru bihi m*aa* fii bu*th*uunihim wa**a**ljuluud**u**

Dengan (air mendidih) itu akan dihancurluluhkan apa yang ada dalam perut dan kulit mereka.

22:21

# وَلَهُمْ مَّقَامِعُ مِنْ حَدِيْدٍ

walahum maq*aa*mi'u min *h*adiid**in**

Dan (azab) untuk mereka cambuk-cambuk dari besi.

22:22

# كُلَّمَآ اَرَادُوْٓا اَنْ يَّخْرُجُوْا مِنْهَا مِنْ غَمٍّ اُعِيْدُوْا فِيْهَا وَذُوْقُوْا عَذَابَ الْحَرِيْقِ ࣖ

kullam*aa* ar*aa*duu an yakhrujuu minh*aa* min ghammin u'iiduu fiih*aa* wa*dz*uuquu 'a*dzaa*ba **a**l*h*ariiq**i**

Setiap kali mereka hendak keluar darinya (neraka) karena tersiksa, mereka dikembalikan (lagi) ke dalamnya. (Kepada mereka dikatakan), “Rasakanlah azab yang membakar ini!”

22:23

# اِنَّ اللّٰهَ يُدْخِلُ الَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِ جَنّٰتٍ تَجْرِيْ مِنْ تَحْتِهَا الْاَنْهٰرُ يُحَلَّوْنَ فِيْهَا مِنْ اَسَاوِرَ مِنْ ذَهَبٍ وَّلُؤْلُؤًاۗ وَلِبَاسُهُمْ فِيْهَا حَرِيْرٌ

inna **al**l*aa*ha yudkhilu **al**la*dz*iina *aa*manuu wa'amiluu **al***shshaa*li*haa*ti jann*aa*tin tajrii min ta*h*tih*aa* **a**l-anh*aa*ru yu*h*allawn

Sungguh, Allah akan memasukkan orang-orang yang beriman dan mengerjakan kebajikan ke dalam surga-surga yang mengalir di bawahnya sungai-sungai. Di sana mereka diberi perhiasan gelang-gelang emas dan mutiara, dan pakaian mereka dari sutera.

22:24

# وَهُدُوْٓا اِلَى الطَّيِّبِ مِنَ الْقَوْلِۚ وَهُدُوْٓا اِلٰى صِرَاطِ الْحَمِيْدِ

wahuduu il*aa* **al***ththh*ayyibi mina **a**lqawli wahuduu il*aa* *sh*ir*aath*i **a**l*h*amiid**i**

Dan mereka diberi petunjuk kepada ucapan-ucapan yang baik dan diberi petunjuk (pula) kepada jalan (Allah) yang terpuji.

22:25

# اِنَّ الَّذِيْنَ كَفَرُوْا وَيَصُدُّوْنَ عَنْ سَبِيْلِ اللّٰهِ وَالْمَسْجِدِ الْحَرَامِ الَّذِيْ جَعَلْنٰهُ لِلنَّاسِ سَوَاۤءً ۨالْعَاكِفُ فِيْهِ وَالْبَادِۗ وَمَنْ يُّرِدْ فِيْهِ بِاِلْحَادٍۢ بِظُلْمٍ نُّذِقْهُ مِنْ عَذَابٍ اَلِيْمٍ ࣖ

inna **al**la*dz*iina kafaruu waya*sh*udduuna 'an sabiili **al**l*aa*hi wa**a**lmasjidi **a**l*h*ar*aa*mi **al**la*dz*ii ja'aln*aa*hu li**l**n

Sungguh, orang-orang kafir dan yang menghalangi (manusia) dari jalan Allah dan dari Masjidilharam yang telah Kami jadikan terbuka untuk semua manusia, baik yang bermukim di sana maupun yang datang dari luar dan siapa saja yang bermaksud melakukan kejahata

22:26

# وَاِذْ بَوَّأْنَا لِاِبْرٰهِيْمَ مَكَانَ الْبَيْتِ اَنْ لَّا تُشْرِكْ بِيْ شَيْـًٔا وَّطَهِّرْ بَيْتِيَ لِلطَّاۤىِٕفِيْنَ وَالْقَاۤىِٕمِيْنَ وَالرُّكَّعِ السُّجُوْدِ

wa-i*dz* bawwa/n*aa* li-ibr*aa*hiima mak*aa*na **a**lbayti an l*aa* tusyrik bii syay-an wa*th*ahhir baytiya li**l***ththaa*\-ifiina wa**a**lq*aa*-imiina wa**al**rru

Dan (ingatlah), ketika Kami tempatkan Ibrahim di tempat Baitullah (dengan mengatakan), “Janganlah engkau mempersekutukan Aku dengan apa pun dan sucikanlah rumah-Ku bagi orang-orang yang tawaf, dan orang yang beribadah dan orang yang rukuk dan sujud.

22:27

# وَاَذِّنْ فِى النَّاسِ بِالْحَجِّ يَأْتُوْكَ رِجَالًا وَّعَلٰى كُلِّ ضَامِرٍ يَّأْتِيْنَ مِنْ كُلِّ فَجٍّ عَمِيْقٍ ۙ

wa-a*dzdz*in fii **al**nn*aa*si bi**a**l*h*ajji ya/tuuka rij*aa*lan wa'al*aa* kulli *daa*mirin ya/tiina min kulli fajjin 'amiiq**in**

Dan serulah manusia untuk mengerjakan haji, niscaya mereka akan datang kepadamu dengan berjalan kaki, atau mengendarai setiap unta yang kurus, mereka datang dari segenap penjuru yang jauh.

22:28

# لِّيَشْهَدُوْا مَنَافِعَ لَهُمْ وَيَذْكُرُوا اسْمَ اللّٰهِ فِيْٓ اَيَّامٍ مَّعْلُوْمٰتٍ عَلٰى مَا رَزَقَهُمْ مِّنْۢ بَهِيْمَةِ الْاَنْعَامِۚ فَكُلُوْا مِنْهَا وَاَطْعِمُوا الْبَاۤىِٕسَ الْفَقِيْرَ ۖ

liyasyhaduu man*aa*fi'a lahum waya*dz*kuruu isma **al**l*aa*hi fii ayy*aa*min ma'luum*aa*tin 'al*aa* m*aa* razaqahum min bahiimati **a**l-an'*aa*mi fakuluu minh*aa* wa-a*th*'imuu

Agar mereka menyaksikan berbagai manfaat untuk mereka dan agar mere-ka menyebut nama Allah pada beberapa hari yang telah ditentukan atas rezeki yang diberikan Dia kepada mereka berupa hewan ternak. Maka makanlah sebagian darinya dan (sebagian lagi) berika

22:29

# ثُمَّ لْيَقْضُوْا تَفَثَهُمْ وَلْيُوْفُوْا نُذُوْرَهُمْ وَلْيَطَّوَّفُوْا بِالْبَيْتِ الْعَتِيْقِ

tsumma lyaq*dh*uu tafatsahum walyuufuu nu*dz*uurahum walya*ththh*awwafuu bi**a**lbayti **a**l'atiiq**i**

Kemudian, hendaklah mereka menghilangkan kotoran (yang ada di badan) mereka, menyempurnakan nazar-nazar mereka dan melakukan tawaf sekeliling rumah tua (Baitullah).

22:30

# ذٰلِكَ وَمَنْ يُّعَظِّمْ حُرُمٰتِ اللّٰهِ فَهُوَ خَيْرٌ لَّهٗ عِنْدَ رَبِّهٖۗ وَاُحِلَّتْ لَكُمُ الْاَنْعَامُ اِلَّا مَا يُتْلٰى عَلَيْكُمْ فَاجْتَنِبُوا الرِّجْسَ مِنَ الْاَوْثَانِ وَاجْتَنِبُوْا قَوْلَ الزُّوْرِ ۙ

*dzaa*lika waman yu'a*zhzh*im *h*urum*aa*ti **al**l*aa*hi fahuwa khayrun lahu 'inda rabbihi wau*h*illat lakumu **a**l-an'*aa*mu ill*aa* m*aa* yutl*aa* 'alaykum fa**i**

**Demikianlah (perintah Allah). Dan barang siapa mengagungkan apa yang terhormat di sisi Allah (hurumat) maka itu lebih baik baginya di sisi Tuhannya. Dan dihalalkan bagi kamu semua hewan ternak, kecuali yang diterangkan kepadamu (keharamannya), maka jauhil**

22:31

# حُنَفَاۤءَ لِلّٰهِ غَيْرَ مُشْرِكِيْنَ بِهٖۗ وَمَنْ يُّشْرِكْ بِاللّٰهِ فَكَاَنَّمَا خَرَّ مِنَ السَّمَاۤءِ فَتَخْطَفُهُ الطَّيْرُ اَوْ تَهْوِيْ بِهِ الرِّيْحُ فِيْ مَكَانٍ سَحِيْقٍ

*h*unaf*aa*-a lill*aa*hi ghayra musyrikiina bihi waman yusyrik bi**al**l*aa*hi faka-annam*aa* kharra mina **al**ssam*aa*-i fatakh*th*afuhu **al***ththh*ayru aw tahwii bihi

(Beribadahlah) dengan ikhlas kepada Allah, tanpa mempersekutukan-Nya. Barangsiapa mempersekutukan Allah, maka seakan-akan dia jatuh dari langit lalu disambar oleh burung, atau diterbangkan angin ke tempat yang jauh.

22:32

# ذٰلِكَ وَمَنْ يُّعَظِّمْ شَعَاۤىِٕرَ اللّٰهِ فَاِنَّهَا مِنْ تَقْوَى الْقُلُوْبِ

*dzaa*lika waman yu'a*zhzh*im sya'*aa*-ira **al**l*aa*hi fa-innah*aa* min taqw*aa* **a**lquluub**i**

Demikianlah (perintah Allah). Dan barangsiapa mengagungkan syiar-syiar Allah, maka sesungguhnya hal itu timbul dari ketakwaan hati.

22:33

# لَكُمْ فِيْهَا مَنَافِعُ اِلٰٓى اَجَلٍ مُّسَمًّى ثُمَّ مَحِلُّهَآ اِلَى الْبَيْتِ الْعَتِيْقِ ࣖ

lakum fiih*aa* man*aa*fi'u il*aa* ajalin musamman tsumma ma*h*illuh*aa* il*aa* **a**lbayti **a**l'atiiq**i**

Bagi kamu padanya (hewan hadyu) ada beberapa manfaat, sampai waktu yang ditentukan, kemudian tempat penyembelihannya adalah di sekitar Baitul Atiq (Baitullah).

22:34

# وَلِكُلِّ اُمَّةٍ جَعَلْنَا مَنْسَكًا لِّيَذْكُرُوا اسْمَ اللّٰهِ عَلٰى مَا رَزَقَهُمْ مِّنْۢ بَهِيْمَةِ الْاَنْعَامِۗ فَاِلٰهُكُمْ اِلٰهٌ وَّاحِدٌ فَلَهٗٓ اَسْلِمُوْاۗ وَبَشِّرِ الْمُخْبِتِيْنَ ۙ

walikulli ummatin ja'aln*aa* mansakan liya*dz*kuruu isma **al**l*aa*hi 'al*aa* m*aa* razaqahum min bahiimati **a**l-an'*aa*mi fa-il*aa*hukum il*aa*hun w*aah*idun falahu aslimuu wabasys

Dan bagi setiap umat telah Kami syariatkan penyembelihan (kurban), agar mereka menyebut nama Allah atas rezeki yang dikaruniakan Allah kepada mereka berupa hewan ternak. Maka Tuhanmu ialah Tuhan Yang Maha Esa, karena itu berserahdirilah kamu kepada-Nya. D

22:35

# الَّذِيْنَ اِذَا ذُكِرَ اللّٰهُ وَجِلَتْ قُلُوْبُهُمْ وَالصَّابِرِيْنَ عَلٰى مَآ اَصَابَهُمْ وَالْمُقِيْمِى الصَّلٰوةِۙ وَمِمَّا رَزَقْنٰهُمْ يُنْفِقُوْنَ

**al**la*dz*iina i*dzaa* *dz*ukira **al**l*aa*hu wajilat quluubuhum wa**al***shshaa*biriina 'al*aa *m*aa *a*shaa*bahum wa**a**lmuqiimii **al***shsh<*

(yaitu) orang-orang yang apabila disebut nama Allah hati mereka bergetar, orang yang sabar atas apa yang menimpa mereka, dan orang yang melaksanakan salat dan orang yang menginfakkan sebagian rezeki yang Kami karuniakan kepada mereka.

22:36

# وَالْبُدْنَ جَعَلْنٰهَا لَكُمْ مِّنْ شَعَاۤىِٕرِ اللّٰهِ لَكُمْ فِيْهَا خَيْرٌۖ فَاذْكُرُوا اسْمَ اللّٰهِ عَلَيْهَا صَوَاۤفَّۚ فَاِذَا وَجَبَتْ جُنُوْبُهَا فَكُلُوْا مِنْهَا وَاَطْعِمُوا الْقَانِعَ وَالْمُعْتَرَّۗ كَذٰلِكَ سَخَّرْنٰهَا لَكُمْ لَعَلَّكُمْ

wa**a**lbudna ja'aln*aa*h*aa* lakum min sya'*aa*-iri **al**l*aa*hi lakum fiih*aa* khayrun fa**u***dz*kuruu isma **al**l*aa*hi 'alayh*aa* *sh*aw*aa*ffa fa

Dan unta-unta itu Kami jadikan untuk-mu bagian dari syiar agama Allah, kamu banyak memperoleh kebaikan padanya. Maka sebutlah nama Allah (ketika kamu akan menyembelihnya) dalam keadaan berdiri (dan kaki-kaki telah terikat). Kemudian apabila telah rebah (m

22:37

# لَنْ يَّنَالَ اللّٰهَ لُحُوْمُهَا وَلَا دِمَاۤؤُهَا وَلٰكِنْ يَّنَالُهُ التَّقْوٰى مِنْكُمْۗ كَذٰلِكَ سَخَّرَهَا لَكُمْ لِتُكَبِّرُوا اللّٰهَ عَلٰى مَا هَدٰىكُمْ ۗ وَبَشِّرِ الْمُحْسِنِيْنَ

lan yan*aa*la **al**l*aa*ha lu*h*uumuh*aa* wal*aa* dim*aa*uh*aa* wal*aa*kin yan*aa*luhu **al**ttaqw*aa* minkum ka*dzaa*lika sakhkharah*aa* lakum litukabbiruu **al<**

Daging (hewan kurban) dan darahnya itu sekali-kali tidak akan sampai kepada Allah, tetapi yang sampai kepada-Nya adalah ketakwaan kamu. Demi-kianlah Dia menundukkannya untuk-mu agar kamu mengagungkan Allah atas petunjuk yang Dia berikan kepadamu. Dan samp

22:38

# ۞ اِنَّ اللّٰهَ يُدَافِعُ عَنِ الَّذِيْنَ اٰمَنُوْاۗ اِنَّ اللّٰهَ لَا يُحِبُّ كُلَّ خَوَّانٍ كَفُوْرٍ ࣖ

inna **al**l*aa*ha yud*aa*fi'u 'ani **al**la*dz*iina *aa*manuu inna **al**l*aa*ha l*aa* yu*h*ibbu kulla khaww*aa*nin kafuur**in**

Sesungguhnya Allah membela orang yang beriman. Sungguh, Allah tidak menyukai setiap orang yang berkhianat dan kufur nikmat.

22:39

# اُذِنَ لِلَّذِيْنَ يُقَاتَلُوْنَ بِاَنَّهُمْ ظُلِمُوْاۗ وَاِنَّ اللّٰهَ عَلٰى نَصْرِهِمْ لَقَدِيْرٌ ۙ

u*dz*ina lilla*dz*iina yuq*aa*taluuna bi-annahum *zh*ulimuu wa-inna **al**l*aa*ha 'al*aa* na*sh*rihim laqadiir**un**

Diizinkan (berperang) kepada orang-orang yang diperangi, karena sesungguhnya mereka dizalimi. Dan sung-guh, Allah Mahakuasa menolong mereka itu,

22:40

# ۨالَّذِيْنَ اُخْرِجُوْا مِنْ دِيَارِهِمْ بِغَيْرِ حَقٍّ اِلَّآ اَنْ يَّقُوْلُوْا رَبُّنَا اللّٰهُ ۗوَلَوْلَا دَفْعُ اللّٰهِ النَّاسَ بَعْضَهُمْ بِبَعْضٍ لَّهُدِّمَتْ صَوَامِعُ وَبِيَعٌ وَّصَلَوٰتٌ وَّمَسٰجِدُ يُذْكَرُ فِيْهَا اسْمُ اللّٰهِ كَثِيْرًاۗ وَ

**al**la*dz*iina ukhrijuu min diy*aa*rihim bighayri *h*aqqin ill*aa* an yaquuluu rabbun*aa* **al**l*aa*hu walawl*aa* daf'u **al**l*aa*hi **al**nn*aa*sa ba'

(yaitu) orang-orang yang diusir dari kampung halamannya tanpa alasan yang benar, hanya karena mereka berkata, “Tuhan kami ialah Allah.” Seandainya Allah tidak menolak (keganasan) sebagian manusia dengan sebagian yang lain, tentu telah dirobohkan biara-bia

22:41

# اَلَّذِيْنَ اِنْ مَّكَّنّٰهُمْ فِى الْاَرْضِ اَقَامُوا الصَّلٰوةَ وَاٰتَوُا الزَّكٰوةَ وَاَمَرُوْا بِالْمَعْرُوْفِ وَنَهَوْا عَنِ الْمُنْكَرِۗ وَلِلّٰهِ عَاقِبَةُ الْاُمُوْرِ

**al**la*dz*iina in makkann*aa*hum fii **a**l-ar*dh*i aq*aa*muu **al***shsh*al*aa*ta wa*aa*tawuu **al**zzak*aa*ta wa-amaruu bi**a**lma'ruufi wanahaw '

(Yaitu) orang-orang yang jika Kami beri kedudukan di bumi, mereka melaksanakan salat, menunaikan zakat, dan menyuruh berbuat yang makruf dan mencegah dari yang mungkar; dan kepada Allah-lah kembali segala urusan.

22:42

# وَاِنْ يُّكَذِّبُوْكَ فَقَدْ كَذَّبَتْ قَبْلَهُمْ قَوْمُ نُوْحٍ وَّعَادٌ وَّثَمُوْدُ ۙ

wa-in yuka*dzdz*ibuuka faqad ka*dzdz*abat qablahum qawmu nuu*h*in wa'*aa*dun watsamuud**u**

Dan jika mereka (orang-orang musyrik) mendustakan engkau (Muhammad), begitu pulalah kaum-kaum yang sebelum mereka, kaum Nuh, ‘Ad, dan Samud (juga telah mendustakan rasul-rasul-Nya),

22:43

# وَقَوْمُ اِبْرٰهِيْمَ وَقَوْمُ لُوْطٍ ۙ

waqawmu ibr*aa*hiima waqawmu luu*th**\*in**

dan (demikian juga) kaum Ibrahim dan kaum Lut,

22:44

# وَّاَصْحٰبُ مَدْيَنَۚ وَكُذِّبَ مُوْسٰى فَاَمْلَيْتُ لِلْكٰفِرِيْنَ ثُمَّ اَخَذْتُهُمْۚ فَكَيْفَ كَانَ نَكِيْرِ

wa-a*sh*-*haa*bu madyana waku*dzdz*iba muus*aa* fa-amlaytu lilk*aa*firiina tsumma akha*dz*tuhum fakayfa k*aa*na nakiir**i**

dan penduduk Madyan. Dan Musa (juga) telah didustakan, namun Aku beri tenggang waktu kepada orang-orang kafir, kemudian Aku siksa mereka, maka betapa hebatnya siksaan-Ku.

22:45

# فَكَاَيِّنْ مِّنْ قَرْيَةٍ اَهْلَكْنٰهَا وَهِيَ ظَالِمَةٌ فَهِيَ خَاوِيَةٌ عَلٰى عُرُوْشِهَاۖ وَبِئْرٍ مُّعَطَّلَةٍ وَّقَصْرٍ مَّشِيْدٍ

faka-ayyin min qaryatin ahlakn*aa*h*aa* wahiya *zhaa*limatun fahiya kh*aa*wiyatun 'al*aa* 'uruusyih*aa* wabi/rin mu'a*ththh*alatin waqa*sh*rin masyiid**in**

Maka betapa banyak negeri yang telah Kami binasakan karena (penduduk)nya dalam keadaan zalim, sehingga runtuh bangunan-bangunannya dan (betapa banyak pula) sumur yang telah ditinggalkan dan istana yang tinggi (ti-dak ada penghuninya).

22:46

# اَفَلَمْ يَسِيْرُوْا فِى الْاَرْضِ فَتَكُوْنَ لَهُمْ قُلُوْبٌ يَّعْقِلُوْنَ بِهَآ اَوْ اٰذَانٌ يَّسْمَعُوْنَ بِهَاۚ فَاِنَّهَا لَا تَعْمَى الْاَبْصَارُ وَلٰكِنْ تَعْمَى الْقُلُوْبُ الَّتِيْ فِى الصُّدُوْرِ

afalam yasiiruu fii **a**l-ar*dh*i fatakuuna lahum quluubun ya'qiluuna bih*aa* aw *aatsaa*nun yasma'uuna bih*aa* fa-innah*aa* l*aa* ta'm*aa* **a**l-ab*shaa*ru wal*aa*kin ta'm*aa*

Maka tidak pernahkah mereka berjalan di bumi, sehingga hati (akal) mereka dapat memahami, telinga mereka dapat mendengar? Sebenarnya bukan mata itu yang buta, tetapi yang buta ialah hati yang di dalam dada.

22:47

# وَيَسْتَعْجِلُوْنَكَ بِالْعَذَابِ وَلَنْ يُّخْلِفَ اللّٰهُ وَعْدَهٗۗ وَاِنَّ يَوْمًا عِنْدَ رَبِّكَ كَاَلْفِ سَنَةٍ مِّمَّا تَعُدُّوْنَ

wayasta'jiluunaka bi**a**l'a*dzaa*bi walan yukhlifa **al**l*aa*hu wa'dahu wa-inna yawman 'inda rabbika ka-alfi sanatin mimm*aa* ta'udduun**a**

Dan mereka meminta kepadamu (Muhammad) agar azab itu disegerakan, padahal Allah tidak akan menyalahi janji-Nya. Dan sesungguhnya sehari di sisi Tuhanmu adalah seperti seribu tahun menurut perhitunganmu.

22:48

# وَكَاَيِّنْ مِّنْ قَرْيَةٍ اَمْلَيْتُ لَهَا وَهِيَ ظَالِمَةٌ ثُمَّ اَخَذْتُهَاۚ وَاِلَيَّ الْمَصِيْرُ ࣖ

waka-ayyin min qaryatin amlaytu lah*aa* wahiya *zhaa*limatun tsumma akha*dz*tuh*aa* wa-ilayya **a**lma*sh*iir**u**

Dan berapa banyak negeri yang Aku tangguhkan (penghancuran)nya, karena penduduknya berbuat zalim, kemudian Aku azab mereka, dan hanya kepada-Kulah tempat kembali (segala sesuatu).

22:49

# قُلْ يٰٓاَيُّهَا النَّاسُ اِنَّمَآ اَنَا۠ لَكُمْ نَذِيْرٌ مُّبِيْنٌ ۚ

qul y*aa* ayyuh*aa* **al**nn*aa*su innam*aa* an*aa* lakum na*dz*iirun mubiin**un**

Katakanlah (Muhammad), “Wahai manusia! Sesungguhnya aku (diutus) kepadamu sebagai pemberi peringatan yang nyata.”

22:50

# فَالَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِ لَهُمْ مَّغْفِرَةٌ وَّرِزْقٌ كَرِيْمٌ

fa**a**lla*dz*iina *aa*manuu wa'amiluu **al***shshaa*li*haa*ti lahum maghfiratun warizqun kariim**un**

Maka orang-orang yang beriman dan mengerjakan kebajikan, mereka memperoleh ampunan dan rezeki yang mulia.

22:51

# وَالَّذِيْنَ سَعَوْا فِيْٓ اٰيٰتِنَا مُعٰجِزِيْنَ اُولٰۤىِٕكَ اَصْحٰبُ الْجَحِيْمِ

wa**a**lla*dz*iina sa'aw fii *aa*y*aa*tin*aa* mu'*aa*jiziina ul*aa*-ika a*sh*-*haa*bu **a**lja*h*iim**i**

Tetapi orang-orang yang berusaha menentang ayat-ayat Kami dengan maksud melemahkan (kemauan untuk beriman), mereka itu adalah penghuni-penghuni neraka Jahim.

22:52

# وَمَآ اَرْسَلْنَا مِنْ قَبْلِكَ مِنْ رَّسُوْلٍ وَّلَا نَبِيٍّ اِلَّآ اِذَا تَمَنّٰىٓ اَلْقَى الشَّيْطٰنُ فِيْٓ اُمْنِيَّتِهٖۚ فَيَنْسَخُ اللّٰهُ مَا يُلْقِى الشَّيْطٰنُ ثُمَّ يُحْكِمُ اللّٰهُ اٰيٰتِهٖۗ وَاللّٰهُ عَلِيْمٌ حَكِيْمٌ ۙ

wam*aa* arsaln*aa* min qablika min rasuulin wal*aa* nabiyyin ill*aa* i*dzaa* tamann*aa* **a**lq*aa* **al**sysyay*thaa*nu fii umniyyatihi fayansakhu **al**l*aa*hu m<

Dan Kami tidak mengutus seorang rasul dan tidak (pula) seorang nabi sebelum engkau (Muhammad), mela-inkan apabila dia mempunyai suatu keinginan, setan pun memasukkan godaan-godaan ke dalam keinginannya itu. Tetapi Allah menghilangkan apa yang dimasukkan s

22:53

# لِّيَجْعَلَ مَا يُلْقِى الشَّيْطٰنُ فِتْنَةً لِّلَّذِيْنَ فِيْ قُلُوْبِهِمْ مَّرَضٌ وَّالْقَاسِيَةِ قُلُوْبُهُمْۗ وَاِنَّ الظّٰلِمِيْنَ لَفِيْ شِقَاقٍۢ بَعِيْدٍ ۙ

liyaj'ala m*aa* yulqii **al**sysyay*thaa*nu fitnatan lilla*dz*iina fii quluubihim mara*dh*un wa**a**lq*aa*siyati quluubuhum wa-inna **al***zhzhaa*limiina lafii syiq*aa*qin ba

Dia (Allah) ingin menjadikan godaan yang ditimbulkan setan itu, sebagai cobaan bagi orang-orang yang dalam hatinya ada penyakit dan orang yang berhati keras. Dan orang-orang yang zalim itu benar-benar dalam permu-suhan yang jauh,

22:54

# وَّلِيَعْلَمَ الَّذِيْنَ اُوْتُوا الْعِلْمَ اَنَّهُ الْحَقُّ مِنْ رَّبِّكَ فَيُؤْمِنُوْا بِهٖ فَتُخْبِتَ لَهٗ قُلُوْبُهُمْۗ وَاِنَّ اللّٰهَ لَهَادِ الَّذِيْنَ اٰمَنُوْٓا اِلٰى صِرَاطٍ مُّسْتَقِيْمٍ

waliya'lama **al**la*dz*iina uutuu **a**l'ilma annahu **a**l*h*aqqu min rabbika fayu/minuu bihi fatukhbita lahu quluubuhum wa-inna **al**l*aa*ha lah*aa*di **al**la*dz*

*dan agar orang-orang yang telah diberi ilmu, meyakini bahwa (Al-Qur'an) itu benar dari Tuhanmu lalu mereka beriman dan hati mereka tunduk kepadanya. Dan sungguh, Allah pemberi petunjuk bagi orang-orang yang beriman kepada jalan yang lurus.*

22:55

# وَلَا يَزَالُ الَّذِيْنَ كَفَرُوْا فِيْ مِرْيَةٍ مِّنْهُ حَتّٰى تَأْتِيَهُمُ السَّاعَةُ بَغْتَةً اَوْ يَأْتِيَهُمْ عَذَابُ يَوْمٍ عَقِيْمٍ

wal*aa* yaz*aa*lu **al**la*dz*iina kafaruu fii miryatin minhu *h*att*aa* ta/tiyahumu **al**ss*aa*'atu baghtatan aw ya/tiyahum 'a*dzaa*bu yawmin 'aqiim**in**

Dan orang-orang kafir itu senantiasa ragu mengenai hal itu (Al-Qur'an), hingga saat (kematiannya) datang kepada mereka dengan tiba-tiba, atau azab hari Kiamat yang datang kepada mereka.

22:56

# اَلْمُلْكُ يَوْمَىِٕذٍ لِّلّٰهِ ۗيَحْكُمُ بَيْنَهُمْۗ فَالَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِ فِيْ جَنّٰتِ النَّعِيْمِ

almulku yawma-i*dz*in lill*aa*hi ya*h*kumu baynahum fa**a**lla*dz*iina *aa*manuu wa'amiluu **al***shshaa*li*haa*ti fii jann*aa*ti **al**nna'iim**i**

Kekuasaan pada hari itu ada pada Allah, Dia memberi keputusan di an-tara mereka. Maka orang-orang yang beriman dan mengerjakan kebajikan berada dalam surga-surga yang penuh kenikmatan.

22:57

# وَالَّذِيْنَ كَفَرُوْا وَكَذَّبُوْا بِاٰيٰتِنَا فَاُولٰۤىِٕكَ لَهُمْ عَذَابٌ مُّهِيْنٌ ࣖ

wa**a**lla*dz*iina kafaruu waka*dzdz*abuu bi-*aa*y*aa*tin*aa* faul*aa*-ika lahum 'a*dzaa*bun muhiin**un**

Dan orang-orang kafir dan yang men-dustakan ayat-ayat Kami, maka mere-ka akan merasakan azab yang meng-hinakan.

22:58

# وَالَّذِيْنَ هَاجَرُوْا فِيْ سَبِيْلِ اللّٰهِ ثُمَّ قُتِلُوْٓا اَوْ مَاتُوْا لَيَرْزُقَنَّهُمُ اللّٰهُ رِزْقًا حَسَنًاۗ وَاِنَّ اللّٰهَ لَهُوَ خَيْرُ الرّٰزِقِيْنَ

wa**a**lla*dz*iina h*aa*jaruu fii sabiili **al**l*aa*hi tsumma qutiluu aw m*aa*tuu layarzuqannahumu **al**l*aa*hu rizqan *h*asanan wa-inna **al**l*aa*ha lahuwa khayru <

Dan orang-orang yang berhijrah di jalan Allah, kemudian mereka terbunuh atau mati, sungguh, Allah akan memberikan kepada mereka rezeki yang baik (surga). Dan sesungguhnya Allah adalah pemberi rezeki yang terbaik.

22:59

# لَيُدْخِلَنَّهُمْ مُّدْخَلًا يَّرْضَوْنَهٗۗ وَاِنَّ اللّٰهَ لَعَلِيْمٌ حَلِيْمٌ

layudkhilannahum mudkhalan yar*dh*awnahu wa-inna **al**l*aa*ha la'aliimun *h*aliim**un**

Sungguh, Dia (Allah) pasti akan memasukkan mereka ke tempat masuk (surga) yang mereka sukai. Dan sesungguhnya Allah Maha Mengetahui, Maha Penyantun.

22:60

# ۞ ذٰلِكَ وَمَنْ عَاقَبَ بِمِثْلِ مَا عُوْقِبَ بِهٖ ثُمَّ بُغِيَ عَلَيْهِ لَيَنْصُرَنَّهُ اللّٰهُ ۗاِنَّ اللّٰهَ لَعَفُوٌّ غَفُوْرٌ

*dzaa*lika waman '*aa*qaba bimitsli m*aa* 'uuqiba bihi tsumma bughiya 'alayhi layan*sh*urannahu **al**l*aa*hu inna **al**l*aa*ha la'afuwwun ghafuur**un**

Demikianlah, dan barangsiapa membalas seimbang dengan (kezaliman) penganiayaan yang pernah dia derita kemudian dia dizalimi (lagi), pasti Allah akan menolongnya. Sungguh, Allah Maha Pemaaf, Maha Pengampun.

22:61

# ذٰلِكَ بِاَنَّ اللّٰهَ يُوْلِجُ الَّيْلَ فِى النَّهَارِ وَيُوْلِجُ النَّهَارَ فِى الَّيْلِ وَاَنَّ اللّٰهَ سَمِيْعٌۢ بَصِيْرٌ

*dzaa*lika bi-anna **al**l*aa*ha yuuliju **al**layla fii **al**nnah*aa*ri wayuuliju **al**nnah*aa*ra fii **al**layli wa-anna **al**l*aa*ha samii'un ba

Demikianlah karena Allah (kuasa) memasukkan malam ke dalam siang dan memasukkan siang ke dalam malam dan sungguh, Allah Maha Mendengar, Maha Melihat.

22:62

# ذٰلِكَ بِاَنَّ اللّٰهَ هُوَ الْحَقُّ وَاَنَّ مَا يَدْعُوْنَ مِنْ دُوْنِهٖ هُوَ الْبَاطِلُ وَاَنَّ اللّٰهَ هُوَ الْعَلِيُّ الْكَبِيْرُ

*dzaa*lika bi-anna **al**l*aa*ha huwa **a**l*h*aqqu wa-anna m*aa* yad'uuna min duunihi huwa **a**lb*aath*ilu wa-anna **al**l*aa*ha huwa **a**l'aliyyu **a<**

Demikianlah (kebesaran Allah) karena Allah, Dialah (Tuhan) Yang Hak. Dan apa saja yang mereka seru selain Dia, itulah yang batil, dan sungguh Allah, Dialah Yang Mahatinggi, Mahabesar.

22:63

# اَلَمْ تَرَ اَنَّ اللّٰهَ اَنْزَلَ مِنَ السَّمَاۤءِ مَاۤءًۖ فَتُصْبِحُ الْاَرْضُ مُخْضَرَّةًۗ اِنَّ اللّٰهَ لَطِيْفٌ خَبِيْرٌ ۚ

alam tara anna **al**l*aa*ha anzala mina **al**ssam*aa*-i m*aa*-an fatu*sh*bi*h*u **a**l-ar*dh*u mukh*dh*arratan inna **al**l*aa*ha la*th*iifun khabiir

Tidakkah engkau memperhatikan, bahwa Allah menurunkan air (hujan) dari langit, sehingga bumi menjadi hijau? Sungguh, Allah Mahahalus, Maha Mengetahui.

22:64

# لَهٗ مَا فِى السَّمٰوٰتِ وَمَا فِى الْاَرْضِۗ وَاِنَّ اللّٰهَ لَهُوَ الْغَنِيُّ الْحَمِيْدُ ࣖ

lahu m*aa* fii **al**ssam*aa*w*aa*ti wam*aa* fii **a**l-ar*dh*i wa-inna **al**l*aa*ha lahuwa **a**lghaniyyu **a**l*h*amiid**u**

Milik-Nyalah apa yang ada di langit dan apa yang ada di bumi. Dan Allah benar-benar Mahakaya, Maha Terpuji.

22:65

# اَلَمْ تَرَ اَنَّ اللّٰهَ سَخَّرَ لَكُمْ مَّا فِى الْاَرْضِ وَالْفُلْكَ تَجْرِيْ فِى الْبَحْرِ بِاَمْرِهٖۗ وَيُمْسِكُ السَّمَاۤءَ اَنْ تَقَعَ عَلَى الْاَرْضِ اِلَّا بِاِذْنِهٖۗ اِنَّ اللّٰهَ بِالنَّاسِ لَرَءُوْفٌ رَّحِيْمٌ

alam tara anna **al**l*aa*ha sakhkhara lakum m*aa* fii **a**l-ar*dh*i wa**a**lfulka tajrii fii **a**lba*h*ri bi-amrihi wayumsiku **al**ssam*aa*-a an taqa'a 'al*aa<*

Tidakkah engkau memperhatikan bahwa Allah menundukkan bagimu (manusia) apa yang ada di bumi dan kapal yang berlayar di lautan dengan perintah-Nya. Dan Dia menahan (benda-benda) langit agar tidak jatuh ke bumi, melainkan dengan izin-Nya? Sungguh, Allah Mah

22:66

# وَهُوَ الَّذِيْٓ اَحْيَاكُمْ ۖ ثُمَّ يُمِيْتُكُمْ ثُمَّ يُحْيِيْكُمْۗ اِنَّ الْاِنْسَانَ لَكَفُوْرٌ

wahuwa **al**la*dz*ii a*h*y*aa*kum tsumma yumiitukum tsumma yu*h*yiikum inna **a**l-ins*aa*na lakafuur**un**

Dan Dialah yang menghidupkan kamu, kemudian mematikan kamu, kemudian menghidupkan kamu kembali (pada hari kebangkitan). Sungguh, manusia itu sangat kufur nikmat.

22:67

# لِكُلِّ اُمَّةٍ جَعَلْنَا مَنْسَكًا هُمْ نَاسِكُوْهُ فَلَا يُنَازِعُنَّكَ فِى الْاَمْرِ وَادْعُ اِلٰى رَبِّكَۗ اِنَّكَ لَعَلٰى هُدًى مُّسْتَقِيْمٍ

likulli ummatin ja'aln*aa* mansakan hum n*aa*sikuuhu fal*aa* yun*aa*zi'unnaka fii **a**l-amri wa**u**d'u il*aa* rabbika innaka la'al*aa* hudan mustaqiim**in**

Bagi setiap umat telah Kami tetapkan syariat tertentu yang (harus) mereka amalkan, maka tidak sepantasnya mereka berbantahan dengan engkau dalam urusan (syariat) ini dan serulah (mereka) kepada Tuhanmu. Sungguh, engkau (Muhammad) berada di jalan yang luru

22:68

# وَاِنْ جَادَلُوْكَ فَقُلِ اللّٰهُ اَعْلَمُ بِمَا تَعْمَلُوْنَ

wa-in j*aa*daluuka faquli **al**l*aa*hu a'lamu bim*aa* ta'maluun**a**

Dan jika mereka membantah engkau, maka katakanlah, “Allah lebih tahu tentang apa yang kamu kerjakan.”

22:69

# اَللّٰهُ يَحْكُمُ بَيْنَكُمْ يَوْمَ الْقِيٰمَةِ فِيْمَا كُنْتُمْ فِيْهِ تَخْتَلِفُوْنَ

**al**l*aa*hu ya*h*kumu baynakum yawma **a**lqiy*aa*mati fiim*aa* kuntum fiihi takhtalifuun**a**

Allah akan mengadili di antara kamu pada hari Kiamat tentang apa yang dahulu kamu memperselisihkannya.

22:70

# اَلَمْ تَعْلَمْ اَنَّ اللّٰهَ يَعْلَمُ مَا فِى السَّمَاۤءِ وَالْاَرْضِۗ اِنَّ ذٰلِكَ فِيْ كِتٰبٍۗ اِنَّ ذٰلِكَ عَلَى اللّٰهِ يَسِيْرٌ

alam ta'lam anna **al**l*aa*ha ya'lamu m*aa* fii **al**ssam*aa*-i wa**a**l-ar*dh*i inna *dzaa*lika fii kit*aa*bin inna *dzaa*lika 'al*aa* **al**l*aa*hi yasiir

Tidakkah engkau tahu bahwa Allah mengetahui apa yang di langit dan di bumi? Sungguh, yang demikian itu sudah terdapat dalam sebuah Kitab (Lauh Mahfuzh). Sesungguhnya yang demikian itu sangat mudah bagi Allah.

22:71

# وَيَعْبُدُوْنَ مِنْ دُوْنِ اللّٰهِ مَا لَمْ يُنَزِّلْ بِهٖ سُلْطٰنًا وَّمَا لَيْسَ لَهُمْ بِهٖ عِلْمٌ ۗوَمَا لِلظّٰلِمِيْنَ مِنْ نَّصِيْرٍ

waya'buduuna min duuni **al**l*aa*hi m*aa* lam yunazzil bihi sul*thaa*nan wam*aa* laysa lahum bihi 'ilmun wam*aa* li**l***zhzhaa*limiina min na*sh*iir**in**

Dan mereka menyembah selain Allah, tanpa dasar yang jelas tentang itu, dan mereka tidak mempunyai pengetahuan (pula) tentang itu. Bagi orang-orang yang zalim tidak ada seorang penolong pun.

22:72

# وَاِذَا تُتْلٰى عَلَيْهِمْ اٰيٰتُنَا بَيِّنٰتٍ تَعْرِفُ فِيْ وُجُوْهِ الَّذِيْنَ كَفَرُوا الْمُنْكَرَۗ يَكَادُوْنَ يَسْطُوْنَ بِالَّذِيْنَ يَتْلُوْنَ عَلَيْهِمْ اٰيٰتِنَاۗ قُلْ اَفَاُنَبِّئُكُمْ بِشَرٍّ مِّنْ ذٰلِكُمْۗ اَلنَّارُۗ وَعَدَهَا اللّٰهُ الَّذِ

wa-i*dzaa* tutl*aa* 'alayhim *aa*y*aa*tun*aa* bayyin*aa*tin ta'rifu fii wujuuhi **al**la*dz*iina kafaruu **a**lmunkara yak*aa*duuna yas*th*uuna bi**a**lla*dz*iina yat

Dan apabila dibacakan di hadapan mereka ayat-ayat Kami yang terang, niscaya engkau akan melihat (tanda-tanda) keingkaran pada wajah orang-orang yang kafir itu. Hampir-hampir mereka menyerang orang-orang yang membacakan ayat-ayat Kami kepada mereka. Kataka

22:73

# يٰٓاَيُّهَا النَّاسُ ضُرِبَ مَثَلٌ فَاسْتَمِعُوْا لَهٗ ۗاِنَّ الَّذِيْنَ تَدْعُوْنَ مِنْ دُوْنِ اللّٰهِ لَنْ يَّخْلُقُوْا ذُبَابًا وَّلَوِ اجْتَمَعُوْا لَهٗ ۗوَاِنْ يَّسْلُبْهُمُ الذُّبَابُ شَيْـًٔا لَّا يَسْتَنْقِذُوْهُ مِنْهُۗ ضَعُفَ الطَّالِبُ وَالْمَط

y*aa* ayyuh*aa* **al**nn*aa*su *dh*uriba matsalun fa**i**stami'uu lahu inna **al**la*dz*iina tad'uuna min duuni **al**l*aa*hi lan yakhluquu *dz*ub*aa*ban walawi ij

Wahai manusia! Telah dibuat suatu perumpamaan. Maka dengarkanlah! Sesungguhnya segala yang kamu seru selain Allah tidak dapat menciptakan seekor lalat pun, walaupun mereka bersatu untuk menciptakannya. Dan jika lalat itu merampas sesuatu dari mereka, mere

22:74

# مَا قَدَرُوا اللّٰهَ حَقَّ قَدْرِهٖۗ اِنَّ اللّٰهَ لَقَوِيٌّ عَزِيْزٌ

m*aa* qadaruu **al**l*aa*ha *h*aqqa qadrihi inna **al**l*aa*ha laqawiyyun 'aziiz**un**

Mereka tidak mengagungkan Allah dengan sebenar-benarnya. Sungguh, Allah Mahakuat, Mahaperkasa.

22:75

# اَللّٰهُ يَصْطَفِيْ مِنَ الْمَلٰۤىِٕكَةِ رُسُلًا وَّمِنَ النَّاسِۗ اِنَّ اللّٰهَ سَمِيْعٌۢ بَصِيْرٌ ۚ

**al**l*aa*hu ya*sth*afii mina **a**lmal*aa*-ikati rusulan wamina **al**nn*aa*si inna **al**l*aa*ha samii'un ba*sh*iir**un**

Allah memilih para utusan(-Nya) dari malaikat dan dari manusia. Sesungguhnya Allah Maha Mendengar, Maha Melihat.

22:76

# يَعْلَمُ مَا بَيْنَ اَيْدِيْهِمْ وَمَا خَلْفَهُمْۗ وَاِلَى اللّٰهِ تُرْجَعُ الْاُمُوْرُ

ya'lamu m*aa* bayna aydiihim wam*aa* khalfahum wa-il*aa* **al**l*aa*hi turja'u **a**l-umuur**u**

Dia (Allah) mengetahui apa yang di hadapan mereka dan apa yang di belakang mereka. Dan hanya kepada Allah dikembalikan segala urusan.

22:77

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوا ارْكَعُوْا وَاسْجُدُوْا وَاعْبُدُوْا رَبَّكُمْ وَافْعَلُوا الْخَيْرَ لَعَلَّكُمْ تُفْلِحُوْنَ ۚ۩

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu irka'uu wa**u**sjuduu wa**u**'buduu rabbakum wa**i**f'aluu **a**lkhayra la'allakum tufli*h*uun**a**

Wahai orang-orang yang beriman! Rukuklah, sujudlah, dan sembahlah Tuhanmu; dan berbuatlah kebaikan, agar kamu beruntung.

22:78

# وَجَاهِدُوْا فِى اللّٰهِ حَقَّ جِهَادِهٖۗ هُوَ اجْتَبٰىكُمْ وَمَا جَعَلَ عَلَيْكُمْ فِى الدِّيْنِ مِنْ حَرَجٍۗ مِلَّةَ اَبِيْكُمْ اِبْرٰهِيْمَۗ هُوَ سَمّٰىكُمُ الْمُسْلِمِيْنَ ەۙ مِنْ قَبْلُ وَفِيْ هٰذَا لِيَكُوْنَ الرَّسُوْلُ شَهِيْدًا عَلَيْكُمْ وَتَكُو

waj*aa*hiduu fii **al**l*aa*hi *h*aqqa jih*aa*dihi huwa ijtab*aa*kum wam*aa* ja'ala 'alaykum fii **al**ddiini min *h*arajin millata abiikum ibr*aa*hiima huwa samm*aa*kumu **a**

Dan berjihadlah kamu pada jalan Allah dengan jihad yang sebenar-benarnya. Dia telah memilih kamu dan Dia sekali-kali tidak menjadikan untuk kamu dalam agama suatu kesempitan. (Ikutilah) agama orang tuamu Ibrahim. Dia (Allah) telah menamai kamu sekalian orang-orang muslim dari dahulu, dan (begitu pula) dalam (Al Quran) ini, supaya Rasul itu menjadi saksi atas dirimu dan supaya kamu semua menjadi saksi atas segenap manusia, maka dirikanlah sembahyang, tunaikanlah zakat dan berpeganglah kamu pada tali Allah. Dia adalah Pelindungmu, maka Dialah sebaik-baik Pelindung dan sebaik-baik Penolong.

<!--EndFragment-->