---
title: (67) Al-Mulk - الملك
date: 2021-10-27T04:05:05.318Z
ayat: 67
description: "Jumlah Ayat: 30 / Arti: Kerajaan"
---
<!--StartFragment-->

67:1

# تَبٰرَكَ الَّذِيْ بِيَدِهِ الْمُلْكُۖ وَهُوَ عَلٰى كُلِّ شَيْءٍ قَدِيْرٌۙ

tab*aa*raka **al**la*dz*ii biyadihi **a**lmulku wahuwa 'al*aa* kulli syay-in qadiir**un**

Mahasuci Allah yang menguasai (segala) kerajaan, dan Dia Mahakuasa atas segala sesuatu.

67:2

# ۨالَّذِيْ خَلَقَ الْمَوْتَ وَالْحَيٰوةَ لِيَبْلُوَكُمْ اَيُّكُمْ اَحْسَنُ عَمَلًاۗ وَهُوَ الْعَزِيْزُ الْغَفُوْرُۙ

**al**la*dz*ii khalaqa **a**lmawta wa**a**l*h*ay*aa*ta liyabluwakum ayyukum a*h*sanu 'amalan wahuwa **a**l'aziizu **a**lghafuur**u**

Yang menciptakan mati dan hidup, untuk menguji kamu, siapa di antara kamu yang lebih baik amalnya. Dan Dia Mahaperkasa, Maha Pengampun.

67:3

# الَّذِيْ خَلَقَ سَبْعَ سَمٰوٰتٍ طِبَاقًاۗ مَا تَرٰى فِيْ خَلْقِ الرَّحْمٰنِ مِنْ تَفٰوُتٍۗ فَارْجِعِ الْبَصَرَۙ هَلْ تَرٰى مِنْ فُطُوْرٍ

**al**la*dz*ii khalaqa sab'a sam*aa*w*aa*tin *th*ib*aa*qan m*aa* tar*aa* fii khalqi **al**rra*h*m*aa*ni min taf*aa*wutin fa**i**rji'i **a**lba*sh*ar

Yang menciptakan tujuh langit berlapis-lapis. Tidak akan kamu lihat sesuatu yang tidak seimbang pada ciptaan Tuhan Yang Maha Pengasih. Maka lihatlah sekali lagi, adakah kamu lihat sesuatu yang cacat?

67:4

# ثُمَّ ارْجِعِ الْبَصَرَ كَرَّتَيْنِ يَنْقَلِبْ اِلَيْكَ الْبَصَرُ خَاسِئًا وَّهُوَ حَسِيْرٌ

tsumma irji'i **a**lba*sh*ara karratayni yanqalib ilayka **a**lba*sh*aru kh*aa*si-an wahuwa *h*asiir**un**

Kemudian ulangi pandangan(mu) sekali lagi (dan) sekali lagi, niscaya pandanganmu akan kembali kepadamu tanpa menemukan cacat dan ia (pandanganmu) dalam keadaan letih.

67:5

# وَلَقَدْ زَيَّنَّا السَّمَاۤءَ الدُّنْيَا بِمَصَابِيْحَ وَجَعَلْنٰهَا رُجُوْمًا لِّلشَّيٰطِيْنِ وَاَعْتَدْنَا لَهُمْ عَذَابَ السَّعِيْرِ

walaqad zayyann*aa* **al**ssam*aa*-a **al**dduny*aa* bima*shaa*bii*h*a waja'aln*aa*h*aa* rujuuman li**l**sysyay*aath*iini wa-a'tadn*aa* lahum 'a*dzaa*ba **al**

**Dan sungguh, telah Kami hiasi langit yang dekat, dengan bintang-bintang dan Kami jadikannya (bintang-bintang itu) sebagai alat-alat pelempar setan, dan Kami sediakan bagi mereka azab neraka yang menyala-nyala.**









67:6

# وَلِلَّذِيْنَ كَفَرُوْا بِرَبِّهِمْ عَذَابُ جَهَنَّمَۗ وَبِئْسَ الْمَصِيْرُ

walilla*dz*iina kafaruu birabbihim 'a*dzaa*bu jahannama wabi/sa **a**lma*sh*iir**u**

Dan orang-orang yang ingkar kepada Tuhannya akan mendapat azab Jahanam. Dan itulah seburuk-buruk tempat kembali.

67:7

# اِذَآ اُلْقُوْا فِيْهَا سَمِعُوْا لَهَا شَهِيْقًا وَّهِيَ تَفُوْرُۙ

i*dzaa* ulquu fiih*aa* sami'uu lah*aa* syahiiqan wahiya tafuur**u**

Apabila mereka dilemparkan ke dalamnya mereka mendengar suara neraka yang mengerikan, sedang neraka itu membara,

67:8

# تَكَادُ تَمَيَّزُ مِنَ الْغَيْظِۗ كُلَّمَآ اُلْقِيَ فِيْهَا فَوْجٌ سَاَلَهُمْ خَزَنَتُهَآ اَلَمْ يَأْتِكُمْ نَذِيْرٌۙ

tak*aa*du tamayyazu mina alghay*zh*i kullam*aa* ulqiya fiih*aa* fawjun sa-alahum khazanatuh*aa* alam ya/tikum na*dz*iir**un**

hampir meledak karena marah. Setiap kali ada sekumpulan (orang-orang kafir) dilemparkan ke dalamnya, penjaga-penjaga (neraka itu) bertanya kepada mereka, “Apakah belum pernah ada orang yang datang memberi peringatan kepadamu (di dunia)?”

67:9

# قَالُوْا بَلٰى قَدْ جَاۤءَنَا نَذِيْرٌ ەۙ فَكَذَّبْنَا وَقُلْنَا مَا نَزَّلَ اللّٰهُ مِنْ شَيْءٍۖ اِنْ اَنْتُمْ اِلَّا فِيْ ضَلٰلٍ كَبِيْرٍ

q*aa*luu bal*aa* qad j*aa*-an*aa* na*dz*iirun faka*dzdz*abn*aa* waquln*aa* m*aa* nazzala **al**l*aa*hu min syay-in in antum ill*aa* fii *dh*al*aa*lin kabiir**in**

Mereka menjawab, “Benar, sungguh, seorang pemberi peringatan telah datang kepada kami, tetapi kami mendustakan(nya) dan kami katakan, “Allah tidak menurunkan sesuatu apa pun, kamu sebenarnya di dalam kesesatan yang besar.”

67:10

# وَقَالُوْا لَوْ كُنَّا نَسْمَعُ اَوْ نَعْقِلُ مَا كُنَّا فِيْٓ اَصْحٰبِ السَّعِيْرِ

waq*aa*luu law kunn*aa* nasma'u aw na'qilu m*aa* kunn*aa* fii a*sh*-*haa*bi **al**ssa'iir**i**

Dan mereka berkata, “Sekiranya (dahulu) kami mendengarkan atau memikirkan (peringatan itu) tentulah kami tidak termasuk penghuni neraka yang menyala-nyala.”

67:11

# فَاعْتَرَفُوْا بِذَنْۢبِهِمْۚ فَسُحْقًا لِّاَصْحٰبِ السَّعِيْرِ

fa**i**'tarafuu bi*dz*anbihim fasu*h*qan li-a*sh*-*haa*bi **al**ssa'iir**i**

Maka mereka mengakui dosanya. Tetapi jauhlah (dari rahmat Allah) bagi penghuni neraka yang menyala-nyala itu.

67:12

# اِنَّ الَّذِيْنَ يَخْشَوْنَ رَبَّهُمْ بِالْغَيْبِ لَهُمْ مَّغْفِرَةٌ وَّاَجْرٌ كَبِيْرٌ

inna **al**la*dz*iina yakhsyawna rabbahum bi**a**lghaybi lahum maghfiratun wa-ajrun kabiir**un**

Sesungguhnya orang-orang yang takut kepada Tuhannya yang tidak terlihat oleh mereka, mereka memperoleh ampunan dan pahala yang besar.

67:13

# وَاَسِرُّوْا قَوْلَكُمْ اَوِ اجْهَرُوْا بِهٖۗ اِنَّهٗ عَلِيْمٌ ۢبِذَاتِ الصُّدُوْرِ

wa-asirruu qawlakum awi ijharuu bihi innahu 'aliimun bi*dzaa*ti **al***shsh*uduur**i**

Dan rahasiakanlah perkataanmu atau nyatakanlah. Sungguh, Dia Maha Mengetahui segala isi hati.

67:14

# اَلَا يَعْلَمُ مَنْ خَلَقَۗ وَهُوَ اللَّطِيْفُ الْخَبِيْرُ ࣖ

al*aa* ya'lamu man khalaqa wahuwa **al**la*th*iifu **a**lkhabiir**u**

Apakah (pantas) Allah yang menciptakan itu tidak mengetahui? Dan Dia Mahahalus, Maha Mengetahui.

67:15

# هُوَ الَّذِيْ جَعَلَ لَكُمُ الْاَرْضَ ذَلُوْلًا فَامْشُوْا فِيْ مَنَاكِبِهَا وَكُلُوْا مِنْ رِّزْقِهٖۗ وَاِلَيْهِ النُّشُوْرُ

huwa **al**la*dz*ii ja'ala lakumu **a**l-ar*dh*a *dz*aluulan fa**i**msyuu fii man*aa*kibih*aa* wakuluu min rizqihi wa-ilayhi **al**nnusyuur**u**

Dialah yang menjadikan bumi untuk kamu yang mudah dijelajahi, maka jelajahilah di segala penjurunya dan makanlah sebagian dari rezeki-Nya. Dan hanya kepada-Nyalah kamu (kembali setelah) dibangkitkan.

67:16

# ءَاَمِنْتُمْ مَّنْ فِى السَّمَاۤءِ اَنْ يَّخْسِفَ بِكُمُ الْاَرْضَ فَاِذَا هِيَ تَمُوْرُۙ

a-amintum man fii **al**ssam*aa*-i an yakhsifa bikumu **a**l-ar*dh*a fa-i*dzaa* hiya tamuur**u**

Sudah merasa amankah kamu, bahwa Dia yang di langit tidak akan membuat kamu ditelan bumi ketika tiba-tiba ia terguncang?

67:17

# اَمْ اَمِنْتُمْ مَّنْ فِى السَّمَاۤءِ اَنْ يُّرْسِلَ عَلَيْكُمْ حَاصِبًاۗ فَسَتَعْلَمُوْنَ كَيْفَ نَذِيْرِ

am amintum man fii **al**ssam*aa*-i an yursila 'alaykum *has*iban fasata'lamuuna kayfa na*dz*iir**i**

Atau sudah merasa amankah kamu, bahwa Dia yang di langit tidak akan mengirimkan badai yang berbatu kepadamu? Namun kelak kamu akan mengetahui bagaimana (akibat mendustakan) peringatan-Ku.

67:18

# وَلَقَدْ كَذَّبَ الَّذِيْنَ مِنْ قَبْلِهِمْ فَكَيْفَ كَانَ نَكِيْرِ

walaqad ka*dzdz*aba **al**la*dz*iina min qablihim fakayfa k*aa*na nakiir**i**

Dan sungguh, orang-orang yang sebelum mereka pun telah mendustakan (rasul-rasul-Nya). Maka betapa hebatnya kemurkaan-Ku!

67:19

# اَوَلَمْ يَرَوْا اِلَى الطَّيْرِ فَوْقَهُمْ صٰۤفّٰتٍ وَّيَقْبِضْنَۘ مَا يُمْسِكُهُنَّ اِلَّا الرَّحْمٰنُۗ اِنَّهٗ بِكُلِّ شَيْءٍۢ بَصِيْرٌ

walaqad ka*dzdz*aba **al**la*dz*iina min qablihim fakayfa k*aa*na nakiir**i**

Tidakkah mereka memperhatikan burung-burung yang mengembangkan dan mengatupkan sayapnya di atas mereka? Tidak ada yang menahannya (di udara) selain Yang Maha Pengasih. Sungguh, Dia Maha Melihat segala sesuatu.

67:20

# اَمَّنْ هٰذَا الَّذِيْ هُوَ جُنْدٌ لَّكُمْ يَنْصُرُكُمْ مِّنْ دُوْنِ الرَّحْمٰنِۗ اِنِ الْكٰفِرُوْنَ اِلَّا فِيْ غُرُوْرٍۚ

amman h*aadzaa* **al**la*dz*ii huwa jundun lakum yan*sh*urukum min duuni **al**rra*h*m*aa*ni ini **a**lk*aa*firuuna ill*aa* fii ghuruur**in**

Atau siapakah yang akan menjadi bala tentara bagimu yang dapat membelamu selain (Allah) Yang Maha Pengasih? Orang-orang kafir itu hanyalah dalam (keadaan) tertipu.

67:21

# اَمَّنْ هٰذَا الَّذِيْ يَرْزُقُكُمْ اِنْ اَمْسَكَ رِزْقَهٗ ۚ بَلْ لَّجُّوْا فِيْ عُتُوٍّ وَّنُفُوْرٍ

amman h*aadzaa* **al**la*dz*ii yarzuqukum in amsaka rizqahu bal lajjuu fii 'utuwwin wanufuur**in**

Atau siapakah yang dapat memberimu rezeki jika Dia menahan rezeki-Nya? Bahkan mereka terus-menerus dalam kesombongan dan menjauhkan diri (dari kebenaran).

67:22

# اَفَمَنْ يَّمْشِيْ مُكِبًّا عَلٰى وَجْهِهٖٓ اَهْدٰىٓ اَمَّنْ يَّمْشِيْ سَوِيًّا عَلٰى صِرَاطٍ مُّسْتَقِيْمٍ

afaman yamsyii mukibban 'al*aa* wajhihi ahd*aa* amman yamsyii sawiyyan 'al*aa* *sh*ir*aath*in mustaqiim**in**

Apakah orang yang merangkak dengan wajah tertelungkup yang lebih terpimpin (dalam kebenaran) ataukah orang yang berjalan tegap di atas jalan yang lurus?

67:23

# قُلْ هُوَ الَّذِيْٓ اَنْشَاَكُمْ وَجَعَلَ لَكُمُ السَّمْعَ وَالْاَبْصَارَ وَالْاَفْـِٕدَةَۗ قَلِيْلًا مَّا تَشْكُرُوْنَ

qul huwa **al**la*dz*ii ansya-akum waja'ala lakumu **al**ssam'a wa**a**l-ab*shaa*ra wa**a**l-af-idata qaliilan m*aa* tasykuruun**a**

Katakanlah, “Dialah yang menciptakan kamu dan menjadikan pendengaran, penglihatan dan hati nurani bagi kamu. (Tetapi) sedikit sekali kamu bersyukur.”

67:24

# قُلْ هُوَ الَّذِيْ ذَرَاَكُمْ فِى الْاَرْضِ وَاِلَيْهِ تُحْشَرُوْنَ

qul huwa **al**la*dz*ii *dz*ara-akum fii **a**l-ar*dh*i wa-ilayhi tu*h*syaruun**a**

Katakanlah, “Dialah yang menjadikan kamu berkembang biak di muka bumi, dan hanya kepada-Nya kamu akan dikumpulkan.”

67:25

# وَيَقُوْلُوْنَ مَتٰى هٰذَا الْوَعْدُ اِنْ كُنْتُمْ صٰدِقِيْنَ

wayaquuluuna mat*aa* h*aadzaa* **a**lwa'du in kuntum *shaa*diqiin**a**

Dan mereka berkata, “Kapan (datangnya) ancaman itu jika kamu orang yang benar?”

67:26

# قُلْ اِنَّمَا الْعِلْمُ عِنْدَ اللّٰهِ ۖوَاِنَّمَآ اَنَا۠ نَذِيْرٌ مُّبِيْنٌ

qul innam*aa* **a**l'ilmu 'inda **al**l*aa*hi wa-innam*aa* an*aa* na*dz*iirun mubiin**un**

Katakanlah (Muhammad), “Sesungguhnya ilmu (tentang hari Kiamat itu) hanya ada pada Allah. Dan aku hanyalah seorang pemberi peringatan yang menjelaskan.”

67:27

# فَلَمَّا رَاَوْهُ زُلْفَةً سِيْۤـَٔتْ وُجُوْهُ الَّذِيْنَ كَفَرُوْا وَقِيْلَ هٰذَا الَّذِيْ كُنْتُمْ بِهٖ تَدَّعُوْنَ

falamm*aa* ra-awhu zulfatan sii-at wujuuhu **al**la*dz*iina kafaruu waqiila h*aadzaa* **al**la*dz*ii kuntum bihi tadda'uun**a**

Maka ketika mereka melihat azab (pada hari Kiamat) sudah dekat, wajah orang-orang kafir itu menjadi muram. Dan dikatakan (kepada mereka), “Inilah (azab) yang dahulunya kamu minta.”

67:28

# قُلْ اَرَءَيْتُمْ اِنْ اَهْلَكَنِيَ اللّٰهُ وَمَنْ مَّعِيَ اَوْ رَحِمَنَاۙ فَمَنْ يُّجِيْرُ الْكٰفِرِيْنَ مِنْ عَذَابٍ اَلِيْمٍ

qul ara-aytum in ahlakaniya **al**l*aa*hu waman ma'iya aw ra*h*iman*aa* faman yujiiru **a**lk*aa*firiina min 'a*dzaa*bin **a**liim**in**

Katakanlah (Muhammad), “Tahukah kamu jika Allah mematikan aku dan orang-orang yang bersamaku atau memberi rahmat kepada kami, (maka kami akan masuk surga), lalu siapa yang dapat melindungi orang-orang kafir dari azab yang pedih?”

67:29

# قُلْ هُوَ الرَّحْمٰنُ اٰمَنَّا بِهٖ وَعَلَيْهِ تَوَكَّلْنَاۚ فَسَتَعْلَمُوْنَ مَنْ هُوَ فِيْ ضَلٰلٍ مُّبِيْنٍ

qul huwa **al**rra*h*m*aa*nu *aa*mann*aa* bihi wa'alayhi tawakkaln*aa* fasata'lamuuna man huwa fii *dh*al*aa*lin mubiin**in**

Katakanlah, “Dialah Yang Maha Pengasih, kami beriman kepada-Nya dan kepada-Nya kami bertawakal. Maka kelak kamu akan tahu siapa yang berada dalam kesesatan yang nyata.”

67:30

# قُلْ اَرَءَيْتُمْ اِنْ اَصْبَحَ مَاۤؤُكُمْ غَوْرًا فَمَنْ يَّأْتِيْكُمْ بِمَاۤءٍ مَّعِيْنٍ ࣖ

qul ara-aytum in a*sh*ba*h*a m*aa*ukum ghawran faman ya/tiikum bim*aa*-in ma'iin**in**

Katakanlah (Muhammad), “Terangkanlah kepadaku jika sumber air kamu menjadi kering; maka siapa yang akan memberimu air yang mengalir?”

<!--EndFragment-->