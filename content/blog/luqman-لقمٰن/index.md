---
title: (31) Luqman - لقمٰن
date: 2021-10-27T03:55:15.039Z
ayat: 31
description: "Jumlah Ayat: 34 / Arti: Luqman"
---
<!--StartFragment-->

31:1

# الۤمّۤ ۗ

alif-l*aa*m-miim

Alif Lam Mim.

31:2

# تِلْكَ اٰيٰتُ الْكِتٰبِ الْحَكِيْمِۙ

tilka *aa*y*aa*tu **a**lkit*aa*bi **a**l*h*akiim**i**

Inilah ayat-ayat Al-Qur'an yang mengandung hikmah,

31:3

# هُدًى وَّرَحْمَةً لِّلْمُحْسِنِيْنَۙ

hudan wara*h*matan lilmu*h*siniin**a**

sebagai petunjuk dan rahmat bagi orang-orang yang berbuat kebaikan,

31:4

# الَّذِيْنَ يُقِيْمُوْنَ الصَّلٰوةَ وَيُؤْتُوْنَ الزَّكٰوةَ وَهُمْ بِالْاٰخِرَةِ هُمْ يُوْقِنُوْنَۗ

**al**la*dz*iina yuqiimuuna **al***shsh*al*aa*ta wayu/tuuna **al**zzak*aa*ta wahum bi**a**l-*aa*khirati hum yuuqinuun**a**

(yaitu) orang-orang yang melaksanakan salat, menunaikan zakat dan mereka meyakini adanya akhirat.

31:5

# اُولٰۤىِٕكَ عَلٰى هُدًى مِّنْ رَّبِّهِمْ وَاُولٰۤىِٕكَ هُمُ الْمُفْلِحُوْنَ

ul*aa*-ika 'al*aa* hudan min rabbihim waul*aa*-ika humu **a**lmufli*h*uun**a**

Merekalah orang-orang yang tetap mendapat petunjuk dari Tuhannya dan mereka itulah orang-orang yang beruntung.

31:6

# وَمِنَ النَّاسِ مَنْ يَّشْتَرِيْ لَهْوَ الْحَدِيْثِ لِيُضِلَّ عَنْ سَبِيْلِ اللّٰهِ بِغَيْرِ عِلْمٍۖ وَّيَتَّخِذَهَا هُزُوًاۗ اُولٰۤىِٕكَ لَهُمْ عَذَابٌ مُّهِيْنٌ

wamina **al**nn*aa*si man yasytarii lahwa **a**l*h*adiitsi liyu*dh*illa 'an sabiili **al**l*aa*hi bighayri 'ilmin wayattakhi*dz*ah*aa* huzuwan ul*aa*-ika lahum 'a*dzaa*bun muhi

Dan di antara manusia (ada) orang yang mempergunakan percakapan kosong untuk menyesatkan (manusia) dari jalan Allah tanpa ilmu dan menjadikannya olok-olokan. Mereka itu akan memperoleh azab yang menghinakan.

31:7

# وَاِذَا تُتْلٰى عَلَيْهِ اٰيٰتُنَا وَلّٰى مُسْتَكْبِرًا كَاَنْ لَّمْ يَسْمَعْهَا كَاَنَّ فِيْٓ اُذُنَيْهِ وَقْرًاۚ فَبَشِّرْهُ بِعَذَابٍ اَلِيْمٍ

wa-i*dzaa* tutl*aa* 'alayhi *aa*y*aa*tun*aa* wall*aa* mustakbiran ka-an lam yasma'h*aa* ka-anna fii u*dz*unayhi waqran fabasysyirhu bi'a*dzaa*bin **a**liim**in**

Dan apabila dibacakan kepadanya ayat-ayat Kami, dia berpaling dengan menyombongkan diri seolah-olah dia belum mendengarnya, seakan-akan ada sumbatan di kedua telinganya, maka gembirakanlah dia dengan azab yang pedih.

31:8

# اِنَّ الَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِ لَهُمْ جَنّٰتُ النَّعِيْمِۙ

inna **al**la*dz*iina *aa*manuu wa'amiluu **al***shshaa*li*haa*ti lahum jann*aa*tu **al**nna'iim**i**

Sesungguhnya orang-orang yang beriman dan mengerjakan kebajikan, mereka akan mendapat surga-surga yang penuh kenikmatan,

31:9

# خٰلِدِيْنَ فِيْهَاۗ وَعْدَ اللّٰهِ حَقًّاۗ وَهُوَ الْعَزِيْزُ الْحَكِيْمُ

kh*aa*lidiina fiih*aa* wa'da **al**l*aa*hi *h*aqqan wahuwa **a**l'aziizu **a**l*h*akiim**u**

mereka kekal di dalamnya, sebagai janji Allah yang benar. Dan Dia Mahaperkasa, Mahabijaksana.

31:10

# خَلَقَ السَّمٰوٰتِ بِغَيْرِ عَمَدٍ تَرَوْنَهَا وَاَلْقٰى فِى الْاَرْضِ رَوَاسِيَ اَنْ تَمِيْدَ بِكُمْ وَبَثَّ فِيْهَا مِنْ كُلِّ دَاۤبَّةٍۗ وَاَنْزَلْنَا مِنَ السَّمَاۤءِ مَاۤءً فَاَنْۢبَتْنَا فِيْهَا مِنْ كُلِّ زَوْجٍ كَرِيْمٍ

khalaqa **al**ssam*aa*w*aa*ti bighayri 'amadin tarawnah*aa* wa-alq*aa* fii **a**l-ar*dh*i raw*aa*siya an tamiida bikum wabatstsa fiih*aa* min kulli d*aa*bbatin wa-anzaln*aa* mina

Dia menciptakan langit tanpa tiang sebagaimana kamu melihatnya, dan Dia meletakkan gunung-gunung (di permukaan) bumi agar ia (bumi) tidak menggoyangkan kamu; dan memperkembangbiakkan segala macam jenis makhluk bergerak yang bernyawa di bumi. Dan Kami turu

31:11

# هٰذَا خَلْقُ اللّٰهِ فَاَرُوْنِيْ مَاذَا خَلَقَ الَّذِيْنَ مِنْ دُوْنِهٖۗ بَلِ الظّٰلِمُوْنَ فِيْ ضَلٰلٍ مُّبِيْنٍ ࣖ

h*aadzaa* khalqu **al**l*aa*hi fa-aruunii m*aatsaa* khalaqa **al**la*dz*iina min duunihi bali **al***zhzhaa*limuuna fii* dh*al*aa*lin mubiin**in**

Inilah ciptaan Allah, maka perlihatkanlah olehmu kepadaku apa yang telah diciptakan oleh (sesembahanmu) selain Allah. Sebenarnya orang-orang yang zalim itu berada di dalam kesesatan yang nyata.

31:12

# وَلَقَدْ اٰتَيْنَا لُقْمٰنَ الْحِكْمَةَ اَنِ اشْكُرْ لِلّٰهِ ۗوَمَنْ يَّشْكُرْ فَاِنَّمَا يَشْكُرُ لِنَفْسِهٖۚ وَمَنْ كَفَرَ فَاِنَّ اللّٰهَ غَنِيٌّ حَمِيْدٌ

walaqad *aa*tayn*aa* luqm*aa*na **a**l*h*ikmata ani usykur lill*aa*hi waman yasykur fa-innam*aa* yasykuru linafsihi waman kafara fa-inna **al**l*aa*ha ghaniyyun *h*amiid**un**

Dan sungguh, telah Kami berikan hikmah kepada Lukman, yaitu, ”Bersyukurlah kepada Allah! Dan barangsiapa bersyukur (kepada Allah), maka sesungguhnya dia bersyukur untuk dirinya sendiri; dan barangsiapa tidak bersyukur (kufur), maka sesungguhnya Allah Mah

31:13

# وَاِذْ قَالَ لُقْمٰنُ لِابْنِهٖ وَهُوَ يَعِظُهٗ يٰبُنَيَّ لَا تُشْرِكْ بِاللّٰهِ ۗاِنَّ الشِّرْكَ لَظُلْمٌ عَظِيْمٌ

wa-i*dz* q*aa*la luqm*aa*nu li**i**bnihi wahuwa ya'i*zh*uhu y*aa* bunayya l*aa* tusyrik bi**al**l*aa*hi inna **al**sysyirka la*zh*ulmun 'a*zh*iim**un**

Dan (ingatlah) ketika Lukman berkata kepada anaknya, ketika dia memberi pelajaran kepadanya, ”Wahai anakku! Janganlah engkau mempersekutukan Allah, sesungguhnya mempersekutukan (Allah) adalah benar-benar kezaliman yang besar.”

31:14

# وَوَصَّيْنَا الْاِنْسَانَ بِوَالِدَيْهِۚ حَمَلَتْهُ اُمُّهٗ وَهْنًا عَلٰى وَهْنٍ وَّفِصَالُهٗ فِيْ عَامَيْنِ اَنِ اشْكُرْ لِيْ وَلِوَالِدَيْكَۗ اِلَيَّ الْمَصِيْرُ

wawa*shsh*ayn*aa* **a**l-ins*aa*na biw*aa*lidayhi *h*amalat-hu ummuhu wahnan 'al*aa* wahnin wafi*shaa*luhu fii '*aa*mayni ani usykur lii waliw*aa*lidayka ilayya **a**lma*sh*iir

Dan Kami perintahkan kepada manusia (agar berbuat baik) kepada kedua orang tuanya. Ibunya telah mengandungnya dalam keadaan lemah yang bertambah-tambah, dan menyapihnya dalam usia dua tahun. Bersyukurlah kepada-Ku dan kepada kedua orang tuamu. Hanya kepad

31:15

# وَاِنْ جَاهَدٰكَ عَلٰٓى اَنْ تُشْرِكَ بِيْ مَا لَيْسَ لَكَ بِهٖ عِلْمٌ فَلَا تُطِعْهُمَا وَصَاحِبْهُمَا فِى الدُّنْيَا مَعْرُوْفًا ۖوَّاتَّبِعْ سَبِيْلَ مَنْ اَنَابَ اِلَيَّۚ ثُمَّ اِلَيَّ مَرْجِعُكُمْ فَاُنَبِّئُكُمْ بِمَا كُنْتُمْ تَعْمَلُوْنَ

wa-in j*aa*had*aa*ka 'al*aa* an tusyrika bii m*aa* laysa laka bihi 'ilmun fal*aa* tu*th*i'hum*aa* wa*shaah*ibhum*aa* fii **al**dduny*aa* ma'ruufan wa**i**ttabi' sabiila man an*a*

Dan jika keduanya memaksamu untuk mempersekutukan Aku dengan sesuatu yang engkau tidak mempunyai ilmu tentang itu, maka janganlah engkau menaati keduanya, dan pergaulilah keduanya di dunia dengan baik, dan ikutilah jalan orang yang kembali kepada-Ku. Kemu

31:16

# يٰبُنَيَّ اِنَّهَآ اِنْ تَكُ مِثْقَالَ حَبَّةٍ مِّنْ خَرْدَلٍ فَتَكُنْ فِيْ صَخْرَةٍ اَوْ فِى السَّمٰوٰتِ اَوْ فِى الْاَرْضِ يَأْتِ بِهَا اللّٰهُ ۗاِنَّ اللّٰهَ لَطِيْفٌ خَبِيْرٌ

y*aa* bunayya innah*aa* in taku mitsq*aa*la *h*abbatin min khardalin fatakun fii *sh*akhratin aw fii **al**ssam*aa*w*aa*ti aw fii **a**l-ar*dh*i ya/ti bih*aa* **al**l*aa*

(Lukman berkata), ”Wahai anakku! Sungguh, jika ada (sesuatu perbuatan) seberat biji sawi, dan berada dalam batu atau di langit atau di bumi, niscaya Allah akan memberinya (balasan). Sesungguhnya Allah Mahahalus, Mahateliti.

31:17

# يٰبُنَيَّ اَقِمِ الصَّلٰوةَ وَأْمُرْ بِالْمَعْرُوْفِ وَانْهَ عَنِ الْمُنْكَرِ وَاصْبِرْ عَلٰى مَآ اَصَابَكَۗ اِنَّ ذٰلِكَ مِنْ عَزْمِ الْاُمُوْرِ

y*aa* bunayya aqimi **al***shsh*al*aa*ta wa/mur bi**a**lma'ruufi wa**i**nha 'ani **a**lmunkari wa**i***sh*bir 'al*aa *m*aa *a*shaa*baka inna* dzaa*lik

Wahai anakku! Laksanakanlah salat dan suruhlah (manusia) berbuat yang makruf dan cegahlah (mereka) dari yang mungkar dan bersabarlah terhadap apa yang menimpamu, sesungguhnya yang demikian itu termasuk perkara yang penting.

31:18

# وَلَا تُصَعِّرْ خَدَّكَ لِلنَّاسِ وَلَا تَمْشِ فِى الْاَرْضِ مَرَحًاۗ اِنَّ اللّٰهَ لَا يُحِبُّ كُلَّ مُخْتَالٍ فَخُوْرٍۚ

wal*aa* tu*sh*a''ir khaddaka li**l**nn*aa*si wal*aa* tamsyi fii **a**l-ar*dh*i mara*h*an inna **al**l*aa*ha l*aa* yu*h*ibbu kulla mukht*aa*lin fakhuur**in**

**Dan janganlah kamu memalingkan wajah dari manusia (karena sombong) dan janganlah berjalan di bumi dengan angkuh. Sungguh, Allah tidak menyukai orang-orang yang sombong dan membanggakan diri.**

31:19

# وَاقْصِدْ فِيْ مَشْيِكَ وَاغْضُضْ مِنْ صَوْتِكَۗ اِنَّ اَنْكَرَ الْاَصْوَاتِ لَصَوْتُ الْحَمِيْرِ ࣖ

wa**i**q*sh*id fii masyyika wa**u**gh*dh*u*dh* min *sh*awtika inna ankara **a**l-a*sh*w*aa*ti la*sh*awtu **a**l*h*amiir**i**

Dan sederhanakanlah dalam berjalan dan lunakkanlah suaramu. Sesungguhnya seburuk-buruk suara ialah suara keledai.”

31:20

# اَلَمْ تَرَوْا اَنَّ اللّٰهَ سَخَّرَ لَكُمْ مَّا فِى السَّمٰوٰتِ وَمَا فِى الْاَرْضِ وَاَسْبَغَ عَلَيْكُمْ نِعَمَهٗ ظَاهِرَةً وَّبَاطِنَةً ۗوَمِنَ النَّاسِ مَنْ يُّجَادِلُ فِى اللّٰهِ بِغَيْرِ عِلْمٍ وَّلَا هُدًى وَّلَا كِتٰبٍ مُّنِيْرٍ

alam taraw anna **al**l*aa*ha sakhkhara lakum m*aa* fii **al**ssam*aa*w*aa*ti wam*aa* fii **a**l-ar*dh*i wa-asbagha 'alaykum ni'amahu *zhaa*hiratan wab*aath*inatan wamina

Tidakkah kamu memperhatikan bahwa Allah telah menundukkan apa yang ada di langit dan apa yang ada di bumi untuk (kepentingan)mu dan menyempurnakan nikmat-Nya untukmu lahir dan batin. Tetapi di antara manusia ada yang membantah tentang (keesaan) Allah tanp

31:21

# وَاِذَا قِيْلَ لَهُمُ اتَّبِعُوْا مَآ اَنْزَلَ اللّٰهُ قَالُوْا بَلْ نَتَّبِعُ مَا وَجَدْنَا عَلَيْهِ اٰبَاۤءَنَاۗ اَوَلَوْ كَانَ الشَّيْطٰنُ يَدْعُوْهُمْ اِلٰى عَذَابِ السَّعِيْرِ

wa-i*dzaa* qiila lahumu ittabi'uu m*aa* anzala **al**l*aa*hu q*aa*luu bal nattabi'u m*aa* wajadn*aa* 'alayhi *aa*b*aa*-an*aa* awa law k*aa*na **al**sysyay*thaa*nu yad'

Dan apabila dikatakan kepada mereka, ”Ikutilah apa yang diturunkan Allah!” Mereka menjawab, ”(Tidak), tetapi kami (hanya) mengikuti kebiasaan yang kami dapati dari nenek moyang kami.” Apakah mereka (akan mengikuti nenek moyang mereka) walaupun sebenarnya

31:22

# ۞ وَمَنْ يُّسْلِمْ وَجْهَهٗٓ اِلَى اللّٰهِ وَهُوَ مُحْسِنٌ فَقَدِ اسْتَمْسَكَ بِالْعُرْوَةِ الْوُثْقٰىۗ وَاِلَى اللّٰهِ عَاقِبَةُ الْاُمُوْرِ

waman yuslim wajhahu il*aa* **al**l*aa*hi wahuwa mu*h*sinun faqadi istamsaka bi**a**l'urwati **a**lwutsq*aa* wa-il*aa* **al**l*aa*hi '*aa*qibatu **a**l-umu

Dan barangsiapa berserah diri kepada Allah, sedang dia orang yang berbuat kebaikan, maka sesungguhnya dia telah berpegang kepada buhul (tali) yang kokoh. Hanya kepada Allah kesudahan segala urusan.

31:23

# وَمَنْ كَفَرَ فَلَا يَحْزُنْكَ كُفْرُهٗۗ اِلَيْنَا مَرْجِعُهُمْ فَنُنَبِّئُهُمْ بِمَا عَمِلُوْاۗ اِنَّ اللّٰهَ عَلِيْمٌۢ بِذَاتِ الصُّدُوْرِ

waman kafara fal*aa* ya*h*zunka kufruhu ilayn*aa* marji'uhum fanunabbi-uhum bim*aa* 'amiluu inna **al**l*aa*ha 'aliimun bi*dzaa*ti **al***shsh*uduur**i**

Dan barangsiapa kafir maka kekafirannya itu janganlah menyedihkanmu (Muhammad). Hanya kepada Kami tempat kembali mereka, lalu Kami beritakan kepada mereka apa yang telah mereka kerjakan. Sesungguhnya Allah Maha Mengetahui segala isi hati.

31:24

# نُمَتِّعُهُمْ قَلِيْلًا ثُمَّ نَضْطَرُّهُمْ اِلٰى عَذَابٍ غَلِيْظٍ

numatti'uhum qaliilan tsumma na*dth*arruhum il*aa* 'a*dzaa*bin ghalii*zh**\*in**

Kami biarkan mereka bersenang-senang sebentar, kemudian Kami paksa mereka (masuk) ke dalam azab yang keras.

31:25

# وَلَىِٕنْ سَاَلْتَهُمْ مَّنْ خَلَقَ السَّمٰوٰتِ وَالْاَرْضَ لَيَقُوْلُنَّ اللّٰهُ ۗقُلِ الْحَمْدُ لِلّٰهِ ۗبَلْ اَكْثَرُهُمْ لَا يَعْلَمُوْنَ

wala-in sa-altahum man khalaqa **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*a layaquulunna **al**l*aa*hu quli **a**l*h*amdu lill*aa*hi bal aktsaruhum l*aa* ya'lamuun**a**

**Dan sungguh, jika engkau (Muhammad) tanyakan kepada mereka, ”Siapakah yang menciptakan langit dan bumi?” Tentu mereka akan menjawab, ”Allah.” Katakanlah, ”Segala puji bagi Allah,” tetapi kebanyakan mereka tidak mengetahui.**

31:26

# لِلّٰهِ مَا فِى السَّمٰوٰتِ وَالْاَرْضِۗ اِنَّ اللّٰهَ هُوَ الْغَنِيُّ الْحَمِيْدُ

lill*aa*hi m*aa* fii **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i inna **al**l*aa*ha huwa **a**lghaniyyu **a**l*h*amiid**u**

Milik Allah-lah apa yang di langit dan di bumi. Sesungguhnya Allah, Dialah Yang Mahakaya, Maha Terpuji.

31:27

# وَلَوْ اَنَّ مَا فِى الْاَرْضِ مِنْ شَجَرَةٍ اَقْلَامٌ وَّالْبَحْرُ يَمُدُّهٗ مِنْۢ بَعْدِهٖ سَبْعَةُ اَبْحُرٍ مَّا نَفِدَتْ كَلِمٰتُ اللّٰهِ ۗاِنَّ اللّٰهَ عَزِيْزٌ حَكِيْمٌ

walaw annam*aa* fii **a**l-ar*dh*i min syajaratin aql*aa*mun wa**a**lba*h*ru yamudduhu min ba'dihi sab'atu ab*h*urin m*aa* nafidat kalim*aa*tu **al**l*aa*hi inna **al**

**Dan seandainya pohon-pohon di bumi menjadi pena dan lautan (menjadi tinta), ditambahkan kepadanya tujuh lautan (lagi) setelah (kering)nya, niscaya tidak akan habis-habisnya (dituliskan) kalimat-kalimat Allah. Sesungguhnya Allah Mahaperkasa, Mahabijaksana.**

31:28

# مَا خَلْقُكُمْ وَلَا بَعْثُكُمْ اِلَّا كَنَفْسٍ وَّاحِدَةٍ ۗاِنَّ اللّٰهَ سَمِيْعٌۢ بَصِيْرٌ

m*aa* khalqukum wal*aa* ba'tsukum ill*aa* kanafsin w*aah*idatin inna **al**l*aa*ha samii'un ba*sh*iir**un**

Menciptakan dan membangkitkan kamu (bagi Allah) hanyalah seperti (menciptakan dan membangkitkan) satu jiwa saja (mudah). Sesungguhnya Allah Maha Mendengar, Maha Melihat.

31:29

# اَلَمْ تَرَ اَنَّ اللّٰهَ يُوْلِجُ الَّيْلَ فِى النَّهَارِ وَيُوْلِجُ النَّهَارَ فِى الَّيْلِ وَسَخَّرَ الشَّمْسَ وَالْقَمَرَۖ كُلٌّ يَّجْرِيْٓ اِلٰٓى اَجَلٍ مُّسَمًّى وَّاَنَّ اللّٰهَ بِمَا تَعْمَلُوْنَ خَبِيْرٌ

alam tara anna **al**l*aa*ha yuuliju **al**layla fii **al**nnah*aa*ri wayuuliju **al**nnah*aa*ra fii **al**layli wasakhkhara **al**sysyamsa wa**a**lq

Tidakkah engkau memperhatikan, bahwa Allah memasukkan malam ke dalam siang dan memasukkan siang ke dalam malam dan Dia menundukkan matahari dan bulan, masing-masing beredar sampai kepada waktu yang ditentukan. Sungguh, Allah Mahateliti apa yang kamu kerja

31:30

# ذٰلِكَ بِاَنَّ اللّٰهَ هُوَ الْحَقُّ وَاَنَّ مَا يَدْعُوْنَ مِنْ دُوْنِهِ الْبَاطِلُۙ وَاَنَّ اللّٰهَ هُوَ الْعَلِيُّ الْكَبِيْرُ ࣖ

*dzaa*lika bi-anna **al**l*aa*ha huwa **a**l*h*aqqu wa-anna m*aa* yad'uuna min duunihi **a**lb*aath*ilu wa-anna **al**l*aa*ha huwa **a**l'aliyyu **a**

**Demikianlah, karena sesungguhnya Allah, Dialah (Tuhan) yang sebenarnya dan apa saja yang mereka seru selain Allah adalah batil. Dan sesungguhnya Allah, Dialah Yang Mahatinggi, Mahabesar.**

31:31

# اَلَمْ تَرَ اَنَّ الْفُلْكَ تَجْرِيْ فِى الْبَحْرِ بِنِعْمَتِ اللّٰهِ لِيُرِيَكُمْ مِّنْ اٰيٰتِهٖۗ اِنَّ فِيْ ذٰلِكَ لَاٰيٰتٍ لِّكُلِّ صَبَّارٍ شَكُوْرٍ

alam tara anna **a**lfulka tajrii fii **a**lba*h*ri bini'mati **al**l*aa*hi liyuriyakum min *aa*y*aa*tihi inna fii *dzaa*lika la*aa*y*aa*tin likulli *sh*abb*aa*rin syakuur

Tidakkah engkau memperhatikan bahwa sesungguhnya kapal itu berlayar di laut dengan nikmat Allah, agar diperlihatkan-Nya kepadamu sebagian dari tanda-tanda (kebesaran)-Nya. Sungguh, pada yang demikian itu terdapat tanda-tanda (kebesaran)-Nya bagi setiap or

31:32

# وَاِذَا غَشِيَهُمْ مَّوْجٌ كَالظُّلَلِ دَعَوُا اللّٰهَ مُخْلِصِيْنَ لَهُ الدِّيْنَ ەۚ فَلَمَّا نَجّٰىهُمْ اِلَى الْبَرِّ فَمِنْهُمْ مُّقْتَصِدٌۗ وَمَا يَجْحَدُ بِاٰيٰتِنَآ اِلَّا كُلُّ خَتَّارٍ كَفُوْرٍ

wa-i*dzaa* ghasyiyahum mawjun ka**al***zhzh*ulali da'awuu **al**l*aa*ha mukhli*sh*iina lahu **al**ddiina falamm*aa *najj*aa*hum il*aa* **a**lbarri faminhum muqta*sh*

*Dan apabila mereka digulung ombak yang besar seperti gunung, mereka menyeru Allah dengan tulus ikhlas beragama kepada-Nya. Tetapi ketika Allah menyelamatkan mereka sampai di daratan, lalu sebagian mereka tetap menempuh jalan yang lurus. Adapun yang mengin*

31:33

# يٰٓاَيُّهَا النَّاسُ اتَّقُوْا رَبَّكُمْ وَاخْشَوْا يَوْمًا لَّا يَجْزِيْ وَالِدٌ عَنْ وَّلَدِهٖۖ وَلَا مَوْلُوْدٌ هُوَ جَازٍ عَنْ وَّالِدِهٖ شَيْـًٔاۗ اِنَّ وَعْدَ اللّٰهِ حَقٌّ فَلَا تَغُرَّنَّكُمُ الْحَيٰوةُ الدُّنْيَاۗ وَلَا يَغُرَّنَّكُمْ بِاللّٰهِ ا

y*aa* ayyuh*aa* **al**nn*aa*su ittaquu rabbakum wa**i**khsyaw yawman l*aa* yajzii w*aa*lidun 'an waladihi wal*aa* mawluudun huwa j*aa*zin 'an w*aa*lidihi syay-an inna wa'da **al**

**Wahai manusia! Bertakwalah kepada Tuhanmu dan takutlah pada hari yang (ketika itu) seorang bapak tidak dapat menolong anaknya, dan seorang anak tidak dapat (pula) menolong bapaknya sedikit pun. Sungguh, janji Allah pasti benar, maka janganlah sekali-kali**

31:34

# اِنَّ اللّٰهَ عِنْدَهٗ عِلْمُ السَّاعَةِۚ وَيُنَزِّلُ الْغَيْثَۚ وَيَعْلَمُ مَا فِى الْاَرْحَامِۗ وَمَا تَدْرِيْ نَفْسٌ مَّاذَا تَكْسِبُ غَدًاۗ وَمَا تَدْرِيْ نَفْسٌۢ بِاَيِّ اَرْضٍ تَمُوْتُۗ اِنَّ اللّٰهَ عَلِيْمٌ خَبِيْرٌ ࣖ

inna **al**l*aa*ha 'indahu 'ilmu **al**ss*aa*'ati wayunazzilu **a**lghaytsa waya'lamu m*aa* fii **a**l-ar*haa*mi wam*aa* tadrii nafsun m*aatsaa* taksibu ghadan wam*aa*



Sesungguhnya Allah, hanya pada sisi-Nya sajalah pengetahuan tentang Hari Kiamat; dan Dialah Yang menurunkan hujan, dan mengetahui apa yang ada dalam rahim. Dan tiada seorangpun yang dapat mengetahui (dengan pasti) apa yang akan diusahakannya besok. Dan tiada seorangpun yang dapat mengetahui di bumi mana dia akan mati. Sesungguhnya Allah Maha Mengetahui lagi Maha Mengenal.

<!--EndFragment-->