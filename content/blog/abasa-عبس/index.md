---
title: (80) 'Abasa - عبس
date: 2021-10-27T04:09:04.589Z
ayat: 80
description: "Jumlah Ayat: 42 / Arti: Bermuka Masam"
---
<!--StartFragment-->

80:1

# عَبَسَ وَتَوَلّٰىٓۙ

'abasa watawall*aa*

Dia (Muhammad) berwajah masam dan berpaling,

80:2

# اَنْ جَاۤءَهُ الْاَعْمٰىۗ

an j*aa*-ahu **a**l-a'm*aa*

karena seorang buta telah datang kepadanya (Abdullah bin Ummi Maktum).

80:3

# وَمَا يُدْرِيْكَ لَعَلَّهٗ يَزَّكّٰىٓۙ

wam*aa* yudriika la'allahu yazzakk*aa*

Dan tahukah engkau (Muhammad) barangkali dia ingin menyucikan dirinya (dari dosa),

80:4

# اَوْ يَذَّكَّرُ فَتَنْفَعَهُ الذِّكْرٰىۗ

aw ya*dzdz*akkaru fatanfa'ahu **al***dzdz*ikr*aa*

atau dia (ingin) mendapatkan pengajaran, yang memberi manfaat kepadanya?

80:5

# اَمَّا مَنِ اسْتَغْنٰىۙ

amm*aa* mani istaghn*aa*

Adapun orang yang merasa dirinya serba cukup (pembesar-pembesar Quraisy),

80:6

# فَاَنْتَ لَهٗ تَصَدّٰىۗ

fa-anta lahu ta*sh*add*aa*

maka engkau (Muhammad) memberi perhatian kepadanya,

80:7

# وَمَا عَلَيْكَ اَلَّا يَزَّكّٰىۗ

wam*aa* 'alayka **al**l*aa* yazzakk*aa*

padahal tidak ada (cela) atasmu kalau dia tidak menyucikan diri (beriman).

80:8

# وَاَمَّا مَنْ جَاۤءَكَ يَسْعٰىۙ

wa-amm*aa* man j*aa*-aka yas'*aa*

Dan adapun orang yang datang kepadamu dengan bersegera (untuk mendapatkan pengajaran),

80:9

# وَهُوَ يَخْشٰىۙ

wahuwa yakhsy*aa*

sedang dia takut (kepada Allah),

80:10

# فَاَنْتَ عَنْهُ تَلَهّٰىۚ

fa-anta 'anhu talahh*aa*

engkau (Muhammad) malah mengabaikannya.

80:11

# كَلَّآ اِنَّهَا تَذْكِرَةٌ ۚ

kall*aa* innah*aa* ta*dz*kira**tun**

Sekali-kali jangan (begitu)! Sungguh, (ajaran-ajaran Allah) itu suatu peringatan,

80:12

# فَمَنْ شَاۤءَ ذَكَرَهٗ ۘ

faman sy*aa*-a *dz*akarah**u**

maka barangsiapa menghendaki, tentulah dia akan memperhatikannya,

80:13

# فِيْ صُحُفٍ مُّكَرَّمَةٍۙ

fii *sh*u*h*ufin mukarrama**tin**

di dalam kitab-kitab yang dimuliakan (di sisi Allah),

80:14

# مَّرْفُوْعَةٍ مُّطَهَّرَةٍ ۢ ۙ

marfuu'atin mu*th*ahhara**tin**

yang ditinggikan (dan) disucikan,

80:15

# بِاَيْدِيْ سَفَرَةٍۙ

bi-aydii safara**tin**

di tangan para utusan (malaikat),

80:16

# كِرَامٍۢ بَرَرَةٍۗ

kir*aa*min barara**tin**

yang mulia lagi berbakti.

80:17

# قُتِلَ الْاِنْسَانُ مَآ اَكْفَرَهٗۗ

qutila **a**l-ins*aa*nu m*aa* akfarah**u**

Celakalah manusia! Alangkah kufurnya dia!

80:18

# مِنْ اَيِّ شَيْءٍ خَلَقَهٗۗ

min ayyi syay-in khalaqah**u**

Dari apakah Dia (Allah) menciptakannya?

80:19

# مِنْ نُّطْفَةٍۗ خَلَقَهٗ فَقَدَّرَهٗۗ

min nu*th*fatin khalaqahu faqaddarah**u**

Dari setetes mani, Dia menciptakannya lalu menentukannya.

80:20

# ثُمَّ السَّبِيْلَ يَسَّرَهٗۙ

tsumma **al**ssabiila yassarah**u**

Kemudian jalannya Dia mudahkan,

80:21

# ثُمَّ اَمَاتَهٗ فَاَقْبَرَهٗۙ

tsumma am*aa*tahu fa-aqbarah**u**

kemudian Dia mematikannya lalu menguburkannya,

80:22

# ثُمَّ اِذَا شَاۤءَ اَنْشَرَهٗۗ

tsumma i*dzaa* sy*aa*-a ansyarah**u**

kemudian jika Dia menghendaki, Dia membangkitkannya kembali.

80:23

# كَلَّا لَمَّا يَقْضِ مَآ اَمَرَهٗۗ

kall*aa* lamm*aa* yaq*dh*i m*aa* amarah**u**

Sekali-kali jangan (begitu)! Dia (manusia) itu belum melaksanakan apa yang Dia (Allah) perintahkan kepadanya.

80:24

# فَلْيَنْظُرِ الْاِنْسَانُ اِلٰى طَعَامِهٖٓ ۙ

falyan*zh*uri **a**l-ins*aa*nu il*aa* *th*a'*aa*mih**i**

Maka hendaklah manusia itu memperhatikan makanannya.

80:25

# اَنَّا صَبَبْنَا الْمَاۤءَ صَبًّاۙ

ann*aa* *sh*ababn*aa* **a**lm*aa*-a *sh*abb*aa***n**

Kamilah yang telah mencurahkan air melimpah (dari langit),

80:26

# ثُمَّ شَقَقْنَا الْاَرْضَ شَقًّاۙ

tsumma syaqaqn*aa* **a**l-ar*dh*a syaqq*aa***n**

kemudian Kami belah bumi dengan sebaik-baiknya,

80:27

# فَاَنْۢبَتْنَا فِيْهَا حَبًّاۙ

fa-anbatn*aa* fiih*aa* *h*abb*aa***n**

lalu di sana Kami tumbuhkan biji-bijian,

80:28

# وَّعِنَبًا وَّقَضْبًاۙ

wa'inaban waqa*dh*b*aa***n**

dan anggur dan sayur-sayuran,

80:29

# وَّزَيْتُوْنًا وَّنَخْلًاۙ

wazaytuunan wanakhl*aa***n**

dan zaitun dan pohon kurma,

80:30

# وَّحَدَاۤئِقَ غُلْبًا

wa*h*ad*aa*-iqa ghulb*aa***n**

dan kebun-kebun (yang) rindang,

80:31

# وَفَاكِهَةً وَّاَبًّا

waf*aa*kihatan wa-abb*aa***n**

dan buah-buahan serta rerumputan.

80:32

# مَتَاعًا لَّكُمْ وَلِاَنْعَامِكُمْۗ

mat*aa*'an lakum wali-an'*aa*mikum

(Semua itu) untuk kesenanganmu dan untuk hewan-hewan ternakmu.

80:33

# فَاِذَا جَاۤءَتِ الصَّاۤخَّةُ ۖ

fa-i*dzaa* j*aa*-ati **al***shshaa*khkha**tu**

Maka apabila datang suara yang memekakkan (tiupan sangkakala yang kedua),

80:34

# يَوْمَ يَفِرُّ الْمَرْءُ مِنْ اَخِيْهِۙ

yawma yafirru **a**lmaru min akhiih**i**

pada hari itu manusia lari dari saudaranya,

80:35

# وَاُمِّهٖ وَاَبِيْهِۙ

waummihi wa-abiih**i**

dan dari ibu dan bapaknya,

80:36

# وَصَاحِبَتِهٖ وَبَنِيْهِۗ

wa*shaah*ibatihi wabaniih**i**

dan dari istri dan anak-anaknya.

80:37

# لِكُلِّ امْرِئٍ مِّنْهُمْ يَوْمَىِٕذٍ شَأْنٌ يُّغْنِيْهِۗ

likulli imri-in minhum yawma-i*dz*in sya/nun yughniih**i**

Setiap orang dari mereka pada hari itu mempunyai urusan yang menyibukkannya.

80:38

# وُجُوْهٌ يَّوْمَىِٕذٍ مُّسْفِرَةٌۙ

wujuuhun yawma-i*dz*in musfira**tun**

Pada hari itu ada wajah-wajah yang berseri-seri,

80:39

# ضَاحِكَةٌ مُّسْتَبْشِرَةٌ ۚ

*dah*ikatun mustabsyira**tun**

tertawa dan gembira ria,

80:40

# وَوُجُوْهٌ يَّوْمَىِٕذٍ عَلَيْهَا غَبَرَةٌۙ

wawujuuhun yawma-i*dz*in 'alayh*aa* ghabara**tun**

dan pada hari itu ada (pula) wajah-wajah yang tertutup debu (suram),

80:41

# تَرْهَقُهَا قَتَرَةٌ ۗ

tarhaquh*aa* qatara**tun**

tertutup oleh kegelapan (ditimpa kehinaan dan kesusahan).

80:42

# اُولٰۤىِٕكَ هُمُ الْكَفَرَةُ الْفَجَرَةُ ࣖ

ul*aa*-ika humu **a**lkafaratu **a**lfajara**tu**

Mereka itulah orang-orang kafir yang durhaka.

<!--EndFragment-->