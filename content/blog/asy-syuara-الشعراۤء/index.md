---
title: (26) Asy-Syu'ara' - الشعراۤء
date: 2021-10-27T03:52:57.049Z
ayat: 26
description: "Jumlah Ayat: 227 / Arti: Para Penyair"
---
<!--StartFragment-->

26:1

# طٰسۤمّۤ

*thaa*-siin-miim

Tha Sin Mim

26:2

# تِلْكَ اٰيٰتُ الْكِتٰبِ الْمُبِيْنِ

tilka *aa*y*aa*tu **a**lkit*aa*bi **a**lmubiin**i**

Inilah ayat-ayat Kitab (Al-Qur'an) yang jelas.

26:3

# لَعَلَّكَ بَاخِعٌ نَّفْسَكَ اَلَّا يَكُوْنُوْا مُؤْمِنِيْنَ

la'allaka b*aa*khi'un nafsaka **al**l*aa* yakuunuu mu/miniin**a**

Boleh jadi engkau (Muhammad) akan membinasakan dirimu (dengan kesedihan), karena mereka (penduduk Mekah) tidak beriman.

26:4

# اِنْ نَّشَأْ نُنَزِّلْ عَلَيْهِمْ مِّنَ السَّمَاۤءِ اٰيَةً فَظَلَّتْ اَعْنَاقُهُمْ لَهَا خَاضِعِيْنَ

in nasya/ nunazzil 'alayhim mina **al**ssam*aa*-i *aa*yatan fa*zh*allat a'n*aa*quhum lah*aa* kh*aad*i'iin**a**

Jika Kami menghendaki, niscaya Kami turunkan kepada mereka mukjizat dari langit, yang akan membuat tengkuk mereka tunduk dengan rendah hati kepadanya.

26:5

# وَمَا يَأْتِيْهِمْ مِّنْ ذِكْرٍ مِّنَ الرَّحْمٰنِ مُحْدَثٍ اِلَّا كَانُوْا عَنْهُ مُعْرِضِيْنَ

wam*aa* ya/tiihim min *dz*ikrin mina **al**rra*h*m*aa*ni mu*h*datsin ill*aa* k*aa*nuu 'anhu mu'ri*dh*iin**a**

Dan setiap kali disampaikan kepada mereka suatu peringatan baru (ayat Al-Qur'an yang diturunkan) dari Tuhan Yang Maha Pengasih, mereka selalu berpaling darinya.

26:6

# فَقَدْ كَذَّبُوْا فَسَيَأْتِيْهِمْ اَنْۢبـٰۤؤُا مَا كَانُوْا بِهٖ يَسْتَهْزِءُوْنَ

faqad ka*dzdz*abuu fasaya/tiihim anb*aa*u m*aa* k*aa*nuu bihi yastahzi-uun**a**

Sungguh, mereka telah mendustakan (Al-Qur'an), maka kelak akan datang kepada mereka (kebenaran) berita-berita mengenai apa (azab) yang dulu mereka perolok-olokkan.

26:7

# اَوَلَمْ يَرَوْا اِلَى الْاَرْضِ كَمْ اَنْۢبَتْنَا فِيْهَا مِنْ كُلِّ زَوْجٍ كَرِيْمٍ

awa lam yaraw il*aa* **a**l-ar*dh*i kam anbatn*aa* fiih*aa* min kulli zawjin kariim**in**

Dan apakah mereka tidak memperhatikan bumi, betapa banyak Kami tumbuhkan di bumi itu berbagai macam pasangan (tumbuh-tumbuhan) yang baik?

26:8

# اِنَّ فِيْ ذٰلِكَ لَاٰيَةًۗ وَمَا كَانَ اَكْثَرُهُمْ مُّؤْمِنِيْنَ

inna fii *dzaa*lika la*aa*yatan wam*aa* k*aa*na aktsaruhum mu/miniin**a**

Sungguh, pada yang demikian itu terdapat tanda (kebesaran Allah), tetapi kebanyakan mereka tidak beriman.

26:9

# وَاِنَّ رَبَّكَ لَهُوَ الْعَزِيْزُ الرَّحِيْمُ ࣖ

wa-inna rabbaka lahuwa **a**l'aziizu **al**rra*h*iim**u**

Dan sungguh, Tuhanmu Dialah Yang Mahaperkasa, Maha Penyayang.

26:10

# وَاِذْ نَادٰى رَبُّكَ مُوْسٰٓى اَنِ ائْتِ الْقَوْمَ الظّٰلِمِيْنَ ۙ

wa-i*dz* n*aa*d*aa* rabbuka muus*aa* ani i/ti **a**lqawma **al***zhzhaa*limiin**a**

Dan (ingatlah) ketika Tuhanmu menyeru Musa (dengan firman-Nya), “Datangilah kaum yang zalim itu,

26:11

# قَوْمَ فِرْعَوْنَ ۗ اَلَا يَتَّقُوْنَ

qawma fir'awna **a**l*aa* yattaquun**a**

(yaitu) kaum Fir‘aun. Mengapa mereka tidak bertakwa?”

26:12

# قَالَ رَبِّ اِنِّيْٓ اَخَافُ اَنْ يُّكَذِّبُوْنِ ۗ

q*aa*la rabbi innii akh*aa*fu an yuka*dzdz*ibuun**i**

Dia (Musa) berkata, “Ya Tuhanku, sungguh, aku takut mereka akan mendustakan aku,

26:13

# وَيَضِيْقُ صَدْرِيْ وَلَا يَنْطَلِقُ لِسَانِيْ فَاَرْسِلْ اِلٰى هٰرُوْنَ

waya*dh*iiqu *sh*adrii wal*aa* yan*th*aliqu lis*aa*nii fa-arsil il*aa* h*aa*ruun**a**

sehingga dadaku terasa sempit dan lidahku tidak lancar, maka utuslah Harun (bersamaku).

26:14

# وَلَهُمْ عَلَيَّ ذَنْۢبٌ فَاَخَافُ اَنْ يَّقْتُلُوْنِ ۚ

walahum 'alayya *dz*anbun fa-akh*aa*fu an yaqtuluun**i**

Sebab aku berdosa terhadap mereka, maka aku takut mereka akan membunuhku.”

26:15

# قَالَ كَلَّاۚ فَاذْهَبَا بِاٰيٰتِنَآ اِنَّا مَعَكُمْ مُّسْتَمِعُوْنَ ۙ

q*aa*la kall*aa* fa**i***dz*hab*aa *bi-*aa*y*aa*tin*aa *inn*aa* ma'akum mustami'uun**a**

(Allah) berfirman, “Jangan takut (mereka tidak akan dapat membunuhmu)! Maka pergilah kamu berdua dengan membawa ayat-ayat Kami (mukjizat-mukjizat); sungguh, Kami bersamamu mendengarkan (apa yang mereka katakan),

26:16

# فَأْتِيَا فِرْعَوْنَ فَقُوْلَآ اِنَّا رَسُوْلُ رَبِّ الْعٰلَمِيْنَ ۙ

fa/tiy*aa* fir'awna faquul*aa* inn*aa* rasuulu rabbi **a**l'*aa*lamiin**a**

maka datanglah kamu berdua kepada Fir‘aun dan katakan, “Sesungguhnya kami adalah rasul-rasul Tuhan seluruh alam,

26:17

# اَنْ اَرْسِلْ مَعَنَا بَنِيْٓ اِسْرَاۤءِيْلَ ۗ

an arsil ma'an*aa* banii isr*aa*-iil**a**

lepaskanlah Bani Israil (pergi) bersama kami.”

26:18

# قَالَ اَلَمْ نُرَبِّكَ فِيْنَا وَلِيْدًا وَّلَبِثْتَ فِيْنَا مِنْ عُمُرِكَ سِنِيْنَ ۗ

q*aa*la alam nurabbika fiin*aa* waliidan walabitsta fiin*aa* min 'umurika siniin**a**

Dia (Fir‘aun) menjawab, “Bukankah kami telah mengasuhmu dalam lingkungan (keluarga) kami, waktu engkau masih kanak-kanak dan engkau tinggal bersama kami beberapa tahun dari umurmu.

26:19

# وَفَعَلْتَ فَعْلَتَكَ الَّتِيْ فَعَلْتَ وَاَنْتَ مِنَ الْكٰفِرِيْنَ

wafa'alta fa'lataka **al**latii fa'alta wa-anta mina **a**lk*aa*firiin**a**

Dan engkau (Musa) telah melakukan (kesalahan dari) perbuatan yang telah engkau lakukan dan engkau termasuk orang yang tidak tahu berterima kasih.”

26:20

# قَالَ فَعَلْتُهَآ اِذًا وَّاَنَا۠ مِنَ الضَّاۤلِّيْنَ

q*aa*la fa'altuh*aa* i*dz*an wa-an*aa* mina **al***dhdhaa*lliin**a**

Dia (Musa) berkata, “Aku telah melakukannya, dan ketika itu aku termasuk orang yang khilaf.

26:21

# فَفَرَرْتُ مِنْكُمْ لَمَّا خِفْتُكُمْ فَوَهَبَ لِيْ رَبِّيْ حُكْمًا وَّجَعَلَنِيْ مِنَ الْمُرْسَلِيْنَ

fafarartu minkum lamm*aa* khiftukum fawahaba lii rabbii *h*ukman waja'alanii mina **a**lmursaliin**a**

Lalu aku lari darimu karena aku takut kepadamu, kemudian Tuhanku menganugerahkan ilmu kepadaku serta Dia menjadikan aku salah seorang di antara rasul-rasul.

26:22

# وَتِلْكَ نِعْمَةٌ تَمُنُّهَا عَلَيَّ اَنْ عَبَّدْتَّ بَنِيْٓ اِسْرَاۤءِيْلَ ۗ

watilka ni'matun tamunnuh*aa* 'alayya an 'abbadta banii isr*aa*-iil**a**

Dan itulah kebaikan yang telah engkau berikan kepadaku, (sementara) itu engkau telah memperbudak Bani Israil.”

26:23

# قَالَ فِرْعَوْنُ وَمَا رَبُّ الْعٰلَمِيْنَ ۗ

q*aa*la fir'awnu wam*aa* rabbu **a**l'*aa*lamiin**a**

Fir‘aun bertanya, “Siapa Tuhan seluruh alam itu?”

26:24

# قَالَ رَبُّ السَّمٰوٰتِ وَالْاَرْضِ وَمَا بَيْنَهُمَاۗ اِنْ كُنْتُمْ مُّوْقِنِيْنَ

q*aa*la rabbu **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i wam*aa* baynahum*aa* in kuntum muuqiniin**a**

Dia (Musa) menjawab, “Tuhan pencipta langit dan bumi dan apa yang ada di antara keduanya (itulah Tuhanmu), jika kamu mempercayai-Nya.”

26:25

# قَالَ لِمَنْ حَوْلَهٗٓ اَلَا تَسْتَمِعُوْنَ

q*aa*la liman *h*awlahu **a**l*aa* tastami'uun**a**

Dia (Fir‘aun) berkata kepada orang-orang di sekelilingnya, “Apakah kamu tidak mendengar (apa yang dikatakannya)?”

26:26

# قَالَ رَبُّكُمْ وَرَبُّ اٰبَاۤىِٕكُمُ الْاَوَّلِيْنَ

q*aa*la rabbukum warabbu *aa*b*aa*-ikumu **a**l-awwaliin**a**

Dia (Musa) berkata, “(Dia) Tuhanmu dan juga Tuhan nenek moyangmu terdahulu.”

26:27

# قَالَ اِنَّ رَسُوْلَكُمُ الَّذِيْٓ اُرْسِلَ اِلَيْكُمْ لَمَجْنُوْنٌ

q*aa*la inna rasuulakumu **al**la*dz*ii ursila ilaykum lamajnuun**un**

Dia (Fir‘aun) berkata, “Sungguh, Rasulmu yang diutus kepada kamu benar-benar orang gila.”

26:28

# قَالَ رَبُّ الْمَشْرِقِ وَالْمَغْرِبِ وَمَا بَيْنَهُمَاۗ اِنْ كُنْتُمْ تَعْقِلُوْنَ

q*aa*la rabbu **a**lmasyriqi wa**a**lmaghribi wam*aa* baynahum*aa* in kuntum ta'qiluun**a**

Dia (Musa) berkata, “(Dialah) Tuhan (yang menguasai) timur dan barat dan apa yang ada di antara keduanya; jika kamu mengerti.”

26:29

# قَالَ لَىِٕنِ اتَّخَذْتَ اِلٰهًا غَيْرِيْ لَاَجْعَلَنَّكَ مِنَ الْمَسْجُوْنِيْنَ

q*aa*la la-ini ittakha*dz*ta il*aa*han ghayrii la-aj'alannaka mina **a**lmasjuuniin**a**

Dia (Fir‘aun) berkata, “Sungguh, jika engkau menyembah Tuhan selain aku, pasti aku masukkan engkau ke dalam penjara.”

26:30

# قَالَ اَوَلَوْ جِئْتُكَ بِشَيْءٍ مُّبِيْنٍ

q*aa*la awa law ji/tuka bisyay-in mubiin**in**

Dia (Musa) berkata, “Apakah (engkau akan melakukan itu) sekalipun aku tunjukkan kepadamu sesuatu (bukti) yang nyata?”

26:31

# قَالَ فَأْتِ بِهٖٓ اِنْ كُنْتَ مِنَ الصّٰدِقِيْنَ

q*aa*la fa/ti bihi in kunta mina **al***shshaa*diqiin**a**

Dia (Fir‘aun) berkata, “Tunjukkan sesuatu (bukti yang nyata) itu, jika engkau termasuk orang yang benar!”

26:32

# فَاَلْقٰى عَصَاهُ فَاِذَا هِيَ ثُعْبَانٌ مُّبِيْنٌ ۚ

fa-alq*aa* 'a*shaa*hu fa-i*dzaa* hiya tsu'b*aa*nun mubiin**un**

Maka dia (Musa) melemparkan tongkatnya, tiba-tiba tongkat itu menjadi ular besar yang sebenarnya.

26:33

# وَنَزَعَ يَدَهٗ فَاِذَا هِيَ بَيْضَاۤءُ لِلنّٰظِرِيْنَ ࣖ

wanaza'a yadahu fa-i*dzaa* hiya bay*daa*u li**l**nn*aats*iriin**a**

Dan dia mengeluarkan tangannya (dari dalam bajunya), tiba-tiba tangan itu menjadi putih (bercahaya) bagi orang-orang yang melihatnya.

26:34

# قَالَ لِلْمَلَاِ حَوْلَهٗٓ اِنَّ هٰذَا لَسٰحِرٌ عَلِيْمٌ ۙ

q*aa*la lilmala-i *h*awlahu inna h*aadzaa* las*aah*irun 'aliim**un**

Dia (Fir‘aun) berkata kepada para pemuka di sekelilingnya, “Sesungguhnya dia (Musa) ini pasti seorang pesihir yang pandai,

26:35

# يُّرِيْدُ اَنْ يُّخْرِجَكُمْ مِّنْ اَرْضِكُمْ بِسِحْرِهٖۖ فَمَاذَا تَأْمُرُوْنَ

yuriidu an yukhrijakum min ar*dh*ikum bisi*h*rihi fam*aatsaa* ta/muruun**a**

dia hendak mengusir kamu dari negerimu dengan sihirnya; karena itu apakah yang kamu sarankan?”

26:36

# قَالُوْٓا اَرْجِهْ وَاَخَاهُ وَابْعَثْ فِى الْمَدَاۤىِٕنِ حٰشِرِيْنَ ۙ

q*aa*luu arjih wa-akh*aa*hu wa**i**b'ats fii **a**lmad*aa*-ini *haa*syiriin**a**

Mereka menjawab, “Tahanlah (untuk sementara) dia dan saudaranya, dan utuslah ke seluruh negeri orang-orang yang akan mengumpulkan (pesihir),

26:37

# يَأْتُوْكَ بِكُلِّ سَحَّارٍ عَلِيْمٍ

ya/tuuka bikulli sa*hhaa*rin 'aliim**in**

niscaya mereka akan mendatangkan semua pesihir yang pandai kepadamu.”

26:38

# فَجُمِعَ السَّحَرَةُ لِمِيْقَاتِ يَوْمٍ مَّعْلُوْمٍ ۙ

fajumi'a **al**ssa*h*aratu limiiq*aa*ti yawmin ma'luum**in**

Lalu dikumpulkanlah para pesihir pada waktu (yang ditetapkan) pada hari yang telah ditentukan,

26:39

# وَّقِيْلَ لِلنَّاسِ هَلْ اَنْتُمْ مُّجْتَمِعُوْنَ ۙ

waqiila li**l**nn*aa*si hal antum mujtami'uun**a**

dan diumumkan kepada orang banyak, “Berkumpullah kamu semua,

26:40

# لَعَلَّنَا نَتَّبِعُ السَّحَرَةَ اِنْ كَانُوْا هُمُ الْغٰلِبِيْنَ

la'allan*aa* nattabi'u **al**ssa*h*arata in k*aa*nuu humu **a**lgh*aa*libiin**a**

agar kita mengikuti para pesihir itu, jika mereka yang menang.”

26:41

# فَلَمَّا جَاۤءَ السَّحَرَةُ قَالُوْا لِفِرْعَوْنَ اَىِٕنَّ لَنَا لَاَجْرًا اِنْ كُنَّا نَحْنُ الْغٰلِبِيْنَ

falamm*aa* j*aa*-a **al**ssa*h*aratu q*aa*luu lifir'awna a-inna lan*aa* la-ajran in kunn*aa* na*h*nu **a**lgh*aa*libiin**a**

Maka ketika para pesihir datang, mereka berkata kepada Fir‘aun, “Apakah kami benar-benar akan mendapat imbalan yang besar jika kami yang menang?”

26:42

# قَالَ نَعَمْ وَاِنَّكُمْ اِذًا لَّمِنَ الْمُقَرَّبِيْنَ

q*aa*la na'am wa-innakum i*dz*an lamina **a**lmuqarrabiin**a**

Dia (Fir‘aun) menjawab, “Ya, dan bahkan kamu pasti akan mendapat kedudukan yang dekat (kepadaku).”

26:43

# قَالَ لَهُمْ مُّوْسٰٓى اَلْقُوْا مَآ اَنْتُمْ مُّلْقُوْنَ

q*aa*la lahum muus*aa* **a**lquu m*aa* antum mulquun**a**

Dia (Musa) berkata kepada mereka, “Lemparkanlah apa yang hendak kamu lemparkan.”

26:44

# فَاَلْقَوْا حِبَالَهُمْ وَعِصِيَّهُمْ وَقَالُوْا بِعِزَّةِ فِرْعَوْنَ اِنَّا لَنَحْنُ الْغٰلِبُوْنَ

fa-alqaw *h*ib*aa*lahum wa'i*sh*iyyahum waq*aa*luu bi'izzati fir'awna inn*aa* lana*h*nu **a**lgh*aa*libuun**a**

Lalu mereka melemparkan tali temali dan tongkat-tongkat mereka seraya berkata, “Demi kekuasaan Fir‘aun, pasti kamilah yang akan menang.”

26:45

# فَاَلْقٰى مُوْسٰى عَصَاهُ فَاِذَا هِيَ تَلْقَفُ مَا يَأْفِكُوْنَ ۚ

fa-alq*aa* muus*aa* 'a*shaa*hu fa-i*dzaa* hiya talqafu m*aa* ya/fikuun**a**

Kemudian Musa melemparkan tongkatnya, maka tiba-tiba ia menelan benda-benda palsu yang mereka ada-adakan itu.

26:46

# فَاُلْقِيَ السَّحَرَةُ سٰجِدِيْنَ ۙ

faulqiya **al**ssa*h*aratu s*aa*jidiin**a**

Maka menyungkurlah para pesihir itu, bersujud.

26:47

# قَالُوْٓا اٰمَنَّا بِرَبِّ الْعٰلَمِيْنَ ۙ

q*aa*luu *aa*mann*aa* birabbi **a**l'*aa*lamiin**a**

Mereka berkata, “Kami beriman kepada Tuhan seluruh alam,

26:48

# رَبِّ مُوْسٰى وَهٰرُوْنَ

rabbi muus*aa* wah*aa*ruun**a**

(yaitu) Tuhannya Musa dan Harun.”

26:49

# قَالَ اٰمَنْتُمْ لَهٗ قَبْلَ اَنْ اٰذَنَ لَكُمْۚ اِنَّهٗ لَكَبِيْرُكُمُ الَّذِيْ عَلَّمَكُمُ السِّحْرَۚ فَلَسَوْفَ تَعْلَمُوْنَ ەۗ لَاُقَطِّعَنَّ اَيْدِيَكُمْ وَاَرْجُلَكُمْ مِّنْ خِلَافٍ وَّلَاُصَلِّبَنَّكُمْ اَجْمَعِيْنَۚ

q*aa*la *aa*mantum lahu qabla an *aadz*ana lakum innahu lakabiirukumu **al**la*dz*ii 'allamakumu **al**ssi*h*ra falasawfa ta'lamuuna lauqa*ththh*i'anna aydiyakum wa-arjulakum min khil*aa*fin wal

Dia (Fir‘aun) berkata, “Mengapa kamu beriman kepada Musa sebelum aku memberi izin kepadamu? Sesungguhnya dia pemimpinmu yang mengajarkan sihir kepadamu. Nanti kamu pasti akan tahu (akibat perbuatanmu). Pasti akan kupotong tangan dan kakimu bersilang dan s

26:50

# قَالُوْا لَا ضَيْرَ ۖاِنَّآ اِلٰى رَبِّنَا مُنْقَلِبُوْنَ ۚ

q*aa*luu l*aa* *dh*ayra inn*aa* il*aa* rabbin*aa* munqalibuun**a**

Mereka berkata, “Tidak ada yang kami takutkan, karena kami akan kembali kepada Tuhan kami.

26:51

# اِنَّا نَطْمَعُ اَنْ يَّغْفِرَ لَنَا رَبُّنَا خَطٰيٰنَآ اَنْ كُنَّآ اَوَّلَ الْمُؤْمِنِيْنَ ۗ ࣖ

inn*aa* na*th*ma'u an yaghfira lan*aa* rabbun*aa* kha*thaa*y*aa*n*aa* an kunn*aa* awwala **a**lmu/miniin**a**

Sesungguhnya kami sangat menginginkan sekiranya Tuhan kami akan mengampuni kesalahan kami, karena kami menjadi orang yang pertama-tama beriman.”

26:52

# ۞ وَاَوْحَيْنَآ اِلٰى مُوْسٰٓى اَنْ اَسْرِ بِعِبَادِيْٓ اِنَّكُمْ مُّتَّبَعُوْنَ

wa-aw*h*ayn*aa* il*aa* muus*aa* an asri bi'ib*aa*dii innakum muttaba'uun**a**

Dan Kami wahyukan (perintahkan) kepada Musa, “Pergilah pada malam hari dengan membawa hamba-hamba-Ku (Bani Israil), sebab pasti kamu akan dikejar.”

26:53

# فَاَرْسَلَ فِرْعَوْنُ فِى الْمَدَاۤىِٕنِ حٰشِرِيْنَ ۚ

fa-arsala fir'awnu fii **a**lmad*aa*-ini *haa*syiriin**a**

Kemudian Fir‘aun mengirimkan orang ke kota-kota (untuk mengumpulkan bala tentaranya).

26:54

# اِنَّ هٰٓؤُلَاۤءِ لَشِرْذِمَةٌ قَلِيْلُوْنَۙ

inna h*aa*ul*aa*-i lasyir*dz*imatun qaliiluun**a**

(Fir‘aun berkata), “Sesungguhnya mereka (Bani Israil) hanya sekelompok kecil,

26:55

# وَاِنَّهُمْ لَنَا لَغَاۤىِٕظُوْنَ ۙ

wa-innahum lan*aa* lagh*aa*-i*zh*uun**a**

dan sesungguhnya mereka telah berbuat hal-hal yang menimbulkan amarah kita,

26:56

# وَاِنَّا لَجَمِيْعٌ حٰذِرُوْنَ ۗ

wa-inn*aa* lajamii'un *hats*iruun**a**

dan sesungguhnya kita semua tanpa kecuali harus selalu waspada.”

26:57

# فَاَخْرَجْنٰهُمْ مِّنْ جَنّٰتٍ وَّعُيُوْنٍ ۙ

fa-akhrajn*aa*hum min jann*aa*tin wa'uyuun**in**

Kemudian, Kami keluarkan mereka (Fir‘aun dan kaumnya) dari taman-taman dan mata air,

26:58

# وَّكُنُوْزٍ وَّمَقَامٍ كَرِيْمٍ ۙ

wakunuuzin wamaq*aa*min kariim**in**

dan (dari) harta kekayaan dan kedudukan yang mulia,

26:59

# كَذٰلِكَۚ وَاَوْرَثْنٰهَا بَنِيْٓ اِسْرَاۤءِيْلَ ۗ

ka*dzaa*lika wa-awratsn*aa*h*aa* banii isr*aa*-iil**a**

demikianlah, dan Kami anugerahkan semuanya (itu) kepada Bani Israil.

26:60

# فَاَتْبَعُوْهُمْ مُّشْرِقِيْنَ

fa-atba'uuhum musyriqiin**a**

Lalu (Fir‘aun dan bala tentaranya) dapat menyusul mereka pada waktu matahari terbit.

26:61

# فَلَمَّا تَرَاۤءَ الْجَمْعٰنِ قَالَ اَصْحٰبُ مُوْسٰٓى اِنَّا لَمُدْرَكُوْنَ ۚ

falamm*aa* tar*aa*-a **a**ljam'*aa*ni q*aa*la a*sh*-*haa*bu muus*aa* inn*aa* lamudrakuun**a**

Maka ketika kedua golongan itu saling melihat, berkatalah pengikut-pengikut Musa, “Kita benar-benar akan tersusul.”

26:62

# قَالَ كَلَّاۗ اِنَّ مَعِيَ رَبِّيْ سَيَهْدِيْنِ

q*aa*la kall*aa* inna ma'iya rabbii sayahdiin**i**

Dia (Musa) menjawab, “Sekali-kali tidak akan (tersusul); sesungguhnya Tuhanku bersamaku, Dia akan memberi petunjuk kepadaku.”

26:63

# فَاَوْحَيْنَآ اِلٰى مُوْسٰٓى اَنِ اضْرِبْ بِّعَصَاكَ الْبَحْرَۗ فَانْفَلَقَ فَكَانَ كُلُّ فِرْقٍ كَالطَّوْدِ الْعَظِيْمِ ۚ

fa-aw*h*ayn*aa* il*aa* muus*aa* ani i*dh*rib bi'a*shaa*ka **a**lba*h*ra fa**i**nfalaqa fak*aa*na kullu firqin ka**al***ththh*awdi **a**l'a*zh*iim

Lalu Kami wahyukan kepada Musa, “Pukullah laut itu dengan tongkatmu.” Maka terbelahlah lautan itu, dan setiap belahan seperti gunung yang besar.

26:64

# وَاَزْلَفْنَا ثَمَّ الْاٰخَرِيْنَ ۚ

wa-azlafn*aa* tsamma **a**l-*aa*khariin**a**

Dan di sanalah Kami dekatkan golongan yang lain.

26:65

# وَاَنْجَيْنَا مُوْسٰى وَمَنْ مَّعَهٗٓ اَجْمَعِيْنَ ۚ

wa-anjayn*aa* muus*aa* waman ma'ahu ajma'iin**a**

Dan Kami selamatkan Musa dan orang-orang yang bersamanya.

26:66

# ثُمَّ اَغْرَقْنَا الْاٰخَرِيْنَ ۗ

tsumma aghraqn*aa* **a**l-*aa*khariin**a**

Kemudian Kami tenggelamkan golongan yang lain.

26:67

# اِنَّ فِيْ ذٰلِكَ لَاٰيَةً ۗوَمَا كَانَ اَكْثَرُهُمْ مُّؤْمِنِيْنَ

inna fii *dzaa*lika la*aa*yatan wam*aa* k*aa*na aktsaruhum mu/miniin**a**

Sungguh, pada yang demikian itu terdapat suatu tanda (kekuasaan Allah), tetapi kebanyakan mereka tidak beriman.

26:68

# وَاِنَّ رَبَّكَ لَهُوَ الْعَزِيْزُ الرَّحِيْمُ ࣖ

wa-inna rabbaka lahuwa **a**l'aziizu **al**rra*h*iim**u**

Dan sesungguhnya Tuhanmu Dialah Yang Mahaperkasa, Maha Penyayang.

26:69

# وَاتْلُ عَلَيْهِمْ نَبَاَ اِبْرٰهِيْمَ ۘ

wa**u**tlu 'alayhim naba-a ibr*aa*hiim**a**

Dan bacakanlah kepada mereka kisah Ibrahim.

26:70

# اِذْ قَالَ لِاَبِيْهِ وَقَوْمِهٖ مَا تَعْبُدُوْنَ

i*dz* q*aa*la li-abiihi waqawmihi m*aa* ta'buduun**a**

Ketika dia (Ibrahim) berkata kepada ayahnya dan kaumnya, “Apakah yang kamu sembah?”

26:71

# قَالُوْا نَعْبُدُ اَصْنَامًا فَنَظَلُّ لَهَا عٰكِفِيْنَ

q*aa*luu na'budu a*sh*n*aa*man fana*zh*allu lah*aa* '*aa*kifiin**a**

Mereka menjawab, “Kami menyembah berhala-berhala dan kami senantiasa tekun menyembahnya.”

26:72

# قَالَ هَلْ يَسْمَعُوْنَكُمْ اِذْ تَدْعُوْنَ ۙ

q*aa*la hal yasma'uunakum i*dz* tad'uun**a**

Dia (Ibrahim) berkata, “Apakah mereka mendengarmu ketika kamu berdoa (kepadanya)?

26:73

# اَوْ يَنْفَعُوْنَكُمْ اَوْ يَضُرُّوْنَ

aw yanfa'uunakum aw ya*dh*urruun**a**

Atau (dapatkah) mereka memberi manfaat atau mencelakakan kamu?”

26:74

# قَالُوْا بَلْ وَجَدْنَآ اٰبَاۤءَنَا كَذٰلِكَ يَفْعَلُوْنَ

q*aa*luu bal wajadn*aa* *aa*b*aa*-an*aa* ka*dzaa*lika yaf'aluun**a**

Mereka menjawab, “Tidak, tetapi kami dapati nenek moyang kami berbuat begitu.”

26:75

# قَالَ اَفَرَءَيْتُمْ مَّا كُنْتُمْ تَعْبُدُوْنَ ۙ

q*aa*la afara-aytum m*aa* kuntum ta'buduun**a**

Dia (Ibrahim) berkata, “Apakah kamu memperhatikan apa yang kamu sembah,

26:76

# اَنْتُمْ وَاٰبَاۤؤُكُمُ الْاَقْدَمُوْنَ ۙ

antum wa*aa*b*aa*ukumu **a**l-aqdamuun**a**

kamu dan nenek moyang kamu yang terdahulu?

26:77

# فَاِنَّهُمْ عَدُوٌّ لِّيْٓ اِلَّا رَبَّ الْعٰلَمِيْنَ ۙ

fa-innahum 'aduwwun lii ill*aa* rabba **a**l'*aa*lamiin**a**

Sesungguhnya mereka (apa yang kamu sembah) itu musuhku, lain halnya Tuhan seluruh alam,

26:78

# الَّذِيْ خَلَقَنِيْ فَهُوَ يَهْدِيْنِ ۙ

**al**la*dz*ii khalaqanii fahuwa yahdiin**i**

(yaitu) Yang telah menciptakan aku, maka Dia yang memberi petunjuk kepadaku,

26:79

# وَالَّذِيْ هُوَ يُطْعِمُنِيْ وَيَسْقِيْنِ ۙ

wa**a**lla*dz*ii huwa yu*th*'imunii wayasqiin**i**

dan Yang memberi makan dan minum kepadaku;

26:80

# وَاِذَا مَرِضْتُ فَهُوَ يَشْفِيْنِ ۙ

wa-i*dzaa* mari*dh*tu fahuwa yasyfiin**i**

dan apabila aku sakit, Dialah yang menyembuhkan aku,

26:81

# وَالَّذِيْ يُمِيْتُنِيْ ثُمَّ يُحْيِيْنِ ۙ

wa**a**lla*dz*ii yumiitunii tsumma yu*h*yiin**i**

dan Yang akan mematikan aku, kemudian akan menghidupkan aku (kembali),

26:82

# وَالَّذِيْٓ اَطْمَعُ اَنْ يَّغْفِرَ لِيْ خَطِيْۤـَٔتِيْ يَوْمَ الدِّيْنِ ۗ

wa**a**lla*dz*ii a*th*ma'u an yaghfira lii kha*th*ii-atii yawma **al**ddiin**i**

dan Yang sangat kuinginkan akan mengampuni kesalahanku pada hari Kiamat.”

26:83

# رَبِّ هَبْ لِيْ حُكْمًا وَّاَلْحِقْنِيْ بِالصّٰلِحِيْنَ ۙ

rabbi hab lii *h*ukman wa-al*h*iqnii bi**al***shshaa*li*h*iin**a**

Ibrahim berdoa), “Ya Tuhanku, berikanlah kepadaku ilmu dan masukkanlah aku ke dalam golongan orang-orang yang saleh,

26:84

# وَاجْعَلْ لِّيْ لِسَانَ صِدْقٍ فِى الْاٰخِرِيْنَ ۙ

wa**i**j'al lii lis*aa*na *sh*idqin fii **a**l-*aa*khiriin**a**

dan jadikanlah aku buah tutur yang baik bagi orang-orang (yang datang) kemudian,

26:85

# وَاجْعَلْنِيْ مِنْ وَّرَثَةِ جَنَّةِ النَّعِيْمِ ۙ

wa**i**j'alnii min waratsati jannati **al**nna'iim**i**

dan jadikanlah aku termasuk orang yang mewarisi surga yang penuh kenikmatan,

26:86

# وَاغْفِرْ لِاَبِيْٓ اِنَّهٗ كَانَ مِنَ الضَّاۤلِّيْنَ ۙ

wa**i**ghfir li-abii innahu k*aa*na mina **al***dhdhaa*lliin**a**

dan ampunilah ayahku, sesungguhnya dia termasuk orang yang sesat,

26:87

# وَلَا تُخْزِنِيْ يَوْمَ يُبْعَثُوْنَۙ

wal*aa* ya*sh*uddunnaka 'an *aa*y*aa*ti **al**l*aa*hi ba'da i*dz* unzilat ilayka wa**u**d'u il*aa* rabbika wal*aa* takuunanna mina **a**lmusyrikiin**a**

dan janganlah Engkau hinakan aku pada hari mereka dibangkitkan,

26:88

# يَوْمَ لَا يَنْفَعُ مَالٌ وَّلَا بَنُوْنَ ۙ

wal*aa* tad'u ma'a **al**l*aa*hi il*aa*han *aa*khara l*aa* il*aa*ha ill*aa* huwa kullu syay-in h*aa*likun ill*aa* wajhahu lahu **a**l*h*ukmu wa-ilayhi turja'uun**a**

(yaitu) pada hari (ketika) harta dan anak-anak tidak berguna,

26:89

# اِلَّا مَنْ اَتَى اللّٰهَ بِقَلْبٍ سَلِيْمٍ ۗ

ill*aa* man at*aa* **al**l*aa*ha biqalbin saliim**in**

kecuali orang-orang yang menghadap Allah dengan hati yang bersih,

26:90

# وَاُزْلِفَتِ الْجَنَّةُ لِلْمُتَّقِيْنَ ۙ

wauzlifati **a**ljannatu lilmuttaqiin**a**

dan surga didekatkan kepada orang-orang yang bertakwa,

26:91

# وَبُرِّزَتِ الْجَحِيْمُ لِلْغٰوِيْنَ ۙ

waburrizati **a**lja*h*iimu lilgh*aa*wiin**a**

dan neraka Jahim diperlihatkan dengan jelas kepada orang-orang yang sesat,”

26:92

# وَقِيْلَ لَهُمْ اَيْنَ مَا كُنْتُمْ تَعْبُدُوْنَ ۙ

waqiila lahum ayna m*aa* kuntum ta'buduun**a**

dan dikatakan kepada mereka, “Di mana berhala-berhala yang dahulu kamu sembah,

26:93

# مِنْ دُوْنِ اللّٰهِ ۗهَلْ يَنْصُرُوْنَكُمْ اَوْ يَنْتَصِرُوْنَ ۗ

min duuni **al**l*aa*hi hal yan*sh*uruunakum aw yanta*sh*iruun**a**

selain Allah? Dapatkah mereka menolong kamu atau menolong diri mereka sendiri?”

26:94

# فَكُبْكِبُوْا فِيْهَا هُمْ وَالْغَاوٗنَ ۙ

fakubkibuu fiih*aa* hum wa**a**lgh*aa*wuun**a**

Maka mereka (sesembahan itu) dijungkirkan ke dalam neraka bersama orang-orang yang sesat,

26:95

# وَجُنُوْدُ اِبْلِيْسَ اَجْمَعُوْنَ ۗ

wajunuudu ibliisa ajma'uun**a**

dan bala tentara Iblis semuanya.

26:96

# قَالُوْا وَهُمْ فِيْهَا يَخْتَصِمُوْنَ

q*aa*luu wahum fiih*aa* yakhta*sh*imuun**a**

Mereka berkata sambil bertengkar di dalamnya (neraka),

26:97

# تَاللّٰهِ اِنْ كُنَّا لَفِيْ ضَلٰلٍ مُّبِيْنٍ ۙ

ta**al**l*aa*hi in kunn*aa* lafii *dh*al*aa*lin mubiin**in**

”Demi Allah, sesungguhnya kita dahulu (di dunia) dalam kesesatan yang nyata,

26:98

# اِذْ نُسَوِّيْكُمْ بِرَبِّ الْعٰلَمِيْنَ

i*dz* nusawwiikum birabbi **a**l'*aa*lamiin**a**

karena kita mempersamakan kamu (berhala-berhala) dengan Tuhan seluruh alam.

26:99

# وَمَآ اَضَلَّنَآ اِلَّا الْمُجْرِمُوْنَ

wam*aa* a*dh*allan*aa* ill*aa* **a**lmujrimuun**a**

Dan tidak ada yang menyesatkan kita kecuali orang-orang yang berdosa.

26:100

# فَمَا لَنَا مِنْ شَافِعِيْنَ ۙ

fam*aa* lan*aa* min sy*aa*fi'iin**a**

Maka sehingga (sekarang) kita tidak mempunyai pemberi syafaat (penolong),

26:101

# وَلَا صَدِيْقٍ حَمِيْمٍ

wal*aa* *sh*adiiqin *h*amiim**in**

dan tidak pula mempunyai teman yang akrab,

26:102

# فَلَوْ اَنَّ لَنَا كَرَّةً فَنَكُوْنَ مِنَ الْمُؤْمِنِيْنَ

falaw anna lan*aa* karratan fanakuuna mina **a**lmu/miniin**a**

Maka seandainya kita dapat kembali (ke dunia) niscaya kita menjadi orang-orang yang beriman.”

26:103

# اِنَّ فِيْ ذٰلِكَ لَاٰيَةً ۗوَمَا كَانَ اَكْثَرُهُمْ مُّؤْمِنِيْنَ

inna fii *dzaa*lika la*aa*yatan wam*aa* k*aa*na aktsaruhum mu/miniin**a**

Sungguh, pada yang demikian itu terdapat tanda (kekuasaan Allah), tetapi kebanyakan mereka tidak beriman.

26:104

# وَاِنَّ رَبَّكَ لَهُوَ الْعَزِيْزُ الرَّحِيْمُ ࣖ

wa-inna rabbaka lahuwa **a**l'aziizu **al**rra*h*iim**u**

Dan sungguh, Tuhanmu benar-benar Dialah Mahaperkasa, Maha Penyayang.

26:105

# كَذَّبَتْ قَوْمُ نُوْحِ ِۨالْمُرْسَلِيْنَ ۚ

ka*dzdz*abat qawmu nuu*h*in **a**lmursaliin**a**

Kaum Nuh telah mendustakan para rasul.

26:106

# اِذْ قَالَ لَهُمْ اَخُوْهُمْ نُوْحٌ اَلَا تَتَّقُوْنَ ۚ

i*dz* q*aa*la lahum akhuuhum nuu*h*un **a**l*aa* tattaquun**a**

Ketika saudara mereka (Nuh) berkata kepada mereka, “Mengapa kamu tidak bertakwa?

26:107

# اِنِّيْ لَكُمْ رَسُوْلٌ اَمِيْنٌ ۙ

innii lakum rasuulun amiin**un**

Sesungguhnya aku ini seorang rasul kepercayaan (yang diutus) kepadamu,

26:108

# فَاتَّقُوا اللّٰهَ وَاَطِيْعُوْنِۚ

fa**i**ttaquu **al**l*aa*ha wa-a*th*ii'uun**i**

Maka bertakwalah kamu kepada Allah dan taatlah kepadaku.

26:109

# وَمَآ اَسْـَٔلُكُمْ عَلَيْهِ مِنْ اَجْرٍۚ اِنْ اَجْرِيَ اِلَّا عَلٰى رَبِّ الْعٰلَمِيْنَ ۚ

wam*aa* as-alukum 'alayhi min ajrin in ajriya ill*aa* 'al*aa* rabbi **a**l'*aa*lamiin**a**

Dan aku tidak meminta imbalan kepadamu atas ajakan itu; imbalanku hanyalah dari Tuhan seluruh alam.

26:110

# فَاتَّقُوا اللّٰهَ وَاَطِيْعُوْنِ

fa**i**ttaquu **al**l*aa*ha wa-a*th*ii'uun**i**

Maka bertakwalah kamu kepada Allah dan taatlah kepadaku.”

26:111

# ۞ قَالُوْٓا اَنُؤْمِنُ لَكَ وَاتَّبَعَكَ الْاَرْذَلُوْنَ ۗ

q*aa*luu anu/minu laka wa**i**ttaba'aka **a**l-ar*dz*aluun**a**

Mereka berkata, “Apakah kami harus beriman kepadamu, padahal pengikut-pengikutmu orang-orang yang hina?”

26:112

# قَالَ وَمَا عِلْمِيْ بِمَا كَانُوْا يَعْمَلُوْنَ ۚ

q*aa*la wam*aa* 'ilmii bim*aa* k*aa*nuu ya'maluun**a**

Dia (Nuh) menjawab, “Tidak ada pengetahuanku tentang apa yang mereka kerjakan.

26:113

# اِنْ حِسَابُهُمْ اِلَّا عَلٰى رَبِّيْ لَوْ تَشْعُرُوْنَ ۚ

in *h*is*aa*buhum ill*aa* 'al*aa* rabbii law tasy'uruun**a**

Perhitungan (amal perbuatan) mereka tidak lain hanyalah kepada Tuhanku, jika kamu menyadari.

26:114

# وَمَآ اَنَا۠ بِطَارِدِ الْمُؤْمِنِيْنَ ۚ

wam*aa* an*aa* bi*thaa*ridi **a**lmu/miniin**a**

Dan aku tidak akan mengusir orang-orang yang beriman.

26:115

# اِنْ اَنَا۠ اِلَّا نَذِيْرٌ مُّبِيْنٌ ۗ

in an*aa* ill*aa* na*dz*iirun mubiin**un**

Aku (ini) hanyalah pemberi peringatan yang jelas.”

26:116

# قَالُوْا لَىِٕنْ لَّمْ تَنْتَهِ يٰنُوْحُ لَتَكُوْنَنَّ مِنَ الْمَرْجُوْمِيْنَۗ

q*aa*luu la-in lam tantahi y*aa* nuu*h*u latakuunanna mina **a**lmarjuumiin**a**

Mereka berkata, “Wahai Nuh! Sungguh, jika engkau tidak (mau) berhenti, niscaya engkau termasuk orang yang dirajam (dilempari batu sampai mati).”

26:117

# قَالَ رَبِّ اِنَّ قَوْمِيْ كَذَّبُوْنِۖ

q*aa*la rabbi inna qawmii ka*dzdz*abuun**i**

Dia (Nuh) berkata, “Ya Tuhanku, sungguh kaumku telah mendustakan aku;

26:118

# فَافْتَحْ بَيْنِيْ وَبَيْنَهُمْ فَتْحًا وَّنَجِّنِيْ وَمَنْ مَّعِيَ مِنَ الْمُؤْمِنِيْنَ

fa**i**fta*h* baynii wabaynahum fat*h*an wanajjinii waman ma'iya mina **a**lmu/miniin**a**

maka berilah keputusan antara aku dengan mereka, dan selamatkanlah aku dan mereka yang beriman bersamaku.”

26:119

# فَاَنْجَيْنٰهُ وَمَنْ مَّعَهٗ فِى الْفُلْكِ الْمَشْحُوْنِ

fa-anjayn*aa*hu waman ma'ahu fii **a**lfulki **a**lmasyhuun**i**

Kemudian Kami menyelamatkannya Nuh dan orang-orang yang bersamanya di dalam kapal yang penuh muatan.

26:120

# ثُمَّ اَغْرَقْنَا بَعْدُ الْبٰقِيْنَ

tsumma aghraqn*aa* ba'du **a**lb*aa*qiin**a**

Kemudian setelah itu Kami tenggelamkan orang-orang yang tinggal.

26:121

# اِنَّ فِيْ ذٰلِكَ لَاٰيَةً ۗوَمَا كَانَ اَكْثَرُهُمْ مُّؤْمِنِيْنَ

inna fii *dzaa*lika la*aa*yatan wam*aa* k*aa*na aktsaruhum mu/miniin**a**

Sungguh, pada yang demikian itu benar-benar terdapat tanda (kekuasaan Allah), tetapi kebanyakan mereka tidak beriman.

26:122

# وَاِنَّ رَبَّكَ لَهُوَ الْعَزِيْزُ الرَّحِيْمُ ࣖ

wa-inna rabbaka lahuwa **a**l'aziizu **al**rra*h*iim**u**

Dan sungguh, Tuhanmu, Dialah Yang Mahaperkasa, Maha Penyayang.

26:123

# كَذَّبَتْ عَادُ ِۨالْمُرْسَلِيْنَ ۖ

ka*dzdz*abat '*aa*dun **a**lmursaliin**a**

(Kaum) ‘Ad telah mendustakan para rasul.

26:124

# اِذْ قَالَ لَهُمْ اَخُوْهُمْ هُوْدٌ اَلَا تَتَّقُوْنَ ۚ

i*dz* q*aa*la lahum akhuuhum huudun **a**l*aa* tattaquun**a**

Ketika saudara mereka Hud berkata kepada mereka, “Mengapa kamu tidak bertakwa?

26:125

# اِنِّيْ لَكُمْ رَسُوْلٌ اَمِيْنٌ ۙ

innii lakum rasuulun amiin**un**

Sungguh, aku ini seorang rasul kepercayaan (yang diutus) kepadamu,

26:126

# فَاتَّقُوا اللّٰهَ وَاَطِيْعُوْنِ ۚ

fa**i**ttaquu **al**l*aa*ha wa-a*th*ii'uun**i**

karena itu bertakwalah kepada Allah dan taatlah kepadaku.

26:127

# وَمَآ اَسْـَٔلُكُمْ عَلَيْهِ مِنْ اَجْرٍۚ اِنْ اَجْرِيَ اِلَّا عَلٰى رَبِّ الْعٰلَمِيْنَ ۗ

wam*aa* as-alukum 'alayhi min ajrin in ajriya ill*aa* 'al*aa* rabbi **a**l'*aa*lamiin**a**

Dan aku tidak meminta imbalan kepadamu atas ajakan itu; imbalanku hanyalah dari Tuhan seluruh alam.

26:128

# اَتَبْنُوْنَ بِكُلِّ رِيْعٍ اٰيَةً تَعْبَثُوْنَ ۙ

atabnuuna bikulli rii'in *aa*yatan ta'batsuun**a**

Apakah kamu mendirikan istana-istana pada setiap tanah yang tinggi untuk kemegahan tanpa ditempati,

26:129

# وَتَتَّخِذُوْنَ مَصَانِعَ لَعَلَّكُمْ تَخْلُدُوْنَۚ

watattakhi*dz*uuna ma*shaa*ni'a la'allakum takhluduun**a**

dan kamu membuat benteng-benteng dengan harapan kamu hidup kekal?

26:130

# وَاِذَا بَطَشْتُمْ بَطَشْتُمْ جَبَّارِيْنَۚ

wa-i*dzaa* ba*th*asytum ba*th*asytum jabb*aa*riin**a**

Dan apabila kamu menyiksa, maka kamu lakukan secara kejam dan bengis.

26:131

# فَاتَّقُوا اللّٰهَ وَاَطِيْعُوْنِۚ

fa**i**ttaquu **al**l*aa*ha wa-a*th*ii'uun**i**

Maka bertakwalah kepada Allah dan taatlah kepadaku,

26:132

# وَاتَّقُوا الَّذِيْٓ اَمَدَّكُمْ بِمَا تَعْلَمُوْنَ ۚ

wa**i**ttaquu **al**la*dz*ii amaddakum bim*aa* ta'lamuun**a**

dan tetaplah kamu bertakwa kepada-Nya yang telah menganugerahkan kepadamu apa yang kamu ketahui.

26:133

# اَمَدَّكُمْ بِاَنْعَامٍ وَّبَنِيْنَۙ

amaddakum bi-an'*aa*min wabaniin**a**

Dia (Allah) telah menganugerahkan kepadamu hewan ternak dan anak-anak,

26:134

# وَجَنّٰتٍ وَّعُيُوْنٍۚ

wajann*aa*tin wa'uyuun**in**

dan kebun-kebun, dan mata air,

26:135

# اِنِّيْٓ اَخَافُ عَلَيْكُمْ عَذَابَ يَوْمٍ عَظِيْمٍ ۗ

innii akh*aa*fu 'alaykum 'a*dzaa*ba yawmin 'a*zh*iim**in**

sesungguhnya aku takut kamu akan ditimpa azab pada hari yang besar.”

26:136

# قَالُوْا سَوَاۤءٌ عَلَيْنَآ اَوَعَظْتَ اَمْ لَمْ تَكُنْ مِّنَ الْوَاعِظِيْنَ ۙ

q*aa*luu saw*aa*un 'alayn*aa* awa'a*zh*ta am lam takun mina **a**lw*aa*'i*zh*iin**a**

Mereka menjawab, “Sama saja bagi kami, apakah engkau memberi nasihat atau tidak memberi nasihat,

26:137

# اِنْ هٰذَآ اِلَّا خُلُقُ الْاَوَّلِيْنَ ۙ

in h*aadzaa* ill*aa* khuluqu **a**l-awwaliin**a**

(agama kami) ini tidak lain hanyalah adat kebiasaan orang-orang terdahulu,

26:138

# وَمَا نَحْنُ بِمُعَذَّبِيْنَ ۚ

wam*aa* na*h*nu bimu'a*dzdz*abiin**a**

dan kami (sama sekali) tidak akan diazab.”

26:139

# فَكَذَّبُوْهُ فَاَهْلَكْنٰهُمْۗ اِنَّ فِيْ ذٰلِكَ لَاٰيَةً ۗوَمَا كَانَ اَكْثَرُهُمْ مُّؤْمِنِيْنَ

faka*dzdz*abuuhu fa-ahlakn*aa*hum inna fii *dzaa*lika la*aa*yatan wam*aa* k*aa*na aktsaruhum mu/miniin**a**

Maka mereka mendustakannya (Hud), lalu Kami binasakan mereka. Sungguh, pada yang demikian itu terdapat tanda (kekuasaan Allah), tetapi kebanyakan mereka tidak beriman.

26:140

# وَاِنَّ رَبَّكَ لَهُوَ الْعَزِيْزُ الرَّحِيْمُ ࣖ

wa-inna rabbaka lahuwa **a**l'aziizu **al**rra*h*iim**u**

Dan sungguh, Tuhanmu, Dialah Yang Mahaperkasa, Maha Penyayang.

26:141

# كَذَّبَتْ ثَمُوْدُ الْمُرْسَلِيْنَ ۖ

ka*dzdz*abat tsamuudu **a**lmursaliin**a**

Kaum Samud telah mendustakan para rasul.

26:142

# اِذْ قَالَ لَهُمْ اَخُوْهُمْ صٰلِحٌ اَلَا تَتَّقُوْنَ ۚ

i*dz* q*aa*la lahum akhuuhum *shaa*li*h*un **a**l*aa* tattaquun**a**

Ketika saudara mereka Saleh berkata kepada mereka, “Mengapa kamu tidak bertakwa?

26:143

# اِنِّيْ لَكُمْ رَسُوْلٌ اَمِيْنٌ ۙ

innii lakum rasuulun amiin**un**

Sungguh, aku ini seorang rasul kepercayaan (yang diutus) kepadamu,

26:144

# فَاتَّقُوا اللّٰهَ وَاَطِيْعُوْنِ ۚ

fa**i**ttaquu **al**l*aa*ha wa-a*th*ii'uun**i**

karena itu bertakwalah kepada Allah dan taatlah kepadaku.

26:145

# وَمَآ اَسْـَٔلُكُمْ عَلَيْهِ مِنْ اَجْرٍۚ اِنْ اَجْرِيَ اِلَّا عَلٰى رَبِّ الْعٰلَمِيْنَ ۗ

wam*aa* as-alukum 'alayhi min ajrin in ajriya ill*aa* 'al*aa* rabbi **a**l'*aa*lamiin**a**

Dan aku tidak meminta sesuatu imbalan kepadamu atas ajakan itu, imbalanku hanyalah dari Tuhan seluruh alam.

26:146

# اَتُتْرَكُوْنَ فِيْ مَا هٰهُنَآ اٰمِنِيْنَ ۙ

atutrakuuna fii m*aa* h*aa*hun*aa* *aa*miniin**a**

Apakah kamu (mengira) akan dibiarkan tinggal di sini (di negeri kamu ini) dengan aman,

26:147

# فِيْ جَنّٰتٍ وَّعُيُوْنٍ ۙ

fii jann*aa*tin wa'uyuun**in**

di dalam kebun-kebun dan mata air,

26:148

# وَّزُرُوْعٍ وَّنَخْلٍ طَلْعُهَا هَضِيْمٌ ۚ

wazuruu'in wanakhlin *th*al'uh*aa* ha*dh*iim**un**

dan tanaman-tanaman dan pohon-pohon kurma yang mayangnya lembut.

26:149

# وَتَنْحِتُوْنَ مِنَ الْجِبَالِ بُيُوْتًا فٰرِهِيْنَ

watan*h*ituuna mina **a**ljib*aa*li buyuutan f*aa*rihiin**a**

Dan kamu pahat dengan terampil sebagian gunung-gunung untuk dijadikan rumah-rumah;

26:150

# فَاتَّقُوا اللّٰهَ وَاَطِيْعُوْنِ ۚ

fa**i**ttaquu **al**l*aa*ha wa-a*th*ii'uun**i**

maka bertakwalah kepada Allah dan taatlah kepadaku;

26:151

# وَلَا تُطِيْعُوْٓا اَمْرَ الْمُسْرِفِيْنَ ۙ

wal*aa* tu*th*ii'uu amra **a**lmusrifiin**a**

dan janganlah kamu menaati perintah orang-orang yang melampaui batas,

26:152

# الَّذِيْنَ يُفْسِدُوْنَ فِى الْاَرْضِ وَلَا يُصْلِحُوْنَ

**al**la*dz*iina yufsiduuna fii **a**l-ar*dh*i wal*aa* yu*sh*li*h*uun**a**

yang berbuat kerusakan di bumi dan tidak mengadakan perbaikan.”

26:153

# قَالُوْٓا اِنَّمَآ اَنْتَ مِنَ الْمُسَحَّرِيْنَ ۙ

q*aa*luu innam*aa* anta mina **a**lmusa*hh*ariin**a**

Mereka berkata, “Sungguh, engkau hanyalah termasuk orang yang kena sihir;

26:154

# مَآ اَنْتَ اِلَّا بَشَرٌ مِّثْلُنَاۙ فَأْتِ بِاٰيَةٍ اِنْ كُنْتَ مِنَ الصّٰدِقِيْنَ

m*aa* anta ill*aa* basyarun mitslun*aa* fa/ti bi-*aa*yatin in kunta mina **al***shshaa*diqiinaa

engkau hanyalah manusia seperti kami; maka datangkanlah sesuatu mukjizat jika engkau termasuk orang yang benar.”

26:155

# قَالَ هٰذِهٖ نَاقَةٌ لَّهَا شِرْبٌ وَّلَكُمْ شِرْبُ يَوْمٍ مَّعْلُوْمٍ ۚ

q*aa*la h*aadz*ihi n*aa*qatun lah*aa* syirbun walakum syirbu yawmin ma'luum**in**

Dia (Saleh) menjawab, “Ini seekor unta betina, yang berhak mendapatkan (giliran) minum, dan kamu juga berhak mendapatkan minum pada hari yang ditentukan.

26:156

# وَلَا تَمَسُّوْهَا بِسُوْۤءٍ فَيَأْخُذَكُمْ عَذَابُ يَوْمٍ عَظِيْمٍ

wal*aa* tamassuuh*aa* bisuu-in faya/khu*dz*akum 'a*dzaa*bu yawmin 'a*zh*iim**in**

Dan jangan kamu menyentuhnya (unta itu) dengan sesuatu kejahatan, nanti kamu akan ditimpa azab pada hari yang dahsyat.”

26:157

# فَعَقَرُوْهَا فَاَصْبَحُوْا نٰدِمِيْنَ ۙ

fa'aqaruuh*aa* fa-a*sh*ba*h*uu n*aa*dimiin**a**

Kemudian mereka membunuhnya, lalu mereka merasa menyesal,

26:158

# فَاَخَذَهُمُ الْعَذَابُۗ اِنَّ فِيْ ذٰلِكَ لَاٰيَةً ۗوَمَا كَانَ اَكْثَرُهُمْ مُّؤْمِنِيْنَ

fa-akha*dz*ahumu **a**l'a*dzaa*bu inna fii *dzaa*lika la*aa*yatan wam*aa* k*aa*na aktsaruhum mu/miniin**a**

maka mereka ditimpa azab. Sungguh, pada yang demikian itu terdapat tanda (kekuasaan Allah), tetapi kebanyakan mereka tidak beriman.

26:159

# وَاِنَّ رَبَّكَ لَهُوَ الْعَزِيْزُ الرَّحِيْمُ ࣖ

wa-inna rabbaka lahuwa **a**l'aziizu **al**rra*h*iim**u**

Dan sungguh, Tuhanmu, Dialah Yang Mahaperkasa, Maha Penyayang.

26:160

# كَذَّبَتْ قَوْمُ لُوْطِ ِۨالْمُرْسَلِيْنَ ۖ

ka*dzdz*abat qawmu luu*th*in **a**lmursaliin**a**

Kaum Lut telah mendustakan para rasul,

26:161

# اِذْ قَالَ لَهُمْ اَخُوْهُمْ لُوْطٌ اَلَا تَتَّقُوْنَ ۚ

i*dz* q*aa*la lahum akhuuhum luu*th*un **a**l*aa* tattaquun**a**

ketika saudara mereka Lut berkata kepada mereka, “Mengapa kamu tidak bertakwa?”

26:162

# اِنِّيْ لَكُمْ رَسُوْلٌ اَمِيْنٌ ۙ

innii lakum rasuulun amiin**un**

Sungguh, aku ini seorang rasul kepercayaan (yang diutus) kepadamu,

26:163

# فَاتَّقُوا اللّٰهَ وَاَطِيْعُوْنِ ۚ

fa**i**ttaquu **al**l*aa*ha wa-a*th*ii'uun**i**

maka bertakwalah kepada Allah dan taatlah kepadaku.

26:164

# وَمَآ اَسْـَٔلُكُمْ عَلَيْهِ مِنْ اَجْرٍ اِنْ اَجْرِيَ اِلَّا عَلٰى رَبِّ الْعٰلَمِيْنَ ۗ

wam*aa* as-alukum 'alayhi min ajrin in ajriya ill*aa* 'al*aa* rabbi **a**l'*aa*lamiin**a**

Dan aku tidak meminta imbalan kepadamu atas ajakan itu; imbalanku hanyalah dari Tuhan seluruh alam.

26:165

# اَتَأْتُوْنَ الذُّكْرَانَ مِنَ الْعٰلَمِيْنَ ۙ

ata/tuuna **al***dzdz*ukr*aa*na mina **a**l'*aa*lamiin**a**

Mengapa kamu mendatangi jenis laki-laki di antara manusia (berbuat homoseks),

26:166

# وَتَذَرُوْنَ مَا خَلَقَ لَكُمْ رَبُّكُمْ مِّنْ اَزْوَاجِكُمْۗ بَلْ اَنْتُمْ قَوْمٌ عٰدُوْنَ

wata*dz*aruuna m*aa* khalaqa lakum rabbukum min azw*aa*jikum bal antum qawmun '*aa*duun**a**

dan kamu tinggalkan (perempuan) yang diciptakan Tuhan untuk menjadi istri-istri kamu? Kamu (memang) orang-orang yang melampaui batas.”

26:167

# قَالُوْا لَىِٕنْ لَّمْ تَنْتَهِ يٰلُوْطُ لَتَكُوْنَنَّ مِنَ الْمُخْرَجِيْنَ

q*aa*luu la-in lam tantahi y*aa* luu*th*u latakuunanna mina **a**lmukhrajiin**a**

Mereka menjawab, “Wahai Lut! Jika engkau tidak berhenti, engkau termasuk orang-orang yang terusir.”

26:168

# قَالَ ِانِّيْ لِعَمَلِكُمْ مِّنَ الْقَالِيْنَ ۗ

q*aa*la innii li'amalikum mina **a**lq*aa*liin**a**

Dia (Lut) berkata, “Aku sungguh benci kepada perbuatanmu.”

26:169

# رَبِّ نَجِّنِيْ وَاَهْلِيْ مِمَّا يَعْمَلُوْنَ

rabbi najjinii wa-ahlii mimm*aa* ya'maluun**a**

(Lut berdoa), “Ya Tuhanku, selamatkanlah aku dan keluargaku dari (akibat) perbuatan yang mereka kerjakan.”

26:170

# فَنَجَّيْنٰهُ وَاَهْلَهٗٓ اَجْمَعِيْنَ ۙ

fanajjayn*aa*hu wa-ahlahu ajma'iin**a**

Lalu Kami selamatkan dia bersama keluarganya semua,

26:171

# اِلَّا عَجُوْزًا فِى الْغٰبِرِيْنَ ۚ

ill*aa* 'ajuuzan fii **a**lgh*aa*biriin**a**

kecuali seorang perempuan tua (istrinya), yang termasuk dalam golongan yang tinggal.

26:172

# ثُمَّ دَمَّرْنَا الْاٰخَرِيْنَ ۚ

tsumma dammarn*aa* **a**l-*aa*khariin**a**

Kemudian Kami binasakan yang lain.

26:173

# وَاَمْطَرْنَا عَلَيْهِمْ مَّطَرًاۚ فَسَاۤءَ مَطَرُ الْمُنْذَرِيْنَ

wa-am*th*arn*aa* 'alayhim ma*th*aran fas*aa*-a ma*th*aru **a**lmun*dz*ariin**a**

Dan Kami hujani mereka (dengan hujan batu), maka betapa buruk hujan yang menimpa orang-orang yang telah diberi peringatan itu.

26:174

# اِنَّ فِيْ ذٰلِكَ لَاٰيَةً ۗوَمَا كَانَ اَكْثَرُهُمْ مُّؤْمِنِيْنَ

inna fii *dzaa*lika la*aa*yatan wam*aa* k*aa*na aktsaruhum mu/miniin**a**

Sungguh, pada yang demikian itu terdapat tanda (kekuasaan Allah), tetapi kebanyakan mereka tidak beriman.

26:175

# وَاِنَّ رَبَّكَ لَهُوَ الْعَزِيْزُ الرَّحِيْمُ ࣖ

wa-inna rabbaka lahuwa **a**l'aziizu **al**rra*h*iim**u**

Dan sungguh, Tuhanmu, Dialah Yang Mahaperkasa, Maha Penyayang.

26:176

# كَذَّبَ اَصْحٰبُ لْـَٔيْكَةِ الْمُرْسَلِيْنَ ۖ

ka*dzdz*aba a*sh*-*haa*bu **a**l-aykati **a**lmursaliin**a**

Penduduk Aikah telah mendustakan para rasul;

26:177

# اِذْ قَالَ لَهُمْ شُعَيْبٌ اَلَا تَتَّقُوْنَ ۚ

i*dz* q*aa*la lahum syu'aybun **a**l*aa* tattaquun**a**

ketika Syuaib berkata kepada mereka, “Mengapa kamu tidak bertakwa?

26:178

# اِنِّيْ لَكُمْ رَسُوْلٌ اَمِيْنٌ ۙ

innii lakum rasuulun amiin**un**

Sungguh, aku adalah rasul kepercayaan (yang diutus) kepadamu,

26:179

# فَاتَّقُوا اللّٰهَ وَاَطِيْعُوْنِ ۚ

fa**i**ttaquu **al**l*aa*ha wa-a*th*ii'uun**i**

maka bertakwalah kepada Allah dan taatlah kepadaku;

26:180

# وَمَآ اَسْـَٔلُكُمْ عَلَيْهِ مِنْ اَجْرٍ اِنْ اَجْرِيَ اِلَّا عَلٰى رَبِّ الْعٰلَمِيْنَ ۗ

wam*aa* as-alukum 'alayhi min ajrin in ajriya ill*aa* 'al*aa* rabbi **a**l'*aa*lamiin**a**

Dan aku tidak meminta imbalan kepadamu atas ajakan itu; imbalanku hanyalah dari Tuhan seluruh alam.

26:181

# ۞ اَوْفُوا الْكَيْلَ وَلَا تَكُوْنُوْا مِنَ الْمُخْسِرِيْنَ ۚ

awfuu **a**lkayla wal*aa* takuunuu mina **a**lmukhsiriin**a**

Sempurnakanlah takaran dan janganlah kamu merugikan orang lain.

26:182

# وَزِنُوْا بِالْقِسْطَاسِ الْمُسْتَقِيْمِ ۚ

wazinuu bi**a**lqis*thaa*si **a**lmustaqiim**i**

Dan timbanglah dengan timbangan yang benar.

26:183

# وَلَا تَبْخَسُوا النَّاسَ اَشْيَاۤءَهُمْ وَلَا تَعْثَوْا فِى الْاَرْضِ مُفْسِدِيْنَ ۚ

wal*aa* tabkhasuu **al**nn*aa*sa asyy*aa*-ahum wal*aa* ta'tsaw fii **a**l-ar*dh*i mufsidiin**a**

Dan janganlah kamu merugikan manusia dengan mengurangi hak-haknya dan janganlah membuat kerusakan di bumi;

26:184

# وَاتَّقُوا الَّذِيْ خَلَقَكُمْ وَالْجِبِلَّةَ الْاَوَّلِيْنَ ۗ

wa**i**ttaquu **al**la*dz*ii khalaqakum wa**a**ljibillata **a**l-awwaliin**a**

dan bertakwalah kepada Allah yang telah menciptakan kamu dan umat-umat yang terdahulu.”

26:185

# قَالُوْٓا اِنَّمَآ اَنْتَ مِنَ الْمُسَحَّرِيْنَ ۙ

q*aa*luu innam*aa* anta mina **a**lmusa*hh*ariin**a**

Mereka berkata, “Engkau tidak lain hanyalah orang-orang yang kena sihir.

26:186

# وَمَآ اَنْتَ اِلَّا بَشَرٌ مِّثْلُنَا وَاِنْ نَّظُنُّكَ لَمِنَ الْكٰذِبِيْنَ ۚ

wam*aa* anta ill*aa* basyarun mitslun*aa* wa-in na*zh*unnuka lamina **a**lk*aadz*ibiin**a**

Dan engkau hanyalah manusia seperti kami, dan sesungguhnya kami yakin engkau termasuk orang-orang yang berdusta.

26:187

# فَاَسْقِطْ عَلَيْنَا كِسَفًا مِّنَ السَّمَاۤءِ اِنْ كُنْتَ مِنَ الصّٰدِقِيْنَ ۗ

fa-asqi*th* 'alayn*aa* kisafan mina **al**ssam*aa*-i in kunta mina **al***shshaa*diqiin**a**

Maka jatuhkanlah kepada kami gumpalan dari langit, jika engkau termasuk orang-orang yang benar.”

26:188

# قَالَ رَبِّيْٓ اَعْلَمُ بِمَا تَعْمَلُوْنَ

q*aa*la rabbii a'lamu bim*aa* ta'maluun**a**

Dia (Syuaib) berkata, “Tuhanku lebih mengetahui apa yang kamu kerjakan.”

26:189

# فَكَذَّبُوْهُ فَاَخَذَهُمْ عَذَابُ يَوْمِ الظُّلَّةِ ۗاِنَّهٗ كَانَ عَذَابَ يَوْمٍ عَظِيْمٍ

faka*dzdz*abuuhu fa-akha*dz*ahum 'a*dzaa*bu yawmi **al***zhzh*ullati innahu k*aa*na 'a*dzaa*ba yawmin 'a*zh*iim**in**

Kemudian mereka mendustakannya (Syuaib), lalu mereka ditimpa azab pada hari yang gelap. Sungguh, itulah azab pada hari yang dahsyat.

26:190

# اِنَّ فِيْ ذٰلِكَ لَاٰيَةً ۗوَمَا كَانَ اَكْثَرُهُمْ مُّؤْمِنِيْنَ

inna fii *dzaa*lika la*aa*yatan wam*aa* k*aa*na aktsaruhum mu/miniin**a**

Sungguh, pada yang demikian itu benar-benar terdapat tanda (kekuasaan Allah), tetapi kebanyakan mereka tidak beriman.

26:191

# وَاِنَّ رَبَّكَ لَهُوَ الْعَزِيْزُ الرَّحِيْمُ ࣖ

wa-inna rabbaka lahuwa **a**l'aziizu **al**rra*h*iim**u**

Dan sungguh, Tuhanmu, Dialah yang Mahaperkasa, Maha Penyayang.

26:192

# وَاِنَّهٗ لَتَنْزِيْلُ رَبِّ الْعٰلَمِيْنَ ۗ

wa-innahu latanziilu rabbi **a**l'*aa*lamiin**a**

Dan sungguh, (Al-Qur'an) ini benar-benar diturunkan oleh Tuhan seluruh alam,

26:193

# نَزَلَ بِهِ الرُّوْحُ الْاَمِيْنُ ۙ

nazala bihi **al**rruu*h*u **a**l-amiin**u**

Yang dibawa turun oleh ar-Ruh al-Amin (Jibril),

26:194

# عَلٰى قَلْبِكَ لِتَكُوْنَ مِنَ الْمُنْذِرِيْنَ ۙ

'al*aa* qalbika litakuuna mina **a**lmun*dz*iriin**a**

ke dalam hatimu (Muhammad) agar engkau termasuk orang yang memberi peringatan,

26:195

# بِلِسَانٍ عَرَبِيٍّ مُّبِيْنٍ ۗ

bilis*aa*nin 'arabiyyin mubiin**in**

dengan bahasa Arab yang jelas.

26:196

# وَاِنَّهٗ لَفِيْ زُبُرِ الْاَوَّلِيْنَ

wa-innahu lafii zuburi **a**l-awwaliin**a**

Dan sungguh, (Al-Qur'an) itu (disebut) dalam kitab-kitab orang yang terdahulu.

26:197

# اَوَلَمْ يَكُنْ لَّهُمْ اٰيَةً اَنْ يَّعْلَمَهٗ عُلَمٰۤؤُا بَنِيْٓ اِسْرَاۤءِيْلَ

awa lam yakun lahum *aa*yatan an ya'lamahu 'ulam*aa*u banii isr*aa*-iil**a**

Apakah tidak (cukup) menjadi bukti bagi mereka, bahwa para ulama Bani Israil mengetahuinya?

26:198

# وَلَوْ نَزَّلْنٰهُ عَلٰى بَعْضِ الْاَعْجَمِيْنَ ۙ

walaw nazzaln*aa*hu 'al*aa* ba'*dh*i **a**l-a'jamiin**a**

Dan seandainya (Al-Qur'an) itu Kami turunkan kepada sebagian dari golongan bukan Arab,

26:199

# فَقَرَاَهٗ عَلَيْهِمْ مَّا كَانُوْا بِهٖ مُؤْمِنِيْنَ ۗ

faqara-ahu 'alayhim m*aa* k*aa*nuu bihi mu/miniin**a**

lalu dia membacakannya kepada mereka (orang-orang kafir); niscaya mereka tidak juga akan beriman kepadanya.

26:200

# كَذٰلِكَ سَلَكْنٰهُ فِيْ قُلُوْبِ الْمُجْرِمِيْنَ ۗ

ka*dzaa*lika salakn*aa*hu fii quluubi **a**lmujrimiin**a**

Demikianlah, Kami masukkan (sifat dusta dan ingkar) ke dalam hati orang-orang yang berdosa,

26:201

# لَا يُؤْمِنُوْنَ بِهٖ حَتّٰى يَرَوُا الْعَذَابَ الْاَلِيْمَ

l*aa* yu/minuuna bihi *h*att*aa* yarawuu **a**l'a*dzaa*ba **a**l-aliim**a**

mereka tidak akan beriman kepadanya, hingga mereka melihat azab yang pedih,

26:202

# فَيَأْتِيَهُمْ بَغْتَةً وَّهُمْ لَا يَشْعُرُوْنَ ۙ

faya/tiyahum baghtatan wahum l*aa* yasy'uruun**a**

maka datang azab kepada mereka secara mendadak, ketika mereka tidak menyadarinya,

26:203

# فَيَقُوْلُوْا هَلْ نَحْنُ مُنْظَرُوْنَ ۗ

fayaquuluu hal na*h*nu mun*zh*aruun**a**

lalu mereka berkata, “Apakah kami diberi penangguhan waktu?”

26:204

# اَفَبِعَذَابِنَا يَسْتَعْجِلُوْنَ

afabi'a*dzaa*bin*aa* yasta'jiluun**a**

Bukankah mereka yang meminta agar azab Kami dipercepat?

26:205

# اَفَرَءَيْتَ اِنْ مَّتَّعْنٰهُمْ سِنِيْنَ ۙ

afara-ayta in matta'n*aa*hum siniin**a**

Maka bagaimana pendapatmu jika kepada mereka Kami berikan kenikmatan hidup beberapa tahun,

26:206

# ثُمَّ جَاۤءَهُمْ مَّا كَانُوْا يُوْعَدُوْنَ ۙ

tsumma j*aa*-ahum m*aa* k*aa*nuu yuu'aduun**a**

kemudian datang kepada mereka azab yang diancamkan kepada mereka,

26:207

# مَآ اَغْنٰى عَنْهُمْ مَّا كَانُوْا يُمَتَّعُوْنَ ۗ

m*aa* aghn*aa* 'anhum m*aa* k*aa*nuu yumatta'uun**a**

niscaya tidak berguna bagi mereka kenikmatan yang mereka rasakan.

26:208

# وَمَآ اَهْلَكْنَا مِنْ قَرْيَةٍ اِلَّا لَهَا مُنْذِرُوْنَ ۖ

wam*aa* ahlakn*aa* min qaryatin ill*aa* lah*aa* mun*dz*iruun**a**

Dan Kami tidak membinasakan sesuatu negeri, kecuali setelah ada orang-orang yang memberi peringatan kepadanya;

26:209

# ذِكْرٰىۚ وَمَا كُنَّا ظٰلِمِيْنَ

*dz*ikr*aa* wam*aa* kunn*aa* *zhaa*limiin**a**

untuk (menjadi) peringatan. Dan Kami tidak berlaku zalim.

26:210

# وَمَا تَنَزَّلَتْ بِهِ الشَّيٰطِيْنُ

wam*aa* tanazzalat bihi **al**sysyay*aath*iin**u**

Dan (Al-Qur'an) itu tidaklah dibawa turun oleh setan-setan.

26:211

# وَمَا يَنْۢبَغِيْ لَهُمْ وَمَا يَسْتَطِيْعُوْنَ ۗ

wam*aa* yanbaghii lahum wam*aa* yasta*th*ii'uun**a**

Dan tidaklah pantas bagi mereka (Al-Qur'an itu), dan mereka pun tidak akan sanggup.

26:212

# اِنَّهُمْ عَنِ السَّمْعِ لَمَعْزُوْلُوْنَ ۗ

innahum 'ani **al**ssam'i lama'zuuluuna

Sesungguhnya untuk mendengarkannya pun mereka dijauhkan.

26:213

# فَلَا تَدْعُ مَعَ اللّٰهِ اِلٰهًا اٰخَرَ فَتَكُوْنَ مِنَ الْمُعَذَّبِيْنَ

fal*aa* tad'u ma'a **al**l*aa*hi il*aa*han *aa*khara fatakuuna mina **a**lmu'a*dzdz*abiin**a**

Maka janganlah kamu menyeru (menyembah) tuhan selain Allah, nanti kamu termasuk orang-orang yang diazab.

26:214

# وَاَنْذِرْ عَشِيْرَتَكَ الْاَقْرَبِيْنَ ۙ

wa-an*dz*ir 'asyiirataka **a**l-aqrabiin**a**

Dan berilah peringatan kepada kerabat-kerabatmu (Muhammad) yang terdekat,

26:215

# وَاخْفِضْ جَنَاحَكَ لِمَنِ اتَّبَعَكَ مِنَ الْمُؤْمِنِيْنَ ۚ

wa**i**khfi*dh* jan*aah*aka limani ittaba'aka mina **a**lmu/miniin**a**

dan rendahkanlah dirimu terhadap orang-orang yang beriman yang mengikutimu.

26:216

# فَاِنْ عَصَوْكَ فَقُلْ اِنِّيْ بَرِيْۤءٌ مِّمَّا تَعْمَلُوْنَ ۚ

fa-in 'a*sh*awka faqul innii barii-un mimm*aa* ta'maluun**a**

Kemudian jika mereka mendurhakaimu maka katakanlah (Muhammad), “Sesungguhnya aku tidak bertanggung jawab terhadap apa yang kamu kerjakan.”

26:217

# وَتَوَكَّلْ عَلَى الْعَزِيْزِ الرَّحِيْمِ ۙ

watawakkal 'al*aa* **a**l'aziizi **al**rra*h*iim**i**

Dan bertawakallah kepada (Allah) Yang Mahaperkasa, Maha Penyayang.

26:218

# الَّذِيْ يَرٰىكَ حِيْنَ تَقُوْمُ

**al**la*dz*ii yar*aa*ka *h*iina taquum**u**

Yang melihat engkau ketika engkau berdiri (untuk salat),

26:219

# وَتَقَلُّبَكَ فِى السّٰجِدِيْنَ

wataqallubaka fii **al**ss*aa*jidiin**a**

dan (melihat) perubahan gerakan badanmu di antara orang-orang yang sujud.

26:220

# اِنَّهٗ هُوَ السَّمِيْعُ الْعَلِيْمُ

innahu huwa **al**ssamii'u **a**l'aliim**u**

Sungguh, Dia Maha Mendengar, Maha Mengetahui.

26:221

# هَلْ اُنَبِّئُكُمْ عَلٰى مَنْ تَنَزَّلُ الشَّيٰطِيْنُ ۗ

hal unabbi-ukum 'al*aa* man tanazzalu **al**sysyay*aath*iin**u**

Maukah Aku beritakan kepadamu, kepada siapa setan-setan itu turun?

26:222

# تَنَزَّلُ عَلٰى كُلِّ اَفَّاكٍ اَثِيْمٍ ۙ

tanazzalu 'al*aa* kulli aff*aa*kin atsiim**in**

Mereka (setan) turun kepada setiap pendusta yang banyak berdosa,

26:223

# يُّلْقُوْنَ السَّمْعَ وَاَكْثَرُهُمْ كٰذِبُوْنَ ۗ

yulquuna **al**ssam'a wa-aktsaruhum k*aadz*ibuun**a**

mereka menyampaikan hasil pendengaran mereka, sedangkan kebanyakan mereka orang-orang pendusta.

26:224

# وَالشُّعَرَاۤءُ يَتَّبِعُهُمُ الْغَاوٗنَ ۗ

wa**al**sysyu'ar*aa*u yattabi'uhumu **a**lgh*aa*wuun**a**

Dan penyair-penyair itu diikuti oleh orang-orang yang sesat.

26:225

# اَلَمْ تَرَ اَنَّهُمْ فِيْ كُلِّ وَادٍ يَّهِيْمُوْنَ ۙ

alam tara annahum fii kulli w*aa*din yahiimuun**a**

Tidakkah engkau melihat bahwa mereka mengembara di setiap lembah,

26:226

# وَاَنَّهُمْ يَقُوْلُوْنَ مَا لَا يَفْعَلُوْنَ ۙ

wa-annahum yaquuluuna m*aa* l*aa* yaf'aluun**a**

dan bahwa mereka mengatakan apa yang mereka sendiri tidak mengerjakan(nya)?

26:227

# اِلَّا الَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِ وَذَكَرُوا اللّٰهَ كَثِيْرًا وَّانْتَصَرُوْا مِنْۢ بَعْدِ مَا ظُلِمُوْا ۗوَسَيَعْلَمُ الَّذِيْنَ ظَلَمُوْٓا اَيَّ مُنْقَلَبٍ يَّنْقَلِبُوْنَ ࣖ



illallażīna āmanụ wa ‘amiluṣ-ṣāliḥāti wa żakarullāha kaṡīraw wantaṣarụ mim ba’di mā ẓulimụ, wa saya’lamullażīna ẓalamū ayya mungqalabiy yangqalibụn

kecuali orang-orang (penyair-penyair) yang beriman dan beramal saleh dan banyak menyebut Allah dan mendapat kemenangan sesudah menderita kezaliman. Dan orang-orang yang zalim itu kelak akan mengetahui ke tempat mana mereka akan kembali.

<!--EndFragment-->