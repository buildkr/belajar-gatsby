---
title: (65) At-Talaq - الطلاق
date: 2021-10-27T04:04:31.602Z
ayat: 65
description: "Jumlah Ayat: 12 / Arti: Talak"
---
<!--StartFragment-->

65:1

# يٰٓاَيُّهَا النَّبِيُّ اِذَا طَلَّقْتُمُ النِّسَاۤءَ فَطَلِّقُوْهُنَّ لِعِدَّتِهِنَّ وَاَحْصُوا الْعِدَّةَۚ وَاتَّقُوا اللّٰهَ رَبَّكُمْۚ لَا تُخْرِجُوْهُنَّ مِنْۢ بُيُوْتِهِنَّ وَلَا يَخْرُجْنَ اِلَّآ اَنْ يَّأْتِيْنَ بِفَاحِشَةٍ مُّبَيِّنَةٍۗ وَتِلْكَ

y*aa* ayyuh*aa* **al**nnabiyyu i*dzaa* *th*allaqtumu **al**nnis*aa*-a fa*th*alliquuhunna li'iddatihinna wa-a*hs*uu **a**l'iddata wa**i**ttaquu **al**l*aa*

Wahai Nabi! Apabila kamu menceraikan istri-istrimu maka hendaklah kamu ceraikan mereka pada waktu mereka dapat (menghadapi) idahnya (yang wajar), dan hitunglah waktu idah itu, serta bertakwalah kepada Allah Tuhanmu. Janganlah kamu keluarkan mereka dari ru







65:2

# فَاِذَا بَلَغْنَ اَجَلَهُنَّ فَاَمْسِكُوْهُنَّ بِمَعْرُوْفٍ اَوْ فَارِقُوْهُنَّ بِمَعْرُوْفٍ وَّاَشْهِدُوْا ذَوَيْ عَدْلٍ مِّنْكُمْ وَاَقِيْمُوا الشَّهَادَةَ لِلّٰهِ ۗذٰلِكُمْ يُوْعَظُ بِهٖ مَنْ كَانَ يُؤْمِنُ بِاللّٰهِ وَالْيَوْمِ الْاٰخِرِ ەۗ وَمَنْ يَّ

fa-i*dzaa* balaghna ajalahunna fa-amsikuuhunna bima'ruufin aw f*aa*riquuhunna bima'ruufin wa-asyhiduu *dz*away 'adlin minkum wa-aqiimuu **al**sysyah*aa*data lill*aa*hi *dzaa*likum yuu'a*zh*u bihi man k*aa<*

Maka apabila mereka telah mendekati akhir idahnya, maka rujuklah (kembali kepada) mereka dengan baik atau lepaskanlah mereka dengan baik dan persaksikanlah dengan dua orang saksi yang adil di antara kamu dan hendaklah kamu tegakkan kesaksian itu karena Al







65:3

# وَّيَرْزُقْهُ مِنْ حَيْثُ لَا يَحْتَسِبُۗ وَمَنْ يَّتَوَكَّلْ عَلَى اللّٰهِ فَهُوَ حَسْبُهٗ ۗاِنَّ اللّٰهَ بَالِغُ اَمْرِهٖۗ قَدْ جَعَلَ اللّٰهُ لِكُلِّ شَيْءٍ قَدْرًا

wayarzuqhu min *h*aytsu l*aa* ya*h*tasibu waman yatawakkal 'al*aa* **al**l*aa*hi fahuwa *h*asbuhu inna **al**l*aa*ha b*aa*lighu amrihi qad ja'ala **al**l*aa*hu likulli sya

dan Dia memberinya rezeki dari arah yang tidak disangka-sangkanya. Dan barangsiapa bertawakal kepada Allah, niscaya Allah akan mencukupkan (keperluan)nya. Sesungguhnya Allah melaksanakan urusan-Nya. Sungguh, Allah telah mengadakan ketentuan bagi setiap se

65:4

# وَالّٰۤـِٔيْ يَىِٕسْنَ مِنَ الْمَحِيْضِ مِنْ نِّسَاۤىِٕكُمْ اِنِ ارْتَبْتُمْ فَعِدَّتُهُنَّ ثَلٰثَةُ اَشْهُرٍۙ وَّالّٰۤـِٔيْ لَمْ يَحِضْنَۗ وَاُولَاتُ الْاَحْمَالِ اَجَلُهُنَّ اَنْ يَّضَعْنَ حَمْلَهُنَّۗ وَمَنْ يَّتَّقِ اللّٰهَ يَجْعَلْ لَّهٗ مِنْ اَمْرِه

wa**a**ll*aa*-ii ya-isna mina **a**lma*h*ii*dh*i min nis*aa*-ikum ini irtabtum fa'iddatuhunna tsal*aa*tsatu asyhurin wa**a**ll*aa*-ii lam ya*h*i*dh*na waul*aa*tu **a**

Perempuan-perempuan yang tidak haid lagi (menopause) di antara istri-istrimu jika kamu ragu-ragu (tentang masa idahnya) maka idahnya adalah tiga bulan; dan begitu (pula) perempuan-perempuan yang tidak haid. Sedangkan perempuan-perempuan yang hamil, waktu







65:5

# ذٰلِكَ اَمْرُ اللّٰهِ اَنْزَلَهٗٓ اِلَيْكُمْۗ وَمَنْ يَّتَّقِ اللّٰهَ يُكَفِّرْ عَنْهُ سَيِّاٰتِهٖ وَيُعْظِمْ لَهٗٓ اَجْرًا

*dzaa*lika amru **al**l*aa*hi anzalahu ilaykum waman yattaqi **al**l*aa*ha yukaffir 'anhu sayyi-*aa*tihi wayu'*zh*im lahu ajr*aa***n**

Itulah perintah Allah yang diturunkan-Nya kepada kamu; barangsiapa bertakwa kepada Allah, niscaya Allah akan menghapus kesalahan-kesalahannya dan akan melipatgandakan pahala baginya.

65:6

# اَسْكِنُوْهُنَّ مِنْ حَيْثُ سَكَنْتُمْ مِّنْ وُّجْدِكُمْ وَلَا تُضَاۤرُّوْهُنَّ لِتُضَيِّقُوْا عَلَيْهِنَّۗ وَاِنْ كُنَّ اُولَاتِ حَمْلٍ فَاَنْفِقُوْا عَلَيْهِنَّ حَتّٰى يَضَعْنَ حَمْلَهُنَّۚ فَاِنْ اَرْضَعْنَ لَكُمْ فَاٰتُوْهُنَّ اُجُوْرَهُنَّۚ وَأْتَمِر

askinuuhunna min *h*aytsu sakantum min wujdikum wal*aa* tu*daa*rruuhunna litu*dh*ayyiquu 'alayhinna wa-in kunna ul*aa*ti *h*amlin fa-anfiquu 'alayhinna *h*att*aa* ya*dh*a'na *h*amlahunna fa-in ar*dh*a

Tempatkanlah mereka (para istri) di mana kamu bertempat tinggal menurut kemampuanmu dan janganlah kamu menyusahkan mereka untuk menyempitkan (hati) mereka. Dan jika mereka (istri-istri yang sudah ditalak) itu sedang hamil, maka berikanlah kepada mereka na

65:7

# لِيُنْفِقْ ذُوْ سَعَةٍ مِّنْ سَعَتِهٖۗ وَمَنْ قُدِرَ عَلَيْهِ رِزْقُهٗ فَلْيُنْفِقْ مِمَّآ اٰتٰىهُ اللّٰهُ ۗ لَا يُكَلِّفُ اللّٰهُ نَفْسًا اِلَّا مَآ اٰتٰىهَاۗ سَيَجْعَلُ اللّٰهُ بَعْدَ عُسْرٍ يُّسْرًا ࣖ

liyunfiq *dz*uu sa'atin min sa'atihi waman qudira 'alayhi rizquhu falyunfiq mimm*aa* *aa*t*aa*hu **al**l*aa*hu l*aa* yukallifu **al**l*aa*hu nafsan ill*aa* m*aa* *aa*t*aa*h

Hendaklah orang yang mempunyai keluasan memberi nafkah menurut kemampuannya, dan orang yang terbatas rezekinya, hendaklah memberi nafkah dari harta yang diberikan Allah kepadanya. Allah tidak membebani kepada seseorang melainkan (sesuai) dengan apa yang d

65:8

# وَكَاَيِّنْ مِّنْ قَرْيَةٍ عَتَتْ عَنْ اَمْرِ رَبِّهَا وَرُسُلِهٖ فَحَاسَبْنٰهَا حِسَابًا شَدِيْدًاۙ وَّعَذَّبْنٰهَا عَذَابًا نُّكْرًا

waka-ayyin min qaryatin 'atat 'an amri rabbih*aa* warusulihi fa*haa*sabn*aa*h*aa* *h*is*aa*ban syadiidan wa'a*dzdz*abn*aa*h*aa* 'a*dzaa*ban nukr*aa***n**

Dan betapa banyak (penduduk) negeri yang mendurhakai perintah Tuhan mereka dan rasul-rasul-Nya, maka Kami buat perhitungan terhadap penduduk negeri itu dengan perhitungan yang ketat, dan Kami azab mereka dengan azab yang mengerikan (di akhirat),

65:9

# فَذَاقَتْ وَبَالَ اَمْرِهَا وَكَانَ عَاقِبَةُ اَمْرِهَا خُسْرًا

fa*dzaa*qat wab*aa*la amrih*aa* wak*aa*na '*aa*qibatu amrih*aa* khusr*aa***n**

sehingga mereka merasakan akibat yang buruk dari perbuatannya, dan akibat perbuatan mereka, itu adalah kerugian yang besar.

65:10

# اَعَدَّ اللّٰهُ لَهُمْ عَذَابًا شَدِيْدًا ۖفَاتَّقُوا اللّٰهَ يٰٓاُولِى الْاَلْبَابِۛ الَّذِيْنَ اٰمَنُوْا ۛ قَدْ اَنْزَلَ اللّٰهُ اِلَيْكُمْ ذِكْرًاۙ

a'adda **al**l*aa*hu lahum 'a*dzaa*ban syadiidan fa**i**ttaquu **al**l*aa*ha y*aa* ulii **a**l-alb*aa*bi **al**la*dz*iina *aa*manuu qad anzala **al**

**Allah menyediakan azab yang keras bagi mereka, maka bertakwalah kepada Allah wahai orang-orang yang mempunyai akal! (Yaitu) orang-orang yang beriman. Sungguh, Allah telah menurunkan peringatan kepadamu,**









65:11

# رَّسُوْلًا يَّتْلُوْا عَلَيْكُمْ اٰيٰتِ اللّٰهِ مُبَيِّنٰتٍ لِّيُخْرِجَ الَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِ مِنَ الظُّلُمٰتِ اِلَى النُّوْرِۗ وَمَنْ يُّؤْمِنْۢ بِاللّٰهِ وَيَعْمَلْ صَالِحًا يُّدْخِلْهُ جَنّٰتٍ تَجْرِيْ مِنْ تَحْتِهَا الْاَنْهٰرُ خ

rasuulan yatluu 'alaykum *aa*y*aa*ti **al**l*aa*hi mubayyin*aa*tin liyukhrija **al**la*dz*iina *aa*manuu wa'amiluu **al***shshaa*li*haa*ti mina **al***zhzh*ul

(dengan mengutus) seorang Rasul yang membacakan ayat-ayat Allah kepadamu yang menerangkan (bermacam-macam hukum), agar Dia mengeluarkan orang-orang yang beriman dan mengerjakan kebajikan, dari kegelapan kepada cahaya. Dan barangsiapa beriman kepada Allah

65:12

# اَللّٰهُ الَّذِيْ خَلَقَ سَبْعَ سَمٰوٰتٍ وَّمِنَ الْاَرْضِ مِثْلَهُنَّۗ يَتَنَزَّلُ الْاَمْرُ بَيْنَهُنَّ لِتَعْلَمُوْٓا اَنَّ اللّٰهَ عَلٰى كُلِّ شَيْءٍ قَدِيْرٌ ەۙ وَّاَنَّ اللّٰهَ قَدْ اَحَاطَ بِكُلِّ شَيْءٍ عِلْمًا ࣖ

**al**l*aa*hu **al**la*dz*ii khalaqa sab'a sam*aa*w*aa*tin wamina **a**l-ar*dh*i mitslahunna yatanazzalu **a**l-amru baynahunna lita'lamuu anna **al**l*aa*ha 'a

Allah yang menciptakan tujuh langit dan dari (penciptaan) bumi juga serupa. Perintah Allah berlaku padanya, agar kamu mengetahui bahwa Allah Mahakuasa atas segala sesuatu, dan ilmu Allah benar-benar meliputi segala sesuatu.

<!--EndFragment-->