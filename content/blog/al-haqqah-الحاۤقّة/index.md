---
title: (69) Al-Haqqah - الحاۤقّة
date: 2021-10-27T04:05:46.774Z
ayat: 69
description: "Jumlah Ayat: 52 / Arti: Hari Kiamat"
---
<!--StartFragment-->

69:1

# اَلْحَاۤقَّةُۙ

al*haa*qqa**tu**

Hari Kiamat,

69:2

# مَا الْحَاۤقَّةُ ۚ

m*aa* **a**l*haa*qqa**tu**

apakah hari Kiamat itu?

69:3

# وَمَآ اَدْرٰىكَ مَا الْحَاۤقَّةُ ۗ

wam*aa* adr*aa*ka m*aa* **a**l*haa*qqa**tu**

Dan tahukah kamu apakah hari Kiamat itu?

69:4

# كَذَّبَتْ ثَمُوْدُ وَعَادٌ ۢبِالْقَارِعَةِ

ka*dzdz*abat tsamuudu wa'*aa*dun bi**a**lq*aa*ri'a**ti**

Kaum Samud, dan ‘Ad telah mendustakan hari Kiamat.

69:5

# فَاَمَّا ثَمُوْدُ فَاُهْلِكُوْا بِالطَّاغِيَةِ

fa-amm*aa* tsamuudu fauhlikuu bi**al***ththaa*ghiya**ti**

Maka adapun kaum Samud, mereka telah dibinasakan dengan suara yang sangat keras,

69:6

# وَاَمَّا عَادٌ فَاُهْلِكُوْا بِرِيْحٍ صَرْصَرٍ عَاتِيَةٍۙ

wa-amm*aa* '*aa*dun fauhlikuu birii*h*in *sh*ar*sh*arin '*aa*tiya**tin**

sedangkan kaum ‘Ad, mereka telah dibinasakan dengan angin topan yang sangat dingin,

69:7

# سَخَّرَهَا عَلَيْهِمْ سَبْعَ لَيَالٍ وَّثَمٰنِيَةَ اَيَّامٍۙ حُسُوْمًا فَتَرَى الْقَوْمَ فِيْهَا صَرْعٰىۙ كَاَنَّهُمْ اَعْجَازُ نَخْلٍ خَاوِيَةٍۚ

wa-amm*aa* '*aa*dun fauhlikuu birii*h*in *sh*ar*sh*arin '*aa*tiya**tin**

Allah menimpakan angin itu kepada mereka selama tujuh malam delapan hari terus-menerus; maka kamu melihat kaum ‘Ad pada waktu itu mati bergelimpangan seperti batang-batang pohon kurma yang telah kosong (lapuk).

69:8

# فَهَلْ تَرٰى لَهُمْ مِّنْۢ بَاقِيَةٍ

fahal tar*aa* lahum min b*aa*qiya**tin**

Maka adakah kamu melihat seorang pun yang masih tersisa di antara mereka?

69:9

# وَجَاۤءَ فِرْعَوْنُ وَمَنْ قَبْلَهٗ وَالْمُؤْتَفِكٰتُ بِالْخَاطِئَةِۚ

waj*aa*-a fir'awnu waman qablahu wa**a**lmu/tafik*aa*tu bi**a**lkh*aath*i-a**ti**

Kemudian datang Fir‘aun dan orang-orang yang sebelumnya dan (penduduk) negeri-negeri yang dijungkirbalikkan karena kesalahan yang besar.

69:10

# فَعَصَوْا رَسُوْلَ رَبِّهِمْ فَاَخَذَهُمْ اَخْذَةً رَّابِيَةً

fa'a*sh*aw rasuula rabbihim fa-akha*dz*ahum akh*dz*atan r*aa*biya**tan**

Maka mereka mendurhakai utusan Tuhannya, Allah menyiksa mereka dengan siksaan yang sangat keras.

69:11

# اِنَّا لَمَّا طَغَا الْمَاۤءُ حَمَلْنٰكُمْ فِى الْجَارِيَةِۙ

inn*aa* lamm*aa* *th*agh*aa* **a**lm*aa*u *h*amaln*aa*kum fii **a**lj*aa*riya**ti**

Sesungguhnya ketika air naik (sampai ke gunung), Kami membawa (nenek moyang) kamu ke dalam kapal,

69:12

# لِنَجْعَلَهَا لَكُمْ تَذْكِرَةً وَّتَعِيَهَآ اُذُنٌ وَّاعِيَةٌ

linaj'alah*aa* lakum ta*dz*kiratan wata'iyah*aa* u*dz*unun w*aa*'iya**tun**

agar Kami jadikan (peristiwa itu) sebagai peringatan bagi kamu dan agar diperhatikan oleh telinga yang mau mendengar.

69:13

# فَاِذَا نُفِخَ فِى الصُّوْرِ نَفْخَةٌ وَّاحِدَةٌ ۙ

fa-i*dzaa* nufikha fii **al***shsh*uuri nafkhatun w*aah*ida**tun**

Maka apabila sangkakala ditiup sekali tiup,

69:14

# وَّحُمِلَتِ الْاَرْضُ وَالْجِبَالُ فَدُكَّتَا دَكَّةً وَّاحِدَةًۙ

wa*h*umilati **a**l-ar*dh*u wa**a**ljib*aa*lu fadukkat*aa* dakkatan w*aah*ida**tan**

dan diangkatlah bumi dan gunung-gunung, lalu dibenturkan keduanya sekali benturan.

69:15

# فَيَوْمَىِٕذٍ وَّقَعَتِ الْوَاقِعَةُۙ

fayawma-i*dz*in waqa'ati **a**lw*aa*qi'a**tu**

Maka pada hari itu terjadilah hari Kiamat,

69:16

# وَانْشَقَّتِ السَّمَاۤءُ فَهِيَ يَوْمَىِٕذٍ وَّاهِيَةٌۙ

wa**i**nsyaqqati **al**ssam*aa*u fahiya yawma-i*dz*in w*aa*hiya**tun**

dan terbelahlah langit, karena pada hari itu langit menjadi rapuh.

69:17

# وَّالْمَلَكُ عَلٰٓى اَرْجَاۤىِٕهَاۗ وَيَحْمِلُ عَرْشَ رَبِّكَ فَوْقَهُمْ يَوْمَىِٕذٍ ثَمٰنِيَةٌ ۗ

wa**a**lmalaku 'al*aa* arj*aa*-ih*aa* waya*h*milu 'arsya rabbika fawqahum yawma-i*dz*in tsam*aa*niya**tun**

Dan para malaikat berada di berbagai penjuru langit. Pada hari itu delapan malaikat menjunjung ‘Arsy (singgasana) Tuhanmu di atas (kepala) mereka.

69:18

# يَوْمَىِٕذٍ تُعْرَضُوْنَ لَا تَخْفٰى مِنْكُمْ خَافِيَةٌ

yawma-i*dz*in tu'ra*dh*uuna l*aa* takhf*aa* minkum kh*aa*fiya**tun**

Pada hari itu kamu dihadapkan (kepada Tuhanmu), tidak ada sesuatu pun dari kamu yang tersembunyi (bagi Allah).

69:19

# فَاَمَّا مَنْ اُوْتِيَ كِتٰبَهٗ بِيَمِيْنِهٖ فَيَقُوْلُ هَاۤؤُمُ اقْرَءُوْا كِتٰبِيَهْۚ

fa-amm*aa* man uutiya kit*aa*bahu biyamiinihi fayaquulu h*aa*umu iqrauu kit*aa*biyah

Adapun orang yang kitabnya diberikan di tangan kanannya, maka dia berkata, “Ambillah, bacalah kitabku (ini).”

69:20

# اِنِّيْ ظَنَنْتُ اَنِّيْ مُلٰقٍ حِسَابِيَهْۚ

innii *zh*anantu annii mul*aa*qin *h*is*aa*biyah

Sesungguhnya aku yakin, bahwa (suatu saat) aku akan menerima perhitungan terhadap diriku.

69:21

# فَهُوَ فِيْ عِيْشَةٍ رَّاضِيَةٍۚ

fahuwa fii 'iisyatin r*aad*iya**tin**

Maka orang itu berada dalam kehidupan yang diridai,

69:22

# فِيْ جَنَّةٍ عَالِيَةٍۙ

fii jannatin '*aa*liya**tin**

dalam surga yang tinggi,

69:23

# قُطُوْفُهَا دَانِيَةٌ

qu*th*uufuh*aa* d*aa*niya**tun**

buah-buahannya dekat,

69:24

# كُلُوْا وَاشْرَبُوْا هَنِيْۤـًٔا ۢبِمَآ اَسْلَفْتُمْ فِى الْاَيَّامِ الْخَالِيَةِ

kuluu wa**i**syrabuu hanii-an bim*aa* aslaftum fii **a**l-ayy*aa*mi **a**lkh*aa*liya**ti**

(kepada mereka dikatakan), “Makan dan minumlah dengan nikmat karena amal yang telah kamu kerjakan pada hari-hari yang telah lalu.”

69:25

# وَاَمَّا مَنْ اُوْتِيَ كِتٰبَهٗ بِشِمَالِهٖ ەۙ فَيَقُوْلُ يٰلَيْتَنِيْ لَمْ اُوْتَ كِتٰبِيَهْۚ

wa-amm*aa* man uutiya kit*aa*bahu bisyim*aa*lihi fayaquulu y*aa* laytanii lam uuta kit*aa*biyah

Dan adapun orang yang kitabnya diberikan di tangan kirinya, maka dia berkata, “Alangkah baiknya jika kitabku (ini) tidak diberikan kepadaku.

69:26

# وَلَمْ اَدْرِ مَا حِسَابِيَهْۚ

walam adri m*aa* *h*is*aa*biyah

Sehingga aku tidak mengetahui bagaimana perhitunganku.

69:27

# يٰلَيْتَهَا كَانَتِ الْقَاضِيَةَۚ

y*aa* laytah*aa* k*aa*nati **a**lq*aad*iya**tu**

Wahai, kiranya (kematian) itulah yang menyudahi segala sesuatu.

69:28

# مَآ اَغْنٰى عَنِّيْ مَالِيَهْۚ

m*aa* aghn*aa* 'annii m*aa*liyah

Hartaku sama sekali tidak berguna bagiku.

69:29

# هَلَكَ عَنِّيْ سُلْطٰنِيَهْۚ

halaka 'annii sul*thaa*niyah

Kekuasaanku telah hilang dariku.”

69:30

# خُذُوْهُ فَغُلُّوْهُۙ

khu*dz*uuhu faghulluuh**u**

(Allah berfirman), “Tangkaplah dia lalu belenggulah tangannya ke lehernya.”

69:31

# ثُمَّ الْجَحِيْمَ صَلُّوْهُۙ

tsumma **a**lja*h*iima *sh*alluuh**u**

Kemudian masukkanlah dia ke dalam api neraka yang menyala-nyala.

69:32

# ثُمَّ فِيْ سِلْسِلَةٍ ذَرْعُهَا سَبْعُوْنَ ذِرَاعًا فَاسْلُكُوْهُۗ

tsumma fii silsilatin *dz*ar'uh*aa* sab'uuna *dz*ir*aa*'an fa**u**slukuuh**u**

Kemudian belitlah dia dengan rantai yang panjangnya tujuh puluh hasta.

69:33

# اِنَّهٗ كَانَ لَا يُؤْمِنُ بِاللّٰهِ الْعَظِيْمِۙ

innahu k*aa*na l*aa* yu/minu bi**al**l*aa*hi **a**l'a*zh*iim**i**

Sesungguhnya dialah orang yang tidak beriman kepada Allah Yang Mahabesar.

69:34

# وَلَا يَحُضُّ عَلٰى طَعَامِ الْمِسْكِيْنِۗ

wal*aa* ya*h*u*dhdh*u 'al*aa* *th*a'*aa*mi **a**lmiskiin**i**

Dan juga dia tidak mendorong (orang lain) untuk memberi makan orang miskin.

69:35

# فَلَيْسَ لَهُ الْيَوْمَ هٰهُنَا حَمِيْمٌۙ

wal*aa* ya*h*u*dhdh*u 'al*aa* *th*a'*aa*mi **a**lmiskiin**i**

Maka pada hari ini di sini tidak ada seorang teman pun baginya.

69:36

# وَّلَا طَعَامٌ اِلَّا مِنْ غِسْلِيْنٍۙ

wal*aa* *th*a'*aa*mun ill*aa* min ghisliin**in**

Dan tidak ada makanan (baginya) kecuali dari darah dan nanah.

69:37

# لَّا يَأْكُلُهٗٓ اِلَّا الْخَاطِـُٔوْنَ ࣖ

l*aa* ya/kuluhu ill*aa* **a**lkh*aath*i-uun**a**

Tidak ada yang memakannya kecuali orang-orang yang berdosa.

69:38

# فَلَآ اُقْسِمُ بِمَا تُبْصِرُوْنَۙ

fal*aa* uqsimu bim*aa* tub*sh*iruun**a**

Maka Aku bersumpah demi apa yang kamu lihat,

69:39

# وَمَا لَا تُبْصِرُوْنَۙ

wam*aa* l*aa* tub*sh*iruun**a**

dan demi apa yang tidak kamu lihat.

69:40

# اِنَّهٗ لَقَوْلُ رَسُوْلٍ كَرِيْمٍۙ

innahu laqawlu rasuulin kariim**in**

Sesungguhnya ia (Al-Qur'an) itu benar-benar wahyu (yang diturunkan kepada) Rasul yang mulia,

69:41

# وَّمَا هُوَ بِقَوْلِ شَاعِرٍۗ قَلِيْلًا مَّا تُؤْمِنُوْنَۙ

wam*aa* huwa biqawli sy*aa*'irin qaliilan m*aa* tu/minuun**a**

dan ia (Al-Qur'an) bukanlah perkataan seorang penyair. Sedikit sekali kamu beriman kepadanya.

69:42

# وَلَا بِقَوْلِ كَاهِنٍۗ قَلِيْلًا مَّا تَذَكَّرُوْنَۗ

wal*aa* biqawli k*aa*hinin qaliilan m*aa* ta*dz*akkaruun**a**

Dan bukan pula perkataan tukang tenung. Sedikit sekali kamu mengambil pelajaran darinya.

69:43

# تَنْزِيْلٌ مِّنْ رَّبِّ الْعٰلَمِيْنَ

tanziilun min rabbi **a**l'*aa*lamiin**a**

Ia (Al-Qur'an) adalah wahyu yang diturunkan dari Tuhan seluruh alam.

69:44

# وَلَوْ تَقَوَّلَ عَلَيْنَا بَعْضَ الْاَقَاوِيْلِۙ

walaw taqawwala 'alayn*aa* ba'*dh*a **a**l-aq*aa*wiil**i**

Dan sekiranya dia (Muhammad) mengada-adakan sebagian perkataan atas (nama) Kami,

69:45

# لَاَخَذْنَا مِنْهُ بِالْيَمِيْنِۙ

la-akha*dz*n*aa* minhu bi**a**lyamiin**i**

pasti Kami pegang dia pada tangan kanannya.

69:46

# ثُمَّ لَقَطَعْنَا مِنْهُ الْوَتِيْنَۖ

tsumma laqa*th*a'n*aa* minhu **a**lwatiin**a**

Kemudian Kami potong pembuluh jantungnya.

69:47

# فَمَا مِنْكُمْ مِّنْ اَحَدٍ عَنْهُ حَاجِزِيْنَۙ

fam*aa* minkum min a*h*adin 'anhu *haa*jiziin**a**

Maka tidak seorang pun dari kamu yang dapat menghalangi (Kami untuk menghukumnya).

69:48

# وَاِنَّهٗ لَتَذْكِرَةٌ لِّلْمُتَّقِيْنَ

wa-innahu lata*dz*kiratun lilmuttaqiin**a**

Dan sungguh, (Al-Qur'an) itu pelajaran bagi orang-orang yang bertakwa.

69:49

# وَاِنَّا لَنَعْلَمُ اَنَّ مِنْكُمْ مُّكَذِّبِيْنَۗ

wa-inn*aa* lana'lamu anna minkum muka*dzdz*ibiin**a**

Dan sungguh, Kami mengetahui bahwa di antara kamu ada orang yang mendustakan.

69:50

# وَاِنَّهٗ لَحَسْرَةٌ عَلَى الْكٰفِرِيْنَۚ

wa-innahu la*h*asratun 'al*aa* **a**lk*aa*firiin**a**

Dan sungguh, (Al-Qur'an) itu akan menimbulkan penyesalan bagi orang-orang kafir (di akhirat).

69:51

# وَاِنَّهٗ لَحَقُّ الْيَقِيْنِ

wa-innahu la*h*aqqu **a**lyaqiin**i**

Dan Sungguh, (Al-Qur'an) itu kebenaran yang meyakinkan.

69:52

# فَسَبِّحْ بِاسْمِ رَبِّكَ الْعَظِيْمِ ࣖ

fasabbi*h* bi**i**smi rabbika **a**l'a*zh*iim**i**

Maka bertasbihlah dengan (menyebut) nama Tuhanmu Yang Mahaagung.

<!--EndFragment-->