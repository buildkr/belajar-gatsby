---
title: (98) Al-Bayyinah - البيّنة
date: 2021-10-27T04:32:39.310Z
ayat: 98
description: "Jumlah Ayat: 8 / Arti: Bukti Nyata"
---
<!--StartFragment-->

98:1

# لَمْ يَكُنِ الَّذِيْنَ كَفَرُوْا مِنْ اَهْلِ الْكِتٰبِ وَالْمُشْرِكِيْنَ مُنْفَكِّيْنَ حَتّٰى تَأْتِيَهُمُ الْبَيِّنَةُۙ

lam yakuni **al**la*dz*iina kafaruu min ahli **a**lkit*aa*bi wa**a**lmusyrikiina munfakkiina *h*att*aa* ta/tiyahumu **a**lbayyina**tu**

Orang-orang yang kafir dari golongan Ahli Kitab dan orang-orang musyrik tidak akan meninggalkan (agama mereka) sampai datang kepada mereka bukti yang nyata,

98:2

# رَسُوْلٌ مِّنَ اللّٰهِ يَتْلُوْا صُحُفًا مُّطَهَّرَةًۙ

rasuulun mina **al**l*aa*hi yatluu *sh*u*h*ufan mu*th*ahhara**tan**

(yaitu) seorang Rasul dari Allah (Muhammad) yang membacakan lembaran-lembaran yang suci (Al-Qur'an),

98:3

# فِيْهَا كُتُبٌ قَيِّمَةٌ ۗ

fiih*aa* kutubun qayyima**tun**

di dalamnya terdapat (isi) kitab-kitab yang lurus (benar).

98:4

# وَمَا تَفَرَّقَ الَّذِيْنَ اُوْتُوا الْكِتٰبَ اِلَّا مِنْۢ بَعْدِ مَا جَاۤءَتْهُمُ الْبَيِّنَةُ ۗ

wam*aa* tafarraqa **al**la*dz*iina uutuu **a**lkit*aa*ba ill*aa* min ba'di m*aa* j*aa*-at-humu **a**lbayyina**tu**

Dan tidaklah terpecah-belah orang-orang Ahli Kitab melainkan setelah datang kepada mereka bukti yang nyata.

98:5

# وَمَآ اُمِرُوْٓا اِلَّا لِيَعْبُدُوا اللّٰهَ مُخْلِصِيْنَ لَهُ الدِّيْنَ ەۙ حُنَفَاۤءَ وَيُقِيْمُوا الصَّلٰوةَ وَيُؤْتُوا الزَّكٰوةَ وَذٰلِكَ دِيْنُ الْقَيِّمَةِۗ

wam*aa* umiruu ill*aa* liya'buduu **al**l*aa*ha mukhli*sh*iina lahu **al**ddiina *h*unaf*aa*-a wayuqiimuu **al***shsh*al*aa*ta wayu/tuu **al**zzak*aa*ta wa

Padahal mereka hanya diperintah menyembah Allah dengan ikhlas menaati-Nya semata-mata karena (menjalankan) agama, dan juga agar melaksanakan salat dan menunaikan zakat; dan yang demikian itulah agama yang lurus (benar).

98:6

# اِنَّ الَّذِيْنَ كَفَرُوْا مِنْ اَهْلِ الْكِتٰبِ وَالْمُشْرِكِيْنَ فِيْ نَارِ جَهَنَّمَ خٰلِدِيْنَ فِيْهَاۗ اُولٰۤىِٕكَ هُمْ شَرُّ الْبَرِيَّةِۗ

inna **al**la*dz*iina kafaruu min ahli **a**lkit*aa*bi wa**a**lmusyrikiina fii n*aa*ri jahannama kh*aa*lidiina fiih*aa* ul*aa*-ika hum syarru **a**lbariyya**ti**

**Sungguh, orang-orang yang kafir dari golongan Ahli Kitab dan orang-orang musyrik (akan masuk) ke neraka Jahanam; mereka kekal di dalamnya selama-lamanya. Mereka itu adalah sejahat-jahat makhluk.**

98:7

# اِنَّ الَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِ اُولٰۤىِٕكَ هُمْ خَيْرُ الْبَرِيَّةِۗ

inna **al**la*dz*iina *aa*manuu wa'amiluu **al***shshaa*li*haa*ti ul*aa*-ika hum khayru **a**lbariyya**ti**

Sungguh, orang-orang yang beriman dan mengerjakan kebajikan, mereka itu adalah sebaik-baik makhluk.

98:8

# جَزَاۤؤُهُمْ عِنْدَ رَبِّهِمْ جَنّٰتُ عَدْنٍ تَجْرِيْ مِنْ تَحْتِهَا الْاَنْهٰرُ خٰلِدِيْنَ فِيْهَآ اَبَدًا ۗرَضِيَ اللّٰهُ عَنْهُمْ وَرَضُوْا عَنْهُ ۗ ذٰلِكَ لِمَنْ خَشِيَ رَبَّهٗ ࣖ

jazā`uhum ‘inda rabbihim jannātu ‘adnin tajrī min taḥtihal-an-hāru khālidīna fīhā abadā, raḍiyallāhu ‘an-hum wa raḍụ ‘an-h, żālika liman khasyiya rabbah

Balasan mereka di sisi Tuhan mereka ialah surga ‘Adn yang mengalir di bawahnya sungai-sungai; mereka kekal di dalamnya selama-lamanya. Allah ridha terhadap mereka dan merekapun ridha kepada-Nya. Yang demikian itu adalah (balasan) bagi orang yang takut kepada Tuhannya.

<!--EndFragment-->