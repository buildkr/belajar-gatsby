---
title: (11) Hud - هود
date: 2021-10-27T03:33:06.310Z
ayat: 11
description: "Jumlah Ayat: 123 / Arti: Hud"
---
<!--StartFragment-->

11:1

# الۤرٰ ۗ كِتٰبٌ اُحْكِمَتْ اٰيٰتُهٗ ثُمَّ فُصِّلَتْ مِنْ لَّدُنْ حَكِيْمٍ خَبِيْرٍۙ

alif-l*aa*m-r*aa* kit*aa*bun u*h*kimat *aa*y*aa*tuhu tsumma fu*shsh*ilat min ladun *h*akiimin khabiir**in**

Alif Lam Ra. (Inilah) Kitab yang ayat-ayatnya disusun dengan rapi kemudian dijelaskan secara terperinci, (yang diturunkan) dari sisi (Allah) Yang Mahabijaksana, Mahateliti,

11:2

# اَلَّا تَعْبُدُوْٓا اِلَّا اللّٰهَ ۗاِنَّنِيْ لَكُمْ مِّنْهُ نَذِيْرٌ وَّبَشِيْرٌۙ

**al**l*aa* ta'buduu ill*aa* **al**l*aa*ha innanii lakum minhu na*dz*iirun wabasyiir**un**

agar kamu tidak menyembah selain Allah. Sesungguhnya aku (Muhammad) adalah pemberi peringatan dan pembawa berita gembira dari-Nya untukmu.

11:3

# وَّاَنِ اسْتَغْفِرُوْا رَبَّكُمْ ثُمَّ تُوْبُوْٓا اِلَيْهِ يُمَتِّعْكُمْ مَّتَاعًا حَسَنًا اِلٰٓى اَجَلٍ مُّسَمًّى وَّيُؤْتِ كُلَّ ذِيْ فَضْلٍ فَضْلَهٗ ۗوَاِنْ تَوَلَّوْا فَاِنِّيْٓ اَخَافُ عَلَيْكُمْ عَذَابَ يَوْمٍ كَبِيْرٍ

wa-ani istaghfiruu rabbakum tsumma tuubuu ilayhi yumatti'kum mat*aa*'an *h*asanan il*aa* ajalin musamman wayu/ti kulla *dz*ii fa*dh*lin fa*dh*lahu wa-in tawallaw fa-inii akh*aa*fu 'alaykum 'a*dzaa*ba yawmin kabiir

Dan hendaklah kamu memohon ampunan kepada Tuhanmu dan bertobat kepada-Nya, niscaya Dia akan memberi kenikmatan yang baik kepadamu sampai waktu yang telah ditentukan. Dan Dia akan memberikan karunia-Nya kepada setiap orang yang berbuat baik. Dan jika kamu

11:4

# اِلَى اللّٰهِ مَرْجِعُكُمْ ۚوَهُوَ عَلٰى كُلِّ شَيْءٍ قَدِيْرٌ

il*aa* **al**l*aa*hi marji'ukum wahuwa 'al*aa* kulli syay-in qadiir**un**

Kepada Allah-lah kamu kembali. Dia Mahakuasa atas segala sesuatu.

11:5

# اَلَآ اِنَّهُمْ يَثْنُوْنَ صُدُوْرَهُمْ لِيَسْتَخْفُوْا مِنْهُۗ اَلَا حِيْنَ يَسْتَغْشُوْنَ ثِيَابَهُمْ ۙيَعْلَمُ مَا يُسِرُّوْنَ وَمَا يُعْلِنُوْنَۚ اِنَّهٗ عَلِيْمٌ ۢ بِذَاتِ الصُّدُوْرِ ۔

al*aa* innahum yatsnuuna *sh*uduurahum liyastakhfuu minhu **a**l*aa* *h*iina yastaghsyuuna tsiy*aa*bahum ya'lamu m*aa* yusirruuna wam*aa* yu'linuuna innahu 'aliimun bi*dzaa*ti **al***shsh*

Ingatlah, sesungguhnya mereka (orang-orang munafik) memalingkan dada untuk menyembunyikan diri dari dia (Muhammad). Ingatlah, ketika mereka menyelimuti dirinya dengan kain, Allah mengetahui apa yang mereka sembunyikan dan apa yang mereka nyatakan, sungguh







11:6

# ۞ وَمَا مِنْ دَاۤبَّةٍ فِى الْاَرْضِ اِلَّا عَلَى اللّٰهِ رِزْقُهَا وَيَعْلَمُ مُسْتَقَرَّهَا وَمُسْتَوْدَعَهَا ۗ كُلٌّ فِيْ كِتٰبٍ مُّبِيْنٍ

wam*aa* min d*aa*bbatin fii **a**l-ar*dh*i ill*aa* 'al*aa* **al**l*aa*hi rizquh*aa* waya'lamu mustaqarrah*aa* wamustawda'ah*aa* kullun fii kit*aa*bin mubiin**in**

Dan tidak satupun makhluk bergerak (bernyawa) di bumi melainkan semuanya dijamin Allah rezekinya. Dia mengetahui tempat kediamannya dan tempat penyimpanannya. Semua (tertulis) dalam Kitab yang nyata (Lauh Mahfuzh).

11:7

# وَهُوَ الَّذِيْ خَلَقَ السَّمٰوٰتِ وَالْاَرْضَ فِيْ سِتَّةِ اَيَّامٍ وَّكَانَ عَرْشُهٗ عَلَى الْمَاۤءِ لِيَبْلُوَكُمْ اَيُّكُمْ اَحْسَنُ عَمَلًا ۗوَلَىِٕنْ قُلْتَ اِنَّكُمْ مَّبْعُوْثُوْنَ مِنْۢ بَعْدِ الْمَوْتِ لَيَقُوْلَنَّ الَّذِيْنَ كَفَرُوْٓا اِنْ هٰ

wahuwa **al**la*dz*ii khalaqa **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*a fii sittati ayy*aa*min wak*aa*na 'arsyuhu 'al*aa* **a**lm*aa*-i liyabluwakum ayyukum a*h*

Dan Dialah yang menciptakan langit dan bumi dalam enam masa, dan ‘Arsy-Nya di atas air, agar Dia menguji siapakah di antara kamu yang lebih baik amalnya. Jika engkau berkata (kepada penduduk Mekah), “Sesungguhnya kamu akan dibangkitkan setelah mati,” nisc

11:8

# وَلَىِٕنْ اَخَّرْنَا عَنْهُمُ الْعَذَابَ اِلٰٓى اُمَّةٍ مَّعْدُوْدَةٍ لَّيَقُوْلُنَّ مَا يَحْبِسُهٗ ۗ اَلَا يَوْمَ يَأْتِيْهِمْ لَيْسَ مَصْرُوْفًا عَنْهُمْ وَحَاقَ بِهِمْ مَّا كَانُوْا بِهٖ يَسْتَهْزِءُوْنَ ࣖ

wala-in akhkharn*aa* 'anhumu **a**l'a*dzaa*ba il*aa* ummatin ma'duudatin layaquulunna m*aa* ya*h*bisuhu **a**l*aa* yawma ya/tiihim laysa ma*sh*ruufan 'anhum wa*haa*qa bihim m*aa* k*a*

Dan sungguh, jika Kami tangguhkan azab terhadap mereka sampai waktu yang ditentukan, niscaya mereka akan berkata, “Apakah yang menghalanginya?” Ketahuilah, ketika azab itu datang kepada mereka, tidaklah dapat dielakkan oleh mereka. Mereka dikepung oleh (a







11:9

# وَلَىِٕنْ اَذَقْنَا الْاِنْسَانَ مِنَّا رَحْمَةً ثُمَّ نَزَعْنٰهَا مِنْهُۚ اِنَّهٗ لَيَـُٔوْسٌ كَفُوْرٌ

wala-in a*dz*aqn*aa* **a**l-ins*aa*na minn*aa* ra*h*matan tsumma naza'n*aa*h*aa* minhu innahu layauusun kafuur**un**

Dan jika Kami berikan rahmat Kami kepada manusia, kemudian (rahmat itu) Kami cabut kembali, pastilah dia menjadi putus asa dan tidak berterima kasih.

11:10

# وَلَىِٕنْ اَذَقْنٰهُ نَعْمَاۤءَ بَعْدَ ضَرَّاۤءَ مَسَّتْهُ لَيَقُوْلَنَّ ذَهَبَ السَّيِّاٰتُ عَنِّيْ ۗاِنَّهٗ لَفَرِحٌ فَخُوْرٌۙ

wala-in a*dz*aqn*aa*hu na'm*aa*-a ba'da *dh*arr*aa*-a massat-hu layaquulanna *dz*ahaba **al**ssayyi-*aa*tu 'annii innahu lafari*h*un fakhuur**un**

Dan jika Kami berikan kebahagiaan kepadanya setelah ditimpa bencana yang menimpanya, niscaya dia akan berkata, “Telah hilang bencana itu dariku.” Sesungguhnya dia (merasa) sangat gembira dan bangga,

11:11

# اِلَّا الَّذِيْنَ صَبَرُوْا وَعَمِلُوا الصّٰلِحٰتِۗ اُولٰۤىِٕكَ لَهُمْ مَّغْفِرَةٌ وَّاَجْرٌ كَبِيْرٌ

ill*aa* **al**la*dz*iina *sh*abaruu wa'amiluu **al***shshaa*li*haa*ti ul*aa*-ika lahum maghfiratun wa-ajrun kabiir**un**

kecuali orang-orang yang sabar, dan mengerjakan kebajikan, mereka memperoleh ampunan dan pahala yang besar.

11:12

# فَلَعَلَّكَ تَارِكٌۢ بَعْضَ مَا يُوْحٰىٓ اِلَيْكَ وَضَاۤىِٕقٌۢ بِهٖ صَدْرُكَ اَنْ يَّقُوْلُوْا لَوْلَآ اُنْزِلَ عَلَيْهِ كَنْزٌ اَوْ جَاۤءَ مَعَهٗ مَلَكٌ ۗاِنَّمَآ اَنْتَ نَذِيْرٌ ۗ وَاللّٰهُ عَلٰى كُلِّ شَيْءٍ وَّكِيْلٌ ۗ

fala'allaka t*aa*rikun ba'*dh*a m*aa* yuu*haa* ilayka wa*daa*-iqun bihi *sh*adruka an yaquuluu lawl*aa* unzila 'alayhi kanzun aw j*aa*-a ma'ahu malakun innam*aa* anta na*dz*iirun wa**al**l*a*

Maka boleh jadi engkau (Muhammad) hendak meninggalkan sebagian dari apa yang diwahyukan kepadamu dan dadamu sempit karenanya, karena mereka akan mengatakan, “Mengapa tidak diturunkan kepadanya harta (kekayaan) atau datang bersamanya malaikat?” Sungguh, en







11:13

# اَمْ يَقُوْلُوْنَ افْتَرٰىهُ ۗقُلْ فَأْتُوْا بِعَشْرِ سُوَرٍ مِّثْلِهٖ مُفْتَرَيٰتٍ وَّادْعُوْا مَنِ اسْتَطَعْتُمْ مِّنْ دُوْنِ اللّٰهِ اِنْ كُنْتُمْ صٰدِقِيْنَ

am yaquuluuna iftar*aa*hu qul fa/tuu bi'asyri suwarin mitslihi muftaray*aa*tin wa**u**d'uu mani ista*th*a'tum min duuni **al**l*aa*hi in kuntum *shaa*diqiin**a**

Bahkan mereka mengatakan, “Dia (Muhammad) telah membuat-buat Al-Qur'an itu.” Katakanlah, “(Kalau demikian), datangkanlah sepuluh surah semisal dengannya (Al-Qur'an) yang dibuat-buat, dan ajaklah siapa saja di antara kamu yang sanggup selain Allah, jika ka

11:14

# فَاِلَّمْ يَسْتَجِيْبُوْا لَكُمْ فَاعْلَمُوْٓا اَنَّمَآ اُنْزِلَ بِعِلْمِ اللّٰهِ وَاَنْ لَّآ اِلٰهَ اِلَّا هُوَ ۚفَهَلْ اَنْتُمْ مُّسْلِمُوْنَ

fa-illam yastajiibuu lakum fa**i**'lamuu annam*aa* unzila bi'ilmi **al**l*aa*hi wa-an l*aa* il*aa*ha ill*aa* huwa fahal antum muslimuun**a**

Maka jika mereka tidak memenuhi tantanganmu, maka (katakanlah), “Ketahuilah, bahwa (Al-Qur'an) itu diturunkan dengan ilmu Allah, dan bahwa tidak ada tuhan selain Dia, maka maukah kamu berserah diri (masuk Islam)?”

11:15

# مَنْ كَانَ يُرِيْدُ الْحَيٰوةَ الدُّنْيَا وَزِيْنَتَهَا نُوَفِّ اِلَيْهِمْ اَعْمَالَهُمْ فِيْهَا وَهُمْ فِيْهَا لَا يُبْخَسُوْنَ

man k*aa*na yuriidu **a**l*h*ay*aa*ta **al**dduny*aa* waziinatah*aa* nuwaffi ilayhim a'm*aa*lahum fiih*aa* wahum fiih*aa* l*aa* yubkhasuun**a**

Barangsiapa menghendaki kehidupan dunia dan perhiasannya, pasti Kami berikan (balasan) penuh atas pekerjaan mereka di dunia (dengan sempurna) dan mereka di dunia tidak akan dirugikan.

11:16

# اُولٰۤىِٕكَ الَّذِيْنَ لَيْسَ لَهُمْ فِى الْاٰخِرَةِ اِلَّا النَّارُ ۖوَحَبِطَ مَا صَنَعُوْا فِيْهَا وَبٰطِلٌ مَّا كَانُوْا يَعْمَلُوْنَ

ul*aa*-ika **al**la*dz*iina laysa lahum fii **a**l-*aa*khirati ill*aa* **al**nn*aa*ru wa*h*abi*th*a m*aa* *sh*ana'uu fiih*aa* wab*aath*ilun m*aa* k*aa*

Itulah orang-orang yang tidak memperoleh (sesuatu) di akhirat kecuali neraka, dan sia-sialah di sana apa yang telah mereka usahakan (di dunia) dan terhapuslah apa yang telah mereka kerjakan.

11:17

# اَفَمَنْ كَانَ عَلٰى بَيِّنَةٍ مِّنْ رَّبِّهٖ وَيَتْلُوْهُ شَاهِدٌ مِّنْهُ وَمِنْ قَبْلِهٖ كِتٰبُ مُوْسٰىٓ اِمَامًا وَّرَحْمَةًۗ اُولٰۤىِٕكَ يُؤْمِنُوْنَ بِهٖ ۗوَمَنْ يَّكْفُرْ بِهٖ مِنَ الْاَحْزَابِ فَالنَّارُ مَوْعِدُهٗ فَلَا تَكُ فِيْ مِرْيَةٍ مِّنْهُ

afaman k*aa*na 'al*aa* bayyinatin min rabbihi wayatluuhu sy*aa*hidun minhu wamin qablihi kit*aa*bu muus*aa* im*aa*man wara*h*matan ul*aa*-ika yu/minuuna bihi waman yakfur bihi mina **a**l-a*h*z*a*

Maka apakah (orang-orang kafir itu sama dengan) orang yang sudah mempunyai bukti yang nyata (Al-Qur'an) dari Tuhannya, dan diikuti oleh saksi dari-Nya dan sebelumnya sudah ada pula Kitab Musa yang menjadi pedoman dan rahmat? Mereka beriman kepadanya (Al-Q







11:18

# وَمَنْ اَظْلَمُ مِمَّنِ افْتَرٰى عَلَى اللّٰهِ كَذِبًاۗ اُولٰۤىِٕكَ يُعْرَضُوْنَ عَلٰى رَبِّهِمْ وَيَقُوْلُ الْاَشْهَادُ هٰٓؤُلَاۤءِ الَّذِيْنَ كَذَبُوْا عَلٰى رَبِّهِمْۚ اَلَا لَعْنَةُ اللّٰهِ عَلَى الظّٰلِمِيْنَ ۙ

waman a*zh*lamu mimmani iftar*aa* 'al*aa* **al**l*aa*hi ka*dz*iban ul*aa*-ika yu'ra*dh*uuna 'al*aa* rabbihim wayaquulu **a**l-asyh*aa*du h*aa*ul*aa*-i **al**la

Dan siapakah yang lebih zalim daripada orang yang mengada-adakan suatu kebohongan terhadap Allah? Mereka itu akan dihadapkan kepada Tuhan mereka, dan para saksi akan berkata, “Orang-orang inilah yang telah berbohong terhadap Tuhan mereka.” Ingatlah, lakna

11:19

# الَّذِيْنَ يَصُدُّوْنَ عَنْ سَبِيْلِ اللّٰهِ وَيَبْغُوْنَهَا عِوَجًاۗ وَهُمْ بِالْاٰخِرَةِ هُمْ كفِٰرُوْنَ

**al**la*dz*iina ya*sh*udduuna 'an sabiili **al**l*aa*hi wayabghuunah*aa* 'iwajan wahum bi**a**l-*aa*khirati hum k*aa*firuun**a**

(yaitu) mereka yang menghalangi dari jalan Allah dan menghendaki agar jalan itu bengkok. Dan mereka itulah orang yang tidak percaya adanya hari akhirat.

11:20

# اُولٰۤىِٕكَ لَمْ يَكُوْنُوْا مُعْجِزِيْنَ فِى الْاَرْضِ وَمَا كَانَ لَهُمْ مِّنْ دُوْنِ اللّٰهِ مِنْ اَوْلِيَاۤءَ ۘ يُضٰعَفُ لَهُمُ الْعَذَابُ ۗمَا كَانُوْا يَسْتَطِيْعُوْنَ السَّمْعَ وَمَا كَانُوْا يُبْصِرُوْنَ

ul*aa*-ika lam yakuunuu mu'jiziina fii **a**l-ar*dh*i wam*aa* k*aa*na lahum min duuni **al**l*aa*hi min awliy*aa*-a yu*daa*'afu lahumu **a**l'a*dzaa*bu m*aa* k*aa*nuu

Mereka tidak mampu menghalangi (siksaan Allah) di bumi, dan tidak akan ada bagi mereka penolong selain Allah. Azab itu dilipatgandakan kepada mereka. Mereka tidak mampu mendengar (kebenaran) dan tidak dapat melihat(nya).

11:21

# اُولٰۤىِٕكَ الَّذِيْنَ خَسِرُوْٓا اَنْفُسَهُمْ وَضَلَّ عَنْهُمْ مَّا كَانُوْا يَفْتَرُوْنَ

ul*aa*-ika **al**la*dz*iina khasiruu anfusahum wa*dh*alla 'anhum m*aa* k*aa*nuu yaftaruun**a**

Mereka itulah orang yang merugikan dirinya sendiri, dan lenyaplah dari mereka apa yang selalu mereka ada-adakan.

11:22

# لَاجَرَمَ اَنَّهُمْ فِى الْاٰخِرَةِ هُمُ الْاَخْسَرُوْنَ

l*aa* jarama annahum fii **a**l-*aa*khirati humu **a**l-akhsaruun**a**

Pasti mereka itu (menjadi) orang yang paling rugi di akhirat.

11:23

# اِنَّ الَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِ وَاَخْبَتُوْٓا اِلٰى رَبِّهِمْۙ اُولٰۤىِٕكَ اَصْحٰبُ الْجَنَّةِۚ هُمْ فِيْهَا خٰلِدُوْنَ

inna **al**la*dz*iina *aa*manuu wa'amiluu **al***shaa*li*haa*ti wa-akhbatuu il*aa* rabbihim ul*aa*-ika a*sh*-*haa*bu **a**ljannati hum fiih*aa* kh*aa*liduun**a<**

Sesungguhnya orang-orang yang beriman dan mengerjakan kebajikan dan merendahkan diri kepada Tuhan, mereka itu penghuni surga, mereka kekal di dalamnya.







11:24

# ۞ مَثَلُ الْفَرِيْقَيْنِ كَالْاَعْمٰى وَالْاَصَمِّ وَالْبَصِيْرِ وَالسَّمِيْعِۗ هَلْ يَسْتَوِيٰنِ مَثَلًا ۗ اَفَلَا تَذَكَّرُوْنَ ࣖ

matsalu **a**lfariiqayni ka**a**l**\-**a'm*aa* wa**a**l-a*sh*ammi wa**a**lba*sh*iiri wa**al**ssamii'i hal yastawiy*aa*ni matsalan afal*aa* ta*dz*akk

Perumpamaan kedua golongan (orang kafir dan mukmin), seperti orang buta dan tuli dengan orang yang dapat melihat dan dapat mendengar. Samakah kedua golongan itu? Maka tidakkah kamu mengambil pelajaran?

11:25

# وَلَقَدْ اَرْسَلْنَا نُوْحًا اِلٰى قَوْمِهٖٓ اِنِّيْ لَكُمْ نَذِيْرٌ مُّبِيْنٌ ۙ

walaqad arsaln*aa* nuu*h*an il*aa* qawmihi innii lakum na*dz*iirun mubiin**un**

Dan sungguh, Kami telah mengutus Nuh kepada kaumnya, (dia berkata), “Sungguh, aku ini adalah pemberi peringatan yang nyata bagi kamu,

11:26

# اَنْ لَّا تَعْبُدُوْٓا اِلَّا اللّٰهَ ۖاِنِّيْٓ اَخَافُ عَلَيْكُمْ عَذَابَ يَوْمٍ اَلِيْمٍ

an l*aa* ta'buduu ill*aa* **al**l*aa*ha innii akh*aa*fu 'alaykum 'a*dzaa*ba yawmin **a**liim**in**

agar kamu tidak menyembah selain Allah. Aku benar-benar khawatir kamu akan ditimpa azab (pada) hari yang sangat pedih.”

11:27

# فَقَالَ الْمَلَاُ الَّذِيْنَ كَفَرُوْا مِنْ قَوْمِهٖ مَا نَرٰىكَ اِلَّا بَشَرًا مِّثْلَنَا وَمَا نَرٰىكَ اتَّبَعَكَ اِلَّا الَّذِيْنَ هُمْ اَرَاذِلُنَا بَادِيَ الرَّأْيِۚ وَمَا نَرٰى لَكُمْ عَلَيْنَا مِنْ فَضْلٍۢ بَلْ نَظُنُّكُمْ كٰذِبِيْنَ

faq*aa*la **a**lmalau **al**la*dz*iina kafaruu min qawmihi m*aa* nar*aa*ka ill*aa* basyaran mitslan*aa* wam*aa* nar*aa*ka ittaba'aka ill*aa* **al**la*dz*iina hum ar

Maka berkatalah para pemuka yang kafir dari kaumnya, “Kami tidak melihat engkau, melainkan hanyalah seorang manusia (biasa) seperti kami, dan kami tidak melihat orang yang mengikuti engkau, melainkan orang yang hina dina di antara kami yang lekas percaya.

11:28

# قَالَ يٰقَوْمِ اَرَءَيْتُمْ اِنْ كُنْتُ عَلٰى بَيِّنَةٍ مِّنْ رَّبِّيْ وَاٰتٰىنِيْ رَحْمَةً مِّنْ عِنْدِهٖ فَعُمِّيَتْ عَلَيْكُمْۗ اَنُلْزِمُكُمُوْهَا وَاَنْتُمْ لَهَا كٰرِهُوْنَ

q*aa*la y*aa* qawmi ara-aytum in kuntu 'al*aa* bayyinatin min rabbii wa*aa*t*aa*nii ra*h*matan min 'indihi fa'ummiyat 'alaykum anulzimukumuuh*aa* wa-antum lah*aa* k*aa*rihuun**a**

Dia (Nuh) berkata, ”Wahai kaumku! Apa pendapatmu jika aku mempunyai bukti yang nyata dari Tuhanku, dan aku diberi rahmat dari sisi-Nya, sedangkan (rahmat itu) disamarkan bagimu. Apa kami akan memaksa kamu untuk menerimanya, padahal kamu tidak menyukainya

11:29

# وَيٰقَوْمِ لَآ اَسْـَٔلُكُمْ عَلَيْهِ مَالًاۗ اِنْ اَجْرِيَ اِلَّا عَلَى اللّٰهِ وَمَآ اَنَا۠ بِطَارِدِ الَّذِيْنَ اٰمَنُوْاۗ اِنَّهُمْ مُّلٰقُوْا رَبِّهِمْ وَلٰكِنِّيْٓ اَرٰىكُمْ قَوْمًا تَجْهَلُوْنَ

way*aa* qawmi l*aa* as-alukum 'alayhi m*aa*lan in ajriya ill*aa* 'al*aa* **al**l*aa*hi wam*aa* an*aa* bi*thaa*ridi **al**la*dz*iina *aa*manuu innahum mul*aa*quu r

Dan wahai kaumku! Aku tidak meminta harta kepada kamu (sebagai imbalan) atas seruanku. Imbalanku hanyalah dari Allah dan aku sekali-kali tidak akan mengusir orang yang telah beriman. Sungguh, mereka akan bertemu dengan Tuhannya, dan sebaliknya aku memanda

11:30

# وَيٰقَوْمِ مَنْ يَّنْصُرُنِيْ مِنَ اللّٰهِ اِنْ طَرَدْتُّهُمْ ۗ اَفَلَا تَذَكَّرُوْنَ

way*aa* qawmi man yan*sh*urunii mina **al**l*aa*hi in *th*aradtuhum afal*aa* ta*dz*akkaruun**a**

Dan wahai kaumku! Siapakah yang akan menolongku dari (azab) Allah jika aku mengusir mereka. Tidakkah kamu mengambil pelajaran?

11:31

# وَلَآ اَقُوْلُ لَكُمْ عِنْدِيْ خَزَاۤىِٕنُ اللّٰهِ وَلَآ اَعْلَمُ الْغَيْبَ وَلَآ اَقُوْلُ اِنِّيْ مَلَكٌ وَّلَآ اَقُوْلُ لِلَّذِيْنَ تَزْدَرِيْٓ اَعْيُنُكُمْ لَنْ يُّؤْتِيَهُمُ اللّٰهُ خَيْرًا ۗ اَللّٰهُ اَعْلَمُ بِمَا فِيْٓ اَنْفُسِهِمْ ۚاِنِّيْٓ اِ

wal*aa* aquulu lakum 'indii khaz*aa*-inu **al**l*aa*hi wal*aa* a'lamu **a**lghayba wal*aa* aquulu innii malakun wal*aa* aquulu lilla*dz*iina tazdarii a'yunukum lan yu/tiyahumu **al**

**Dan aku tidak mengatakan kepadamu, bahwa aku mempunyai gudang-gudang rezeki dan kekayaan dari Allah, dan aku tidak mengetahui yang gaib, dan tidak (pula) mengatakan bahwa sesungguhnya aku adalah malaikat, dan aku tidak (juga) mengatakan kepada orang yang**









11:32

# قَالُوْا يٰنُوْحُ قَدْ جَادَلْتَنَا فَاَ كْثَرْتَ جِدَالَنَا فَأْتِنَا بِمَا تَعِدُنَآ اِنْ كُنْتَ مِنَ الصّٰدِقِيْنَ

q*aa*luu y*aa* nuu*h*u qad j*aa*daltan*aa* fa-aktsarta jid*aa*lan*aa* fa/tin*aa* bim*aa* ta'idun*aa* in kunta mina **al***shshaa*diqiin**a**

Mereka berkata, “Wahai Nuh! Sungguh, engkau telah berbantah dengan kami, dan engkau telah memperpanjang bantahanmu terhadap kami, maka datangkanlah kepada kami azab yang engkau ancamkan, jika kamu termasuk orang yang benar.”

11:33

# قَالَ اِنَّمَا يَأْتِيْكُمْ بِهِ اللّٰهُ اِنْ شَاۤءَ وَمَآ اَنْتُمْ بِمُعْجِزِيْنَ

q*aa*la innam*aa* ya/tiikum bihi **al**l*aa*hu in sy*aa*-a wam*aa* antum bimu'jiziin**a**

Dia (Nuh) menjawab, “Hanya Allah yang akan mendatangkan azab kepadamu jika Dia menghendaki, dan kamu tidak akan dapat melepaskan diri.

11:34

# وَلَا يَنْفَعُكُمْ نُصْحِيْٓ اِنْ اَرَدْتُّ اَنْ اَنْصَحَ لَكُمْ اِنْ كَانَ اللّٰهُ يُرِيْدُ اَنْ يُّغْوِيَكُمْ ۗهُوَ رَبُّكُمْ ۗوَاِلَيْهِ تُرْجَعُوْنَۗ

wal*aa* yanfa'ukum nu*sy*ii in aradtu an an*sh*a*h*a lakum in k*aa*na **al**l*aa*hu yuriidu an yughwiyakum huwa rabbukum wa-ilayhi turja'uun**a**

Dan nasihatku tidak akan bermanfaat bagimu sekalipun aku ingin memberi nasihat kepadamu, kalau Allah hendak menyesatkan kamu. Dia adalah Tuhanmu, dan kepada-Nyalah kamu dikembalikan.”

11:35

# اَمْ يَقُوْلُوْنَ افْتَرٰىهُۗ قُلْ اِنِ افْتَرَيْتُهٗ فَعَلَيَّ اِجْرَامِيْ وَاَنَا۠ بَرِيْۤءٌ مِّمَّا تُجْرِمُوْنَ ࣖ

am yaquuluuna iftar*aa*hu qul ini iftaraytuhu fa'alayya ijr*aa*mii wa-an*aa* barii-un mimm*aa* tujrimuun**a**

Bahkan mereka (orang kafir) berkata, “Dia cuma mengada-ada saja.” Katakanlah (Muhammad), “Jika aku mengada-ada, akulah yang akan memikul dosanya, dan aku bebas dari dosa yang kamu perbuat.”

11:36

# وَاُوْحِيَ اِلٰى نُوْحٍ اَنَّهٗ لَنْ يُّؤْمِنَ مِنْ قَوْمِكَ اِلَّا مَنْ قَدْ اٰمَنَ فَلَا تَبْتَىِٕسْ بِمَا كَانُوْا يَفْعَلُوْنَۖ

wauu*h*iya il*aa* nuu*h*in annahu lan yu/mina min qawmika ill*aa* man qad *aa*mana fal*aa* tabta-is bim*aa* k*aa*nuu yaf'aluun**a**

Dan diwahyukan kepada Nuh, “Ketahuilah tidak akan beriman di an-tara kaummu, kecuali orang yang benar-benar beriman (saja), karena itu janganlah engkau bersedih hati tentang apa yang mereka perbuat.

11:37

# وَاصْنَعِ الْفُلْكَ بِاَعْيُنِنَا وَوَحْيِنَا وَلَا تُخَاطِبْنِيْ فِى الَّذِيْنَ ظَلَمُوْا ۚاِنَّهُمْ مُّغْرَقُوْنَ

wa**i***sh*na'i **a**lfulka bi-a'yunin*aa* wawa*h*yin*aa* wal*aa* tukh*aath*ibnii fii **al**la*dz*iina *zh*alamuu innahum mughraquun**a**

Dan buatlah kapal itu dengan pengawasan dan petunjuk wahyu Kami, dan janganlah engkau bicarakan dengan Aku tentang orang-orang yang zalim. Sesungguhnya mereka itu akan ditenggelamkan.”

11:38

# وَيَصْنَعُ الْفُلْكَۗ وَكُلَّمَا مَرَّ عَلَيْهِ مَلَاٌ مِّنْ قَوْمِهٖ سَخِرُوْا مِنْهُ ۗقَالَ اِنْ تَسْخَرُوْا مِنَّا فَاِنَّا نَسْخَرُ مِنْكُمْ كَمَا تَسْخَرُوْنَۗ

waya*sh*na'u **a**lfulka wakullam*aa* marra 'alayhi malaun min qawmihi sakhiruu minhu q*aa*la in taskharuu minn*aa* fa-inn*aa* naskharu minkum kam*aa* taskharuun**a**

Dan mulailah dia (Nuh) membuat kapal. Setiap kali pemimpin kaumnya berjalan melewatinya, mereka mengejeknya. Dia (Nuh) berkata, “Jika kamu mengejek kami, maka kami (pun) akan mengejekmu sebagaimana kamu mengejek (kami).

11:39

# فَسَوْفَ تَعْلَمُوْنَۙ مَنْ يَّأْتِيْهِ عَذَابٌ يُّخْزِيْهِ وَيَحِلُّ عَلَيْهِ عَذَابٌ مُّقِيْمٌ

fasawfa ta'lamuuna man ya/tiihi 'a*dzaa*bun yukhziihi waya*h*illu 'alayhi 'a*dzaa*bun muqiim**un**

Maka kelak kamu akan mengetahui siapa yang akan ditimpa azab yang menghinakan dan (siapa) yang akan ditimpa azab yang kekal.”

11:40

# حَتّٰىٓ اِذَا جَاۤءَ اَمْرُنَا وَفَارَ التَّنُّوْرُۙ قُلْنَا احْمِلْ فِيْهَا مِنْ كُلٍّ زَوْجَيْنِ اثْنَيْنِ وَاَهْلَكَ اِلَّا مَنْ سَبَقَ عَلَيْهِ الْقَوْلُ وَمَنْ اٰمَنَ ۗوَمَآ اٰمَنَ مَعَهٗٓ اِلَّا قَلِيْلٌ

*h*att*aa* i*dzaa* j*aa*-a amrun*aa* waf*aa*ra **al**ttannuuru quln*aa* i*h*mil fiih*aa* min kullin zawjayni itsnayni wa-ahlaka ill*aa* man sabaqa 'alayhi **a**lqawlu waman *aa<*

Hingga apabila perintah Kami datang dan tanur (dapur) telah memancarkan air, Kami berfirman, “Muatkanlah ke dalamnya (kapal itu) dari masing-masing (hewan) sepasang (jantan dan betina), dan (juga) keluargamu kecuali orang yang telah terkena ketetapan terd







11:41

# ۞ وَقَالَ ارْكَبُوْا فِيْهَا بِسْمِ اللّٰهِ مَجْرٰ۪ىهَا وَمُرْسٰىهَا ۗاِنَّ رَبِّيْ لَغَفُوْرٌ رَّحِيْمٌ

waq*aa*la irkabuu fiih*aa* bismi **al**l*aa*hi majr*aa*h*aa* wamurs*aa*h*aa* inna rabbii laghafuurun ra*h*iim**un**

Dan dia berkata, ”Naiklah kamu semua ke dalamnya (kapal) dengan (menyebut) nama Allah pada waktu berlayar dan berlabuhnya. Sesungguhnya Tuhanku Maha Pengampun, Maha Penyayang.”

11:42

# وَهِيَ تَجْرِيْ بِهِمْ فِيْ مَوْجٍ كَالْجِبَالِۗ وَنَادٰى نُوْحُ ِۨابْنَهٗ وَكَانَ فِيْ مَعْزِلٍ يّٰبُنَيَّ ارْكَبْ مَّعَنَا وَلَا تَكُنْ مَّعَ الْكٰفِرِيْنَ

wahiya tajrii bihim fii mawjin ka**a**ljib*aa*li wan*aa*d*aa* nuu*h*unu ibnahu wak*aa*na fii ma'zilin y*aa* bunayya irkab ma'an*aa* wal*aa* takun ma'a **a**lk*aa*firiin**a**

**Dan kapal itu berlayar membawa mereka ke dalam gelombang laksana gunung-gunung. Dan Nuh memanggil anaknya, ketika dia (anak itu) berada di tempat yang jauh terpencil, “Wahai anakku! Naiklah (ke kapal) bersama kami dan janganlah engkau bersama orang-orang**









11:43

# قَالَ سَاٰوِيْٓ اِلٰى جَبَلٍ يَّعْصِمُنِيْ مِنَ الْمَاۤءِ ۗقَالَ لَا عَاصِمَ الْيَوْمَ مِنْ اَمْرِ اللّٰهِ اِلَّا مَنْ رَّحِمَ ۚوَحَالَ بَيْنَهُمَا الْمَوْجُ فَكَانَ مِنَ الْمُغْرَقِيْنَ

q*aa*la sa*aa*wii il*aa* jabalin ya'*sh*imunii mina **a**lm*aa*-i q*aa*la l*aa* '*aas*ima **a**lyawma min amri **al**l*aa*hi ill*aa* man ra*h*ima wa*haa*la

Dia (anaknya) menjawab, “Aku akan mencari perlindungan ke gunung yang dapat menghindarkan aku dari air bah!” (Nuh) berkata, “Tidak ada yang melindungi dari siksaan Allah pada hari ini selain Allah yang Maha Penyayang.” Dan gelombang menjadi penghalang an

11:44

# وَقِيْلَ يٰٓاَرْضُ ابْلَعِيْ مَاۤءَكِ وَيَا سَمَاۤءُ اَقْلِعِيْ وَغِيْضَ الْمَاۤءُ وَقُضِيَ الْاَمْرُ وَاسْتَوَتْ عَلَى الْجُوْدِيِّ وَقِيْلَ بُعْدًا لِّلْقَوْمِ الظّٰلِمِيْنَ

waqiila y*aa* ar*dh*u ibla'ii m*aa*-aki way*aa* sam*aa*u aqli'ii waghii*dh*a **a**lm*aa*u waqu*dh*iya **a**l-amru wa**i**stawat 'al*aa* **a**ljuudiyyi waqiila

Dan difirmankan, “Wahai bumi! Telanlah airmu dan wahai langit (hujan!) berhentilah.” Dan air pun disurutkan, dan perintah pun diselesaikan dan kapal itupun berlabuh di atas gunung Judi, dan dikatakan, ”Binasalah orang-orang zalim.”

11:45

# وَنَادٰى نُوْحٌ رَّبَّهٗ فَقَالَ رَبِّ اِنَّ ابْنِيْ مِنْ اَهْلِيْۚ وَاِنَّ وَعْدَكَ الْحَقُّ وَاَنْتَ اَحْكَمُ الْحٰكِمِيْنَ

wan*aa*d*aa* nuu*h*un rabbahu faq*aa*la rabbi inna ibnii min ahlii wa-inna wa'daka **a**l*h*aqqu wa-anta a*h*kamu **a**l*haa*kimiin**a**

Dan Nuh memohon kepada Tuhannya sambil berkata, “Ya Tuhanku, sesungguhnya anakku adalah termasuk keluargaku, dan janji-Mu itu pasti benar. Engkau adalah hakim yang paling adil.”

11:46

# قَالَ يٰنُوْحُ اِنَّهٗ لَيْسَ مِنْ اَهْلِكَ ۚاِنَّهٗ عَمَلٌ غَيْرُ صَالِحٍ فَلَا تَسْـَٔلْنِ مَا لَيْسَ لَكَ بِهٖ عِلْمٌ ۗاِنِّيْٓ اَعِظُكَ اَنْ تَكُوْنَ مِنَ الْجٰهِلِيْنَ

q*aa*la y*aa* nuu*h*u innahu laysa min ahlika innahu 'amalun ghayru *shaa*li*h*in fal*aa* tas-alni m*aa* laysa laka bihi 'ilmun innii a'i*zh*uka an takuuna mina **a**lj*aa*hiliin**a**

Dia (Allah) berfirman, “Wahai Nuh! Sesungguhnya dia bukanlah termasuk keluargamu, karena perbuatannya sungguh tidak baik, sebab itu jangan engkau memohon kepada-Ku sesuatu yang tidak engkau ketahui (hakikatnya). Aku menasihatimu agar (engkau) tidak termas

11:47

# قَالَ رَبِّ اِنِّيْٓ اَعُوْذُ بِكَ اَنْ اَسْـَٔلَكَ مَا لَيْسَ لِيْ بِهٖ عِلْمٌ ۗوَاِلَّا تَغْفِرْ لِيْ وَتَرْحَمْنِيْٓ اَكُنْ مِّنَ الْخٰسِرِيْنَ

q*aa*la rabbi innii a'uu*dz*u bika an as-alaka m*aa* laysa lii bihi 'ilmun wa-ill*aa* taghfir lii watar*h*amnii akun mina **a**lkh*aa*siriin**a**

Dia (Nuh) berkata, “Ya Tuhanku, sesungguhnya aku berlindung kepada-Mu untuk memohon kepada-Mu sesuatu yang aku tidak mengetahui (hakikatnya). Kalau Engkau tidak mengampuniku, dan (tidak) menaruh belas kasihan kepadaku, niscaya aku termasuk orang yang rugi

11:48

# قِيْلَ يٰنُوْحُ اهْبِطْ بِسَلٰمٍ مِّنَّا وَبَرَكٰتٍ عَلَيْكَ وَعَلٰٓى اُمَمٍ مِّمَّنْ مَّعَكَ ۗوَاُمَمٌ سَنُمَتِّعُهُمْ ثُمَّ يَمَسُّهُمْ مِّنَّا عَذَابٌ اَلِيْمٌ

qiila y*aa* nuu*h*u ihbi*th* bisal*aa*min minn*aa* wabarak*aa*tin 'alayka wa'al*aa* umamin mimman ma'aka waumamun sanumatti'uhum tsumma yamassuhum minn*aa* 'a*dzaa*bun **a**liim**un**

Difirmankan, “Wahai Nuh! Turunlah dengan selamat sejahtera dan penuh keberkahan dari Kami, bagimu dan bagi semua umat (mukmin) yang bersamamu. Dan ada umat-umat yang Kami beri kesenangan (dalam kehidupan dunia), kemudian mereka akan ditimpa azab Kami yang

11:49

# تِلْكَ مِنْ اَنْۢبَاۤءِ الْغَيْبِ نُوْحِيْهَآ اِلَيْكَ ۚمَا كُنْتَ تَعْلَمُهَآ اَنْتَ وَلَا قَوْمُكَ مِنْ قَبْلِ هٰذَاۚ فَاصْبِرْۚ اِنَّ الْعَاقِبَةَ لِلْمُتَّقِيْنَ ࣖ

tilka min anb*aa*-i **a**lghaybi nuu*h*iih*aa* ilayka m*aa* kunta ta'lamuh*aa* anta wal*aa* qawmuka min qabli h*aadzaa* fa**i***sh*bir inna **a**l'*aa*qibata lilmuttaqiin<

Itulah sebagian dari berita-berita gaib yang Kami wahyukan kepadamu (Muhammad); tidak pernah engkau mengetahuinya dan tidak (pula) kaummu sebelum ini. Maka bersabarlah, sungguh, kesudahan (yang baik) adalah bagi orang yang bertakwa.

11:50

# وَاِلٰى عَادٍ اَخَاهُمْ هُوْدًا ۗقَالَ يٰقَوْمِ اعْبُدُوا اللّٰهَ مَا لَكُمْ مِّنْ اِلٰهٍ غَيْرُهٗ ۗاِنْ اَنْتُمْ اِلَّا مُفْتَرُوْنَ

wa-il*aa* '*aa*din akh*aa*hum huudan q*aa*la y*aa* qawmi u'buduu **al**l*aa*ha m*aa* lakum min il*aa*hin ghayruhu in antum ill*aa* muftaruun**a**

Dan kepada kaum ‘Ad (Kami utus) saudara mereka, Hud. Dia berkata, “Wahai kaumku! Sembahlah Allah, tidak ada tuhan bagimu selain Dia. (Selama ini) kamu hanyalah mengada-ada.

11:51

# يٰقَوْمِ لَآ اَسْـَٔلُكُمْ عَلَيْهِ اَجْرًا ۗاِنْ اَجْرِيَ اِلَّا عَلَى الَّذِيْ فَطَرَنِيْ ۗ اَفَلَا تَعْقِلُوْنَ

y*aa* qawmi l*aa* as-alukum 'alayhi ajran in ajriya ill*aa* 'al*aa* **al**la*dz*ii fa*th*aranii afal*aa* ta'qiluun**a**

Wahai kaumku! Aku tidak meminta imbalan kepadamu atas (seruanku) ini. Imbalanku hanyalah dari Allah yang telah menciptakanku. Tidakkah kamu mengerti?”

11:52

# وَيٰقَوْمِ اسْتَغْفِرُوْا رَبَّكُمْ ثُمَّ تُوْبُوْٓا اِلَيْهِ يُرْسِلِ السَّمَاۤءَ عَلَيْكُمْ مِّدْرَارًا وَّيَزِدْكُمْ قُوَّةً اِلٰى قُوَّتِكُمْ وَلَا تَتَوَلَّوْا مُجْرِمِيْنَ

way*aa* qawmi istaghfiruu rabbakum tsumma tuubuu ilayhi yursili **al**ssam*aa*-a 'alaykum midr*aa*ran wayazidkum quwwatan il*aa* quwwatikum wal*aa* tatawallaw mujrimiin**a**

Dan (Hud berkata), “Wahai kaumku! Mohonlah ampunan kepada Tuhanmu lalu bertobatlah kepada-Nya, niscaya Dia menurunkan hujan yang sangat deras, Dia akan menambahkan kekuatan di atas kekuatanmu, dan janganlah kamu berpaling menjadi orang yang berdosa.”

11:53

# قَالُوْا يٰهُوْدُ مَاجِئْتَنَا بِبَيِّنَةٍ وَّمَا نَحْنُ بِتَارِكِيْٓ اٰلِهَتِنَا عَنْ قَوْلِكَ وَمَا نَحْنُ لَكَ بِمُؤْمِنِيْنَ

q*aa*luu y*aa* huudu m*aa* ji/tan*aa* bibayyinatin wam*aa* na*h*nu bit*aa*rikii *aa*lihatin*aa* 'an qawlika wam*aa* na*h*nu laka bimu/miniin**a**

Mereka (kaum ‘Ad) berkata, “Wahai Hud! Engkau tidak mendatangkan suatu bukti yang nyata kepada kami, dan kami tidak akan meninggalkan sesembahan kami karena perkataanmu dan kami tidak akan mempercayaimu,

11:54

# اِنْ نَّقُوْلُ اِلَّا اعْتَرٰىكَ بَعْضُ اٰلِهَتِنَا بِسُوْۤءٍ ۗقَالَ اِنِّيْٓ اُشْهِدُ اللّٰهَ وَاشْهَدُوْٓا اَنِّيْ بَرِيْۤءٌ مِّمَّا تُشْرِكُوْنَ

in naquulu ill*aa* i'tar*aa*ka ba'*dh*u *aa*lihatin*aa* bisuu-in q*aa*la innii usyhidu **al**l*aa*ha wa**i**syhaduu annii barii-un mimm*aa* tusyrikuun**a**

kami hanya mengatakan bahwa sebagian sesembahan kami telah menimpakan penyakit gila atas dirimu.” Dia (Hud) menjawab, “Sesungguhnya aku bersaksi kepada Allah dan saksikanlah bahwa aku berlepas diri dari apa yang kamu persekutukan,

11:55

# مِنْ دُوْنِهٖ فَكِيْدُوْنِيْ جَمِيْعًا ثُمَّ لَا تُنْظِرُوْنِ

min duunihi fakiiduunii jamii'an tsumma l*aa* tun*zh*iruun**i**

dengan yang lain, sebab itu jalankanlah semua tipu dayamu terhadapku dan jangan kamu tunda lagi.

11:56

# اِنِّيْ تَوَكَّلْتُ عَلَى اللّٰهِ رَبِّيْ وَرَبِّكُمْ ۗمَا مِنْ دَاۤبَّةٍ اِلَّا هُوَ اٰخِذٌۢ بِنَاصِيَتِهَا ۗاِنَّ رَبِّيْ عَلٰى صِرَاطٍ مُّسْتَقِيْمٍ

innii tawakkaltu 'al*aa* **al**l*aa*hi rabbii warabbikum m*aa* min d*aa*bbatin ill*aa* huwa *aa*khi*dz*un bin*aas*iyatih*aa* inna rabbii 'al*aa* *sh*ir*aath*in mustaqiim**in**

**Sesungguhnya aku bertawakal kepada Allah Tuhanku dan Tuhanmu. Tidak satu pun makhluk bergerak yang bernyawa melainkan Dialah yang memegang ubun-ubunnya (menguasainya). Sungguh, Tuhanku di jalan yang lurus (adil).**









11:57

# فَاِنْ تَوَلَّوْا فَقَدْ اَبْلَغْتُكُمْ مَّآ اُرْسِلْتُ بِهٖٓ اِلَيْكُمْ ۗوَيَسْتَخْلِفُ رَبِّيْ قَوْمًا غَيْرَكُمْۗ وَلَا تَضُرُّوْنَهٗ شَيْـًٔا ۗاِنَّ رَبِّيْ عَلٰى كُلِّ شَيْءٍ حَفِيْظٌ

fa-in tawallaw faqad ablaghtukum m*aa* ursiltu bihi ilaykum wayastakhlifu rabbii qawman ghayrakum wal*aa* ta*dh*urruunahu syay-an inna rabbii 'al*aa* kulli syay-in *h*afii*zh***un**

Maka jika kamu berpaling, maka sungguh, aku telah menyampaikan kepadamu apa yang menjadi tugasku sebagai rasul kepadamu. Dan Tuhanku akan mengganti kamu dengan kaum yang lain, sedang kamu tidak dapat mendatangkan mudarat kepada-Nya sedikit pun. Sesungguhn

11:58

# وَلَمَّا جَاۤءَ اَمْرُنَا نَجَّيْنَا هُوْدًا وَّالَّذِيْنَ اٰمَنُوْا مَعَهٗ بِرَحْمَةٍ مِّنَّاۚ وَنَجَّيْنٰهُمْ مِّنْ عَذَابٍ غَلِيْظٍ

walamm*aa* j*aa*-a amrun*aa* najjayn*aa* huudan wa**a**lla*dz*iina *aa*manuu ma'ahu bira*h*matin minn*aa* wanajjayn*aa*hum min 'a*dzaa*bin ghalii*zh***in**

Dan ketika azab Kami datang, Kami selamatkan Hud dan orang-orang yang beriman bersama dia dengan rahmat Kami. Kami selamatkan (pula) mereka (di akhirat) dari azab yang berat.

11:59

# وَتِلْكَ عَادٌ ۖجَحَدُوْا بِاٰيٰتِ رَبِّهِمْ وَعَصَوْا رُسُلَهٗ وَاتَّبَعُوْٓا اَمْرَ كُلِّ جَبَّارٍ عَنِيْدٍ

watilka '*aa*dun ja*h*aduu bi-*aa*y*aa*ti rabbihim wa'a*sh*aw rusulahu wa**i**ttaba'uu amra kulli jabb*aa*rin 'aniid**in**

Dan itulah (kisah) kaum ‘Ad yang mengingkari tanda-tanda (kekuasaan) Tuhan. Mereka mendurhakai rasul-rasul-Nya dan menuruti perintah semua penguasa yang sewenang-wenang lagi durhaka.

11:60

# وَاُتْبِعُوْا فِيْ هٰذِهِ الدُّنْيَا لَعْنَةً وَّيَوْمَ الْقِيٰمَةِ ۗ اَلَآ اِنَّ عَادًا كَفَرُوْا رَبَّهُمْ ۗ اَلَا بُعْدًا لِّعَادٍ قَوْمِ هُوْدٍ ࣖ

wautbi'uu fii h*aadz*ihi **al**dduny*aa* la'natan wayawma **a**lqiy*aa*mati **a**l*aa* inna '*aa*dan kafaruu rabbahum **a**l*aa* bu'dan li'*aa*din qawmi huud**in<**

Dan mereka selalu diikuti dengan laknat di dunia ini dan (begitu pula) di hari Kiamat. Ingatlah, kaum ‘Ad itu ingkar kepada Tuhan mereka. Sungguh, binasalah kaum ‘Ad, umat Hud itu,







11:61

# ۞ وَاِلٰى ثَمُوْدَ اَخَاهُمْ صٰلِحًا ۘ قَالَ يٰقَوْمِ اعْبُدُوا اللّٰهَ مَا لَكُمْ مِّنْ اِلٰهٍ غَيْرُهٗ ۗهُوَ اَنْشَاَكُمْ مِّنَ الْاَرْضِ وَاسْتَعْمَرَكُمْ فِيْهَا فَاسْتَغْفِرُوْهُ ثُمَّ تُوْبُوْٓا اِلَيْهِ ۗاِنَّ رَبِّيْ قَرِيْبٌ مُّجِيْبٌ

wa-il*aa* tsamuuda akh*aa*hum *shaa*li*h*an q*aa*la y*aa* qawmi u'buduu **al**l*aa*ha m*aa* lakum min il*aa*hin ghayruhu huwa ansya-akum mina **a**l-ar*dh*i wa**i**st

dan kepada kaum samud (Kami utus) saudara mereka, Saleh. Dia berkata, “Wahai kaumku! Sembahlah Allah, tidak ada tuhan bagimu selain Dia. Dia telah menciptakanmu dari bumi (tanah) dan menjadikanmu pemakmurnya, karena itu mohonlah ampunan kepada-Nya, kemudi

11:62

# قَالُوْا يٰصٰلِحُ قَدْ كُنْتَ فِيْنَا مَرْجُوًّا قَبْلَ هٰذَآ اَتَنْهٰىنَآ اَنْ نَّعْبُدَ مَا يَعْبُدُ اٰبَاۤؤُنَا وَاِنَّنَا لَفِيْ شَكٍّ مِّمَّا تَدْعُوْنَآ اِلَيْهِ مُرِيْبٍ

q*aa*luu y*aa* *shaa*li*h*u qad kunta fiin*aa* marjuwwan qabla h*aadzaa* atanh*aa*n*aa* an na'buda m*aa* ya'budu *aa*b*aa*un*aa* wa-innan*aa* lafii syakkin mimm*aa* tad'uun*aa* ila

Mereka (kaum samud) berkata, “Wahai Saleh! Sungguh, engkau sebelum ini berada di tengah-tengah kami merupakan orang yang di harapkan, mengapa engkau melarang kami menyembah apa yang disembah oleh nenek moyang kami? Sungguh, kami benar-benar dalam keraguan

11:63

# قَالَ يٰقَوْمِ اَرَءَيْتُمْ اِنْ كُنْتُ عَلٰى بَيِّنَةٍ مِّنْ رَّبِّيْۗ وَاٰتٰىنِيْ مِنْهُ رَحْمَةً فَمَنْ يَّنْصُرُنِيْ مِنَ اللّٰهِ اِنْ عَصَيْتُهٗ ۗفَمَا تَزِيْدُوْنَنِيْ غَيْرَ تَخْسِيْرٍ

q*aa*la y*aa* qawmi ara-aytum in kuntu 'al*aa* bayyinatin min rabbii wa*aa*t*aa*nii minhu ra*h*matan faman yan*sh*urunii mina **al**l*aa*hi in 'a*sh*aytuhu fam*aa* taziiduunanii ghayra takhsii

Dia (Saleh) berkata, “Wahai kaumku! Terangkanlah kepadaku jika aku mempunyai bukti yang nyata dari Tuhanku dan diberi-Nya aku rahmat (kenabian) dari-Nya, maka siapa yang akan menolongku dari (azab) Allah jika aku mendurhakai-Nya? Maka kamu hanya akan mena

11:64

# وَيٰقَوْمِ هٰذِهٖ نَاقَةُ اللّٰهِ لَكُمْ اٰيَةً فَذَرُوْهَا تَأْكُلْ فِيْٓ اَرْضِ اللّٰهِ وَلَا تَمَسُّوْهَا بِسُوْۤءٍ فَيَأْخُذَكُمْ عَذَابٌ قَرِيْبٌ

way*aa* qawmi h*aadz*ihi n*aa*qatu **al**l*aa*hi lakum *aa*yatan fa*dz*aruuh*aa* ta/kul fii ar*dh*i **al**l*aa*hi wal*aa* tamassuuh*aa* bisuu-in faya/khu*dz*akum 'a*dz*

Dan wahai kaumku! Inilah unta betina dari Allah, sebagai mukjizat untukmu, sebab itu biarkanlah dia makan di bumi Allah, dan janganlah kamu mengganggunya dengan gangguan apa pun yang akan menyebabkan kamu segera ditimpa (azab).”







11:65

# فَعَقَرُوْهَا فَقَالَ تَمَتَّعُوْا فِيْ دَارِكُمْ ثَلٰثَةَ اَيَّامٍ ۗذٰلِكَ وَعْدٌ غَيْرُ مَكْذُوْبٍ

fa'aqaruuh*aa* faq*aa*la tamatta'uu fii d*aa*rikum tsal*aa*tsata ayy*aa*min *dzaa*lika wa'dun ghayru mak*dz*uub**in**

Maka mereka menyembelih unta itu, kemudian dia (Saleh) berkata, “Bersukarialah kamu semua di rumahmu selama tiga hari. Itu adalah janji yang tidak dapat didustakan.”

11:66

# فَلَمَّا جَاۤءَ اَمْرُنَا نَجَّيْنَا صٰلِحًا وَّالَّذِيْنَ اٰمَنُوْا مَعَهٗ بِرَحْمَةٍ مِّنَّا وَمِنْ خِزْيِ يَوْمِىِٕذٍ ۗاِنَّ رَبَّكَ هُوَ الْقَوِيُّ الْعَزِيْزُ

falamm*aa* j*aa*-a amrun*aa* najjayn*aa* *shaa*li*h*an wa**a**lla*dz*iina *aa*manuu ma'ahu bira*h*matin minn*aa* wamin khizyi yawmi-i*dz*in inna rabbaka huwa **a**lqawiyyu

Maka ketika keputusan Kami datang, Kami selamatkan Saleh dan orang-orang yang beriman bersamanya dengan rahmat Kami dan (Kami selamatkan) dari kehinaan pada hari itu. Sungguh, Tuhanmu, Dia Mahakuat, Mahaperkasa.

11:67

# وَاَخَذَ الَّذِيْنَ ظَلَمُوا الصَّيْحَةُ فَاَصْبَحُوْا فِيْ دِيَارِهِمْ جٰثِمِيْنَۙ

wa-akha*dz*a **al**la*dz*iina *zh*alamuu **al***shsh*ay*h*atu fa-a*sh*ba*h*uu fii diy*aa*rihim j*aa*tsimiin**a**

Kemudian suara yang mengguntur menimpa orang-orang zalim itu, sehingga mereka mati bergelimpangan di rumahnya.

11:68

# كَاَنْ لَّمْ يَغْنَوْا فِيْهَا ۗ اَلَآ اِنَّ ثَمُوْدَا۠ كَفَرُوْا رَبَّهُمْ ۗ اَلَا بُعْدًا لِّثَمُوْدَ ࣖ

ka-an lam yaghnaw fiih*aa* **a**l*aa* inna tsamuuda kafaruu rabbahum **a**l*aa* bu'dan litsamuud**a**

Seolah-olah mereka belum pernah tinggal di tempat itu. Ingatlah, kaum samud mengingkari Tuhan mereka. Ingatlah, binasalah kaum samud.

11:69

# وَلَقَدْ جَاۤءَتْ رُسُلُنَآ اِبْرٰهِيْمَ بِالْبُشْرٰى قَالُوْا سَلٰمًا ۖقَالَ سَلٰمٌ فَمَا لَبِثَ اَنْ جَاۤءَ بِعِجْلٍ حَنِيْذٍ

walaqad j*aa*-at rusulun*aa* ibr*aa*hiima bi**a**lbusyr*aa* q*aa*luu sal*aa*man q*aa*la sal*aa*mun fam*aa* labitsa an j*aa*-a bi'ijlin *h*anii*dz***in**

Dan para utusan Kami (para malaikat) telah datang kepada Ibrahim dengan membawa kabar gembira, mereka mengucapkan, “Selamat.” Dia (Ibrahim) menjawab, “Selamat (atas kamu).” Ma-ka tidak lama kemudian Ibrahim menyuguhkan daging anak sapi yang dipanggang.

11:70

# فَلَمَّا رَاٰىٓ اَيْدِيَهُمْ لَا تَصِلُ اِلَيْهِ نَكِرَهُمْ وَاَوْجَسَ مِنْهُمْ خِيْفَةً ۗقَالُوْا لَا تَخَفْ اِنَّآ اُرْسِلْنَآ اِلٰى قَوْمِ لُوْطٍۗ

falamm*aa* ra*aa* aydiyahum l*aa* ta*sh*ilu ilayhi nakirahum wa-awjasa minhum khiifatan q*aa*luu l*aa* takhaf inn*aa* ursiln*aa* il*aa* qawmi luu*th***in**

Maka ketika dilihatnya tangan mereka tidak menjamahnya, dia (Ibrahim) mencurigai mereka, dan merasa takut kepada mereka. Mereka (malaikat) berkata, “Jangan takut, sesungguhnya kami diutus kepada kaum Lut.”

11:71

# وَامْرَاَتُهٗ قَاۤىِٕمَةٌ فَضَحِكَتْ فَبَشَّرْنٰهَا بِاِسْحٰقَۙ وَمِنْ وَّرَاۤءِ اِسْحٰقَ يَعْقُوْبَ

wa**i**mra-atuhu q*aa*-imatun fa*dh*a*h*ikat fabasysyarn*aa*h*aa* bi-is*haa*qa wamin war*aa*-i is*haa*qa ya'quub**a**

Dan istrinya berdiri lalu dia tersenyum. Maka Kami sampaikan kepadanya kabar gembira tentang (kelahiran) Ishak dan setelah Ishak (akan lahir) Yakub.

11:72

# قَالَتْ يٰوَيْلَتٰىٓ ءَاَلِدُ وَاَنَا۠ عَجُوْزٌ وَّهٰذَا بَعْلِيْ شَيْخًا ۗاِنَّ هٰذَا لَشَيْءٌ عَجِيْبٌ

q*aa*lat y*aa* waylat*aa* a-alidu wa-an*aa* 'ajuuzun wah*aadzaa* ba'lii syaykhan inna h*aadzaa* lasyay-un 'ajiib**un**

Dia (istrinya) berkata, “Sungguh ajaib, mungkinkah aku akan melahirkan anak padahal aku sudah tua, dan suamiku ini sudah sangat tua? Ini benar-benar sesuatu yang ajaib.”

11:73

# قَالُوْٓا اَتَعْجَبِيْنَ مِنْ اَمْرِ اللّٰهِ رَحْمَتُ اللّٰهِ وَبَرَكٰتُهٗ عَلَيْكُمْ اَهْلَ الْبَيْتِۗ اِنَّهُ حَمِيْدٌ مَّجِيْدٌ

q*aa*luu ata'jabiina min amri **al**l*aa*hi ra*h*matu **al**l*aa*hi wabarak*aa*tuhu 'alaykum ahla **a**lbayti innahu *h*amiidun majiid**un**

Mereka (para malaikat) berkata, “Mengapa engkau merasa heran tentang ketetapan Allah? (Itu adalah) rahmat dan berkah Allah, dicurahkan kepada kamu, wahai ahlulbait! Sesungguhnya Allah Maha Terpuji, Maha Pengasih.”

11:74

# فَلَمَّا ذَهَبَ عَنْ اِبْرٰهِيْمَ الرَّوْعُ وَجَاۤءَتْهُ الْبُشْرٰى يُجَادِلُنَا فِيْ قَوْمِ لُوْطٍ

falamm*aa* *dz*ahaba 'an ibr*aa*hiima **al**rraw'u waj*aa*-at-hu **a**lbusyr*aa* yuj*aa*dilun*aa* fii qawmi luu*th***in**

Maka ketika rasa takut hilang dari Ibrahim dan kabar gembira telah datang kepadanya, dia pun bersoal jawab dengan (para malaikat) Kami tentang kaum Lut.

11:75

# اِنَّ اِبْرٰهِيْمَ لَحَلِيْمٌ اَوَّاهٌ مُّنِيْبٌ

inna ibr*aa*hiima la*h*aliimun aww*aa*hun muniib**un**

Ibrahim sungguh penyantun, lembut hati dan suka kembali (kepada Allah).

11:76

# يٰٓاِبْرٰهِيْمُ اَعْرِضْ عَنْ هٰذَا ۚاِنَّهٗ قَدْ جَاۤءَ اَمْرُ رَبِّكَۚ وَاِنَّهُمْ اٰتِيْهِمْ عَذَابٌ غَيْرُ مَرْدُوْدٍ

y*aa* ibr*aa*hiimu a'ri*dh* 'an h*aadzaa* innahu qad j*aa*-a amru rabbika wa-innahum *aa*tiihim 'a*dzaa*bun ghayru marduud**in**

Wahai Ibrahim! Tinggalkanlah (perbincangan) ini, sungguh, ketetapan Tuhanmu telah datang, dan mereka itu akan ditimpa azab yang tidak dapat ditolak.

11:77

# وَلَمَّا جَاۤءَتْ رُسُلُنَا لُوْطًا سِيْۤءَ بِهِمْ وَضَاقَ بِهِمْ ذَرْعًا وَّقَالَ هٰذَا يَوْمٌ عَصِيْبٌ

walamm*aa* j*aa*-at rusulun*aa* luu*th*an sii-a bihim wa*daa*qa bihim *dz*ar'an waq*aa*la h*aadzaa* yawmun 'a*sh*iib**un**

Dan ketika para utusan Kami (para malaikat) itu datang kepada Lut, dia merasa curiga dan dadanya merasa sempit karena (kedatangan)nya. Dia (Lut) berkata, “Ini hari yang sangat sulit.”

11:78

# وَجَاۤءَهٗ قَوْمُهٗ يُهْرَعُوْنَ اِلَيْهِۗ وَمِنْ قَبْلُ كَانُوْا يَعْمَلُوْنَ السَّيِّاٰتِۗ قَالَ يٰقَوْمِ هٰٓؤُلَاۤءِ بَنَاتِيْ هُنَّ اَطْهَرُ لَكُمْ فَاتَّقُوا اللّٰهَ وَلَا تُخْزُوْنِ فِيْ ضَيْفِيْۗ اَلَيْسَ مِنْكُمْ رَجُلٌ رَّشِيْدٌ

waj*aa*-ahu qawmuhu yuhra'uuna ilayhi wamin qablu k*aa*nuu ya'maluuna **al**ssayyi-*aa*ti q*aa*la y*aa* qawmi h*aa*ul*aa*-i ban*aa*tii hunna a*th*haru lakum fa**i**ttaquu **al**

**Dan kaumnya segera datang kepadanya. Dan sejak dahulu mereka selalu melakukan perbuatan keji. Lut berkata, “Wahai kaumku! Inilah putri-putri (negeri)ku mereka lebih suci bagimu, maka bertakwalah kepada Allah dan janganlah kamu mencemarkan (nama)ku terhada**









11:79

# قَالُوْا لَقَدْ عَلِمْتَ مَا لَنَا فِيْ بَنٰتِكَ مِنْ حَقٍّۚ وَاِنَّكَ لَتَعْلَمُ مَا نُرِيْدُ

q*aa*luu laqad 'alimta m*aa* lan*aa* fii ban*aa*tika min *h*aqqin wa-innaka lata'lamu m*aa* nuriid**u**

Mereka menjawab, “Sesungguhnya eng-kau pasti tahu bahwa kami tidak mempunyai keinginan (syahwat) terhadap putri-putrimu; dan engkau tentu mengetahui apa yang (sebenarnya) kami kehendaki.”

11:80

# قَالَ لَوْ اَنَّ لِيْ بِكُمْ قُوَّةً اَوْ اٰوِيْٓ اِلٰى رُكْنٍ شَدِيْدٍ

q*aa*la law anna lii bikum quwwatan aw *aa*wii il*aa* ruknin syadiid**in**

Dia (Lut) berkata, “Sekiranya aku mempunyai kekuatan (untuk menolakmu) atau aku dapat berlindung kepada keluarga yang kuat (tentu aku lakukan).”

11:81

# قَالُوْا يٰلُوْطُ اِنَّا رُسُلُ رَبِّكَ لَنْ يَّصِلُوْٓا اِلَيْكَ فَاَسْرِ بِاَهْلِكَ بِقِطْعٍ مِّنَ الَّيْلِ وَلَا يَلْتَفِتْ مِنْكُمْ اَحَدٌ اِلَّا امْرَاَتَكَۗ اِنَّهٗ مُصِيْبُهَا مَآ اَصَابَهُمْ ۗاِنَّ مَوْعِدَهُمُ الصُّبْحُ ۗ اَلَيْسَ الصُّبْحُ بِق

q*aa*luu y*aa* luu*th*u inn*aa* rusulu rabbika lan ya*sh*iluu ilayka fa-asri bi-ahlika biqi*th*'in mina **al**layli wal*aa* yaltafit minkum a*h*adun ill*aa* imra-ataka innahu mu*sh*iibuh*aa<*

Mereka (para malaikat) berkata, “Wahai Lut! Sesungguhnya kami adalah para utusan Tuhanmu, mereka tidak akan dapat mengganggu kamu, sebab itu pergilah beserta keluargamu pada akhir malam dan jangan ada seorang pun di antara kamu yang menoleh ke belakang, k







11:82

# فَلَمَّا جَاۤءَ اَمْرُنَا جَعَلْنَا عَالِيَهَا سَافِلَهَا وَاَمْطَرْنَا عَلَيْهَا حِجَارَةً مِّنْ سِجِّيْلٍ مَّنْضُوْدٍ

falamm*aa* j*aa*-a amrun*aa* ja'aln*aa* '*aa*liyah*aa* s*aa*filah*aa* wa-am*th*arn*aa* 'alayh*aa* *h*ij*aa*ratan min sijjiilin man*dh*uud**in**

Maka ketika keputusan Kami datang, Kami menjungkirbalikkannya negeri kaum Lut, dan Kami hujani mereka bertubi-tubi dengan batu dari tanah yang terbakar,

11:83

# مُسَوَّمَةً عِنْدَ رَبِّكَۗ وَمَا هِيَ مِنَ الظّٰلِمِيْنَ بِبَعِيْدٍ ࣖ

musawwamatan 'inda rabbika wam*aa* hiya mina **al***zhzhaa*limiina biba'iid**in**

yang diberi tanda oleh Tuhanmu. Dan siksaan itu tiadalah jauh dari orang yang zalim.

11:84

# ۞ وَاِلٰى مَدْيَنَ اَخَاهُمْ شُعَيْبًا ۗقَالَ يٰقَوْمِ اعْبُدُوا اللّٰهَ مَا لَكُمْ مِّنْ اِلٰهٍ غَيْرُهٗ ۗوَلَا تَنْقُصُوا الْمِكْيَالَ وَالْمِيْزَانَ اِنِّيْٓ اَرٰىكُمْ بِخَيْرٍ وَّاِنِّيْٓ اَخَافُ عَلَيْكُمْ عَذَابَ يَوْمٍ مُّحِيْطٍ

wa-il*aa* madyana akh*aa*hum syu'ayban q*aa*la y*aa* qawmi u'buduu **al**l*aa*ha m*aa* lakum min il*aa*hin ghayruhu wal*aa* tanqu*sh*uu **a**lmiky*aa*la wa**a**lmiiz<

Dan kepada (penduduk) Madyan (Kami utus) saudara mereka, Syuaib. Dia berkata, “Wahai kaumku! Sembahlah Allah, tidak ada tuhan bagimu selain Dia. Dan janganlah kamu kurangi takaran dan timbangan. Sesungguhnya aku melihat kamu dalam keadaan yang baik (makmu

11:85

# وَيٰقَوْمِ اَوْفُوا الْمِكْيَالَ وَالْمِيْزَانَ بِالْقِسْطِ وَلَا تَبْخَسُوا النَّاسَ اَشْيَاۤءَهُمْ وَلَا تَعْثَوْا فِى الْاَرْضِ مُفْسِدِيْنَ

way*aa* qawmi awfuu **a**lmiky*aa*la wa**a**lmiiz*aa*na bi**a**lqis*th*i wal*aa* tabkhasuu **al**nn*aa*sa asyy*aa*-ahum wal*aa* ta'tsaw fii **a**l-ar<

Dan wahai kaumku! Penuhilah takaran dan timbangan dengan adil, dan janganlah kamu merugikan manusia terhadap hak-hak mereka dan jangan kamu membuat kejahatan di bumi dengan berbuat kerusakan.

11:86

# بَقِيَّتُ اللّٰهِ خَيْرٌ لَّكُمْ اِنْ كُنْتُمْ مُّؤْمِنِيْنَ ەۚ وَمَآ اَنَا۠ عَلَيْكُمْ بِحَفِيْظٍ

baqiyyatu **al**l*aa*hi khayrun lakum in kuntum mu/miniina wam*aa* an*aa* 'alaykum bi*h*afii*zh***in**

Sisa (yang halal) dari Allah adalah lebih baik bagimu jika kamu orang yang beriman. Dan aku bukanlah seorang penjaga atas dirimu.”

11:87

# قَالُوْا يٰشُعَيْبُ اَصَلٰوتُكَ تَأْمُرُكَ اَنْ نَّتْرُكَ مَا يَعْبُدُ اٰبَاۤؤُنَآ اَوْ اَنْ نَّفْعَلَ فِيْٓ اَمْوَالِنَا مَا نَشٰۤؤُا ۗاِنَّكَ لَاَنْتَ الْحَلِيْمُ الرَّشِيْدُ

q*aa*luu y*aa* syu'aybu a*sh*al*aa*tuka ta/muruka an natruka m*aa* ya'budu *aa*b*aa*un*aa* aw an naf'ala fii amw*aa*lin*aa* m*aa* nasy*aa*u innaka la-anta **a**l*h*aliimu

Mereka berkata, “Wahai Syuaib! Apakah agamamu yang menyuruhmu agar kami meninggalkan apa yang disembah nenek moyang kami atau melarang kami mengelola harta kami menurut cara yang kami kehendaki? Sesungguhnya engkau benar-benar orang yang sangat penyantun

11:88

# قَالَ يٰقَوْمِ اَرَءَيْتُمْ اِنْ كُنْتُ عَلٰى بَيِّنَةٍ مِّنْ رَّبِّيْ وَرَزَقَنِيْ مِنْهُ رِزْقًا حَسَنًا وَّمَآ اُرِيْدُ اَنْ اُخَالِفَكُمْ اِلٰى مَآ اَنْهٰىكُمْ عَنْهُ ۗاِنْ اُرِيْدُ اِلَّا الْاِصْلَاحَ مَا اسْتَطَعْتُۗ وَمَا تَوْفِيْقِيْٓ اِلَّا بِا

q*aa*la y*aa* qawmi ara-aytum in kuntu 'al*aa* bayyinatin min rabbii warazaqanii minhu rizqan *h*asanan wam*aa* uriidu an ukh*aa*lifakum il*aa* m*aa* anh*aa*kum 'anhu in uriidu ill*aa* **a**l-

Dia (Syuaib) berkata, “Wahai kaumku! Terangkan padaku jika aku mempunyai bukti yang nyata dari Tuhanku dan aku dianugerahi-Nya rezeki yang baik (pantaskah aku menyalahi perintah-Nya)? Aku tidak bermaksud menyalahi kamu terhadap apa yang aku larang darinya

11:89

# وَيٰقَوْمِ لَا يَجْرِمَنَّكُمْ شِقَاقِيْٓ اَنْ يُّصِيْبَكُمْ مِّثْلُ مَآ اَصَابَ قَوْمَ نُوْحٍ اَوْ قَوْمَ هُوْدٍ اَوْ قَوْمَ صٰلِحٍ ۗوَمَا قَوْمُ لُوْطٍ مِّنْكُمْ بِبَعِيْدٍ

way*aa* qawmi l*aa* yajrimannakum syiq*aa*qii an yu*sh*iibakum mitslu m*aa* a*shaa*ba qawma nuu*h*in aw qawma huudin aw qawma *shaa*li*h*in wam*aa* qawmu luu*th*in minkum biba'iid**in**

Dan wahai kaumku! Janganlah pertentangan antara aku (dengan kamu) menyebabkan kamu berbuat dosa, sehingga kamu ditimpa siksaan seperti yang menimpa kaum Nuh, kaum Hud atau kaum Saleh, sedang kaum Lut tidak jauh dari kamu.

11:90

# وَاسْتَغْفِرُوْا رَبَّكُمْ ثُمَّ تُوْبُوْٓا اِلَيْهِ ۗاِنَّ رَبِّيْ رَحِيْمٌ وَّدُوْدٌ

wa**i**staghfiruu rabbakum tsumma tuubuu ilayhi inna rabbii ra*h*iimun waduud**un**

Dan mohonlah ampunan kepada Tuhanmu, kemudian bertobatlah kepada-Nya. Sungguh, Tuhanku Maha Penyayang, Maha Pengasih.”

11:91

# قَالُوْا يٰشُعَيْبُ مَا نَفْقَهُ كَثِيْرًا مِّمَّا تَقُوْلُ وَاِنَّا لَنَرٰىكَ فِيْنَا ضَعِيْفًا ۗوَلَوْلَا رَهْطُكَ لَرَجَمْنٰكَ ۖوَمَآ اَنْتَ عَلَيْنَا بِعَزِيْزٍ

q*aa*luu y*aa* syu'aybu m*aa* nafqahu katsiiran mimm*aa* taquulu wa-inn*aa* lanar*aa*ka fiin*aa* *dh*a'iifan walawl*aa* rah*th*uka larajamn*aa*ka wam*aa* anta 'alayn*aa* bi'aziiz**in**

**Mereka berkata, “Wahai Syuaib! Kami tidak banyak mengerti tentang apa yang engkau katakan itu, sedang kenyataannya kami memandang engkau seorang yang lemah di antara kami. Kalau tidak karena keluargamu, tentu kami telah merajam engkau, sedang engkau pun b**









11:92

# قَالَ يٰقَوْمِ اَرَهْطِيْٓ اَعَزُّ عَلَيْكُمْ مِّنَ اللّٰهِ ۗوَاتَّخَذْتُمُوْهُ وَرَاۤءَكُمْ ظِهْرِيًّا ۗاِنَّ رَبِّيْ بِمَا تَعْمَلُوْنَ مُحِيْطٌ

q*aa*la y*aa* qawmi arah*th*ii a'azzu 'alaykum mina **al**l*aa*hi wa**i**ttakha*dz*tumuuhu war*aa*-akum *zh*ihriyyan inna rabbii bim*aa* ta'maluuna mu*h*ii*th***un**

Dia (Syuaib) menjawab, “Wahai kaumku! Apakah keluargaku lebih terhormat menurut pandanganmu daripada Allah, bahkan Dia kamu tempatkan di belakangmu (diabaikan)? Ketahuilah sesungguhnya (pengetahuan) Tuhanku meliputi apa yang kamu kerjakan.

11:93

# وَيٰقَوْمِ اعْمَلُوْا عَلٰى مَكَانَتِكُمْ اِنِّيْ عَامِلٌ ۗسَوْفَ تَعْلَمُوْنَۙ مَنْ يَّأْتِيْهِ عَذَابٌ يُّخْزِيْهِ وَمَنْ هُوَ كَاذِبٌۗ وَارْتَقِبُوْٓا اِنِّيْ مَعَكُمْ رَقِيْبٌ

way*aa* qawmi i'maluu 'al*aa* mak*aa*natikum innii '*aa*milun sawfa ta'lamuuna man ya/tiihi 'a*dzaa*bun yukhziihi waman huwa k*aadz*ibun wa**i**rtaqibuu inne ma'akum raqiib**un**

Dan wahai kaumku! Berbuatlah menurut kemampuanmu, sesungguhnya aku pun berbuat (pula). Kelak kamu akan mengetahui siapa yang akan ditimpa azab yang menghinakan dan siapa yang berdusta. Dan tunggulah! Sesungguhnya aku bersamamu adalah orang yang menunggu.”

11:94

# وَلَمَّا جَاۤءَ اَمْرُنَا نَجَّيْنَا شُعَيْبًا وَّالَّذِيْنَ اٰمَنُوْا مَعَهٗ بِرَحْمَةٍ مِّنَّاۚ وَاَخَذَتِ الَّذِيْنَ ظَلَمُوا الصَّيْحَةُ فَاَصْبَحُوْا فِيْ دِيَارِهِمْ جٰثِمِيْنَۙ

walamm*aa* j*aa*-a amrun*aa* najjayn*aa* syu'ayban wa**a**lla*dz*iina *aa*manuu ma'ahu bira*h*matin minn*aa* wa-akha*dz*ati **al**la*dz*iina *zh*alamuu **al**

Maka ketika keputusan Kami datang, Kami selamatkan Syuaib dan orang-orang yang beriman bersamanya dengan rahmat Kami. Sedang orang yang zalim dibinasakan oleh suara yang mengguntur, sehingga mereka mati bergelimpangan di rumahnya,







11:95

# كَاَنْ لَّمْ يَغْنَوْا فِيْهَا ۗ اَلَا بُعْدًا لِّمَدْيَنَ كَمَا بَعِدَتْ ثَمُوْدُ ࣖ

ka-an lam yaghnaw fiih*aa* **a**l*aa* bu'dan limadyana kam*aa* ba'idat tsamuud**u**

seolah-olah mereka belum pernah tinggal di tempat itu. Ingatlah, binasalah penduduk Madyan sebagaimana kaum samud (juga) telah binasa.

11:96

# وَلَقَدْ اَرْسَلْنَا مُوْسٰى بِاٰيٰتِنَا وَسُلْطٰنٍ مُّبِيْنٍۙ

walaqad arsaln*aa* muus*aa* bi-*aa*y*aa*tin*aa* wasul*thaa*nin mubiin**in**

Dan sungguh, Kami telah mengutus Musa dengan tanda-tanda (kekuasaan) Kami dan bukti yang nyata,

11:97

# اِلٰى فِرْعَوْنَ وَملَا۟ىِٕهٖ فَاتَّبَعُوْٓا اَمْرَ فِرْعَوْنَ ۚوَمَآ اَمْرُ فِرْعَوْنَ بِرَشِيْدٍ

il*aa* fir'awna wamala-ihi fa**i**ttaba'uu amra fir'awna wam*aa* amru fir'awna birasyiid**in**

Kepada Fir‘aun dan para pemuka kaumnya, tetapi mereka mengikuti perintah Fir‘aun, padahal perintah Fir‘aun bukanlah (perintah) yang benar.

11:98

# يَقْدُمُ قَوْمَهٗ يَوْمَ الْقِيٰمَةِ فَاَوْرَدَهُمُ النَّارَ ۗوَبِئْسَ الْوِرْدُ الْمَوْرُوْدُ

yaqdumu qawmahu yawma **a**lqiy*aa*mati fa-awradahumu **al**nn*aa*ra wabi/sa **a**lwirdu **a**lmawruud**u**

Dia (Fir‘aun) berjalan di depan kaumnya di hari Kiamat, lalu membawa mereka masuk ke dalam neraka. Neraka itu seburuk-buruk tempat yang dimasuki.

11:99

# وَاُتْبِعُوْا فِيْ هٰذِهٖ لَعْنَةً وَّيَوْمَ الْقِيٰمَةِۗ بِئْسَ الرِّفْدُ الْمَرْفُوْدُ

wautbi'uu fii h*aadz*ihi la'natan wayawma **a**lqiy*aa*mati bi/sa **al**rrifdu **a**lmarfuud**u**

Dan mereka diikuti dengan laknat di sini (dunia) dan (begitu pula) pada hari Kiamat. (Laknat) itu seburuk-buruk pemberian yang diberikan.

11:100

# ذٰلِكَ مِنْ اَنْۢبَاۤءِ الْقُرٰى نَقُصُّهٗ عَلَيْكَ مِنْهَا قَاۤىِٕمٌ وَّحَصِيْدٌ

*dzaa*lika min anb*aa*-i **a**lqur*aa* naqu*shsh*uhu 'alayka minh*aa* q*aa*-imun wa*h*a*sh*iid**un**

Itulah beberapa berita tentang negeri-negeri (yang telah dibinasakan) yang Kami ceritakan kepadamu (Muhammad). Di antara negeri-negeri itu sebagian masih ada bekas-bekasnya dan ada (pula) yang telah musnah.

11:101

# وَمَا ظَلَمْنٰهُمْ وَلٰكِنْ ظَلَمُوْٓا اَنْفُسَهُمْ فَمَآ اَغْنَتْ عَنْهُمْ اٰلِهَتُهُمُ الَّتِيْ يَدْعُوْنَ مِنْ دُوْنِ اللّٰهِ مِنْ شَيْءٍ لَّمَّا جَاۤءَ اَمْرُ رَبِّكَۗ وَمَا زَادُوْهُمْ غَيْرَ تَتْبِيْبٍ

wam*aa* *zh*alamn*aa*hum wal*aa*kin *zh*alamuu anfusahum fam*aa* aghnat 'anhum *aa*lihatuhumu **al**latii yad'uuna min duuni **al**l*aa*hi min syay-in lamm*aa* j*aa*-a amru rabbik

Dan Kami tidak menzalimi mereka, tetapi merekalah yang menzalimi diri mereka sendiri, karena itu tidak bermanfaat sedikit pun bagi mereka sesembahan yang mereka sembah selain Allah, ketika siksaan Tuhanmu datang. Sesembahan itu hanya menambah kebinasaan b

11:102

# وَكَذٰلِكَ اَخْذُ رَبِّكَ اِذَآ اَخَذَ الْقُرٰى وَهِيَ ظَالِمَةٌ ۗاِنَّ اَخْذَهٗٓ اَلِيْمٌ شَدِيْدٌ

waka*dzaa*lika akh*dz*u rabbika i*dzaa* akha*dz*a **a**lqur*aa* wahiya *zhaa*limatun inna akh*dz*ahu **a**liimun syadiid**un**

Dan begitulah siksa Tuhanmu apabila Dia menyiksa (penduduk) negeri-negeri yang berbuat zalim. Sungguh, siksa-Nya sangat pedih, sangat berat.

11:103

# اِنَّ فِيْ ذٰلِكَ لَاٰيَةً لِّمَنْ خَافَ عَذَابَ الْاٰخِرَةِ ۗذٰلِكَ يَوْمٌ مَّجْمُوْعٌۙ لَّهُ النَّاسُ وَذٰلِكَ يَوْمٌ مَّشْهُوْدٌ

inna fii *dzaa*lika la*aa*yatan liman kh*aa*fa 'a*dzaa*ba **a**l-*aa*khirati *dzaa*lika yawmun majmuu'un lahu **al**nn*aa*su wa*dzaa*lika yawmun masyhuudun

Sesungguhnya pada yang demikian itu pasti terdapat pelajaran bagi orang-orang yang takut kepada azab akhirat. Itulah hari ketika semua manusia dikumpulkan (untuk dihisab), dan itulah hari yang disaksikan (oleh semua makhluk).

11:104

# وَمَا نُؤَخِّرُهٗٓ اِلَّا لِاَجَلٍ مَّعْدُوْدٍۗ

wam*aa* nu-akhkhiruhu ill*aa* li-ajalin ma'duud**in**

Dan Kami tidak akan menunda, kecuali sampai waktu yang sudah ditentukan.

11:105

# يَوْمَ يَأْتِ لَا تَكَلَّمُ نَفْسٌ اِلَّا بِاِذْنِهٖۚ فَمِنْهُمْ شَقِيٌّ وَّسَعِيْدٌ

yawma ya/ti l*aa* takallamu nafsun ill*aa* bi-i*dz*nihi faminhum syaqiyyun wasa'iid**in**

Ketika hari itu datang, tidak seorang pun yang berbicara, kecuali dengan izin-Nya; maka di antara mereka ada yang sengsara dan ada yang berbahagia.

11:106

# فَاَمَّا الَّذِيْنَ شَقُوْا فَفِى النَّارِ لَهُمْ فِيْهَا زَفِيْرٌ وَّشَهِيْقٌۙ

fa-amm*aa* **al**la*dz*iina syaquu fafii **al**nn*aa*ri lahum fiih*aa* zafiirun wasyahiiq**un**

Maka adapun orang-orang yang sengsara, maka (tempatnya) di dalam neraka, di sana mereka mengeluarkan dan menarik nafas dengan merintih,

11:107

# خٰلِدِيْنَ فِيْهَا مَا دَامَتِ السَّمٰوٰتُ وَالْاَرْضُ اِلَّا مَا شَاۤءَ رَبُّكَۗ اِنَّ رَبَّكَ فَعَّالٌ لِّمَا يُرِيْدُ

kh*aa*lidiina fiih*aa* m*aa* d*aa*mati **al**ssam*aa*w*aa*tu wa**a**l-ar*dh*u ill*aa* m*aa* sy*aa*-a rabbuka inna rabbaka fa''*aa*lun lim*aa* yuriid**u**

mereka kekal di dalamnya selama ada langit dan bumi, kecuali jika Tuhanmu menghendaki (yang lain). Sungguh, Tuhanmu Maha Pelaksana terhadap apa yang Dia kehendaki.

11:108

# ۞ وَاَمَّا الَّذِيْنَ سُعِدُوْا فَفِى الْجَنَّةِ خٰلِدِيْنَ فِيْهَا مَا دَامَتِ السَّمٰوٰتُ وَالْاَرْضُ اِلَّا مَا شَاۤءَ رَبُّكَۗ عَطَاۤءً غَيْرَ مَجْذُوْذٍ

wa-amm*aa* **al**la*dz*iina su'iduu fafii **a**ljannati kh*aa*lidiina fiih*aa* m*aa* d*aa*mati **al**ssam*aa*w*aa*tu wa**a**l-ar*dh*u ill*aa* m*aa*

Dan adapun orang-orang yang berbahagia, maka (tempatnya) di dalam surga; mereka kekal di dalamnya selama ada langit dan bumi, kecuali jika Tuhanmu menghendaki (yang lain); sebagai karunia yang tidak ada putus-putusnya.

11:109

# فَلَا تَكُ فِيْ مِرْيَةٍ مِّمَّا يَعْبُدُ هٰٓؤُلَاۤءِ ۗمَا يَعْبُدُوْنَ اِلَّا كَمَا يَعْبُدُ اٰبَاۤؤُهُمْ مِّنْ قَبْلُ ۗوَاِنَّا لَمُوَفُّوْهُمْ نَصِيْبَهُمْ غَيْرَ مَنْقُوْصٍ ࣖ

fal*aa* taku fii miryatin mimm*aa* ya'budu h*aa*ul*aa*-i m*aa* ya'buduuna ill*aa* kam*aa* ya'budu *aa*b*aa*uhum min qablu wa-inn*aa* lamuwaffuuhum na*sh*iibahum ghayra manquu*sh***in**

**Maka janganlah engkau (Muhammad) ragu-ragu tentang apa yang mereka sembah. Mereka menyembah sebagaimana nenek moyang mereka dahulu menyembah. Kami pasti akan menyempurnakan pembalasan (terhadap) mereka tanpa dikurangi sedikit pun.**









11:110

# وَلَقَدْ اٰتَيْنَا مُوْسَى الْكِتٰبَ فَاخْتُلِفَ فِيْهِ ۗوَلَوْلَا كَلِمَةٌ سَبَقَتْ مِنْ رَّبِّكَ لَقُضِيَ بَيْنَهُمْ ۚوَاِنَّهُمْ لَفِيْ شَكٍّ مِّنْهُ مُرِيْبٍ

walaqad *aa*tayn*aa* muus*aa* **a**lkit*aa*ba fa**i**khtulifa fiihi walawl*aa* kalimatun sabaqat min rabbika laqu*dh*iya baynahum wa-innahum lafii syakkin minhu muriib**un**

Dan sungguh, Kami telah memberikan Kitab (Taurat) kepada Musa, lalu diperselisihkannya. Dan kalau tidak ada ketetapan yang terdahulu dari Tuhanmu, niscaya telah dilaksanakan hukuman di antara mereka. Sungguh, mereka (orang kafir Mekah) benar-benar dalam k

11:111

# وَاِنَّ كُلًّا لَّمَّا لَيُوَفِّيَنَّهُمْ رَبُّكَ اَعْمَالَهُمْ ۗاِنَّهٗ بِمَا يَعْمَلُوْنَ خَبِيْرٌ

wa-inna kullan lamm*aa* layuwaffiyannahum rabbuka a'm*aa*lahum innahu bim*aa* ya'maluuna khabiir**un**

Dan sesungguhnya kepada masing-masing (yang berselisih itu) pasti Tuhanmu akan memberi balasan secara penuh atas perbuatan mereka. Sungguh, Dia Mahateliti terhadap apa yang mereka kerjakan.

11:112

# فَاسْتَقِمْ كَمَآ اُمِرْتَ وَمَنْ تَابَ مَعَكَ وَلَا تَطْغَوْاۗ اِنَّهٗ بِمَا تَعْمَلُوْنَ بَصِيْرٌ

fa**i**staqim kam*aa* umirta waman t*aa*ba ma'aka wal*aa* ta*th*ghaw innahu bim*aa* ta'maluuna ba*sh*iir**un**

Maka tetaplah engkau (Muhammad) (di jalan yang benar), sebagaimana telah diperintahkan kepadamu dan (juga) orang yang bertobat bersamamu, dan janganlah kamu melampaui batas. Sungguh, Dia Maha Melihat apa yang kamu kerjakan.

11:113

# وَلَا تَرْكَنُوْٓا اِلَى الَّذِيْنَ ظَلَمُوْا فَتَمَسَّكُمُ النَّارُۙ وَمَا لَكُمْ مِّنْ دُوْنِ اللّٰهِ مِنْ اَوْلِيَاۤءَ ثُمَّ لَا تُنْصَرُوْنَ

wal*aa* tarkanuu il*aa* **al**la*dz*iina *zh*alamuu fatamassakumu **al**nn*aa*ru wam*aa* lakum min duuni **al**l*aa*hi min awliy*aa*-a tsumma l*aa* tun*sh*aruun

Dan janganlah kamu cenderung kepada orang yang zalim yang menyebabkan kamu disentuh api neraka, sedangkan kamu tidak mempunyai seorang penolong pun selain Allah, sehingga kamu tidak akan diberi pertolongan.

11:114

# وَاَقِمِ الصَّلٰوةَ طَرَفَيِ النَّهَارِ وَزُلَفًا مِّنَ الَّيْلِ ۗاِنَّ الْحَسَنٰتِ يُذْهِبْنَ السَّيِّاٰتِۗ ذٰلِكَ ذِكْرٰى لِلذَّاكِرِيْنَ

wa-aqimi **al***shsh*al*aa*ta *th*arafayi **al**nnah*aa*ri wazulafan mina **al**layli inna **a**l*h*asan*aa*ti yu*dz*hibna **al**ssayyi-*aa*ti *dzaa*

Dan laksanakanlah salat pada kedua ujung siang (pagi dan petang) dan pada bagian permulaan malam. Perbuatan-perbuatan baik itu menghapus kesalahan-kesalahan. Itulah peringatan bagi orang-orang yang selalu mengingat (Allah).







11:115

# وَاصْبِرْ فَاِنَّ اللّٰهَ لَا يُضِيْعُ اَجْرَ الْمُحْسِنِيْنَ

wa**i***sh*bir fa-inna **al**l*aa*ha l*aa* yu*dh*ii'u ajra **a**lmu*h*siniin**a**

Dan bersabarlah, karena sesungguhnya Allah tidak menyia-nyiakan pahala orang yang berbuat kebaikan.

11:116

# فَلَوْلَا كَانَ مِنَ الْقُرُوْنِ مِنْ قَبْلِكُمْ اُولُوْا بَقِيَّةٍ يَّنْهَوْنَ عَنِ الْفَسَادِ فِى الْاَرْضِ اِلَّا قَلِيْلًا مِّمَّنْ اَنْجَيْنَا مِنْهُمْ ۚوَاتَّبَعَ الَّذِيْنَ ظَلَمُوْا مَآ اُتْرِفُوْا فِيْهِ وَكَانُوْا مُجْرِمِيْنَ

falawl*aa* k*aa*na mina **a**lquruuni min qablikum uluu baqiyyatin yanhawna 'ani **a**lfas*aa*di fii **a**l-ar*dh*i ill*aa* qaliilan mimman anjayn*aa* minhum wa**i**ttaba'a

Maka mengapa tidak ada di antara umat-umat sebelum kamu orang yang mempunyai keutamaan yang melarang (berbuat) kerusakan di bumi, kecuali sebagian kecil di antara orang yang telah Kami selamatkan. Dan orang-orang yang zalim hanya mementingkan kenikmatan d

11:117

# وَمَا كَانَ رَبُّكَ لِيُهْلِكَ الْقُرٰى بِظُلْمٍ وَّاَهْلُهَا مُصْلِحُوْنَ

wam*aa* k*aa*na rabbuka liyuhlika **a**lqur*aa* bi*zh*ulmin wa-ahluh*aa* mu*sh*li*h*uun**a**

Dan Tuhanmu tidak akan membinasakan negeri-negeri secara zalim, selama penduduknya orang-orang yang berbuat kebaikan.

11:118

# وَلَوْ شَاۤءَ رَبُّكَ لَجَعَلَ النَّاسَ اُمَّةً وَّاحِدَةً وَّلَا يَزَالُوْنَ مُخْتَلِفِيْنَۙ

walaw sy*aa*-a rabbuka laja'ala **al**nn*aa*sa ummatan w*aah*idatan wal*aa* yaz*aa*luuna mukhtalifiin**a**

Dan jika Tuhanmu menghendaki, tentu Dia jadikan manusia umat yang satu, tetapi mereka senantiasa berselisih (pendapat),

11:119

# اِلَّا مَنْ رَّحِمَ رَبُّكَ ۗوَلِذٰلِكَ خَلَقَهُمْ ۗوَتَمَّتْ كَلِمَةُ رَبِّكَ لَاَمْلَـَٔنَّ جَهَنَّمَ مِنَ الْجِنَّةِ وَالنَّاسِ اَجْمَعِيْنَ

ill*aa* man ra*h*ima rabbuka wali*dzaa*lika khalaqahum watammat kalimatu rabbika la-amla-anna jahannama mina **a**ljinnati wa**al**nn*aa*si ajma'iin**a**

kecuali orang yang diberi rahmat oleh Tuhanmu. Dan untuk itulah Allah menciptakan mereka. Kalimat (keputusan) Tuhanmu telah tetap, “Aku pasti akan memenuhi neraka Jahanam dengan jin dan manusia (yang durhaka) semuanya.”

11:120

# وَكُلًّا نَّقُصُّ عَلَيْكَ مِنْ اَنْۢبَاۤءِ الرُّسُلِ مَا نُثَبِّتُ بِهٖ فُؤَادَكَ وَجَاۤءَكَ فِيْ هٰذِهِ الْحَقُّ وَمَوْعِظَةٌ وَّذِكْرٰى لِلْمُؤْمِنِيْنَ

wakullan naqu*shsh*u 'alayka min anb*aa*-i **al**rrusuli m*aa* nutsabbitu bihi fu-*aa*daka waj*aa*-aka fii h*aadz*ihi **a**l*h*aqqu wamaw'i*zh*atun wa*dz*ikr*aa* lilmu/miniin

Dan semua kisah rasul-rasul, Kami ceritakan kepadamu (Muhammad), agar dengan kisah itu Kami teguhkan hatimu; dan di dalamnya telah diberikan kepadamu (segala) kebenaran, nasihat dan peringatan bagi orang yang beriman.

11:121

# وَقُلْ لِّلَّذِيْنَ لَا يُؤْمِنُوْنَ اعْمَلُوْا عَلٰى مَكَانَتِكُمْۗ اِنَّا عٰمِلُوْنَۙ

waqul lilla*dz*iina l*aa* yu/minuuna i'maluu 'al*aa* mak*aa*natikum inn*aa* '*aa*miluun**a**

Dan katakanlah (Muhammad) kepada orang yang tidak beriman, “Berbuatlah menurut kedudukanmu; kami pun benar-benar akan berbuat,

11:122

# وَانْتَظِرُوْاۚ اِنَّا مُنْتَظِرُوْنَ

wa**i**nta*zh*iruu inn*aa* munta*zh*iruun**a**

dan tunggulah, sesungguhnya kami pun termasuk yang menunggu.”

11:123

# وَلِلّٰهِ غَيْبُ السَّمٰوٰتِ وَالْاَرْضِ وَاِلَيْهِ يُرْجَعُ الْاَمْرُ كُلُّهٗ فَاعْبُدْهُ وَتَوَكَّلْ عَلَيْهِۗ وَمَا رَبُّكَ بِغَافِلٍ عَمَّا تَعْمَلُوْنَ ࣖ

walill*aa*hi ghaybu **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i wa-ilayhi yurja'u **a**l-amru kulluhu fa**u**'budhu watawakkal 'alayhi wam*aa* rabbuka bigh*aa*filin 'amm*aa*

Dan milik Allah meliputi rahasia langit dan bumi dan kepada-Nya segala urusan dikembalikan. Maka sembahlah Dia dan bertawakallah kepada-Nya. Dan Tuhanmu tidak akan lengah terhadap apa yang kamu kerjakan.

<!--EndFragment-->