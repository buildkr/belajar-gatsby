---
title: (84) Al-Insyiqaq - الانشقاق
date: 2021-10-27T04:22:56.847Z
ayat: 84
description: "Jumlah Ayat: 25 / Arti: Terbelah"
---
<!--StartFragment-->

84:1

# اِذَا السَّمَاۤءُ انْشَقَّتْۙ

i*dzaa* **al**ssam*aa*u insyaqqath

Apabila langit terbelah,

84:2

# وَاَذِنَتْ لِرَبِّهَا وَحُقَّتْۙ

wa-a*dz*inat lirabbih*aa* wa*h*uqqath

dan patuh kepada Tuhannya, dan sudah semestinya patuh,

84:3

# وَاِذَا الْاَرْضُ مُدَّتْۙ

wa-i*dzaa* **a**l-ar*dh*u muddath

dan apabila bumi diratakan,

84:4

# وَاَلْقَتْ مَا فِيْهَا وَتَخَلَّتْۙ

wa-alqat m*aa* fiih*aa* watakhallath

dan memuntahkan apa yang ada di dalamnya dan menjadi kosong,

84:5

# وَاَذِنَتْ لِرَبِّهَا وَحُقَّتْۗ

wa-a*dz*inat lirabbih*aa* wa*h*uqqath

dan patuh kepada Tuhannya, dan sudah semestinya patuh.

84:6

# يٰٓاَيُّهَا الْاِنْسَانُ اِنَّكَ كَادِحٌ اِلٰى رَبِّكَ كَدْحًا فَمُلٰقِيْهِۚ

y*aa* ayyuh*aa* **a**l-ins*aa*nu innaka k*aa*di*h*un il*aa* rabbika kad*h*an famul*aa*qiih**i**

Wahai manusia! Sesungguhnya kamu telah bekerja keras menuju Tuhanmu, maka kamu akan menemui-Nya.

84:7

# فَاَمَّا مَنْ اُوْتِيَ كِتٰبَهٗ بِيَمِيْنِهٖۙ

fa-am*aa* man uutiya kit*aa*bahu biyamiinih**i**

Maka adapun orang yang catatannya diberikan dari sebelah kanannya,

84:8

# فَسَوْفَ يُحَاسَبُ حِسَابًا يَّسِيْرًاۙ

fasawfa yu*haa*sabu *h*is*aa*ban yasiir*aa***n**

maka dia akan diperiksa dengan pemeriksaan yang mudah,

84:9

# وَّيَنْقَلِبُ اِلٰٓى اَهْلِهٖ مَسْرُوْرًاۗ

wayanqalibu il*aa* ahlihi masruur*aa***n**

dan dia akan kembali kepada keluarganya (yang sama-sama beriman) dengan gembira.

84:10

# وَاَمَّا مَنْ اُوْتِيَ كِتٰبَهٗ وَرَاۤءَ ظَهْرِهٖۙ

wa-amm*aa* man uutiya kit*aa*bahu war*aa*-a *zh*ahrih**i**

Dan adapun orang yang catatannya diberikan dari sebelah belakang,

84:11

# فَسَوْفَ يَدْعُوْ ثُبُوْرًاۙ

fasawfa yad'uu tsubuur*aa***n**

maka dia akan berteriak, “Celakalah aku!”

84:12

# وَّيَصْلٰى سَعِيْرًاۗ

waya*sh*l*aa* sa'iir*aa***n**

Dan dia akan masuk ke dalam api yang menyala-nyala (neraka).

84:13

# اِنَّهٗ كَانَ فِيْٓ اَهْلِهٖ مَسْرُوْرًاۗ

innahu k*aa*na fii ahlihi masruur*aa***n**

Sungguh, dia dahulu (di dunia) bergembira di kalangan keluarganya (yang sama-sama kafir).

84:14

# اِنَّهٗ ظَنَّ اَنْ لَّنْ يَّحُوْرَ ۛ

innahu *zh*anna an lan ya*h*uur**a**

Sesungguhnya dia mengira bahwa dia tidak akan kembali (kepada Tuhannya).

84:15

# بَلٰىۛ اِنَّ رَبَّهٗ كَانَ بِهٖ بَصِيْرًاۗ

bal*aa* inna rabbahu k*aa*na bihi ba*sh*iir*aa***n**

Tidak demikian, sesungguhnya Tuhannya selalu melihatnya.

84:16

# فَلَآ اُقْسِمُ بِالشَّفَقِۙ

fal*aa* uqsimu bi**al**sysyafaq**i**

Maka Aku bersumpah demi cahaya merah pada waktu senja,

84:17

# وَالَّيْلِ وَمَا وَسَقَۙ

wa**a**llayli wam*aa* wasaq**a**

demi malam dan apa yang diselubunginya,

84:18

# وَالْقَمَرِ اِذَا اتَّسَقَۙ

wa**a**lqamari i*dzaa* ittasaq**a**

demi bulan apabila jadi purnama,

84:19

# لَتَرْكَبُنَّ طَبَقًا عَنْ طَبَقٍۗ

latarkabunna *th*abaqan 'an *th*abaq**in**

sungguh, akan kamu jalani tingkat demi tingkat (dalam kehidupan).

84:20

# فَمَا لَهُمْ لَا يُؤْمِنُوْنَۙ

fam*aa* lahum l*aa* yu/minuun**a**

Maka mengapa mereka tidak mau beriman?

84:21

# وَاِذَا قُرِئَ عَلَيْهِمُ الْقُرْاٰنُ لَا يَسْجُدُوْنَ ۗ ۩

wa-i*dzaa* quri-a 'alayhimu **a**lqur-*aa*nu l*aa* yasjuduun**a**

Dan apabila Al-Qur'an dibacakan kepada mereka, mereka tidak (mau) bersujud,

84:22

# بَلِ الَّذِيْنَ كَفَرُوْا يُكَذِّبُوْنَۖ

bali **al**la*dz*iina kafaruu yuka*dzdz*ibuun**a**

bahkan orang-orang kafir itu mendustakan(nya).

84:23

# وَاللّٰهُ اَعْلَمُ بِمَا يُوْعُوْنَۖ

wa**al**l*aa*hu a'lamu bim*aa* yuu'uun**a**

Dan Allah lebih mengetahui apa yang mereka sembunyikan (dalam hati mereka).

84:24

# فَبَشِّرْهُمْ بِعَذَابٍ اَلِيْمٍۙ

fabasysyirhum bi'a*dzaa*bin **a**liim**in**

Maka sampaikanlah kepada mereka (ancaman) azab yang pedih,

84:25

# اِلَّا الَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِ لَهُمْ اَجْرٌ غَيْرُ مَمْنُوْنٍ ࣖ

ill*aa* **al**la*dz*iina *aa*manuu wa'amiluu **al***shshaa*li*haa*ti lahum ajrun ghayru mamnuun**in**

kecuali orang-orang yang beriman dan mengerjakan kebajikan, mereka akan mendapat pahala yang tidak putus-putusnya.

<!--EndFragment-->