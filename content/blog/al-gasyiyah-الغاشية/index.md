---
title: (88) Al-Gasyiyah - الغاشية
date: 2021-10-27T04:25:58.796Z
ayat: 88
description: "Jumlah Ayat: 26 / Arti: Hari Kiamat"
---
<!--StartFragment-->

88:1

# هَلْ اَتٰىكَ حَدِيْثُ الْغَاشِيَةِۗ

hal at*aa*ka *h*adiitsu **a**lgh*aa*syiyat**i**

Sudahkah sampai kepadamu berita tentang (hari Kiamat)?

88:2

# وُجُوْهٌ يَّوْمَىِٕذٍ خَاشِعَةٌ ۙ

wujuuhun yawma-i*dz*in kh*aa*syi'a**tun**

Pada hari itu banyak wajah yang tertunduk terhina,

88:3

# عَامِلَةٌ نَّاصِبَةٌ ۙ

'*aa*milatun n*aas*iba**tun**

(karena) bekerja keras lagi kepayahan,

88:4

# تَصْلٰى نَارًا حَامِيَةً ۙ

ta*sh*l*aa* n*aa*ran *haa*miya**tan**

mereka memasuki api yang sangat panas (neraka),

88:5

# تُسْقٰى مِنْ عَيْنٍ اٰنِيَةٍ ۗ

tusq*aa* min 'aynin *aa*niya**tin**

diberi minum dari sumber mata air yang sangat panas.

88:6

# لَيْسَ لَهُمْ طَعَامٌ اِلَّا مِنْ ضَرِيْعٍۙ

laysa lahum *th*a'*aa*mun ill*aa* min *dh*arii'**in**

Tidak ada makanan bagi mereka selain dari pohon yang berduri,

88:7

# لَّا يُسْمِنُ وَلَا يُغْنِيْ مِنْ جُوْعٍۗ

l*aa* yusminu wal*aa* yughnii min juu'**in**

yang tidak menggemukkan dan tidak menghilangkan lapar.

88:8

# وُجُوْهٌ يَّوْمَىِٕذٍ نَّاعِمَةٌ ۙ

wujuuhun yawma-i*dz*in n*aa*'ima**tun**

Pada hari itu banyak (pula) wajah yang berseri-seri,

88:9

# لِّسَعْيِهَا رَاضِيَةٌ ۙ

lisa'yih*aa* r*aad*iya**tun**

merasa senang karena usahanya (sendiri),

88:10

# فِيْ جَنَّةٍ عَالِيَةٍۙ

fii jannatin '*aa*liya**tin**

(mereka) dalam surga yang tinggi,

88:11

# لَّا تَسْمَعُ فِيْهَا لَاغِيَةً ۗ

l*aa* tasma'u fiih*aa* l*aa*ghiya**tan**

di sana (kamu) tidak mendengar perkataan yang tidak berguna.

88:12

# فِيْهَا عَيْنٌ جَارِيَةٌ ۘ

fiih*aa* 'aynun j*aa*riya**tun**

Di sana ada mata air yang mengalir.

88:13

# فِيْهَا سُرُرٌ مَّرْفُوْعَةٌ ۙ

fiih*aa* sururun marfuu'a**tun**

Di sana ada dipan-dipan yang ditinggikan,

88:14

# وَّاَكْوَابٌ مَّوْضُوْعَةٌ ۙ

wa-akw*aa*bun maw*dh*uu'a**tun**

dan gelas-gelas yang tersedia (di dekatnya),

88:15

# وَّنَمَارِقُ مَصْفُوْفَةٌ ۙ

wanam*aa*riqu ma*sh*fuufa**tun**

dan bantal-bantal sandaran yang tersusun,

88:16

# وَّزَرَابِيُّ مَبْثُوْثَةٌ ۗ

wazar*aa*biyyu mabtsuutsa**tun**

dan permadani-permadani yang terhampar.

88:17

# اَفَلَا يَنْظُرُوْنَ اِلَى الْاِبِلِ كَيْفَ خُلِقَتْۗ

afal*aa* yan*zh*uruuna il*aa* **a**l-ibili kayfa khuliqath

Maka tidakkah mereka memperhatikan unta, bagaimana diciptakan?

88:18

# وَاِلَى السَّمَاۤءِ كَيْفَ رُفِعَتْۗ

wa-il*aa* **al**ssam*aa*-i kayfa rufi'ath

dan langit, bagaimana ditinggikan?

88:19

# وَاِلَى الْجِبَالِ كَيْفَ نُصِبَتْۗ

wa-il*aa* **a**ljib*aa*li kayfa nu*sh*ibath

Dan gunung-gunung bagaimana ditegakkan?

88:20

# وَاِلَى الْاَرْضِ كَيْفَ سُطِحَتْۗ

wa-il*aa* **a**l-ar*dh*i kayfa su*th*i*h*ath

Dan bumi bagaimana dihamparkan?

88:21

# فَذَكِّرْۗ اِنَّمَآ اَنْتَ مُذَكِّرٌۙ

fa*dz*akkir innam*aa* anta mu*dz*akkir**un**

Maka berilah peringatan, karena sesungguhnya engkau (Muhammad) hanyalah pemberi peringatan.

88:22

# لَّسْتَ عَلَيْهِمْ بِمُصَيْطِرٍۙ

lasta 'alayhim bimu*sh*ay*th*ir**in**

Engkau bukanlah orang yang berkuasa atas mereka,

88:23

# اِلَّا مَنْ تَوَلّٰى وَكَفَرَۙ

ill*aa* man tawall*aa* wakafar**a**

kecuali (jika ada) orang yang berpaling dan kafir,

88:24

# فَيُعَذِّبُهُ اللّٰهُ الْعَذَابَ الْاَكْبَرَۗ

fayu'a*dzdz*ibuhu **al**l*aa*hu **a**l'a*dzaa*ba **a**l-akbar**a**

maka Allah akan mengazabnya dengan azab yang besar.

88:25

# اِنَّ اِلَيْنَآ اِيَابَهُمْ

inna ilayn*aa* iy*aa*bahum

Sungguh, kepada Kamilah mereka kembali,

88:26

# ثُمَّ اِنَّ عَلَيْنَا حِسَابَهُمْ ࣖ

tsumma inna 'alayn*aa* *h*is*aa*bahum

kemudian sesungguhnya (kewajiban) Kamilah membuat perhitungan atas mereka.

<!--EndFragment-->