---
title: (21) Al-Anbiya' - الانبياۤء
date: 2021-10-27T03:48:43.990Z
ayat: 21
description: "Jumlah Ayat: 112 / Arti: Para Nabi"
---
<!--StartFragment-->

21:1

# اِقْتَرَبَ لِلنَّاسِ حِسَابُهُمْ وَهُمْ فِيْ غَفْلَةٍ مُّعْرِضُوْنَ ۚ

iqtaraba li**l**nn*aa*si *h*is*aa*buhum wahum fii ghaflatin mu'ri*dh*uun**a**

Telah semakin dekat kepada manusia perhitungan amal mereka, sedang mereka dalam keadaan lalai (dengan dunia), berpaling (dari akhirat).

21:2

# مَا يَأْتِيْهِمْ مِّنْ ذِكْرٍ مِّنْ رَّبِّهِمْ مُّحْدَثٍ اِلَّا اسْتَمَعُوْهُ وَهُمْ يَلْعَبُوْنَ ۙ

m*aa* ya/tiihim min *dz*ikrin min rabbihim mu*h*datsin ill*aa* istama'uuhu wahum yal'abuun**a**

Setiap diturunkan kepada mereka ayat-ayat yang baru dari Tuhan, mereka mendengarkannya sambil bermain-main.

21:3

# لَاهِيَةً قُلُوْبُهُمْۗ وَاَسَرُّوا النَّجْوَىۖ الَّذِيْنَ ظَلَمُوْاۖ هَلْ هٰذَآ اِلَّا بَشَرٌ مِّثْلُكُمْۚ اَفَتَأْتُوْنَ السِّحْرَ وَاَنْتُمْ تُبْصِرُوْنَ

l*aa*hiyatan quluubuhum wa-asarruu **al**nnajw*aa* **al**la*dz*iina *zh*alamuu hal h*aadzaa* ill*aa* basyarun mitslukum afata/tuuna **al**ssi*h*ra wa-antum tub*sh*iruun

Hati mereka dalam keadaan lalai. Dan orang-orang yang zalim itu merahasiakan pembicaraan mereka, “(Orang) ini (Muhammad) tidak lain hanyalah seorang manusia (juga) seperti kamu. Apakah kamu menerima sihir itu padahal kamu menyaksikannya?”







21:4

# قٰلَ رَبِّيْ يَعْلَمُ الْقَوْلَ فِى السَّمَاۤءِ وَالْاَرْضِۖ وَهُوَ السَّمِيْعُ الْعَلِيْمُ

q*aa*la rabbii ya'lamu **a**lqawla fii **al**ssam*aa*-i wa**a**l-ar*dh*i wahuwa **al**ssamii'u **a**l'aliim**u**

Dia (Muhammad) berkata, “Tuhanku mengetahui (semua) perkataan di langit dan di bumi, dan Dia Maha Mendengar, Maha Mengetahui!”

21:5

# بَلْ قَالُوْٓا اَضْغَاثُ اَحْلَامٍۢ بَلِ افْتَرٰىهُ بَلْ هُوَ شَاعِرٌۚ فَلْيَأْتِنَا بِاٰيَةٍ كَمَآ اُرْسِلَ الْاَوَّلُوْنَ

bal q*aa*luu a*dh*gh*aa*tsu a*h*l*aa*min bali iftar*aa*hu bal huwa sy*aa*'irun falya/tin*aa* bi-*aa*yatin kam*aa* ursila **a**l-awwaluun**a**

Bahkan mereka mengatakan, “(Al-Qur'an itu buah) mimpi-mimpi yang kacau, atau hasil rekayasanya (Muhammad), atau bahkan dia hanya seorang penyair, cobalah dia datangkan kepada kita suatu tanda (bukti), seperti halnya rasul-rasul yang diutus terdahulu.”

21:6

# مَآ اٰمَنَتْ قَبْلَهُمْ مِّنْ قَرْيَةٍ اَهْلَكْنٰهَاۚ اَفَهُمْ يُؤْمِنُوْنَ

m*aa* *aa*manat qablahum min qaryatin ahlakn*aa*h*aa* afahum yu/minuun**a**

Penduduk suatu negeri sebelum mereka, yang telah Kami binasakan, mereka itu tidak beriman (padahal telah Kami kirimkan bukti). Apakah mereka akan beriman?

21:7

# وَمَآ اَرْسَلْنَا قَبْلَكَ اِلَّا رِجَالًا نُّوْحِيْٓ اِلَيْهِمْ فَسْـَٔلُوْٓا اَهْلَ الذِّكْرِ اِنْ كُنْتُمْ لَا تَعْلَمُوْنَ

wam*aa* arsaln*aa* qablaka ill*aa* rij*aa*lan nuu*h*ii ilayhim fa**i**s-aluu ahla **al***dzdz*ikri in kuntum l*aa* ta'lamuun**a**

Dan Kami tidak mengutus (rasul-rasul) sebelum engkau (Muhammad), melainkan beberapa orang laki-laki yang Kami beri wahyu kepada mereka, maka tanyakanlah kepada orang yang berilmu, jika kamu tidak mengetahui.

21:8

# وَمَا جَعَلْنٰهُمْ جَسَدًا لَّا يَأْكُلُوْنَ الطَّعَامَ وَمَا كَانُوْا خٰلِدِيْنَ

wam*aa* ja'aln*aa*hum jasadan l*aa* ya/kuluuna **al***ththh*a'*aa*ma wam*aa* k*aa*nuu kh*aa*lidiin**a**

Dan Kami tidak menjadikan mereka (rasul-rasul) suatu tubuh yang tidak memakan makanan dan mereka tidak (pula) hidup kekal.

21:9

# ثُمَّ صَدَقْنٰهُمُ الْوَعْدَ فَاَنْجَيْنٰهُمْ وَمَنْ نَّشَاۤءُ وَاَهْلَكْنَا الْمُسْرِفِيْنَ

tsumma *sh*adaqn*aa*humu **a**lwa'da fa-anjayn*aa*hum waman nasy*aa*u wa-ahlakn*aa* **a**lmusrifiin**a**

Kemudian Kami tepati janji (yang telah Kami janjikan) kepada mereka. Maka Kami selamatkan mereka dan orang-orang yang Kami kehendaki, dan Kami binasakan orang-orang yang melampaui batas.

21:10

# لَقَدْ اَنْزَلْنَآ اِلَيْكُمْ كِتٰبًا فِيْهِ ذِكْرُكُمْۗ اَفَلَا تَعْقِلُوْنَ ࣖ

laqad anzaln*aa* ilaykum kit*aa*ban fiihi *dz*ikrukum afal*aa* ta'qiluun**a**

Sungguh, telah Kami turunkan kepadamu sebuah Kitab (Al-Qur'an) yang di dalamnya terdapat peringatan bagimu. Maka apakah kamu tidak mengerti?

21:11

# وَكَمْ قَصَمْنَا مِنْ قَرْيَةٍ كَانَتْ ظَالِمَةً وَّاَنْشَأْنَا بَعْدَهَا قَوْمًا اٰخَرِيْنَ

wakam qa*sh*amn*aa* min qaryatin k*aa*nat *zhaa*limatan wa-ansya/n*aa* ba'dah*aa* qawman *aa*khariin**a**

Dan berapa banyak (penduduk) negeri yang zalim yang telah Kami binasakan, dan Kami jadikan generasi yang lain setelah mereka itu (sebagai penggantinya).

21:12

# فَلَمَّآ اَحَسُّوْا بَأْسَنَآ اِذَا هُمْ مِّنْهَا يَرْكُضُوْنَ ۗ

falamm*aa* a*h*assuu ba/san*aa* i*dzaa* hum minh*aa* yarku*dh*uun**a**

Maka ketika mereka merasakan azab Kami, tiba-tiba mereka melarikan diri dari (negerinya) itu.

21:13

# لَا تَرْكُضُوْا وَارْجِعُوْٓا اِلٰى مَآ اُتْرِفْتُمْ فِيْهِ وَمَسٰكِنِكُمْ لَعَلَّكُمْ تُسْـَٔلُوْنَ

l*aa* tarku*dh*uu wa**i**rji'uu il*aa* m*aa* utriftum fiihi wamas*aa*kinikum la'allakum tus-aluun**a**

Janganlah kamu lari tergesa-gesa; kembalilah kamu kepada kesenangan hidupmu dan tempat-tempat kediamanmu (yang baik), agar kamu dapat ditanya.

21:14

# قَالُوْا يٰوَيْلَنَآ اِنَّا كُنَّا ظٰلِمِيْنَ

q*aa*luu y*aa* waylan*aa* inn*aa* kunn*aa* *zhaa*limiin**a**

Mereka berkata, “Betapa celaka kami, sungguh, kami orang-orang yang zalim.”

21:15

# فَمَا زَالَتْ تِّلْكَ دَعْوٰىهُمْ حَتّٰى جَعَلْنٰهُمْ حَصِيْدًا خَامِدِيْنَ

fam*aa* z*aa*lat tilka da'w*aa*hum *h*att*aa* ja'aln*aa*hum *h*a*sh*iidan kh*aa*midiin**a**

Maka demikianlah keluhan mereka berkepanjangan, sehingga mereka Kami jadikan sebagai tanaman yang telah dituai, yang tidak dapat hidup lagi.

21:16

# وَمَا خَلَقْنَا السَّمَاۤءَ وَالْاَرْضَ وَمَا بَيْنَهُمَا لٰعِبِيْنَ

wam*aa* khalaqn*aa* **al**ssam*aa*-a wa**a**l-ar*dh*a wam*aa* baynahum*aa* l*aa*'ibiin**a**

Dan Kami tidak menciptakan langit dan bumi dan segala apa yang ada di antara keduanya dengan main-main.

21:17

# لَوْ اَرَدْنَآ اَنْ نَّتَّخِذَ لَهْوًا لَّاتَّخَذْنٰهُ مِنْ لَّدُنَّآ ۖاِنْ كُنَّا فٰعِلِيْنَ

law aradn*aa* an nattakhi*dz*a lahwan la**i**ttakha*dz*n*aa*hu min ladunn*aa* in kunn*aa* f*aa*'iliin**a**

Seandainya Kami hendak membuat suatu permainan (istri dan anak), tentulah Kami membuatnya dari sisi Kami, jika Kami benar-benar menghendaki berbuat demikian.

21:18

# بَلْ نَقْذِفُ بِالْحَقِّ عَلَى الْبَاطِلِ فَيَدْمَغُهٗ فَاِذَا هُوَ زَاهِقٌۗ وَلَكُمُ الْوَيْلُ مِمَّا تَصِفُوْنَ

bal naq*dz*ifu bi**a**l*h*aqqi 'al*aa* **a**lb*aath*ili fayadmaghuhu fa-i*dzaa* huwa z*aa*hiqun walakumu **a**lwaylu mimm*aa* ta*sh*ifuun**a**

Sebenarnya Kami melemparkan yang hak (kebenaran) kepada yang batil (tidak benar) lalu yang hak itu menghancurkannya, maka seketika itu (yang batil) lenyap. Dan celaka kamu karena kamu menyifati (Allah dengan sifat-sifat yang tidak pantas bagi-Nya).

21:19

# وَلَهٗ مَنْ فِى السَّمٰوٰتِ وَالْاَرْضِۗ وَمَنْ عِنْدَهٗ لَا يَسْتَكْبِرُوْنَ عَنْ عِبَادَتِهٖ وَلَا يَسْتَحْسِرُوْنَ ۚ

walahu man fii **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i waman 'indahu l*aa* yastakbiruuna 'an 'ib*aa*datihi wal*aa* yasta*h*siruun**a**

Dan milik-Nya siapa yang di langit dan di bumi. Dan (malaikat-malaikat) yang di sisi-Nya, tidak mempunyai rasa angkuh untuk menyembah-Nya dan tidak (pula) merasa letih.

21:20

# يُسَبِّحُوْنَ الَّيْلَ وَالنَّهَارَ لَا يَفْتُرُوْنَ

yusabbi*h*uuna **al**layla wa**al**nnah*aa*ra l*aa* yafturuun**a**

Mereka (malaikat-malaikat) bertasbih tidak henti-hentinya malam dan siang.

21:21

# اَمِ اتَّخَذُوْٓا اٰلِهَةً مِّنَ الْاَرْضِ هُمْ يُنْشِرُوْنَ

ami ittakha*dz*uu *aa*lihatan mina **a**l-ar*dh*i hum yunsyiruun**a**

Apakah mereka mengambil tuhan-tuhan dari bumi, yang dapat menghidupkan (orang-orang yang mati)?

21:22

# لَوْ كَانَ فِيْهِمَآ اٰلِهَةٌ اِلَّا اللّٰهُ لَفَسَدَتَاۚ فَسُبْحٰنَ اللّٰهِ رَبِّ الْعَرْشِ عَمَّا يَصِفُوْنَ

law k*aa*na fiihim*aa* *aa*lihatun ill*aa* **al**l*aa*hu lafasadat*aa* fasub*haa*na **al**l*aa*hi rabbi **a**l'arsyi 'amm*aa* ya*sh*ifuun**a**

Seandainya pada keduanya (di langit dan di bumi) ada tuhan-tuhan selain Allah, tentu keduanya telah binasa. Mahasuci Allah yang memiliki ‘Arsy, dari apa yang mereka sifatkan.

21:23

# لَا يُسْـَٔلُ عَمَّا يَفْعَلُ وَهُمْ يُسْـَٔلُوْنَ

l*aa* yus-alu 'amm*aa* yaf'alu wahum yus-aluun**a**

Dia (Allah) tidak ditanya tentang apa yang dikerjakan, tetapi merekalah yang akan ditanya.

21:24

# اَمِ اتَّخَذُوْا مِنْ دُوْنِهٖٓ اٰلِهَةً ۗقُلْ هَاتُوْا بُرْهَانَكُمْۚ هٰذَا ذِكْرُ مَنْ مَّعِيَ وَذِكْرُ مَنْ قَبْلِيْۗ بَلْ اَكْثَرُهُمْ لَا يَعْلَمُوْنَۙ الْحَقَّ فَهُمْ مُّعْرِضُوْنَ

ami ittakha*dz*uu min duunihi *aa*lihatan qul h*aa*tuu burh*aa*nakum h*aadzaa* *dz*ikru man ma'iya wa*dz*ikru man qablii bal aktsaruhum l*aa* ya'lamuuna **a**l*h*aqqa fahum mu'ri*dh*uun

Atau apakah mereka mengambil tuhan-tuhan selain Dia? Katakanlah (Muhammad), “Kemukakanlah alasan-alasanmu! (Al-Qur'an) ini adalah peringatan bagi orang yang bersamaku, dan peringatan bagi orang sebelumku.” Tetapi kebanyakan mereka tidak mengetahui yang ha

21:25

# وَمَآ اَرْسَلْنَا مِنْ قَبْلِكَ مِنْ رَّسُوْلٍ اِلَّا نُوْحِيْٓ اِلَيْهِ اَنَّهٗ لَآ اِلٰهَ اِلَّآ اَنَا۠ فَاعْبُدُوْنِ

wam*aa* arsaln*aa* min qablika min rasuulin ill*aa* nuu*h*ii ilayhi annahu l*aa* il*aa*ha ill*aa* an*aa* fa**u**'buduun**i**

Dan Kami tidak mengutus seorang rasul pun sebelum engkau (Muhammad), melainkan Kami wahyukan kepadanya, bahwa tidak ada tuhan (yang berhak disembah) selain Aku, maka sembahlah Aku.

21:26

# وَقَالُوا اتَّخَذَ الرَّحْمٰنُ وَلَدًا سُبْحٰنَهٗ ۗبَلْ عِبَادٌ مُّكْرَمُوْنَ ۙ

waq*aa*luu ittakha*dz*a **al**rra*h*m*aa*nu waladan sub*haa*nahu bal 'ib*aa*dun mukramuun**a**

Dan mereka berkata, “Tuhan Yang Maha Pengasih telah menjadikan (malaikat) sebagai anak.” Mahasuci Dia. Sebenarnya mereka (para malaikat itu) adalah hamba-hamba yang dimuliakan,

21:27

# لَا يَسْبِقُوْنَهٗ بِالْقَوْلِ وَهُمْ بِاَمْرِهٖ يَعْمَلُوْنَ

l*aa* yasbiquunahu bi**a**lqawli wahum bi-amrihi ya'maluun**a**

mereka tidak berbicara mendahului-Nya dan mereka mengerjakan perintah-perintah-Nya.

21:28

# يَعْلَمُ مَا بَيْنَ اَيْدِيْهِمْ وَمَا خَلْفَهُمْ وَلَا يَشْفَعُوْنَۙ اِلَّا لِمَنِ ارْتَضٰى وَهُمْ مِّنْ خَشْيَتِهٖ مُشْفِقُوْنَ

ya'lamu m*aa* bayna aydiihim wam*aa* khalfahum wal*aa* yasyfa'uuna ill*aa* limani irta*daa* wahum min khasyyatihi musyfiquun**a**

Dia (Allah) mengetahui segala sesuatu yang di hadapan mereka (malaikat) dan yang di belakang mereka, dan mereka tidak memberi syafaat melainkan kepada orang yang diridai (Allah), dan mereka selalu berhati-hati karena takut kepada-Nya.

21:29

# ۞ وَمَنْ يَّقُلْ مِنْهُمْ اِنِّيْٓ اِلٰهٌ مِّنْ دُوْنِهٖ فَذٰلِكَ نَجْزِيْهِ جَهَنَّمَۗ كَذٰلِكَ نَجْزِى الظّٰلِمِيْنَ ࣖ

waman yaqul minhum innii il*aa*hun min duunihi fa*dzaa*lika najziihi jahannama ka*dzaa*lika najzii **al***zhzhaa*limiin**a**

Dan barangsiapa di antara mereka berkata, “Sungguh, aku adalah tuhan selain Allah,” maka orang itu Kami beri balasan dengan Jahanam. Demikianlah Kami memberikan balasan kepada orang-orang yang zalim.

21:30

# اَوَلَمْ يَرَ الَّذِيْنَ كَفَرُوْٓا اَنَّ السَّمٰوٰتِ وَالْاَرْضَ كَانَتَا رَتْقًا فَفَتَقْنٰهُمَاۗ وَجَعَلْنَا مِنَ الْمَاۤءِ كُلَّ شَيْءٍ حَيٍّۗ اَفَلَا يُؤْمِنُوْنَ

awa lam yar*aa* **al**la*dz*iina kafaruu anna **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*a k*aa*nat*aa* ratqan fafataqn*aa*hum*aa* waja'aln*aa* mina **a**lm

Dan apakah orang-orang kafir tidak mengetahui bahwa langit dan bumi keduanya dahulunya menyatu, kemudian Kami pisahkan antara keduanya; dan Kami jadikan segala sesuatu yang hidup berasal dari air; maka mengapa mereka tidak beriman?







21:31

# وَجَعَلْنَا فِى الْاَرْضِ رَوَاسِيَ اَنْ تَمِيْدَ بِهِمْۖ وَجَعَلْنَا فِيْهَا فِجَاجًا سُبُلًا لَّعَلَّهُمْ يَهْتَدُوْنَ

waja'aln*aa* fii **a**l-ar*dh*i raw*aa*siya an tamiida bihim waja'aln*aa* fiih*aa* fij*aa*jan subulan la'allahum yahtaduun**a**

Dan Kami telah menjadikan di bumi ini gunung-gunung yang kokoh agar ia (tidak) guncang bersama mereka, dan Kami jadikan (pula) di sana jalan-jalan yang luas, agar mereka mendapat petunjuk.

21:32

# وَجَعَلْنَا السَّمَاۤءَ سَقْفًا مَّحْفُوْظًاۚ وَهُمْ عَنْ اٰيٰتِهَا مُعْرِضُوْنَ

waja'aln*aa* **al**ssam*aa*-a saqfan ma*h*fuu*zh*an wahum 'an *aa*y*aa*tih*aa* mu'ri*dh*uun**a**

Dan Kami menjadikan langit sebagai atap yang terpelihara, namun mereka tetap berpaling dari tanda-tanda (kebesaran Allah) itu (matahari, bulan, angin, awan, dan lain-lain).

21:33

# وَهُوَ الَّذِيْ خَلَقَ الَّيْلَ وَالنَّهَارَ وَالشَّمْسَ وَالْقَمَرَۗ كُلٌّ فِيْ فَلَكٍ يَّسْبَحُوْنَ

wahuwa **al**la*dz*ii khalaqa **al**layla wa**al**nnah*aa*ra wa**al**sysyamsa wa**a**lqamara kullun fii falakin yasba*h*uun**a**

Dan Dialah yang telah menciptakan malam dan siang, matahari dan bulan. Masing-masing beredar pada garis edarnya.

21:34

# وَمَا جَعَلْنَا لِبَشَرٍ مِّنْ قَبْلِكَ الْخُلْدَۗ اَفَا۟ىِٕنْ مِّتَّ فَهُمُ الْخٰلِدُوْنَ

wam*aa* ja'aln*aa* libasyarin min qablika **a**lkhulda afa-in mitta fahumu **a**lkh*aa*liduun**a**

Dan Kami tidak menjadikan hidup abadi bagi seorang manusia sebelum engkau (Muhammad); maka jika engkau wafat, apakah mereka akan kekal?

21:35

# كُلُّ نَفْسٍ ذَاۤىِٕقَةُ الْمَوْتِۗ وَنَبْلُوْكُمْ بِالشَّرِّ وَالْخَيْرِ فِتْنَةً ۗوَاِلَيْنَا تُرْجَعُوْنَ

kullu nafsin *dzaa*-iqatu **a**lmawti wanabluukum bi**al**sysyarri wa**a**lkhayri fitnatan wa-ilayn*aa* turja'uun**a**

Setiap yang bernyawa akan merasakan mati. Kami akan menguji kamu dengan keburukan dan kebaikan sebagai cobaan. Dan kamu akan dikembalikan hanya kepada Kami.

21:36

# وَاِذَا رَاٰكَ الَّذِيْنَ كَفَرُوْٓا اِنْ يَّتَّخِذُوْنَكَ اِلَّا هُزُوًاۗ اَهٰذَا الَّذِيْ يَذْكُرُ اٰلِهَتَكُمْۚ وَهُمْ بِذِكْرِ الرَّحْمٰنِ هُمْ كٰفِرُوْنَ

wa-i*dzaa* ra*aa*ka **al**la*dz*iina kafaruu in yattakhi*dz*uunaka ill*aa* huzuwan ah*aadzaa* **al**la*dz*ii ya*dz*kuru *aa*lihatakum wahum bi*dz*ikri **al**rra*h*

*Dan apabila orang-orang kafir itu melihat engkau (Muhammad), mereka hanya memperlakukan engkau menjadi bahan ejekan. (Mereka mengatakan), “Apakah ini orang yang mencela tuhan-tuhanmu?” Padahal mereka orang yang ingkar mengingat Allah Yang Maha Pengasih.*









21:37

# خُلِقَ الْاِنْسَانُ مِنْ عَجَلٍۗ سَاُورِيْكُمْ اٰيٰتِيْ فَلَا تَسْتَعْجِلُوْنِ

khuliqa **a**l-ins*aa*nu min 'ajalin sauriikum *aa*y*aa*tii fal*aa* tasta'jiluun**a**

Manusia diciptakan (bersifat) tergesa-gesa. Kelak akan Aku perlihatkan kepadamu tanda-tanda (kekuasaan)-Ku. Maka janganlah kamu meminta Aku menyegerakannya.

21:38

# وَيَقُوْلُوْنَ مَتٰى هٰذَا الْوَعْدُ اِنْ كُنْتُمْ صٰدِقِيْنَ

wayaquuluuna mat*aa* h*aadzaa* **a**lwa'du in kuntum *shaa*diqiin**a**

Dan mereka berkata, “Kapankah janji itu (akan datang), jika kamu orang yang benar?”

21:39

# لَوْ يَعْلَمُ الَّذِيْنَ كَفَرُوْا حِيْنَ لَا يَكُفُّوْنَ عَنْ وُّجُوْهِهِمُ النَّارَ وَلَا عَنْ ظُهُوْرِهِمْ وَلَا هُمْ يُنْصَرُوْنَ

law ya'lamu **al**la*dz*iina kafaruu *h*iina l*aa* yakuffuuna 'an wujuuhihimu **al**nn*aa*ra wal*aa* 'an *zh*uhuurihim wal*aa* hum yun*sh*aruun**a**

Seandainya orang kafir itu mengetahui, ketika mereka itu tidak mampu mengelakkan api neraka dari wajah dan punggung mereka, sedang mereka tidak mendapat pertolongan (tentulah mereka tidak meminta disegerakan).

21:40

# بَلْ تَأْتِيْهِمْ بَغْتَةً فَتَبْهَتُهُمْ فَلَا يَسْتَطِيْعُوْنَ رَدَّهَا وَلَا هُمْ يُنْظَرُوْنَ

bal ta/tiihim baghtatan fatabhatuhum fal*aa* yasta*th*ii'uuna raddah*aa* wal*aa* hum yun*zh*aruun**a**

Sebenarnya (hari Kiamat) itu akan datang kepada mereka secara tiba-tiba, lalu mereka menjadi panik; maka mereka tidak sanggup menolaknya dan tidak (pula) diberi penangguhan (waktu).

21:41

# وَلَقَدِ اسْتُهْزِئَ بِرُسُلٍ مِّنْ قَبْلِكَ فَحَاقَ بِالَّذِيْنَ سَخِرُوْا مِنْهُمْ مَّا كَانُوْا بِهٖ يَسْتَهْزِءُوْنَ ࣖ

walaqadi istuhzi-a birusulin min qablika fa*haa*qa bi**a**lla*dz*iina sakhiruu minhum m*aa* k*aa*nuu bihi yastahzi-uun**a**

Dan sungguh, rasul-rasul sebelum engkau (Muhammad) pun telah diperolok-olokkan, maka turunlah (siksaan) kepada orang-orang yang mencemoohkan apa (rasul-rasul) yang selalu mereka perolok-olokkan.

21:42

# قُلْ مَنْ يَّكْلَؤُكُمْ بِالَّيْلِ وَالنَّهَارِ مِنَ الرَّحْمٰنِۗ بَلْ هُمْ عَنْ ذِكْرِ رَبِّهِمْ مُّعْرِضُوْنَ

qul man yaklaukum bi**a**llayli wa**al**nnah*aa*ri mina **al**rra*h*m*aa*ni bal hum 'an *dz*ikri rabbihim mu'ri*dh*uun**a**

Katakanlah, “Siapakah yang akan menjaga kamu pada waktu malam dan siang dari (siksaan) Allah Yang Maha Pengasih?” Tetapi mereka enggan mengingat Tuhan mereka.

21:43

# اَمْ لَهُمْ اٰلِهَةٌ تَمْنَعُهُمْ مِّنْ دُوْنِنَاۗ لَا يَسْتَطِيْعُوْنَ نَصْرَ اَنْفُسِهِمْ وَلَا هُمْ مِّنَّا يُصْحَبُوْنَ

am lahum *aa*lihatun tamna'uhum min duunin*aa* l*aa* yasta*th*ii'uuna na*sh*ra anfusihim wal*aa* hum minn*aa* yu*sy*abuun**a**

Ataukah mereka mempunyai tuhan-tuhan yang dapat memelihara mereka dari (azab) Kami? Tuhan-tuhan mereka itu tidak sanggup menolong diri mereka sendiri dan tidak (pula) mereka dilindungi dari (azab) Kami.

21:44

# بَلْ مَتَّعْنَا هٰٓؤُلَاۤءِ وَاٰبَاۤءَهُمْ حَتّٰى طَالَ عَلَيْهِمُ الْعُمُرُۗ اَفَلَا يَرَوْنَ اَنَّا نَأْتِى الْاَرْضَ نَنْقُصُهَا مِنْ اَطْرَافِهَاۗ اَفَهُمُ الْغٰلِبُوْنَ

bal matta'n*aa* h*aa*ul*aa*-i wa*aa*b*aa*-ahum *h*att*aa* *thaa*la 'alayhimu **a**l'umuru afal*aa* yarawna ann*aa* na/tii **a**l-ar*dh*a nanqu*sh*uh*aa* min

Sebenarnya Kami telah memberi mereka dan nenek moyang mereka kenikmatan (hidup di dunia) hingga panjang usia mereka. Maka apakah mereka tidak melihat bahwa Kami mendatangi negeri (yang berada di bawah kekuasaan orang kafir), lalu Kami kurangi luasnya dari

21:45

# قُلْ اِنَّمَآ اُنْذِرُكُمْ بِالْوَحْيِۖ وَلَا يَسْمَعُ الصُّمُّ الدُّعَاۤءَ اِذَا مَا يُنْذَرُوْنَ

qul innam*aa* un*dz*irukum bi**a**lwa*h*yi wal*aa* yasma'u **al***shsh*ummu **al**ddu'*aa*-a i*dzaa* m*aa* yun*dz*aruun**a**

Katakanlah (Muhammad), “Sesungguhnya aku hanya memberimu peringatan sesuai dengan wahyu.” Tetapi orang tuli tidak mendengar seruan apabila mereka diberi peringatan.

21:46

# وَلَىِٕنْ مَّسَّتْهُمْ نَفْحَةٌ مِّنْ عَذَابِ رَبِّكَ لَيَقُوْلُنَّ يٰوَيْلَنَآ اِنَّا كُنَّا ظٰلِمِيْنَ

wala-in massat-hum naf*h*atun min 'a*dzaa*bi rabbika layaquulunna y*aa* waylan*aa* inn*aa* kunn*aa* *zhaa*limiin**a**

Dan jika mereka ditimpa sedikit saja azab Tuhanmu, pastilah mereka berkata, “Celakalah kami! Sesungguhnya kami termasuk orang yang selalu menzalimi (diri sendiri).”

21:47

# وَنَضَعُ الْمَوَازِيْنَ الْقِسْطَ لِيَوْمِ الْقِيٰمَةِ فَلَا تُظْلَمُ نَفْسٌ شَيْـًٔاۗ وَاِنْ كَانَ مِثْقَالَ حَبَّةٍ مِّنْ خَرْدَلٍ اَتَيْنَا بِهَاۗ وَكَفٰى بِنَا حَاسِبِيْنَ

wana*dh*a'u **a**lmaw*aa*ziina **a**lqis*th*a liyawmi **a**lqiy*aa*mati fal*aa* tu*zh*lamu nafsun syay-an wa-in k*aa*na mitsq*aa*la *h*abbatin min khardalin atayn*aa*

Dan Kami akan memasang timbangan yang tepat pada hari Kiamat, maka tidak seorang pun dirugikan walau sedikit; sekalipun hanya seberat biji sawi, pasti Kami mendatangkannya (pahala). Dan cukuplah Kami yang membuat perhitungan.

21:48

# وَلَقَدْ اٰتَيْنَا مُوْسٰى وَهٰرُوْنَ الْفُرْقَانَ وَضِيَاۤءً وَّذِكْرًا لِّلْمُتَّقِيْنَ ۙ

walaqad *aa*tayn*aa* muus*aa* wah*aa*ruuna **a**lfurq*aa*na wa*dh*iy*aa*-an wa*dz*ikran lilmuttaqiin**a**

Dan sungguh, Kami telah memberikan kepada Musa dan Harun, Furqan (Kitab Taurat) dan penerangan serta pelajaran bagi orang-orang yang bertakwa.

21:49

# الَّذِيْنَ يَخْشَوْنَ رَبَّهُمْ بِالْغَيْبِ وَهُمْ مِّنَ السَّاعَةِ مُشْفِقُوْنَ

**al**la*dz*iina yakhsyawna rabbahum bi**a**lghaybi wahum mina **al**ss*aa*'ati musyfiquun**a**

(Yaitu) orang-orang yang takut (azab) Tuhannya, sekalipun mereka tidak melihat-Nya, dan mereka merasa takut akan (tibanya) hari Kiamat.

21:50

# وَهٰذَا ذِكْرٌ مُّبٰرَكٌ اَنْزَلْنٰهُۗ اَفَاَنْتُمْ لَهٗ مُنْكِرُوْنَ ࣖ

wah*aadzaa* *dz*ikrun mub*aa*rakun anzaln*aa*hu afa-antum lahu munkiruun**a**

Dan ini (Al-Qur'an) adalah suatu peringatan yang mempunyai berkah yang telah Kami turunkan. Maka apakah kamu mengingkarinya?

21:51

# ۞ وَلَقَدْ اٰتَيْنَآ اِبْرٰهِيْمَ رُشْدَهٗ مِنْ قَبْلُ وَكُنَّا بِهٖ عٰلِمِيْنَ

walaqad *aa*tayn*aa* ibr*aa*hiima rusydahu min qablu wakunn*aa* bihi '*aa*limiin**a**

Dan sungguh, sebelum dia (Musa dan Harun) telah Kami berikan kepada Ibrahim petunjuk, dan Kami telah mengetahui dia.

21:52

# اِذْ قَالَ لِاَبِيْهِ وَقَوْمِهٖ مَا هٰذِهِ التَّمَاثِيْلُ الَّتِيْٓ اَنْتُمْ لَهَا عَاكِفُوْنَ

i*dz* q*aa*la li-abiihi waqawmihi m*aa* h*aadz*ihi **al**ttam*aa*tsiilu **al**latii antum lah*aa* '*aa*kifuun**a**

(Ingatlah), ketika dia (Ibrahim) berkata kepada ayahnya dan kaumnya, “Patung-patung apakah ini yang kamu tekun menyembahnya?”

21:53

# قَالُوْا وَجَدْنَآ اٰبَاۤءَنَا لَهَا عٰبِدِيْنَ

q*aa*luu wajadn*aa* *aa*b*aa*-an*aa* lah*aa* '*aa*bidiin**a**

Mereka menjawab, “Kami mendapati nenek moyang kami menyembahnya.”

21:54

# قَالَ لَقَدْ كُنْتُمْ اَنْتُمْ وَاٰبَاۤؤُكُمْ فِيْ ضَلٰلٍ مُّبِيْنٍ

q*aa*la laqad kuntum antum wa*aa*b*aa*ukum fii *dh*al*aa*lin mubiin**in**

Dia (Ibrahim) berkata, “Sesungguhnya kamu dan nenek moyang kamu berada dalam kesesatan yang nyata.”

21:55

# قَالُوْٓا اَجِئْتَنَا بِالْحَقِّ اَمْ اَنْتَ مِنَ اللّٰعِبِيْنَ

q*aa*luu aji/tan*aa* bi**a**l*h*aqqi am anta mina **al**l*aa*'ibiin**a**

Mereka berkata, “Apakah engkau da-tang kepada kami membawa kebenaran atau engkau main-main?”

21:56

# قَالَ بَلْ رَّبُّكُمْ رَبُّ السَّمٰوٰتِ وَالْاَرْضِ الَّذِيْ فَطَرَهُنَّۖ وَاَنَا۠ عَلٰى ذٰلِكُمْ مِّنَ الشّٰهِدِيْنَ

q*aa*la bal rabbukum rabbu **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i **al**la*dz*ii fa*th*arahunna wa-an*aa* 'al*aa* *dzaa*likum mina **al**sysy*aa*hidiin

Dia (Ibrahim) menjawab, “Sebenarnya Tuhan kamu ialah Tuhan (pemilik) langit dan bumi; (Dialah) yang telah menciptakannya; dan aku termasuk orang yang dapat bersaksi atas itu.”

21:57

# وَتَاللّٰهِ لَاَكِيْدَنَّ اَصْنَامَكُمْ بَعْدَ اَنْ تُوَلُّوْا مُدْبِرِيْنَ

wata**al**l*aa*hi la-akiidanna a*sh*n*aa*makum ba'da an tuwalluu mudbiriin**a**

Dan demi Allah, sungguh, aku akan melakukan tipu daya terhadap berhala-berhalamu setelah kamu pergi meninggalkannya.

21:58

# فَجَعَلَهُمْ جُذَاذًا اِلَّا كَبِيْرًا لَّهُمْ لَعَلَّهُمْ اِلَيْهِ يَرْجِعُوْنَ

faja'alahum ju*dzats*an ill*aa* kabiiran lahum la'allahum ilayhi yarji'uun**a**

Maka dia (Ibrahim) menghancurkan (berhala-berhala itu) berkeping-keping, kecuali yang terbesar (induknya); agar mereka kembali (untuk bertanya) kepadanya.

21:59

# قَالُوْا مَنْ فَعَلَ هٰذَا بِاٰلِهَتِنَآ اِنَّهٗ لَمِنَ الظّٰلِمِيْنَ

q*aa*luu man fa'ala h*aadzaa* bi-*aa*lihatin*aa* innahu lamina **al***zhzhaa*limiin**a**

Mereka berkata, “Siapakah yang melakukan (perbuatan) ini terhadap tuhan-tuhan kami? Sungguh, dia termasuk orang yang zalim.”

21:60

# قَالُوْا سَمِعْنَا فَتًى يَّذْكُرُهُمْ يُقَالُ لَهٗٓ اِبْرٰهِيْمُ ۗ

q*aa*luu sami'n*aa* fatan ya*dz*kuruhum yuq*aa*lu lahu ibr*aa*hiim**u**

Mereka (yang lain) berkata, “Kami mendengar ada seorang pemuda yang mencela (berhala-berhala ini), namanya Ibrahim.”

21:61

# قَالُوْا فَأْتُوْا بِهٖ عَلٰٓى اَعْيُنِ النَّاسِ لَعَلَّهُمْ يَشْهَدُوْنَ

q*aa*luu fa/tuu bihi 'al*aa* a'yuni **al**nn*aa*si la'allahum yasyhaduun**a**

Mereka berkata, “(Kalau demikian) bawalah dia dengan diperlihatkan kepada orang banyak, agar mereka menyaksikan.”

21:62

# قَالُوْٓا ءَاَنْتَ فَعَلْتَ هٰذَا بِاٰلِهَتِنَا يٰٓاِبْرٰهِيْمُ ۗ

q*aa*luu a-anta fa'alta h*aadzaa* bi-*aa*lihatin*aa* y*aa* ibr*aa*hiim**u**

Mereka bertanya, “Apakah engkau yang melakukan (perbuatan) ini terhadap tuhan-tuhan kami, wahai Ibrahim?”

21:63

# قَالَ بَلْ فَعَلَهٗ كَبِيْرُهُمْ هٰذَا فَسْـَٔلُوْهُمْ اِنْ كَانُوْا يَنْطِقُوْنَ

q*aa*la bal fa'alahu kabiiruhum h*aadzaa* fa**i**s-aluuhum in k*aa*nuu yan*th*iquun**a**

Dia (Ibrahim) menjawab, “Sebenarnya (patung) besar itu yang melakukannya, maka tanyakanlah kepada mereka, jika mereka dapat berbicara.”

21:64

# فَرَجَعُوْٓا اِلٰٓى اَنْفُسِهِمْ فَقَالُوْٓا اِنَّكُمْ اَنْتُمُ الظّٰلِمُوْنَ ۙ

faraja'uu il*aa* anfusihim faq*aa*luu innakum antumu **al***zhzhaa*limuun**a**

Maka mereka kembali kepada kesadaran mereka dan berkata, “Sesungguhnya kamulah yang menzalimi (diri sendiri).”

21:65

# ثُمَّ نُكِسُوْا عَلٰى رُءُوْسِهِمْۚ لَقَدْ عَلِمْتَ مَا هٰٓؤُلَاۤءِ يَنْطِقُوْنَ

tsumma nukisuu 'al*aa* ruuusihim laqad 'alimta m*aa* h*aa*ul*aa*-i yan*th*iquun**a**

Kemudian mereka menundukkan kepala (lalu berkata), “Engkau (Ibrahim) pasti tahu bahwa (berhala-berhala) itu tidak dapat berbicara.”

21:66

# قَالَ اَفَتَعْبُدُوْنَ مِنْ دُوْنِ اللّٰهِ مَا لَا يَنْفَعُكُمْ شَيْـًٔا وَّلَا يَضُرُّكُمْ ۗ

q*aa*la afata'buduuna min duuni **al**l*aa*hi m*aa* l*aa* yanfa'ukum syay-an wal*aa* ya*dh*urrukum

Dia (Ibrahim) berkata, “Mengapa kamu menyembah selain Allah, sesuatu yang tidak dapat memberi manfaat sedikit pun, dan tidak (pula) mendatangkan mudarat kepada kamu?

21:67

# اُفٍّ لَّكُمْ وَلِمَا تَعْبُدُوْنَ مِنْ دُوْنِ اللّٰهِ ۗاَفَلَا تَعْقِلُوْنَ

uffin lakum walim*aa* ta'buduuna min duuni **al**l*aa*hi afal*aa* ta'qiluun**a**

Celakalah kamu dan apa yang kamu sembah selain Allah! Tidakkah kamu mengerti?”

21:68

# قَالُوْا حَرِّقُوْهُ وَانْصُرُوْٓا اٰلِهَتَكُمْ اِنْ كُنْتُمْ فٰعِلِيْنَ

q*aa*luu *h*arriquuhu wa**u**n*sh*uruu *aa*lihatakum in kuntum f*aa*'iliin**a**

Mereka berkata, “Bakarlah dia dan bantulah tuhan-tuhan kamu, jika kamu benar-benar hendak berbuat.”

21:69

# قُلْنَا يَا نَارُ كُوْنِيْ بَرْدًا وَّسَلٰمًا عَلٰٓى اِبْرٰهِيْمَ ۙ

quln*aa* y*aa* n*aa*ru kuunii bardan wasal*aa*man 'al*aa* ibr*aa*hiim**a**

Kami (Allah) berfirman, “Wahai api! Jadilah kamu dingin, dan penyelamat bagi Ibrahim!”

21:70

# وَاَرَادُوْا بِهٖ كَيْدًا فَجَعَلْنٰهُمُ الْاَخْسَرِيْنَ ۚ

wa-ar*aa*duu bihi kaydan faja'aln*aa*humu **a**l-akhsariin**a**

Dan mereka hendak berbuat jahat terhadap Ibrahim, maka Kami menjadikan mereka itu orang-orang yang paling rugi.

21:71

# وَنَجَّيْنٰهُ وَلُوْطًا اِلَى الْاَرْضِ الَّتِيْ بٰرَكْناَ فِيْهَا لِلْعٰلَمِيْنَ

wanajjayn*aa*hu waluu*th*an il*aa* **a**l-ar*dh*i **al**latii b*aa*rakn*aa* fiih*aa* lil'*aa*lamiin**a**

Dan Kami selamatkan dia (Ibrahim) dan Lut ke sebuah negeri yang telah Kami berkahi untuk seluruh alam.

21:72

# وَوَهَبْنَا لَهٗٓ اِسْحٰقَ وَيَعْقُوْبَ نَافِلَةً ۗوَكُلًّا جَعَلْنَا صٰلِحِيْنَ

wawahabn*aa* lahu is*haa*qa waya'quuba n*aa*filatan wakullan ja'aln*aa* *shaa*li*h*iin**a**

Dan Kami menganugerahkan kepadanya (Ibrahim) Ishak dan Yakub, sebagai suatu anugerah. Dan masing-masing Kami jadikan orang yang saleh.

21:73

# وَجَعَلْنٰهُمْ اَىِٕمَّةً يَّهْدُوْنَ بِاَمْرِنَا وَاَوْحَيْنَآ اِلَيْهِمْ فِعْلَ الْخَيْرٰتِ وَاِقَامَ الصَّلٰوةِ وَاِيْتَاۤءَ الزَّكٰوةِۚ وَكَانُوْا لَنَا عٰبِدِيْنَ ۙ

waja'aln*aa*hum a-immatan yahduuna bi-amrin*aa* wa-aw*h*ayn*aa* ilayhim fi'la **a**lkhayr*aa*ti wa-iq*aa*ma **al***shsh*al*aa*ti wa-iit*aa*-a **al**zzak*aa*ti wak*a*

Dan Kami menjadikan mereka itu sebagai pemimpin-pemimpin yang memberi petunjuk dengan perintah Kami dan Kami wahyukan kepada mereka agar berbuat kebaikan, melaksanakan salat dan menunaikan zakat, dan hanya kepada Kami mereka menyembah.







21:74

# وَلُوْطًا اٰتَيْنٰهُ حُكْمًا وَّعِلْمًا وَّنَجَّيْنٰهُ مِنَ الْقَرْيَةِ الَّتِيْ كَانَتْ تَّعْمَلُ الْخَبٰۤىِٕثَ ۗاِنَّهُمْ كَانُوْا قَوْمَ سَوْءٍ فٰسِقِيْنَۙ

waluu*th*an *aa*tayn*aa*hu *h*ukman wa'ilman wanajjayn*aa*hu mina **a**lqaryati **al**latii k*aa*nat ta'malu **a**lkhab*aa*-itsa innahum k*aa*nuu qawma saw-in f*aa*siqiin<

Kepada Lut, Kami berikan hikmah dan ilmu, dan Kami selamatkan dia dari (azab yang telah menimpa penduduk) kota yang melakukan perbuatan keji. Sungguh, mereka orang-orang yang jahat lagi fasik.

21:75

# وَاَدْخَلْنٰهُ فِيْ رَحْمَتِنَاۗ اِنَّهٗ مِنَ الصّٰلِحِيْنَ ࣖ

wa-adkhaln*aa*hu fii ra*h*matin*aa* innahu mina **al***shshaa*li*h*iin**a**

Dan Kami masukkan dia ke dalam rahmat Kami; sesungguhnya dia termasuk golongan orang yang saleh.

21:76

# وَنُوْحًا اِذْ نَادٰى مِنْ قَبْلُ فَاسْتَجَبْنَا لَهٗ فَنَجَّيْنٰهُ وَاَهْلَهٗ مِنَ الْكَرْبِ الْعَظِيْمِ ۚ

wanuu*h*an i*dz* n*aa*d*aa* min qablu fa**i**stajabn*aa* lahu fanajjayn*aa*hu wa-ahlahu mina **a**lkarbi **a**l'a*zh*iim**i**

Dan (ingatlah kisah) Nuh, sebelum itu, ketika dia berdoa. Kami perkenankan (doa)nya, lalu Kami selamatkan dia bersama pengikutnya dari bencana yang besar.

21:77

# وَنَصَرْنٰهُ مِنَ الْقَوْمِ الَّذِيْنَ كَذَّبُوْا بِاٰيٰتِنَاۗ اِنَّهُمْ كَانُوْا قَوْمَ سَوْءٍ فَاَغْرَقْنٰهُمْ اَجْمَعِيْنَ

wana*sh*arn*aa*hu mina **a**lqawmi **al**la*dz*iina ka*dzdz*abuu bi-*aa*y*aa*tin*aa* innahum k*aa*nuu qawma saw-in fa-aghraqn*aa*hum ajma'iin**a**

Dan Kami menolongnya dari orang-orang yang telah mendustakan ayat-ayat Kami. Sesungguhnya mereka adalah orang-orang yang jahat, maka Kami tenggelamkan mereka semuanya.

21:78

# وَدَاوٗدَ وَسُلَيْمٰنَ اِذْ يَحْكُمٰنِ فِى الْحَرْثِ اِذْ نَفَشَتْ فِيْهِ غَنَمُ الْقَوْمِۚ وَكُنَّا لِحُكْمِهِمْ شٰهِدِيْنَ ۖ

wad*aa*wuuda wasulaym*aa*na i*dz* ya*h*kum*aa*ni fii **a**l*h*artsi i*dz* nafasyat fiihi ghanamu **a**lqawmi wakunn*aa* li*h*ukmihim sy*aa*hidiin**a**

Dan (ingatlah kisah) Dawud dan Sulaiman, ketika keduanya memberikan keputusan mengenai ladang, karena (ladang itu) dirusak oleh kambing-kambing milik kaumnya. Dan Kami menyaksikan keputusan (yang diberikan) oleh mereka itu.

21:79

# فَفَهَّمْنٰهَا سُلَيْمٰنَۚ وَكُلًّا اٰتَيْنَا حُكْمًا وَّعِلْمًاۖ وَّسَخَّرْنَا مَعَ دَاوٗدَ الْجِبَالَ يُسَبِّحْنَ وَالطَّيْرَۗ وَكُنَّا فٰعِلِيْنَ

fafahhamn*aa*h*aa* sulaym*aa*na wakullan *aa*tayn*aa* *h*ukman wa'ilman wasakhkharn*aa* ma'a d*aa*wuuda **a**ljib*aa*la yusabbi*h*na wa**al***ththh*ayra wakunn*aa* f*a*

Dan Kami memberikan pengertian kepada Sulaiman (tentang hukum yang lebih tepat); dan kepada masing-masing Kami berikan hikmah dan ilmu, dan Kami tundukkan gunung-gunung dan burung-burung, semua bertasbih bersama Dawud. Dan Kamilah yang melakukannya.







21:80

# وَعَلَّمْنٰهُ صَنْعَةَ لَبُوْسٍ لَّكُمْ لِتُحْصِنَكُمْ مِّنْۢ بَأْسِكُمْۚ فَهَلْ اَنْتُمْ شَاكِرُوْنَ

wa'allamn*aa*hu *sh*an'ata labuusin lakum litu*hs*inakum min ba/sikum fahal antum sy*aa*kiruun**a**

Dan Kami ajarkan (pula) kepada Dawud cara membuat baju besi untukmu, guna melindungi kamu dalam peperangan. Apakah kamu bersyukur (kepada Allah)?

21:81

# وَلِسُلَيْمٰنَ الرِّيْحَ عَاصِفَةً تَجْرِيْ بِاَمْرِهٖٓ اِلَى الْاَرْضِ الَّتِيْ بٰرَكْنَا فِيْهَاۗ وَكُنَّا بِكُلِّ شَيْءٍ عٰلِمِيْنَ

walisulaym*aa*na **al**rrii*h*a '*aas*ifatan tajrii bi-amrihi il*aa* **a**l-ar*dh*i **al**latii b*aa*rakn*aa* fiih*aa* wakunn*aa* bikulli syay-in '*aa*limiin**a**

Dan (Kami tundukkan) untuk Sulaiman angin yang sangat kencang tiupannya yang berhembus dengan perintahnya ke negeri yang Kami beri berkah padanya. Dan Kami Maha Mengetahui segala sesuatu.







21:82

# وَمِنَ الشَّيٰطِيْنِ مَنْ يَّغُوْصُوْنَ لَهٗ وَيَعْمَلُوْنَ عَمَلًا دُوْنَ ذٰلِكَۚ وَكُنَّا لَهُمْ حٰفِظِيْنَ ۙ

wamina **al**sysyay*aath*iini man yaghuu*sh*uuna lahu waya'maluuna 'amalan duuna *dzaa*lika wakunn*aa* lahum *haa*fi*zh*iin**a**

Dan (Kami tundukkan pula kepada Sulaiman) segolongan setan-setan yang menyelam (ke dalam laut) untuknya dan mereka mengerjakan pekerjaan selain itu; dan Kami yang memelihara mereka itu.

21:83

# ۞ وَاَيُّوْبَ اِذْ نَادٰى رَبَّهٗٓ اَنِّيْ مَسَّنِيَ الضُّرُّ وَاَنْتَ اَرْحَمُ الرَّاحِمِيْنَ ۚ

wa-ayyuuba i*dz* n*aa*d*aa* rabbahu annii massaniya **al***dhdh*urru wa-anta ar*h*amu **al**rr*aah*imiin**a**

Dan (ingatlah kisah) Ayub, ketika dia berdoa kepada Tuhannya, “(Ya Tuhanku), sungguh, aku telah ditimpa penyakit, padahal Engkau Tuhan Yang Maha Penyayang dari semua yang penyayang.”

21:84

# فَاسْتَجَبْنَا لَهٗ فَكَشَفْنَا مَا بِهٖ مِنْ ضُرٍّ وَّاٰتَيْنٰهُ اَهْلَهٗ وَمِثْلَهُمْ مَّعَهُمْ رَحْمَةً مِّنْ عِنْدِنَا وَذِكْرٰى لِلْعٰبِدِيْنَ ۚ

fa**i**stajabn*aa* lahu fakasyafn*aa* m*aa* bihi min *dh*urrin wa*aa*tayn*aa*hu ahlahu wamitslahum ma'ahum ra*h*matan min 'indin*aa* wa*dz*ikr*aa* lil'*aa*bidiin**a**

Maka Kami kabulkan (doa)nya, lalu Kami lenyapkan penyakit yang ada padanya dan Kami kembalikan keluarganya kepadanya, dan (Kami lipat gandakan jumlah mereka) sebagai suatu rahmat dari Kami, dan untuk menjadi peringatan bagi semua yang menyembah Kami.

21:85

# وَاِسْمٰعِيْلَ وَاِدْرِيْسَ وَذَا الْكِفْلِۗ كُلٌّ مِّنَ الصّٰبِرِيْنَ ۙ

wa-ism*aa*'iila wa-idriisa wa*dzaa* **a**lkifli kullun mina **al***shshaa*biriin**a**

Dan (ingatlah kisah) Ismail, Idris dan Zulkifli. Mereka semua termasuk orang-orang yang sabar.

21:86

# وَاَدْخَلْنٰهُمْ فِيْ رَحْمَتِنَاۗ اِنَّهُمْ مِّنَ الصّٰلِحِيْنَ

wa-adkhaln*aa*hum fii ra*h*matin*aa* innahum mina **al***shshaa*li*h*iin**a**

Dan Kami masukkan mereka ke dalam rahmat Kami. Sungguh, mereka termasuk orang-orang yang saleh.

21:87

# وَذَا النُّوْنِ اِذْ ذَّهَبَ مُغَاضِبًا فَظَنَّ اَنْ لَّنْ نَّقْدِرَ عَلَيْهِ فَنَادٰى فِى الظُّلُمٰتِ اَنْ لَّآ اِلٰهَ اِلَّآ اَنْتَ سُبْحٰنَكَ اِنِّيْ كُنْتُ مِنَ الظّٰلِمِيْنَ ۚ

wa*dzaa* **al**nnuuni i*dz* *dz*ahaba mugh*aad*iban fa*zh*anna an lan naqdira 'alayhi fan*aa*d*aa* fii **al***zhzh*ulum*aa*ti an l*aa* il*aa*ha ill*aa* anta sub*haa*

*Dan (ingatlah kisah) Zun Nun (Yunus), ketika dia pergi dalam keadaan marah, lalu dia menyangka bahwa Kami tidak akan menyulitkannya, maka dia berdoa dalam keadaan yang sangat gelap, ”Tidak ada tuhan selain Engkau, Mahasuci Engkau. Sungguh, aku termasuk o*









21:88

# فَاسْتَجَبْنَا لَهٗۙ وَنَجَّيْنٰهُ مِنَ الْغَمِّۗ وَكَذٰلِكَ نُـْۨجِى الْمُؤْمِنِيْنَ

fa**i**stajabn*aa* lahu wanajjayn*aa*hu mina **a**lghammi waka*dzaa*lika nunjii **a**lmu/miniin**a**

Maka Kami kabulkan (doa)nya dan Kami selamatkan dia dari kedukaan. Dan demikianlah Kami menyelamatkan orang-orang yang beriman.

21:89

# وَزَكَرِيَّآ اِذْ نَادٰى رَبَّهٗ رَبِّ لَا تَذَرْنِيْ فَرْدًا وَّاَنْتَ خَيْرُ الْوٰرِثِيْنَ ۚ

wazakariyy*aa* i*dz* n*aa*d*aa* rabbahu rabbi l*aa* ta*dz*arnii fardan wa-anta khayru **a**lw*aa*ritsiin**a**

Dan (ingatlah kisah) Zakaria, ketika dia berdoa kepada Tuhannya, “Ya Tuhanku, janganlah Engkau biarkan aku hidup seorang diri (tanpa keturunan) dan Engkaulah ahli waris yang terbaik.

21:90

# فَاسْتَجَبْنَا لَهٗ ۖوَوَهَبْنَا لَهٗ يَحْيٰى وَاَصْلَحْنَا لَهٗ زَوْجَهٗۗ اِنَّهُمْ كَانُوْا يُسٰرِعُوْنَ فِى الْخَيْرٰتِ وَيَدْعُوْنَنَا رَغَبًا وَّرَهَبًاۗ وَكَانُوْا لَنَا خٰشِعِيْنَ

fa**i**stajabn*aa* lahu wawahabn*aa* lahu ya*h*y*aa* wa-a*sh*la*h*n*aa* lahu zawjahu innahum k*aa*nuu yus*aa*ri'uuna fii **a**lkhayr*aa*ti wayad'uunan*aa* raghaban warahaban

Maka Kami kabulkan (doa)nya, dan Kami anugerahkan kepadanya Yahya, dan Kami jadikan istrinya (dapat mengandung). Sungguh, mereka selalu bersegera dalam (mengerjakan) kebaikan, dan mereka berdoa kepada Kami dengan penuh harap dan cemas. Dan mereka orang-or

21:91

# وَالَّتِيْٓ اَحْصَنَتْ فَرْجَهَا فَنَفَخْنَا فِيْهَا مِنْ رُّوْحِنَا وَجَعَلْنٰهَا وَابْنَهَآ اٰيَةً لِّلْعٰلَمِيْنَ

wa**a**llatii a*hs*anat farjah*aa* fanafakhn*aa* fiih*aa* min ruu*h*in*aa* waja'aln*aa*h*aa* wa**i**bnah*aa* *aa*yatan lil'*aa*lamiin**a**

Dan (ingatlah kisah Maryam) yang memelihara kehormatannya, lalu Kami tiupkan (roh) dari Kami ke dalam (tubuh)nya; Kami jadikan dia dan anaknya sebagai tanda (kebesaran Allah) bagi seluruh alam.

21:92

# اِنَّ هٰذِهٖٓ اُمَّتُكُمْ اُمَّةً وَّاحِدَةًۖ وَّاَنَا۠ رَبُّكُمْ فَاعْبُدُوْنِ

inna h*aadz*ihi ummatukum ummatan w*aah*idatan wa-an*aa* rabbukum fau'buduun**i**

Sungguh, (agama tauhid) inilah agama kamu, agama yang satu, dan Aku adalah Tuhanmu, maka sembahlah Aku.

21:93

# وَتَقَطَّعُوْٓا اَمْرَهُمْ بَيْنَهُمْۗ كُلٌّ اِلَيْنَا رَاجِعُوْنَ ࣖ

wataqa*ththh*a'uu amrahum baynahum kullun ilayn*aa* r*aa*ji'uun**a**

Tetapi mereka terpecah belah dalam urusan (agama) mereka di antara mereka. Masing-masing (golongan itu semua) akan kembali kepada Kami.

21:94

# فَمَنْ يَّعْمَلْ مِنَ الصّٰلِحٰتِ وَهُوَ مُؤْمِنٌ فَلَا كُفْرَانَ لِسَعْيِهٖۚ وَاِنَّا لَهٗ كَاتِبُوْنَ

faman ya'mal mina **al***shshaa*li*haa*ti wahuwa mu/minun fal*aa* kufr*aa*na lisa'yihi wa-inn*aa* lahu k*aa*tibuun**a**

Barangsiapa mengerjakan kebajikan, dan dia beriman, maka usahanya tidak akan diingkari (disia-siakan), dan sungguh, Kamilah yang mencatat untuknya.

21:95

# وَحَرَامٌ عَلٰى قَرْيَةٍ اَهْلَكْنٰهَآ اَنَّهُمْ لَا يَرْجِعُوْنَ

wa*h*ar*aa*mun 'al*aa* qaryatin ahlakn*aa*h*aa* annahum l*aa* yarji'uun**a**

Dan tidak mungkin bagi (penduduk) suatu negeri yang telah Kami binasakan, bahwa mereka tidak akan kembali (kepada Kami).

21:96

# حَتّٰىٓ اِذَا فُتِحَتْ يَأْجُوْجُ وَمَأْجُوْجُ وَهُمْ مِّنْ كُلِّ حَدَبٍ يَّنْسِلُوْنَ

*h*att*aa* i*dzaa* futi*h*at ya/juuju wama/juuju wahum min kulli *h*adabin yansiluun**a**

Hingga apabila (tembok) Yakjuj dan Makjuj dibukakan dan mereka turun dengan cepat dari seluruh tempat yang tinggi.

21:97

# وَاقْتَرَبَ الْوَعْدُ الْحَقُّ فَاِذَا هِيَ شَاخِصَةٌ اَبْصَارُ الَّذِيْنَ كَفَرُوْاۗ يٰوَيْلَنَا قَدْ كُنَّا فِيْ غَفْلَةٍ مِّنْ هٰذَا بَلْ كُنَّا ظٰلِمِيْنَ

wa**i**qtaraba **a**lwa'du **a**l*h*aqqu fa-i*dzaa* hiya sy*aa*khi*sh*atun ab*shaa*ru **al**la*dz*iina kafaruu y*aa* waylan*aa* qad kunn*aa* fii ghaflatin min

Dan (apabila) janji yang benar (hari berbangkit) telah dekat, maka tiba-tiba mata orang-orang yang kafir terbelalak. (Mereka berkata), ”Alangkah celakanya kami! Kami benar-benar lengah tentang ini, bahkan kami benar-benar orang yang zalim.”

21:98

# اِنَّكُمْ وَمَا تَعْبُدُوْنَ مِنْ دُوْنِ اللّٰهِ حَصَبُ جَهَنَّمَۗ اَنْتُمْ لَهَا وَارِدُوْنَ

innakum wam*aa* ta'buduuna min duuni **al**l*aa*hi *h*a*sh*abu jahannama antum lah*aa* w*aa*riduun**a**

Sungguh, kamu (orang kafir) dan apa yang kamu sembah selain Allah, adalah bahan bakar Jahanam. Kamu (pasti) masuk ke dalamnya.

21:99

# لَوْ كَانَ هٰٓؤُلَاۤءِ اٰلِهَةً مَّا وَرَدُوْهَاۗ وَكُلٌّ فِيْهَا خٰلِدُوْنَ

law k*aa*na h*aa*ul*aa*-i *aa*lihatan m*aa* waraduuh*aa* wakullun fiih*aa* kh*aa*liduun**a**

Seandainya (berhala-berhala) itu tuhan, tentu mereka tidak akan memasukinya (neraka). Tetapi semuanya akan kekal di dalamnya.

21:100

# لَهُمْ فِيْهَا زَفِيْرٌ وَّهُمْ فِيْهَا لَا يَسْمَعُوْنَ

lahum fiih*aa* zafiirun wahum fiih*aa* l*aa* yasma'uun**a**

Mereka merintih dan menjerit di dalamnya (neraka), dan mereka di dalamnya tidak dapat mendengar.

21:101

# اِنَّ الَّذِيْنَ سَبَقَتْ لَهُمْ مِّنَّا الْحُسْنٰىٓۙ اُولٰۤىِٕكَ عَنْهَا مُبْعَدُوْنَ ۙ

inna **al**la*dz*iina sabaqat lahum minn*aa* **a**l*h*usn*aa* ul*aa*-ika 'anh*aa* mub'aduun**a**

Sungguh, sejak dahulu bagi orang-orang yang telah ada (ketetapan) yang baik dari Kami, mereka itu akan dijauhkan (dari neraka).

21:102

# لَا يَسْمَعُوْنَ حَسِيْسَهَاۚ وَهُمْ فِيْ مَا اشْتَهَتْ اَنْفُسُهُمْ خٰلِدُوْنَ ۚ

l*aa* yasma'uuna *h*asiisah*aa* wahum fii m*aa* isytahat anfusuhum kh*aa*liduun**a**

Mereka tidak mendengar bunyi desis (api neraka), dan mereka kekal dalam (menikmati) semua yang mereka ingini.

21:103

# لَا يَحْزُنُهُمُ الْفَزَعُ الْاَكْبَرُ وَتَتَلَقّٰىهُمُ الْمَلٰۤىِٕكَةُۗ هٰذَا يَوْمُكُمُ الَّذِيْ كُنْتُمْ تُوْعَدُوْنَ

l*aa* ya*h*zunuhumu **a**lfaza'u **a**l-akbaru watatalaqq*aa*humu **a**lmal*aa*-ikatu h*aadzaa* yawmukumu **al**la*dz*ii kuntum tuu'aduun**a**

Kejutan yang dahsyat tidak membuat mereka merasa sedih, dan para malaikat akan menyambut mereka (dengan ucapan), “Inilah harimu yang telah dijanjikan kepadamu.”

21:104

# يَوْمَ نَطْوِى السَّمَاۤءَ كَطَيِّ السِّجِلِّ لِلْكُتُبِۗ كَمَا بَدَأْنَآ اَوَّلَ خَلْقٍ نُّعِيْدُهٗۗ وَعْدًا عَلَيْنَاۗ اِنَّا كُنَّا فٰعِلِيْنَ

yawma na*th*wii **al**ssam*aa*-a ka*th*ayyi **al**ssijlli lilkutubi kam*aa* bada/n*aa* awwala khalqin nu'iiduhu wa'dan 'alayn*aa* inn*aa* kunn*aa* f*aa*'iliin**a**

(Ingatlah) pada hari langit Kami gulung seperti menggulung lembaran-lembaran kertas. Sebagaimana Kami telah memulai penciptaan pertama, begitulah Kami akan mengulanginya lagi. (Suatu) janji yang pasti Kami tepati; sungguh, Kami akan melaksanakannya.

21:105

# وَلَقَدْ كَتَبْنَا فِى الزَّبُوْرِ مِنْۢ بَعْدِ الذِّكْرِ اَنَّ الْاَرْضَ يَرِثُهَا عِبَادِيَ الصّٰلِحُوْنَ

walaqad katabn*aa* fii **al**zzabuuri min ba'di **al***dzdz*ikri anna **a**l-ar*dh*a yaritsuh*aa* 'ib*aa*diya **al***shshaa*li*h*uun**a**

Dan sungguh, telah Kami tulis di dalam Zabur setelah (tertulis) di dalam Az-Zikr (Lauh Mahfuzh), bahwa bumi ini akan diwarisi oleh hamba-hamba-Ku yang saleh.

21:106

# اِنَّ فِيْ هٰذَا لَبَلٰغًا لِّقَوْمٍ عٰبِدِيْنَ ۗ

inna fii h*aadzaa* labal*aa*ghan liqawmin '*aa*bidiin**a**

Sungguh, (apa yang disebutkan) di dalam (Al-Qur'an) ini, benar-benar menjadi petunjuk (yang lengkap) bagi orang-orang yang menyembah (Allah).

21:107

# وَمَآ اَرْسَلْنٰكَ اِلَّا رَحْمَةً لِّلْعٰلَمِيْنَ

wam*aa* arsaln*aa*ka ill*aa* ra*h*matan lil'*aa*lamiin**a**

Dan Kami tidak mengutus engkau (Muhammad) melainkan untuk (menjadi) rahmat bagi seluruh alam.

21:108

# قُلْ اِنَّمَا يُوْحٰٓى اِلَيَّ اَنَّمَآ اِلٰهُكُمْ اِلٰهٌ وَّاحِدٌۚ فَهَلْ اَنْتُمْ مُّسْلِمُوْنَ

qul innam*aa* yuu*haa* ilayya annam*aa* il*aa*hukum il*aa*hun w*aah*idun fahal antum muslimuun**a**

Katakanlah (Muhammad), “Sungguh, apa yang diwahyukan kepadaku ialah bahwa Tuhanmu adalah Tuhan Yang Esa, maka apakah kamu telah berserah diri (kepada-Nya)?”

21:109

# فَاِنْ تَوَلَّوْا فَقُلْ اٰذَنْتُكُمْ عَلٰى سَوَاۤءٍۗ وَاِنْ اَدْرِيْٓ اَقَرِيْبٌ اَمْ بَعِيْدٌ مَّا تُوْعَدُوْنَ

fa-in tawallaw faqul *aadz*antukum 'al*aa* saw*aa*-in wa-in adrii aqariibun am ba'iidun m*aa* tuu'aduun**a**

Maka jika mereka berpaling, maka katakanlah (Muhammad), “Aku telah menyampaikan kepadamu (ajaran) yang sama (antara kita) dan aku tidak tahu apakah yang diancamkan kepadamu itu sudah dekat atau masih jauh.”

21:110

# اِنَّهٗ يَعْلَمُ الْجَهْرَ مِنَ الْقَوْلِ وَيَعْلَمُ مَا تَكْتُمُوْنَ

innahu ya'lamu **a**ljahra mina **a**lqawli waya'lamu m*aa* taktumuun**a**

Sungguh, Dia (Allah) mengetahui perkataan (yang kamu ucapkan) dengan terang-terangan, dan mengetahui (pula) apa yang kamu rahasiakan.

21:111

# وَاِنْ اَدْرِيْ لَعَلَّهٗ فِتْنَةٌ لَّكُمْ وَمَتَاعٌ اِلٰى حِيْنٍ

wa-in adrii la'allahu fitnatun lakum wamat*aa*'un il*aa* *h*iin**in**

Dan aku tidak tahu, boleh jadi hal itu cobaan bagi kamu dan kesenangan sampai waktu yang ditentukan.

21:112

# قَالَ رَبِّ احْكُمْ بِالْحَقِّۗ وَرَبُّنَا الرَّحْمٰنُ الْمُسْتَعَانُ عَلٰى مَا تَصِفُوْنَ ࣖ

q*aa*la rabbi u*h*kum bi**a**l*h*aqqi warabbun*aa* **al**rra*h*m*aa*nu **a**lmusta'*aa*nu 'al*aa* m*aa* ta*sh*ifuun**a**

Dia (Muhammad) berkata, “Ya Tuhanku, berilah keputusan dengan adil. Dan Tuhan kami Maha Pengasih, tempat memohon segala pertolongan atas semua yang kamu katakan.”

<!--EndFragment-->