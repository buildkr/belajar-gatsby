---
title: (39) Az-Zumar - الزمر
date: 2021-10-27T03:57:46.558Z
ayat: 39
description: "Jumlah Ayat: 75 / Arti: Rombongan"
---
<!--StartFragment-->

39:1

# تَنْزِيْلُ الْكِتٰبِ مِنَ اللّٰهِ الْعَزِيْزِ الْحَكِيْمِ

tanziilu **a**lkit*aa*bi mina **al**l*aa*hi **a**l'aziizi **a**l*h*akiim**i**

Kitab (Al-Qur'an) ini diturunkan oleh Allah Yang Mahamulia, Mahabijaksana.

39:2

# اِنَّآ اَنْزَلْنَآ اِلَيْكَ الْكِتٰبَ بِالْحَقِّ فَاعْبُدِ اللّٰهَ مُخْلِصًا لَّهُ الدِّيْنَۗ

inn*aa* anzaln*aa* ilayka **a**lkit*aa*ba bi**a**l*h*aqqi fa**u**'budi **al**l*aa*ha mukhli*sh*an lahu **al**ddiin**a**

Sesungguhnya Kami menurunkan Kitab (Al-Qur'an) kepadamu (Muhammad) dengan (membawa) kebenaran. Maka sembahlah Allah dengan tulus ikhlas beragama kepada-Nya.

39:3

# اَلَا لِلّٰهِ الدِّيْنُ الْخَالِصُ ۗوَالَّذِيْنَ اتَّخَذُوْا مِنْ دُوْنِهٖٓ اَوْلِيَاۤءَۘ مَا نَعْبُدُهُمْ اِلَّا لِيُقَرِّبُوْنَآ اِلَى اللّٰهِ زُلْفٰىۗ اِنَّ اللّٰهَ يَحْكُمُ بَيْنَهُمْ فِيْ مَا هُمْ فِيْهِ يَخْتَلِفُوْنَ ەۗ اِنَّ اللّٰهَ لَا يَهْدِيْ

al*aa* lill*aa*hi **al**ddiinu **a**lkh*aa*li*sh*u wa**a**lla*dz*iina ittakha*dz*uu min duunihi awliy*aa*-a m*aa* na'buduhum ill*aa* liyuqarribuun*aa* il*aa*

Ingatlah! Hanya milik Allah agama yang murni (dari syirik). Dan orang-orang yang mengambil pelindung selain Dia (berkata), “Kami tidak menyembah mereka melainkan (berharap) agar mereka mendekatkan kami kepada Allah dengan sedekat-dekatnya.” Sungguh, Allah

39:4

# لَوْ اَرَادَ اللّٰهُ اَنْ يَّتَّخِذَ وَلَدًا لَّاصْطَفٰى مِمَّا يَخْلُقُ مَا يَشَاۤءُ ۙ سُبْحٰنَهٗ ۗهُوَ اللّٰهُ الْوَاحِدُ الْقَهَّارُ

law ar*aa*da **al**l*aa*hu an yattakhi*dz*a waladan la**i***sth*af*aa* mimm*aa* yakhluqu m*aa* yasy*aa*u sub*haa*nahu huwa **al**l*aa*hu **a**lw*aah*

*Sekiranya Allah hendak mengambil anak, tentu Dia akan memilih apa yang Dia kehendaki dari apa yang telah diciptakan-Nya. Mahasuci Dia. Dialah Allah Yang Maha Esa, Mahaperkasa.*









39:5

# خَلَقَ السَّمٰوٰتِ وَالْاَرْضَ بِالْحَقِّۚ يُكَوِّرُ الَّيْلَ عَلَى النَّهَارِ وَيُكَوِّرُ النَّهَارَ عَلَى الَّيْلِ وَسَخَّرَ الشَّمْسَ وَالْقَمَرَۗ كُلٌّ يَّجْرِيْ لِاَجَلٍ مُّسَمًّىۗ اَلَا هُوَ الْعَزِيْزُ الْغَفَّارُ

khalaqa **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*a bi**a**l*h*aqqi yukawwiru **al**layla 'al*aa* **al**nnah*aa*ri wayukawwiru **al**nnah*aa*ra 'al

Dia menciptakan langit dan bumi dengan (tujuan) yang benar; Dia memasukkan malam atas siang dan memasukkan siang atas malam dan menundukkan matahari dan bulan, masing-masing berjalan menurut waktu yang ditentukan. Ingatlah! Dialah Yang Mahamulia, Maha Pen

39:6

# خَلَقَكُمْ مِّنْ نَّفْسٍ وَّاحِدَةٍ ثُمَّ جَعَلَ مِنْهَا زَوْجَهَا وَاَنْزَلَ لَكُمْ مِّنَ الْاَنْعَامِ ثَمٰنِيَةَ اَزْوَاجٍ ۗ يَخْلُقُكُمْ فِيْ بُطُوْنِ اُمَّهٰتِكُمْ خَلْقًا مِّنْۢ بَعْدِ خَلْقٍ فِيْ ظُلُمٰتٍ ثَلٰثٍۗ ذٰلِكُمُ اللّٰهُ رَبُّكُمْ لَهُ الْم

khalaqakum min nafsin w*aah*idatin tsumma ja'ala minh*aa* zawjah*aa* wa-anzala lakum mina **a**l-an'*aa*mi tsam*aa*niyata azw*aa*jin yakhluqukum fii bu*th*uuni ummah*aa*tikum khalqan min ba'di khalqin f

Dia menciptakan kamu dari diri yang satu (Adam) kemudian darinya Dia jadikan pasangannya dan Dia menurunkan delapan pasang hewan ternak untukmu. Dia menjadikan kamu dalam perut ibumu kejadian demi kejadian dalam tiga kegelapan. Yang (berbuat) demikian itu

39:7

# اِنْ تَكْفُرُوْا فَاِنَّ اللّٰهَ غَنِيٌّ عَنْكُمْ ۗوَلَا يَرْضٰى لِعِبَادِهِ الْكُفْرَۚ وَاِنْ تَشْكُرُوْا يَرْضَهُ لَكُمْۗ وَلَا تَزِرُ وَازِرَةٌ وِّزْرَ اُخْرٰىۗ ثُمَّ اِلٰى رَبِّكُمْ مَّرْجِعُكُمْ فَيُنَبِّئُكُمْ بِمَا كُنْتُمْ تَعْمَلُوْنَۗ اِنَّهٗ عَ

in takfuruu fa-inna **al**l*aa*ha ghaniyyun 'ankum wal*aa* yar*daa* li'ib*aa*dihi **a**lkufra wa-in tasykuruu yar*dh*ahu lakum wal*aa* taziru w*aa*ziratun wizra ukhr*aa* tsumma il*aa*

Jika kamu kafir (ketahuilah) maka sesungguhnya Allah tidak memerlukanmu dan Dia tidak meridai kekafiran hamba-hamba-Nya. Jika kamu bersyukur, Dia meridai kesyukuranmu itu. Seseorang yang berdosa tidak memikul dosa orang lain. Kemudian kepada Tuhanmulah ke

39:8

# وَاِذَا مَسَّ الْاِنْسَانَ ضُرٌّ دَعَا رَبَّهٗ مُنِيْبًا اِلَيْهِ ثُمَّ اِذَا خَوَّلَهٗ نِعْمَةً مِّنْهُ نَسِيَ مَا كَانَ يَدْعُوْٓا اِلَيْهِ مِنْ قَبْلُ وَجَعَلَ لِلّٰهِ اَنْدَادًا لِّيُضِلَّ عَنْ سَبِيْلِهٖ ۗ قُلْ تَمَتَّعْ بِكُفْرِكَ قَلِيْلًا ۖاِنَّكَ

wa-i*dzaa* massa **a**l-ins*aa*na *dh*urrun da'*aa* rabbahu muniiban ilayhi tsumma i*dzaa* khawwalahu ni'matan minhu nasiya m*aa* k*aa*na yad'uu ilayhi min qablu waja'ala lill*aa*hi and*aa*dan liyu

Dan apabila manusia ditimpa bencana, dia memohon (pertolongan) kepada Tuhannya dengan kembali (taat) kepada-Nya; tetapi apabila Dia memberikan nikmat kepadanya dia lupa (akan bencana) yang pernah dia berdoa kepada Allah sebelum itu, dan diadakannya sekutu

39:9

# اَمَّنْ هُوَ قَانِتٌ اٰنَاۤءَ الَّيْلِ سَاجِدًا وَّقَاۤىِٕمًا يَّحْذَرُ الْاٰخِرَةَ وَيَرْجُوْا رَحْمَةَ رَبِّهٖۗ قُلْ هَلْ يَسْتَوِى الَّذِيْنَ يَعْلَمُوْنَ وَالَّذِيْنَ لَا يَعْلَمُوْنَ ۗ اِنَّمَا يَتَذَكَّرُ اُولُوا الْاَلْبَابِ ࣖ

amman huwa q*aa*nitun *aa*n*aa*-a **al**layli s*aa*jidan waq*aa*-iman ya*hts*aru **a**l-*aa*khirata wayarjuu ra*h*mata rabbihi qul hal yastawii **al**la*dz*iina ya'lamuuna

(Apakah kamu orang musyrik yang lebih beruntung) ataukah orang yang beribadah pada waktu malam dengan sujud dan berdiri, karena takut kepada (azab) akhirat dan mengharapkan rahmat Tuhannya? Katakanlah, “Apakah sama orang-orang yang mengetahui dengan orang

39:10

# قُلْ يٰعِبَادِ الَّذِيْنَ اٰمَنُوا اتَّقُوْا رَبَّكُمْ ۗلِلَّذِيْنَ اَحْسَنُوْا فِيْ هٰذِهِ الدُّنْيَا حَسَنَةٌ ۗوَاَرْضُ اللّٰهِ وَاسِعَةٌ ۗاِنَّمَا يُوَفَّى الصّٰبِرُوْنَ اَجْرَهُمْ بِغَيْرِ حِسَابٍ

qul y*aa* 'ib*aa*di **al**la*dz*iina *aa*manuu ittaquu rabbakum lilla*dz*iina a*h*sanuu fii h*aadz*ihi **al**dduny*aa* *h*asanatun wa-ar*dh*u **al**l*aa*hi w*a*

Katakanlah (Muhammad), “Wahai hamba-hamba-Ku yang beriman! Bertakwalah kepada Tuhanmu.” Bagi orang-orang yang berbuat baik di dunia ini akan memperoleh kebaikan. Dan bumi Allah itu luas. Hanya orang-orang yang bersabarlah yang disempurnakan pahalanya tanp







39:11

# قُلْ اِنِّيْٓ اُمِرْتُ اَنْ اَعْبُدَ اللّٰهَ مُخْلِصًا لَّهُ الدِّيْنَ

qul innii umirtu an a'buda **al**l*aa*ha mukhli*sh*an lahu **al**ddiin**a**

Katakanlah, “Sesungguhnya aku diperintahkan agar menyembah Allah dengan penuh ketaatan kepada-Nya dalam (menjalankan) agama.

39:12

# وَاُمِرْتُ لِاَنْ اَكُوْنَ اَوَّلَ الْمُسْلِمِيْنَ

waumirtu li-an akuuna awwala **a**lmuslimiin**a**

Dan aku diperintahkan agar menjadi orang yang pertama-tama berserah diri.”

39:13

# قُلْ اِنِّيْٓ اَخَافُ اِنْ عَصَيْتُ رَبِّيْ عَذَابَ يَوْمٍ عَظِيْمٍ

qul innii akh*aa*fu in 'a*sh*aytu rabbii 'a*dzaa*ba yawmin 'a*zh*iim**in**

Katakanlah, “Sesungguhnya aku takut akan azab pada hari yang besar jika aku durhaka kepada Tuhanku.”

39:14

# قُلِ اللّٰهَ اَعْبُدُ مُخْلِصًا لَّهٗ دِيْنِيْۚ

quli **al**l*aa*ha a'budu mukhli*sh*an lahu diinii

Katakanlah, “Hanya Allah yang aku sembah dengan penuh ketaatan kepada-Nya dalam (menjalankan) agamaku.”

39:15

# فَاعْبُدُوْا مَا شِئْتُمْ مِّنْ دُوْنِهٖۗ قُلْ اِنَّ الْخٰسِرِيْنَ الَّذِيْنَ خَسِرُوْٓا اَنْفُسَهُمْ وَاَهْلِيْهِمْ يَوْمَ الْقِيٰمَةِۗ اَلَا ذٰلِكَ هُوَ الْخُسْرَانُ الْمُبِيْنُ

fa**u**'buduu m*aa* syi/tum min duunihi qul inna **a**lkh*aa*siriina **al**la*dz*iina khasiruu anfusahum wa-ahliihim yawma **a**lqiy*aa*mati **a**l*aa* *dzaa*li

Maka sembahlah selain Dia sesukamu! (wahai orang-orang musyrik). Katakanlah, “Sesungguhnya orang-orang yang rugi ialah orang-orang yang merugikan diri mereka sendiri dan keluarganya pada hari Kiamat.” Ingatlah! Yang demikian itu adalah kerugian yang nyata

39:16

# لَهُمْ مِّنْ فَوْقِهِمْ ظُلَلٌ مِّنَ النَّارِ وَمِنْ تَحْتِهِمْ ظُلَلٌ ۗذٰلِكَ يُخَوِّفُ اللّٰهُ بِهٖ عِبَادَهٗ ۗيٰعِبَادِ فَاتَّقُوْنِ

lahum min fawqihim *zh*ulalun mina **al**nn*aa*ri wamin ta*h*tihim *zh*ulalun *dzaa*lika yukhawwifu **al**l*aa*hu bihi 'ib*aa*dahu y*aa* 'ib*aa*di fa**i**ttaquun**i<**

Di atas mereka ada lapisan-lapisan dari api dan di bawahnya juga ada lapisan-lapisan yang disediakan bagi mereka. Demikianlah Allah mengancam hamba-hamba-Nya (dengan azab itu). “Wahai hamba-hamba-Ku, maka bertakwalah kepada-Ku.”







39:17

# وَالَّذِيْنَ اجْتَنَبُوا الطَّاغُوْتَ اَنْ يَّعْبُدُوْهَا وَاَنَابُوْٓا اِلَى اللّٰهِ لَهُمُ الْبُشْرٰىۚ فَبَشِّرْ عِبَادِۙ

wa**a**lla*dz*iina ijtanabuu **al***ththaa*ghuuta an ya'buduuh*aa* wa-an*aa*buu il*aa* **al**l*aa*hi lahumu **a**lbusyr*aa* fabasysyir 'ib*aa*d**i**

Dan orang-orang yang menjauhi tagut (yaitu) tidak menyembahnya dan kembali kepada Allah, mereka pantas mendapat berita gembira; sebab itu sampaikanlah kabar gembira itu kepada hamba-hamba-Ku,

39:18

# الَّذِيْنَ يَسْتَمِعُوْنَ الْقَوْلَ فَيَتَّبِعُوْنَ اَحْسَنَهٗ ۗ اُولٰۤىِٕكَ الَّذِيْنَ هَدٰىهُمُ اللّٰهُ وَاُولٰۤىِٕكَ هُمْ اُولُوا الْاَلْبَابِ

**al**la*dz*iina yastami'uuna **a**lqawla fayattabi'uuna a*h*sanahu ul*aa*-ika **al**la*dz*iina had*aa*humu **al**l*aa*hu waul*aa*-ika hum uluu **a**l-alb<

(yaitu) mereka yang mendengarkan perkataan lalu mengikuti apa yang paling baik di antaranya. Mereka itulah orang-orang yang telah diberi petunjuk oleh Allah dan mereka itulah orang-orang yang mempunyai akal sehat.

39:19

# اَفَمَنْ حَقَّ عَلَيْهِ كَلِمَةُ الْعَذَابِۗ اَفَاَنْتَ تُنْقِذُ مَنْ فِى النَّارِ ۚ

afaman *h*aqqa 'alayhi kalimatu **a**l'a*dzaa*bi afa-anta tunqi*dz*u man fii **al**nn*aa*r**i**

Maka apakah (engkau hendak mengubah nasib) orang-orang yang telah dipastikan mendapat azab? Apakah engkau (Muhammad) akan menyelamatkan orang yang berada dalam api neraka?

39:20

# لٰكِنِ الَّذِيْنَ اتَّقَوْا رَبَّهُمْ لَهُمْ غُرَفٌ مِّنْ فَوْقِهَا غُرَفٌ مَّبْنِيَّةٌ ۙتَجْرِيْ مِنْ تَحْتِهَا الْاَنْهٰرُ ەۗ وَعْدَ اللّٰهِ ۗ لَا يُخْلِفُ اللّٰهُ الْمِيْعَادَ

l*aa*kini **al**la*dz*iina ittaqaw rabbahum lahum ghurafun min fawqih*aa* ghurafun mabniyyatun tajrii min ta*h*tih*aa* **a**l-anh*aa*ru wa'da **al**l*aa*hi l*aa* yukhlifu

Tetapi orang-orang yang bertakwa kepada Tuhannya, mereka mendapat kamar-kamar (di surga), di atasnya terdapat pula kamar-kamar yang dibangun (bertingkat-tingkat), yang mengalir di bawahnya sungai-sungai. (Itulah) janji Allah. Allah tidak akan memungkiri j

39:21

# اَلَمْ تَرَ اَنَّ اللّٰهَ اَنْزَلَ مِنَ السَّمَاۤءِ مَاۤءً فَسَلَكَهٗ يَنَابِيْعَ فِى الْاَرْضِ ثُمَّ يُخْرِجُ بِهٖ زَرْعًا مُّخْتَلِفًا اَلْوَانُهٗ ثُمَّ يَهِيْجُ فَتَرٰىهُ مُصْفَرًّا ثُمَّ يَجْعَلُهٗ حُطَامًا ۗاِنَّ فِيْ ذٰلِكَ لَذِكْرٰى لِاُولِى الْاَل

alam tara anna **al**l*aa*ha anzala mina **al**ssam*aa*-i m*aa*-an fasalakahu yan*aa*bii'a fii **a**l-ar*dh*i tsumma yukhriju bihi zar'an mukhtalifan **a**lw*aa*nuhu tsumma

Apakah engkau tidak memperhatikan, bahwa Allah menurunkan air dari langit, lalu diaturnya menjadi sumber-sumber air di bumi, kemudian dengan air itu ditumbuhkan-Nya tanam-tanaman yang bermacam-macam warnanya, kemudian menjadi kering, lalu engkau melihatny

39:22

# اَفَمَنْ شَرَحَ اللّٰهُ صَدْرَهٗ لِلْاِسْلَامِ فَهُوَ عَلٰى نُوْرٍ مِّنْ رَّبِّهٖ ۗفَوَيْلٌ لِّلْقٰسِيَةِ قُلُوْبُهُمْ مِّنْ ذِكْرِ اللّٰهِ ۗ اُولٰۤىِٕكَ فِيْ ضَلٰلٍ مُّبِيْنٍ

afaman syara*h*a **al**l*aa*hu *sh*adrahu lil-isl*aa*mi fahuwa 'al*aa* nuurin min rabbihi fawaylun lilq*aa*siyati quluubuhum min *dz*ikri **al**l*aa*hi ul*aa*-ika fii *dh*al*aa<*

Maka apakah orang-orang yang dibukakan hatinya oleh Allah untuk (menerima) agama Islam lalu dia mendapat cahaya dari Tuhannya (sama dengan orang yang hatinya membatu)? Maka celakalah mereka yang hatinya telah membatu untuk mengingat Allah. Mereka itu dal







39:23

# اَللّٰهُ نَزَّلَ اَحْسَنَ الْحَدِيْثِ كِتٰبًا مُّتَشَابِهًا مَّثَانِيَۙ تَقْشَعِرُّ مِنْهُ جُلُوْدُ الَّذِيْنَ يَخْشَوْنَ رَبَّهُمْ ۚ ثُمَّ تَلِيْنُ جُلُوْدُهُمْ وَقُلُوْبُهُمْ اِلٰى ذِكْرِ اللّٰهِ ۗذٰلِكَ هُدَى اللّٰهِ يَهْدِيْ بِهٖ مَنْ يَّشَاۤءُ ۗوَمَ

**al**l*aa*hu nazzala a*h*sana **a**l*h*adiitsi kit*aa*ban mutasy*aa*bihan mats*aa*niya taqsya'irru minhu juluudu **al**la*dz*iina yakhsyawna rabbahum tsumma taliinu juluuduhum waqu

Allah telah menurunkan perkataan yang paling baik (yaitu) Al-Qur'an yang serupa (ayat-ayatnya) lagi berulang-ulang, gemetar karenanya kulit orang-orang yang takut kepada Tuhannya, kemudian menjadi tenang kulit dan hati mereka ketika mengingat Allah. Itula

39:24

# اَفَمَنْ يَّتَّقِيْ بِوَجْهِهٖ سُوْۤءَ الْعَذَابِ يَوْمَ الْقِيٰمَةِ ۗوَقِيْلَ لِلظّٰلِمِيْنَ ذُوْقُوْا مَا كُنْتُمْ تَكْسِبُوْنَ

afaman yattaqii biwajhihi suu-a **a**l'a*dzaa*bi yawma **a**lqiy*aa*mati waqiila li**l***zhzhaa*limiina *dz*uuquu m*aa* kuntum taksibuun**a**

Maka apakah orang-orang yang melindungi wajahnya menghindari azab yang buruk pada hari Kiamat (sama dengan orang mukmin yang tidak kena azab)? Dan dikatakan kepada orang-orang yang zalim, “Rasakanlah olehmu balasan apa yang telah kamu kerjakan.”

39:25

# كَذَّبَ الَّذِيْنَ مِنْ قَبْلِهِمْ فَاَتٰىهُمُ الْعَذَابُ مِنْ حَيْثُ لَا يَشْعُرُوْنَ

ka*dzdz*aba **al**la*dz*iina min qablihim fa-at*aa*humu **a**l'a*dzaa*bu min *h*aytsu l*aa* yasy'uruun**a**

Orang-orang yang sebelum mereka telah mendustakan (rasul-rasul), maka datanglah kepada mereka azab dari arah yang tidak mereka sangka.

39:26

# فَاَذَاقَهُمُ اللّٰهُ الْخِزْيَ فِى الْحَيٰوةِ الدُّنْيَا ۚوَلَعَذَابُ الْاٰخِرَةِ اَكْبَرُ ۘ لَوْ كَانُوْا يَعْلَمُوْنَ

fa-a*dzaa*qahumu **al**l*aa*hu **a**lkhizya fii **a**l*h*ay*aa*ti **al**dduny*aa* wala'a*dzaa*bu **a**l-*aa*khirati akbaru law k*aa*nuu ya'lamuun

Maka Allah menimpakan kepada mereka kehinaan pada kehidupan dunia. Dan sungguh, azab akhirat lebih besar, kalau (saja) mereka mengetahui.

39:27

# وَلَقَدْ ضَرَبْنَا لِلنَّاسِ فِيْ هٰذَا الْقُرْاٰنِ مِنْ كُلِّ مَثَلٍ لَّعَلَّهُمْ يَتَذَكَّرُوْنَۚ

walaqad *dh*arabn*aa* li**l**nn*aa*si fii h*aadzaa* **a**lqur-*aa*ni min kulli matsalin la'allahum yata*dz*akkaruun**a**

Dan sungguh, telah Kami buatkan dalam Al-Qur'an ini segala macam perumpamaan bagi manusia agar mereka dapat pelajaran.

39:28

# قُرْاٰنًا عَرَبِيًّا غَيْرَ ذِيْ عِوَجٍ لَّعَلَّهُمْ يَتَّقُوْنَ

qur-*aa*nan 'arabiyyan ghayra *dz*ii 'iwajin la'allahum yattaquun**a**

(Yaitu) Al-Qur'an dalam bahasa Arab, tidak ada kebengkokan (di dalamnya) agar mereka bertakwa.

39:29

# ضَرَبَ اللّٰهُ مَثَلًا رَّجُلًا فِيْهِ شُرَكَاۤءُ مُتَشَاكِسُوْنَ وَرَجُلًا سَلَمًا لِّرَجُلٍ هَلْ يَسْتَوِيٰنِ مَثَلًا ۗ اَلْحَمْدُ لِلّٰهِ ۗبَلْ اَكْثَرُهُمْ لَا يَعْلَمُوْنَ

*dh*araba **al**l*aa*hu matsalan rajulan fiihi syurak*aa*u mutasy*aa*kisuuna warajulan salaman lirajulin hal yastawiy*aa*ni matsalan **a**l*h*amdu lill*aa*hi bal aktsaruhum l*aa* ya'lamuun<

Allah membuat perumpamaan (yaitu) seorang laki-laki (hamba sahaya) yang dimiliki oleh beberapa orang yang berserikat yang dalam perselisihan, dan seorang hamba sahaya yang menjadi milik penuh dari seorang (saja). Adakah kedua hamba sahaya itu sama keadaan

39:30

# اِنَّكَ مَيِّتٌ وَّاِنَّهُمْ مَّيِّتُوْنَ ۖ

innaka mayyitun wa-innahum mayyituun**a**

Sesungguhnya engkau (Muhammad) akan mati dan mereka akan mati (pula).

39:31

# ثُمَّ اِنَّكُمْ يَوْمَ الْقِيٰمَةِ عِنْدَ رَبِّكُمْ تَخْتَصِمُوْنَ ࣖ ۔

tsumma innakum yawma **a**lqiy*aa*mati 'inda rabbikum takhta*sh*imuun**a**

Kemudian sesungguhnya kamu pada hari Kiamat akan berbantah-bantahan di hadapan Tuhanmu.

39:32

# ۞ فَمَنْ اَظْلَمُ مِمَّنْ كَذَبَ عَلَى اللّٰهِ وَكَذَّبَ بِالصِّدْقِ اِذْ جَاۤءَهٗۗ اَلَيْسَ فِيْ جَهَنَّمَ مَثْوًى لِّلْكٰفِرِيْنَ

faman a*zh*lamu mimman ka*dz*aba 'al*aa* **al**l*aa*hi waka*dzdz*aba bi**al***shsh*idqi i*dz* j*aa*-ahu alaysa fii jahannama matswan lilk*aa*firiin**a**

Maka siapakah yang lebih zalim daripada orang yang membuat-buat kebohongan terhadap Allah dan mendustakan kebenaran yang datang kepadanya? Bukankah di neraka Jahanam tempat tinggal bagi orang-orang kafir?

39:33

# وَالَّذِيْ جَاۤءَ بِالصِّدْقِ وَصَدَّقَ بِهٖٓ اُولٰۤىِٕكَ هُمُ الْمُتَّقُوْنَ

wa**a**lla*dz*ii j*aa*-a bi**al***shsh*idqi wa*sh*addaqa bihi ul*aa*-ika humu **a**lmuttaquun**a**

Dan orang yang membawa kebenaran (Muhammad) dan orang yang membenarkannya, mereka itulah orang yang bertakwa.

39:34

# لَهُمْ مَّا يَشَاۤءُوْنَ عِنْدَ رَبِّهِمْ ۗ ذٰلِكَ جَزٰۤؤُا الْمُحْسِنِيْنَۚ

lahum m*aa* yasy*aa*uuna 'inda rabbihim *dzaa*lika jaz*aa*u **a**lmu*h*siniin**a**

Mereka memperoleh apa yang mereka kehendaki di sisi Tuhannya. Demikianlah balasan bagi orang-orang yang berbuat baik,

39:35

# لِيُكَفِّرَ اللّٰهُ عَنْهُمْ اَسْوَاَ الَّذِيْ عَمِلُوْا وَيَجْزِيَهُمْ اَجْرَهُمْ بِاَحْسَنِ الَّذِيْ كَانُوْا يَعْمَلُوْنَ

liyukaffira **al**l*aa*hu 'anhum aswa-a **al**la*dz*ii 'amiluu wayajziyahum ajrahum bi-a*h*sani **al**la*dz*ii k*aa*nuu ya'maluun**a**

agar Allah menghapus perbuatan mereka yang paling buruk yang pernah mereka lakukan dan memberi pahala kepada mereka dengan yang lebih baik daripada apa yang mereka kerjakan.

39:36

# اَلَيْسَ اللّٰهُ بِكَافٍ عَبْدَهٗۗ وَيُخَوِّفُوْنَكَ بِالَّذِيْنَ مِنْ دُوْنِهٖۗ وَمَنْ يُّضْلِلِ اللّٰهُ فَمَا لَهٗ مِنْ هَادٍۚ

alaysa **al**l*aa*hu bik*aa*fin 'abdahu wayukhawwifuunaka bi**a**lla*dz*iina min duunihi waman yu*dh*lili **al**l*aa*hu fam*aa* lahu min h*aa*d**in**

Bukankah Allah yang mencukupi hamba-Nya? Mereka menakut-nakutimu dengan (sesembahan) yang selain Dia. Barangsiapa dibiarkan sesat oleh Allah maka tidak seorang pun yang dapat memberi petunjuk kepadanya.

39:37

# وَمَنْ يَّهْدِ اللّٰهُ فَمَا لَهٗ مِنْ مُّضِلٍّ ۗ اَلَيْسَ اللّٰهُ بِعَزِيْزٍ ذِى انْتِقَامٍ

waman yahdi **al**l*aa*hu fam*aa* lahu min mu*dh*illin alaysa **al**l*aa*hu bi'aziizin *dz*ii intiq*aa*m**in**

Dan barangsiapa diberi petunjuk oleh Allah, maka tidak seorang pun yang dapat menyesatkannya. Bukankah Allah Mahaperkasa dan mempunyai (kekuasaan untuk) menghukum?

39:38

# وَلَىِٕنْ سَاَلْتَهُمْ مَّنْ خَلَقَ السَّمٰوٰتِ وَالْاَرْضَ لَيَقُوْلُنَّ اللّٰهُ ۗ قُلْ اَفَرَءَيْتُمْ مَّا تَدْعُوْنَ مِنْ دُوْنِ اللّٰهِ اِنْ اَرَادَنِيَ اللّٰهُ بِضُرٍّ هَلْ هُنَّ كٰشِفٰتُ ضُرِّهٖٓ اَوْ اَرَادَنِيْ بِرَحْمَةٍ هَلْ هُنَّ مُمْسِكٰتُ رَح

wala-in sa-altahum man khalaqa **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*a layaquulunna **al**l*aa*hu qul afara-aytum m*aa* tad'uuna min duuni **al**l*aa*hi in ar*aa*daniya

Dan sungguh, jika engkau tanyakan kepada mereka, “Siapakah yang menciptakan langit dan bumi?” Niscaya mereka menjawab, “Allah.” Katakanlah, “Kalau begitu tahukah kamu tentang apa yang kamu sembah selain Allah, jika Allah hendak mendatangkan bencana kepada

39:39

# قُلْ يٰقَوْمِ اعْمَلُوْا عَلٰى مَكَانَتِكُمْ اِنِّيْ عَامِلٌ ۚفَسَوْفَ تَعْلَمُوْنَۙ

qul y*aa* qawmi i'maluu 'al*aa* mak*aa*natikum innii '*aa*milun fasawfa ta'lamuun**a**

Katakanlah (Muhammad), “Wahai kaumku! Berbuatlah menurut kedudukanmu, aku pun berbuat (demikian). Kelak kamu akan mengetahui,

39:40

# مَنْ يَّأْتِيْهِ عَذَابٌ يُّخْزِيْهِ وَيَحِلُّ عَلَيْهِ عَذَابٌ مُّقِيْمٌ

man ya/tiihi 'a*dzaa*bun yukhziihi waya*h*illu 'alayhi 'a*dzaa*bun muqiim**un**

siapa yang mendapat siksa yang menghinakan dan kepadanya ditimpakan azab yang kekal.”

39:41

# اِنَّآ اَنْزَلْنَا عَلَيْكَ الْكِتٰبَ لِلنَّاسِ بِالْحَقِّۚ فَمَنِ اهْتَدٰى فَلِنَفْسِهٖۚ وَمَنْ ضَلَّ فَاِنَّمَا يَضِلُّ عَلَيْهَا ۚوَمَآ اَنْتَ عَلَيْهِمْ بِوَكِيْلٍ ࣖ

inn*aa* anzaln*aa* 'alayka **a**lkit*aa*ba li**l**nn*aa*si bi**a**l*h*aqqi famani ihtad*aa* falinafsihi waman *dh*alla fa-innam*aa* ya*dh*illu 'alayh*aa* wam*aa*

Sungguh, Kami menurunkan kepadamu Kitab (Al-Qur'an) dengan membawa kebenaran untuk manusia; barangsiapa mendapat petunjuk maka (petunjuk itu) untuk dirinya sendiri, dan siapa sesat maka sesungguhnya kesesatan itu untuk dirinya sendiri, dan engkau bukanlah

39:42

# اَللّٰهُ يَتَوَفَّى الْاَنْفُسَ حِيْنَ مَوْتِهَا وَالَّتِيْ لَمْ تَمُتْ فِيْ مَنَامِهَا ۚ فَيُمْسِكُ الَّتِي قَضٰى عَلَيْهَا الْمَوْتَ وَيُرْسِلُ الْاُخْرٰىٓ اِلٰٓى اَجَلٍ مُّسَمًّىۗ اِنَّ فِيْ ذٰلِكَ لَاٰيٰتٍ لِّقَوْمٍ يَّتَفَكَّرُوْنَ

**al**l*aa*hu yatawaff*aa* **a**l-anfusa *h*iina mawtih*aa* wa**a**llatii lam tamut fii man*aa*mih*aa* fayumsiku **al**latii qa*daa* 'alayh*aa* **a**l

Allah memegang nyawa (seseorang) pada saat kematiannya dan nyawa (seseorang) yang belum mati ketika dia tidur; maka Dia tahan nyawa (orang) yang telah Dia tetapkan kematiannya dan Dia lepaskan nyawa yang lain sampai waktu yang ditentukan. Sungguh, pada ya

39:43

# اَمِ اتَّخَذُوْا مِنْ دُوْنِ اللّٰهِ شُفَعَاۤءَۗ قُلْ اَوَلَوْ كَانُوْا لَا يَمْلِكُوْنَ شَيْـًٔا وَّلَا يَعْقِلُوْنَ

ami ittakha*dz*uu min duuni **al**l*aa*hi syufa'*aa*-a qul awa law k*aa*nuu l*aa* yamlikuuna syay-an wal*aa* ya'qiluun**a**

Ataukah mereka mengambil penolong selain Allah. Katakanlah, “Apakah (kamu mengambilnya juga) meskipun mereka tidak memiliki sesuatu apa pun dan tidak mengerti?”

39:44

# قُلْ لِّلّٰهِ الشَّفَاعَةُ جَمِيْعًا ۗ لَهٗ مُلْكُ السَّمٰوٰتِ وَالْاَرْضِۗ ثُمَّ اِلَيْهِ تُرْجَعُوْنَ

qul lill*aa*hi **al**sysyaf*aa*'atu jamii'an lahu mulku **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i tsumma ilayhi turja'uun**a**

Katakanlah, “Pertolongan itu hanya milik Allah semuanya. Dia memiliki kerajaan langit dan bumi. Kemudian kepada-Nya kamu dikembalikan.”

39:45

# وَاِذَا ذُكِرَ اللّٰهُ وَحْدَهُ اشْمَـَٔزَّتْ قُلُوْبُ الَّذِيْنَ لَا يُؤْمِنُوْنَ بِالْاٰخِرَةِۚ وَاِذَا ذُكِرَ الَّذِيْنَ مِنْ دُوْنِهٖٓ اِذَا هُمْ يَسْتَبْشِرُوْنَ

wa-i*dzaa* *dz*ukira **al**l*aa*hu wa*h*dahu isyma-azzat quluubu **al**la*dz*iina l*aa* yu/minuuna bi**a**l-*aa*khirati wa-i*dzaa* *dz*ukira **al**la*dz*

Dan apabila yang disebut hanya nama Allah, kesal sekali hati orang-orang yang tidak beriman kepada akhirat. Namun apabila nama-nama sembahan selain Allah yang disebut, tiba-tiba mereka menjadi bergembira.

39:46

# قُلِ اللهم فَاطِرَ السَّمٰوٰتِ وَالْاَرْضِ عٰلِمَ الْغَيْبِ وَالشَّهَادَةِ اَنْتَ تَحْكُمُ بَيْنَ عِبَادِكَ فِيْ مَا كَانُوْا فِيْهِ يَخْتَلِفُوْنَ

quli **al**l*aa*humma f*aath*ira **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i '*aa*lima **a**lghaybi wa**al**sysyah*aa*dati anta ta*h*kumu bayna 'ib*aa*

*Katakanlah, “Ya Allah, Pencipta langit dan bumi, yang mengetahui segala yang gaib dan yang nyata, Engkaulah yang memutuskan di antara hamba-hamba-Mu tentang apa yang selalu mereka perselisihkan.”*









39:47

# وَلَوْ اَنَّ لِلَّذِيْنَ ظَلَمُوْا مَا فِى الْاَرْضِ جَمِيْعًا وَّمِثْلَهٗ مَعَهٗ لَافْتَدَوْا بِهٖ مِنْ سُوْۤءِ الْعَذَابِ يَوْمَ الْقِيٰمَةِۗ وَبَدَا لَهُمْ مِّنَ اللّٰهِ مَا لَمْ يَكُوْنُوْا يَحْتَسِبُوْنَ

walaw anna lilla*dz*iina *zh*alamuu m*aa* fii **a**l-ar*dh*i jamii'an wamitslahu ma'ahu la**i**ftadaw bihi min suu-i **a**l'a*dzaa*bi yawma **a**lqiy*aa*mati wabad*aa*

Dan sekiranya orang-orang yang zalim mempunyai segala apa yang ada di bumi dan ditambah lagi sebanyak itu, niscaya mereka akan menebus dirinya dengan itu dari azab yang buruk pada hari Kiamat. Dan jelaslah bagi mereka azab dari Allah yang dahulu tidak per

39:48

# وَبَدَا لَهُمْ سَيِّاٰتُ مَا كَسَبُوْا وَحَاقَ بِهِمْ مَّا كَانُوْا بِهٖ يَسْتَهْزِءُوْنَ

wabad*aa* lahum sayyi-*aa*tu m*aa* kasabuu wa*haa*qa bihim m*aa* k*aa*nuu bihi yastahzi-uun**a**

Dan jelaslah bagi mereka kejahatan apa yang telah mereka kerjakan dan mereka diliputi oleh apa yang dahulu mereka selalu memperolok-olokkannya.

39:49

# فَاِذَا مَسَّ الْاِنْسَانَ ضُرٌّ دَعَانَاۖ ثُمَّ اِذَا خَوَّلْنٰهُ نِعْمَةً مِّنَّاۙ قَالَ اِنَّمَآ اُوْتِيْتُهٗ عَلٰى عِلْمٍ ۗبَلْ هِيَ فِتْنَةٌ وَّلٰكِنَّ اَكْثَرَهُمْ لَا يَعْلَمُوْنَ

fa-i*dzaa* massa **a**l-ins*aa*na *dh*urrun da'*aa*n*aa* tsumma i*dzaa* khawwaln*aa*hu ni'matan minn*aa* q*aa*la innam*aa* uutiituhu 'al*aa* 'ilmin bal hiya fitnatun wal*aa*kinna akt

Maka apabila manusia ditimpa bencana dia menyeru Kami, kemudian apabila Kami berikan nikmat Kami kepadanya dia berkata, “Sesungguhnya aku diberi nikmat ini hanyalah karena kepintaranku.” Sebenarnya, itu adalah ujian, tetapi kebanyakan mereka tidak mengeta

39:50

# قَدْ قَالَهَا الَّذِيْنَ مِنْ قَبْلِهِمْ فَمَآ اَغْنٰى عَنْهُمْ مَّا كَانُوْا يَكْسِبُوْنَ

qad q*aa*lah*aa* **al**la*dz*iina min qablihim fam*aa* aghn*aa* 'anhum m*aa* k*aa*nuu yaksibuun**a**

Sungguh, orang-orang yang sebelum mereka pun telah mengatakan hal itu, maka tidak berguna lagi bagi mereka apa yang dahulu mereka kerjakan.

39:51

# فَاَصَابَهُمْ سَيِّاٰتُ مَا كَسَبُوْا ۗوَالَّذِيْنَ ظَلَمُوْا مِنْ هٰٓؤُلَاۤءِ سَيُصِيْبُهُمْ سَيِّاٰتُ مَا كَسَبُوْا ۙوَمَا هُمْ بِمُعْجِزِيْنَ

fa-a*shaa*bahum sayyi-*aa*tu m*aa* kasabuu wa**a**lla*dz*iina *zh*alamuu min h*aa*ul*aa*-i sayu*sh*iibuhum sayyi-*aa*tu m*aa* kasabuu wam*aa* hum bimu'jiziin**a**

Lalu mereka ditimpa (bencana) dari akibat buruk apa yang mereka perbuat. Dan orang-orang yang zalim di antara mereka juga akan ditimpa (bencana) dari akibat buruk apa yang mereka kerjakan dan mereka tidak dapat melepaskan diri.

39:52

# اَوَلَمْ يَعْلَمُوْٓا اَنَّ اللّٰهَ يَبْسُطُ الرِّزْقَ لِمَنْ يَّشَاۤءُ وَيَقْدِرُ ۗاِنَّ فِيْ ذٰلِكَ لَاٰيٰتٍ لِّقَوْمٍ يُّؤْمِنُوْنَ ࣖ

awa lam ya'lamuu anna **al**l*aa*ha yabsu*th*u **al**rrizqa liman yasy*aa*u wayaqdiru inna fii *dzaa*lika la*aa*y*aa*tin liqawmin yu/minuun**a**

Dan tidakkah mereka mengetahui bahwa Allah melapangkan rezeki bagi siapa yang Dia kehendaki dan membatasinya (bagi siapa yang Dia kehendaki)? Sesungguhnya pada yang demikian itu terdapat tanda-tanda (kekuasaan Allah) bagi kaum yang beriman.

39:53

# ۞ قُلْ يٰعِبَادِيَ الَّذِيْنَ اَسْرَفُوْا عَلٰٓى اَنْفُسِهِمْ لَا تَقْنَطُوْا مِنْ رَّحْمَةِ اللّٰهِ ۗاِنَّ اللّٰهَ يَغْفِرُ الذُّنُوْبَ جَمِيْعًا ۗاِنَّهٗ هُوَ الْغَفُوْرُ الرَّحِيْمُ

qul y*aa* 'ib*aa*diya **al**la*dz*iina asrafuu 'al*aa* anfusihim l*aa* taqna*th*uu min ra*h*mati **al**l*aa*hi inna **al**l*aa*ha yaghfiru **al***dzdz*un

Katakanlah, “Wahai hamba-hamba-Ku yang melampaui batas terhadap diri mereka sendiri! Janganlah kamu berputus asa dari rahmat Allah. Sesungguhnya Allah mengampuni dosa-dosa semuanya. Sungguh, Dialah Yang Maha Pengampun, Maha Penyayang.

39:54

# وَاَنِيْبُوْٓا اِلٰى رَبِّكُمْ وَاَسْلِمُوْا لَهٗ مِنْ قَبْلِ اَنْ يَّأْتِيَكُمُ الْعَذَابُ ثُمَّ لَا تُنْصَرُوْنَ

wa-aniibuu il*aa* rabbikum wa-aslimuu lahu min qabli an ya/tiyakumu **a**l'a*dzaa*bu tsumma l*aa* tun*sh*aruun**a**

Dan kembalilah kamu kepada Tuhanmu, dan berserah dirilah kepada-Nya sebelum datang azab kepadamu, kemudian kamu tidak dapat ditolong.

39:55

# وَاتَّبِعُوْٓا اَحْسَنَ مَآ اُنْزِلَ اِلَيْكُمْ مِّنْ رَّبِّكُمْ مِّنْ قَبْلِ اَنْ يَّأْتِيَكُمُ الْعَذَابُ بَغْتَةً وَّاَنْتُمْ لَا تَشْعُرُوْنَ ۙ

wa**i**ttabi'uu a*h*sana m*aa* unzila ilaykum min rabbikum min qabli an ya/tiyakumu **a**l'a*dzaa*bu baghtatan wa-antum l*aa* tasy'uruun**a**

Dan ikutilah sebaik-baik apa yang telah diturunkan kepadamu (Al-Qur'an) dari Tuhanmu sebelum datang azab kepadamu secara mendadak, sedang kamu tidak menyadarinya,

39:56

# اَنْ تَقُوْلَ نَفْسٌ يّٰحَسْرَتٰى عَلٰى مَا فَرَّطْتُّ فِيْ جَنْۢبِ اللّٰهِ وَاِنْ كُنْتُ لَمِنَ السَّاخِرِيْنَۙ

an taquula nafsun y*aa* *h*asrat*aa* 'al*aa* m*aa* farra*th*tu fii janbi **al**l*aa*hi wa-in kuntu lamina **al**ss*aa*khiriin**a**

agar jangan ada orang yang mengatakan, ‘Alangkah besar penyesalanku atas kelalaianku dalam (menunaikan kewajiban) terhadap Allah, dan sesungguhnya aku termasuk orang-orang yang memperolok-olokkan (agama Allah),’

39:57

# اَوْ تَقُوْلَ لَوْ اَنَّ اللّٰهَ هَدٰىنِيْ لَكُنْتُ مِنَ الْمُتَّقِيْنَ ۙ

aw taquula law anna **al**l*aa*ha had*aa*nii lakuntu mina **a**lmuttaqiin**a**

atau (agar jangan) ada yang berkata, ‘Sekiranya Allah memberi petunjuk kepadaku tentulah aku termasuk orang-orang yang bertakwa,’

39:58

# اَوْ تَقُوْلَ حِيْنَ تَرَى الْعَذَابَ لَوْ اَنَّ لِيْ كَرَّةً فَاَكُوْنَ مِنَ الْمُحْسِنِيْنَ

aw taquula *h*iina tar*aa* **a**l'a*dzaa*ba law anna lii karratan fa-akuuna mina **a**lmu*h*siniin**a**

atau (agar jangan) ada yang berkata ketika melihat azab, ‘Sekiranya aku dapat kembali (ke dunia), tentu aku termasuk orang-orang yang berbuat baik.’

39:59

# بَلٰى قَدْ جَاۤءَتْكَ اٰيٰتِيْ فَكَذَّبْتَ بِهَا وَاسْتَكْبَرْتَ وَكُنْتَ مِنَ الْكٰفِرِيْنَ

bal*aa* qad j*aa*-atka *aa*y*aa*tii faka*dzdz*abta bih*aa* wa**i**stakbarta wakunta mina **a**lk*aa*firiin**a**

Sungguh, sebenarnya keterangan-keterangan-Ku telah datang kepadamu, tetapi kamu mendustakannya, malah kamu menyombongkan diri dan termasuk orang kafir.”

39:60

# وَيَوْمَ الْقِيٰمَةِ تَرَى الَّذِيْنَ كَذَبُوْا عَلَى اللّٰهِ وُجُوْهُهُمْ مُّسْوَدَّةٌ ۗ اَلَيْسَ فِيْ جَهَنَّمَ مَثْوًى لِّلْمُتَكَبِّرِيْنَ

wayawma alqiy*aa*mati tar*aa* **al**la*dz*iina ka*dz*abuu 'al*aa* **al**l*aa*hi wujuuhuhum muswaddatun alaysa fii jahannama matswan lilmutakabbiriin**a**

Dan pada hari Kiamat engkau akan melihat orang-orang yang berbuat dusta terhadap Allah, wajahnya menghitam. Bukankah neraka Jahanam itu tempat tinggal bagi orang yang menyombongkan diri?

39:61

# وَيُنَجِّى اللّٰهُ الَّذِيْنَ اتَّقَوْا بِمَفَازَتِهِمْۖ لَا يَمَسُّهُمُ السُّوْۤءُ وَلَا هُمْ يَحْزَنُوْنَ

wayunajjii **al**l*aa*hu **al**la*dz*iina ittaqaw bimaf*aa*zatihim l*aa* yamassuhumu **al**ssuu-u wal*aa* hum ya*h*zanuun**a**

Dan Allah menyelamatkan orang-orang yang bertakwa karena kemenangan mereka. Mereka tidak disentuh oleh azab dan tidak bersedih hati.

39:62

# اَللّٰهُ خَالِقُ كُلِّ شَيْءٍ ۙوَّهُوَ عَلٰى كُلِّ شَيْءٍ وَّكِيْلٌ

**al**l*aa*hu kh*aa*liqu kulli syay-in wahuwa 'al*aa* kulli syay-in wakiil**un**

Allah pencipta segala sesuatu dan Dia Maha Pemelihara atas segala sesuatu.

39:63

# لَهٗ مَقَالِيْدُ السَّمٰوٰتِ وَالْاَرْضِۗ وَالَّذِيْنَ كَفَرُوْا بِاٰيٰتِ اللّٰهِ اُولٰۤىِٕكَ هُمُ الْخٰسِرُوْنَ ࣖ

lahu maq*aa*liidu **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i wa**a**lla*dz*iina kafaruu bi-*aa*y*aa*ti **al**l*aa*hi ul*aa*-ika humu **a**lkh*aa*

*Milik-Nyalah kunci-kunci (perbenda-haraan) langit dan bumi. Dan orang-orang yang kafir terhadap ayat-ayat Allah, mereka itulah orang yang rugi.*









39:64

# قُلْ اَفَغَيْرَ اللّٰهِ تَأْمُرُوْۤنِّيْٓ اَعْبُدُ اَيُّهَا الْجٰهِلُوْنَ

qul afaghayra **al**l*aa*hi ta/muruunnii a'budu ayyuh*aa* **a**lj*aa*hiluun**a**

Katakanlah (Muhammad), “Apakah kamu menyuruh aku menyembah selain Allah, wahai orang-orang yang bodoh?”

39:65

# وَلَقَدْ اُوْحِيَ اِلَيْكَ وَاِلَى الَّذِيْنَ مِنْ قَبْلِكَۚ لَىِٕنْ اَشْرَكْتَ لَيَحْبَطَنَّ عَمَلُكَ وَلَتَكُوْنَنَّ مِنَ الْخٰسِرِيْنَ

walaqad uu*h*iya ilayka wa-il*aa* **al**la*dz*iina min qablika la-in asyrakta laya*h*ba*th*anna 'amaluka walatakuunanna mina **a**lkh*aa*siriin**a**

Dan sungguh, telah diwahyukan kepadamu dan kepada (nabi-nabi) yang sebelummu, “Sungguh, jika engkau mempersekutukan (Allah), niscaya akan hapuslah amalmu dan tentulah engkau termasuk orang yang rugi.

39:66

# بَلِ اللّٰهَ فَاعْبُدْ وَكُنْ مِّنَ الشّٰكِرِيْنَ

bali **al**l*aa*ha fa**u**'bud wakun mina **al**sysy*aa*kiriin**a**

Karena itu, hendaklah Allah saja yang engkau sembah dan hendaklah engkau termasuk orang yang bersyukur.”

39:67

# وَمَا قَدَرُوا اللّٰهَ حَقَّ قَدْرِهٖۖ وَالْاَرْضُ جَمِيْعًا قَبْضَتُهٗ يَوْمَ الْقِيٰمَةِ وَالسَّمٰوٰتُ مَطْوِيّٰتٌۢ بِيَمِيْنِهٖ ۗسُبْحٰنَهٗ وَتَعٰلٰى عَمَّا يُشْرِكُوْنَ

wam*aa* qadaruu **al**l*aa*ha *h*aqqa qadrihi wa**a**l-ar*dh*u jamii'an qab*dh*atuhu yawma **a**lqiy*aa*mati wa**al**ssam*aa*w*aa*tu ma*th*wiyy*aa*tun bi

Dan mereka tidak mengagungkan Allah sebagaimana mestinya padahal bumi seluruhnya dalam genggaman-Nya pada hari Kiamat dan langit digulung dengan tangan kanan-Nya. Mahasuci Dia dan Mahatinggi Dia dari apa yang mereka persekutukan.

39:68

# وَنُفِخَ فِى الصُّوْرِ فَصَعِقَ مَنْ فِى السَّمٰوٰتِ وَمَنْ فِى الْاَرْضِ اِلَّا مَنْ شَاۤءَ اللّٰهُ ۗ ثُمَّ نُفِخَ فِيْهِ اُخْرٰى فَاِذَا هُمْ قِيَامٌ يَّنْظُرُوْنَ

wanufikha fii **al***shsh*uuri fa*sh*a'iqa man fii **al**ssam*aa*w*aa*ti waman fii **a**l-ar*dh*i ill*aa* man sy*aa*-a **al**l*aa*hu tsumma nufikha fiihi ukhr*aa*

Dan sangkakala pun ditiup, maka matilah semua (makhluk) yang di langit dan di bumi kecuali mereka yang dikehendaki Allah. Kemudian ditiup sekali lagi (sangkakala itu) maka seketika itu mereka bangun (dari kuburnya) menunggu (keputusan Allah).







39:69

# وَاَشْرَقَتِ الْاَرْضُ بِنُوْرِ رَبِّهَا وَوُضِعَ الْكِتٰبُ وَجِايْۤءَ بِالنَّبِيّٖنَ وَالشُّهَدَاۤءِ وَقُضِيَ بَيْنَهُمْ بِالْحَقِّ وَهُمْ لَا يُظْلَمُوْنَ

wa-asyraqati **a**l-ar*dh*u binuuri rabbih*aa* wawu*dh*i'a **a**lkit*aa*bu wajii-a bi**al**nnabiyyiina wa**al**sysyuhad*aa*-i waqu*dh*iya baynahum bi**a**l*h<*

Dan bumi (padang Mahsyar) menjadi terang benderang dengan cahaya (keadilan) Tuhannya; dan buku-buku (perhitungan perbuatan mereka) diberikan (kepada masing-masing), nabi-nabi dan saksi-saksi pun dihadirkan, lalu diberikan keputusan di antara mereka secara







39:70

# وَوُفِّيَتْ كُلُّ نَفْسٍ مَّا عَمِلَتْ وَهُوَ اَعْلَمُ بِمَا يَفْعَلُوْنَ ࣖ

wawuffiyat kullu nafsin m*aa* 'amilat wahuwa a'lamu bim*aa* yaf'aluun**a**

Dan kepada setiap jiwa diberi balasan dengan sempurna sesuai dengan apa yang telah dikerjakannya dan Dia lebih mengetahui apa yang mereka kerjakan.

39:71

# وَسِيْقَ الَّذِيْنَ كَفَرُوْٓا اِلٰى جَهَنَّمَ زُمَرًا ۗحَتّٰىٓ اِذَا جَاۤءُوْهَا فُتِحَتْ اَبْوَابُهَا وَقَالَ لَهُمْ خَزَنَتُهَآ اَلَمْ يَأْتِكُمْ رُسُلٌ مِّنْكُمْ يَتْلُوْنَ عَلَيْكُمْ اٰيٰتِ رَبِّكُمْ وَيُنْذِرُوْنَكُمْ لِقَاۤءَ يَوْمِكُمْ هٰذَا ۗقَا

wasiiqa **al**la*dz*iina kafaruu il*aa* jahannama zumaran *h*att*aa* i*dzaa* j*aa*uuh*aa* futi*h*at abw*aa*buh*aa* waq*aa*la lahum khazanatuh*aa* alam ya/tikum rusulun minkum yatluun

Orang-orang yang kafir digiring ke neraka Jahanam secara berombongan. Sehingga apabila mereka sampai kepadanya (neraka) pintu-pintunya dibukakan dan penjaga-penjaga berkata kepada mereka, “Apakah belum pernah datang kepadamu rasul-rasul dari kalangan kamu

39:72

# قِيْلَ ادْخُلُوْٓا اَبْوَابَ جَهَنَّمَ خٰلِدِيْنَ فِيْهَا ۚفَبِئْسَ مَثْوَى الْمُتَكَبِّرِيْنَ

qiila udkhuluu abw*aa*ba jahannama kh*aa*lidiina fiih*aa* fabi/sa matsw*aa* **a**lmutakabbiriin**a**

Dikatakan (kepada mereka), “Masukilah pintu-pintu neraka Jahanam itu, (kamu) kekal di dalamnya.” Maka (neraka Jahanam) itulah seburuk-buruk tempat tinggal bagi orang-orang yang menyombongkan diri.

39:73

# وَسِيْقَ الَّذِيْنَ اتَّقَوْا رَبَّهُمْ اِلَى الْجَنَّةِ زُمَرًا ۗحَتّٰىٓ اِذَا جَاۤءُوْهَا وَفُتِحَتْ اَبْوَابُهَا وَقَالَ لَهُمْ خَزَنَتُهَا سَلٰمٌ عَلَيْكُمْ طِبْتُمْ فَادْخُلُوْهَا خٰلِدِيْنَ

wasiiqa **al**la*dz*iina ittaqaw rabbahum il*aa* **a**ljannati zumaran *h*att*aa* i*dzaa* j*aa*uuh*aa* wafuti*h*at abw*aa*buh*aa* waq*aa*la lahum khazanatuh*aa* sal*aa*

Dan orang-orang yang bertakwa kepada Tuhannya diantar ke dalam surga secara berombongan. Sehingga apabila mereka sampai kepadanya (surga) dan pintu-pintunya telah dibukakan, penjaga-penjaganya berkata kepada mereka, “Kesejahteraan (dilimpahkan) atasmu, be







39:74

# وَقَالُوا الْحَمْدُ لِلّٰهِ الَّذِيْ صَدَقَنَا وَعْدَهٗ وَاَوْرَثَنَا الْاَرْضَ نَتَبَوَّاُ مِنَ الْجَنَّةِ حَيْثُ نَشَاۤءُ ۚفَنِعْمَ اَجْرُ الْعٰمِلِيْنَ

waq*aa*luu **a**l*h*amdu lill*aa*hi **al**la*dz*ii *sh*adaqan*aa* wa'dahu wa-awratsan*aa* **a**l-ar*dh*a natabawwau mina **a**ljannati *h*aytsu nasy*aa*u

Dan mereka berkata, “Segala puji bagi Allah yang telah memenuhi janji-Nya kepada kami dan telah memberikan tempat ini kepada kami sedang kami (diperkenankan) menempati surga di mana saja yang kami kehendaki.” Maka (surga itulah) sebaik-baik balasan bagi o

39:75

# وَتَرَى الْمَلٰۤىِٕكَةَ حَاۤفِّيْنَ مِنْ حَوْلِ الْعَرْشِ يُسَبِّحُوْنَ بِحَمْدِ رَبِّهِمْۚ وَقُضِيَ بَيْنَهُمْ بِالْحَقِّ وَقِيْلَ الْحَمْدُ لِلّٰهِ رَبِّ الْعٰلَمِيْنَ ࣖ

watar*aa* **a**lmal*aa*-ikata *haa*ffiina min *h*awli **a**l'arsyi yusabbi*h*uuna bi*h*amdi rabbihim waqu*dh*iya baynahum bi**a**l*h*aqqi waqiila **a**l*h*amd

Dan engkau (Muhammad) akan melihat malaikat-malaikat melingkar di sekeliling ‘Arsy, bertasbih sambil memuji Tuhannya; lalu diberikan keputusan di antara mereka (hamba-hamba Allah) secara adil dan dikatakan, “Segala puji bagi Allah, Tuhan seluruh alam.”

<!--EndFragment-->