---
title: (91) Asy-Syams - الشمس
date: 2021-10-27T04:27:23.166Z
ayat: 91
description: "Jumlah Ayat: 15 / Arti: Matahari"
---
<!--StartFragment-->

91:1

# وَالشَّمْسِ وَضُحٰىهَاۖ

wa**al**sysyamsi wa*dh*u*haa*h*aa*

Demi matahari dan sinarnya pada pagi hari,

91:2

# وَالْقَمَرِ اِذَا تَلٰىهَاۖ

wa**a**lqamari i*dzaa* tal*aa*h*aa*

demi bulan apabila mengiringinya,

91:3

# وَالنَّهَارِ اِذَا جَلّٰىهَاۖ

wa**al**nnah*aa*ri i*dzaa* jall*aa*h*aa*

demi siang apabila menampakkannya,

91:4

# وَالَّيْلِ اِذَا يَغْشٰىهَاۖ

wa**a**llayli i*dzaa* yaghsy*aa*h*aa*

demi malam apabila menutupinya (gelap gulita),

91:5

# وَالسَّمَاۤءِ وَمَا بَنٰىهَاۖ

wa**al**ssam*aa*-i wam*aa* ban*aa*h*aa*

demi langit serta pembinaannya (yang menakjubkan),

91:6

# وَالْاَرْضِ وَمَا طَحٰىهَاۖ

wa**a**l-ar*dh*i wam*aa* *th*a*haa*h*aa*

demi bumi serta penghamparannya,

91:7

# وَنَفْسٍ وَّمَا سَوّٰىهَاۖ

wanafsin wam*aa* saww*aa*h*aa*

demi jiwa serta penyempurnaan (ciptaan)nya,

91:8

# فَاَلْهَمَهَا فُجُوْرَهَا وَتَقْوٰىهَاۖ

fa-alhamah*aa* fujuurah*aa* wataqw*aa*h*aa*

maka Dia mengilhamkan kepadanya (jalan) kejahatan dan ketakwaannya,

91:9

# قَدْ اَفْلَحَ مَنْ زَكّٰىهَاۖ

qad afla*h*a man zakk*aa*h*aa*

sungguh beruntung orang yang menyucikannya (jiwa itu),

91:10

# وَقَدْ خَابَ مَنْ دَسّٰىهَاۗ

waqad kh*aa*ba man dass*aa*h*aa*

dan sungguh rugi orang yang mengotorinya.

91:11

# كَذَّبَتْ ثَمُوْدُ بِطَغْوٰىهَآ ۖ

ka*dzdz*abat tsamuudu bi*th*aghw*aa*h*aa*

(Kaum) samud telah mendustakan (rasulnya) karena mereka melampaui batas (zalim),

91:12

# اِذِ انْۢبَعَثَ اَشْقٰىهَاۖ

i*dz*i inba'atsa asyq*aa*h*aa*

ketika bangkit orang yang paling celaka di antara mereka,

91:13

# فَقَالَ لَهُمْ رَسُوْلُ اللّٰهِ نَاقَةَ اللّٰهِ وَسُقْيٰهَاۗ

faq*aa*la lahum rasuulu **al**l*aa*hi n*aa*qata **al**l*aa*hi wasuqy*aa*h*aa*

lalu Rasul Allah (Saleh) berkata kepada mereka, “(Biarkanlah) unta betina dari Allah ini dengan minumannya.”

91:14

# فَكَذَّبُوْهُ فَعَقَرُوْهَاۖ فَدَمْدَمَ عَلَيْهِمْ رَبُّهُمْ بِذَنْۢبِهِمْ فَسَوّٰىهَاۖ

faka*dzdz*abuuhu fa'aqaruuh*aa* fadamdama 'alayhim rabbuhum bi*dz*anbihim fasaww*aa*h*aa*

Namun mereka mendustakannya dan menyembelihnya, karena itu Tuhan membinasakan mereka karena dosanya, lalu diratakan-Nya (dengan tanah),

91:15

# وَلَا يَخَافُ عُقْبٰهَا ࣖ

wal*aa* yakh*aa*fu 'uqb*aa*h*aa*

dan Dia tidak takut terhadap akibatnya.

<!--EndFragment-->