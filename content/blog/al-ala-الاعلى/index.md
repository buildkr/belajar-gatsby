---
title: (87) Al-A'la - الاعلى
date: 2021-10-27T04:25:24.600Z
ayat: 87
description: "Jumlah Ayat: 19 / Arti: Maha Tinggi"
---
<!--StartFragment-->

87:1

# سَبِّحِ اسْمَ رَبِّكَ الْاَعْلَىۙ

sabbi*h*i isma rabbika **a**l-a'l*aa*

Sucikanlah nama Tuhanmu Yang Mahatinggi,

87:2

# الَّذِيْ خَلَقَ فَسَوّٰىۖ

**al**la*dz*ii khalaqa fasaww*aa*

Yang menciptakan, lalu menyempurnakan (ciptaan-Nya).

87:3

# وَالَّذِيْ قَدَّرَ فَهَدٰىۖ

wa**a**lla*dz*ii qaddara fahad*aa*

Yang menentukan kadar (masing-masing) dan memberi petunjuk,

87:4

# وَالَّذِيْٓ اَخْرَجَ الْمَرْعٰىۖ

wa**a**lla*dz*ii akhraja **a**lmar'*aa*

dan Yang menumbuhkan rerumputan,

87:5

# فَجَعَلَهٗ غُثَاۤءً اَحْوٰىۖ

faja'alahu ghuts*aa*-an a*h*w*aa*

lalu dijadikan-Nya (rumput-rumput) itu kering kehitam-hitaman.

87:6

# سَنُقْرِئُكَ فَلَا تَنْسٰىٓ ۖ

sanuqri-uka fal*aa* tans*aa*

Kami akan membacakan (Al-Qur'an) kepadamu (Muhammad) sehingga engkau tidak akan lupa,

87:7

# اِلَّا مَا شَاۤءَ اللّٰهُ ۗاِنَّهٗ يَعْلَمُ الْجَهْرَ وَمَا يَخْفٰىۗ

ill*aa* m*aa* sy*aa*-a **al**l*aa*hu innahu ya'lamu **a**ljahra wam*aa* yakhf*aa*

kecuali jika Allah menghendaki. Sungguh, Dia mengetahui yang terang dan yang tersembunyi.

87:8

# وَنُيَسِّرُكَ لِلْيُسْرٰىۖ

wanuyassiruka lilyusr*aa*

Dan Kami akan memudahkan bagimu ke jalan kemudahan (mencapai kebahagiaan dunia dan akhirat),

87:9

# فَذَكِّرْ اِنْ نَّفَعَتِ الذِّكْرٰىۗ

fa*dz*akkir in nafa'ati **al***dzdz*ikr*aa*

oleh sebab itu berikanlah peringatan, karena peringatan itu bermanfaat,

87:10

# سَيَذَّكَّرُ مَنْ يَّخْشٰىۙ

saya*dzdz*akkaru man yakhsy*aa*

orang yang takut (kepada Allah) akan mendapat pelajaran,

87:11

# وَيَتَجَنَّبُهَا الْاَشْقَىۙ

wayatajannabuh*aa* **a**l-asyq*aa*

dan orang-orang yang celaka (kafir) akan menjauhinya,

87:12

# الَّذِيْ يَصْلَى النَّارَ الْكُبْرٰىۚ

**al**la*dz*ii ya*sh*l*aa* **al**nn*aa*ra **a**lkubr*aa*

(yaitu) orang yang akan memasuki api yang besar (neraka),

87:13

# ثُمَّ لَا يَمُوْتُ فِيْهَا وَلَا يَحْيٰىۗ

tsumma l*aa* yamuutu fiih*aa* wal*aa* ya*h*y*aa*

selanjutnya dia di sana tidak mati dan tidak (pula) hidup.

87:14

# قَدْ اَفْلَحَ مَنْ تَزَكّٰىۙ

qad afla*h*a man tazakk*aa*

Sungguh beruntung orang yang menyucikan diri (dengan beriman),

87:15

# وَذَكَرَ اسْمَ رَبِّهٖ فَصَلّٰىۗ

wa*dz*akara isma rabbihi fa*sh*all*aa*

dan mengingat nama Tuhannya, lalu dia salat.

87:16

# بَلْ تُؤْثِرُوْنَ الْحَيٰوةَ الدُّنْيَاۖ

bal tu/tsiruuna **a**l*h*ay*aa*ta **al**dduny*aa*

Sedangkan kamu (orang-orang kafir) memilih kehidupan dunia,

87:17

# وَالْاٰخِرَةُ خَيْرٌ وَّاَبْقٰىۗ

wa**a**l-*aa*khiratu khayrun wa-abq*aa*

padahal kehidupan akhirat itu lebih baik dan lebih kekal.

87:18

# اِنَّ هٰذَا لَفِى الصُّحُفِ الْاُوْلٰىۙ

inna h*aadzaa* lafii **al***shsh*u*h*ufi **a**l-uul*aa*

Sesungguhnya ini terdapat dalam kitab-kitab yang dahulu,

87:19

# صُحُفِ اِبْرٰهِيْمَ وَمُوْسٰى ࣖ

*sh*u*h*ufi ibr*aa*hiima wamuus*aa*

(yaitu) kitab-kitab Ibrahim dan Musa.

<!--EndFragment-->