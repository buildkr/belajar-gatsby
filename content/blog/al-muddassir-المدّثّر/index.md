---
title: (74) Al-Muddassir - المدّثّر
date: 2021-10-27T04:07:16.131Z
ayat: 74
description: "Jumlah Ayat: 56 / Arti: Orang Yang Berkemul"
---
<!--StartFragment-->

74:1

# يٰٓاَيُّهَا الْمُدَّثِّرُۙ

y*aa* ayyuh*aa* **a**lmuddatstsir**u**

Wahai orang yang berkemul (berselimut)!

74:2

# قُمْ فَاَنْذِرْۖ

qum fa-an*dz*ir

bangunlah, lalu berilah peringatan!

74:3

# وَرَبَّكَ فَكَبِّرْۖ

warabbaka fakabbir

dan agungkanlah Tuhanmu,

74:4

# وَثِيَابَكَ فَطَهِّرْۖ

watsiy*aa*baka fa*th*ahhir

dan bersihkanlah pakaianmu,

74:5

# وَالرُّجْزَ فَاهْجُرْۖ

wa**al**rrujza fa**u**hjur

dan tinggalkanlah segala (perbuatan) yang keji,

74:6

# وَلَا تَمْنُنْ تَسْتَكْثِرُۖ

wal*aa* tamnun tastaktsir**u**

dan janganlah engkau (Muhammad) memberi (dengan maksud) memperoleh (balasan) yang lebih banyak.

74:7

# وَلِرَبِّكَ فَاصْبِرْۗ

walirabbika fa**i***sh*bir

Dan karena Tuhanmu, bersabarlah.

74:8

# فَاِذَا نُقِرَ فِى النَّاقُوْرِۙ

fa-i*dzaa* nuqira fii **al**nn*aa*quur**i**

Maka apabila sangkakala ditiup,

74:9

# فَذٰلِكَ يَوْمَىِٕذٍ يَّوْمٌ عَسِيْرٌۙ

fa*dzal*ika yawma-i*dz*in yawmun 'asiir**un**

maka itulah hari yang serba sulit,

74:10

# عَلَى الْكٰفِرِيْنَ غَيْرُ يَسِيْرٍ

'al*aa* **a**lk*aa*firiina ghayru yasiir**in**

bagi orang-orang kafir tidak mudah.

74:11

# ذَرْنِيْ وَمَنْ خَلَقْتُ وَحِيْدًاۙ

*dz*arnii waman khalaqtu wa*h*iid*aa***n**

Biarkanlah Aku (yang bertindak) terhadap orang yang Aku sendiri telah menciptakannya,

74:12

# وَّجَعَلْتُ لَهٗ مَالًا مَّمْدُوْدًاۙ

waja'altu lahu m*aa*lan mamduud*aa***n**

dan Aku beri kekayaan yang melimpah,

74:13

# وَّبَنِيْنَ شُهُوْدًاۙ

wabaniina syuhuud*aa***n**

dan anak-anak yang selalu bersamanya,

74:14

# وَّمَهَّدْتُّ لَهٗ تَمْهِيْدًاۙ

wamahhadtu lahu tamhiid*aa***n**

dan Aku beri kelapangan (hidup) seluas-luasnya.

74:15

# ثُمَّ يَطْمَعُ اَنْ اَزِيْدَۙ

tsumma ya*th*ma'u an aziid**a**

Kemudian dia ingin sekali agar Aku menambahnya.

74:16

# كَلَّاۗ اِنَّهٗ كَانَ لِاٰيٰتِنَا عَنِيْدًاۗ

kall*aa* innahu k*aa*na li-*aa*y*aa*tin*aa* 'aniid*aa***n**

Tidak bisa! Sesungguhnya dia telah menentang ayat-ayat Kami (Al-Qur'an).

74:17

# سَاُرْهِقُهٗ صَعُوْدًاۗ

saurhiquhu *sh*a'uud*aa***n**

Aku akan membebaninya dengan pendakian yang memayahkan.

74:18

# اِنَّهٗ فَكَّرَ وَقَدَّرَۙ

innahu fakkara waqaddar**a**

Sesungguhnya dia telah memikirkan dan menetapkan (apa yang ditetapkannya),

74:19

# فَقُتِلَ كَيْفَ قَدَّرَۙ

faqutila kayfa qaddar**a**

maka celakalah dia! Bagaimana dia menetapkan?

74:20

# ثُمَّ قُتِلَ كَيْفَ قَدَّرَۙ

tsumma qutila kayfa qaddar**a**

Sekali lagi, celakalah dia! Bagaimana dia menetapkan?

74:21

# ثُمَّ نَظَرَۙ

tsumma na*zh*ar**a**

Kemudian dia (merenung) memikirkan,

74:22

# ثُمَّ عَبَسَ وَبَسَرَۙ

tsumma 'abasa wabasar**a**

lalu berwajah masam dan cemberut,

74:23

# ثُمَّ اَدْبَرَ وَاسْتَكْبَرَۙ

tsumma adbara wa**i**stakbar**a**

kemudian berpaling (dari kebenaran) dan menyombongkan diri,

74:24

# فَقَالَ اِنْ هٰذَآ اِلَّا سِحْرٌ يُّؤْثَرُۙ

faq*aa*la in h*aadzaa* ill*aa* si*h*run yu/tsar**u**

lalu dia berkata, “(Al-Qur'an) ini hanyalah sihir yang dipelajari (dari orang-orang dahulu).

74:25

# اِنْ هٰذَآ اِلَّا قَوْلُ الْبَشَرِۗ

in h*aadzaa* ill*aa* qawlu **a**lbasyar**i**

Ini hanyalah perkataan manusia.”

74:26

# سَاُصْلِيْهِ سَقَرَ

sau*sh*liihi saqar**a**

Kelak, Aku akan memasukkannya ke dalam (neraka) Saqar,

74:27

# وَمَآ اَدْرٰىكَ مَا سَقَرُۗ

wam*aa* adr*aa*ka m*aa* saqar**u**

dan tahukah kamu apa (neraka) Saqar itu?

74:28

# لَا تُبْقِيْ وَلَا تَذَرُۚ

l*aa* tubqii wal*aa* ta*dz*ar**u**

Ia (Saqar itu) tidak meninggalkan dan tidak membiarkan,

74:29

# لَوَّاحَةٌ لِّلْبَشَرِۚ

laww*aah*atun lilbasyar**i**

yang menghanguskan kulit manusia.

74:30

# عَلَيْهَا تِسْعَةَ عَشَرَۗ

'alayh*aa* tis'ata 'asyar**a**

Di atasnya ada sembilan belas (malaikat penjaga).

74:31

# وَمَا جَعَلْنَآ اَصْحٰبَ النَّارِ اِلَّا مَلٰۤىِٕكَةً ۖوَّمَا جَعَلْنَا عِدَّتَهُمْ اِلَّا فِتْنَةً لِّلَّذِيْنَ كَفَرُوْاۙ لِيَسْتَيْقِنَ الَّذِيْنَ اُوْتُوا الْكِتٰبَ وَيَزْدَادَ الَّذِيْنَ اٰمَنُوْٓا اِيْمَانًا وَّلَا يَرْتَابَ الَّذِيْنَ اُوْتُوا الْ

wam*aa* ja'aln*aa* a*sh*-*haa*ba **al**nn*aa*ri ill*aa* mal*aa*-ikatan wam*aa* ja'aln*aa* 'iddatahum ill*aa* fitnatan lilla*dz*iina kafaruu liyastayqina **al**la*dz*iina

Dan yang Kami jadikan penjaga neraka itu hanya dari malaikat; dan Kami menentukan bilangan mereka itu hanya sebagai cobaan bagi orang-orang kafir, agar orang-orang yang diberi kitab menjadi yakin, agar orang yang beriman bertambah imannya, agar orang-oran

74:32

# كَلَّا وَالْقَمَرِۙ

kall*aa* wa**a**lqamar**i**

Tidak! Demi bulan,

74:33

# وَالَّيْلِ اِذْ اَدْبَرَۙ

wa**a**llayli i*dz* adbar**a**

dan demi malam ketika telah berlalu,

74:34

# وَالصُّبْحِ اِذَآ اَسْفَرَۙ

wa**al***shsh*ub*h*i i*dzaa* asfar**a**

dan demi subuh apabila mulai terang,

74:35

# اِنَّهَا لَاِحْدَى الْكُبَرِۙ

innah*aa* la-i*h*d*aa* **a**lkubar**i**

sesunggunya (Saqar itu) adalah salah satu (bencana) yang sangat besar,

74:36

# نَذِيْرًا لِّلْبَشَرِۙ

na*dz*iiran lilbasyar**i**

sebagai peringatan bagi manusia,

74:37

# لِمَنْ شَاۤءَ مِنْكُمْ اَنْ يَّتَقَدَّمَ اَوْ يَتَاَخَّرَۗ

liman sy*aa*-a minkum an yataqaddama aw yata-akhkhar**a**

(yaitu) bagi siapa di antara kamu yang ingin maju atau mundur.

74:38

# كُلُّ نَفْسٍۢ بِمَا كَسَبَتْ رَهِيْنَةٌۙ

kullu nafsin bim*aa* kasabat rahiina**tun**

Setiap orang bertanggung jawab atas apa yang telah dilakukannya,

74:39

# اِلَّآ اَصْحٰبَ الْيَمِيْنِ ۛ

ill*aa* a*sh*-*haa*ba **a**lyamiin**i**

kecuali golongan kanan,

74:40

# فِيْ جَنّٰتٍ ۛ يَتَسَاۤءَلُوْنَۙ

fii jann*aa*tin yatas*aa*-aluun**a**

berada di dalam surga, mereka saling menanyakan,

74:41

# عَنِ الْمُجْرِمِيْنَۙ

'ani **a**lmujrimiin**a**

tentang (keadaan) orang-orang yang berdosa,

74:42

# مَا سَلَكَكُمْ فِيْ سَقَرَ

m*aa* salakakum fii saqar**a**

”Apa yang menyebabkan kamu masuk ke dalam (neraka) Saqar?”

74:43

# قَالُوْا لَمْ نَكُ مِنَ الْمُصَلِّيْنَۙ

q*aa*luu lam naku mina **a**lmu*sh*alliin**a**

Mereka menjawab, “Dahulu kami tidak termasuk orang-orang yang me-laksanakan salat,

74:44

# وَلَمْ نَكُ نُطْعِمُ الْمِسْكِيْنَۙ

walam naku nu*th*'imu **a**lmiskiin**a**

dan kami (juga) tidak memberi makan orang miskin,

74:45

# وَكُنَّا نَخُوْضُ مَعَ الْخَاۤىِٕضِيْنَۙ

wakunn*aa* nakhuu*dh*u ma'a **a**lkh*aa*-i*dh*iin**a**

bahkan kami biasa berbincang (untuk tujuan yang batil), bersama orang-orang yang membicarakannya,

74:46

# وَكُنَّا نُكَذِّبُ بِيَوْمِ الدِّيْنِۙ

wakunn*aa* nuka*dzdz*ibu biyawmi **al**ddiin**i**

dan kami mendustakan hari pembalasan,

74:47

# حَتّٰىٓ اَتٰىنَا الْيَقِيْنُۗ

*h*att*aa* at*aa*n*aa* **a**lyaqiin**u**

sampai datang kepada kami kematian.”

74:48

# فَمَا تَنْفَعُهُمْ شَفَاعَةُ الشَّافِعِيْنَۗ

fam*aa* tanfa'uhum syaf*aa*'atu **al**sysy*aa*fi'iin**a**

Maka tidak berguna lagi bagi mereka syafaat (pertolongan) dari orang-orang yang memberikan syafaat.

74:49

# فَمَا لَهُمْ عَنِ التَّذْكِرَةِ مُعْرِضِيْنَۙ

fam*aa* lahum 'ani **al**tta*dz*kirati mu'ri*dh*iin**a**

Lalu mengapa mereka (orang-orang kafir) berpaling dari peringatan (Allah)?

74:50

# كَاَنَّهُمْ حُمُرٌ مُّسْتَنْفِرَةٌۙ

ka-annahum *h*umurun mustanfira**tun**

seakan-akan mereka keledai liar yang lari terkejut,

74:51

# فَرَّتْ مِنْ قَسْوَرَةٍۗ

farrat min qaswara**tin**

lari dari singa.

74:52

# بَلْ يُرِيْدُ كُلُّ امْرِئٍ مِّنْهُمْ اَنْ يُّؤْتٰى صُحُفًا مُّنَشَّرَةًۙ

bal yuriidu kullu imri-in minhum an yu/t*aa* *sh*u*h*ufan munasysyara**tan**

Bahkan setiap orang dari mereka ingin agar diberikan kepadanya lembaran-lembaran (kitab) yang terbuka.

74:53

# كَلَّاۗ بَلْ لَّا يَخَافُوْنَ الْاٰخِرَةَۗ

kall*aa* bal l*aa* yakh*aa*fuuna **a**l-*aa*khira**ta**

Tidak! Sebenarnya mereka tidak takut kepada akhirat.

74:54

# كَلَّآ اِنَّهٗ تَذْكِرَةٌ ۚ

kall*aa* innahu ta*dz*kira**tun**

Tidak! Sesungguhnya (Al-Qur'an) itu benar-benar suatu peringatan.

74:55

# فَمَنْ شَاۤءَ ذَكَرَهٗۗ

faman sy*aa*-a *dz*akarah**u**

Maka barangsiapa menghendaki, tentu dia mengambil pelajaran darinya.

74:56

# وَمَا يَذْكُرُوْنَ اِلَّآ اَنْ يَّشَاۤءَ اللّٰهُ ۗهُوَ اَهْلُ التَّقْوٰى وَاَهْلُ الْمَغْفِرَةِ ࣖ

wam*aa* ya*dz*kuruuna ill*aa* an yasy*aa*-a **al**l*aa*hu huwa ahlu **al**ttaqw*aa* wa-ahlu **a**lmaghfirat**i**

Dan mereka tidak akan mengambil pelajaran darinya (Al-Qur'an) kecuali (jika) Allah menghendakinya. Dialah Tuhan yang patut (kita) bertakwa kepada-Nya dan yang berhak memberi ampunan.

<!--EndFragment-->