---
title: (85) Al-Buruj - البروج
date: 2021-10-27T04:24:41.413Z
ayat: 85
description: "Jumlah Ayat: 22 / Arti: Gugusan Bintang"
---
<!--StartFragment-->

85:1

# وَالسَّمَاۤءِ ذَاتِ الْبُرُوْجِۙ

wa**al**ssam*aa*-i *dzaa*ti **a**lburuuj**i**

Demi langit yang mempunyai gugusan bintang,

85:2

# وَالْيَوْمِ الْمَوْعُوْدِۙ

wa**a**lyawmi **a**lmaw'uud**i**

dan demi hari yang dijanjikan.

85:3

# وَشَاهِدٍ وَّمَشْهُوْدٍۗ

wa**a**lyawmi **a**lmaw'uud**i**

Demi yang menyaksikan dan yang disaksikan.

85:4

# قُتِلَ اَصْحٰبُ الْاُخْدُوْدِۙ

qutila a*sh*-*haa*bu **a**lukhduud**i**

Binasalah orang-orang yang membuat parit (yaitu para pembesar Najran di Yaman),

85:5

# النَّارِ ذَاتِ الْوَقُوْدِۙ

a**l**nn*aa*ri *dzaa*ti **a**lwaquud**i**

yang berapi (yang mempunyai) kayu bakar,

85:6

# اِذْ هُمْ عَلَيْهَا قُعُوْدٌۙ

i*dz* hum 'alayh*aa* qu'uud**un**

ketika mereka duduk di sekitarnya,

85:7

# وَّهُمْ عَلٰى مَا يَفْعَلُوْنَ بِالْمُؤْمِنِيْنَ شُهُوْدٌ ۗ

wahum 'al*aa* m*aa* yaf'aluuna bi**a**lmu/miniina syuhuud**un**

sedang mereka menyaksikan apa yang mereka perbuat terhadap orang-orang mukmin.

85:8

# وَمَا نَقَمُوْا مِنْهُمْ اِلَّآ اَنْ يُّؤْمِنُوْا بِاللّٰهِ الْعَزِيْزِ الْحَمِيْدِۙ

wam*aa* naqamuu minhum ill*aa* an yu/minuu bi**al**l*aa*hi **a**l'aziizi **a**l*h*amiid**i**

Dan mereka menyiksa orang-orang mukmin itu hanya karena (orang-orang mukmin itu) beriman kepada Allah Yang Mahaperkasa, Maha Terpuji,

85:9

# الَّذِيْ لَهٗ مُلْكُ السَّمٰوٰتِ وَالْاَرْضِ ۗوَاللّٰهُ عَلٰى كُلِّ شَيْءٍ شَهِيْدٌ ۗ

**al**la*dz*ii lahu mulku **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i wa**al**l*aa*hu 'al*aa* kulli syay-in syahiid**un**

yang memiliki kerajaan langit dan bumi. Dan Allah Maha Menyaksikan segala sesuatu.

85:10

# اِنَّ الَّذِيْنَ فَتَنُوا الْمُؤْمِنِيْنَ وَالْمُؤْمِنٰتِ ثُمَّ لَمْ يَتُوْبُوْا فَلَهُمْ عَذَابُ جَهَنَّمَ وَلَهُمْ عَذَابُ الْحَرِيْقِۗ

inna **al**la*dz*iina *aa*manuu wa'amiluu **al***shshaa*li*haa*ti lahum jann*aa*tun tajrii min ta*h*tih*aa* **a**l-anh*aa*ru *dzaa*lika **a**lfawzu **a<**

Sungguh, orang-orang yang mendatangkan cobaan (bencana, membunuh, menyiksa) kepada orang-orang mukmin laki-laki dan perempuan lalu mereka tidak bertobat, maka mereka akan mendapat azab Jahanam dan mereka akan mendapat azab (neraka) yang membakar.







85:11

# اِنَّ الَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِ لَهُمْ جَنّٰتٌ تَجْرِيْ مِنْ تَحْتِهَا الْاَنْهٰرُ ەۗ ذٰلِكَ الْفَوْزُ الْكَبِيْرُۗ

inna **al**la*dz*iina *aa*manuu wa'amiluu **al***shshaa*li*haa*ti lahum jann*aa*tun tajrii min ta*h*tih*aa* **a**l-anh*aa*ru *dzaa*lika **a**lfawzu **a<**

Sungguh, orang-orang yang beriman dan mengerjakan kebajikan, mereka akan mendapat surga yang mengalir di bawahnya sungai-sungai, itulah kemenangan yang agung.







85:12

# اِنَّ بَطْشَ رَبِّكَ لَشَدِيْدٌ ۗ

inna ba*th*sya rabbika lasyadiid**un**

Sungguh, azab Tuhanmu sangat keras.

85:13

# اِنَّهٗ هُوَ يُبْدِئُ وَيُعِيْدُۚ

innahu huwa yubdi-u wayu'iid**u**

Sungguh, Dialah yang memulai pen-ciptaan (makhluk) dan yang menghidupkannya (kembali).

85:14

# وَهُوَ الْغَفُوْرُ الْوَدُوْدُۙ

wahuwa **a**lghafuuru **a**lwaduud**u**

Dan Dialah Yang Maha Pengampun, Maha Pengasih,

85:15

# ذُو الْعَرْشِ الْمَجِيْدُۙ

*dz*uu **a**l'arsyi **a**lmajiid**i**

yang memiliki ‘Arsy, lagi Mahamulia,

85:16

# فَعَّالٌ لِّمَا يُرِيْدُۗ

fa''*aa*lun lim*aa* yuriid**u**

Mahakuasa berbuat apa yang Dia kehendaki.

85:17

# هَلْ اَتٰىكَ حَدِيْثُ الْجُنُوْدِۙ

hal at*aa*ka *h*adiitsu **a**ljunuud**i**

Sudahkah sampai kepadamu berita tentang bala tentara (penentang),

85:18

# فِرْعَوْنَ وَثَمُوْدَۗ

fir'awna watsamuud**a**

(yaitu) Fir‘aun dan Samud?

85:19

# بَلِ الَّذِيْنَ كَفَرُوْا فِيْ تَكْذِيْبٍۙ

bali **al**la*dz*iina kafaruu fii tak*dz*iib**in**

Memang orang-orang kafir (selalu) mendustakan,

85:20

# وَّاللّٰهُ مِنْ وَّرَاۤىِٕهِمْ مُّحِيْطٌۚ

wa**al**l*aa*hu min war*aa*-ihim mu*h*ii*th***un**

padahal Allah mengepung dari belakang mereka (sehingga tidak dapat lolos).

85:21

# بَلْ هُوَ قُرْاٰنٌ مَّجِيْدٌۙ

bal huwa qur-*aa*nun majiid**un**

Bahkan (yang didustakan itu) ialah Al-Qur'an yang mulia,

85:22

# فِيْ لَوْحٍ مَّحْفُوْظٍ ࣖ

fii law*h*in ma*h*fuu*zh***in**

yang (tersimpan) dalam (tempat) yang terjaga (Lauh Mahfuzh).

<!--EndFragment-->