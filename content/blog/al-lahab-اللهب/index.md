---
title: (111) Al-Lahab - اللهب
date: 2021-10-27T04:19:21.459Z
ayat: 111
description: "Jumlah Ayat: 5 / Arti: Api Yang Bergejolak"
---
<!--StartFragment-->

111:1

# تَبَّتْ يَدَآ اَبِيْ لَهَبٍ وَّتَبَّۗ

tabbat yad*aa* abii lahabin watab**ba**

Binasalah kedua tangan Abu Lahab dan benar-benar binasa dia!

111:2

# مَآ اَغْنٰى عَنْهُ مَالُهٗ وَمَا كَسَبَۗ

m*aa* aghn*aa* 'anhu m*aa*luhu wam*aa* kasab**a**

Tidaklah berguna baginya hartanya dan apa yang dia usahakan.

111:3

# سَيَصْلٰى نَارًا ذَاتَ لَهَبٍۙ

saya*sh*l*aa* n*aa*ran *dzaa*ta lahab**in**

Kelak dia akan masuk ke dalam api yang bergejolak (neraka).

111:4

# وَّامْرَاَتُهٗ ۗحَمَّالَةَ الْحَطَبِۚ

wa**i**mra-atuhu *h*amm*aa*lata **a**l*h*a*th*ab**i**

Dan (begitu pula) istrinya, pembawa kayu bakar (penyebar fitnah).

111:5

# فِيْ جِيْدِهَا حَبْلٌ مِّنْ مَّسَدٍ ࣖ

fii jiidih*aa* *h*ablun min masad**in**

Di lehernya ada tali dari sabut yang dipintal.

<!--EndFragment-->