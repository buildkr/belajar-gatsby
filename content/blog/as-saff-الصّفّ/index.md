---
title: (61) As-Saff - الصّفّ
date: 2021-10-27T04:02:14.875Z
ayat: 61
description: "Jumlah Ayat: 14 / Arti: Barisan"
---
<!--StartFragment-->

61:1

# سَبَّحَ لِلّٰهِ مَا فِى السَّمٰوٰتِ وَمَا فِى الْاَرْضِۚ وَهُوَ الْعَزِيْزُ الْحَكِيْمُ

sabba*h*a lill*aa*hi m*aa* fii **al**ssam*aa*w*aa*ti wam*aa* fii **a**l-ar*dh*i wahuwa **a**l'aziizu **a**l*h*akiim**u**

Apa yang ada di langit dan apa yang ada di bumi bertasbih kepada Allah; dan Dialah Yang Mahaperkasa, Mahabijaksana.

61:2

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْا لِمَ تَقُوْلُوْنَ مَا لَا تَفْعَلُوْنَ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu lima taquuluuna m*aa* l*aa* taf'aluun**a**

Wahai orang-orang yang beriman! Mengapa kamu mengatakan sesuatu yang tidak kamu kerjakan?

61:3

# كَبُرَ مَقْتًا عِنْدَ اللّٰهِ اَنْ تَقُوْلُوْا مَا لَا تَفْعَلُوْنَ

kabura maqtan 'inda **al**l*aa*hi an taquuluu m*aa* l*aa* taf'aluun**a**

(Itu) sangatlah dibenci di sisi Allah jika kamu mengatakan apa-apa yang tidak kamu kerjakan.

61:4

# اِنَّ اللّٰهَ يُحِبُّ الَّذِيْنَ يُقَاتِلُوْنَ فِيْ سَبِيْلِهٖ صَفًّا كَاَنَّهُمْ بُنْيَانٌ مَّرْصُوْصٌ

inna **al**l*aa*ha yu*h*ibbu **al**la*dz*iina yuq*aa*tiluuna fii sabiilihi *sh*affan ka-annahum buny*aa*nun mar*sh*uu*sh**\*un**

Sesungguhnya Allah mencintai orang-orang yang berperang di jalan-Nya dalam barisan yang teratur, mereka seakan-akan seperti suatu bangunan yang tersusun kokoh.

61:5

# وَاِذْ قَالَ مُوْسٰى لِقَوْمِهٖ يٰقَوْمِ لِمَ تُؤْذُوْنَنِيْ وَقَدْ تَّعْلَمُوْنَ اَنِّيْ رَسُوْلُ اللّٰهِ اِلَيْكُمْۗ فَلَمَّا زَاغُوْٓا اَزَاغَ اللّٰهُ قُلُوْبَهُمْۗ وَاللّٰهُ لَا يَهْدِى الْقَوْمَ الْفٰسِقِيْنَ

wa-i*dz* q*aa*la muus*aa* liqawmihi y*aa* qawmi lima tu/*dz*uunanii waqad ta'lamuuna annii rasuulu **al**l*aa*hi ilaykum falamm*aa* z*aa*ghuu az*aa*gha **al**l*aa*hu quluubahum wa

Dan (ingatlah) ketika Musa berkata kepada kaumnya, “Wahai kaumku! Mengapa kamu menyakitiku, padahal kamu sungguh mengetahui bahwa sesungguhnya aku utusan Allah kepadamu?” Maka ketika mereka berpaling (dari kebenaran), Allah memalingkan hati mereka. Dan Al

61:6

# وَاِذْ قَالَ عِيْسَى ابْنُ مَرْيَمَ يٰبَنِيْٓ اِسْرَاۤءِيْلَ اِنِّيْ رَسُوْلُ اللّٰهِ اِلَيْكُمْ مُّصَدِّقًا لِّمَا بَيْنَ يَدَيَّ مِنَ التَّوْرٰىةِ وَمُبَشِّرًاۢ بِرَسُوْلٍ يَّأْتِيْ مِنْۢ بَعْدِى اسْمُهٗٓ اَحْمَدُۗ فَلَمَّا جَاۤءَهُمْ بِالْبَيِّنٰتِ قَا

wa-i*dz* q*aa*la 'iis*aa* ibnu maryama y*aa* banii isr*aa*-iila innii rasuulu **al**l*aa*hi ilaykum mu*sh*addiqan lim*aa* bayna yadayya mina **al**ttawr*aa*ti wamubasysyiran birasuulin

Dan (ingatlah) ketika Isa putra Maryam berkata, “Wahai Bani Israil! Sesungguhnya aku utusan Allah kepadamu, yang membenarkan kitab (yang turun) sebelumku, yaitu Taurat dan memberi kabar gembira dengan seorang Rasul yang akan datang setelahku, yang namanya

61:7

# وَمَنْ اَظْلَمُ مِمَّنِ افْتَرٰى عَلَى اللّٰهِ الْكَذِبَ وَهُوَ يُدْعٰىٓ اِلَى الْاِسْلَامِۗ وَاللّٰهُ لَا يَهْدِى الْقَوْمَ الظّٰلِمِيْنَ

waman a*zh*lamu mimmani iftar*aa* 'al*aa* **al**l*aa*hi **a**lka*dz*iba wahuwa yud'*aa* il*aa* **a**l-isl*aa*mi wa**al**l*aa*hu l*aa* yahdii **a**

**Dan siapakah yang lebih zalim daripada orang yang mengada-adakan kebohongan terhadap Allah padahal dia diajak kepada (agama) Islam? Dan Allah tidak memberi petunjuk kepada orang-orang yang zalim.**

61:8

# يُرِيْدُوْنَ لِيُطْفِـُٔوْا نُوْرَ اللّٰهِ بِاَفْوَاهِهِمْۗ وَاللّٰهُ مُتِمُّ نُوْرِهٖ وَلَوْ كَرِهَ الْكٰفِرُوْنَ

yuriiduuna liyu*th*fi-uu nuura **al**l*aa*hi bi-afw*aa*hihim wa**al**l*aa*hu mutimmu nuurihi walaw kariha **a**lk*aa*firuun**a**

Mereka hendak memadamkan cahaya (agama) Allah dengan mulut (ucapan-ucapan) mereka, tetapi Allah tetap menyempurnakan cahaya-Nya meskipun orang-orang kafir membencinya.

61:9

# هُوَ الَّذِيْٓ اَرْسَلَ رَسُوْلَهٗ بِالْهُدٰى وَدِيْنِ الْحَقِّ لِيُظْهِرَهٗ عَلَى الدِّيْنِ كُلِّهٖۙ وَلَوْ كَرِهَ الْمُشْرِكُوْنَ ࣖ

huwa **al**la*dz*ii arsala rasuulahu bi**a**lhud*aa* wadiini **a**l*h*aqqi liyu*zh*hirahu 'al*aa* **al**ddiini kullihi walaw kariha **a**lmusyrikuun**a**

**Dialah yang mengutus Rasul-Nya dengan membawa petunjuk dan agama yang benar, untuk memenangkannya di atas segala agama meskipun orang-orang musyrik membencinya.**

61:10

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْا هَلْ اَدُلُّكُمْ عَلٰى تِجَارَةٍ تُنْجِيْكُمْ مِّنْ عَذَابٍ اَلِيْمٍ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu hal adullukum 'al*aa* tij*aa*ratin tunjiikum min 'a*dzaa*bin **a**liim**in**

Wahai orang-orang yang beriman! Maukah kamu Aku tunjukkan suatu perdagangan yang dapat menyelamatkan kamu dari azab yang pedih?

61:11

# تُؤْمِنُوْنَ بِاللّٰهِ وَرَسُوْلِهٖ وَتُجَاهِدُوْنَ فِيْ سَبِيْلِ اللّٰهِ بِاَمْوَالِكُمْ وَاَنْفُسِكُمْۗ ذٰلِكُمْ خَيْرٌ لَّكُمْ اِنْ كُنْتُمْ تَعْلَمُوْنَۙ

tu/minuuna bi**al**l*aa*hi warasuulihi watuj*aa*hiduuna fii sabiili **al**l*aa*hi bi-amw*aa*likum wa-anfusikum *dzaa*likum khayrun lakum in kuntum ta'lamuun**a**

Yaitu kamu beriman kepada Allah dan Rasul-Nya dan berjihad di jalan Allah dengan harta dan jiwamu. Itulah yang lebih baik bagi kamu jika kamu mengetahui,

61:12

# يَغْفِرْ لَكُمْ ذُنُوْبَكُمْ وَيُدْخِلْكُمْ جَنّٰتٍ تَجْرِيْ مِنْ تَحْتِهَا الْاَنْهٰرُ وَمَسٰكِنَ طَيِّبَةً فِيْ جَنّٰتِ عَدْنٍۗ ذٰلِكَ الْفَوْزُ الْعَظِيْمُۙ

yaghfir lakum *dz*unuubakum wayudkhilkum jann*aa*tin tajrii min ta*h*tih*aa* **a**l-anh*aa*ru wamas*aa*kina *th*ayyibatan fii jann*aa*ti 'adnin *dzaa*lika **a**lfawzu **a**

**niscaya Allah mengampuni dosa-dosamu dan memasukkan kamu ke dalam surga yang mengalir di bawahnya sungai-sungai, dan ke tempat-tempat tinggal yang baik di dalam surga ‘Adn. Itulah kemenangan yang agung.**

61:13

# وَاُخْرٰى تُحِبُّوْنَهَاۗ نَصْرٌ مِّنَ اللّٰهِ وَفَتْحٌ قَرِيْبٌۗ وَبَشِّرِ الْمُؤْمِنِيْنَ

waukhr*aa* tu*h*ibbuunah*aa* na*sh*run mina **al**l*aa*hi wafat*h*un qarriibun wabasysyiri **a**lmu/miniin**a**

Dan (ada lagi) karunia yang lain yang kamu sukai (yaitu) pertolongan dari Allah dan kemenangan yang dekat (waktunya). Dan sampaikanlah berita gembira kepada orang-orang mukmin.

61:14

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْا كُوْنُوْٓا اَنْصَارَ اللّٰهِ كَمَا قَالَ عِيْسَى ابْنُ مَرْيَمَ لِلْحَوَارِيّٖنَ مَنْ اَنْصَارِيْٓ اِلَى اللّٰهِ ۗقَالَ الْحَوَارِيُّوْنَ نَحْنُ اَنْصَارُ اللّٰهِ فَاٰمَنَتْ طَّاۤىِٕفَةٌ مِّنْۢ بَنِيْٓ اِسْرَاۤءِيْلَ وَكَف

yā ayyuhallażīna āmanụ kụnū anṣārallāhi kamā qāla ‘īsabnu maryama lil-ḥawāriyyīna man anṣārī ilallāh, qālal-ḥawāriyyụna naḥnu anṣārullāhi fa āmanat ṭā\`ifatum mim banī isrā\`īla wa kafarat ṭā`ifah, fa ayyadnallażīna āmanụ ‘alā ‘aduwwihim fa aṣbaḥụ ẓāhirīn 

Hai orang-orang yang beriman, jadilah kamu penolong (agama) Allah sebagaimana Isa ibnu Maryam telah berkata kepada pengikut-pengikutnya yang setia: “Siapakah yang akan menjadi penolong-penolongku (untuk menegakkan agama) Allah?” Pengikut-pengikut yang setia itu berkata: “Kamilah penolong-penolong agama Allah”, lalu segolongan dari Bani Israil beriman dan segolongan lain kafir; maka Kami berikan kekuatan kepada orang-orang yang beriman terhadap musuh-musuh mereka, lalu mereka menjadi orang-orang yang menang.

<!--EndFragment-->