---
title: (46) Al-Ahqaf - الاحقاف
date: 2021-10-27T04:07:22.230Z
ayat: 46
description: "Jumlah Ayat: 35 / Arti: Bukit Pasir"
---
<!--StartFragment-->

46:1

# حٰمۤ ۚ

*haa*-miim

Ha Mim

46:2

# تَنْزِيْلُ الْكِتٰبِ مِنَ اللّٰهِ الْعَزِيْزِ الْحَكِيْمِ

tanziilu **a**lkit*aa*bi mina **al**l*aa*hi **a**l'aziizi **a**l*h*akiim**i**

Kitab ini diturunkan dari Allah Yang Mahaperkasa, Mahabijaksana.

46:3

# مَا خَلَقْنَا السَّمٰوٰتِ وَالْاَرْضَ وَمَا بَيْنَهُمَآ اِلَّا بِالْحَقِّ وَاَجَلٍ مُّسَمًّىۗ وَالَّذِيْنَ كَفَرُوْا عَمَّآ اُنْذِرُوْا مُعْرِضُوْنَ

m*aa* khalaqn*aa* **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*a wam*aa* baynahum*aa* ill*aa* bi**a**l*h*aqqi wa-ajalin musamman wa**a**lla*dz*iina kafaruu 'amm<

Kami tidak menciptakan langit dan bumi dan apa yang ada di antara keduanya melainkan dengan (tujuan) yang benar dan dalam waktu yang ditentukan. Namun orang-orang yang kafir, berpaling dari peringatan yang diberikan kepada mereka.

46:4

# قُلْ اَرَءَيْتُمْ مَّا تَدْعُوْنَ مِنْ دُوْنِ اللّٰهِ اَرُوْنِيْ مَاذَا خَلَقُوْا مِنَ الْاَرْضِ اَمْ لَهُمْ شِرْكٌ فِى السَّمٰوٰتِ ۖائْتُوْنِيْ بِكِتٰبٍ مِّنْ قَبْلِ هٰذَآ اَوْ اَثٰرَةٍ مِّنْ عِلْمٍ اِنْ كُنْتُمْ صٰدِقِيْنَ

qul ara-aytum m*aa* tad'uuna min duuni **al**l*aa*hi aruunii m*aatsaa* khalaquu mina **a**l-ar*dh*i am lahum syirkun fii **al**ssam*aa*w*aa*ti iituunii bikit*aa*bin min qabli h*aa*

Katakanlah (Muhammad), “Terangkanlah (kepadaku) tentang apa yang kamu sembah selain Allah; perlihatkan kepadaku apa yang telah mereka ciptakan dari bumi atau adakah peran serta mereka dalam (penciptaan) langit? Bawalah kepadaku kitab yang sebelum (Al-Qur'

46:5

# وَمَنْ اَضَلُّ مِمَّنْ يَّدْعُوْا مِنْ دُوْنِ اللّٰهِ مَنْ لَّا يَسْتَجِيْبُ لَهٗٓ اِلٰى يَوْمِ الْقِيٰمَةِ وَهُمْ عَنْ دُعَاۤىِٕهِمْ غٰفِلُوْنَ

waman a*dh*allu mimman yad'uu min duuni **al**l*aa*hi man l*aa* yastajiibu lahu il*aa* yawmi **a**lqiy*aa*mati wahum 'an du'*aa*-ihim gh*aa*filuun**a**

Dan siapakah yang lebih sesat daripada orang-orang yang menyembah selain Allah (sembahan) yang tidak dapat memperkenankan (doa)nya sampai hari Kiamat dan mereka lalai dari (memperhatikan) doa mereka?

46:6

# وَاِذَا حُشِرَ النَّاسُ كَانُوْا لَهُمْ اَعْدَاۤءً وَّكَانُوْا بِعِبَادَتِهِمْ كٰفِرِيْنَ

wa-i*dzaa* *h*usyira **al**nn*aa*su k*aa*nuu lahum a'd*aa*-an wak*aa*nuu bi'ib*aa*datihim k*aa*firiin**a**

Dan apabila manusia dikumpulkan (pada hari Kiamat), sesembahan itu menjadi musuh mereka dan mengingkari pemujaan-pemujaan yang mereka lakukan kepadanya.

46:7

# وَاِذَا تُتْلٰى عَلَيْهِمْ اٰيٰتُنَا بَيِّنٰتٍ قَالَ الَّذِيْنَ كَفَرُوْا لِلْحَقِّ لَمَّا جَاۤءَهُمْۙ هٰذَا سِحْرٌ مُّبِيْنٌۗ

wa-i*dzaa* tutl*aa* 'alayhim *aa*y*aa*tun*aa* bayyin*aa*tin q*aa*la **al**la*dz*iina kafaruu lil*h*aqqi lamm*aa* j*aa*-ahum h*aadzaa* si*h*run mubiin**un**

Dan apabila mereka dibacakan ayat-ayat Kami yang jelas, orang-orang yang kafir berkata ketika kebenaran itu datang kepada mereka, “Ini adalah sihir yang nyata.”

46:8

# اَمْ يَقُوْلُوْنَ افْتَرٰىهُ ۗ قُلْ اِنِ افْتَرَيْتُهٗ فَلَا تَمْلِكُوْنَ لِيْ مِنَ اللّٰهِ شَيْـًٔا ۗهُوَ اَعْلَمُ بِمَا تُفِيْضُوْنَ فِيْهِۗ كَفٰى بِهٖ شَهِيْدًا ۢ بَيْنِيْ وَبَيْنَكُمْ ۗ وَهُوَ الْغَفُوْرُ الرَّحِيْمُ

am yaquuluuna iftar*aa*hu qul ini iftaraytuhu fal*aa* tamlikuuna lii mina **al**l*aa*hi syay-an huwa a'lamu bim*aa* tufii*dh*uuna fiihi kaf*aa* bihi syahiidan baynii wabaynakum wahuwa **a**lghafuuru

Bahkan mereka berkata, “Dia (Muhammad) telah mengada-adakannya (Al-Qur'an).” Katakanlah, “Jika aku mengada-adakannya, maka kamu tidak kuasa sedikit pun menghindarkan aku dari (azab) Allah. Dia lebih tahu apa yang kamu percakapkan tentang Al-Qur'an itu. Cu

46:9

# قُلْ مَا كُنْتُ بِدْعًا مِّنَ الرُّسُلِ وَمَآ اَدْرِيْ مَا يُفْعَلُ بِيْ وَلَا بِكُمْۗ اِنْ اَتَّبِعُ اِلَّا مَا يُوْحٰٓى اِلَيَّ وَمَآ اَنَا۠ اِلَّا نَذِيْرٌ مُّبِيْنٌ

qul m*aa* kuntu bid'an mina **al**rrusuli wam*aa* adrii m*aa* yuf'alu bii wal*aa* bikum in attabi'u ill*aa* m*aa* yuu*haa* ilayya wam*aa* an*aa* ill*aa* na*dz*iirun mubiin**un**

**Katakanlah (Muhammad), “Aku bukanlah Rasul yang pertama di antara rasul-rasul dan aku tidak tahu apa yang akan diperbuat terhadapku dan terhadapmu. Aku hanyalah mengikuti apa yang diwahyukan kepadaku dan aku hanyalah pemberi peringatan yang menjelaskan.”**

46:10

# قُلْ اَرَءَيْتُمْ اِنْ كَانَ مِنْ عِنْدِ اللّٰهِ وَكَفَرْتُمْ بِهٖ وَشَهِدَ شَاهِدٌ مِّنْۢ بَنِيْٓ اِسْرَاۤءِيْلَ عَلٰى مِثْلِهٖ فَاٰمَنَ وَاسْتَكْبَرْتُمْۗ اِنَّ اللّٰهَ لَا يَهْدِى الْقَوْمَ الظّٰلِمِيْنَ ࣖ

qul ara-aytum in k*aa*na min 'indi **al**l*aa*hi wakafartum bihi wasyahida sy*aa*hidun min banii isr*aa*-iila 'al*aa* mitslihi fa*aa*mana wa**i**stakbartum inna **al**l*aa*ha l*aa*

Katakanlah, “Terangkanlah kepadaku, bagaimana pendapatmu jika sebenarnya (Al-Qur'an) ini datang dari Allah, dan kamu mengingkarinya, padahal ada seorang saksi dari Bani Israil yang mengakui (kebenaran) yang serupa dengan (yang disebut dalam) Al-Qur'an lal

46:11

# وَقَالَ الَّذِيْنَ كَفَرُوْا لِلَّذِيْنَ اٰمَنُوْا لَوْ كَانَ خَيْرًا مَّا سَبَقُوْنَآ اِلَيْهِۗ وَاِذْ لَمْ يَهْتَدُوْا بِهٖ فَسَيَقُوْلُوْنَ هٰذَآ اِفْكٌ قَدِيْمٌ

waq*aa*la **al**la*dz*iina kafaruu lilla*dz*iina *aa*manuu law k*aa*na khayran m*aa* sabaquun*aa* ilayhi wa-i*dz* lam yahtaduu bihi fasayaquuluuna h*aadzaa* ifkun qadiim**un**

Dan orang-orang yang kafir berkata kepada orang-orang yang beriman, “Sekiranya Al-Qur'an itu sesuatu yang baik, tentu mereka tidak pantas mendahului kami (beriman) kepadanya.” Tetapi karena mereka tidak mendapat petunjuk dengannya maka mereka akan berkata

46:12

# وَمِنْ قَبْلِهٖ كِتٰبُ مُوْسٰٓى اِمَامًا وَّرَحْمَةً ۗوَهٰذَا كِتٰبٌ مُّصَدِّقٌ لِّسَانًا عَرَبِيًّا لِّيُنْذِرَ الَّذِيْنَ ظَلَمُوْا ۖوَبُشْرٰى لِلْمُحْسِنِيْنَ

wamin qablihi kit*aa*bu muus*aa* im*aa*man wara*h*matan wah*aadzaa* kit*aa*bun mu*sh*addiqun lis*aa*nan 'arabiyyan liyun*dz*ira **al**la*dz*iina *zh*alamuu wabusyr*aa* lilmu*h*s

Dan sebelum (Al-Qur'an) itu telah ada Kitab Musa sebagai petunjuk dan rahmat. Dan (Al-Qur'an) ini adalah Kitab yang membenarkannya dalam bahasa Arab untuk memberi peringatan kepada orang-orang yang zalim dan memberi kabar gembira kepada orang-orang yang b

46:13

# اِنَّ الَّذِيْنَ قَالُوْا رَبُّنَا اللّٰهُ ثُمَّ اسْتَقَامُوْا فَلَا خَوْفٌ عَلَيْهِمْ وَلَا هُمْ يَحْزَنُوْنَۚ

inna **al**la*dz*iina q*aa*luu rabbun*aa* **al**l*aa*hu tsumma istaq*aa*muu fal*aa* khawfun 'alayhim wal*aa* hum ya*h*zanuun**a**

Sesungguhnya orang-orang yang berkata, “Tuhan kami adalah Allah,” kemudian mereka tetap istiqamah tidak ada rasa khawatir pada mereka, dan mereka tidak (pula) bersedih hati.

46:14

# اُولٰۤىِٕكَ اَصْحٰبُ الْجَنَّةِ خٰلِدِيْنَ فِيْهَاۚ جَزَاۤءً ۢبِمَا كَانُوْا يَعْمَلُوْنَ

ul*aa*-ika a*sh*-*haa*bu **a**ljannati kh*aa*lidiina fiih*aa* jaz*aa*-an bim*aa* k*aa*nuu ya'maluun**a**

Mereka itulah para penghuni surga, kekal di dalamnya; sebagai balasan atas apa yang telah mereka kerjakan.

46:15

# وَوَصَّيْنَا الْاِنْسَانَ بِوَالِدَيْهِ اِحْسَانًا ۗحَمَلَتْهُ اُمُّهٗ كُرْهًا وَّوَضَعَتْهُ كُرْهًا ۗوَحَمْلُهٗ وَفِصٰلُهٗ ثَلٰثُوْنَ شَهْرًا ۗحَتّٰىٓ اِذَا بَلَغَ اَشُدَّهٗ وَبَلَغَ اَرْبَعِيْنَ سَنَةًۙ قَالَ رَبِّ اَوْزِعْنِيْٓ اَنْ اَشْكُرَ نِعْمَتَك

wawa*shsh*ayn*aa* **a**l-ins*aa*na biw*aa*lidayhi i*h*s*aa*nan *h*amalat-hu ummuhu kurhan wawa*dh*a'at-hu kurhan wa*h*amluhu wafi*shaa*luhu tsal*aa*tsuuna syahran *h*att*aa* i

Dan Kami perintahkan kepada manusia agar berbuat baik kepada kedua orang tuanya. Ibunya telah mengandungnya dengan susah payah, dan melahirkannya dengan susah payah (pula). Masa mengandung sampai menyapihnya selama tiga puluh bulan, sehingga apabila dia (

46:16

# اُولٰۤىِٕكَ الَّذِيْنَ نَتَقَبَّلُ عَنْهُمْ اَحْسَنَ مَا عَمِلُوْا وَنَتَجَاوَزُ عَنْ سَيِّاٰتِهِمْ فِيْٓ اَصْحٰبِ الْجَنَّةِۗ وَعْدَ الصِّدْقِ الَّذِيْ كَانُوْا يُوْعَدُوْنَ

ul*aa*-ika **al**la*dz*iina nataqabbalu 'anhum a*h*sana m*aa* 'amiluu wanataj*aa*wazu 'an sayyi-*aa*tihim fii a*sh*-*haa*bi **a**ljannati wa'da **al***shsh*idqi **al**

Mereka itulah orang-orang yang Kami terima amal baiknya yang telah mereka kerjakan dan (orang-orang) yang Kami maafkan kesalahan-kesalahannya, (mereka akan menjadi) penghuni-penghuni surga. Itu janji yang benar yang telah dijanjikan kepada mereka.

46:17

# وَالَّذِيْ قَالَ لِوَالِدَيْهِ اُفٍّ لَّكُمَآ اَتَعِدَانِنِيْٓ اَنْ اُخْرَجَ وَقَدْ خَلَتِ الْقُرُوْنُ مِنْ قَبْلِيْۚ وَهُمَا يَسْتَغِيْثٰنِ اللّٰهَ وَيْلَكَ اٰمِنْ ۖاِنَّ وَعْدَ اللّٰهِ حَقٌّۚ فَيَقُوْلُ مَا هٰذَآ اِلَّآ اَسَاطِيْرُ الْاَوَّلِيْنَ

wa**a**lla*dz*ii q*aa*la liw*aa*lidayhi uffin lakum*aa* ata'id*aa*ninii an ukhraja waqad khalati **a**lquruunu min qablii wahum*aa* yastaghiits*aa*ni **al**l*aa*ha waylaka *a*

Dan orang yang berkata kepada kedua orang tuanya, “Ah.” Apakah kamu berdua memperingatkan kepadaku bahwa aku akan dibangkitkan (dari kubur), padahal beberapa umat sebelumku telah berlalu? Lalu kedua orang tuanya itu memohon pertolongan kepada Allah (seray

46:18

# اُولٰۤىِٕكَ الَّذِيْنَ حَقَّ عَلَيْهِمُ الْقَوْلُ فِيْٓ اُمَمٍ قَدْ خَلَتْ مِنْ قَبْلِهِمْ مِّنَ الْجِنِّ وَالْاِنْسِ ۗاِنَّهُمْ كَانُوْا خٰسِرِيْنَ

ul*aa*-ika **al**la*dz*iina *h*aqqa 'alayhimu **a**lqawlu fii umamin qad khalat min qablihim mina **a**ljinni wa**a**l-insi innahum k*aa*nuu kh*aa*siriin**a**

Mereka itu orang-orang yang telah pasti terkena ketetapan (azab) bersama umat-umat dahulu sebelum mereka, dari (golongan) jin dan manusia. Mereka adalah orang-orang yang rugi.

46:19

# وَلِكُلٍّ دَرَجٰتٌ مِّمَّا عَمِلُوْاۚ وَلِيُوَفِّيَهُمْ اَعْمَالَهُمْ وَهُمْ لَا يُظْلَمُوْنَ

walikullin daraj*aa*tun mimm*aa* 'amiluu waliyuwaffiyahum a'm*aa*lahum wahum l*aa* yu*zh*lamuun**a**

Dan setiap orang memperoleh tingkatan sesuai dengan apa yang telah mereka kerjakan dan agar Allah mencukupkan balasan amal perbuatan mereka dan mereka tidak dirugikan.

46:20

# وَيَوْمَ يُعْرَضُ الَّذِيْنَ كَفَرُوْا عَلَى النَّارِۗ اَذْهَبْتُمْ طَيِّبٰتِكُمْ فِيْ حَيَاتِكُمُ الدُّنْيَا وَاسْتَمْتَعْتُمْ بِهَاۚ فَالْيَوْمَ تُجْزَوْنَ عَذَابَ الْهُوْنِ بِمَا كُنْتُمْ تَسْتَكْبِرُوْنَ فِى الْاَرْضِ بِغَيْرِ الْحَقِّ وَبِمَا كُنْتُ

wayawma yu'ra*dh*u **al**la*dz*iina kafaruu 'al*aa* **al**nn*aa*ri a*dz*habtum *th*ayyib*aa*tikum fii *h*ay*aa*tikumu **al**dduny*aa* wa**i**stamta'tum b

Dan (ingatlah) pada hari (ketika) orang-orang kafir dihadapkan ke neraka (seraya dikatakan kepada mereka), “Kamu telah menghabiskan (rezeki) yang baik untuk kehidupan duniamu dan kamu telah bersenang-senang (menikmati)nya; maka pada hari ini kamu dibalas

46:21

# ۞ وَاذْكُرْ اَخَا عَادٍۗ اِذْ اَنْذَرَ قَوْمَهٗ بِالْاَحْقَافِ وَقَدْ خَلَتِ النُّذُرُ مِنْۢ بَيْنِ يَدَيْهِ وَمِنْ خَلْفِهٖٓ اَلَّا تَعْبُدُوْٓا اِلَّا اللّٰهَ ۗاِنِّيْٓ اَخَافُ عَلَيْكُمْ عَذَابَ يَوْمٍ عَظِيْمٍ

wa**u***dz*kur akh*aa *'*aa*din i*dz *an*dz*ara qawmahu bi**a**l-a*h*q*aa*fi waqad khalati **al**nnu*dz*uru min bayni yadayhi wamin khalfihi **al**l*aa* ta'bu

Dan ingatlah (Hud) saudara kaum ‘Ad yaitu ketika dia mengingatkan kaumnya tentang bukit-bukit pasir dan sesungguhnya telah berlalu beberapa orang pemberi peringatan sebelumnya dan setelahnya (dengan berkata), “Janganlah kamu menyembah selain Allah, aku su

46:22

# قَالُوْٓا اَجِئْتَنَا لِتَأْفِكَنَا عَنْ اٰلِهَتِنَاۚ فَأْتِنَا بِمَا تَعِدُنَآ اِنْ كُنْتَ مِنَ الصّٰدِقِيْنَ

q*aa*luu aji/tan*aa* lita/fikan*aa* 'an *aa*lihatin*aa* fa/tin*aa* bim*aa* ta'idun*aa* in kunta mina **al***shshaa*diqiin**a**

Mereka menjawab, “Apakah engkau datang kepada kami untuk memalingkan kami dari (menyembah) tuhan-tuhan kami? Maka datangkanlah kepada kami azab yang telah engkau ancamkan kepada kami jika engkau termasuk orang yang benar.”

46:23

# قَالَ اِنَّمَا الْعِلْمُ عِنْدَ اللّٰهِ ۖوَاُبَلِّغُكُمْ مَّآ اُرْسِلْتُ بِهٖ وَلٰكِنِّيْٓ اَرٰىكُمْ قَوْمًا تَجْهَلُوْنَ

q*aa*la innam*aa* **a**l'ilmu 'inda **al**l*aa*hi wauballighukum m*aa* ursiltu bihi wal*aa*kinnii ar*aa*kum qawman tajhaluun**a**

Dia (Hud) berkata, “Sesungguhnya ilmu (tentang itu) hanya pada Allah dan aku (hanya) menyampaikan kepadamu apa yang diwahyukan kepadaku, tetapi aku melihat kamu adalah kaum yang berlaku bodoh.”

46:24

# فَلَمَّا رَاَوْهُ عَارِضًا مُّسْتَقْبِلَ اَوْدِيَتِهِمْ قَالُوْا هٰذَا عَارِضٌ مُّمْطِرُنَا ۗبَلْ هُوَ مَا اسْتَعْجَلْتُمْ بِهٖ ۗرِيْحٌ فِيْهَا عَذَابٌ اَلِيْمٌۙ

falamm*aa* ra-awhu '*aa*ri*dh*an mustaqbila awdiyatihim q*aa*luu h*aadzaa* '*aa*ri*dh*un mum*th*irun*aa* bal huwa m*aa* ista'jaltum bihi rii*h*un fiih*aa* 'a*dzaa*bun **a**liim<

Maka ketika mereka melihat azab itu berupa awan yang menuju ke lembah-lembah mereka, mereka berkata, “Inilah awan yang akan menurunkan hujan kepada kita.” (Bukan!) Tetapi itulah azab yang kamu minta agar disegerakan datangnya (yaitu) angin yang mengandung

46:25

# تُدَمِّرُ كُلَّ شَيْءٍۢ بِاَمْرِ رَبِّهَا فَاَصْبَحُوْا لَا يُرٰىٓ اِلَّا مَسٰكِنُهُمْۗ كَذٰلِكَ نَجْزِى الْقَوْمَ الْمُجْرِمِيْنَ

tudammiru kulla syay-in bi-amri rabbih*aa* fa-a*sh*ba*h*uu l*aa* yur*aa* ill*aa* mas*aa*kinuhum ka*dzaa*lika najzii **a**lqawma **a**lmujrimiin**a**

yang menghancurkan segala sesuatu dengan perintah Tuhannya, sehingga mereka (kaum ‘Ad) menjadi tidak tampak lagi (di bumi) kecuali hanya (bekas-bekas) tempat tinggal mereka. Demikianlah Kami memberi balasan kepada kaum yang berdosa.

46:26

# وَلَقَدْ مَكَّنّٰهُمْ فِيْمَآ اِنْ مَّكَّنّٰكُمْ فِيْهِ وَجَعَلْنَا لَهُمْ سَمْعًا وَّاَبْصَارًا وَّاَفْـِٕدَةًۖ فَمَآ اَغْنٰى عَنْهُمْ سَمْعُهُمْ وَلَآ اَبْصَارُهُمْ وَلَآ اَفْـِٕدَتُهُمْ مِّنْ شَيْءٍ اِذْ كَانُوْا يَجْحَدُوْنَ بِاٰيٰتِ اللّٰهِ وَحَا

walaqad makkann*aa*hum fiim*aa* in makkann*aa*kum fiihi waja'aln*aa* lahum sam'an wa-ab*shaa*ran wa-af-idatan fam*aa* aghn*aa* 'anhum sam'uhum wal*aa* ab*shaa*ruhum wal*aa* af-idatuhum min syay-in i*dz*

*Dan sungguh, Kami telah meneguhkan kedudukan mereka (dengan kemakmuran dan kekuatan) yang belum pernah Kami berikan kepada kamu dan Kami telah memberikan kepada mereka pendengaran, penglihatan, dan hati; tetapi pendengaran, penglihatan, dan hati mereka it*

46:27

# وَلَقَدْ اَهْلَكْنَا مَا حَوْلَكُمْ مِّنَ الْقُرٰى وَصَرَّفْنَا الْاٰيٰتِ لَعَلَّهُمْ يَرْجِعُوْنَ

walaqad ahlakn*aa* m*aa* *h*awlakum mina **a**lqur*aa* wa*sh*arrafn*aa* **a**l-*aa*y*aa*ti la'allahum yarji'uun**a**

Dan sungguh, telah Kami binasakan negeri-negeri di sekitarmu dan juga telah Kami menjelaskan berulang-ulang tanda-tanda (kebesaran Kami), agar mereka kembali (bertobat).

46:28

# فَلَوْلَا نَصَرَهُمُ الَّذِيْنَ اتَّخَذُوْا مِنْ دُوْنِ اللّٰهِ قُرْبَانًا اٰلِهَةً ۗبَلْ ضَلُّوْا عَنْهُمْۚ وَذٰلِكَ اِفْكُهُمْ وَمَا كَانُوْا يَفْتَرُوْنَ

falawl*aa* na*sh*arahumu **al**la*dz*iina ittakha*dz*uu min duuni **al**l*aa*hi qurb*aa*nan *aa*lihatan bal *dh*alluu 'anhum wa*dzaa*lika ifkuhum wam*aa* k*aa*nuu yaftaruun

Maka mengapa (berhala-berhala dan tuhan-tuhan) yang mereka sembah selain Allah untuk mendekatkan diri (kepada-Nya) tidak dapat menolong mereka? Bahkan tuhan-tuhan itu telah lenyap dari mereka? Dan itulah akibat kebohongan mereka dan apa yang dahulu mereka

46:29

# وَاِذْ صَرَفْنَآ اِلَيْكَ نَفَرًا مِّنَ الْجِنِّ يَسْتَمِعُوْنَ الْقُرْاٰنَۚ فَلَمَّا حَضَرُوْهُ قَالُوْٓا اَنْصِتُوْاۚ فَلَمَّا قُضِيَ وَلَّوْا اِلٰى قَوْمِهِمْ مُّنْذِرِيْنَ

wa-i*dz* *sh*arafn*aa* ilayka nafaran mina **a**ljinni yastami'uuna **a**lqur-*aa*na falamm*aa* *h*a*dh*aruuhu q*aa*luu an*sh*ituu falamm*aa* qu*dh*iya wallaw il*aa* qaw

Dan (ingatlah) ketika Kami hadapkan kepadamu (Muhammad) serombongan jin yang mendengarkan (bacaan) Al-Qur'an, maka ketika mereka menghadiri (pembacaan)nya mereka berkata, “Diamlah kamu (untuk mendengarkannya)!” Maka ketika telah selesai mereka kembali kep

46:30

# قَالُوْا يٰقَوْمَنَآ اِنَّا سَمِعْنَا كِتٰبًا اُنْزِلَ مِنْۢ بَعْدِ مُوْسٰى مُصَدِّقًا لِّمَا بَيْنَ يَدَيْهِ يَهْدِيْٓ اِلَى الْحَقِّ وَاِلٰى طَرِيْقٍ مُّسْتَقِيْمٍ

q*aa*luu y*aa* qawman*aa* inn*aa* sami'n*aa* kit*aa*ban unzila min ba'di muus*aa* mu*sh*addiqan lim*aa* bayna yadayhi yahdii il*aa* **a**l*h*aqqi wa-il*aa* *th*ariiqin mustaqiim

Mereka berkata, “Wahai kaum kami! Sungguh, kami telah mendengarkan Kitab (Al-Qur'an) yang diturunkan setelah Musa, membenarkan (kitab-kitab) yang datang sebelumnya, membimbing kepada kebenaran dan kepada jalan yang lurus.

46:31

# يٰقَوْمَنَآ اَجِيْبُوْا دَاعِيَ اللّٰهِ وَاٰمِنُوْا بِهٖ يَغْفِرْ لَكُمْ مِّنْ ذُنُوْبِكُمْ وَيُجِرْكُمْ مِّنْ عَذَابٍ اَلِيْمٍ

y*aa* qawman*aa* ajiibuu d*aa*'iya **al**l*aa*hi wa*aa*minuu bihi yaghfir lakum min *dz*unuubikum wayujirkum min 'a*dzaa*bin **a**liim**in**

Wahai kaum kami! Terimalah (seruan) orang (Muhammad) yang menyeru kepada Allah. Dan berimanlah kepada-Nya, niscaya Dia akan mengampuni dosa-dosamu dan melepaskan kamu dari azab yang pedih.

46:32

# وَمَنْ لَّا يُجِبْ دَاعِيَ اللّٰهِ فَلَيْسَ بِمُعْجِزٍ فِى الْاَرْضِ وَلَيْسَ لَهٗ مِنْ دُوْنِهٖٓ اَوْلِيَاۤءُ ۗ اُولٰۤىِٕكَ فِيْ ضَلٰلٍ مُّبِيْنٍ

waman l*aa* yujib d*aa*'iya **al**l*aa*hi falaysa bimu'jizin fii **a**l-ar*dh*i walaysa lahu min duunihi awliy*aa*-a ul*aa*-ika fii *dh*al*aa*lin mubiin**in**

Dan barang siapa tidak menerima (seruan) orang yang menyeru kepada Allah (Muhammad) maka dia tidak akan dapat melepaskan diri dari siksa Allah di bumi padahal tidak ada pelindung baginya selain Allah. Mereka berada dalam kesesatan yang nyata.”

46:33

# اَوَلَمْ يَرَوْا اَنَّ اللّٰهَ الَّذِيْ خَلَقَ السَّمٰوٰتِ وَالْاَرْضَ وَلَمْ يَعْيَ بِخَلْقِهِنَّ بِقٰدِرٍ عَلٰٓى اَنْ يُّحْيِ َۧ الْمَوْتٰى ۗبَلٰٓى اِنَّهٗ عَلٰى كُلِّ شَيْءٍ قَدِيْرٌ

awa lam yaraw anna **al**l*aa*ha **al**la*dz*ii khalaqa **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*a walam ya'ya bikhalqihinna biq*aa*dirin 'al*aa* an yu*h*yiya

Dan tidakkah mereka memperhatikan bahwa sesungguhnya Allah yang menciptakan langit dan bumi dan Dia tidak merasa payah karena menciptakannya, dan Dia kuasa menghidupkan yang mati? Begitulah; sungguh, Dia Mahakuasa atas segala sesuatu.

46:34

# وَيَوْمَ يُعْرَضُ الَّذِيْنَ كَفَرُوْا عَلَى النَّارِۗ اَلَيْسَ هٰذَا بِالْحَقِّ ۗ قَالُوْا بَلٰى وَرَبِّنَا ۗقَالَ فَذُوْقُوا الْعَذَابَ بِمَا كُنْتُمْ تَكْفُرُوْنَ

wayawma yu'ra*dh*u **al**la*dz*iina kafaruu 'al*aa* **al**nn*aa*ri alaysa h*aadzaa* bi**a**l*h*aqqi q*aa*luu bal*aa* warabbin*aa* q*aa*la fa*dz*uuquu al'a*dzaa*

Dan (ingatlah) pada hari (ketika) orang-orang yang kafir dihadapkan kepada neraka, (mereka akan ditanya), “Bukankah (azab) ini benar?” Mereka menjawab, “Ya benar, demi Tuhan kami.” Allah berfirman, “Maka rasakanlah azab ini disebabkan dahulu kamu mengingk

46:35

# فَاصْبِرْ كَمَا صَبَرَ اُولُوا الْعَزْمِ مِنَ الرُّسُلِ وَلَا تَسْتَعْجِلْ لَّهُمْ ۗ كَاَنَّهُمْ يَوْمَ يَرَوْنَ مَا يُوْعَدُوْنَۙ لَمْ يَلْبَثُوْٓا اِلَّا سَاعَةً مِّنْ نَّهَارٍ ۗ بَلٰغٌ ۚفَهَلْ يُهْلَكُ اِلَّا الْقَوْمُ الْفٰسِقُوْنَ ࣖ

fa**i***sh*bir kam*aa* *sh*abara uluu **a**l'azmi mina **al**rrusuli wal*aa* tasta'jil lahum ka-annahum yawma yarawna m*aa* yuu'aduuna lam yalbatsuu ill*aa* s*aa*'atan min nah*aa<*

Maka bersabarlah kamu seperti orang-orang yang mempunyai keteguhan hati dari rasul-rasul telah bersabar dan janganlah kamu meminta disegerakan (azab) bagi mereka. Pada hari mereka melihat azab yang diancamkan kepada mereka (merasa) seolah-olah tidak tinggal (di dunia) melainkan sesaat pada siang hari. (Inilah) suatu pelajaran yang cukup, maka tidak dibinasakan melainkan kaum yang fasik.

<!--EndFragment-->