---
title: (62) Al-Jumu'ah - الجمعة
date: 2021-10-27T04:02:58.936Z
ayat: 62
description: "Jumlah Ayat: 11 / Arti: Jumat"
---
<!--StartFragment-->

62:1

# يُسَبِّحُ لِلّٰهِ مَا فِى السَّمٰوٰتِ وَمَا فِى الْاَرْضِ الْمَلِكِ الْقُدُّوْسِ الْعَزِيْزِ الْحَكِيْمِ

yusabbi*h*u lill*aa*hi m*aa* fii **al**ssam*aa*w*aa*ti wam*aa* fii **a**l-ar*dh*i **a**lmaliki **a**lqudduusi **a**l'aziizi **a**l*h*akii

Apa yang ada di langit dan apa yang ada di bumi senantiasa bertasbih kepada Allah. Maharaja, Yang Mahasuci, Yang Mahaperkasa, Mahabijaksana.

62:2

# هُوَ الَّذِيْ بَعَثَ فِى الْاُمِّيّٖنَ رَسُوْلًا مِّنْهُمْ يَتْلُوْا عَلَيْهِمْ اٰيٰتِهٖ وَيُزَكِّيْهِمْ وَيُعَلِّمُهُمُ الْكِتٰبَ وَالْحِكْمَةَ وَاِنْ كَانُوْا مِنْ قَبْلُ لَفِيْ ضَلٰلٍ مُّبِيْنٍۙ

huwa **al**la*dz*ii ba'atsa fii **a**l-ummiyyiina rasuulan minhum yatluu 'alayhim *aa*y*aa*tihi wayuzakkiihim wayu'allimuhumu **a**lkit*aa*ba wa**a**l*h*ikmata wa-in k*aa*nu

Dialah yang mengutus seorang Rasul kepada kaum yang buta huruf dari kalangan mereka sendiri, yang membacakan kepada mereka ayat-ayat-Nya, menyucikan (jiwa) mereka dan mengajarkan kepada mereka Kitab dan Hikmah (Sunnah), meskipun sebelumnya, mereka benar-b

62:3

# وَّاٰخَرِيْنَ مِنْهُمْ لَمَّا يَلْحَقُوْا بِهِمْۗ وَهُوَ الْعَزِيْزُ الْحَكِيْمُۙ

wa*aa*khariina minhum lamm*aa* yal*h*aquu bihim wahuwa **a**l'aziizu **a**l*h*akiim**u**

Dan (juga) kepada kaum yang lain dari mereka yang belum berhubungan dengan mereka. Dan Dialah Yang Mahaperkasa, Mahabijaksana.

62:4

# ذٰلِكَ فَضْلُ اللّٰهِ يُؤْتِيْهِ مَنْ يَّشَاۤءُۗ وَاللّٰهُ ذُو الْفَضْلِ الْعَظِيْمِ

*dzaa*lika fa*dh*lu **al**l*aa*hi yu/tiihi man yasy*aa*u wa**al**l*aa*hu *dz*uu **a**lfa*dh*li **a**l'a*zh*iim**i**

Demikianlah karunia Allah, yang diberikan kepada siapa yang Dia kehendaki; dan Allah memiliki karunia yang besar.

62:5

# مَثَلُ الَّذِيْنَ حُمِّلُوا التَّوْرٰىةَ ثُمَّ لَمْ يَحْمِلُوْهَا كَمَثَلِ الْحِمَارِ يَحْمِلُ اَسْفَارًاۗ بِئْسَ مَثَلُ الْقَوْمِ الَّذِيْنَ كَذَّبُوْا بِاٰيٰتِ اللّٰهِ ۗوَاللّٰهُ لَا يَهْدِى الْقَوْمَ الظّٰلِمِيْنَ

matsalu **al**la*dz*iina *h*ummiluu **al**ttawr*aa*ta tsumma lam ya*h*miluuh*aa* kamatsali **a**l*h*im*aa*ri ya*h*milu asf*aa*ran bi/sa matsalu **a**lqawmi

Perumpamaan orang-orang yang diberi tugas membawa Taurat, kemudian mereka tidak membawanya (tidak mengamalkannya) adalah seperti keledai yang membawa kitab-kitab yang tebal. Sangat buruk perumpamaan kaum yang mendustakan ayat-ayat Allah. Dan Allah tidak m

62:6

# قُلْ يٰٓاَيُّهَا الَّذِيْنَ هَادُوْٓا اِنْ زَعَمْتُمْ اَنَّكُمْ اَوْلِيَاۤءُ لِلّٰهِ مِنْ دُوْنِ النَّاسِ فَتَمَنَّوُا الْمَوْتَ اِنْ كُنْتُمْ صٰدِقِيْنَ

qul y*aa* ayyuh*aa* **al**la*dz*iina h*aa*duu in za'amtum annakum awliy*aa*u lill*aa*hi min duuni **al**nn*aa*si fatamannawuu **a**lmawta in kuntum *shaa*diqiin**a**

**Katakanlah (Muhammad), “Wahai orang-orang Yahudi! Jika kamu mengira bahwa kamulah kekasih Allah, bukan orang-orang yang lain, maka harapkanlah kematianmu, jika kamu orang yang benar.”**

62:7

# وَلَا يَتَمَنَّوْنَهٗٓ اَبَدًاۢ بِمَا قَدَّمَتْ اَيْدِيْهِمْۗ وَاللّٰهُ عَلِيْمٌۢ بِالظّٰلِمِيْنَ

wal*aa* yatamannawnahu abadan bim*aa* qaddamat aydiihim wa**al**l*aa*hu 'aliimun bi**al***zhzhaa*limiin**a**

Dan mereka tidak akan mengharapkan kematian itu selamanya disebabkan kejahatan yang telah mereka perbuat dengan tangan mereka sendiri. Dan Allah Maha Mengetahui orang-orang yang zalim.

62:8

# قُلْ اِنَّ الْمَوْتَ الَّذِيْ تَفِرُّوْنَ مِنْهُ فَاِنَّهٗ مُلٰقِيْكُمْ ثُمَّ تُرَدُّوْنَ اِلٰى عَالِمِ الْغَيْبِ وَالشَّهَادَةِ فَيُنَبِّئُكُمْ بِمَا كُنْتُمْ تَعْمَلُوْنَ ࣖ

qul inna **a**lmawta **al**la*dz*ii tafirruuna minhu fa-innahu mul*aa*qiikum tsumma turadduuna il*aa* '*aa*limi **a**lghaybi wa**al**sysyah*aa*dati fayunabbi-ukum bim*aa* ku

Katakanlah, “Sesungguhnya kematian yang kamu lari dari padanya, ia pasti menemui kamu, kemudian kamu akan dikembalikan kepada (Allah), yang mengetahui yang gaib dan yang nyata, lalu Dia beritakan kepadamu apa yang telah kamu kerjakan.”

62:9

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْٓا اِذَا نُوْدِيَ لِلصَّلٰوةِ مِنْ يَّوْمِ الْجُمُعَةِ فَاسْعَوْا اِلٰى ذِكْرِ اللّٰهِ وَذَرُوا الْبَيْعَۗ ذٰلِكُمْ خَيْرٌ لَّكُمْ اِنْ كُنْتُمْ تَعْلَمُوْنَ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu i*dzaa* nuudiya li**l***shsh*al*aa*ti min yawmi **a**ljumu'ati fa**i**s'aw il*aa* *dz*ikri **al**l*a*

Wahai orang-orang yang beriman! Apabila telah diseru untuk melaksanakan salat pada hari Jum‘at, maka segeralah kamu mengingat Allah dan tinggalkanlah jual beli. Yang demikian itu lebih baik bagimu jika kamu mengetahui.

62:10

# فَاِذَا قُضِيَتِ الصَّلٰوةُ فَانْتَشِرُوْا فِى الْاَرْضِ وَابْتَغُوْا مِنْ فَضْلِ اللّٰهِ وَاذْكُرُوا اللّٰهَ كَثِيْرًا لَّعَلَّكُمْ تُفْلِحُوْنَ

fa-i*dzaa* qu*dh*iyati **al***shsh*al*aa*tu fa**i**ntasyiruu fii **a**l-ar*dh*i wa**i**btaghuu min fa*dh*li **al**l*aa*hi wa**u***dz*kuru

Apabila salat telah dilaksanakan, maka bertebaranlah kamu di bumi; carilah karunia Allah dan ingatlah Allah banyak-banyak agar kamu beruntung.

62:11

# وَاِذَا رَاَوْا تِجَارَةً اَوْ لَهْوًا ۨانْفَضُّوْٓا اِلَيْهَا وَتَرَكُوْكَ قَاۤىِٕمًاۗ قُلْ مَا عِنْدَ اللّٰهِ خَيْرٌ مِّنَ اللَّهْوِ وَمِنَ التِّجَارَةِۗ وَاللّٰهُ خَيْرُ الرّٰزِقِيْنَ ࣖ

wa iżā ra\`au tijāratan au lahwaninfaḍḍū ilaihā wa tarakụka qā\`imā, qul mā ‘indallāhi khairum minal-lahwi wa minat-tijārah, wallāhu khairur-rāziqīn 

Dan apabila mereka melihat perniagaan atau permainan, mereka bubar untuk menuju kepadanya dan mereka tinggalkan kamu sedang berdiri (berkhotbah). Katakanlah: “Apa yang di sisi Allah lebih baik daripada permainan dan perniagaan”, dan Allah Sebaik-baik Pemberi rezeki.

<!--EndFragment-->