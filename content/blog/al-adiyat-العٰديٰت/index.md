---
title: (100) Al-'Adiyat - العٰديٰت
date: 2021-10-27T04:33:20.150Z
ayat: 100
description: "Jumlah Ayat: 11 / Arti: Kuda Yang Berlari Kencang  "
---
<!--StartFragment-->

100:1

# وَالْعٰدِيٰتِ ضَبْحًاۙ

wa**a**l'*aa*diy*aa*ti *dh*ab*haa***n**

Demi kuda perang yang berlari kencang terengah-engah,

100:2

# فَالْمُوْرِيٰتِ قَدْحًاۙ

fa**a**lmuuriy*aa*ti qad*haa***n**

dan kuda yang memercikkan bunga api (dengan pukulan kuku kakinya),

100:3

# فَالْمُغِيْرٰتِ صُبْحًاۙ

fa**a**lmughiir*aa*ti *sh*ub*haa***n**

dan kuda yang menyerang (dengan tiba-tiba) pada waktu pagi,

100:4

# فَاَثَرْنَ بِهٖ نَقْعًاۙ

fa-atsarna bihi naq'*aa***n**

sehingga menerbangkan debu,

100:5

# فَوَسَطْنَ بِهٖ جَمْعًاۙ

fawasa*th*na bihi jam'*aa***n**

lalu menyerbu ke tengah-tengah kumpulan musuh,

100:6

# اِنَّ الْاِنْسَانَ لِرَبِّهٖ لَكَنُوْدٌ ۚ

inna **a**l-ins*aa*na lirabbihi lakanuud**un**

sungguh, manusia itu sangat ingkar, (tidak bersyukur) kepada Tuhannya,

100:7

# وَاِنَّهٗ عَلٰى ذٰلِكَ لَشَهِيْدٌۚ

wa-innahu 'al*aa* *dzaa*lika lasyahiid**un**

dan sesungguhnya dia (manusia) menyaksikan (mengakui) keingkarannya,

100:8

# وَاِنَّهٗ لِحُبِّ الْخَيْرِ لَشَدِيْدٌ ۗ

wa-innahu li*h*ubbi **a**lkhayri lasyadiid**un**

dan sesungguhnya cintanya kepada harta benar-benar berlebihan.

100:9

# ۞ اَفَلَا يَعْلَمُ اِذَا بُعْثِرَ مَا فِى الْقُبُوْرِۙ

afal*aa* ya'lamu i*dzaa* bu'tsira m*aa* fii **a**lqubuur**i**

Maka tidakkah dia mengetahui apabila apa yang di dalam kubur dikeluarkan,

100:10

# وَحُصِّلَ مَا فِى الصُّدُوْرِۙ

wa*h*u*shsh*ila m*aa* fii **al***shsh*uduur**i**

dan apa yang tersimpan di dalam dada dilahirkan?

100:11

# اِنَّ رَبَّهُمْ بِهِمْ يَوْمَىِٕذٍ لَّخَبِيْرٌ ࣖ

inna rabbahum bihim yawma-i*dz*in lakhabiir**un**

sungguh, Tuhan mereka pada hari itu Mahateliti terhadap keadaan mereka.

<!--EndFragment-->