---
title: (9) At-Taubah - التوبة
date: 2021-10-27T03:25:19.386Z
ayat: 9
description: "Jumlah Ayat: 129 / Arti: Pengampunan"
---
<!--StartFragment-->

9:1

# بَرَاۤءَةٌ مِّنَ اللّٰهِ وَرَسُوْلِهٖٓ اِلَى الَّذِيْنَ عَاهَدْتُّمْ مِّنَ الْمُشْرِكِيْنَۗ

bar*aa*-atun mina **al**l*aa*hi warasuulihi il*aa* **al**la*dz*iina '*aa*hadtum mina **a**lmusyrikiin**a**

(Inilah pernyataan) pemutusan hubungan dari Allah dan Rasul-Nya kepada orang-orang musyrik yang kamu telah mengadakan perjanjian (dengan mereka).

9:2

# فَسِيْحُوْا فِى الْاَرْضِ اَرْبَعَةَ اَشْهُرٍ وَّاعْلَمُوْٓا اَنَّكُمْ غَيْرُ مُعْجِزِى اللّٰهِ ۙوَاَنَّ اللّٰهَ مُخْزِى الْكٰفِرِيْنَ

fasii*h*uu fii **a**l-ar*dh*i arba'ata asyhurin wa**i**'lamuu annakum ghayru mu'jizii **al**l*aa*hi wa-anna **al**l*aa*ha mukhzii **a**lk*aa*firiin**a**

Maka berjalanlah kamu (kaum musyrikin) di bumi selama empat bulan dan ketahuilah bahwa kamu tidak dapat melemahkan Allah, dan sesungguhnya Allah menghinakan orang-orang kafir.

9:3

# وَاَذَانٌ مِّنَ اللّٰهِ وَرَسُوْلِهٖٓ اِلَى النَّاسِ يَوْمَ الْحَجِّ الْاَكْبَرِ اَنَّ اللّٰهَ بَرِيْۤءٌ مِّنَ الْمُشْرِكِيْنَ ەۙ وَرَسُوْلُهٗ ۗفَاِنْ تُبْتُمْ فَهُوَ خَيْرٌ لَّكُمْۚ وَاِنْ تَوَلَّيْتُمْ فَاعْلَمُوْٓا اَنَّكُمْ غَيْرُ مُعْجِزِى اللّٰهِ ۗو

wa-a*dzaa*nun mina **al**l*aa*hi warasuulihi il*aa* **al**nn*aa*si yawma **a**l*h*ajji **a**l-akbari anna **al**l*aa*ha barii-un mina **a**lmusyrik

Dan satu maklumat (pemberitahuan) dari Allah dan Rasul-Nya kepada umat manusia pada hari haji akbar, bahwa sesungguhnya Allah dan Rasul-Nya berlepas diri dari orang-orang musyrik. Kemudian jika kamu (kaum musyrikin) bertobat, maka itu lebih baik bagimu; d

9:4

# اِلَّا الَّذِيْنَ عَاهَدْتُّمْ مِّنَ الْمُشْرِكِيْنَ ثُمَّ لَمْ يَنْقُصُوْكُمْ شَيْـًٔا وَّلَمْ يُظَاهِرُوْا عَلَيْكُمْ اَحَدًا فَاَتِمُّوْٓا اِلَيْهِمْ عَهْدَهُمْ اِلٰى مُدَّتِهِمْۗ اِنَّ اللّٰهَ يُحِبُّ الْمُتَّقِيْنَ

ill*aa* **al**la*dz*iina '*aa*hadtum mina **a**lmusyrikiina tsumma lam yanqu*sh*uukum syay-an walam yu*zhaa*hiruu 'alaykum a*h*adan fa-atimmuu ilayhim 'ahdahum il*aa* muddatihim inna **al<**

kecuali orang-orang musyrik yang telah mengadakan perjanjian dengan kamu dan mereka sedikit pun tidak mengurangi (isi perjanjian) dan tidak (pula) mereka membantu seorang pun yang memusuhi kamu, maka terhadap mereka itu penuhilah janjinya sampai batas wak







9:5

# فَاِذَا انْسَلَخَ الْاَشْهُرُ الْحُرُمُ فَاقْتُلُوا الْمُشْرِكِيْنَ حَيْثُ وَجَدْتُّمُوْهُمْ وَخُذُوْهُمْ وَاحْصُرُوْهُمْ وَاقْعُدُوْا لَهُمْ كُلَّ مَرْصَدٍۚ فَاِنْ تَابُوْا وَاَقَامُوا الصَّلٰوةَ وَاٰتَوُا الزَّكٰوةَ فَخَلُّوْا سَبِيْلَهُمْۗ اِنَّ اللّٰه

fa-i*dzaa* insalakha **a**l-asyhuru **a**l*h*urumu fa**u**qtuluu **a**lmusyrikiina *h*aytsu wajadtumuuhum wakhu*dz*uuhum wa**u***hs*uruuhum wa**u**q'udu

Apabila telah habis bulan-bulan haram, maka perangilah orang-orang musyrik di mana saja kamu temui, tangkaplah dan kepunglah mereka, dan awasilah di tempat pengintaian. Jika mereka bertobat dan melaksanakan salat serta menunaikan zakat, maka berilah kebeb

9:6

# وَاِنْ اَحَدٌ مِّنَ الْمُشْرِكِيْنَ اسْتَجَارَكَ فَاَجِرْهُ حَتّٰى يَسْمَعَ كَلٰمَ اللّٰهِ ثُمَّ اَبْلِغْهُ مَأْمَنَهٗ ۗذٰلِكَ بِاَنَّهُمْ قَوْمٌ لَّا يَعْلَمُوْنَ ࣖ

wa-in a*h*adun mina **a**lmusyrikiina istaj*aa*raka fa-ajirhu *h*att*aa* yasma'a kal*aa*ma **al**l*aa*hi tsumma ablighhu ma/manahu *dzaa*lika bi-annahum qawmun l*aa* ya'lamuun**a**

**Dan jika di antara kaum musyrikin ada yang meminta perlindungan kepadamu, maka lindungilah agar dia dapat mendengar firman Allah, kemudian antarkanlah dia ke tempat yang aman baginya. (Demikian) itu karena sesungguhnya mereka kaum yang tidak mengetahui.**









9:7

# كَيْفَ يَكُوْنُ لِلْمُشْرِكِيْنَ عَهْدٌ عِنْدَ اللّٰهِ وَعِنْدَ رَسُوْلِهٖٓ اِلَّا الَّذِيْنَ عَاهَدْتُّمْ عِنْدَ الْمَسْجِدِ الْحَرَامِۚ فَمَا اسْتَقَامُوْا لَكُمْ فَاسْتَقِيْمُوْا لَهُمْ ۗاِنَّ اللّٰهَ يُحِبُّ الْمُتَّقِيْنَ

kayfa yakuunu lilmusyrikiina 'ahdun 'inda **al**l*aa*hi wa'inda rasuulihi ill*aa* **al**la*dz*iina '*aa*hadtum 'inda **a**lmasjidi **a**l*h*ar*aa*mi fam*aa* istaq*aa<*

Bagaimana mungkin ada perjanjian (aman) di sisi Allah dan Rasul-Nya dengan orang-orang musyrik, kecuali dengan orang-orang yang kamu telah mengadakan perjanjian (dengan mereka) di dekat Masjidilharam (Hudaibiyah), maka selama mereka berlaku jujur terhadap







9:8

# كَيْفَ وَاِنْ يَّظْهَرُوْا عَلَيْكُمْ لَا يَرْقُبُوْا فِيْكُمْ اِلًّا وَّلَا ذِمَّةً ۗيُرْضُوْنَكُمْ بِاَفْوَاهِهِمْ وَتَأْبٰى قُلُوْبُهُمْۚ وَاَكْثَرُهُمْ فٰسِقُوْنَۚ

kayfa wa-in ya*zh*haruu 'alaykum l*aa* yarqubuu fiikum illan wal*aa* *dz*immatan yur*dh*uunakum bi-afw*aa*hihim wata/b*aa* quluubuhum wa-aktsaruhum f*aa*siquun**a**

Bagaimana mungkin (ada perjanjian demikian), padahal jika mereka memperoleh kemenangan atas kamu, mereka tidak memelihara hubungan kekerabatan denganmu dan tidak (pula mengindahkan) perjanjian. Mereka menyenangkan hatimu dengan mulutnya, sedang hatinya me

9:9

# اِشْتَرَوْا بِاٰيٰتِ اللّٰهِ ثَمَنًا قَلِيْلًا فَصَدُّوْا عَنْ سَبِيْلِهٖۗ اِنَّهُمْ سَاۤءَ مَاكَانُوْا يَعْمَلُوْنَ

isytaraw bi-*aa*y*aa*ti **al**l*aa*hi tsamanan qaliilan fa*sh*adduu 'an sabiilihi innahum s*aa*-a m*aa* k*aa*nuu ya'maluun**a**

Mereka memperjualbelikan ayat-ayat Allah dengan harga murah, lalu mereka menghalang-halangi (orang) dari jalan Allah. Sungguh, betapa buruknya apa yang mereka kerjakan.

9:10

# لَا يَرْقُبُوْنَ فِيْ مُؤْمِنٍ اِلًّا وَّلَا ذِمَّةً ۗوَاُولٰۤىِٕكَ هُمُ الْمُعْتَدُوْنَ

l*aa* yarqubuuna fii mu/minin illan wal*aa* *dz*immatan waul*aa*-ika humu **a**lmu'taduun**a**

Mereka tidak memelihara (hubungan) kekerabatan dengan orang mukmin dan tidak (pula mengindahkan) perjanjian. Dan mereka itulah orang-orang yang melampaui batas.

9:11

# فَاِنْ تَابُوْا وَاَقَامُوا الصَّلٰوةَ وَاٰتَوُا الزَّكٰوةَ فَاِخْوَانُكُمْ فِى الدِّيْنِ ۗوَنُفَصِّلُ الْاٰيٰتِ لِقَوْمٍ يَّعْلَمُوْنَ

fa-in t*aa*buu wa-aq*aa*muu **al***shsh*al*aa*ta wa*aa*tawuu **al**zzak*aa*ta fa-ikhw*aa*nukum fii **al**ddiini wanufa*shsh*ilu **a**l-*aa*y*aa*ti liqawm

Dan jika mereka bertobat, melaksanakan salat dan menunaikan zakat, maka (berarti mereka itu) adalah saudara-saudaramu seagama. Kami menjelaskan ayat-ayat itu bagi orang-orang yang mengetahui.

9:12

# وَاِنْ نَّكَثُوْٓا اَيْمَانَهُمْ مِّنْۢ بَعْدِ عَهْدِهِمْ وَطَعَنُوْا فِيْ دِيْنِكُمْ فَقَاتِلُوْٓا اَىِٕمَّةَ الْكُفْرِۙ اِنَّهُمْ لَآ اَيْمَانَ لَهُمْ لَعَلَّهُمْ يَنْتَهُوْنَ

wa-in nakatsuu aym*aa*nahum min ba'di 'ahdihim wa*th*a'anuu fii diinikum faq*aa*tiluu a-immata **a**lkufri innahum l*aa* aym*aa*na lahum la'allahum yantahuun**a**

Dan jika mereka melanggar sumpah setelah ada perjanjian, dan mencerca agamamu, maka perangilah pemimpin-pemimpin kafir itu. Sesungguhnya mereka adalah orang-orang yang tidak dapat dipegang janjinya, mudah-mudahan mereka berhenti.

9:13

# اَلَا تُقَاتِلُوْنَ قَوْمًا نَّكَثُوْٓا اَيْمَانَهُمْ وَهَمُّوْا بِاِخْرَاجِ الرَّسُوْلِ وَهُمْ بَدَءُوْكُمْ اَوَّلَ مَرَّةٍۗ اَتَخْشَوْنَهُمْ ۚفَاللّٰهُ اَحَقُّ اَنْ تَخْشَوْهُ اِنْ كُنْتُمْ مُّؤْمِنِيْنَ

al*aa* tuq*aa*tiluuna qawman nakatsuu aym*aa*nahum wahammuu bi-ikhr*aa*ji **al**rrasuuli wahum badauukum awwala marratin atakhsyawnahum fa**al**l*aa*hu a*h*aqqu an takhsyawhu in kuntum mu/miniin

Mengapa kamu tidak memerangi orang-orang yang melanggar sumpah (janjinya), dan telah merencanakan untuk mengusir Rasul, dan mereka yang pertama kali memerangi kamu? Apakah kamu takut kepada mereka, padahal Allah-lah yang lebih berhak untuk kamu takuti, ji

9:14

# قَاتِلُوْهُمْ يُعَذِّبْهُمُ اللّٰهُ بِاَيْدِيْكُمْ وَيُخْزِهِمْ وَيَنْصُرْكُمْ عَلَيْهِمْ وَيَشْفِ صُدُوْرَ قَوْمٍ مُّؤْمِنِيْنَۙ

q*aa*tiluuhum yu'a*dzdz*ibhumu **al**l*aa*hu bi-aydiikum wayukhzihim wayan*sh*urkum 'alayhim wayasyfi *sh*uduura qawmin mu/miniin**a**

Perangilah mereka, niscaya Allah akan menyiksa mereka dengan (perantaraan) tanganmu dan Dia akan menghina mereka dan menolongmu (dengan kemenangan) atas mereka, serta melegakan hati orang-orang yang beriman,

9:15

# وَيُذْهِبْ غَيْظَ قُلُوْبِهِمْۗ وَيَتُوْبُ اللّٰهُ عَلٰى مَنْ يَّشَاۤءُۗ وَاللّٰهُ عَلِيْمٌ حَكِيْمٌ

wayu*dz*hib ghay*zh*a quluubihim wayatuubu **al**l*aa*hu 'al*aa* man yasy*aa*u wa**al**l*aa*hu 'aliimun *h*akiim**un**

dan Dia menghilangkan kemarahan hati mereka (orang mukmin). Dan Allah menerima tobat orang yang Dia kehendaki. Allah Maha Mengetahui, Mahabijaksana.

9:16

# اَمْ حَسِبْتُمْ اَنْ تُتْرَكُوْا وَلَمَّا يَعْلَمِ اللّٰهُ الَّذِيْنَ جَاهَدُوْا مِنْكُمْ وَلَمْ يَتَّخِذُوْا مِنْ دُوْنِ اللّٰهِ وَلَا رَسُوْلِهٖ وَلَا الْمُؤْمِنِيْنَ وَلِيْجَةً ۗوَاللّٰهُ خَبِيْرٌۢ بِمَا تَعْمَلُوْنَ ࣖ

am *h*asibtum an tutrakuu walamm*aa* ya'lami **al**l*aa*hu **al**la*dz*iina j*aa*haduu minkum walam yattakhi*dz*uu min duuni **al**l*aa*hi wal*aa* rasuulihi wal*aa*

Apakah kamu mengira bahwa kamu akan dibiarkan (begitu saja), padahal Allah belum mengetahui orang-orang yang berjihad di antara kamu dan tidak mengambil teman yang setia selain Allah, Rasul-Nya dan orang-orang yang beriman. Allah Mahateliti terhadap apa y

9:17

# مَا كَانَ لِلْمُشْرِكِيْنَ اَنْ يَّعْمُرُوْا مَسٰجِدَ اللّٰهِ شٰهِدِيْنَ عَلٰٓى اَنْفُسِهِمْ بِالْكُفْرِۗ اُولٰۤىِٕكَ حَبِطَتْ اَعْمَالُهُمْۚ وَ فِى النَّارِ هُمْ خٰلِدُوْنَ

m*aa* k*aa*na lilmusyrikiina an ya'muruu mas*aa*jida **al**l*aa*hi sy*aa*hidiina 'al*aa* anfusihim bi**a**lkufri ul*aa*-ika *h*abi*th*at a'm*aa*luhum wafii **al**nn

Tidaklah pantas orang-orang musyrik memakmurkan masjid Allah, padahal mereka mengakui bahwa mereka sendiri kafir. Mereka itu sia-sia amalnya, dan mereka kekal di dalam neraka.

9:18

# اِنَّمَا يَعْمُرُ مَسٰجِدَ اللّٰهِ مَنْ اٰمَنَ بِاللّٰهِ وَالْيَوْمِ الْاٰخِرِ وَاَقَامَ الصَّلٰوةَ وَاٰتَى الزَّكٰوةَ وَلَمْ يَخْشَ اِلَّا اللّٰهَ ۗفَعَسٰٓى اُولٰۤىِٕكَ اَنْ يَّكُوْنُوْا مِنَ الْمُهْتَدِيْنَ

innam*aa* ya'muru mas*aa*jida **al**l*aa*hi man *aa*mana bi**al**l*aa*hi wa**a**lyawmi **a**l-*aa*khiri wa-aq*aa*ma **al***shsh*al*aa*ta wa*aa<*

Sesungguhnya yang memakmurkan masjid Allah hanyalah orang-orang yang beriman kepada Allah dan hari kemudian, serta (tetap) melaksanakan salat, menunaikan zakat dan tidak takut (kepada apa pun) kecuali kepada Allah. Maka mudah-mudahan mereka termasuk orang







9:19

# ۞ اَجَعَلْتُمْ سِقَايَةَ الْحَاۤجِّ وَعِمَارَةَ الْمَسْجِدِ الْحَرَامِ كَمَنْ اٰمَنَ بِاللّٰهِ وَالْيَوْمِ الْاٰخِرِ وَجَاهَدَ فِيْ سَبِيْلِ اللّٰهِ ۗ لَا يَسْتَوٗنَ عِنْدَ اللّٰهِ ۗوَاللّٰهُ لَا يَهْدِى الْقَوْمَ الظّٰلِمِيْنَۘ

aja'altum siq*aa*yata **a**l*haa*jji wa'im*aa*rata **a**lmasjidi **a**l*h*ar*aa*mi kaman *aa*mana bi**al**l*aa*hi wa**a**lyawmi **a**l-*aa*

*Apakah (orang-orang) yang memberi minuman kepada orang-orang yang mengerjakan haji dan mengurus Masjidilharam, kamu samakan dengan orang yang beriman kepada Allah dan hari kemudian serta berjihad di jalan Allah? Mereka tidak sama di sisi Allah. Allah tida*









9:20

# اَلَّذِيْنَ اٰمَنُوْا وَهَاجَرُوْا وَجَاهَدُوْا فِيْ سَبِيْلِ اللّٰهِ بِاَمْوَالِهِمْ وَاَنْفُسِهِمْۙ اَعْظَمُ دَرَجَةً عِنْدَ اللّٰهِ ۗوَاُولٰۤىِٕكَ هُمُ الْفَاۤىِٕزُوْنَ

**al**la*dz*iina *aa*manuu wah*aa*jaruu waj*aa*haduu fii sabiili **al**l*aa*hi bi-amw*aa*lihim wa-anfusihim a'*zh*amu darajatan 'inda **al**l*aa*hi waul*aa*-ika humu

Orang-orang yang beriman dan berhijrah serta berjihad di jalan Allah, dengan harta dan jiwa mereka, adalah lebih tinggi derajatnya di sisi Allah. Mereka itulah orang-orang yang memperoleh kemenangan.

9:21

# يُبَشِّرُهُمْ رَبُّهُمْ بِرَحْمَةٍ مِّنْهُ وَرِضْوَانٍ وَّجَنّٰتٍ لَّهُمْ فِيْهَا نَعِيْمٌ مُّقِيْمٌۙ

yubasysyiruhum rabbuhum bira*h*matin minhu wari*dh*w*aa*nin wajann*aa*tin lahum fiih*aa* na'iimun muqiim**un**

Tuhan menggembirakan mereka dengan memberikan rahmat, keridaan dan surga, mereka memperoleh kesenangan yang kekal di dalamnya,

9:22

# خٰلِدِيْنَ فِيْهَآ اَبَدًا ۗاِنَّ اللّٰهَ عِنْدَهٗٓ اَجْرٌ عَظِيْمٌ

kh*aa*lidiina fiih*aa* abadan inna **al**l*aa*ha 'indahu ajrun 'a*zh*iim**un**

mereka kekal di dalamnya selama-lamanya. Sungguh, di sisi Allah terdapat pahala yang besar.

9:23

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْا لَا تَتَّخِذُوْٓا اٰبَاۤءَكُمْ وَاِخْوَانَكُمْ اَوْلِيَاۤءَ اِنِ اسْتَحَبُّوا الْكُفْرَ عَلَى الْاِيْمَانِۗ وَمَنْ يَّتَوَلَّهُمْ مِّنْكُمْ فَاُولٰۤىِٕكَ هُمُ الظّٰلِمُوْنَ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu l*aa* tattakhi*dz*uu *aa*b*aa*-akum wa-ikhw*aa*nakum awliy*aa*-a ini ista*h*abbuu **a**lkufra 'al*aa* **a**l-iim<

Wahai orang-orang yang beriman! Janganlah kamu jadikan bapak-bapakmu dan saudara-saudaramu sebagai pelindung, jika mereka lebih menyukai kekafiran daripada keimanan. Barangsiapa di antara kamu yang menjadikan mereka pelindung, maka mereka itulah orang-ora

9:24

# قُلْ اِنْ كَانَ اٰبَاۤؤُكُمْ وَاَبْنَاۤؤُكُمْ وَاِخْوَانُكُمْ وَاَزْوَاجُكُمْ وَعَشِيْرَتُكُمْ وَاَمْوَالُ ِۨاقْتَرَفْتُمُوْهَا وَتِجَارَةٌ تَخْشَوْنَ كَسَادَهَا وَمَسٰكِنُ تَرْضَوْنَهَآ اَحَبَّ اِلَيْكُمْ مِّنَ اللّٰهِ وَرَسُوْلِهٖ وَجِهَادٍ فِيْ سَبِيْ

qul in k*aa*na *aa*b*aa*ukum wa-abn*aa*ukum wa-ikhw*aa*nukum wa-azw*aa*jukum wa'asyiiratukum wa-amw*aa*lun iqtaraftumuuh*aa* watij*aa*ratun takhsyawna kas*aa*dah*aa* wamas*aa*kinu tar*dh*awn

Katakanlah, “Jika bapak-bapakmu, anak-anakmu, saudara-saudaramu, istri-istrimu, keluargamu, harta kekayaan yang kamu usahakan, perdagangan yang kamu khawatirkan kerugiannya, dan rumah-rumah tempat tinggal yang kamu sukai, lebih kamu cintai dari pada Allah

9:25

# لَقَدْ نَصَرَكُمُ اللّٰهُ فِيْ مَوَاطِنَ كَثِيْرَةٍۙ وَّيَوْمَ حُنَيْنٍۙ اِذْ اَعْجَبَتْكُمْ كَثْرَتُكُمْ فَلَمْ تُغْنِ عَنْكُمْ شَيْـًٔا وَّضَاقَتْ عَلَيْكُمُ الْاَرْضُ بِمَا رَحُبَتْ ثُمَّ وَلَّيْتُمْ مُّدْبِرِيْنَۚ

laqad na*sh*arakumu **al**l*aa*hu fii maw*aath*ina katsiiratin wayawma *h*unaynin i*dz* a'jabatkum katsratukum falam tughni 'ankum syay-an wa*daa*qat 'alaykumu **a**l-ar*dh*u bim*aa* ra*h*

Sungguh, Allah telah menolong kamu (mukminin) di banyak medan perang, dan (ingatlah) Perang Hunain, ketika jumlahmu yang besar itu membanggakan kamu, tetapi (jumlah yang banyak itu) sama sekali tidak berguna bagimu, dan bumi yang luas itu terasa sempit ba







9:26

# ثُمَّ اَنْزَلَ اللّٰهُ سَكِيْنَتَهٗ عَلٰى رَسُوْلِهٖ وَعَلَى الْمُؤْمِنِيْنَ وَاَنْزَلَ جُنُوْدًا لَّمْ تَرَوْهَا وَعَذَّبَ الَّذِيْنَ كَفَرُوْاۗ وَذٰلِكَ جَزَاۤءُ الْكٰفِرِيْنَ

tsumma anzala **al**l*aa*hu sakiinatahu 'al*aa* rasuulihi wa'al*aa* **a**lmu/miniina wa-anzala junuudan lam tarawh*aa* wa'a*dzdz*aba **al**la*dz*iina kafaruu wa*dzaa*lika jaz*aa*

*Kemudian Allah menurunkan ketenangan kepada Rasul-Nya dan kepada orang-orang yang beriman, dan Dia menurunkan bala tentara (para malaikat) yang tidak terlihat olehmu, dan Dia menimpakan azab kepada orang-orang kafir. Itulah balasan bagi orang-orang kafir.*









9:27

# ثُمَّ يَتُوْبُ اللّٰهُ مِنْۢ بَعْدِ ذٰلِكَ عَلٰى مَنْ يَّشَاۤءُۗ وَاللّٰهُ غَفُوْرٌ رَّحِيْمٌ

tsumma yatuubu **al**l*aa*hu min ba'di *dzaa*lika 'al*aa* man yasy*aa*u wa**al**l*aa*hu ghafuurun ra*h*iim**un**

Setelah itu Allah menerima tobat orang yang Dia kehendaki. Allah Maha Pengampun, Maha Penyayang.

9:28

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْٓا اِنَّمَا الْمُشْرِكُوْنَ نَجَسٌ فَلَا يَقْرَبُوا الْمَسْجِدَ الْحَرَامَ بَعْدَ عَامِهِمْ هٰذَا ۚوَاِنْ خِفْتُمْ عَيْلَةً فَسَوْفَ يُغْنِيْكُمُ اللّٰهُ مِنْ فَضْلِهٖٓ اِنْ شَاۤءَۗ اِنَّ اللّٰهَ عَلِيْمٌ حَكِيْمٌ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu innam*aa* **a**lmusyrikuuna najasun fal*aa* yaqrabuu **a**lmasjida **a**l*h*ar*aa*ma ba'da '*aa*mihim h*aadzaa*

*Wahai orang-orang yang beriman! Sesungguhnya orang-orang musyrik itu najis (kotor jiwa), karena itu janganlah mereka mendekati Masjidilharam setelah tahun ini. Dan jika kamu khawatir menjadi miskin (karena orang kafir tidak datang), maka Allah nanti akan*









9:29

# قَاتِلُوا الَّذِيْنَ لَا يُؤْمِنُوْنَ بِاللّٰهِ وَلَا بِالْيَوْمِ الْاٰخِرِ وَلَا يُحَرِّمُوْنَ مَا حَرَّمَ اللّٰهُ وَرَسُوْلُهٗ وَلَا يَدِيْنُوْنَ دِيْنَ الْحَقِّ مِنَ الَّذِيْنَ اُوْتُوا الْكِتٰبَ حَتّٰى يُعْطُوا الْجِزْيَةَ عَنْ يَّدٍ وَّهُمْ صَاغِرُوْ

q*aa*tiluu **al**la*dz*iina l*aa* yu/minuuna bi**al**l*aa*hi wal*aa* bi**a**lyawmi **a**l-*aa*khiri wal*aa* yu*h*arrimuuna m*aa* *h*arrama **al**

**Perangilah orang-orang yang tidak beriman kepada Allah dan hari kemudian, mereka yang tidak mengharamkan apa yang telah diharamkan Allah dan Rasul-Nya dan mereka yang tidak beragama dengan agama yang benar (agama Allah), (yaitu orang-orang) yang telah dib**









9:30

# وَقَالَتِ الْيَهُوْدُ عُزَيْرُ ِۨابْنُ اللّٰهِ وَقَالَتِ النَّصٰرَى الْمَسِيْحُ ابْنُ اللّٰهِ ۗذٰلِكَ قَوْلُهُمْ بِاَفْوَاهِهِمْۚ يُضَاهِـُٔوْنَ قَوْلَ الَّذِيْنَ كَفَرُوْا مِنْ قَبْلُ ۗقَاتَلَهُمُ اللّٰهُ ۚ اَنّٰى يُؤْفَكُوْنَ

waq*aa*lati **a**lyahuudu 'uzayrun ibnu **al**l*aa*hi waq*aa*lati **al**nna*shaa*r*aa* **a**lmasii*h*u ibnu **al**l*aa*hi *dzaa*lika qawluhum bi-afw

Dan orang-orang Yahudi berkata, “Uzair putra Allah,” dan orang-orang Nasrani berkata, “Al-Masih putra Allah.” Itulah ucapan yang keluar dari mulut mereka. Mereka meniru ucapan orang-orang kafir yang terdahulu. Allah melaknat mereka; bagaimana mereka samp

9:31

# اِتَّخَذُوْٓا اَحْبَارَهُمْ وَرُهْبَانَهُمْ اَرْبَابًا مِّنْ دُوْنِ اللّٰهِ وَالْمَسِيْحَ ابْنَ مَرْيَمَۚ وَمَآ اُمِرُوْٓا اِلَّا لِيَعْبُدُوْٓا اِلٰهًا وَّاحِدًاۚ لَآ اِلٰهَ اِلَّا هُوَۗ سُبْحٰنَهٗ عَمَّا يُشْرِكُوْنَ

ittakha*dz*uu a*h*b*aa*rahum waruhb*aa*nahum arb*aa*ban min duuni **al**l*aa*hi wa**a**lmasii*h*a ibna maryama wam*aa* umiruu ill*aa* liya'buduu il*aa*han w*aah*idan l*aa*

*Mereka menjadikan orang-orang alim (Yahudi), dan rahib-rahibnya (Nasrani) sebagai tuhan selain Allah, dan (juga) Al-Masih putra Maryam; padahal mereka hanya disuruh menyembah Tuhan Yang Maha Esa; tidak ada tuhan selain Dia. Mahasuci Dia dari apa yang mere*









9:32

# يُرِيْدُوْنَ اَنْ يُّطْفِـُٔوْا نُوْرَ اللّٰهِ بِاَفْوَاهِهِمْ وَيَأْبَى اللّٰهُ اِلَّآ اَنْ يُّتِمَّ نُوْرَهٗ وَلَوْ كَرِهَ الْكٰفِرُوْنَ

yuriiduuna an yu*th*fi-uu nuura **al**l*aa*hi bi-afw*aa*hihim waya/b*aa* **al**l*aa*hu ill*aa* an yutimma nuurahu walaw kariha **a**lk*aa*firuun**a**

Mereka hendak memadamkan cahaya (agama) Allah dengan mulut (ucapan-ucapan) mereka, tetapi Allah menolaknya, malah berkehendak menyempurnakan cahaya-Nya, walaupun orang-orang kafir itu tidak menyukai.

9:33

# هُوَ الَّذِيْٓ اَرْسَلَ رَسُوْلَهٗ بِالْهُدٰى وَدِيْنِ الْحَقِّ لِيُظْهِرَهٗ عَلَى الدِّيْنِ كُلِّهٖۙ وَلَوْ كَرِهَ الْمُشْرِكُوْنَ

huwa **al**la*dz*ii arsala rasuulahu bi**a**lhud*aa* wadiini **a**l*h*aqqi liyu*zh*hirahu 'al*aa* **al**ddiini kullihi walaw kariha **a**lmusyrikuun**a**

**Dialah yang telah mengutus Rasul-Nya dengan petunjuk (Al-Qur'an) dan agama yang benar untuk diunggulkan atas segala agama, walaupun orang-orang musyrik tidak menyukai.**









9:34

# ۞ يٰٓاَيُّهَا الَّذِينَ اٰمَنُوْٓا اِنَّ كَثِيْرًا مِّنَ الْاَحْبَارِ وَالرُّهْبَانِ لَيَأْكُلُوْنَ اَمْوَالَ النَّاسِ بِالْبَاطِلِ وَيَصُدُّوْنَ عَنْ سَبِيْلِ اللّٰهِ ۗوَالَّذِيْنَ يَكْنِزُوْنَ الذَّهَبَ وَالْفِضَّةَ وَلَا يُنْفِقُوْنَهَا فِيْ سَبِيْلِ ا

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu inna katsiiran mina **a**l-a*h*b*aa*ri wa**al**rruhb*aa*ni laya/kuluuna amw*aa*la **al**nn*aa*si bi**a**

**Wahai orang-orang yang beriman! Sesungguhnya banyak dari orang-orang alim dan rahib-rahib mereka benar-benar memakan harta orang dengan jalan yang batil, dan (mereka) menghalang-halangi (manusia) dari jalan Allah. Dan orang-orang yang menyimpan emas dan p**









9:35

# يَّوْمَ يُحْمٰى عَلَيْهَا فِيْ نَارِ جَهَنَّمَ فَتُكْوٰى بِهَا جِبَاهُهُمْ وَجُنُوْبُهُمْ وَظُهُوْرُهُمْۗ هٰذَا مَا كَنَزْتُمْ لِاَنْفُسِكُمْ فَذُوْقُوْا مَا كُنْتُمْ تَكْنِزُوْنَ

yawma yu*h*m*aa* 'alayh*aa* fii n*aa*ri jahannama fatukw*aa* bih*aa* jib*aa*huhum wajunuubuhum wa*zh*uhuuruhum h*aadzaa* m*aa* kanaztum li-anfusikum fa*dz*uuquu m*aa* kuntum taknizuun**a**

**(Ingatlah) pada hari ketika emas dan perak dipanaskan dalam neraka Jahanam, lalu dengan itu disetrika dahi, lambung dan punggung mereka (seraya dikatakan) kepada mereka, “Inilah harta bendamu yang kamu simpan untuk dirimu sendiri, maka rasakanlah (akibat**









9:36

# اِنَّ عِدَّةَ الشُّهُوْرِ عِنْدَ اللّٰهِ اثْنَا عَشَرَ شَهْرًا فِيْ كِتٰبِ اللّٰهِ يَوْمَ خَلَقَ السَّمٰوٰتِ وَالْاَرْضَ مِنْهَآ اَرْبَعَةٌ حُرُمٌ ۗذٰلِكَ الدِّيْنُ الْقَيِّمُ ەۙ فَلَا تَظْلِمُوْا فِيْهِنَّ اَنْفُسَكُمْ وَقَاتِلُوا الْمُشْرِكِيْنَ كَاۤفّ

inna 'iddata **al**sysyuhuuri 'inda **al**l*aa*hi itsn*aa* 'asyara syahran fii kit*aa*bi **al**l*aa*hi yawma khalaqa **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*a

Sesungguhnya jumlah bulan menurut Allah ialah dua belas bulan, (sebagaimana) dalam ketetapan Allah pada waktu Dia menciptakan langit dan bumi, di antaranya ada empat bulan haram. Itulah (ketetapan) agama yang lurus, maka janganlah kamu menzalimi dirimu da

9:37

# اِنَّمَا النَّسِيْۤءُ زِيَادَةٌ فِى الْكُفْرِ يُضَلُّ بِهِ الَّذِيْنَ كَفَرُوْا يُحِلُّوْنَهٗ عَامًا وَّيُحَرِّمُوْنَهٗ عَامًا لِّيُوَاطِـُٔوْا عِدَّةَ مَا حَرَّمَ اللّٰهُ فَيُحِلُّوْا مَا حَرَّمَ اللّٰهُ ۗزُيِّنَ لَهُمْ سُوْۤءُ اَعْمَالِهِمْۗ وَاللّٰهُ ل

innam*aa* **al**nnasii-u ziy*aa*datun fii **a**lkufri yu*dh*allu bihi **al**la*dz*iina kafaruu yu*h*illuunahu '*aa*man wayu*h*arrimuunahu '*aa*man liyuw*aath*i-uu 'iddata

Sesungguhnya pengunduran (bulan haram) itu hanya menambah kekafiran. Orang-orang kafir disesatkan dengan (pengunduran) itu, mereka menghalalkannya suatu tahun dan mengharamkannya pada suatu tahun yang lain, agar mereka dapat menyesuaikan dengan bilangan y

9:38

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْا مَا لَكُمْ اِذَا قِيْلَ لَكُمُ انْفِرُوْا فِيْ سَبِيْلِ اللّٰهِ اثَّاقَلْتُمْ اِلَى الْاَرْضِۗ اَرَضِيْتُمْ بِالْحَيٰوةِ الدُّنْيَا مِنَ الْاٰخِرَةِۚ فَمَا مَتَاعُ الْحَيٰوةِ الدُّنْيَا فِى الْاٰخِرَةِ اِلَّا قَلِيْلٌ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu m*aa* lakum i*dzaa* qiila lakumu infiruu fii sabiili **al**l*aa*hi itsts*aa*qaltum il*aa* **a**l-ar*dh*i ara*dh*iitum

Wahai orang-orang yang beriman! Mengapa apabila dikatakan kepada kamu, “Berangkatlah (untuk berperang) di jalan Allah,” kamu merasa berat dan ingin tinggal di tempatmu? Apakah kamu lebih menyenangi kehidupan di dunia daripada kehidupan di akhirat? Padahal

9:39

# اِلَّا تَنْفِرُوْا يُعَذِّبْكُمْ عَذَابًا اَلِيمًاۙ وَّيَسْتَبْدِلْ قَوْمًا غَيْرَكُمْ وَلَا تَضُرُّوْهُ شَيْـًٔاۗ وَاللّٰهُ عَلٰى كُلِّ شَيْءٍ قَدِيْرٌ

ill*aa* tanfiruu yu'a*dzdz*ibkum 'a*dzaa*ban **a**liiman wayastabdil qawman ghayrakum wal*aa* ta*dh*urruuhu syay-an wa**al**l*aa*hu 'al*aa* kulli syay-in qadiir**un**

Jika kamu tidak berangkat (untuk berperang), niscaya Allah akan menghukum kamu dengan azab yang pedih dan menggantikan kamu dengan kaum yang lain, dan kamu tidak akan merugikan-Nya sedikit pun. Dan Allah Mahakuasa atas segala sesuatu.

9:40

# اِلَّا تَنْصُرُوْهُ فَقَدْ نَصَرَهُ اللّٰهُ اِذْ اَخْرَجَهُ الَّذِيْنَ كَفَرُوْا ثَانِيَ اثْنَيْنِ اِذْ هُمَا فِى الْغَارِ اِذْ يَقُوْلُ لِصَاحِبِهٖ لَا تَحْزَنْ اِنَّ اللّٰهَ مَعَنَاۚ فَاَنْزَلَ اللّٰهُ سَكِيْنَتَهٗ عَلَيْهِ وَاَيَّدَهٗ بِجُنُوْدٍ لَّمْ

ill*aa* tan*sh*uruuhu faqad na*sh*arahu **al**l*aa*hu i*dz* akhrajahu **al**la*dz*iina kafaruu ts*aa*niya itsnayni i*dz* hum*aa* fii **a**lgh*aa*ri i*dz* yaquulu

Jika kamu tidak menolongnya (Muhammad), sesungguhnya Allah telah menolongnya (yaitu) ketika orang-orang kafir mengusirnya (dari Mekah); sedang dia salah seorang dari dua orang ketika keduanya berada dalam gua, ketika itu dia berkata kepada sahabatnya, “Ja

9:41

# اِنْفِرُوْا خِفَافًا وَّثِقَالًا وَّجَاهِدُوْا بِاَمْوَالِكُمْ وَاَنْفُسِكُمْ فِيْ سَبِيْلِ اللّٰهِ ۗذٰلِكُمْ خَيْرٌ لَّكُمْ اِنْ كُنْتُمْ تَعْلَمُوْنَ

infiruu khif*aa*fan watsiq*aa*lan waj*aa*hiduu bi-amw*aa*likum wa-anfusikum fii sabiili **al**l*aa*hi *dzaa*likum khayrun lakum in kuntum ta'lamuun**a**

Berangkatlah kamu baik dengan rasa ringan maupun dengan rasa berat, dan berjihadlah dengan harta dan jiwamu di jalan Allah. Yang demikian itu adalah lebih baik bagimu jika kamu mengetahui.

9:42

# لَوْ كَانَ عَرَضًا قَرِيْبًا وَّسَفَرًا قَاصِدًا لَّاتَّبَعُوْكَ وَلٰكِنْۢ بَعُدَتْ عَلَيْهِمُ الشُّقَّةُۗ وَسَيَحْلِفُوْنَ بِاللّٰهِ لَوِ اسْتَطَعْنَا لَخَرَجْنَا مَعَكُمْۚ يُهْلِكُوْنَ اَنْفُسَهُمْۚ وَاللّٰهُ يَعْلَمُ اِنَّهُمْ لَكٰذِبُوْنَ ࣖ

law k*aa*na 'ara*dh*an qariiban wasafaran q*aas*idan la**i**ttaba'uuka wal*aa*kin ba'udat 'alayhimu **al**sysyuqqatu wasaya*h*lifuuna bi**al**l*aa*hi lawi ista*th*a'n*aa* lakha

Sekiranya (yang kamu serukan kepada mereka) ada keuntungan yang mudah diperoleh dan perjalanan yang tidak seberapa jauh, niscaya mereka mengikutimu, tetapi tempat yang dituju itu terasa sangat jauh bagi mereka. Mereka akan bersumpah dengan (nama) Allah, “

9:43

# عَفَا اللّٰهُ عَنْكَۚ لِمَ اَذِنْتَ لَهُمْ حَتّٰى يَتَبَيَّنَ لَكَ الَّذِيْنَ صَدَقُوْا وَتَعْلَمَ الْكٰذِبِيْنَ

'af*aa* **al**l*aa*hu 'anka lima a*dz*inta lahum *h*att*aa* yatabayyana laka **al**la*dz*iina *sh*adaquu wata'lama **a**lk*aadz*ibiin**a**

Allah memaafkanmu (Muhammad). Mengapa engkau memberi izin kepada mereka (untuk tidak pergi berperang), sebelum jelas bagimu orang-orang yang benar-benar (berhalangan) dan sebelum engkau mengetahui orang-orang yang berdusta?

9:44

# لَا يَسْتَأْذِنُكَ الَّذِيْنَ يُؤْمِنُوْنَ بِاللّٰهِ وَالْيَوْمِ الْاٰخِرِ اَنْ يُّجَاهِدُوْا بِاَمْوَالِهِمْ وَاَنْفُسِهِمْۗ وَاللّٰهُ عَلِيْمٌۢ بِالْمُتَّقِيْنَ

l*aa* yasta/*dz*inuka **al**la*dz*iina yu/minuuna bi**al**l*aa*hi wa**a**lyawmi **a**l-*aa*khiri an yuj*aa*hiduu bi-amw*aa*lihim wa-anfusihim wa**al**l*a*

Orang-orang yang beriman kepada Allah dan hari kemudian, tidak akan meminta izin (tidak ikut) kepadamu untuk berjihad dengan harta dan jiwa mereka. Allah mengetahui orang-orang yang bertakwa.







9:45

# اِنَّمَا يَسْتَأْذِنُكَ الَّذِيْنَ لَا يُؤْمِنُوْنَ بِاللّٰهِ وَالْيَوْمِ الْاٰخِرِ وَارْتَابَتْ قُلُوْبُهُمْ فَهُمْ فِيْ رَيْبِهِمْ يَتَرَدَّدُوْنَ

innam*aa* yasta/*dz*inuka **al**la*dz*iina l*aa* yu/minuuna bi**al**l*aa*hi wa**a**lyawmi **a**l-*aa*khiri wa**i**rt*aa*bat quluubuhum fahum fii raybihim y

Sesungguhnya yang akan meminta izin kepadamu (Muhammad), hanyalah orang-orang yang tidak beriman kepada Allah dan hari kemudian, dan hati mereka ragu, karena itu mereka selalu bimbang dalam keraguan.

9:46

# ۞ وَلَوْ اَرَادُوا الْخُرُوْجَ لَاَعَدُّوْا لَهٗ عُدَّةً وَّلٰكِنْ كَرِهَ اللّٰهُ انْۢبِعَاثَهُمْ فَثَبَّطَهُمْ وَقِيْلَ اقْعُدُوْا مَعَ الْقٰعِدِيْنَ

walaw ar*aa*duu **a**lkhuruuja la-a'adduu lahu 'uddatan wal*aa*kin kariha **al**l*aa*hu inbi'*aa*tsahum fatsabba*th*ahum waqiila uq'uduu ma'a **a**lq*aa*'idiin**a**

Dan jika mereka mau berangkat, niscaya mereka menyiapkan persiapan untuk keberangkatan itu, tetapi Allah tidak menyukai keberangkatan mereka, maka Dia melemahkan keinginan mereka, dan dikatakan (kepada mereka), “Tinggallah kamu bersama orang-orang yang ti

9:47

# لَوْ خَرَجُوْا فِيْكُمْ مَّا زَادُوْكُمْ اِلَّا خَبَالًا وَّلَاَوْضَعُوْا خِلٰلَكُمْ يَبْغُوْنَكُمُ الْفِتْنَةَۚ وَفِيْكُمْ سَمّٰعُوْنَ لَهُمْۗ وَاللّٰهُ عَلِيْمٌۢ بِالظّٰلِمِيْنَ

law kharajuu fiikum m*aa* z*aa*duukum ill*aa* khab*aa*lan wala-aw*dh*a'uu khil*aa*lakum yabghuunakumu **a**lfitnata wafiikum samm*aa*'uuna lahum wa**al**l*aa*hu 'aliimun bi**al**

**Jika (mereka berangkat bersamamu), niscaya mereka tidak akan menambah (kekuatan)mu, malah hanya akan membuat kekacauan, dan mereka tentu bergegas maju ke depan di celah-celah barisanmu untuk mengadakan kekacauan (di barisanmu); sedang di antara kamu ada o**









9:48

# لَقَدِ ابْتَغَوُا الْفِتْنَةَ مِنْ قَبْلُ وَقَلَّبُوْا لَكَ الْاُمُوْرَ حَتّٰى جَاۤءَ الْحَقُّ وَظَهَرَ اَمْرُ اللّٰهِ وَهُمْ كٰرِهُوْنَ

laqadi ibtaghawuu **a**lfitnata min qablu waqallabuu laka **a**l-umuura *h*att*aa* j*aa*-a **a**l*h*aqqu wa*zh*ahara amru **al**l*aa*hi wahum k*aa*rihuun**a**

**Sungguh, sebelum itu mereka memang sudah berusaha membuat kekacauan dan mengatur berbagai macam tipu daya bagimu (memutarbalikkan persoalan), hingga datanglah kebenaran (pertolongan Allah), dan menanglah urusan (agama) Allah, padahal mereka tidak menyukai**









9:49

# وَمِنْهُمْ مَّنْ يَّقُوْلُ ائْذَنْ لِّيْ وَلَا تَفْتِنِّيْۗ اَلَا فِى الْفِتْنَةِ سَقَطُوْاۗ وَاِنَّ جَهَنَّمَ لَمُحِيْطَةٌ ۢ بِالْكٰفِرِيْنَ

waminhum man yaquulu i/*dz*an lii wal*aa* taftinnii **a**l*aa* fii **a**lfitnati saqa*th*uu wa-inna jahannama lamu*h*ii*th*atun bi**a**lk*aa*firiin**a**

Dan di antara mereka ada orang yang berkata, “Berilah aku izin (tidak pergi berperang) dan janganlah engkau (Muhammad) menjadikan aku terjerumus ke dalam fitnah.” Ketahuilah, bahwa mereka telah terjerumus ke dalam fitnah. Dan sungguh, Jahanam meliputi ora

9:50

# اِنْ تُصِبْكَ حَسَنَةٌ تَسُؤْهُمْۚ وَاِنْ تُصِبْكَ مُصِيْبَةٌ يَّقُوْلُوْا قَدْ اَخَذْنَآ اَمْرَنَا مِنْ قَبْلُ وَيَتَوَلَّوْا وَّهُمْ فَرِحُوْنَ

in tu*sh*ibka *h*asanatun tasu/hum wa-in tu*sh*ibka mu*sh*iibatun yaquuluu qad akha*dz*n*aa* amran*aa* min qablu wayatawallaw wahum fari*h*uun**a**

Jika engkau (Muhammad) mendapat kebaikan, mereka tidak senang; tetapi jika engkau ditimpa bencana, mereka berkata, “Sungguh, sejak semula kami telah berhati-hati (tidak pergi berperang),” dan mereka berpaling dengan (perasaan) gembira.

9:51

# قُلْ لَّنْ يُّصِيْبَنَآ اِلَّا مَا كَتَبَ اللّٰهُ لَنَاۚ هُوَ مَوْلٰىنَا وَعَلَى اللّٰهِ فَلْيَتَوَكَّلِ الْمُؤْمِنُوْنَ

qul lan yu*sh*iiban*aa* ill*aa* m*aa* kataba **al**l*aa*hu lan*aa* huwa mawl*aa*n*aa* wa'al*aa* **al**l*aa*hi falyatawakkali **a**lmu/minuun**a**

Katakanlah (Muhammad), “Tidak akan menimpa kami melainkan apa yang telah ditetapkan Allah bagi kami. Dialah pelindung kami, dan hanya kepada Allah bertawakallah orang-orang yang beriman.”

9:52

# قُلْ هَلْ تَرَبَّصُوْنَ بِنَآ اِلَّآ اِحْدَى الْحُسْنَيَيْنِۗ وَنَحْنُ نَتَرَبَّصُ بِكُمْ اَنْ يُّصِيْبَكُمُ اللّٰهُ بِعَذَابٍ مِّنْ عِنْدِهٖٓ اَوْ بِاَيْدِيْنَاۖ فَتَرَبَّصُوْٓا اِنَّا مَعَكُمْ مُّتَرَبِّصُوْنَ

qul hal tarabba*sh*uuna bin*aa* ill*aa* i*h*d*aa* **a**l*h*usnayayni wana*h*nu natarabba*sh*u bikum an yu*sh*iibakumu **al**l*aa*hu bi'a*dzaa*bin min 'indihi aw bi-aydiin*a*

Katakanlah (Muhammad), “Tidak ada yang kamu tunggu-tunggu bagi kami, kecuali salah satu dari dua kebaikan (menang atau mati syahid). Dan kami menunggu-nunggu bagi kamu bahwa Allah akan menimpakan azab kepadamu dari sisi-Nya, atau (azab) melalui tangan kam







9:53

# قُلْ اَنْفِقُوْا طَوْعًا اَوْ كَرْهًا لَّنْ يُّتَقَبَّلَ مِنْكُمْ ۗاِنَّكُمْ كُنْتُمْ قَوْمًا فٰسِقِيْنَ

qul anfiquu *th*aw'an aw karhan lan yutaqabbala minkum innakum kuntum qawman f*aa*siqiin**a**

Katakanlah (Muhammad), “Infakkanlah hartamu baik dengan sukarela maupun dengan terpaksa, namun (infakmu) tidak akan diterima. Sesungguhnya kamu adalah orang-orang yang fasik.”

9:54

# وَمَا مَنَعَهُمْ اَنْ تُقْبَلَ مِنْهُمْ نَفَقٰتُهُمْ اِلَّآ اَنَّهُمْ كَفَرُوْا بِاللّٰهِ وَبِرَسُوْلِهٖ وَلَا يَأْتُوْنَ الصَّلٰوةَ اِلَّا وَهُمْ كُسَالٰى وَلَا يُنْفِقُوْنَ اِلَّا وَهُمْ كٰرِهُوْنَ

wam*aa* mana'ahum an tuqbala minhum nafaq*aa*tuhum ill*aa* annahum kafaruu bi**al**l*aa*hi wabirasuulihi wal*aa* ya/tuuna **al***shsh*al*aa*ta ill*aa* wahum kus*aa*l*aa* wal*aa<*

Dan yang menghalang-halangi infak mereka untuk diterima adalah karena mereka kafir (ingkar) kepada Allah dan Rasul-Nya dan mereka tidak melaksanakan salat, melainkan dengan malas dan tidak (pula) menginfakkan (harta) mereka, melainkan dengan rasa enggan (







9:55

# فَلَا تُعْجِبْكَ اَمْوَالُهُمْ وَلَآ اَوْلَادُهُمْ ۗاِنَّمَا يُرِيْدُ اللّٰهُ لِيُعَذِّبَهُمْ بِهَا فِى الْحَيٰوةِ الدُّنْيَا وَتَزْهَقَ اَنْفُسُهُمْ وَهُمْ كٰفِرُوْنَ

fal*aa* tu'jibka amw*aa*luhum wal*aa* awl*aa*duhum innam*aa* yuriidu **al**l*aa*hu liyu'a*dzdz*ibahum bih*aa* fii **a**l*h*ay*aa*ti **al**dduny*aa* watazhaqa anf

Maka janganlah harta dan anak-anak mereka membuatmu kagum. Sesungguhnya maksud Allah dengan itu adalah untuk menyiksa mereka dalam kehidupan dunia dan kelak akan mati dalam keadaan kafir.

9:56

# وَيَحْلِفُوْنَ بِاللّٰهِ اِنَّهُمْ لَمِنْكُمْۗ وَمَا هُمْ مِّنْكُمْ وَلٰكِنَّهُمْ قَوْمٌ يَّفْرَقُوْنَ

waya*h*lifuuna bi**al**l*aa*hi innahum laminkum wam*aa* hum minkum wal*aa*kinnahum qawmun yafraquun**a**

Dan mereka (orang-orang munafik) bersumpah dengan (nama) Allah, bahwa sesungguhnya mereka termasuk golonganmu; namun mereka bukanlah dari golonganmu, tetapi mereka orang-orang yang sangat takut (kepadamu).

9:57

# لَوْ يَجِدُوْنَ مَلْجَاً اَوْ مَغٰرٰتٍ اَوْ مُدَّخَلًا لَّوَلَّوْا اِلَيْهِ وَهُمْ يَجْمَحُوْنَ

law yajiduuna malja-an aw magh*aa*r*aa*tin aw muddakhalan lawallaw ilayhi wahum yajma*h*uun**a**

Sekiranya mereka memperoleh tempat perlindungan, gua-gua atau lubang-lubang (dalam tanah), niscaya mereka pergi (lari) ke sana dengan secepat-cepatnya.

9:58

# وَمِنْهُمْ مَّنْ يَّلْمِزُكَ فِى الصَّدَقٰتِۚ فَاِنْ اُعْطُوْا مِنْهَا رَضُوْا وَاِنْ لَّمْ يُعْطَوْا مِنْهَآ اِذَا هُمْ يَسْخَطُوْنَ

waminhum man yalmizuka fii **al***shsh*adaq*aa*ti fa-in u'*th*uu minh*aa* ra*dh*uu wa-in lam yu'*th*aw minh*aa* i*dzaa* hum yaskha*th*uun**a**

Dan di antara mereka ada yang mencelamu tentang (pembagian) sedekah (zakat); jika mereka diberi bagian, mereka bersenang hati, dan jika mereka tidak diberi bagian, tiba-tiba mereka marah.

9:59

# وَلَوْ اَنَّهُمْ رَضُوْا مَآ اٰتٰىهُمُ اللّٰهُ وَرَسُوْلُهٗۙ وَقَالُوْا حَسْبُنَا اللّٰهُ سَيُؤْتِيْنَا اللّٰهُ مِنْ فَضْلِهٖ وَرَسُوْلُهٗٓ اِنَّآ اِلَى اللّٰهِ رَاغِبُوْنَ ࣖ

walaw annahum ra*dh*uu m*aa* *aa*t*aa*humu **al**l*aa*hu warasuuluhu waq*aa*luu *h*asbun*aa* **al**l*aa*hu sayu/tiin*aa* **al**l*aa*hu min fa*dh*lihi warasu

Dan sekiranya mereka benar-benar rida dengan apa yang diberikan kepada mereka oleh Allah dan Rasul-Nya, dan berkata, “Cukuplah Allah bagi kami, Allah dan Rasul-Nya akan memberikan kepada kami sebagian dari karunia-Nya. Sesungguhnya kami orang-orang yang b

9:60

# ۞ اِنَّمَا الصَّدَقٰتُ لِلْفُقَرَاۤءِ وَالْمَسٰكِيْنِ وَالْعَامِلِيْنَ عَلَيْهَا وَالْمُؤَلَّفَةِ قُلُوْبُهُمْ وَفِى الرِّقَابِ وَالْغَارِمِيْنَ وَفِيْ سَبِيْلِ اللّٰهِ وَابْنِ السَّبِيْلِۗ فَرِيْضَةً مِّنَ اللّٰهِ ۗوَاللّٰهُ عَلِيْمٌ حَكِيْمٌ

innam*aa* **al***shsh*adaq*aa*tu lilfuqar*aa*-i wa**a**lmas*aa*kiini wa**a**l'*aa*miliina 'alayh*aa* wa**a**lmu-allafati quluubuhum wafii **al**rriq*aa*

*Sesungguhnya zakat itu hanyalah untuk orang-orang fakir, orang miskin, amil zakat, yang dilunakkan hatinya (mualaf), untuk (memerdekakan) hamba sahaya, untuk (membebaskan) orang yang berutang, untuk jalan Allah dan untuk orang yang sedang dalam perjalanan*









9:61

# وَمِنْهُمُ الَّذِيْنَ يُؤْذُوْنَ النَّبِيَّ وَيَقُوْلُوْنَ هُوَ اُذُنٌ ۗقُلْ اُذُنُ خَيْرٍ لَّكُمْ يُؤْمِنُ بِاللّٰهِ وَيُؤْمِنُ لِلْمُؤْمِنِيْنَ وَرَحْمَةٌ لِّلَّذِيْنَ اٰمَنُوْا مِنْكُمْۗ وَالَّذِيْنَ يُؤْذُوْنَ رَسُوْلَ اللّٰهِ لَهُمْ عَذَابٌ اَلِيْمٌ

waminhumu **al**la*dz*iina yu/*dz*uuna **al**nnabiyya wayaquuluuna huwa u*dz*unun qul u*dz*unu khayrin lakum yu/minu bi**al**l*aa*hi wayu/minu lilmu/miniina wara*h*matun lilla*dz*ii

Dan di antara mereka (orang munafik) ada orang-orang yang menyakiti hati Nabi (Muhammad) dan mengatakan, “Nabi mempercayai semua apa yang didengarnya.” Katakanlah, “Dia mempercayai semua yang baik bagi kamu, dia beriman kepada Allah, mempercayai orang-or

9:62

# يَحْلِفُوْنَ بِاللّٰهِ لَكُمْ لِيُرْضُوْكُمْ وَاللّٰهُ وَرَسُوْلُهٗٓ اَحَقُّ اَنْ يُّرْضُوْهُ اِنْ كَانُوْا مُؤْمِنِيْنَ

ya*h*lifuuna bi**al**l*aa*hi lakum liyur*dh*uukum wa**al**l*aa*hu warasuuluhu a*h*aqqu an yur*dh*uuhu in k*aa*nuu mu/miniin**a**

Mereka bersumpah kepadamu dengan (nama) Allah untuk menyenangkan kamu, padahal Allah dan Rasul-Nya lebih pantas mereka mencari keridaan-Nya jika mereka orang mukmin.

9:63

# اَلَمْ يَعْلَمُوْٓا اَنَّهٗ مَنْ يُّحَادِدِ اللّٰهَ وَرَسُوْلَهٗ فَاَنَّ لَهٗ نَارَ جَهَنَّمَ خَالِدًا فِيْهَاۗ ذٰلِكَ الْخِزْيُ الْعَظِيْمُ

alam ya'lamuu annahu man yu*haa*didi **al**l*aa*ha warasuulahu fa-anna lahu n*aa*ra jahannama kh*aa*lidan fiih*aa* *dzaa*lika **a**lkhizyu **a**l'a*zh*iim**u**

Tidakkah mereka (orang munafik) mengetahui bahwa barangsiapa menentang Allah dan Rasul-Nya, maka sesungguhnya neraka Jahanamlah baginya, dia kekal di dalamnya. Itulah kehinaan yang besar.

9:64

# يَحْذَرُ الْمُنٰفِقُوْنَ اَنْ تُنَزَّلَ عَلَيْهِمْ سُوْرَةٌ تُنَبِّئُهُمْ بِمَا فِيْ قُلُوْبِهِمْۗ قُلِ اسْتَهْزِءُوْاۚ اِنَّ اللّٰهَ مُخْرِجٌ مَّا تَحْذَرُوْنَ

ya*hts*aru **a**lmun*aa*fiquuna an tunazzala 'alayhim suuratun tunabbi-uhum bim*aa* fii quluubihim quli istahzi-uu inna **al**l*aa*ha mukhrijun m*aa* ta*hts*aruun**a**

Orang-orang munafik itu takut jika diturunkan suatu surah yang menerangkan apa yang tersembunyi di dalam hati mereka. Katakanlah (kepada mereka), “Teruskanlah berolok-olok (terhadap Allah dan Rasul-Nya).” Sesungguhnya Allah akan mengungkapkan apa yang kam

9:65

# وَلَىِٕنْ سَاَلْتَهُمْ لَيَقُوْلُنَّ اِنَّمَا كُنَّا نَخُوْضُ وَنَلْعَبُۗ قُلْ اَبِاللّٰهِ وَاٰيٰتِهٖ وَرَسُوْلِهٖ كُنْتُمْ تَسْتَهْزِءُوْنَ

wala-in sa-altahum layaquulunna innam*aa* kunn*aa* nakhuu*dh*u wanal'abu qul abi**al**l*aa*hi wa*aa*y*aa*tihi warasuulihi kuntum tastahzi-uun**a**

Dan jika kamu tanyakan kepada mereka, niscaya mereka akan menjawab, “Sesungguhnya kami hanya bersenda gurau dan bermain-main saja.” Katakanlah, “Mengapa kepada Allah, dan ayat-ayat-Nya serta Rasul-Nya kamu selalu berolok-olok?”

9:66

# لَا تَعْتَذِرُوْا قَدْ كَفَرْتُمْ بَعْدَ اِيْمَانِكُمْ ۗ اِنْ نَّعْفُ عَنْ طَاۤىِٕفَةٍ مِّنْكُمْ نُعَذِّبْ طَاۤىِٕفَةً ۢ بِاَنَّهُمْ كَانُوْا مُجْرِمِيْنَ ࣖ

l*aa* ta'ta*dz*iruu qad kafartum ba'da iim*aa*nikum in na'fu 'an *thaa*-ifatin minkum nu'a*dzdz*ib *thaa*-ifatan bi-annahum k*aa*nuu mujrimiin**a**

Tidak perlu kamu meminta maaf, karena kamu telah kafir setelah beriman. Jika Kami memaafkan sebagian dari kamu (karena telah tobat), niscaya Kami akan mengazab golongan (yang lain) karena sesungguhnya mereka adalah orang-orang yang (selalu) berbuat dosa.

9:67

# اَلْمُنٰفِقُوْنَ وَالْمُنٰفِقٰتُ بَعْضُهُمْ مِّنْۢ بَعْضٍۘ يَأْمُرُوْنَ بِالْمُنْكَرِ وَيَنْهَوْنَ عَنِ الْمَعْرُوْفِ وَيَقْبِضُوْنَ اَيْدِيَهُمْۗ نَسُوا اللّٰهَ فَنَسِيَهُمْ ۗ اِنَّ الْمُنٰفِقِيْنَ هُمُ الْفٰسِقُوْنَ

almun*aa*fiquuna wa**a**lmun*aa*fiq*aa*tu ba'*dh*uhum min ba'*dh*in ya/muruuna bi**a**lmunkari wayanhawna 'ani **a**lma'ruufi wayaqbi*dh*uuna aydiyahum nasuu **al**l*aa*

*Orang-orang munafik laki-laki dan perempuan, satu dengan yang lain adalah (sama), mereka menyuruh (berbuat) yang mungkar dan mencegah (perbuatan) yang makruf dan mereka menggenggamkan tangannya (kikir). Mereka telah melupakan kepada Allah, maka Allah melu*









9:68

# وَعَدَ اللّٰهُ الْمُنٰفِقِيْنَ وَالْمُنٰفِقٰتِ وَالْكُفَّارَ نَارَ جَهَنَّمَ خٰلِدِيْنَ فِيْهَاۗ هِيَ حَسْبُهُمْ ۚوَلَعَنَهُمُ اللّٰهُ ۚوَلَهُمْ عَذَابٌ مُّقِيْمٌۙ

wa'ada **al**l*aa*hu **a**lmun*aa*fiqiina wa**a**lmun*aa*fiq*aa*ti wa**a**lkuff*aa*ra n*aa*ra jahannama kh*aa*lidiina fiih*aa* hiya *h*asbuhum wala'anahumu

Allah menjanjikan (mengancam) orang-orang munafik laki-laki dan perempuan dan orang-orang kafir dengan neraka Jahanam. Mereka kekal di dalamnya. Cukuplah (neraka) itu bagi mereka. Allah melaknat mereka; dan mereka mendapat azab yang kekal,

9:69

# كَالَّذِيْنَ مِنْ قَبْلِكُمْ كَانُوْٓا اَشَدَّ مِنْكُمْ قُوَّةً وَّاَكْثَرَ اَمْوَالًا وَّاَوْلَادًاۗ فَاسْتَمْتَعُوْا بِخَلَاقِهِمْ فَاسْتَمْتَعْتُمْ بِخَلَاقِكُمْ كَمَا اسْتَمْتَعَ الَّذِيْنَ مِنْ قَبْلِكُمْ بِخَلَاقِهِمْ وَخُضْتُمْ كَالَّذِيْ خَاضُوْاۗ

ka**a**lla*dz*iina min qablikum k*aa*nuu asyadda minkum quwwatan wa-aktsara amw*aa*lan wa-awl*aa*dan fa**i**stamta'uu bikhal*aa*qihim fa**i**stamta'tum bikhal*aa*qikum kam*aa* istam

(keadaan kamu kaum munafik dan musyrikin) seperti orang-orang sebelum kamu, mereka lebih kuat daripada kamu, dan lebih banyak harta dan anak-anaknya. Maka mereka telah menikmati bagiannya, dan kamu telah menikmati bagianmu sebagaimana orang-orang yang seb

9:70

# اَلَمْ يَأْتِهِمْ نَبَاُ الَّذِيْنَ مِنْ قَبْلِهِمْ قَوْمِ نُوْحٍ وَّعَادٍ وَّثَمُوْدَ ەۙ وَقَوْمِ اِبْرٰهِيْمَ وَاَصْحٰبِ مَدْيَنَ وَالْمُؤْتَفِكٰتِۗ اَتَتْهُمْ رُسُلُهُمْ بِالْبَيِّنٰتِۚ فَمَا كَانَ اللّٰهُ لِيَظْلِمَهُمْ وَلٰكِنْ كَانُوْٓا اَنْفُسَهُمْ

alam ya/tihim nabau **al**la*dz*iina min qablihim qawmi nuu*h*in wa'*aa*din watsamuuda waqawmi ibr*aa*hiima wa-a*sh*-*haa*bi madyana wa**a**lmu/tafik*aa*ti atat-hum rusuluhum bi**a**

**Apakah tidak sampai kepada mereka berita (tentang) orang-orang yang sebelum mereka, (yaitu) kaum Nuh, ‘Ad, samud, kaum Ibrahim, penduduk Madyan, dan (penduduk) negeri-negeri yang telah musnah? Telah datang kepada mereka rasul-rasul dengan membawa bukti-bu**









9:71

# وَالْمُؤْمِنُوْنَ وَالْمُؤْمِنٰتُ بَعْضُهُمْ اَوْلِيَاۤءُ بَعْضٍۘ يَأْمُرُوْنَ بِالْمَعْرُوْفِ وَيَنْهَوْنَ عَنِ الْمُنْكَرِ وَيُقِيْمُوْنَ الصَّلٰوةَ وَيُؤْتُوْنَ الزَّكٰوةَ وَيُطِيْعُوْنَ اللّٰهَ وَرَسُوْلَهٗ ۗاُولٰۤىِٕكَ سَيَرْحَمُهُمُ اللّٰهُ ۗاِنَّ ا

wa**a**lmu/minuuna wa**a**lmu/min*aa*tu ba'*dh*uhum awliy*aa*u ba'*dh*in ya/muruuna bi**a**lma'ruufi wayanhawna 'ani **a**lmunkari wayuqiimuuna **al***shsh*al*aa*

*Dan orang-orang yang beriman, laki-laki dan perempuan, sebagian mereka menjadi penolong bagi sebagian yang lain. Mereka menyuruh (berbuat) yang makruf, dan mencegah dari yang mungkar, melaksanakan salat, menunaikan zakat, dan taat kepada Allah dan Rasul-N*









9:72

# وَعَدَ اللّٰهُ الْمُؤْمِنِيْنَ وَالْمُؤْمِنٰتِ جَنّٰتٍ تَجْرِيْ مِنْ تَحْتِهَا الْاَنْهٰرُ خٰلِدِيْنَ فِيْهَا وَمَسٰكِنَ طَيِّبَةً فِيْ جَنّٰتِ عَدْنٍ ۗوَرِضْوَانٌ مِّنَ اللّٰهِ اَكْبَرُ ۗذٰلِكَ هُوَ الْفَوْزُ الْعَظِيْمُ ࣖ

wa'ada **al**l*aa*hu **a**lmu/miniina wa**a**lmu/min*aa*ti jann*aa*tin tajrii min ta*h*tih*aa* **a**l-anh*aa*ru kh*aa*lidiina fiih*aa* wamas*aa*kina *th*

Allah menjanjikan kepada orang-orang mukmin laki-laki dan perempuan, (akan mendapat) surga yang mengalir di bawahnya sungai-sungai, mereka kekal di dalamnya, dan (mendapat) tempat yang baik di surga ‘Adn. Dan keridaan Allah lebih besar. Itulah kemenangan

9:73

# يٰٓاَيُّهَا النَّبِيُّ جَاهِدِ الْكُفَّارَ وَالْمُنٰفِقِيْنَ وَاغْلُظْ عَلَيْهِمْ ۗوَمَأْوٰىهُمْ جَهَنَّمُ وَبِئْسَ الْمَصِيْرُ

y*aa* ayyuh*aa* **al**nnabiyyu j*aa*hidi **a**lkuff*aa*ra wa**a**lmun*aa*fiqiina wa**u**ghlu*zh* 'alayhim wama/w*aa*hum jahannamu wabi/sa **a**lma*sh*

Wahai Nabi! Berjihadlah (melawan) orang-orang kafir dan orang-orang munafik, dan bersikap keraslah terhadap mereka. Tempat mereka adalah neraka Jahanam. Dan itulah seburuk-buruk tempat kembali.

9:74

# يَحْلِفُوْنَ بِاللّٰهِ مَا قَالُوْا ۗوَلَقَدْ قَالُوْا كَلِمَةَ الْكُفْرِ وَكَفَرُوْا بَعْدَ اِسْلَامِهِمْ وَهَمُّوْا بِمَا لَمْ يَنَالُوْاۚ وَمَا نَقَمُوْٓا اِلَّآ اَنْ اَغْنٰىهُمُ اللّٰهُ وَرَسُوْلُهٗ مِنْ فَضْلِهٖ ۚفَاِنْ يَّتُوْبُوْا يَكُ خَيْرًا لَّ

ya*h*lifuuna bi**al**l*aa*hi m*aa* q*aa*luu walaqad q*aa*luu kalimata **a**lkufri wakafaruu ba'da isl*aa*mihim wahammuu bim*aa* lam yan*aa*luu wam*aa* naqamuu ill*aa* an aghn*aa*

Mereka (orang munafik) bersumpah dengan (nama) Allah, bahwa mereka tidak mengatakan (sesuatu yang menyakiti Muhammad). Sungguh, mereka telah mengucapkan perkataan kekafiran, dan telah menjadi kafir setelah Islam, dan menginginkan apa yang mereka tidak dap







9:75

# ۞ وَمِنْهُمْ مَّنْ عٰهَدَ اللّٰهَ لَىِٕنْ اٰتٰىنَا مِنْ فَضْلِهٖ لَنَصَّدَّقَنَّ وَلَنَكُوْنَنَّ مِنَ الصّٰلِحِيْنَ

waminhum man '*aa*hada **al**l*aa*ha la-in *aa*t*aa*n*aa* min fa*dh*lihi lana*shsh*addaqanna walanakuunanna mina **al***shshaa*li*h*iin**a**

Dan di antara mereka ada orang yang telah berjanji kepada Allah, “Sesungguhnya jika Allah memberikan sebagian dari karunia-Nya kepada kami, niscaya kami akan bersedekah dan niscaya kami termasuk orang-orang yang saleh.”

9:76

# فَلَمَّآ اٰتٰىهُمْ مِّنْ فَضْلِهٖ بَخِلُوْا بِهٖ وَتَوَلَّوْا وَّهُمْ مُّعْرِضُوْنَ

falamm*aa* *aa*t*aa*hum min fa*dh*lihi bakhiluu bihi watawallaw wahum mu'ri*dh*uun**a**

Ketika Allah memberikan kepada mereka sebagian dari karunia-Nya, mereka menjadi kikir dan berpaling, dan selalu menentang (kebenaran).

9:77

# فَاَعْقَبَهُمْ نِفَاقًا فِيْ قُلُوْبِهِمْ اِلٰى يَوْمِ يَلْقَوْنَهٗ بِمَآ اَخْلَفُوا اللّٰهَ مَا وَعَدُوْهُ وَبِمَا كَانُوْا يَكْذِبُوْنَ

fa-a'qabahum nif*aa*qan fii quluubihim il*aa* yawmi yalqawnahu bim*aa* akhlafuu **al**l*aa*ha m*aa* wa'aduuhu wabim*aa* k*aa*nuu yak*dz*ibuun**a**

Maka Allah menanamkan kemunafikan dalam hati mereka sampai pada waktu mereka menemui-Nya, karena mereka telah mengingkari janji yang telah mereka ikrarkan kepada-Nya dan (juga) karena mereka selalu berdusta.

9:78

# اَلَمْ يَعْلَمُوْٓا اَنَّ اللّٰهَ يَعْلَمُ سِرَّهُمْ وَنَجْوٰىهُمْ وَاَنَّ اللّٰهَ عَلَّامُ الْغُيُوْبِ

alam ya'lamuu anna **al**l*aa*ha ya'lamu sirrahum wanajw*aa*hum wa-anna **al**l*aa*ha 'all*aa*mu **a**lghuyuub**i**

Tidakkah mereka mengetahui bahwa Allah mengetahui rahasia dan bisikan mereka, dan bahwa Allah mengetahui segala yang gaib?

9:79

# اَلَّذِيْنَ يَلْمِزُوْنَ الْمُطَّوِّعِيْنَ مِنَ الْمُؤْمِنِيْنَ فِى الصَّدَقٰتِ وَالَّذِيْنَ لَا يَجِدُوْنَ اِلَّا جُهْدَهُمْ فَيَسْخَرُوْنَ مِنْهُمْ ۗسَخِرَ اللّٰهُ مِنْهُمْ ۖ وَلَهُمْ عَذَابٌ اَلِيْمٌ

**al**la*dz*iina yalmizuuna **a**lmu*ththh*awwi'iina mina **a**lmu/miniina fii **al***shsh*adaq*aa*ti wa**a**lla*dz*iina l*aa* yajiduuna ill*aa* juhdahum f

(Orang munafik) yaitu mereka yang mencela orang-orang beriman yang memberikan sedekah dengan sukarela dan yang (mencela) orang-orang yang hanya memperoleh (untuk disedekahkan) sekedar kesanggupannya, maka orang-orang munafik itu menghina mereka. Allah aka

9:80

# اِسْتَغْفِرْ لَهُمْ اَوْ لَا تَسْتَغْفِرْ لَهُمْۗ اِنْ تَسْتَغْفِرْ لَهُمْ سَبْعِيْنَ مَرَّةً فَلَنْ يَّغْفِرَ اللّٰهُ لَهُمْ ۗذٰلِكَ بِاَنَّهُمْ كَفَرُوْا بِاللّٰهِ وَرَسُوْلِهٖۗ وَاللّٰهُ لَا يَهْدِى الْقَوْمَ الْفٰسِقِيْنَ ࣖ

istaghfir lahum aw l*aa* tastaghfir lahum in tastaghfir lahum sab'iina marratan falan yaghfira **al**l*aa*hu lahum *dzaa*lika bi-annahum kafaruu bi**al**l*aa*hi warasuulihi wa**al**l*aa*hu l<

(Sama saja) engkau (Muhammad) memohonkan ampunan bagi mereka atau tidak memohonkan ampunan bagi mereka. Walaupun engkau memohonkan ampunan bagi mereka tujuh puluh kali, Allah tidak akan memberi ampunan kepada mereka. Yang demikian itu karena mereka ingkar

9:81

# فَرِحَ الْمُخَلَّفُوْنَ بِمَقْعَدِهِمْ خِلٰفَ رَسُوْلِ اللّٰهِ وَكَرِهُوْٓا اَنْ يُّجَاهِدُوْا بِاَمْوَالِهِمْ وَاَنْفُسِهِمْ فِيْ سَبِيْلِ اللّٰهِ وَقَالُوْا لَا تَنْفِرُوْا فِى الْحَرِّۗ قُلْ نَارُ جَهَنَّمَ اَشَدُّ حَرًّاۗ لَوْ كَانُوْا يَفْقَهُوْنَ

fari*h*a **a**lmukhallafuuna bimaq'adihim khil*aa*fa rasuuli **al**l*aa*hi wakarihuu an yuj*aa*hiduu bi-amw*aa*lihim wa-anfusihim fii sabiili **al**l*aa*hi waq*aa*luu l*aa* tan

Orang-orang yang ditinggalkan (tidak ikut berperang), merasa gembira dengan duduk-duduk diam sepeninggal Rasulullah. Mereka tidak suka berjihad dengan harta dan jiwa mereka di jalan Allah dan mereka berkata, “Janganlah kamu berangkat (pergi berperang) dal

9:82

# فَلْيَضْحَكُوْا قَلِيْلًا وَّلْيَبْكُوْا كَثِيْرًاۚ جَزَاۤءًۢ بِمَا كَانُوْا يَكْسِبُوْنَ

falya*dh*akuu qaliilan walyabkuu katsiiran jaz*aa*-an bim*aa* k*aa*nuu yaksibuun**a**

Maka biarkanlah mereka tertawa sedikit dan menangis yang banyak, sebagai balasan terhadap apa yang selalu mereka perbuat.

9:83

# فَاِنْ رَّجَعَكَ اللّٰهُ اِلٰى طَاۤىِٕفَةٍ مِّنْهُمْ فَاسْتَأْذَنُوْكَ لِلْخُرُوْجِ فَقُلْ لَّنْ تَخْرُجُوْا مَعِيَ اَبَدًا وَّلَنْ تُقَاتِلُوْا مَعِيَ عَدُوًّاۗ اِنَّكُمْ رَضِيْتُمْ بِالْقُعُوْدِ اَوَّلَ مَرَّةٍۗ فَاقْعُدُوْا مَعَ الْخَالِفِيْنَ

fa-in raja'aka **al**l*aa*hu il*aa* *thaa*-ifatin minhum fa**i**sta/*dz*anuuka lilkhuruuji faqul lan takhrujuu ma'iya abadan walan tuq*aa*tiluu ma'iya 'aduwwan innakum ra*dh*iitum bi**a<**

Maka jika Allah mengembalikanmu (Muhammad) kepada suatu golongan dari mereka (orang-orang munafik), kemudian mereka meminta izin kepadamu untuk keluar (pergi berperang), maka katakanlah, “Kamu tidak boleh keluar bersamaku selama-lamanya dan tidak boleh me







9:84

# وَلَا تُصَلِّ عَلٰٓى اَحَدٍ مِّنْهُمْ مَّاتَ اَبَدًا وَّلَا تَقُمْ عَلٰى قَبْرِهٖۗ اِنَّهُمْ كَفَرُوْا بِاللّٰهِ وَرَسُوْلِهٖ وَمَاتُوْا وَهُمْ فٰسِقُوْنَ

wal*aa* tu*sh*alli 'al*aa* a*h*adin minhum m*aa*ta abadan wal*aa* taqum 'al*aa* qabrihi innahum kafaruu bi**al**l*aa*hi warasuulihi wam*aa*tuu wahum f*aa*siquun**a**

Dan janganlah engkau (Muhammad) melaksanakan salat untuk seseorang yang mati di antara mereka (orang-orang munafik), selama-lamanya dan janganlah engkau berdiri (mendoakan) di atas kuburnya. Sesungguhnya mereka ingkar kepada Allah dan Rasul-Nya dan mereka

9:85

# وَلَا تُعْجِبْكَ اَمْوَالُهُمْ وَاَوْلَادُهُمْۗ اِنَّمَا يُرِيْدُ اللّٰهُ اَنْ يُّعَذِّبَهُمْ بِهَا فِى الدُّنْيَا وَتَزْهَقَ اَنْفُسُهُمْ وَهُمْ كٰفِرُوْنَ

wal*aa* tu'jibka amw*aa*luhum wa-awl*aa*duhum innam*aa* yuriidu **al**l*aa*hu an yu'a*dzdz*ibahum bih*aa* fii **al**dduny*aa* watazhaqa anfusuhum wahum k*aa*firuun**a**

Dan janganlah engkau (Muhammad) kagum terhadap harta dan anak-anak mereka. Sesungguhnya dengan itu Allah hendak menyiksa mereka di dunia dan agar nyawa mereka melayang, sedang mereka dalam keadaan kafir.

9:86

# وَاِذَآ اُنْزِلَتْ سُوْرَةٌ اَنْ اٰمِنُوْا بِاللّٰهِ وَجَاهِدُوْا مَعَ رَسُوْلِهِ اسْتَأْذَنَكَ اُولُوا الطَّوْلِ مِنْهُمْ وَقَالُوْا ذَرْنَا نَكُنْ مَّعَ الْقٰعِدِيْنَ

wa-i*dzaa* unzilat suuratun an *aa*minuu bi**al**l*aa*hi waj*aa*hiduu ma'a rasuulihi ista/*dz*anaka uluu **al***ththh*awli minhum waq*aa*luu *dz*arn*aa* nakun ma'a **a**lq

Dan apabila diturunkan suatu surah (yang memerintahkan kepada orang-orang munafik), “Berimanlah kepada Allah dan berjihadlah bersama Rasul-Nya,” niscaya orang-orang yang kaya dan berpengaruh di antara mereka meminta izin kepadamu (untuk tidak berjihad) da

9:87

# رَضُوْا بِاَنْ يَّكُوْنُوْا مَعَ الْخَوَالِفِ وَطُبِعَ عَلٰى قُلُوْبِهِمْ فَهُمْ لَا يَفْقَهُوْنَ

ra*dh*uu bi-an yakuunuu ma'a **a**lkhaw*aa*lifi wa*th*ubi'a 'al*aa* quluubihim fahum l*aa* yafqahuun**a**

Mereka rela berada bersama orang-orang yang tidak pergi berperang, dan hati mereka telah tertutup, sehingga mereka tidak memahami (kebahagiaan beriman dan berjihad).

9:88

# لٰكِنِ الرَّسُوْلُ وَالَّذِيْنَ اٰمَنُوْا مَعَهٗ جَاهَدُوْا بِاَمْوَالِهِمْ وَاَنْفُسِهِمْۗ وَاُولٰۤىِٕكَ لَهُمُ الْخَيْرٰتُ ۖوَاُولٰۤىِٕكَ هُمُ الْمُفْلِحُوْنَ

l*aa*kini **al**rrasuulu wa**a**lla*dz*iina *aa*manuu ma'ahu j*aa*haduu bi-amw*aa*lihim wa-anfusihim waul*aa*-ika lahumu **a**lkhayr*aa*tu waul*aa*-ika humu **a**l

Tetapi Rasul dan orang-orang yang beriman bersama dia, (mereka) berjihad dengan harta dan jiwa. Mereka itu memperoleh kebaikan. Mereka itulah orang-orang yang beruntung.

9:89

# اَعَدَّ اللّٰهُ لَهُمْ جَنّٰتٍ تَجْرِيْ مِنْ تَحْتِهَا الْاَنْهٰرُ خٰلِدِيْنَ فِيْهَاۗ ذٰلِكَ الْفَوْزُ الْعَظِيْمُ ࣖ

a'adda **al**l*aa*hu lahum jann*aa*tin tajrii min ta*h*tih*aa* **a**l-anh*aa*ru kh*aa*lidiina fiih*aa* *dzaa*lika **a**lfawzu **a**l'a*zh*iim**u**

**Allah telah menyediakan bagi mereka surga yang mengalir di bawahnya sungai-sungai, mereka kekal di dalamnya. Itulah kemenangan yang agung.**









9:90

# وَجَاۤءَ الْمُعَذِّرُوْنَ مِنَ الْاَعْرَابِ لِيُؤْذَنَ لَهُمْ وَقَعَدَ الَّذِيْنَ كَذَبُوا اللّٰهَ وَرَسُوْلَهٗ ۗسَيُصِيْبُ الَّذِيْنَ كَفَرُوْا مِنْهُمْ عَذَابٌ اَلِيْمٌ

waj*aa*-a **a**lmu'a*dzdz*iruuna mina **a**l-a'r*aa*bi liyu/*dz*ana lahum waqa'ada **al**la*dz*iina ka*dz*abuu **al**l*aa*ha warasuulahu sayu*sh*iibu **al**

**Dan di antara orang-orang Arab Badui datang (kepada Nabi) mengemukakan alasan, agar diberi izin (untuk tidak pergi berperang), sedang orang-orang yang mendustakan Allah dan Rasul-Nya, duduk berdiam. Kelak orang-orang yang kafir di antara mereka akan ditim**









9:91

# لَيْسَ عَلَى الضُّعَفَاۤءِ وَلَا عَلَى الْمَرْضٰى وَلَا عَلَى الَّذِيْنَ لَا يَجِدُوْنَ مَا يُنْفِقُوْنَ حَرَجٌ اِذَا نَصَحُوْا لِلّٰهِ وَرَسُوْلِهٖۗ مَا عَلَى الْمُحْسِنِيْنَ مِنْ سَبِيْلٍ ۗوَاللّٰهُ غَفُوْرٌ رَّحِيْمٌۙ

laysa 'al*aa* **al***dhdh*u'af*aa*-i wal*aa* 'al*aa* **a**lmar*daa* wal*aa* 'al*aa* **al**la*dz*iina l*aa* yajiduuna m*aa* yunfiquuna *h*arajun i*dzaa*

Tidak ada dosa (karena tidak pergi berperang) atas orang yang lemah, orang yang sakit dan orang yang tidak memperoleh apa yang akan mereka infakkan, apabila mereka berlaku ikhlas kepada Allah dan Rasul-Nya. Tidak ada alasan apa pun untuk menyalahkan orang

9:92

# وَّلَا عَلَى الَّذِيْنَ اِذَا مَآ اَتَوْكَ لِتَحْمِلَهُمْ قُلْتَ لَآ اَجِدُ مَآ اَحْمِلُكُمْ عَلَيْهِ ۖتَوَلَّوْا وَّاَعْيُنُهُمْ تَفِيْضُ مِنَ الدَّمْعِ حَزَنًا اَلَّا يَجِدُوْا مَا يُنْفِقُوْنَۗ

wal*aa* 'al*aa* **al**la*dz*iina i*dzaa* m*aa* atawka lita*h*milahum qulta l*aa* ajidu m*aa* a*h*milukum 'alayhi tawallaw wa-a'yunuhum tafii*dh*u mina **al**ddam'i *h*azanan

dan tidak ada (pula dosa) atas orang-orang yang datang kepadamu (Muhammad), agar engkau memberi kendaraan kepada mereka, lalu engkau berkata, “Aku tidak memperoleh kendaraan untuk membawamu,” lalu mereka kembali, sedang mata mereka bercucuran air mata kar

9:93

# اِنَّمَا السَّبِيْلُ عَلَى الَّذِيْنَ يَسْتَأْذِنُوْنَكَ وَهُمْ اَغْنِيَاۤءُۚ رَضُوْا بِاَنْ يَّكُوْنُوْا مَعَ الْخَوَالِفِۙ وَطَبَعَ اللّٰهُ عَلٰى قُلُوْبِهِمْ فَهُمْ لَا يَعْلَمُوْنَ ۔

innam*aa* **al**ssabiilu 'al*aa* **al**la*dz*iina yasta/*dz*inuunaka wahum aghniy*aa*u ra*dh*uu bi-an yakuunuu ma'a **a**lkhaw*aa*lifi wa*th*aba'a **al**l*aa*

Sesungguhnya alasan (untuk menyalahkan) hanyalah terhadap orang-orang yang meminta izin kepadamu (untuk tidak ikut berperang), padahal mereka orang kaya. Mereka rela berada bersama orang-orang yang tidak ikut berperang dan Allah telah mengunci hati mereka

9:94

# يَعْتَذِرُوْنَ اِلَيْكُمْ اِذَا رَجَعْتُمْ اِلَيْهِمْ ۗ قُلْ لَّا تَعْتَذِرُوْا لَنْ نُّؤْمِنَ لَكُمْ قَدْ نَبَّاَنَا اللّٰهُ مِنْ اَخْبَارِكُمْ وَسَيَرَى اللّٰهُ عَمَلَكُمْ وَرَسُوْلُهٗ ثُمَّ تُرَدُّوْنَ اِلٰى عٰلِمِ الْغَيْبِ وَالشَّهَادَةِ فَيُنَبِّئُك

ya'ta*dz*iruuna ilaykum i*dzaa* raja'tum ilayhim qul l*aa* ta'ta*dz*iruu lan nu/mina lakum qad nabba-an*aa* **al**l*aa*hu min akhb*aa*rikum wasayar*aa* **al**l*aa*hu 'amalakum warasuul

Mereka (orang-orang munafik yang tidak ikut berperang) akan mengemukakan alasannya kepadamu ketika kamu telah kembali kepada mereka. Katakanlah (Muhammad), “Janganlah kamu mengemukakan alasan; kami tidak percaya lagi kepadamu, sungguh, Allah telah memberi

9:95

# سَيَحْلِفُوْنَ بِاللّٰهِ لَكُمْ اِذَا انْقَلَبْتُمْ اِلَيْهِمْ لِتُعْرِضُوْا عَنْهُمْ ۗ فَاَعْرِضُوْا عَنْهُمْ ۗ اِنَّهُمْ رِجْسٌۙ وَّمَأْوٰىهُمْ جَهَنَّمُ جَزَاۤءً ۢبِمَا كَانُوْا يَكْسِبُوْنَ

saya*h*lifuuna bi**al**l*aa*hi lakum i*dzaa* inqalabtum ilayhim litu'ri*dh*uu 'anhum fa-a'ri*dh*uu 'anhum innahum rijsun wama/w*aa*hum jahannamu jaz*aa*-an bim*aa* k*aa*nuu yaksibuun**a**

**Mereka akan bersumpah kepadamu dengan nama Allah, ketika kamu kembali kepada mereka, agar kamu berpaling dari mereka. Maka berpalinglah dari mereka; karena sesungguhnya mereka itu berjiwa kotor dan tempat mereka neraka Jahanam, sebagai balasan atas apa ya**









9:96

# يَحْلِفُوْنَ لَكُمْ لِتَرْضَوْا عَنْهُمْ ۚفَاِنْ تَرْضَوْا عَنْهُمْ فَاِنَّ اللّٰهَ لَا يَرْضٰى عَنِ الْقَوْمِ الْفٰسِقِيْنَ

ya*h*lifuuna lakum litar*dh*aw 'anhum fa-in tar*dh*aw 'anhum fa-inna **al**l*aa*ha l*aa* yar*daa* 'ani **a**lqawmi **a**lf*aa*siqiin**a**

Mereka akan bersumpah kepadamu agar kamu bersedia menerima mereka. Tetapi sekalipun kamu menerima mereka, Allah tidak akan rida kepada orang-orang yang fasik.

9:97

# اَلْاَعْرَابُ اَشَدُّ كُفْرًا وَّنِفَاقًا وَّاَجْدَرُ اَلَّا يَعْلَمُوْا حُدُوْدَ مَآ اَنْزَلَ اللّٰهُ عَلٰى رَسُوْلِهٖ ۗوَاللّٰهُ عَلِيْمٌ حَكِيْمٌ

al-a'r*aa*bu asyaddu kufran wanif*aa*qan wa-ajdaru **al**l*aa* ya'lamuu *h*uduuda m*aa* anzala **al**l*aa*hu 'al*aa* rasuulihi wa**al**l*aa*hu 'aliimun *h*akiim**un<**

Orang-orang Arab Badui itu lebih kuat kekafiran dan kemunafikannya, dan sangat wajar tidak mengetahui hukum-hukum yang diturunkan Allah kepada Rasul-Nya. Allah Maha Mengetahui, Mahabijaksana.







9:98

# وَمِنَ الْاَعْرَابِ مَنْ يَّتَّخِذُ مَا يُنْفِقُ مَغْرَمًا وَّيَتَرَبَّصُ بِكُمُ الدَّوَاۤىِٕرَ ۗعَلَيْهِمْ دَاۤىِٕرَةُ السَّوْءِ ۗوَاللّٰهُ سَمِيْعٌ عَلِيْمٌ

wamina **a**l-a'r*aa*bi man yattakhi*dz*u m*aa* yunfiqu maghraman wayatarabba*sh*u bikumu **al**ddaw*aa*-ira 'alayhim d*aa*-iratu **al**ssaw-i wa**al**l*aa*hu samii'un

Dan di antara orang-orang Arab Badui itu ada yang memandang apa yang diinfakkannya (di jalan Allah) sebagai suatu kerugian; dia menanti-nanti marabahaya menimpamu, merekalah yang akan ditimpa marabahaya. Allah Maha Mendengar, Maha Mengetahui.

9:99

# وَمِنَ الْاَعْرَابِ مَنْ يُّؤْمِنُ بِاللّٰهِ وَالْيَوْمِ الْاٰخِرِ وَيَتَّخِذُ مَا يُنْفِقُ قُرُبٰتٍ عِنْدَ اللّٰهِ وَصَلَوٰتِ الرَّسُوْلِ ۗ اَلَآ اِنَّهَا قُرْبَةٌ لَّهُمْ ۗ سَيُدْخِلُهُمُ اللّٰهُ فِيْ رَحْمَتِهٖ ۗاِنَّ اللّٰهَ غَفُوْرٌ رَّحِيْمٌ ࣖ

wamina **a**l-a'r*aa*bi man yu/minu bi**al**l*aa*hi wa**a**lyawmi **a**l-*aa*khiri wayattakhi*dz*u m*aa* yunfiqu qurub*aa*tin 'inda **al**l*aa*hi wa*sh*

*Dan di antara orang-orang Arab Badui itu ada yang beriman kepada Allah dan hari kemudian, dan memandang apa yang diinfakkannya (di jalan Allah) sebagai jalan mendekatkan kepada Allah dan sebagai jalan untuk (memperoleh) doa Rasul. Ketahuilah, sesungguhnya*









9:100

# وَالسّٰبِقُوْنَ الْاَوَّلُوْنَ مِنَ الْمُهٰجِرِيْنَ وَالْاَنْصَارِ وَالَّذِيْنَ اتَّبَعُوْهُمْ بِاِحْسَانٍۙ رَّضِيَ اللّٰهُ عَنْهُمْ وَرَضُوْا عَنْهُ وَاَعَدَّ لَهُمْ جَنّٰتٍ تَجْرِيْ تَحْتَهَا الْاَنْهٰرُ خٰلِدِيْنَ فِيْهَآ اَبَدًا ۗذٰلِكَ الْفَوْزُ الْ

wa**al**ss*aa*biquuna **a**l-awwaluuna mina **a**lmuh*aa*jiriina wa**a**l-an*shaa*ri wa**a**lla*dz*iina ittaba'uuhum bi-i*h*s*aa*nin ra*dh*iya **al**

**Dan orang-orang yang terdahulu lagi yang pertama-tama (masuk Islam) di antara orang-orang Muhajirin dan Ansar dan orang-orang yang mengikuti mereka dengan baik, Allah rida kepada mereka dan mereka pun rida kepada Allah. Allah menyediakan bagi mereka surga**









9:101

# وَمِمَّنْ حَوْلَكُمْ مِّنَ الْاَعْرَابِ مُنٰفِقُوْنَ ۗوَمِنْ اَهْلِ الْمَدِيْنَةِ مَرَدُوْا عَلَى النِّفَاقِۗ لَا تَعْلَمُهُمْۗ نَحْنُ نَعْلَمُهُمْۗ سَنُعَذِّبُهُمْ مَّرَّتَيْنِ ثُمَّ يُرَدُّوْنَ اِلٰى عَذَابٍ عَظِيْمٍ ۚ

wamimman *h*awlakum mina **a**l-a'r*aa*bi mun*aa*fiquuna wamin ahli **a**lmadiinati maraduu 'al*aa* **al**nnif*aa*qi l*aa* ta'lamuhum na*h*nu na'lamuhum sanu'a*dzdz*ibuhum marr

Dan di antara orang-orang Arab Badui yang (tinggal) di sekitarmu, ada orang-orang munafik. Dan di antara penduduk Madinah (ada juga orang-orang munafik), mereka keterlaluan dalam kemunafikannya. Engkau (Muhammad) tidak mengetahui mereka, tetapi Kami menge

9:102

# وَاٰخَرُوْنَ اعْتَرَفُوْا بِذُنُوْبِهِمْ خَلَطُوْا عَمَلًا صَالِحًا وَّاٰخَرَ سَيِّئًاۗ عَسَى اللّٰهُ اَنْ يَّتُوْبَ عَلَيْهِمْۗ اِنَّ اللّٰهَ غَفُوْرٌ رَّحِيْمٌ

wa*aa*kharuuna i'tarafuu bi*dz*unuubihim khala*th*uu 'amalan *shaa*li*h*an wa*aa*khara sayyi-an 'as*aa* **al**l*aa*hu an yatuuba 'alayhim inna **al**l*aa*ha ghafuurun ra*h*iim

Dan (ada pula) orang lain yang mengakui dosa-dosa mereka, mereka mencampuradukkan pekerjaan yang baik dengan pekerjaan lain yang buruk. Mudah-mudahan Allah menerima tobat mereka. Sesungguhnya Allah Maha Pengampun, Maha Penyayang.

9:103

# خُذْ مِنْ اَمْوَالِهِمْ صَدَقَةً تُطَهِّرُهُمْ وَتُزَكِّيْهِمْ بِهَا وَصَلِّ عَلَيْهِمْۗ اِنَّ صَلٰوتَكَ سَكَنٌ لَّهُمْۗ وَاللّٰهُ سَمِيْعٌ عَلِيْمٌ

khu*dz* min amw*aa*lihim *sh*adaqatan tu*th*ahhiruhum watuzakkiihim bih*aa* wa*sh*alli 'alayhim inna *sh*al*aa*taka sakanun lahum wa**al**l*aa*hu samii'un 'aliim**un**

Ambillah zakat dari harta mereka, guna membersihkan dan menyucikan mereka, dan berdoalah untuk mereka. Sesungguhnya doamu itu (menumbuhkan) ketenteraman jiwa bagi mereka. Allah Maha Mendengar, Maha Mengetahui.

9:104

# اَلَمْ يَعْلَمُوْٓا اَنَّ اللّٰهَ هُوَ يَقْبَلُ التَّوْبَةَ عَنْ عِبَادِهٖ وَيَأْخُذُ الصَّدَقٰتِ وَاَنَّ اللّٰهَ هُوَ التَّوَّابُ الرَّحِيْمُ

alam ya'lamuu anna **al**l*aa*ha huwa yaqbalu **al**ttawbata 'an 'ib*aa*dihi waya/khu*dz*u **al***shsh*adaq*aa*ti wa-anna **al**l*aa*ha huwa **al**ttaww*aa*

*Tidakkah mereka mengetahui, bahwa Allah menerima tobat hamba-hamba-Nya dan menerima zakat(nya), dan bahwa Allah Maha Penerima tobat, Maha Penyayang?*









9:105

# وَقُلِ اعْمَلُوْا فَسَيَرَى اللّٰهُ عَمَلَكُمْ وَرَسُوْلُهٗ وَالْمُؤْمِنُوْنَۗ وَسَتُرَدُّوْنَ اِلٰى عٰلِمِ الْغَيْبِ وَالشَّهَادَةِ فَيُنَبِّئُكُمْ بِمَا كُنْتُمْ تَعْمَلُوْنَۚ

waquli i'maluu fasayar*aa* **al**l*aa*hu 'amalakum warasuuluhu wa**a**lmu/minuuna wasaturadduuna il*aa* '*aa*limi **a**lghaybi wa**al**sysyah*aa*dati fayunabbi-ukum bim*aa*

Dan katakanlah, “Bekerjalah kamu, maka Allah akan melihat pekerjaanmu, begitu juga Rasul-Nya dan orang-orang mukmin, dan kamu akan dikembalikan kepada (Allah) Yang Mengetahui yang gaib dan yang nyata, lalu diberitakan-Nya kepada kamu apa yang telah kamu k

9:106

# وَاٰخَرُوْنَ مُرْجَوْنَ لِاَمْرِ اللّٰهِ اِمَّا يُعَذِّبُهُمْ وَاِمَّا يَتُوْبُ عَلَيْهِمْۗ وَاللّٰهُ عَلِيْمٌ حَكِيْمٌ

wa*aa*kharuuna murjawna li-amri **al**l*aa*hi imm*aa* yu'a*dzdz*ibuhum wa-imm*aa* yatuubu 'alayhim wa**al**l*aa*hu 'aliimun *h*akiim**un**

Dan ada (pula) orang-orang lain yang ditangguhkan sampai ada keputusan Allah; mungkin Allah akan mengazab mereka dan mungkin Allah akan menerima tobat mereka. Allah Maha Mengetahui, Mahabijaksana.

9:107

# وَالَّذِيْنَ اتَّخَذُوْا مَسْجِدًا ضِرَارًا وَّكُفْرًا وَّتَفْرِيْقًاۢ بَيْنَ الْمُؤْمِنِيْنَ وَاِرْصَادًا لِّمَنْ حَارَبَ اللّٰهَ وَرَسُوْلَهٗ مِنْ قَبْلُ ۗوَلَيَحْلِفُنَّ اِنْ اَرَدْنَآ اِلَّا الْحُسْنٰىۗ وَاللّٰهُ يَشْهَدُ اِنَّهُمْ لَكٰذِبُوْنَ

wa**a**lla*dz*iina ittakha*dz*uu masjidan *dh*ir*aa*ran wakufran watafriiqan bayna **a**lmu/miniina wa-ir*shaa*dan liman *haa*raba **al**l*aa*ha warasuulahu min qablu walaya*h*

Dan (di antara orang-orang munafik itu) ada yang mendirikan masjid untuk menimbulkan bencana (pada orang-orang yang beriman), untuk kekafiran dan untuk memecah belah di antara orang-orang yang beriman serta menunggu kedatangan orang-orang yang telah memer

9:108

# لَا تَقُمْ فِيْهِ اَبَدًاۗ لَمَسْجِدٌ اُسِّسَ عَلَى التَّقْوٰى مِنْ اَوَّلِ يَوْمٍ اَحَقُّ اَنْ تَقُوْمَ فِيْهِۗ فِيْهِ رِجَالٌ يُّحِبُّوْنَ اَنْ يَّتَطَهَّرُوْاۗ وَاللّٰهُ يُحِبُّ الْمُطَّهِّرِيْنَ

l*aa* taqum fiihi abadan lamasjidun ussisa 'al*aa* **al**ttaqw*aa* min awwali yawmin a*h*aqqu an taquuma fiihi fiihi rij*aa*lun yu*h*ibbuuna an yata*th*ahharuu wa**al**l*aa*hu yu*h*ibb

Janganlah engkau melaksanakan salat dalam masjid itu selama-lamanya. Sungguh, masjid yang didirikan atas dasar takwa, sejak hari pertama adalah lebih pantas engkau melaksanakan salat di dalamnya. Di dalamnya ada orang-orang yang ingin membersihkan diri. A

9:109

# اَفَمَنْ اَسَّسَ بُنْيَانَهٗ عَلٰى تَقْوٰى مِنَ اللّٰهِ وَرِضْوَانٍ خَيْرٌ اَمْ مَّنْ اَسَّسَ بُنْيَانَهٗ عَلٰى شَفَا جُرُفٍ هَارٍ فَانْهَارَ بِهٖ فِيْ نَارِ جَهَنَّمَۗ وَاللّٰهُ لَا يَهْدِى الْقَوْمَ الظّٰلِمِيْنَ

afaman assasa buny*aa*nahu 'al*aa* taqw*aa* mina **al**l*aa*hi wari*dh*w*aa*nin khayrun am man assasa buny*aa*nahu 'al*aa* syaf*aa* jurufin h*aa*rin fa**i**nh*aa*ra bihi fii

Maka apakah orang-orang yang mendirikan bangunan (masjid) atas dasar takwa kepada Allah dan keridaan(-Nya) itu lebih baik, ataukah orang-orang yang mendirikan bangunannya di tepi jurang yang runtuh, lalu (bangunan) itu roboh bersama-sama dengan dia ke dal

9:110

# لَا يَزَالُ بُنْيَانُهُمُ الَّذِيْ بَنَوْا رِيْبَةً فِيْ قُلُوْبِهِمْ اِلَّآ اَنْ تَقَطَّعَ قُلُوْبُهُمْۗ وَاللّٰهُ عَلِيْمٌ حَكِيْمٌ ࣖ

l*aa* yaz*aa*lu buny*aa*nuhumu **al**la*dz*ii banaw riibatan fii quluubihim ill*aa* an taqa*ththh*a'a quluubuhum wa**al**l*aa*hu 'aliimun *h*akiim**un**

Bangunan yang mereka dirikan itu senantiasa menjadi penyebab keraguan dalam hati mereka, sampai hati mereka hancur. Dan Allah Maha Mengetahui, Mahabijaksana.

9:111

# ۞ اِنَّ اللّٰهَ اشْتَرٰى مِنَ الْمُؤْمِنِيْنَ اَنْفُسَهُمْ وَاَمْوَالَهُمْ بِاَنَّ لَهُمُ الْجَنَّةَۗ يُقَاتِلُوْنَ فِيْ سَبِيْلِ اللّٰهِ فَيَقْتُلُوْنَ وَيُقْتَلُوْنَ وَعْدًا عَلَيْهِ حَقًّا فِى التَّوْرٰىةِ وَالْاِنْجِيْلِ وَالْقُرْاٰنِۗ وَمَنْ اَوْفٰى

inna **al**l*aa*ha isytar*aa* mina **a**lmu/miniina anfusahum wa-amw*aa*lahum bi-anna lahumu **a**ljannata yuq*aa*tiluuna fii sabiili **al**l*aa*hi fayaqtuluuna wayuqtaluuna wa'd

Sesungguhnya Allah membeli dari orang-orang mukmin, baik diri mau-pun harta mereka dengan memberikan surga untuk mereka. Mereka berperang di jalan Allah; sehingga mereka membunuh atau terbunuh, (sebagai) janji yang benar dari Allah di dalam Taurat, Injil,

9:112

# اَلتَّاۤىِٕبُوْنَ الْعٰبِدُوْنَ الْحَامِدُوْنَ السَّاۤىِٕحُوْنَ الرَّاكِعُوْنَ السَّاجِدُوْنَ الْاٰمِرُوْنَ بِالْمَعْرُوْفِ وَالنَّاهُوْنَ عَنِ الْمُنْكَرِ وَالْحٰفِظُوْنَ لِحُدُوْدِ اللّٰهِ ۗوَبَشِّرِ الْمُؤْمِنِيْنَ

a**l**tt*aa*-ibuuna **a**l'*aa*biduuna **a**l*haa*miduuna **al**ss*aa*-i*h*uuna **al**rr*aa*ki'uuna **al**ss*aa*jiduuna **a**l-

Mereka itu adalah orang-orang yang bertobat, beribadah, memuji (Allah), mengembara (demi ilmu dan agama), rukuk, sujud, menyuruh berbuat makruf dan mencegah dari yang mungkar dan yang memelihara hukum-hukum Allah. Dan gembirakanlah orang-orang yang berima

9:113

# مَا كَانَ لِلنَّبِيِّ وَالَّذِيْنَ اٰمَنُوْٓا اَنْ يَّسْتَغْفِرُوْا لِلْمُشْرِكِيْنَ وَلَوْ كَانُوْٓا اُولِيْ قُرْبٰى مِنْۢ بَعْدِ مَا تَبَيَّنَ لَهُمْ اَنَّهُمْ اَصْحٰبُ الْجَحِيْمِ

m*aa* k*aa*na li**l**nnabiyyi wa**a**lla*dz*iina *aa*manuu an yastaghfiruu lilmusyrikiina walaw k*aa*nuu ulii qurb*aa* min ba'di m*aa* tabayyana lahum annahum a*sh*-*haa*bu **a**

**Tidak pantas bagi Nabi dan orang-orang yang beriman memohonkan ampunan (kepada Allah) bagi orang-orang musyrik, sekalipun orang-orang itu kaum kerabat(nya), setelah jelas bagi mereka, bahwa orang-orang musyrik itu penghuni neraka Jahanam.**









9:114

# وَمَا كَانَ اسْتِغْفَارُ اِبْرٰهِيْمَ لِاَبِيْهِ اِلَّا عَنْ مَّوْعِدَةٍ وَّعَدَهَآ اِيَّاهُۚ فَلَمَّا تَبَيَّنَ لَهٗٓ اَنَّهٗ عَدُوٌّ لِّلّٰهِ تَبَرَّاَ مِنْهُۗ اِنَّ اِبْرٰهِيْمَ لَاَوَّاهٌ حَلِيْمٌ

wam*aa* k*aa*na istighf*aa*ru ibr*aa*hiima li-abiihi ill*aa* 'an maw'idatin wa'adah*aa* iyy*aa*hu falamm*aa* tabayyana lahu annahu 'aduwwun lill*aa*hi tabarra-a minhu inna ibr*aa*hiima la-aww*aa*hun <

Adapun permohonan ampunan Ibrahim (kepada Allah) untuk bapaknya, tidak lain hanyalah karena suatu janji yang telah diikrarkannya kepada bapaknya. Maka ketika jelas bagi Ibrahim bahwa bapaknya adalah musuh Allah, maka Ibrahim berlepas diri darinya. Sungguh

9:115

# وَمَا كَانَ اللّٰهُ لِيُضِلَّ قَوْمًاۢ بَعْدَ اِذْ هَدٰىهُمْ حَتّٰى يُبَيِّنَ لَهُمْ مَّا يَتَّقُوْنَۗ اِنَّ اللّٰهَ بِكُلِّ شَيْءٍ عَلِيْمٌ

wam*aa* k*aa*na **al**l*aa*hu liyu*dh*illa qawman ba'da i*dz* had*aa*hum *h*att*aa* yubayyina lahum m*aa* yattaquuna inna **al**l*aa*ha bikulli syay-in 'aliim**un**

Dan Allah sekali-kali tidak akan menyesatkan suatu kaum, setelah mereka diberi-Nya petunjuk, sehingga dapat dijelaskan kepada mereka apa yang harus mereka jauhi. Sungguh, Allah Maha Mengetahui segala sesuatu.

9:116

# اِنَّ اللّٰهَ لَهٗ مُلْكُ السَّمٰوٰتِ وَالْاَرْضِۗ يُحْيٖ وَيُمِيْتُۗ وَمَا لَكُمْ مِّنْ دُوْنِ اللّٰهِ مِنْ وَّلِيٍّ وَّلَا نَصِيْرٍ

inna **al**l*aa*ha lahu mulku **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i yu*h*yii wayumiitu wam*aa* lakum min duuni **al**l*aa*hi min waliyyin wal*aa* na*sh*iir

Sesungguhnya Allah memiliki kekuasaan langit dan bumi. Dia menghidupkan dan mematikan. Tidak ada pelindung dan penolong bagimu selain Allah.

9:117

# لَقَدْ تَّابَ اللّٰهُ عَلَى النَّبِيِّ وَالْمُهٰجِرِيْنَ وَالْاَنْصَارِ الَّذِيْنَ اتَّبَعُوْهُ فِيْ سَاعَةِ الْعُسْرَةِ مِنْۢ بَعْدِ مَا كَادَ يَزِيْغُ قُلُوْبُ فَرِيْقٍ مِّنْهُمْ ثُمَّ تَابَ عَلَيْهِمْۗ اِنَّهٗ بِهِمْ رَءُوْفٌ رَّحِيْمٌ ۙ

laqad t*aa*ba **al**l*aa*hu 'al*aa* **al**nnabiyyi wa**a**lmuh*aa*jiriina wa**a**l-an*shaa*ri **al**la*dz*iina ittaba'uuhu fii s*aa*'ati **a**

Sungguh, Allah telah menerima tobat Nabi, orang-orang Muhajirin dan orang-orang Ansar, yang mengikuti Nabi pada masa-masa sulit, setelah hati segolongan dari mereka hampir berpaling, kemudian Allah menerima tobat mereka. Sesungguhnya Allah Maha Pengasih,

9:118

# وَّعَلَى الثَّلٰثَةِ الَّذِيْنَ خُلِّفُوْاۗ حَتّٰٓى اِذَا ضَاقَتْ عَلَيْهِمُ الْاَرْضُ بِمَا رَحُبَتْ وَضَاقَتْ عَلَيْهِمْ اَنْفُسُهُمْ وَظَنُّوْٓا اَنْ لَّا مَلْجَاَ مِنَ اللّٰهِ اِلَّآ اِلَيْهِۗ ثُمَّ تَابَ عَلَيْهِمْ لِيَتُوْبُوْاۗ اِنَّ اللّٰهَ هُوَ

wa'al*aa* **al**tstsal*aa*tsati **al**la*dz*iina khullifuu *h*att*aa* i*dzaa* *daa*qat 'alayhimu **a**l-ar*dh*u bim*aa* ra*h*ubat wa*daa*qat 'alayhim anfusuhum w

dan terhadap tiga orang yang ditinggalkan. Hingga ketika bumi terasa sempit bagi mereka, padahal bumi itu luas dan jiwa mereka pun telah (pula terasa) sempit bagi mereka, serta mereka telah mengetahui bahwa tidak ada tempat lari dari (siksaan) Allah, mela

9:119

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوا اتَّقُوا اللّٰهَ وَكُوْنُوْا مَعَ الصّٰدِقِيْنَ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu ittaquu **al**l*aa*ha wakuunuu ma'a **al***shshaa*diqiin**a**

Wahai orang-orang yang beriman! Bertakwalah kepada Allah, dan bersamalah kamu dengan orang-orang yang benar.

9:120

# مَا كَانَ لِاَهْلِ الْمَدِيْنَةِ وَمَنْ حَوْلَهُمْ مِّنَ الْاَعْرَابِ اَنْ يَّتَخَلَّفُوْا عَنْ رَّسُوْلِ اللّٰهِ وَلَا يَرْغَبُوْا بِاَنْفُسِهِمْ عَنْ نَّفْسِهٖۗ ذٰلِكَ بِاَنَّهُمْ لَا يُصِيْبُهُمْ ظَمَاٌ وَّلَا نَصَبٌ وَّلَا مَخْمَصَةٌ فِيْ سَبِيْلِ الل

m*aa* k*aa*na li-ahli **a**lmadiinati waman *h*awlahum mina **a**l-a'r*aa*bi an yatakhallafuu 'an rasuuli **al**l*aa*hi wal*aa* yarghabuu bi-anfusihim 'an nafsihi *dzaa*lika bi-anna

Tidak pantas bagi penduduk Madinah dan orang-orang Arab Badui yang berdiam di sekitar mereka, tidak turut menyertai Rasulullah (pergi berperang) dan tidak pantas (pula) bagi mereka lebih mencintai diri mereka daripada (mencintai) diri Rasul. Yang demikian

9:121

# وَلَا يُنْفِقُوْنَ نَفَقَةً صَغِيْرَةً وَّلَا كَبِيْرَةً وَّلَا يَقْطَعُوْنَ وَادِيًا اِلَّا كُتِبَ لَهُمْ لِيَجْزِيَهُمُ اللّٰهُ اَحْسَنَ مَا كَانُوْا يَعْمَلُوْنَ

wal*aa* yunfiquuna nafaqatan *sh*aghiiratan wal*aa* kabiiratan wal*aa* yaq*th*a'uuna w*aa*diyan ill*aa* kutiba lahum liyajziyahumu **al**l*aa*hu a*h*sana m*aa* k*aa*nuu ya'maluun**a**

dan tidaklah mereka memberikan infak, baik yang kecil maupun yang besar dan tidak (pula) melintasi suatu lembah (berjihad), kecuali akan dituliskan bagi mereka (sebagai amal kebajikan), untuk diberi balasan oleh Allah (dengan) yang lebih baik daripada apa







9:122

# ۞ وَمَا كَانَ الْمُؤْمِنُوْنَ لِيَنْفِرُوْا كَاۤفَّةًۗ فَلَوْلَا نَفَرَ مِنْ كُلِّ فِرْقَةٍ مِّنْهُمْ طَاۤىِٕفَةٌ لِّيَتَفَقَّهُوْا فِى الدِّيْنِ وَلِيُنْذِرُوْا قَوْمَهُمْ اِذَا رَجَعُوْٓا اِلَيْهِمْ لَعَلَّهُمْ يَحْذَرُوْنَ ࣖ

wam*aa* k*aa*na **a**lmu/minuuna liyanfiruu k*aa*ffatan falawl*aa* nafara min kulli firqatin minhum *thaa*-ifatun liyatafaqqahuu fii **al**ddiini waliyun*dz*iruu qawmahum i*dzaa* raja'uu

Dan tidak sepatutnya orang-orang mukmin itu semuanya pergi (ke medan perang). Mengapa sebagian dari setiap golongan di antara mereka tidak pergi untuk memperdalam pengetahuan agama mereka dan untuk memberi peringatan kepada kaumnya apabila mereka telah ke

9:123

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْا قَاتِلُوا الَّذِيْنَ يَلُوْنَكُمْ مِّنَ الْكُفَّارِ وَلْيَجِدُوْا فِيْكُمْ غِلْظَةًۗ وَاعْلَمُوْٓا اَنَّ اللّٰهَ مَعَ الْمُتَّقِيْنَ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu q*aa*tiluu **al**la*dz*iina yaluunakum mina **a**lkuff*aa*ri walyajiduu fiikum ghil*zh*atan wa**i**'lamuu anna **a**

Wahai orang yang beriman! Perangilah orang-orang kafir yang di sekitar kamu, dan hendaklah mereka merasakan sikap tegas darimu, dan ketahuilah bahwa Allah beserta orang yang bertakwa.







9:124

# وَاِذَا مَآ اُنْزِلَتْ سُوْرَةٌ فَمِنْهُمْ مَّنْ يَّقُوْلُ اَيُّكُمْ زَادَتْهُ هٰذِهٖٓ اِيْمَانًاۚ فَاَمَّا الَّذِيْنَ اٰمَنُوْا فَزَادَتْهُمْ اِيْمَانًا وَّهُمْ يَسْتَبْشِرُوْنَ

wa-i*dzaa* m*aa* unzilat suuratun faminhum man yaquulu ayyukum z*aa*dat-hu h*aadz*ihi iim*aa*nan fa-amm*aa* **al**la*dz*iina *aa*manuu faz*aa*dat-hum iim*aa*nan wahum yastabsyiruun**a**

**Dan apabila diturunkan suatu surah, maka di antara mereka (orang-orang munafik) ada yang berkata, “Siapakah di antara kamu yang bertambah imannya dengan (turunnya) surah ini?” Adapun orang-orang yang beriman, maka surah ini menambah imannya, dan mereka me**









9:125

# وَاَمَّا الَّذِيْنَ فِيْ قُلُوْبِهِمْ مَّرَضٌ فَزَادَتْهُمْ رِجْسًا اِلٰى رِجْسِهِمْ وَمَاتُوْا وَهُمْ كٰفِرُوْنَ

wa-amm*aa* **al**la*dz*iina fii quluubihim mara*dh*un faz*aa*dat-hum rijsan il*aa* rijsihim wam*aa*tuu wahum k*aa*firuun**a**

Dan adapun orang-orang yang di dalam hatinya ada penyakit, maka (dengan surah itu) akan menambah kekafiran mereka yang telah ada dan mereka akan mati dalam keadaan kafir.

9:126

# اَوَلَا يَرَوْنَ اَنَّهُمْ يُفْتَنُوْنَ فِيْ كُلِّ عَامٍ مَّرَّةً اَوْ مَرَّتَيْنِ ثُمَّ لَا يَتُوْبُوْنَ وَلَا هُمْ يَذَّكَّرُوْنَ

awa l*aa* yarawna annahum yuftanuuna fii kulli '*aa*min marratan aw marratayni tsumma l*aa* yatuubuuna wal*aa* hum ya*dzdz*akkaruun**a**

Dan tidakkah mereka (orang-orang munafik) memperhatikan bahwa mereka diuji sekali atau dua kali setiap tahun, namun mereka tidak (juga) bertobat dan tidak (pula) mengambil pelajaran?

9:127

# وَاِذَا مَآ اُنْزِلَتْ سُوْرَةٌ نَّظَرَ بَعْضُهُمْ اِلٰى بَعْضٍۗ هَلْ يَرٰىكُمْ مِّنْ اَحَدٍ ثُمَّ انْصَرَفُوْاۗ صَرَفَ اللّٰهُ قُلُوْبَهُمْ بِاَنَّهُمْ قَوْمٌ لَّا يَفْقَهُوْنَ

wa-i*dzaa* m*aa* unzilat suuratun na*zh*ara ba'*dh*uhum il*aa* ba'*dh*in hal yar*aa*kum min a*h*adin tsumma in*sh*arafuu *sh*arafa **al**l*aa*hu quluubahum bi-annahum qawmun l*aa* ya

Dan apabila diturunkan suatu surah, satu sama lain di antara mereka saling berpandangan (sambil berkata), “Adakah seseorang (dari kaum muslimin) yang melihat kamu?” Setelah itu mereka pun pergi. Allah memalingkan hati mereka disebabkan mereka adalah kaum

9:128

# لَقَدْ جَاۤءَكُمْ رَسُوْلٌ مِّنْ اَنْفُسِكُمْ عَزِيْزٌ عَلَيْهِ مَا عَنِتُّمْ حَرِيْصٌ عَلَيْكُمْ بِالْمُؤْمِنِيْنَ رَءُوْفٌ رَّحِيْمٌ

laqad j*aa*-akum rasuulun min anfusikum 'aziizun 'alayhi m*aa* 'anittum *h*arii*sh*un 'alaykum bi**a**lmu/miniina rauufun ra*h*iim**un**

Sungguh, telah datang kepadamu seorang rasul dari kaummu sendiri, berat terasa olehnya penderitaan yang kamu alami, (dia) sangat menginginkan (keimanan dan keselamatan) bagimu, penyantun dan penyayang terhadap orang-orang yang beriman.

9:129

# فَاِنْ تَوَلَّوْا فَقُلْ حَسْبِيَ اللّٰهُ لَآ اِلٰهَ اِلَّا هُوَ ۗ عَلَيْهِ تَوَكَّلْتُ وَهُوَ رَبُّ الْعَرْشِ الْعَظِيْمِ ࣖ

fa-in tawallaw faqul *h*asbiya **al**l*aa*hu l*aa* il*aa*ha ill*aa* huwa 'alayhi tawakkaltu wahuwa rabbu **a**l'arsyi **a**l'a*zh*iim**i**

Maka jika mereka berpaling (dari keimanan), maka katakanlah (Muhammad), “Cukuplah Allah bagiku; tidak ada tuhan selain Dia. Hanya kepada-Nya aku bertawakal, dan Dia adalah Tuhan yang memiliki ‘Arsy (singgasana) yang agung.”

<!--EndFragment-->