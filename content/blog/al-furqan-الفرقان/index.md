---
title: (25) Al-Furqan - الفرقان
date: 2021-10-27T03:52:32.143Z
ayat: 25
description: "Jumlah Ayat: 77 / Arti: Pembeda"
---
<!--StartFragment-->

25:1

# تَبٰرَكَ الَّذِيْ نَزَّلَ الْفُرْقَانَ عَلٰى عَبْدِهٖ لِيَكُوْنَ لِلْعٰلَمِيْنَ نَذِيْرًا ۙ

tab*aa*raka **al**la*dz*ii nazzala **a**lfurq*aa*na 'al*aa* 'abdihi liyakuuna lil'*aa*lamiina na*dz*iir*aa***n**

Mahasuci Allah yang telah menurunkan Furqan (Al-Qur'an) kepada hamba-Nya (Muhammad), agar dia menjadi pemberi peringatan kepada seluruh alam (jin dan manusia).

25:2

# ۨالَّذِيْ لَهٗ مُلْكُ السَّمٰوٰتِ وَالْاَرْضِ وَلَمْ يَتَّخِذْ وَلَدًا وَّلَمْ يَكُنْ لَّهٗ شَرِيْكٌ فِى الْمُلْكِ وَخَلَقَ كُلَّ شَيْءٍ فَقَدَّرَهٗ تَقْدِيْرًا

**al**la*dz*ii lahu mulku **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i walam yattakhi*dz* waladan walam yakun lahu syariikun fii **a**lmulki wakhalaqa kulla syay-in faqaddarahu taqdiir

Yang memiliki kerajaan langit dan bumi, tidak mempunyai anak, tidak ada sekutu bagi-Nya dalam kekuasaan(-Nya), dan Dia menciptakan segala sesuatu, lalu menetapkan ukuran-ukurannya dengan tepat.

25:3

# وَاتَّخَذُوْا مِنْ دُوْنِهٖٓ اٰلِهَةً لَّا يَخْلُقُوْنَ شَيْـًٔا وَّهُمْ يُخْلَقُوْنَ وَلَا يَمْلِكُوْنَ لِاَنْفُسِهِمْ ضَرًّا وَّلَا نَفْعًا وَّلَا يَمْلِكُوْنَ مَوْتًا وَّلَا حَيٰوةً وَّلَا نُشُوْرًا

wa**i**ttakha*dz*uu min duunihi *aa*lihatan l*aa* yakhluquuna syay-an wahum yukhlaquuna wal*aa* yamlikuuna li-anfusihim *dh*arran wal*aa* naf'an wal*aa* yamlikuuna mawtan wal*aa* *h*ay*aa*tan

Namun mereka mengambil tuhan-tuhan selain Dia (untuk disembah), padahal mereka (tuhan-tuhan itu) tidak menciptakan apa pun, bahkan mereka sendiri diciptakan dan tidak kuasa untuk (menolak) bahaya terhadap dirinya dan tidak dapat (mendatangkan) manfaat ser

25:4

# وَقَالَ الَّذِيْنَ كَفَرُوْٓا اِنْ هٰذَآ اِلَّآ اِفْكُ ِۨافْتَرٰىهُ وَاَعَانَهٗ عَلَيْهِ قَوْمٌ اٰخَرُوْنَۚ فَقَدْ جَاۤءُوْ ظُلْمًا وَّزُوْرًا ۚ

waq*aa*la **al**la*dz*iina kafaruu in h*aadzaa* ill*aa* ifkun iftar*aa*hu wa-a'*aa*nahu 'alayhi qawmun *aa*kharuuna faqad j*aa*uu *zh*ulman wazuur*aa***n**

Dan orang-orang kafir berkata, “(Al-Qur'an) ini tidak lain hanyalah kebohongan yang diada-adakan oleh dia (Muhammad), dibantu oleh orang-orang lain,” Sungguh, mereka telah berbuat zalim dan dusta yang besar.

25:5

# وَقَالُوْٓا اَسَاطِيْرُ الْاَوَّلِيْنَ اكْتَتَبَهَا فَهِيَ تُمْلٰى عَلَيْهِ بُكْرَةً وَّاَصِيْلًا

waq*aa*luu as*aath*iiru **a**l-awwaliina iktatabah*aa* fahiya tuml*aa* 'alayhi bukratan wa-a*sh*iil*aa***n**

Dan mereka berkata, “(Itu hanya) dongeng-dongeng orang-orang terdahulu, yang diminta agar dituliskan, lalu dibacakanlah dongeng itu kepadanya setiap pagi dan petang.”

25:6

# قُلْ اَنْزَلَهُ الَّذِيْ يَعْلَمُ السِّرَّ فِى السَّمٰوٰتِ وَالْاَرْضِۗ اِنَّهٗ كَانَ غَفُوْرًا رَّحِيْمًا

qul anzalahu **al**la*dz*ii ya'lamu **al**ssirra fii **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i innahu k*aa*na ghafuuran ra*h*iim*aa***n**

Katakanlah (Muhammad), “(Al-Qur'an) itu diturunkan oleh (Allah) yang mengetahui rahasia di langit dan di bumi. Sungguh, Dia Maha Pengampun, Maha Penyayang.”

25:7

# وَقَالُوْا مَالِ هٰذَا الرَّسُوْلِ يَأْكُلُ الطَّعَامَ وَيَمْشِيْ فِى الْاَسْوَاقِۗ لَوْلَآ اُنْزِلَ اِلَيْهِ مَلَكٌ فَيَكُوْنَ مَعَهٗ نَذِيْرًا ۙ

waq*aa*luu m*aa*li h*aadzaa* **al**rrasuuli ya/kulu **al***ththh*a'*aa*ma wayamsyii fii **a**l-asw*aa*qi lawl*aa* unzila ilayhi malakun fayakuuna ma'ahu na*dz*iir*aa*

Dan mereka berkata, “Mengapa Rasul (Muhammad) ini memakan makanan dan berjalan di pasar-pasar? Mengapa malaikat tidak diturunkan kepadanya (agar malaikat) itu memberikan peringatan bersama dia,

25:8

# اَوْ يُلْقٰىٓ اِلَيْهِ كَنْزٌ اَوْ تَكُوْنُ لَهٗ جَنَّةٌ يَّأْكُلُ مِنْهَاۗ وَقَالَ الظّٰلِمُوْنَ اِنْ تَتَّبِعُوْنَ اِلَّا رَجُلًا مَّسْحُوْرًا

aw yulq*aa* ilayhi kanzun aw takuunu lahu jannatun ya/kulu minh*aa* waq*aa*la **al***zhzhaa*limuuna in tattabi'uuna ill*aa* rajulan mas*h*uur*aa***n**

atau (mengapa tidak) diturunkan kepadanya harta kekayaan atau (mengapa tidak ada) kebun baginya, sehingga dia dapat makan dari (hasil)nya?” Dan orang-orang zalim itu berkata, “Kamu hanyalah mengikuti seorang laki-laki yang kena sihir.”

25:9

# اُنْظُرْ كَيْفَ ضَرَبُوْا لَكَ الْاَمْثَالَ فَضَلُّوْا فَلَا يَسْتَطِيْعُوْنَ سَبِيْلًا ࣖ

un*zh*ur kayfa *dh*arabuu laka **a**l-amts*aa*la fa*dh*alluu fal*aa* yasta*th*ii'uuna sabiil*aa***n**

Perhatikanlah, bagaimana mereka membuat perumpamaan-perumpamaan tentang engkau, maka sesatlah mereka, mereka tidak sanggup (mendapatkan) jalan (untuk menentang kerasulanmu).

25:10

# تَبٰرَكَ الَّذِيْٓ اِنْ شَاۤءَ جَعَلَ لَكَ خَيْرًا مِّنْ ذٰلِكَ جَنّٰتٍ تَجْرِيْ مِنْ تَحْتِهَا الْاَنْهٰرُۙ وَيَجْعَلْ لَّكَ قُصُوْرًا

tab*aa*raka **al**la*dz*ii in sy*aa*-a ja'ala laka khayran min *dzaa*lika jann*aa*tin tajrii min ta*h*tih*aa* **a**l-anh*aa*ru wayaj'al laka qu*sh*uur*aa***n**

Mahasuci (Allah) yang jika Dia menghendaki, niscaya Dia jadikan bagimu yang lebih baik daripada itu, (yaitu) surga-surga yang mengalir di bawahnya sungai-sungai, dan Dia jadikan (pula) istana-istana untukmu.

25:11

# بَلْ كَذَّبُوْا بِالسَّاعَةِۙ وَاَعْتَدْنَا لِمَنْ كَذَّبَ بِالسَّاعَةِ سَعِيْرًا

bal ka*dzdz*abuu bi**al**ss*aa*'ati wa-a'tadn*aa* liman ka*dzdz*aba bi**al**ss*aa*'ati sa'iir*aa***n**

Bahkan mereka mendustakan hari Kiamat. Dan Kami menyediakan neraka yang menyala-nyala bagi siapa yang mendustakan hari Kiamat.

25:12

# اِذَا رَاَتْهُمْ مِّنْ مَّكَانٍۢ بَعِيْدٍ سَمِعُوْا لَهَا تَغَيُّظًا وَّزَفِيْرًا

i*dzaa* ra-at-hum min mak*aa*nin ba'iidin sami'uu lah*aa* taghayyu*zh*an wazafiir*aa***n**

Apabila ia (neraka) melihat mereka dari tempat yang jauh, mereka mendengar suaranya yang gemuruh karena marahnya.

25:13

# وَاِذَآ اُلْقُوْا مِنْهَا مَكَانًا ضَيِّقًا مُّقَرَّنِيْنَ دَعَوْا هُنَالِكَ ثُبُوْرًا ۗ

wa-i*dzaa* ulquu minh*aa* mak*aa*nan *dh*ayyiqan muqarraniina da'aw hun*aa*lika tsubuur*aa***n**

Dan apabila mereka dilemparkan ke tempat yang sempit di neraka dengan dibelenggu, mereka di sana berteriak mengharapkan kebinasaan.

25:14

# لَا تَدْعُوا الْيَوْمَ ثُبُوْرًا وَّاحِدًا وَّادْعُوْا ثُبُوْرًا كَثِيْرًا

l*aa* tad'uu **a**lyawma tsubuuran w*aah*idan wa**u**d'uu tsubuuran katsiir*aa***n**

(Akan dikatakan kepada mereka), “Janganlah kamu mengharapkan pada hari ini satu kebinasaan, melainkan harapkanlah kebinasaan yang berulang-ulang.”

25:15

# قُلْ اَذٰلِكَ خَيْرٌ اَمْ جَنَّةُ الْخُلْدِ الَّتِيْ وُعِدَ الْمُتَّقُوْنَۗ كَانَتْ لَهُمْ جَزَاۤءً وَّمَصِيْرًا

qul a*dzaa*lika khayrun am jannatu **a**lkhuldi **al**latii wu'ida **a**lmuttaquuna k*aa*nat lahum jaz*aa*-an wama*sh*iir*aa***n**

Katakanlah (Muhammad), “Apakah (azab) seperti itu yang baik, atau surga yang kekal yang dijanjikan kepada orang-orang yang bertakwa sebagai balasan, dan tempat kembali bagi mereka?”

25:16

# لَهُمْ فِيْهَا مَا يَشَاۤءُوْنَ خٰلِدِيْنَۗ كَانَ عَلٰى رَبِّكَ وَعْدًا مَّسْـُٔوْلًا

lahum fiih*aa* m*aa* yasy*aa*uuna kh*aa*lidiina k*aa*na 'al*aa* rabbika wa'dan mas-uul*aa***n**

Bagi mereka segala yang mereka kehendaki ada di dalamnya (surga), mereka kekal (di dalamnya). Itulah janji Tuhanmu yang pantas dimohonkan (kepada-Nya).

25:17

# وَيَوْمَ يَحْشُرُهُمْ وَمَا يَعْبُدُوْنَ مِنْ دُوْنِ اللّٰهِ فَيَقُوْلُ ءَاَنْتُمْ اَضْلَلْتُمْ عِبَادِيْ هٰٓؤُلَاۤءِ اَمْ هُمْ ضَلُّوا السَّبِيْلَ ۗ

wayawma ya*h*syuruhum wam*aa* ya'buduuna min duuni **al**l*aa*hi fayaquulu a-antum a*dh*laltum 'ib*aa*dii h*aa*ul*aa*-i am hum *dh*alluu **al**ssabiil**a**

Dan (ingatlah) pada hari (ketika) Allah mengumpulkan mereka bersama apa yang mereka sembah selain Allah, lalu Dia berfirman (kepada yang disembah), “Apakah kamu yang menyesatkan hamba-hamba-Ku itu, atau mereka sendirikah yang sesat dari jalan (yang benar)

25:18

# قَالُوْا سُبْحٰنَكَ مَا كَانَ يَنْۢبَغِيْ لَنَآ اَنْ نَّتَّخِذَ مِنْ دُوْنِكَ مِنْ اَوْلِيَاۤءَ وَلٰكِنْ مَّتَّعْتَهُمْ وَاٰبَاۤءَهُمْ حَتّٰى نَسُوا الذِّكْرَۚ وَكَانُوْا قَوْمًاۢ بُوْرًا

q*aa*luu sub*haa*naka m*aa* k*aa*na yanbaghii lan*aa* an nattakhi*dz*a min duunika min awliy*aa*-a wal*aa*kin matta'tahum wa*aa*b*aa*-ahum *h*att*aa* nasuu **al***dzdz*ikra wak<

Mereka (yang disembah itu) menjawab, “Mahasuci Engkau, tidaklah pantas bagi kami mengambil pelindung selain Engkau, tetapi Engkau telah memberi mereka dan nenek moyang mereka kenikmatan hidup, sehingga mereka melupakan peringatan; dan mereka kaum yang bin

25:19

# فَقَدْ كَذَّبُوْكُمْ بِمَا تَقُوْلُوْنَۙ فَمَا تَسْتَطِيْعُوْنَ صَرْفًا وَّلَا نَصْرًاۚ وَمَنْ يَّظْلِمْ مِّنْكُمْ نُذِقْهُ عَذَابًا كَبِيْرًا

faqad ka*dzdz*abuukum bim*aa* taquuluuna fam*aa* tasta*th*ii'uuna *sh*arfan wal*aa* na*sh*ran waman ya*zh*lim minkum nu*dz*iqhu 'a*dzaa*ban kabiir*aa***n**

Maka sungguh, mereka (yang disembah itu) telah mengingkari apa yang kamu katakan, maka kamu tidak akan dapat menolak (azab) dan tidak dapat (pula) menolong (dirimu), dan barangsiapa di antara kamu berbuat zalim, niscaya Kami timpakan kepadanya rasa azab y

25:20

# وَمَآ اَرْسَلْنَا قَبْلَكَ مِنَ الْمُرْسَلِيْنَ اِلَّآ اِنَّهُمْ لَيَأْكُلُوْنَ الطَّعَامَ وَيَمْشُوْنَ فِى الْاَسْوَاقِۗ وَجَعَلْنَا بَعْضَكُمْ لِبَعْضٍ فِتْنَةً ۗ اَتَصْبِرُوْنَۚ وَكَانَ رَبُّكَ بَصِيْرًا ࣖ ۔

wam*aa* arsaln*aa* qablaka mina **a**lmursaliina ill*aa* innahum laya/kuluuna **al***ththh*a'*aa*ma wayamsyuuna fii **a**l-asw*aa*qi waja'aln*aa* ba'*dh*akum liba'*dh*in f

Dan Kami tidak mengutus rasul-rasul sebelummu (Muhammad), melainkan mereka pasti memakan makanan dan berjalan di pasar-pasar. Dan Kami jadikan sebagian kamu sebagai cobaan bagi sebagian yang lain. Maukah kamu bersabar? Dan Tuhanmu Maha Melihat.

25:21

# ۞ وَقَالَ الَّذِيْنَ لَا يَرْجُوْنَ لِقَاۤءَنَا لَوْلَآ اُنْزِلَ عَلَيْنَا الْمَلٰۤىِٕكَةُ اَوْ نَرٰى رَبَّنَا ۗ لَقَدِ اسْتَكْبَرُوْا فِيْٓ اَنْفُسِهِمْ وَعَتَوْ عُتُوًّا كَبِيْرًا

waq*aa*la **al**la*dz*iina l*aa* yarjuuna liq*aa*-an*aa* lawl*aa* unzila 'alayn*aa* **a**lmal*aa*-ikatu aw nar*aa* rabban*aa* laqadi istakbaruu fii anfusihim wa'ataw 'utuwwan kabi

Dan orang-orang yang tidak mengharapkan pertemuan dengan Kami (di akhirat) berkata, “Mengapa bukan para malaikat yang diturunkan kepada kita atau (mengapa) kita (tidak) melihat Tuhan kita?” Sungguh, mereka telah menyombongkan diri mereka dan benar-benar t

25:22

# يَوْمَ يَرَوْنَ الْمَلٰۤىِٕكَةَ لَا بُشْرٰى يَوْمَىِٕذٍ لِّلْمُجْرِمِيْنَ وَيَقُوْلُوْنَ حِجْرًا مَّحْجُوْرًا

yawma yarawna **a**lmal*aa*-ikata l*aa* busyr*aa* yawma-i*dz*in lilmujrimiina wayaquuluuna *h*ijran ma*h*juur*aa***n**

(Ingatlah) pada hari (ketika) mereka melihat para malaikat, pada hari itu tidak ada kabar gembira bagi orang-orang yang berdosa dan mereka berkata, “Hijran mahjura.”

25:23

# وَقَدِمْنَآ اِلٰى مَا عَمِلُوْا مِنْ عَمَلٍ فَجَعَلْنٰهُ هَبَاۤءً مَّنْثُوْرًا

waqadimn*aa* il*aa* m*aa* 'amiluu min 'amalin faja'aln*aa*hu hab*aa*-an mantsuur*aa***n**

Dan Kami akan perlihatkan segala amal yang mereka kerjakan, lalu Kami akan jadikan amal itu (bagaikan) debu yang beterbangan.

25:24

# اَصْحٰبُ الْجَنَّةِ يَوْمَىِٕذٍ خَيْرٌ مُّسْتَقَرًّا وَّاَحْسَنُ مَقِيْلًا

a*sh*-*haa*bu **a**ljannati yawma-i*dz*in khayrun mustaqarran wa-a*h*sanu maqiil*aa***n**

Penghuni-penghuni surga pada hari itu paling baik tempat tinggalnya dan paling indah tempat istirahatnya.

25:25

# وَيَوْمَ تَشَقَّقُ السَّمَاۤءُ بِالْغَمَامِ وَنُزِّلَ الْمَلٰۤىِٕكَةُ تَنْزِيْلًا

wayawma tasyaqqaqu **al**ssam*aa*u bi**a**lgham*aa*mi wanuzzila **a**lmal*aa*-ikatu tanziil*aa***n**

Dan (ingatlah) pada hari (ketika) langit pecah mengeluarkan kabut putih dan para malaikat diturunkan (secara) bergelombang.

25:26

# اَلْمُلْكُ يَوْمَىِٕذِ ِۨالْحَقُّ لِلرَّحْمٰنِۗ وَكَانَ يَوْمًا عَلَى الْكٰفِرِيْنَ عَسِيْرًا

almulku yawma-i*dz*in **a**l*h*aqqu li**l**rra*h*m*aa*ni wak*aa*na yawman 'al*aa* **a**lk*aa*firiina 'asiir*aa***n**

Kerajaan yang hak pada hari itu adalah milik Tuhan Yang Maha Pengasih. Dan itulah hari yang sulit bagi orang-orang kafir.

25:27

# وَيَوْمَ يَعَضُّ الظَّالِمُ عَلٰى يَدَيْهِ يَقُوْلُ يٰلَيْتَنِى اتَّخَذْتُ مَعَ الرَّسُوْلِ سَبِيْلًا

wayawma ya'a*dhdh*u **al***zhzhaa*limu 'al*aa* yadayhi yaquulu y*aa* laytanii ittakha*dz*tu ma'a **al**rrasuuli sabiil*aa***n**

Dan (ingatlah) pada hari (ketika) orang-orang zalim menggigit dua jarinya, (menyesali perbuatannya) seraya berkata, “Wahai! Sekiranya (dulu) aku mengambil jalan bersama Rasul.

25:28

# يٰوَيْلَتٰى لَيْتَنِيْ لَمْ اَتَّخِذْ فُلَانًا خَلِيْلًا

y*aa* waylat*aa* laytanii lam attakhi*dz* ful*aa*nan khaliil*aa***n**

Wahai, celaka aku! Sekiranya (dulu) aku tidak menjadikan si fulan itu teman akrab(ku),

25:29

# لَقَدْ اَضَلَّنِيْ عَنِ الذِّكْرِ بَعْدَ اِذْ جَاۤءَنِيْۗ وَكَانَ الشَّيْطٰنُ لِلْاِنْسَانِ خَذُوْلًا

laqad a*dh*allanii 'ani **al***dzdz*ikri ba'da i*dz* j*aa*-anii wak*aa*na **al**sysyay*thaa*nu lil-ins*aa*ni kha*dz*uul*aa***n**

sungguh, dia telah menyesatkan aku dari peringatan (Al-Qur'an) ketika (Al-Qur'an) itu telah datang kepadaku. Dan setan memang pengkhianat manusia.”

25:30

# وَقَالَ الرَّسُوْلُ يٰرَبِّ اِنَّ قَوْمِى اتَّخَذُوْا هٰذَا الْقُرْاٰنَ مَهْجُوْرًا

waq*aa*la **al**rrasuulu y*aa* rabbi inna qawmii ittakha*dz*uu h*aadzaa* **a**lqur-*aa*na mahjuur*aa***n**

Dan Rasul (Muhammad) berkata, “Ya Tuhanku, sesungguhnya kaumku telah menjadikan Al-Qur'an ini diabaikan.”

25:31

# وَكَذٰلِكَ جَعَلْنَا لِكُلِّ نَبِيٍّ عَدُوًّا مِّنَ الْمُجْرِمِيْنَۗ وَكَفٰى بِرَبِّكَ هَادِيًا وَّنَصِيْرًا

waka*dzaa*lika ja'aln*aa* likulli nabiyyin 'aduwwan mina **a**lmujrimiina wakaf*aa* birabbika h*aa*diyan wana*sh*iir*aa***n**

Begitulah, bagi setiap nabi, telah Kami adakan musuh dari orang-orang yang berdosa. Tetapi cukuplah Tuhanmu menjadi pemberi petunjuk dan penolong.

25:32

# وَقَالَ الَّذِيْنَ كَفَرُوْا لَوْلَا نُزِّلَ عَلَيْهِ الْقُرْاٰنُ جُمْلَةً وَّاحِدَةً ۛ كَذٰلِكَ ۛ لِنُثَبِّتَ بِهٖ فُؤَادَكَ وَرَتَّلْنٰهُ تَرْتِيْلًا

waq*aa*la **al**la*dz*iina kafaruu lawl*aa* nuzzila 'alayhi **a**lqur-*aa*nu jumlatan w*aah*idatan ka*dzaa*lika linutsabbita bihi fu-*aa*daka warattaln*aa*hu tartiil*aa***n**

**Dan orang-orang kafir berkata, “Mengapa Al-Qur'an itu tidak diturunkan kepadanya sekaligus?” Demikianlah, agar Kami memperteguh hatimu (Muhammad) dengannya dan Kami membacakannya secara tartil (berangsur-angsur, perlahan dan benar).**









25:33

# وَلَا يَأْتُوْنَكَ بِمَثَلٍ اِلَّا جِئْنٰكَ بِالْحَقِّ وَاَحْسَنَ تَفْسِيْرًا ۗ

wal*aa* ya/tuunaka bimatsalin ill*aa* ji/n*aa*ka bi**a**l*h*aqqi wa-a*h*sana tafsiir*aa***n**

Dan mereka (orang-orang kafir itu) tidak datang kepadamu (membawa) sesuatu yang aneh, melainkan Kami datangkan kepadamu yang benar dan penjelasan yang paling baik.

25:34

# اَلَّذِيْنَ يُحْشَرُوْنَ عَلٰى وُجُوْهِهِمْ اِلٰى جَهَنَّمَۙ اُولٰۤىِٕكَ شَرٌّ مَّكَانًا وَّاَضَلُّ سَبِيْلًا ࣖ

**al**la*dz*iina yu*h*syaruuna 'al*aa* wujuuhihim il*aa* jahannama ul*aa*-ika syarrun mak*aa*nan wa-a*dh*allu sabiil*aa***n**

Orang-orang yang dikumpulkan ke neraka Jahanam dengan diseret wajahnya, mereka itulah yang paling buruk tempatnya dan paling sesat jalannya.

25:35

# وَلَقَدْ اٰتَيْنَا مُوْسَى الْكِتٰبَ وَجَعَلْنَا مَعَهٗٓ اَخَاهُ هٰرُوْنَ وَزِيْرًا ۚ

walaqad *aa*tayn*aa* muus*aa* **a**lkit*aa*ba waja'aln*aa* ma'ahu akh*aa*hu h*aa*ruuna waziir*aa***n**

Dan sungguh, Kami telah memberikan Kitab (Taurat) kepada Musa dan Kami telah menjadikan Harun saudaranya, menyertai dia sebagai wazir (pembantu).

25:36

# فَقُلْنَا اذْهَبَآ اِلَى الْقَوْمِ الَّذِيْنَ كَذَّبُوْا بِاٰيٰتِنَاۗ فَدَمَّرْنٰهُمْ تَدْمِيْرًا ۗ

faquln*aa* i*dz*hab*aa* il*aa* **a**lqawmi **al**la*dz*iina ka*dzdz*abuu bi-*aa*y*aa*tin*aa* fadammarn*aa*hum tadmiir*aa***n**

Kemudian Kami berfirman (kepada keduanya), “Pergilah kamu berdua kepada kaum yang mendustakan ayat-ayat Kami.” Lalu Kami hancurkan mereka dengan sehancur-hancurnya.

25:37

# وَقَوْمَ نُوْحٍ لَّمَّا كَذَّبُوا الرُّسُلَ اَغْرَقْنٰهُمْ وَجَعَلْنٰهُمْ لِلنَّاسِ اٰيَةًۗ وَاَعْتَدْنَا لِلظّٰلِمِيْنَ عَذَابًا اَلِيْمًا ۚ

waqawma nuu*h*in lamm*aa* ka*dzdz*abuu **al**rrusula aghraqn*aa*hum waja'aln*aa*hum li**l**nn*aa*si *aa*yatan wa-a'tadn*aa* li**l***zhzhaa*limiina 'a*dzaa*ban

Dan (telah Kami binasakan) kaum Nuh ketika mereka mendustakan para rasul. Kami tenggelamkam mereka dan Kami jadikan (cerita) mereka itu pelajaran bagi manusia. Dan Kami telah sediakan bagi orang-orang zalim azab yang pedih;

25:38

# وَعَادًا وَّثَمُوْدَا۟ وَاَصْحٰبَ الرَّسِّ وَقُرُوْنًاۢ بَيْنَ ذٰلِكَ كَثِيْرًا

wa'*aa*dan watsamuuda wa-a*sh*-*haa*ba **al**rrassi waquruunan bayna *dzaa*lika katsiir*aa***n**

dan (telah Kami binasakan) kaum ‘Ad dan samud dan penduduk Rass serta banyak (lagi) generasi di antara (kaum-kaum) itu.

25:39

# وَكُلًّا ضَرَبْنَا لَهُ الْاَمْثَالَۖ وَكُلًّا تَبَّرْنَا تَتْبِيْرًا

wakullan *dh*arabn*aa* lahu **a**l-amts*aa*la wakullan tabbarn*aa* tatbiir*aa***n**

Dan masing-masing telah Kami jadikan perumpamaan dan masing-masing telah Kami hancurkan sehancur-hancurnya.

25:40

# وَلَقَدْ اَتَوْا عَلَى الْقَرْيَةِ الَّتِيْٓ اُمْطِرَتْ مَطَرَ السَّوْءِۗ اَفَلَمْ يَكُوْنُوْا يَرَوْنَهَاۚ بَلْ كَانُوْا لَا يَرْجُوْنَ نُشُوْرًا

walaqad ataw 'al*aa* **a**lqaryati **al**latii um*th*irat ma*th*ara **al**ssaw-i afalam yakuunuu yarawnah*aa* bal k*aa*nuu l*aa* yarjuuna nusyuur*aa***n**

Dan sungguh, mereka (kaum musyrik Mekah) telah melalui negeri (Sodom) yang (dulu) dijatuhi hujan yang buruk (hujan batu). Tidakkah mereka menyaksikannya? Bahkan mereka itu sebenarnya tidak mengharapkan hari kebangkitan.

25:41

# وَاِذَا رَاَوْكَ اِنْ يَّتَّخِذُوْنَكَ اِلَّا هُزُوًاۗ اَهٰذَا الَّذِيْ بَعَثَ اللّٰهُ رَسُوْلًا

wa-i*dzaa* ra-awka in yattakhi*dz*uunaka ill*aa* huzuwan ah*aadzaa* **al**la*dz*ii ba'atsa **al**l*aa*hu rasuul*aa***n**

Dan apabila mereka melihat engkau (Muhammad), mereka hanyalah menjadikan engkau sebagai ejekan (dengan mengatakan), “Inikah orangnya yang diutus Allah sebagai Rasul?

25:42

# اِنْ كَادَ لَيُضِلُّنَا عَنْ اٰلِهَتِنَا لَوْلَآ اَنْ صَبَرْنَا عَلَيْهَاۗ وَسَوْفَ يَعْلَمُوْنَ حِيْنَ يَرَوْنَ الْعَذَابَ مَنْ اَضَلُّ سَبِيْلًا

in k*aa*da layu*dh*illun*aa* 'an *aa*lihatin*aa* lawl*aa* an *sh*abarn*aa* 'alayh*aa* wasawfa ya'lamuuna *h*iina yarawna **a**l'a*dzaa*ba man a*dh*allu sabiil*aa***n**

**Sungguh, hampir saja dia menyesatkan kita dari sesembahan kita, seandainya kita tidak tetap bertahan (menyembah)nya.” Dan kelak mereka akan mengetahui pada saat mereka melihat azab, siapa yang paling sesat jalannya.**









25:43

# اَرَءَيْتَ مَنِ اتَّخَذَ اِلٰهَهٗ هَوٰىهُۗ اَفَاَنْتَ تَكُوْنُ عَلَيْهِ وَكِيْلًا ۙ

ara-ayta mani ittakha*dz*a il*aa*hahu haw*aa*hu afa-anta takuunu 'alayhi wakiil*aa***n**

Sudahkah engkau (Muhammad) melihat orang yang menjadikan keinginannya sebagai tuhannya. Apakah engkau akan menjadi pelindungnya?

25:44

# اَمْ تَحْسَبُ اَنَّ اَكْثَرَهُمْ يَسْمَعُوْنَ اَوْ يَعْقِلُوْنَۗ اِنْ هُمْ اِلَّا كَالْاَنْعَامِ بَلْ هُمْ اَضَلُّ سَبِيْلًا ࣖ

am ta*h*sabu anna aktsarahum yasma'uuna aw ya'qiluuna in hum ill*aa* ka**a**l-an'*aa*mi bal hum a*dh*allu sabiil*aa***n**

Atau apakah engkau mengira bahwa kebanyakan mereka itu mendengar atau memahami? Mereka itu hanyalah seperti hewan ternak, bahkan lebih sesat jalannya.

25:45

# اَلَمْ تَرَ اِلٰى رَبِّكَ كَيْفَ مَدَّ الظِّلَّۚ وَلَوْ شَاۤءَ لَجَعَلَهٗ سَاكِنًاۚ ثُمَّ جَعَلْنَا الشَّمْسَ عَلَيْهِ دَلِيْلًا ۙ

alam tara il*aa* rabbika kayfa madda **al***zhzh*illa walaw sy*aa*-a laja'alahu s*aa*kinan tsumma ja'aln*aa* **al**sysyamsa 'alayhi daliil*aa***n**

Tidakkah engkau memperhatikan (penciptaan) Tuhanmu, bagaimana Dia memanjangkan (dan memendekkan) bayang-bayang; dan sekiranya Dia menghendaki, niscaya Dia jadikannya (bayang-bayang itu) tetap, kemudian Kami jadikan matahari sebagai petunjuk,

25:46

# ثُمَّ قَبَضْنٰهُ اِلَيْنَا قَبْضًا يَّسِيْرًا

tsumma qaba*dh*n*aa*hu ilayn*aa* qab*dh*an yasiir*aa***n**

kemudian Kami menariknya (bayang-bayang itu) kepada Kami sedikit demi sedikit.

25:47

# وَهُوَ الَّذِيْ جَعَلَ لَكُمُ الَّيْلَ لِبَاسًا وَّالنَّوْمَ سُبَاتًا وَّجَعَلَ النَّهَارَ نُشُوْرًا

wahuwa **al**la*dz*ii ja'ala lakumu **al**layla lib*aa*san wa**al**nnawma sub*aa*tan waja'ala **al**nnah*aa*ra nusyuur*aa***n**

Dan Dialah yang menjadikan malam untukmu (sebagai) pakaian, dan tidur untuk istirahat, dan Dia menjadikan siang untuk bangkit berusaha.

25:48

# وَهُوَ الَّذِيْٓ اَرْسَلَ الرِّيٰحَ بُشْرًاۢ بَيْنَ يَدَيْ رَحْمَتِهٖۚ وَاَنْزَلْنَا مِنَ السَّمَاۤءِ مَاۤءً طَهُوْرًا ۙ

wahuwa **al**la*dz*ii arsala **al**rriy*aah*a busyran bayna yaday ra*h*matihi wa-anzaln*aa* mina **al**ssam*aa*-i m*aa*-an *th*ahuur*aa***n**

Dan Dialah yang meniupkan angin (sebagai) pembawa kabar gembira sebelum kedatangan rahmat-Nya (hujan); dan Kami turunkan dari langit air yang sangat bersih,

25:49

# لِّنُحْيِ َۧ بِهٖ بَلْدَةً مَّيْتًا وَّنُسْقِيَهٗ مِمَّا خَلَقْنَآ اَنْعَامًا وَّاَنَاسِيَّ كَثِيْرًا

linu*h*yiya bihi baldatan maytan wanusqiyahu mimm*aa* khalaqn*aa* an'*aa*man wa-an*aa*siyya katsiir*aa***n**

agar (dengan air itu) Kami menghidupkan negeri yang mati (tandus), dan Kami memberi minum kepada sebagian apa yang telah Kami ciptakan, (berupa) hewan-hewan ternak dan manusia yang banyak.

25:50

# وَلَقَدْ صَرَّفْنٰهُ بَيْنَهُمْ لِيَذَّكَّرُوْاۖ فَاَبٰىٓ اَكْثَرُ النَّاسِ اِلَّا كُفُوْرًا

walaqad *sh*arrafn*aa*hu baynahum liya*dzdz*akkaruu fa-ab*aa* aktsaru **al**nn*aa*si ill*aa* kufuur*aa***n**

Dan sungguh, Kami telah mempergilirkan (hujan) itu di antara mereka agar mereka mengambil pelajaran; tetapi kebanyakan manusia tidak mau (bersyukur), bahkan mereka mengingkari (nikmat).

25:51

# وَلَوْ شِئْنَا لَبَعَثْنَا فِيْ كُلِّ قَرْيَةٍ نَّذِيْرًا ۖ

walaw syi/n*aa* laba'atsn*aa* fii kulli qaryatin na*dz*iir*aa***n**

Dan sekiranya Kami menghendaki, niscaya Kami utus seorang pemberi peringatan pada setiap negeri.

25:52

# فَلَا تُطِعِ الْكٰفِرِيْنَ وَجَاهِدْهُمْ بِهٖ جِهَادًا كَبِيْرًا

fal*aa* tu*th*i'i **a**lk*aa*firiina waj*aa*hidhum bihi jih*aa*dan kabiir*aa***n**

Maka janganlah engkau taati orang-orang kafir, dan berjuanglah terhadap mereka dengannya (Al-Qur'an) dengan (semangat) perjuangan yang besar.

25:53

# ۞ وَهُوَ الَّذِيْ مَرَجَ الْبَحْرَيْنِ هٰذَا عَذْبٌ فُرَاتٌ وَّهٰذَا مِلْحٌ اُجَاجٌۚ وَجَعَلَ بَيْنَهُمَا بَرْزَخًا وَّحِجْرًا مَّحْجُوْرًا

wahuwa **al**la*dz*ii maraja **a**lba*h*rayni h*aadzaa* 'a*dz*bun fur*aa*tun wah*aadzaa* mil*h*un uj*aa*jun waja'ala baynahum*aa* barzakhan wa*h*ijran ma*h*juur*aa*

Dan Dialah yang membiarkan dua laut mengalir (berdampingan); yang ini tawar dan segar dan yang lain sangat asin lagi pahit; dan Dia jadikan antara keduanya dinding dan batas yang tidak tembus.

25:54

# وَهُوَ الَّذِيْ خَلَقَ مِنَ الْمَاۤءِ بَشَرًا فَجَعَلَهٗ نَسَبًا وَّصِهْرًاۗ وَكَانَ رَبُّكَ قَدِيْرًا

wahuwa **al**la*dz*ii khalaqa mina **a**lm*aa*-i basyaran faja'alahu nasaban wa*sh*ihran wak*aa*na rabbuka qadiir*aa***n**

Dan Dia (pula) yang menciptakan manusia dari air, lalu Dia jadikan manusia itu (mempunyai) keturunan dan musaharah dan Tuhanmu adalah Mahakuasa.

25:55

# وَيَعْبُدُوْنَ مِنْ دُوْنِ اللّٰهِ مَا لَا يَنْفَعُهُمْ وَلَا يَضُرُّهُمْۗ وَكَانَ الْكَافِرُ عَلٰى رَبِّهٖ ظَهِيْرًا

waya'buduuna min duuni **al**l*aa*hi m*aa* l*aa* yanfa'uhum wal*aa* ya*dh*urruhum wak*aa*na **a**lk*aa*firu 'al*aa* rabbihi *zh*ahiir*aa***n**

Dan mereka menyembah selain Allah apa yang tidak memberi manfaat kepada mereka dan tidak (pula) mendatangkan bencana kepada mereka. Orang-orang kafir adalah penolong (setan untuk berbuat durhaka) terhadap Tuhannya.

25:56

# وَمَآ اَرْسَلْنٰكَ اِلَّا مُبَشِّرًا وَّنَذِيْرًا

wam*aa* arsaln*aa*ka ill*aa* mubasysyiran wana*dz*iir*aa***n**

Dan tidaklah Kami mengutus engkau (Muhammad) melainkan hanya sebagai pembawa kabar gembira dan pemberi peringatan.

25:57

# قُلْ مَآ اَسْـَٔلُكُمْ عَلَيْهِ مِنْ اَجْرٍ اِلَّا مَنْ شَاۤءَ اَنْ يَّتَّخِذَ اِلٰى رَبِّهٖ سَبِيْلًا

qul m*aa* as-alukum 'alayhi min ajrin ill*aa* man sy*aa*-a an yattakhi*dz*a il*aa* rabbihi sabiil*aa***n**

Katakanlah, “Aku tidak meminta imbalan apa pun dari kamu dalam menyampaikan (risalah) itu, melainkan (mengharapkan agar) orang-orang mau mengambil jalan kepada Tuhannya.”

25:58

# وَتَوَكَّلْ عَلَى الْحَيِّ الَّذِيْ لَا يَمُوْتُ وَسَبِّحْ بِحَمْدِهٖۗ وَكَفٰى بِهٖ بِذُنُوْبِ عِبَادِهٖ خَبِيْرًا ۚ

watawakkal 'al*aa* **a**l*h*ayyi **al**la*dz*ii l*aa* yamuutu wasabbi*h* bi*h*amdihi wakaf*aa* bihi bi*dz*unuubi 'ib*aa*dihi khabiir*aa***n**

Dan bertawakallah kepada Allah Yang Hidup, Yang tidak mati, dan bertasbihlah dengan memuji-Nya. Dan cukuplah Dia Maha Mengetahui dosa hamba-hamba-Nya,

25:59

# اَلَّذِيْ خَلَقَ السَّمٰوٰتِ وَالْاَرْضَ وَمَا بَيْنَهُمَا فِيْ سِتَّةِ اَيَّامٍ ثُمَّ اسْتَوٰى عَلَى الْعَرْشِۚ اَلرَّحْمٰنُ فَسْـَٔلْ بِهٖ خَبِيْرًا

**al**la*dz*ii khalaqa **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*a wam*aa* baynahum*aa* fii sittati ayy*aa*min tsumma istaw*aa* 'al*aa* **a**l'arsyi **al**

**yang menciptakan langit dan bumi dan apa yang ada di antara keduanya dalam enam masa, kemudian Dia bersemayam di atas ‘Arsy, (Dialah) Yang Maha Pengasih, maka tanyakanlah (tentang Allah) kepada orang yang lebih mengetahui (Muhammad).**









25:60

# وَاِذَا قِيْلَ لَهُمُ اسْجُدُوْا لِلرَّحْمٰنِ قَالُوْا وَمَا الرَّحْمٰنُ اَنَسْجُدُ لِمَا تَأْمُرُنَا وَزَادَهُمْ نُفُوْرًا ۩ ࣖ

wa-i*dzaa* qiila lahumu usjuduu li**l**rra*h*m*aa*ni q*aa*luu wam*aa* **al**rra*h*m*aa*nu anasjudu lim*aa* ta/murun*aa* waz*aa*dahum nufuur*aa***n**

Dan apabila dikatakan kepada mereka, “Sujudlah kepada Yang Maha Pengasih”, mereka menjawab, “Siapakah yang Maha Pengasih itu? Apakah kami harus sujud kepada Allah yang engkau (Muhammad) perintahkan kepada kami (bersujud kepada-Nya)?” Dan mereka makin jauh

25:61

# تَبٰرَكَ الَّذِيْ جَعَلَ فِى السَّمَاۤءِ بُرُوْجًا وَّجَعَلَ فِيْهَا سِرَاجًا وَّقَمَرًا مُّنِيْرًا

tab*aa*raka **al**la*dz*ii ja'ala fii **al**ssam*aa*-i buruujan waja'ala fiih*aa* sir*aa*jan waqamaran muniir*aa***n**

Mahasuci Allah yang menjadikan di langit gugusan bintang-bintang dan Dia juga menjadikan padanya matahari dan bulan yang bersinar.

25:62

# وَهُوَ الَّذِيْ جَعَلَ الَّيْلَ وَالنَّهَارَ خِلْفَةً لِّمَنْ اَرَادَ اَنْ يَّذَّكَّرَ اَوْ اَرَادَ شُكُوْرًا

wahuwa **al**la*dz*ii ja'ala **al**layla wa**al**nnah*aa*ra khilfatan liman ar*aa*da an ya*dzdz*akkara aw ar*aa*da syukuur*aa***n**

Dan Dia (pula) yang menjadikan malam dan siang silih berganti bagi orang yang ingin mengambil pelajaran atau yang ingin bersyukur.

25:63

# وَعِبَادُ الرَّحْمٰنِ الَّذِيْنَ يَمْشُوْنَ عَلَى الْاَرْضِ هَوْنًا وَّاِذَا خَاطَبَهُمُ الْجٰهِلُوْنَ قَالُوْا سَلٰمًا

wa'ib*aa*du **al**rra*h*m*aa*ni **al**la*dz*iina yamsyuuna 'al*aa* **a**l-ar*dh*i hawnan wa-i*dzaa* kh*aath*abahumu **a**lj*aa*hiluuna q*aa*luu sal*aa<*

Adapun hamba-hamba Tuhan Yang Maha Pengasih itu adalah orang-orang yang berjalan di bumi dengan rendah hati dan apabila orang-orang bodoh menyapa mereka (dengan kata-kata yang menghina), mereka mengucapkan “salam,”







25:64

# وَالَّذِيْنَ يَبِيْتُوْنَ لِرَبِّهِمْ سُجَّدًا وَّقِيَامًا

wa**a**lla*dz*iina yabiituuna lirabbihim sujjadan waqiy*aa*m*aa***n**

dan orang-orang yang menghabiskan waktu malam untuk beribadah kepada Tuhan mereka dengan bersujud dan berdiri.

25:65

# وَالَّذِيْنَ يَقُوْلُوْنَ رَبَّنَا اصْرِفْ عَنَّا عَذَابَ جَهَنَّمَۖ اِنَّ عَذَابَهَا كَانَ غَرَامًا ۖ

wa**a**lla*dz*iina yaquuluuna rabban*aa* i*sh*rif 'ann*aa* 'a*dzaa*ba jahannama inna 'a*dzaa*bah*aa* k*aa*na ghar*aa*m*aa***n**

Dan orang-orang yang berkata, “Ya Tuhan kami, jauhkanlah azab Jahanam dari kami, karena sesungguhnya azabnya itu membuat kebinasaan yang kekal,”

25:66

# اِنَّهَا سَاۤءَتْ مُسْتَقَرًّا وَّمُقَامًا

nnah*aa* s*aa*-at mustaqarran wamuq*aa*m*aa***n**

sungguh, Jahanam itu seburuk-buruk tempat menetap dan tempat kediaman.

25:67

# وَالَّذِيْنَ اِذَآ اَنْفَقُوْا لَمْ يُسْرِفُوْا وَلَمْ يَقْتُرُوْا وَكَانَ بَيْنَ ذٰلِكَ قَوَامًا

wa**a**lla*dz*iina i*dzaa* anfaquu lam yusrifuu walam yaqturuu wak*aa*na bayna *dzaa*lika qaw*aa*m*aa***n**

Dan (termasuk hamba-hamba Tuhan Yang Maha Pengasih) orang-orang yang apabila menginfakkan (harta), mereka tidak berlebihan, dan tidak (pula) kikir, di antara keduanya secara wajar,

25:68

# وَالَّذِيْنَ لَا يَدْعُوْنَ مَعَ اللّٰهِ اِلٰهًا اٰخَرَ وَلَا يَقْتُلُوْنَ النَّفْسَ الَّتِيْ حَرَّمَ اللّٰهُ اِلَّا بِالْحَقِّ وَلَا يَزْنُوْنَۚ وَمَنْ يَّفْعَلْ ذٰلِكَ يَلْقَ اَثَامًا ۙ

wa**a**lla*dz*iina l*aa* yad'uuna ma'*aa* **al**l*aa*hi il*aa*han *aa*khara wal*aa* yaqtuluuna **al**nnafsa **al**latii *h*arrama **al**l*aa*hu i

dan orang-orang yang tidak mempersekutukan Allah dengan sembahan lain dan tidak membunuh orang yang diharamkan Allah kecuali dengan (alasan) yang benar, dan tidak berzina; dan barangsiapa melakukan demikian itu, niscaya dia mendapat hukuman yang berat,

25:69

# يُّضٰعَفْ لَهُ الْعَذَابُ يَوْمَ الْقِيٰمَةِ وَيَخْلُدْ فِيْهٖ مُهَانًا ۙ

yu*daa*'af lahu **a**l'a*dzaa*bu yawma **a**lqiy*aa*mati wayakhlud fiihi muh*aa*n*aa***n**

(yakni) akan dilipatgandakan azab untuknya pada hari Kiamat dan dia akan kekal dalam azab itu, dalam keadaan terhina,

25:70

# اِلَّا مَنْ تَابَ وَاٰمَنَ وَعَمِلَ عَمَلًا صَالِحًا فَاُولٰۤىِٕكَ يُبَدِّلُ اللّٰهُ سَيِّاٰتِهِمْ حَسَنٰتٍۗ وَكَانَ اللّٰهُ غَفُوْرًا رَّحِيْمًا

ill*aa* man t*aa*ba wa*aa*mana wa'amila 'amalan *shaa*li*h*an faul*aa*-ika yubaddilu **al**l*aa*hu sayyi-*aa*tihim *h*asan*aa*tin wak*aa*na **al**l*aa*hu ghafuuran ra

kecuali orang-orang yang bertobat dan beriman dan mengerjakan kebajikan; maka kejahatan mereka diganti Allah dengan kebaikan. Allah Maha Pengampun, Maha Penyayang.







25:71

# وَمَنْ تَابَ وَعَمِلَ صَالِحًا فَاِنَّهٗ يَتُوْبُ اِلَى اللّٰهِ مَتَابًا

waman t*aa*ba wa'amila *shaa*li*h*an fa-innahu yatuubu il*aa* **al**l*aa*hi mat*aa*b*aa***n**

Dan barangsiapa bertobat dan mengerjakan kebajikan, maka sesungguhnya dia bertobat kepada Allah dengan tobat yang sebenar-benarnya.

25:72

# وَالَّذِيْنَ لَا يَشْهَدُوْنَ الزُّوْرَۙ وَاِذَا مَرُّوْا بِاللَّغْوِ مَرُّوْا كِرَامًا

wa**a**lla*dz*iina l*aa* yasyhaduuna **al**zzuura wa-i*dzaa* marruu bi**a**llaghwi marruu kir*aa*m*aa***n**

Dan orang-orang yang tidak memberikan kesaksian palsu, dan apabila mereka bertemu dengan (orang-orang) yang mengerjakan perbuatan-perbuatan yang tidak berfaedah, mereka berlalu dengan menjaga kehormatan dirinya,

25:73

# وَالَّذِيْنَ اِذَا ذُكِّرُوْا بِاٰيٰتِ رَبِّهِمْ لَمْ يَخِرُّوْا عَلَيْهَا صُمًّا وَّعُمْيَانًا

wa**a**lla*dz*iina i*dzaa* *dz*ukkiruu bi-*aa*y*aa*ti rabbihim lam yakhirruu 'alayh*aa* *sh*umman wa'umy*aa*n*aa***n**

dan orang-orang yang apabila diberi peringatan dengan ayat-ayat Tuhan mereka, mereka tidak bersikap sebagai orang-orang yang tuli dan buta,

25:74

# وَالَّذِيْنَ يَقُوْلُوْنَ رَبَّنَا هَبْ لَنَا مِنْ اَزْوَاجِنَا وَذُرِّيّٰتِنَا قُرَّةَ اَعْيُنٍ وَّاجْعَلْنَا لِلْمُتَّقِيْنَ اِمَامًا

wa**a**lla*dz*iina yaquuluuna rabban*aa* hab lan*aa* min azw*aa*jin*aa* wa*dz*urriyy*aa*tin*aa* qurrata a'yunin wa**i**j'aln*aa* lilmuttaqiina im*aa*m*aa***n**

Dan orang-orang yang berkata, “Ya Tuhan kami, anugerahkanlah kepada kami pasangan kami dan keturunan kami sebagai penyenang hati (kami), dan jadikanlah kami pemimpin bagi orang-orang yang bertakwa.”

25:75

# اُولٰۤىِٕكَ يُجْزَوْنَ الْغُرْفَةَ بِمَا صَبَرُوْا وَيُلَقَّوْنَ فِيْهَا تَحِيَّةً وَّسَلٰمًا ۙ

ul*aa*-ika yujzawna **a**lghurfata bim*aa* *sh*abaruu wayulaqqawna fiih*aa* ta*h*iyyatan wasal*aa*m*aa***n**

Mereka itu akan diberi balasan dengan tempat yang tinggi (dalam surga) atas kesabaran mereka, dan di sana mereka akan disambut dengan penghormatan dan salam.

25:76

# خٰلِدِيْنَ فِيْهَاۗ حَسُنَتْ مُسْتَقَرًّا وَّمُقَامًا

kh*aa*lidiina fiih*aa* *h*asunat mustaqarran wamuq*aa*m*aa***n**

Mereka kekal di dalamnya. Surga itu sebaik-baik tempat menetap dan tempat kediaman.

25:77

# قُلْ مَا يَعْبَؤُا بِكُمْ رَبِّيْ لَوْلَا دُعَاۤؤُكُمْۚ فَقَدْ كَذَّبْتُمْ فَسَوْفَ يَكُوْنُ لِزَامًا ࣖ

qul m*aa* ya'bau bikum rabbii lawl*aa* du'*aa*ukum faqad ka*dzdz*abtum fasawfa yakuunu liz*aa*m*aa***n**

Katakanlah (Muhammad, kepada orang-orang musyrik), “Tuhanku tidak akan mengindahkan kamu, kalau tidak karena ibadahmu. (Tetapi bagaimana kamu beribadah kepada-Nya), padahal sungguh, kamu telah mendustakan-Nya? Karena itu, kelak (azab) pasti (menimpamu).”

<!--EndFragment-->