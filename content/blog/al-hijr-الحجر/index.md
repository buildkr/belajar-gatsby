---
title: (15) Al-Hijr - الحجر
date: 2021-10-27T03:36:05.631Z
ayat: 15
description: "Jumlah Ayat: 99 / Arti: Hijr"
---
<!--StartFragment-->

15:1

# الۤرٰ ۗتِلْكَ اٰيٰتُ الْكِتٰبِ وَقُرْاٰنٍ مُّبِيْنٍ ۔

alif-l*aa*m-r*aa* tilka *aa*y*aa*tu **a**lkit*aa*bi waqur-*aa*nin mubiin**in**

Alif Lam Ra. (Surah) ini adalah (sebagian dari) ayat-ayat Kitab (yang sempurna) yaitu (ayat-ayat) Al-Qur'an yang memberi penjelasan.

15:2

# رُبَمَا يَوَدُّ الَّذِيْنَ كَفَرُوْا لَوْ كَانُوْا مُسْلِمِيْنَ

rubam*aa* yawaddu **al**la*dz*iina kafaruu law k*aa*nuu muslimiin**a**

Orang kafir itu kadang-kadang (nanti di akhirat) menginginkan, sekiranya mereka dahulu (di dunia) menjadi orang Muslim.

15:3

# ذَرْهُمْ يَأْكُلُوْا وَيَتَمَتَّعُوْا وَيُلْهِهِمُ الْاَمَلُ فَسَوْفَ يَعْلَمُوْنَ

*dz*arhum ya/kuluu wayatamatta'uu wayulhihimu **a**l-amalu fasawfa ya'lamuun**a**

Biarkanlah mereka (di dunia ini) makan dan bersenang-senang dan dilalaikan oleh angan-angan (kosong) mereka, kelak mereka akan mengetahui (akibat perbuatannya).

15:4

# وَمَآ اَهْلَكْنَا مِنْ قَرْيَةٍ اِلَّا وَلَهَا كِتَابٌ مَّعْلُوْمٌ

wam*aa* ahlakn*aa* min qaryatin ill*aa* walah*aa* kit*aa*bun ma'luum**un**

Dan Kami tidak membinasakan suatu negeri, melainkan sudah ada ketentuan yang ditetapkan baginya.

15:5

# مَا تَسْبِقُ مِنْ اُمَّةٍ اَجَلَهَا وَمَا يَسْتَأْخِرُوْنَ

m*aa* tasbiqu min ummatin ajalah*aa* wam*aa* yasta/khiruun**a**

Tidak ada suatu umat pun yang dapat mendahului ajalnya, dan tidak (pula) dapat meminta penundaan(nya).

15:6

# وَقَالُوْا يٰٓاَيُّهَا الَّذِيْ نُزِّلَ عَلَيْهِ الذِّكْرُ اِنَّكَ لَمَجْنُوْنٌ ۗ

waq*aa*luu y*aa* ayyuh*aa* **al**la*dz*ii nuzzila 'alayhi **al***dzdz*ikru innaka lamajnuun**un**

Dan mereka berkata, “Wahai orang yang kepadanya diturunkan Al-Qur'an, sesungguhnya engkau (Muhammad) benar-benar orang gila.

15:7

# لَوْمَا تَأْتِيْنَا بِالْمَلٰۤىِٕكَةِ اِنْ كُنْتَ مِنَ الصّٰدِقِيْنَ

law m*aa* ta/tiin*aa* bi**a**lmal*aa*-ikati in kunta mina **al***shshaa*diqiin**a**

Mengapa engkau tidak mendatangkan malaikat kepada kami, jika engkau termasuk orang yang benar?”

15:8

# مَا نُنَزِّلُ الْمَلٰۤىِٕكَةَ اِلَّا بِالْحَقِّ وَمَا كَانُوْٓا اِذًا مُّنْظَرِيْنَ

m*aa* nunazzilu **a**lmal*aa*-ikata ill*aa* bi**a**l*h*aqqi wam*aa* k*aa*nuu i*dz*an mun*zh*ariin**a**

Kami tidak menurunkan malaikat melainkan dengan kebenaran (untuk membawa azab) dan mereka ketika itu tidak diberikan penangguhan

15:9

# اِنَّا نَحْنُ نَزَّلْنَا الذِّكْرَ وَاِنَّا لَهٗ لَحٰفِظُوْنَ

inn*aa* na*h*nu nazzaln*aa* **al***dzdz*ikra wa-inn*aa* lahu la*haa*fi*zh*uun**a**

Sesungguhnya Kamilah yang menurunkan Al-Qur'an, dan pasti Kami (pula) yang memeliharanya.

15:10

# وَلَقَدْ اَرْسَلْنَا مِنْ قَبْلِكَ فِيْ شِيَعِ الْاَوَّلِيْنَ

walaqad arsaln*aa* min qablika fii syiya'i **a**l-awwaliin**a**

Dan sungguh, Kami telah mengutus (beberapa rasul) sebelum engkau (Muhammad) kepada umat-umat terdahulu.

15:11

# وَمَا يَأْتِيْهِمْ مِّنْ رَّسُوْلٍ اِلَّا كَانُوْا بِهٖ يَسْتَهْزِءُوْنَ

wam*aa* ya/tiihim min rasuulin ill*aa* k*aa*nuu bihi yastahzi-uun**a**

Dan setiap kali seorang rasul datang kepada mereka, mereka selalu memperolok-olokannya.

15:12

# كَذٰلِكَ نَسْلُكُهٗ فِيْ قُلُوْبِ الْمُجْرِمِيْنَۙ

ka*dzaa*lika naslukuhu fii quluubi **a**lmujrimiin**a**

Demikianlah, Kami memasukkannya (olok-olok itu) ke dalam hati orang yang berdosa,

15:13

# لَا يُؤْمِنُوْنَ بِهٖ وَقَدْ خَلَتْ سُنَّةُ الْاَوَّلِيْنَ

l*aa* yu/minuuna bihi waqad khalat sunnatu **a**l-awwaliin**a**

mereka tidak beriman kepadanya (Al-Qur'an) padahal telah berlalu sunatullah terhadap orang-orang terdahulu.

15:14

# وَلَوْ فَتَحْنَا عَلَيْهِمْ بَابًا مِّنَ السَّمَاۤءِ فَظَلُّوْا فِيْهِ يَعْرُجُوْنَۙ

walaw fata*h*n*aa* 'alayhim b*aa*ban mina **al**ssam*aa*-i fa*zh*alluu fiihi ya'rujuun**a**

Dan kalau Kami bukakan kepada mereka salah satu pintu langit, lalu mereka terus menerus naik ke atasnya,

15:15

# لَقَالُوْٓا اِنَّمَا سُكِّرَتْ اَبْصَارُنَا بَلْ نَحْنُ قَوْمٌ مَّسْحُوْرُوْنَ ࣖ

laq*aa*luu innam*aa* sukkirat ab*shaa*run*aa* bal na*h*nu qawmun mas*h*uuruun**a**

tentulah mereka berkata, “Sesungguhnya pandangan kamilah yang dikaburkan, bahkan kami adalah orang yang terkena sihir.”

15:16

# وَلَقَدْ جَعَلْنَا فِى السَّمَاۤءِ بُرُوْجًا وَّزَيَّنّٰهَا لِلنّٰظِرِيْنَۙ

walaqad ja'aln*aa* fii **al**ssam*aa*-i buruujan wazayyann*aa*h*aa* li**l**nn*aats*iriin**a**

Dan sungguh, Kami telah menciptakan gugusan bintang di langit dan men-jadikannya terasa indah bagi orang yang memandang(nya),

15:17

# وَحَفِظْنٰهَا مِنْ كُلِّ شَيْطٰنٍ رَّجِيْمٍۙ

wa*h*afi*zh*n*aa*h*aa* min kulli syay*thaa*nin rajiim**in**

dan Kami menjaganya dari setiap (gangguan) setan yang terkutuk,

15:18

# اِلَّا مَنِ اسْتَرَقَ السَّمْعَ فَاَتْبَعَهٗ شِهَابٌ مُّبِيْنٌ

ill*aa* mani istaraqa **al**ssam'a fa-atba'ahu syih*aa*bun mubiin**un**

kecuali (setan) yang mencuri-curi (berita) yang dapat didengar (dari malaikat) lalu dikejar oleh semburan api yang terang.

15:19

# وَالْاَرْضَ مَدَدْنٰهَا وَاَلْقَيْنَا فِيْهَا رَوَاسِيَ وَاَنْۢبَتْنَا فِيْهَا مِنْ كُلِّ شَيْءٍ مَّوْزُوْنٍ

wa**a**l-ar*dh*a madadn*aa*h*aa* wa-alqayn*aa* fiih*aa* raw*aa*siya wa-anbatn*aa* fiih*aa* min kulli syay-in mawzuun**in**

Dan Kami telah menghamparkan bumi dan Kami pancangkan padanya gunung-gunung serta Kami tumbuhkan di sana segala sesuatu menurut ukuran.

15:20

# وَجَعَلْنَا لَكُمْ فِيْهَا مَعَايِشَ وَمَنْ لَّسْتُمْ لَهٗ بِرَازِقِيْنَ

waja'aln*aa* lakum fiih*aa* ma'*aa*yisya waman lastum lahu bir*aa*ziqiin**a**

Dan Kami telah menjadikan padanya sumber-sumber kehidupan untuk keperluanmu, dan (Kami ciptakan pula) makhluk-makhluk yang bukan kamu pemberi rezekinya.

15:21

# وَاِنْ مِّنْ شَيْءٍ اِلَّا عِنْدَنَا خَزَاۤىِٕنُهٗ وَمَا نُنَزِّلُهٗٓ اِلَّا بِقَدَرٍ مَّعْلُوْمٍ

wa-in min syay-in ill*aa* 'indan*aa* khaz*aa*-inuhu wam*aa* nunazziluhu ill*aa* biqadarin ma'luum**in**

Dan tidak ada sesuatu pun, melainkan pada sisi Kamilah khazanahnya; Kami tidak menurunkannya melainkan dengan ukuran tertentu.

15:22

# وَاَرْسَلْنَا الرِّيٰحَ لَوَاقِحَ فَاَنْزَلْنَا مِنَ السَّمَاۤءِ مَاۤءً فَاَسْقَيْنٰكُمُوْهُۚ وَمَآ اَنْتُمْ لَهٗ بِخَازِنِيْنَ

wa-arsaln*aa* **al**rriy*aah*a law*aa*qi*h*a fa-anzaln*aa* mina **al**ssam*aa*-i m*aa*-an fa-asqayn*aa*kumuuhu wam*aa* antum lahu bikh*aa*ziniin**a**

Dan Kami telah meniupkan angin untuk mengawinkan dan Kami turunkan hujan dari langit, lalu Kami beri minum kamu dengan (air) itu, dan bukanlah kamu yang menyimpannya.

15:23

# وَاِنَّا لَنَحْنُ نُحْيٖ وَنُمِيْتُ وَنَحْنُ الْوَارِثُوْنَ

wa-inn*aa* lana*h*nu nu*h*yii wanumiitu wana*h*nu **a**lw*aa*ritsuun**a**

Dan sungguh, Kamilah yang menghidupkan dan mematikan dan Kami (pulalah) yang mewarisi.

15:24

# وَلَقَدْ عَلِمْنَا الْمُسْتَقْدِمِيْنَ مِنْكُمْ وَلَقَدْ عَلِمْنَا الْمُسْتَأْخِرِيْنَ

walaqad 'alimn*aa* **a**lmustaqdimiina minkum walaqad 'alimn*aa* **a**lmusta/khiriin**a**

Dan sungguh, Kami mengetahui orang yang terdahulu sebelum kamu dan Kami mengetahui pula orang yang terkemudian.

15:25

# وَاِنَّ رَبَّكَ هُوَ يَحْشُرُهُمْۗ اِنَّهٗ حَكِيْمٌ عَلِيْمٌ ࣖ

wa-inna rabbaka huwa ya*h*syuruhum innahu *h*akiimun 'aliim**u**

Dan sesungguhnya Tuhanmu, Dialah yang akan mengumpulkan mereka. Sungguh, Dia Mahabijaksana, Maha Mengetahui.

15:26

# وَلَقَدْ خَلَقْنَا الْاِنْسَانَ مِنْ صَلْصَالٍ مِّنْ حَمَاٍ مَّسْنُوْنٍۚ

walaqad khalaqn*aa* **a**l-ins*aa*na min *sh*al*shaa*lin min *h*ama-in masnuun**in**

Dan sungguh, Kami telah menciptakan manusia (Adam) dari tanah liat kering dari lumpur hitam yang diberi bentuk.

15:27

# وَالْجَاۤنَّ خَلَقْنٰهُ مِنْ قَبْلُ مِنْ نَّارِ السَّمُوْمِ

wa**a**lj*aa*nna khalaqn*aa*hu min qablu min n*aa*ri **al**ssamuum**i**

Dan Kami telah menciptakan jin sebelum (Adam) dari api yang sangat panas.

15:28

# وَاِذْ قَالَ رَبُّكَ لِلْمَلٰۤىِٕكَةِ اِنِّيْ خَالِقٌۢ بَشَرًا مِّنْ صَلْصَالٍ مِّنْ حَمَاٍ مَّسْنُوْنٍۚ

wa-i*dz* q*aa*la rabbuka lilmal*aa*-ikati innii kh*aa*liqun basyaran min *sh*al*shaa*lin min *h*ama-in masnuun**in**

Dan (ingatlah), ketika Tuhanmu berfirman kepada para malaikat, “Sungguh, Aku akan menciptakan seorang manusia dari tanah liat kering dari lumpur hitam yang diberi bentuk.

15:29

# فَاِذَا سَوَّيْتُهٗ وَنَفَخْتُ فِيْهِ مِنْ رُّوْحِيْ فَقَعُوْا لَهٗ سٰجِدِيْنَ

fa-i*dzaa* sawwaytuhu wanafakhtu fiihi min ruu*h*ii faqa'uu lahu s*aa*jidiin**a**

Maka apabila Aku telah menyempurnakan (kejadian)nya, dan Aku telah meniupkan roh (ciptaan)-Ku ke dalamnya, maka tunduklah kamu kepadanya dengan bersujud.”

15:30

# فَسَجَدَ الْمَلٰۤىِٕكَةُ كُلُّهُمْ اَجْمَعُوْنَۙ

fasajada **a**lmal*aa*-ikatu kulluhum ajma'uun**a**

Maka bersujudlah para malaikat itu semuanya bersama-sama,

15:31

# اِلَّآ اِبْلِيْسَۗ اَبٰىٓ اَنْ يَّكُوْنَ مَعَ السّٰجِدِيْنَ

ill*aa* ibliisa ab*aa* an yakuuna ma'a **al**ss*aa*jidiin**a**

kecuali Iblis. Ia enggan ikut bersama-sama para (malaikat) yang sujud itu.

15:32

# قَالَ يٰٓاِبْلِيْسُ مَا لَكَ اَلَّا تَكُوْنَ مَعَ السّٰجِدِيْنَ

q*aa*la y*aa* ibliisu m*aa* laka **al**l*aa* takuuna ma'a **al**ss*aa*jidiin**a**

Dia (Allah) berfirman, “Wahai Iblis! Apa sebabnya kamu (tidak ikut) sujud bersama mereka?”

15:33

# قَالَ لَمْ اَكُنْ لِّاَسْجُدَ لِبَشَرٍ خَلَقْتَهٗ مِنْ صَلْصَالٍ مِّنْ حَمَاٍ مَّسْنُوْنٍ

q*aa*la lam akun li-asjuda libasyarin khalaqtahu min *sh*al*shaa*lin min *h*ama-in masnuun**in**

Ia (Iblis) berkata, “Aku sekali-kali tidak akan sujud kepada manusia yang Engkau telah menciptakannya dari tanah liat kering dari lumpur hitam yang diberi bentuk.”

15:34

# قَالَ فَاخْرُجْ مِنْهَا فَاِنَّكَ رَجِيْمٌۙ

q*aa*la fa**u**khruj minh*aa* fa-innaka rajiim**un**

Dia (Allah) berfirman, “(Kalau begitu) keluarlah dari surga, karena sesungguhnya kamu terkutuk,

15:35

# وَّاِنَّ عَلَيْكَ اللَّعْنَةَ اِلٰى يَوْمِ الدِّيْنِ

wa-inna 'alayka **al**la'nata il*aa* yawmi **al**ddiin**i**

dan sesungguhnya kutukan itu tetap menimpamu sampai hari Kiamat.”

15:36

# قَالَ رَبِّ فَاَنْظِرْنِيْٓ اِلٰى يَوْمِ يُبْعَثُوْنَ

q*aa*la rabbi fa-an*zh*irnii il*aa* yawmi yub'atsuun**a**

Ia (Iblis) berkata, “Ya Tuhanku, (kalau begitu) maka berilah penangguhan kepadaku sampai hari (manusia) dibangkitkan.”

15:37

# قَالَ فَاِنَّكَ مِنَ الْمُنْظَرِيْنَۙ

q*aa*la fa-innaka mina **a**lmun*zh*ariin**a**

Allah berfirman, “(Baiklah) maka sesungguhnya kamu termasuk yang diberi penangguhan,

15:38

# اِلٰى يَوْمِ الْوَقْتِ الْمَعْلُوْمِ

il*aa* yawmi **a**lwaqti **a**lma'luum**i**

sampai hari yang telah ditentukan (kiamat).”

15:39

# قَالَ رَبِّ بِمَآ اَغْوَيْتَنِيْ لَاُزَيِّنَنَّ لَهُمْ فِى الْاَرْضِ وَلَاُغْوِيَنَّهُمْ اَجْمَعِيْنَۙ

q*aa*la rabbi bim*aa* aghwaytanii lauzayyinanna lahum fii **a**l-ar*dh*i walaughwiyannahum ajma'iin**a**

Ia (Iblis) berkata, “Tuhanku, oleh karena Engkau telah memutuskan bahwa aku sesat, aku pasti akan jadikan (kejahatan) terasa indah bagi mereka di bumi, dan aku akan menyesatkan mereka semuanya,

15:40

# اِلَّا عِبَادَكَ مِنْهُمُ الْمُخْلَصِيْنَ

ill*aa* 'ib*aa*daka minhumu **a**lmukhla*sh*iin**a**

kecuali hamba-hamba-Mu yang terpilih di antara mereka.”

15:41

# قَالَ هٰذَا صِرَاطٌ عَلَيَّ مُسْتَقِيْمٌ

q*aa*la h*aadzaa* *sh*ir*aath*un 'alayya mustaqiim**un**

Dia (Allah) berfirman, “Ini adalah jalan yang lurus (menuju) kepada-Ku.”

15:42

# اِنَّ عِبَادِيْ لَيْسَ لَكَ عَلَيْهِمْ سُلْطٰنٌ اِلَّا مَنِ اتَّبَعَكَ مِنَ الْغٰوِيْنَ

inna 'ib*aa*dii laysa laka 'alayhim sul*thaa*nun ill*aa* mani ittaba'aka mina **a**lgh*aa*wiin**a**

Sesungguhnya kamu (Iblis) tidak kuasa atas hamba-hamba-Ku, kecuali mereka yang mengikutimu, yaitu orang yang sesat.

15:43

# وَاِنَّ جَهَنَّمَ لَمَوْعِدُهُمْ اَجْمَعِيْنَۙ

wa-inna jahannama lamaw'iduhum ajma'iin**a**

Dan sungguh, Jahanam itu benar-benar (tempat) yang telah dijanjikan untuk mereka (pengikut setan) semuanya.

15:44

# لَهَا سَبْعَةُ اَبْوَابٍۗ لِكُلِّ بَابٍ مِّنْهُمْ جُزْءٌ مَّقْسُوْمٌ ࣖ

lah*aa* sab'atu abw*aa*bin likulli b*aa*bin minhum juz-un maqsuum**un**

(Jahanam) itu mempunyai tujuh pintu. Setiap pintu (telah ditetapkan) untuk golongan tertentu dari mereka.

15:45

# اِنَّ الْمُتَّقِيْنَ فِيْ جَنّٰتٍ وَّعُيُوْنٍۗ

inna **a**lmuttaqiina fii jann*aa*tin wa'uyuun**in**

Sesungguhnya orang yang bertakwa itu berada dalam surga-surga (taman-taman), dan (di dekat) mata air (yang mengalir).

15:46

# اُدْخُلُوْهَا بِسَلٰمٍ اٰمِنِيْنَ

udkhuluuh*aa* bisal*aa*min *aa*miniin**a**

(Allah berfirman), “Masuklah ke dalamnya dengan sejahtera dan aman.”

15:47

# وَنَزَعْنَا مَا فِيْ صُدُوْرِهِمْ مِّنْ غِلٍّ اِخْوَانًا عَلٰى سُرُرٍ مُّتَقٰبِلِيْنَ

wanaza'n*aa* m*aa* fii *sh*uduurihim min ghillin ikhw*aa*nan 'al*aa* sururin mutaq*aa*biliin**a**

Dan Kami lenyapkan segala rasa dendam yang ada dalam hati mereka; mereka merasa bersaudara, duduk berhadap-hadapan di atas dipan-dipan.

15:48

# لَا يَمَسُّهُمْ فِيْهَا نَصَبٌ وَّمَا هُمْ مِّنْهَا بِمُخْرَجِيْنَ

l*aa* yamassuhum fiih*aa* na*sh*abun wam*aa* hum minh*aa* bimukhrajiin**a**

Mereka tidak merasa lelah di dalamnya dan mereka tidak akan dikeluarkan darinya.

15:49

# ۞ نَبِّئْ عِبَادِيْٓ اَنِّيْٓ اَنَا الْغَفُوْرُ الرَّحِيْمُۙ

nabbi/ 'ib*aa*dii annii an*aa* **a**lghafuuru **al**rra*h*iim**u**

Kabarkanlah kepada hamba-hamba-Ku, bahwa Akulah Yang Maha Pengampun, Maha Penyayang,

15:50

# وَاَنَّ عَذَابِيْ هُوَ الْعَذَابُ الْاَلِيْمُ

wa-anna 'a*dzaa*bii huwa **a**l'a*dzaa*bu **a**l-aliim**u**

dan sesungguhnya azab-Ku adalah azab yang sangat pedih.

15:51

# وَنَبِّئْهُمْ عَنْ ضَيْفِ اِبْرٰهِيْمَۘ

wanabbi/hum 'an *dh*ayfi ibr*aa*hiim**a**

Dan kabarkanlah (Muhammad) kepada mereka tentang tamu Ibrahim (malaikat).

15:52

# اِذْ دَخَلُوْا عَلَيْهِ فَقَالُوْا سَلٰمًاۗ قَالَ اِنَّا مِنْكُمْ وَجِلُوْنَ

i*dz* dakhaluu 'alayhi faq*aa*luu sal*aa*man q*aa*la inn*aa* minkum wajiluun**a**

Ketika mereka masuk ke tempatnya, lalu mereka mengucapkan, “Salam.” Dia (Ibrahim) berkata, “Kami benar-benar merasa takut kepadamu.”

15:53

# قَالُوْا لَا تَوْجَلْ اِنَّا نُبَشِّرُكَ بِغُلٰمٍ عَلِيْمٍ

q*aa*luu l*aa* tawjal inn*aa* nubasysyiruka bighul*aa*min 'aliim**in**

(Mereka) berkata, “Janganlah engkau merasa takut, sesungguhnya kami memberi kabar gembira kepadamu dengan (kelahiran seorang) anak laki-laki (yang akan menjadi) orang yang pandai (Ishak).”

15:54

# قَالَ اَبَشَّرْتُمُوْنِيْ عَلٰٓى اَنْ مَّسَّنِيَ الْكِبَرُ فَبِمَ تُبَشِّرُوْنَ

q*aa*la abasysyartumuunii 'al*aa* an massaniya **a**lkibaru fabima tubasysyiruun**i**

Dia (Ibrahim) berkata, “Benarkah kamu memberi kabar gembira kepadaku padahal usiaku telah lanjut, lalu (dengan cara) bagaimana kamu memberi (kabar gembira) tersebut?”

15:55

# قَالُوْا بَشَّرْنٰكَ بِالْحَقِّ فَلَا تَكُنْ مِّنَ الْقٰنِطِيْنَ

q*aa*luu basysyarn*aa*ka bi**a**l*h*aqqi fal*aa* takun mina **a**lq*aa*ni*th*iin**a**

(Mereka) menjawab, “Kami menyampaikan kabar gembira kepadamu dengan benar, maka janganlah engkau termasuk orang yang berputus asa.”

15:56

# قَالَ وَمَنْ يَّقْنَطُ مِنْ رَّحْمَةِ رَبِّهٖٓ اِلَّا الضَّاۤلُّوْنَ

q*aa*la waman yaqna*th*u min ra*h*mati rabbihi ill*aa* **al***dhdhaa*lluun**a**

Dia (Ibrahim) berkata, “Tidak ada yang berputus asa dari rahmat Tuhannya, kecuali orang yang sesat.”

15:57

# قَالَ فَمَا خَطْبُكُمْ اَيُّهَا الْمُرْسَلُوْنَ

q*aa*la fam*aa* kha*th*bukum ayyuh*aa* **a**lmursaluun**a**

Dia (Ibrahim) berkata, “Apakah urusanmu yang penting, wahai para utusan?”

15:58

# قَالُوْٓا اِنَّآ اُرْسِلْنَآ اِلٰى قَوْمٍ مُّجْرِمِيْنَۙ

q*aa*luu inn*aa* ursiln*aa* il*aa* qawmin mujrimiin**a**

(Mereka) menjawab, “Sesungguhnya kami diutus kepada kaum yang berdosa,

15:59

# اِلَّآ اٰلَ لُوْطٍۗ اِنَّا لَمُنَجُّوْهُمْ اَجْمَعِيْنَۙ

ill*aa* *aa*la luu*th*in inn*aa* lamunajjuuhum ajma'iin**a**

kecuali para pengikut Lut. Sesungguhnya kami pasti menyelamatkan mereka semuanya,

15:60

# اِلَّا امْرَاَتَهٗ قَدَّرْنَآ اِنَّهَا لَمِنَ الْغٰبِرِيْنَ ࣖ

ill*aa* imra-atahu qaddarn*aa* innah*aa* lamina **a**lgh*aa*biriin**a**

kecuali istrinya, kami telah menentukan, bahwa dia termasuk orang yang tertinggal (bersama orang kafir lainnya).”

15:61

# فَلَمَّا جَاۤءَ اٰلَ لُوْطِ ِۨالْمُرْسَلُوْنَۙ

falamm*aa* j*aa*-a *aa*la luu*th*in **a**lmursaluun**a**

Maka ketika utusan itu datang kepada para pengikut Lut,

15:62

# قَالَ اِنَّكُمْ قَوْمٌ مُّنْكَرُوْنَ

q*aa*la innakum qawmun munkaruun**a**

dia (Lut) berkata, “Sesungguhnya kamu orang yang tidak kami kenal.”

15:63

# قَالُوْا بَلْ جِئْنٰكَ بِمَا كَانُوْا فِيْهِ يَمْتَرُوْنَ

q*aa*luu bal ji/n*aa*ka bim*aa* k*aa*nuu fiihi yamtaruun**a**

(Para utusan) menjawab, “Sebenarnya kami ini datang kepadamu membawa azab yang selalu mereka dustakan.

15:64

# وَاَتَيْنٰكَ بِالْحَقِّ وَاِنَّا لَصٰدِقُوْنَ

wa-atayn*aa*ka bi**a**l*h*aqqi wa-inn*aa* la*shaa*diquun**a**

Dan kami datang kepadamu membawa kebenaran dan sungguh, kami orang yang benar.

15:65

# فَاَسْرِ بِاَهْلِكَ بِقِطْعٍ مِّنَ الَّيْلِ وَاتَّبِعْ اَدْبَارَهُمْ وَلَا يَلْتَفِتْ مِنْكُمْ اَحَدٌ وَّامْضُوْا حَيْثُ تُؤْمَرُوْنَ

fa-asri bi-ahlika biqi*th*'in mina **al**layli wa**i**ttabi' adb*aa*rahum wal*aa* yaltafit minkum a*h*adun wa**i**m*dh*uu *h*aytsu tu/maruun**a**

Maka pergilah kamu pada akhir malam beserta keluargamu, dan ikutilah mereka dari belakang. Jangan ada di antara kamu yang menoleh ke belakang dan teruskanlah perjalanan ke tempat yang diperintahkan kepadamu.”

15:66

# وَقَضَيْنَآ اِلَيْهِ ذٰلِكَ الْاَمْرَ اَنَّ دَابِرَ هٰٓؤُلَاۤءِ مَقْطُوْعٌ مُّصْبِحِيْنَ

waqa*dh*ayn*aa* ilayhi *dzaa*lika **a**l-amra anna d*aa*bira h*aa*ul*aa*-i maq*th*uu'un mu*sh*bi*h*iin**a**

Dan telah Kami tetapkan kepadanya (Lut) keputusan itu, bahwa akhirnya mereka akan ditumpas habis pada waktu subuh.

15:67

# وَجَاۤءَ اَهْلُ الْمَدِيْنَةِ يَسْتَبْشِرُوْنَ

waj*aa*-a ahlu **a**lmadiinati yastabsyiruun**a**

Dan datanglah penduduk kota itu (ke rumah Lut) dengan gembira (karena kedatangan tamu itu).

15:68

# قَالَ اِنَّ هٰٓؤُلَاۤءِ ضَيْفِيْ فَلَا تَفْضَحُوْنِۙ

q*aa*la inna h*aa*ul*aa*-i *dh*ayfii fal*aa* taf*dh*a*h*uun**i**

Dia (Lut) berkata, “Sesungguhnya mereka adalah tamuku; maka jangan kamu mempermalukan aku,

15:69

# وَاتَّقُوا اللّٰهَ وَلَا تُخْزُوْنِ

wa**i**ttaquu **al**l*aa*ha wal*aa* tukhzuun**i**

Dan bertakwalah kepada Allah dan janganlah kamu membuat aku terhina.”

15:70

# قَالُوْٓا اَوَلَمْ نَنْهَكَ عَنِ الْعٰلَمِيْنَ

q*aa*luu awa lam nanhaka 'ani **a**l'*aa*lamiin**a**

(Mereka) berkata, “Bukankah kami telah melarangmu dari (melindungi) manusia?”

15:71

# قَالَ هٰٓؤُلَاۤءِ بَنٰتِيْٓ اِنْ كُنْتُمْ فٰعِلِيْنَۗ

q*aa*la h*aa*ul*aa*-i ban*aa*tii in kuntum f*aa*'iliin**a**

Dia (Lut) berkata, “Mereka itulah putri-putri (negeri)ku (nikahlah dengan mereka), jika kamu hendak berbuat.”

15:72

# لَعَمْرُكَ اِنَّهُمْ لَفِيْ سَكْرَتِهِمْ يَعْمَهُوْنَ

la'amruka innahum lafii sakratihim ya'mahuuna

(Allah berfirman), “Demi umurmu (Muhammad), sungguh, mereka terombang-ambing dalam kemabukan (kesesatan).”

15:73

# فَاَخَذَتْهُمُ الصَّيْحَةُ مُشْرِقِيْنَۙ

fa-akha*dz*at-humu **al***shsh*ay*h*atu musyriqiin**a**

Maka mereka dibinasakan oleh suara keras yang mengguntur, ketika matahari akan terbit.

15:74

# فَجَعَلْنَا عَالِيَهَا سَافِلَهَا وَاَمْطَرْنَا عَلَيْهِمْ حِجَارَةً مِّنْ سِجِّيْلٍ

faja'aln*aa* '*aa*liyah*aa* s*aa*filah*aa* wa-am*th*arn*aa* 'alayhim *h*ij*aa*ratan min sijjiil**in**

Maka Kami jungkirbalikkan (negeri itu) dan Kami hujani mereka dengan batu dari tanah yang keras.

15:75

# اِنَّ فِيْ ذٰلِكَ لَاٰيٰتٍ لِّلْمُتَوَسِّمِيْنَۙ

inna fii *dzaa*lika la*aa*y*aa*tin lilmutawassimiin**a**

Sungguh, pada yang demikian itu benar-benar terdapat tanda-tanda (kekuasaan Allah) bagi orang yang memperhatikan tanda-tanda,

15:76

# وَاِنَّهَا لَبِسَبِيْلٍ مُّقِيْمٍ

wa-innah*aa* labisabiilin muqiim**in**

dan sungguh, (negeri) itu benar-benar terletak di jalan yang masih tetap (dilalui manusia).

15:77

# اِنَّ فِيْ ذٰلِكَ لَاٰيَةً لِّلْمُؤْمِنِيْنَۗ

inna fii *dzaa*lika la*aa*yatan lilmu/miniin**a**

Sungguh, pada yang demikian itu benar-benar terdapat tanda (kekuasaan Allah) bagi orang yang beriman.

15:78

# وَاِنْ كَانَ اَصْحٰبُ الْاَيْكَةِ لَظٰلِمِيْنَۙ

wa-in k*aa*na a*sh*-*haa*bu **a**l-aykati la*zhaa*limiin**a**

Dan sesungguhnya penduduk Aikah itu benar-benar kaum yang zalim,

15:79

# فَانْتَقَمْنَا مِنْهُمْۘ وَاِنَّهُمَا لَبِاِمَامٍ مُّبِيْنٍۗ ࣖ

fa**i**ntaqamn*aa* minhum wa-innahum*aa* labi-im*aa*min mubiin**in**

maka Kami membinasakan mereka. Dan sesungguhnya kedua (negeri) itu terletak di satu jalur jalan raya.

15:80

# وَلَقَدْ كَذَّبَ اَصْحٰبُ الْحِجْرِ الْمُرْسَلِيْنَۙ

walaqad ka*dzdz*aba a*sh*-*haa*bu **a**l*h*ijri **a**lmursaliin**a**

Dan sesungguhnya penduduk negeri Hijr benar-benar telah mendustakan para rasul (mereka),

15:81

# وَاٰتَيْنٰهُمْ اٰيٰتِنَا فَكَانُوْا عَنْهَا مُعْرِضِيْنَۙ

wa*aa*tayn*aa*hum *aa*y*aa*tin*aa* fak*aa*nuu 'anh*aa* mu'ri*dh*iin**a**

dan Kami telah mendatangkan kepada mereka tanda-tanda (kekuasaan) Kami, tetapi mereka selalu berpaling darinya,

15:82

# وَكَانُوْا يَنْحِتُوْنَ مِنَ الْجِبَالِ بُيُوْتًا اٰمِنِيْنَ

wak*aa*nuu yan*h*ituuna mina **a**ljib*aa*li buyuutan *aa*miniin**a**

dan mereka memahat rumah-rumah dari gunung batu, (yang didiami) dengan rasa aman.

15:83

# فَاَخَذَتْهُمُ الصَّيْحَةُ مُصْبِحِيْنَۙ

fa-akha*dz*at-humu **al***shsh*ay*h*atu mu*sh*bi*h*iin**a**

Kemudian mereka dibinasakan oleh suara keras yang mengguntur pada pagi hari,

15:84

# فَمَآ اَغْنٰى عَنْهُمْ مَّا كَانُوْا يَكْسِبُوْنَۗ

fam*aa* aghn*aa* 'anhum m*aa* k*aa*nuu yaksibuun**a**

sehingga tidak berguna bagi mereka, apa yang telah mereka usahakan.

15:85

# وَمَا خَلَقْنَا السَّمٰوٰتِ وَالْاَرْضَ وَمَا بَيْنَهُمَآ اِلَّا بِالْحَقِّۗ وَاِنَّ السَّاعَةَ لَاٰتِيَةٌ فَاصْفَحِ الصَّفْحَ الْجَمِيْلَ

wam*aa* khalaqn*aa* **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*a wam*aa* baynahum*aa* ill*aa* bi**a**l*h*aqqi wa-inna **al**ss*aa*'ata la*aa*tiyatun fa

Dan Kami tidak menciptakan langit dan bumi serta apa yang ada di antara keduanya, melainkan dengan kebenaran. Dan sungguh, Kiamat pasti akan datang, maka maafkanlah (mereka) dengan cara yang baik.

15:86

# اِنَّ رَبَّكَ هُوَ الْخَلّٰقُ الْعَلِيْمُ

inna rabbaka huwa **a**lkhall*aa*qu **a**l'aliim**u**

Sungguh, Tuhanmu, Dialah Yang Maha Pencipta, Maha Mengetahui.

15:87

# وَلَقَدْ اٰتَيْنٰكَ سَبْعًا مِّنَ الْمَثَانِيْ وَالْقُرْاٰنَ الْعَظِيْمَ

walaqad *aa*tayn*aa*ka sab'an mina **a**lmats*aa*nii wa**a**lqur-*aa*na **a**l'a*zh*iim**a**

Dan sungguh, Kami telah memberikan kepadamu tujuh (ayat) yang (dibaca) berulang-ulang dan Al-Qur'an yang agung.

15:88

# لَا تَمُدَّنَّ عَيْنَيْكَ اِلٰى مَا مَتَّعْنَا بِهٖٓ اَزْوَاجًا مِّنْهُمْ وَلَا تَحْزَنْ عَلَيْهِمْ وَاخْفِضْ جَنَاحَكَ لِلْمُؤْمِنِيْنَ

l*aa* tamuddanna 'aynayka il*aa* m*aa* matta'n*aa* bihi azw*aa*jan minhum wal*aa* ta*h*zan 'alayhim wa**i**khfi*dh* jan*aah*aka lilmu/miniin**a**

Jangan sekali-kali engkau (Muhammad) tujukan pandanganmu kepada kenikmatan hidup yang telah Kami berikan kepada beberapa golongan di antara mereka (orang kafir), dan jangan engkau bersedih hati terhadap mereka dan berendah hatilah engkau terhadap orang ya

15:89

# وَقُلْ اِنِّيْٓ اَنَا النَّذِيْرُ الْمُبِيْنُۚ

waqul innii an*aa* **al**nna*dz*iiru **a**lmubiin**u**

Dan katakanlah (Muhammad), “Sesungguhnya aku adalah pemberi peringatan yang jelas.”

15:90

# كَمَآ اَنْزَلْنَا عَلَى الْمُقْتَسِمِيْنَۙ

kam*aa* anzaln*aa* 'al*aa* **a**lmuqtasimiin**a**

Sebagaimana (Kami telah memberi peringatan), Kami telah menurunkan (azab) kepada orang yang memilah-milah (Kitab Allah),

15:91

# الَّذِيْنَ جَعَلُوا الْقُرْاٰنَ عِضِيْنَ

**al**la*dz*iina ja'aluu **a**lqur-*aa*na 'i*dh*iin**a**

(yaitu) orang-orang yang telah menjadikan Al-Qur'an itu terbagi-bagi.

15:92

# فَوَرَبِّكَ لَنَسْـَٔلَنَّهُمْ اَجْمَعِيْنَۙ

fawarabbika lanas-alannahum ajma'iin**a**

Maka demi Tuhanmu, Kami pasti akan menanyai mereka semua,

15:93

# عَمَّا كَانُوْا يَعْمَلُوْنَ

'amm*aa* k*aa*nuu ya'maluun**a**

tentang apa yang telah mereka kerjakan dahulu.

15:94

# فَاصْدَعْ بِمَا تُؤْمَرُ وَاَعْرِضْ عَنِ الْمُشْرِكِيْنَ

fa**i***sh*da' bim*aa* tu/maru wa-a'ri*dh* 'ani **a**lmusyrikiin**a**

Maka sampaikanlah (Muhammad) secara terang-terangan segala apa yang diperintahkan (kepadamu) dan berpalinglah dari orang yang musyrik.

15:95

# اِنَّا كَفَيْنٰكَ الْمُسْتَهْزِءِيْنَۙ

inn*aa* kafayn*aa*ka **a**lmustahzi-iin**a**

Sesungguhnya Kami memelihara engkau (Muhammad) dari (kejahatan) orang yang memperolok-olokkan (engkau),

15:96

# الَّذِيْنَ يَجْعَلُوْنَ مَعَ اللّٰهِ اِلٰهًا اٰخَرَۚ فَسَوْفَ يَعْلَمُوْنَ

**al**la*dz*iina yaj'aluuna ma'a **al**l*aa*hi il*aa*han *aa*khara fasawfa ya'lamuun**a**

(yaitu) orang yang menganggap adanya tuhan selain Allah; mereka kelak akan mengetahui (akibatnya).

15:97

# وَلَقَدْ نَعْلَمُ اَنَّكَ يَضِيْقُ صَدْرُكَ بِمَا يَقُوْلُوْنَۙ

walaqad na'lamu annaka ya*dh*iiqu *sh*adruka bim*aa* yaquuluun**a**

Dan sungguh, Kami mengetahui bahwa dadamu menjadi sempit disebabkan apa yang mereka ucapkan,

15:98

# فَسَبِّحْ بِحَمْدِ رَبِّكَ وَكُنْ مِّنَ السَّاجِدِيْنَۙ

fasabbi*h* bi*h*amdi rabbika wakun mina **al**ss*aa*jidiin**a**

maka bertasbihlah dengan memuji Tuhanmu dan jadilah engkau di antara orang yang bersujud (salat),

15:99

# وَاعْبُدْ رَبَّكَ حَتّٰى يَأْتِيَكَ الْيَقِيْنُ ࣖࣖ

wa**u**'bud rabbaka *h*att*aa* ya/tiyaka **a**lyaqiin**u**

Dan sembahlah Tuhanmu sampai yakin (ajal) datang kepadamu.

<!--EndFragment-->