---
title: (79) An-Nazi'at - النّٰزعٰت
date: 2021-10-27T04:08:47.588Z
ayat: 79
description: "Jumlah Ayat: 46 / Arti: Malaikat Yang Mencabut"
---
<!--StartFragment-->

79:1

# وَالنّٰزِعٰتِ غَرْقًاۙ

wa**al**nn*aa*zi'*aa*ti gharq*aa***n**

Demi (malaikat) yang mencabut (nyawa) dengan keras.

79:2

# وَّالنّٰشِطٰتِ نَشْطًاۙ

wa**al**nn*aa*syi*thaa*ti nasy*thaa***n**

Demi (malaikat) yang mencabut (nyawa) dengan lemah lembut.

79:3

# وَّالسّٰبِحٰتِ سَبْحًاۙ

wa**al**ss*aa*bi*haa*ti sab*haa***n**

Demi (malaikat) yang turun dari langit dengan cepat,

79:4

# فَالسّٰبِقٰتِ سَبْقًاۙ

fa**al**ss*aa*biq*aa*ti sabq*aa***n**

dan (malaikat) yang mendahului dengan kencang,

79:5

# فَالْمُدَبِّرٰتِ اَمْرًاۘ

fa**a**lmudabbir*aa*ti amr*aa***n**

dan (malaikat) yang mengatur urusan (dunia).

79:6

# يَوْمَ تَرْجُفُ الرَّاجِفَةُۙ

yawma tarjufu **al**rr*aa*jifa**tu**

(Sungguh, kamu akan dibangkitkan) pada hari ketika tiupan pertama mengguncangkan alam,

79:7

# تَتْبَعُهَا الرَّادِفَةُ ۗ

tatba'uh*aa* **al**rr*aa*difa**tu**

(tiupan pertama) itu diiringi oleh tiupan kedua.

79:8

# قُلُوْبٌ يَّوْمَىِٕذٍ وَّاجِفَةٌۙ

quluubun yawma-i*dz*in w*aa*jifa**tun**

Hati manusia pada waktu itu merasa sangat takut,

79:9

# اَبْصَارُهَا خَاشِعَةٌ ۘ

ab*shaa*ruh*aa* kh*aa*syi'a**tun**

pandangannya tunduk.

79:10

# يَقُوْلُوْنَ ءَاِنَّا لَمَرْدُوْدُوْنَ فِى الْحَافِرَةِۗ

yaquuluuna a-inn*aa* lamarduuduuna fii **a**l*haa*fira**ti**

(Orang-orang kafir) berkata, “Apakah kita benar-benar akan dikembalikan kepada kehidupan yang semula?

79:11

# ءَاِذَا كُنَّا عِظَامًا نَّخِرَةً ۗ

a-i*dzaa* kunn*aa* 'i*zhaa*man nakhira**tan**

Apakah (akan dibangkitkan juga) apabila kita telah menjadi tulang belulang yang hancur?”

79:12

# قَالُوْا تِلْكَ اِذًا كَرَّةٌ خَاسِرَةٌ ۘ

q*aa*luu tilka i*dz*an karratun kh*aa*sira**tun**

Mereka berkata, “Kalau demikian, itu adalah suatu pengembalian yang merugikan.”

79:13

# فَاِنَّمَا هِيَ زَجْرَةٌ وَّاحِدَةٌۙ

fa-innam*aa* hiya zajratun w*aah*ida**tun**

Maka pengembalian itu hanyalah dengan sekali tiupan saja.

79:14

# فَاِذَا هُمْ بِالسَّاهِرَةِۗ

fa-i*dzaa* hum bi**al**ss*aa*hira**ti**

Maka seketika itu mereka hidup kembali di bumi (yang baru).

79:15

# هَلْ اَتٰىكَ حَدِيْثُ مُوْسٰىۘ

hal at*aa*ka *h*adiitsu muus*aa*

Sudahkah sampai kepadamu (Muhammad) kisah Musa?

79:16

# اِذْ نَادٰىهُ رَبُّهٗ بِالْوَادِ الْمُقَدَّسِ طُوًىۚ

i*dz* n*aa*d*aa*hu rabbuhu bi**a**lw*aa*di **a**lmuqaddasi *th*uw*aa***n**

Ketika Tuhan memanggilnya (Musa) di lembah suci yaitu Lembah Tuwa;

79:17

# اِذْهَبْ اِلٰى فِرْعَوْنَ اِنَّهٗ طَغٰىۖ

i*dz*hab il*aa* fir'awna innahu *th*agh*aa*

pergilah engkau kepada Fir‘aun! Sesungguhnya dia telah melampaui batas,

79:18

# فَقُلْ هَلْ لَّكَ اِلٰٓى اَنْ تَزَكّٰىۙ

faqul hal laka il*aa* an tazakk*aa*

Maka katakanlah (kepada Fir‘aun), “Adakah keinginanmu untuk membersihkan diri (dari kesesatan),

79:19

# وَاَهْدِيَكَ اِلٰى رَبِّكَ فَتَخْشٰىۚ

wa-ahdiyaka il*aa* rabbika fatakhsy*aa*

dan engkau akan kupimpin ke jalan Tuhanmu agar engkau takut kepada-Nya?”

79:20

# فَاَرٰىهُ الْاٰيَةَ الْكُبْرٰىۖ

fa-ar*aa*hu **a**l-*aa*yata **a**lkubr*aa*

Lalu (Musa) memperlihatkan kepadanya mukjizat yang besar.

79:21

# فَكَذَّبَ وَعَصٰىۖ

faka*dzdz*aba wa'a*shaa*

Tetapi dia (Fir‘aun) mendustakan dan mendurhakai.

79:22

# ثُمَّ اَدْبَرَ يَسْعٰىۖ

tsumma adbara yas'*aa*

Kemudian dia berpaling seraya berusaha menantang (Musa).

79:23

# فَحَشَرَ فَنَادٰىۖ

fa*h*asyara fan*aa*d*aa*

Kemudian dia mengumpulkan (pembesar-pembesarnya) lalu berseru (memanggil kaumnya).

79:24

# فَقَالَ اَنَا۠ رَبُّكُمُ الْاَعْلٰىۖ

faq*aa*la an*aa* rabbukumu **a**l-a'l*aa*

(Seraya) berkata, “Akulah tuhanmu yang paling tinggi.”

79:25

# فَاَخَذَهُ اللّٰهُ نَكَالَ الْاٰخِرَةِ وَالْاُوْلٰىۗ

fa-akha*dz*ahu **al**l*aa*hu nak*aa*la **a**l-*aa*khirati wa**a**l-uul*aa*

Maka Allah menghukumnya dengan azab di akhirat dan siksaan di dunia.

79:26

# اِنَّ فِيْ ذٰلِكَ لَعِبْرَةً لِّمَنْ يَّخْشٰى ۗ ࣖ

inna fii *dzaa*lika la'ibratan liman yakhsy*aa*

Sungguh, pada yang demikian itu terdapat pelajaran bagi orang yang takut (kepada Allah).

79:27

# ءَاَنْتُمْ اَشَدُّ خَلْقًا اَمِ السَّمَاۤءُ ۚ بَنٰىهَاۗ

a-antum asyaddu khalqan ami **al**ssam*aa*u ban*aa*h*aa*

Apakah penciptaan kamu yang lebih hebat ataukah langit yang telah dibangun-Nya?

79:28

# رَفَعَ سَمْكَهَا فَسَوّٰىهَاۙ

rafa'a samkah*aa* fasaww*aa*h*aa*

Dia telah meninggikan bangunannya lalu menyempurnakannya,

79:29

# وَاَغْطَشَ لَيْلَهَا وَاَخْرَجَ ضُحٰىهَاۖ

wa-agh*th*asya laylah*aa* wa-akhraja *dh*u*haa*h*aa*

dan Dia menjadikan malamnya (gelap gulita), dan menjadikan siangnya (terang benderang).

79:30

# وَالْاَرْضَ بَعْدَ ذٰلِكَ دَحٰىهَاۗ

wa**a**l-ar*dh*a ba'da *dzaa*lika da*haa*h*aa*

Dan setelah itu bumi Dia hamparkan.

79:31

# اَخْرَجَ مِنْهَا مَاۤءَهَا وَمَرْعٰىهَاۖ

akhraja minh*aa* m*aa*-ah*aa* wamar'*aa*h*aa*

Darinya Dia pancarkan mata air, dan (ditumbuhkan) tumbuh-tumbuhannya.

79:32

# وَالْجِبَالَ اَرْسٰىهَاۙ

wa**a**ljib*aa*la ars*aa*h*aa*

Dan gunung-gunung Dia pancangkan dengan teguh.

79:33

# مَتَاعًا لَّكُمْ وَلِاَنْعَامِكُمْۗ

mat*aa*'an lakum wali-an'*aa*mikum

(Semua itu) untuk kesenanganmu dan untuk hewan-hewan ternakmu.

79:34

# فَاِذَا جَاۤءَتِ الطَّاۤمَّةُ الْكُبْرٰىۖ

fa-i*dzaa* j*aa*-ati **al***ththaa*mmatu **a**lkubr*aa*

Maka apabila malapetaka besar (hari Kiamat) telah datang,

79:35

# يَوْمَ يَتَذَكَّرُ الْاِنْسَانُ مَا سَعٰىۙ

yawma yata*dz*akkaru **a**l-ins*aa*nu m*aa* sa'*aa*

yaitu pada hari (ketika) manusia teringat akan apa yang telah dikerjakannya,

79:36

# وَبُرِّزَتِ الْجَحِيْمُ لِمَنْ يَّرٰى

waburrizati **a**lja*h*iimu liman yar*aa*

dan neraka diperlihatkan dengan jelas kepada setiap orang yang melihat.

79:37

# فَاَمَّا مَنْ طَغٰىۖ

fa-amm*aa* man *th*agh*aa*

Maka adapun orang yang melampaui batas,

79:38

# وَاٰثَرَ الْحَيٰوةَ الدُّنْيَاۙ

wa*aa*tsara **a**l*h*ay*aa*ta **al**dduny*aa*

dan lebih mengutamakan kehidupan dunia,

79:39

# فَاِنَّ الْجَحِيْمَ هِيَ الْمَأْوٰىۗ

fa-inna **a**lja*h*iima hiya **a**lma/w*aa*

maka sungguh, nerakalah tempat tinggalnya.

79:40

# وَاَمَّا مَنْ خَافَ مَقَامَ رَبِّهٖ وَنَهَى النَّفْسَ عَنِ الْهَوٰىۙ

wa-am*aa* man kh*aa*fa maq*aa*ma rabbihi wanah*aa* **al**nnafsa 'ani **a**lhaw*aa*

Dan adapun orang-orang yang takut kepada kebesaran Tuhannya dan menahan diri dari (keinginan) hawa nafsunya,

79:41

# فَاِنَّ الْجَنَّةَ هِيَ الْمَأْوٰىۗ

fa-inna **a**ljannata hiya **a**lma/w*aa*

maka sungguh, surgalah tempat tinggal(nya).

79:42

# يَسْـَٔلُوْنَكَ عَنِ السَّاعَةِ اَيَّانَ مُرْسٰىهَاۗ

yas-aluunaka 'ani **al**ss*aa*'ati ayy*aa*na murs*aa*h*aa*

Mereka (orang-orang kafir) bertanya kepadamu (Muhammad) tentang hari Kiamat, “Kapankah terjadinya?”

79:43

# فِيْمَ اَنْتَ مِنْ ذِكْرٰىهَاۗ

fiima anta min *dz*ikr*aa*h*aa*

Untuk apa engkau perlu menyebutkannya (waktunya)?

79:44

# اِلٰى رَبِّكَ مُنْتَهٰىهَاۗ

il*aa* rabbika muntah*aa*h*aa*

Kepada Tuhanmulah (dikembalikan) kesudahannya (ketentuan waktunya).

79:45

# اِنَّمَآ اَنْتَ مُنْذِرُ مَنْ يَّخْشٰىهَاۗ

innam*aa* anta mun*dz*iru man yakhsy*aa*h*aa*

Engkau (Muhammad) hanyalah pemberi peringatan bagi siapa yang takut kepadanya (hari Kiamat).

79:46

# كَاَنَّهُمْ يَوْمَ يَرَوْنَهَا لَمْ يَلْبَثُوْٓا اِلَّا عَشِيَّةً اَوْ ضُحٰىهَا ࣖ

ka-annahum yawma yarawnah*aa* lam yalbatsuu ill*aa* 'asyiyyatan aw *dh*u*haa*h

Pada hari ketika mereka melihat hari Kiamat itu (karena suasananya hebat), mereka merasa seakan-akan hanya (sebentar saja) tinggal (di dunia) pada waktu sore atau pagi hari.

<!--EndFragment-->