---
title: (71) Nuh - نوح
date: 2021-10-27T04:06:24.515Z
ayat: 71
description: "Jumlah Ayat: 28 / Arti: Nuh"
---
<!--StartFragment-->

71:1

# اِنَّآ اَرْسَلْنَا نُوْحًا اِلٰى قَوْمِهٖٓ اَنْ اَنْذِرْ قَوْمَكَ مِنْ قَبْلِ اَنْ يَّأْتِيَهُمْ عَذَابٌ اَلِيْمٌ

inn*aa* arsaln*aa* nuu*h*an il*aa* qawmihi an an*dz*ir qawmaka min qabli an ya/tiyahum 'a*dzaa*bun **a**liim**un**

Sesungguhnya Kami telah mengutus Nuh kepada kaumnya (dengan perintah), “Berilah kaummu peringatan sebelum datang kepadanya azab yang pedih.”

71:2

# قَالَ يٰقَوْمِ اِنِّيْ لَكُمْ نَذِيْرٌ مُّبِيْنٌۙ

q*aa*la y*aa* qawmi innii lakum na*dz*iirun mubiin**un**

Dia (Nuh) berkata, “Wahai kaumku! Sesungguhnya aku ini seorang pemberi peringatan yang menjelaskan kepada kamu,

71:3

# اَنِ اعْبُدُوا اللّٰهَ وَاتَّقُوْهُ وَاَطِيْعُوْنِۙ

ani u'buduu **al**l*aa*ha wa**i**ttaquuhu wa-a*th*ii'uun**i**

(yaitu) sembahlah Allah, bertakwalah kepada-Nya dan taatlah kepadaku,

71:4

# يَغْفِرْ لَكُمْ مِّنْ ذُنُوْبِكُمْ وَيُؤَخِّرْكُمْ اِلٰٓى اَجَلٍ مُّسَمًّىۗ اِنَّ اَجَلَ اللّٰهِ اِذَا جَاۤءَ لَا يُؤَخَّرُۘ لَوْ كُنْتُمْ تَعْلَمُوْنَ

yaghfir lakum min *dz*unuubikum wayu-akhkhirkum il*aa* ajalin musamman inna ajala **al**l*aa*hi i*dzaa* j*aa*-a l*aa* yu-akhkharu law kuntum ta'lamuun**a**

niscaya Dia mengampuni sebagian dosa-dosamu dan menangguhkan kamu (memanjangkan umurmu) sampai pada batas waktu yang ditentukan. Sungguh, ketetapan Allah itu apabila telah datang tidak dapat ditunda, seandainya kamu mengetahui.”

71:5

# قَالَ رَبِّ اِنِّيْ دَعَوْتُ قَوْمِيْ لَيْلًا وَّنَهَارًاۙ

q*aa*la rabbi innii da'awtu qawmii laylan wanah*aa*r*aa***n**

Dia (Nuh) berkata, “Ya Tuhanku, sesungguhnya aku telah menyeru kaumku siang dan malam,

71:6

# فَلَمْ يَزِدْهُمْ دُعَاۤءِيْٓ اِلَّا فِرَارًا

falam yazidhum du'*aa*-ii ill*aa* fir*aa*r*aa***n**

tetapi seruanku itu tidak menambah (iman) mereka, justru mereka lari (dari kebenaran).

71:7

# وَاِنِّيْ كُلَّمَا دَعَوْتُهُمْ لِتَغْفِرَ لَهُمْ جَعَلُوْٓا اَصَابِعَهُمْ فِيْٓ اٰذَانِهِمْ وَاسْتَغْشَوْا ثِيَابَهُمْ وَاَصَرُّوْا وَاسْتَكْبَرُوا اسْتِكْبَارًاۚ

wa-innii kullam*aa* da'awtuhum litaghfira lahum ja'aluu a*shaa*bi'ahum fii *aatsaa*nihim wa**i**staghsyaw tsiy*aa*bahum wa-a*sh*arruu wa**i**stakbaruu istikb*aa*r*aa***n**

Dan sesungguhnya aku setiap kali menyeru mereka (untuk beriman) agar Engkau mengampuni mereka, mereka memasukkan anak jarinya ke telinganya dan menutupkan bajunya (ke wajahnya) dan mereka tetap (mengingkari) dan sangat menyombongkan diri.

71:8

# ثُمَّ اِنِّيْ دَعَوْتُهُمْ جِهَارًاۙ

tsumma innii da'awtuhum jih*aa*r*aa***n**

Lalu sesungguhnya aku menyeru mereka dengan cara terang-terangan.

71:9

# ثُمَّ اِنِّيْٓ اَعْلَنْتُ لَهُمْ وَاَسْرَرْتُ لَهُمْ اِسْرَارًاۙ

tsumma innii a'lantu lahum wa-asrartu lahum isr*aa*r*aa***n**

Kemudian aku menyeru mereka secara terbuka dan dengan diam-diam,

71:10

# فَقُلْتُ اسْتَغْفِرُوْا رَبَّكُمْ اِنَّهٗ كَانَ غَفَّارًاۙ

faqultu istaghfiruu rabbakum innahu k*aa*na ghaff*aa*r*aa***n**

maka aku berkata (kepada mereka), “Mohonlah ampunan kepada Tuhanmu, Sungguh, Dia Maha Pengampun,

71:11

# يُّرْسِلِ السَّمَاۤءَ عَلَيْكُمْ مِّدْرَارًاۙ

yursili **al**ssam*aa*-a 'alaykum midr*aa*r*aa***n**

niscaya Dia akan menurunkan hujan yang lebat dari langit kepadamu,

71:12

# وَّيُمْدِدْكُمْ بِاَمْوَالٍ وَّبَنِيْنَ وَيَجْعَلْ لَّكُمْ جَنّٰتٍ وَّيَجْعَلْ لَّكُمْ اَنْهٰرًاۗ

wayumdidkum bi-amw*aa*lin wabaniina wayaj'al lakum jann*aa*tin wayaj'al lakum anh*aa*r*aa***n**

dan Dia memperbanyak harta dan anak-anakmu, dan mengadakan kebun-kebun untukmu dan mengadakan sungai-sungai untukmu.”

71:13

# مَا لَكُمْ لَا تَرْجُوْنَ لِلّٰهِ وَقَارًاۚ

m*aa* lakum l*aa* tarjuuna lill*aa*hi waq*aa*r*aa***n**

Mengapa kamu tidak takut akan kebesaran Allah?

71:14

# وَقَدْ خَلَقَكُمْ اَطْوَارًا

waqad khalaqakum a*th*w*aa*r*aa***n**

Dan sungguh, Dia telah menciptakan kamu dalam beberapa tingkatan (kejadian).

71:15

# اَلَمْ تَرَوْا كَيْفَ خَلَقَ اللّٰهُ سَبْعَ سَمٰوٰتٍ طِبَاقًاۙ

alam taraw kayfa khalaqa **al**l*aa*hu sab'a sam*aa*w*aa*tin *th*ib*aa*q*aa***n**

Tidakkah kamu memperhatikan bagai-mana Allah telah menciptakan tujuh langit berlapis-lapis?

71:16

# وَّجَعَلَ الْقَمَرَ فِيْهِنَّ نُوْرًا وَّجَعَلَ الشَّمْسَ سِرَاجًا

waja'ala **a**lqamara fiihinna nuuran waja'ala **al**sysyamsa sir*aa*j*aa***n**

Dan di sana Dia menciptakan bulan yang bercahaya dan menjadikan matahari sebagai pelita (yang cemerlang)?

71:17

# وَاللّٰهُ اَنْۢبَتَكُمْ مِّنَ الْاَرْضِ نَبَاتًاۙ

wa**al**l*aa*hu anbatakum mina **a**l-ar*dh*i nab*aa*t*aa***n**

Dan Allah menumbuhkan kamu dari tanah, tumbuh (berangsur-angsur),

71:18

# ثُمَّ يُعِيْدُكُمْ فِيْهَا وَيُخْرِجُكُمْ اِخْرَاجًا

tsumma yu'iidukum fiih*aa* wayukhrijukum ikhr*aa*j*aa***n**

kemudian Dia akan mengembalikan kamu ke dalamnya (tanah) dan mengeluarkan kamu (pada hari Kiamat) dengan pasti.

71:19

# وَاللّٰهُ جَعَلَ لَكُمُ الْاَرْضَ بِسَاطًاۙ

wa**al**l*aa*hu ja'ala lakumu **a**l-ar*dh*a bis*aataa***n**

Dan Allah menjadikan bumi untukmu sebagai hamparan,

71:20

# لِّتَسْلُكُوْا مِنْهَا سُبُلًا فِجَاجًا ࣖ

litaslukuu minh*aa* subulan fij*aa*j*aa***n**

agar kamu dapat pergi kian kemari di jalan-jalan yang luas.

71:21

# قَالَ نُوْحٌ رَّبِّ اِنَّهُمْ عَصَوْنِيْ وَاتَّبَعُوْا مَنْ لَّمْ يَزِدْهُ مَالُهٗ وَوَلَدُهٗٓ اِلَّا خَسَارًاۚ

q*aa*la nuu*h*un rabbi innahum 'a*sh*awnii wa**i**ttaba'uu man lam yazidhu m*aa*luhu wawaladuhu ill*aa* khas*aa*r*aa***n**

Nuh berkata, “Ya Tuhanku, sesungguhnya mereka durhaka kepadaku, dan mereka mengikuti orang-orang yang harta dan anak-anaknya hanya menambah kerugian baginya,

71:22

# وَمَكَرُوْا مَكْرًا كُبَّارًاۚ

wamakaruu makran kubb*aa*r*aa***n**

dan mereka melakukan tipu daya yang sangat besar.”

71:23

# وَقَالُوْا لَا تَذَرُنَّ اٰلِهَتَكُمْ وَلَا تَذَرُنَّ وَدًّا وَّلَا سُوَاعًا ەۙ وَّلَا يَغُوْثَ وَيَعُوْقَ وَنَسْرًاۚ

waq*aa*luu l*aa* ta*dz*arunna *aa*lihatakum wal*aa* ta*dz*arunna waddan wal*aa* suw*aa*'an wal*aa* yaghuutsa waya'uuqa wanasr*aa***n**

Dan mereka berkata, “Jangan sekali-kali kamu meninggalkan (penyembahan) tuhan-tuhan kamu dan jangan pula sekali-kali kamu meninggalkan (penyembahan) Wadd, dan jangan pula Suwa‘, Yagus, Ya‘uq dan Nasr.”

71:24

# وَقَدْ اَضَلُّوْا كَثِيْرًا ەۚ وَلَا تَزِدِ الظّٰلِمِيْنَ اِلَّا ضَلٰلًا

waqad a*dh*alluu katsiiran wal*aa* tazidi **al***zhzhaa*limiina ill*aa* *dh*al*aa*l*aa***n**

Dan sungguh, mereka telah menyesatkan banyak orang; dan janganlah Engkau tambahkan bagi orang-orang yang zalim itu selain kesesatan.

71:25

# مِمَّا خَطِيْۤـٰٔتِهِمْ اُغْرِقُوْا فَاُدْخِلُوْا نَارًا ەۙ فَلَمْ يَجِدُوْا لَهُمْ مِّنْ دُوْنِ اللّٰهِ اَنْصَارًا

mimm*aa* kha*th*ii-*aa*tihim ughriquu faudkhiluu n*aa*ran falam yajiduu lahum min duuni **al**l*aa*hi an*shaa*r*aa***n**

Disebabkan kesalahan-kesalahan mereka, mereka ditenggelamkan lalu dimasukkan ke neraka, maka mereka tidak mendapat penolong selain Allah.

71:26

# وَقَالَ نُوْحٌ رَّبِّ لَا تَذَرْ عَلَى الْاَرْضِ مِنَ الْكٰفِرِيْنَ دَيَّارًا

waq*aa*la nuu*h*un rabbi l*aa* ta*dz*ar 'al*aa* **a**l-ar*dh*i mina **a**lk*aa*firiina dayy*aa*r*aa***n**

Dan Nuh berkata, “Ya Tuhanku, janganlah Engkau biarkan seorang pun di antara orang-orang kafir itu tinggal di atas bumi.

71:27

# اِنَّكَ اِنْ تَذَرْهُمْ يُضِلُّوْا عِبَادَكَ وَلَا يَلِدُوْٓا اِلَّا فَاجِرًا كَفَّارًا

innaka in ta*dz*arhum yu*dh*illuu 'ib*aa*daka wal*aa* yaliduu ill*aa* f*aa*jiran kaff*aa*r*aa***n**

Sesungguhnya jika Engkau biarkan mereka tinggal, niscaya mereka akan menyesatkan hamba-hamba-Mu, dan mereka hanya akan melahirkan anak-anak yang jahat dan tidak tahu bersyukur.

71:28

# رَبِّ اغْفِرْ لِيْ وَلِوَالِدَيَّ وَلِمَنْ دَخَلَ بَيْتِيَ مُؤْمِنًا وَّلِلْمُؤْمِنِيْنَ وَالْمُؤْمِنٰتِۗ وَلَا تَزِدِ الظّٰلِمِيْنَ اِلَّا تَبَارًا ࣖ

rabbi ighfir lii waliw*aa*lidayya waliman dakhala baytiya mu/minan walilmu/miniina wa**a**lmu/min*aa*ti wal*aa* tazidi **al***zhzhaa*limiina ill*aa* tab*aa*ran

Ya Tuhanku, ampunilah aku, ibu bapakku, dan siapa pun yang memasuki rumahku dengan beriman dan semua orang yang beriman laki-laki dan perempuan. Dan janganlah Engkau tambahkan bagi orang-orang yang zalim itu selain kehancuran.”

<!--EndFragment-->