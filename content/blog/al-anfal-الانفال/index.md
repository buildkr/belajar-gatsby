---
title: (8) Al-Anfal - الانفال
date: 2021-10-27T03:24:14.134Z
ayat: 8
description: "Jumlah Ayat: 75 / Arti: Rampasan Perang"
---
<!--StartFragment-->

8:1

# يَسْـَٔلُوْنَكَ عَنِ الْاَنْفَالِۗ قُلِ الْاَنْفَالُ لِلّٰهِ وَالرَّسُوْلِۚ فَاتَّقُوا اللّٰهَ وَاَصْلِحُوْا ذَاتَ بَيْنِكُمْ ۖوَاَطِيْعُوا اللّٰهَ وَرَسُوْلَهٗٓ اِنْ كُنْتُمْ مُّؤْمِنِيْنَ

yas-aluunaka 'ani **a**l-anf*aa*li quli **a**l-anf*aa*lu lill*aa*hi wa**al**rrasuuli fa**i**ttaquu **al**l*aa*ha wa-a*sh*li*h*uu *dzaa*ta baynikum wa-a*t*

Mereka menanyakan kepadamu (Muhammad) tentang (pembagian) harta rampasan perang. Katakanlah, “Harta rampasan perang itu milik Allah dan Rasul (menurut ketentuan Allah dan Rasul-Nya), maka bertakwalah kepada Allah dan perbaikilah hubungan di antara sesamam

8:2

# اِنَّمَا الْمُؤْمِنُوْنَ الَّذِيْنَ اِذَا ذُكِرَ اللّٰهُ وَجِلَتْ قُلُوْبُهُمْ وَاِذَا تُلِيَتْ عَلَيْهِمْ اٰيٰتُهٗ زَادَتْهُمْ اِيْمَانًا وَّعَلٰى رَبِّهِمْ يَتَوَكَّلُوْنَۙ

innam*aa* **a**lmu/minuuna **al**la*dz*iina i*dzaa* *dz*ukira **al**l*aa*hu wajilat quluubuhum wa-i*dzaa* tuliyat 'alayhim *aa*y*aa*tuhu z*aa*dat-hum iim*aa*nan wa'al

Sesungguhnya orang-orang yang beriman adalah mereka yang apabila disebut nama Allah gemetar hatinya, dan apabila dibacakan ayat-ayat-Nya kepada mereka, bertambah (kuat) imannya dan hanya kepada Tuhan mereka bertawakal,

8:3

# الَّذِيْنَ يُقِيْمُوْنَ الصَّلٰوةَ وَمِمَّا رَزَقْنٰهُمْ يُنْفِقُوْنَۗ

**al**la*dz*iina yuqiimuuna **al***shsh*al*aa*ta wamimm*aa *razaqn*aa*hum yunfiquun**a**

(Yaitu) orang-orang yang melaksanakan salat dan yang menginfakkan sebagian dari rezeki yang Kami berikan kepada mereka.

8:4

# اُولٰۤىِٕكَ هُمُ الْمُؤْمِنُوْنَ حَقًّاۗ لَهُمْ دَرَجٰتٌ عِنْدَ رَبِّهِمْ وَمَغْفِرَةٌ وَّرِزْقٌ كَرِيْمٌۚ

ul*aa*-ika humu **a**lmu/minuuna *h*aqqan lahum daraj*aa*tun 'inda rabbihim wamaghfiratun warizqun kariim**un**

Mereka itulah orang-orang yang benar-benar beriman. Mereka akan memperoleh derajat (tinggi) di sisi Tuhannya dan ampunan serta rezeki (nikmat) yang mulia.

8:5

# كَمَآ اَخْرَجَكَ رَبُّكَ مِنْۢ بَيْتِكَ بِالْحَقِّۖ وَاِنَّ فَرِيْقًا مِّنَ الْمُؤْمِنِيْنَ لَكٰرِهُوْنَ

kam*aa* akhrajaka rabbuka min baytika bi**a**l*h*aqqi wa-inna fariiqan mina **a**lmu/miniina lak*aa*rihuun**a**

Sebagaimana Tuhanmu menyuruhmu pergi dari rumahmu dengan kebenaran, meskipun sesungguhnya sebagian dari orang-orang yang beriman itu tidak menyukainya,

8:6

# يُجَادِلُوْنَكَ فِى الْحَقِّ بَعْدَمَا تَبَيَّنَ كَاَنَّمَا يُسَاقُوْنَ اِلَى الْمَوْتِ وَهُمْ يَنْظُرُوْنَ ۗ

yuj*aa*diluunaka fii **a**l*h*aqqi ba'da m*aa* tabayyana ka-annam*aa* yus*aa*quuna il*aa* **a**lmawti wahum yan*zh*uruun**a**

mereka membantahmu (Muhammad) tentang kebenaran setelah nyata (bahwa mereka pasti menang), seakan-akan mereka dihalau kepada kematian, sedang mereka melihat (sebab kematian itu).

8:7

# وَاِذْ يَعِدُكُمُ اللّٰهُ اِحْدَى الطَّاۤىِٕفَتَيْنِ اَنَّهَا لَكُمْ وَتَوَدُّوْنَ اَنَّ غَيْرَ ذَاتِ الشَّوْكَةِ تَكُوْنُ لَكُمْ وَيُرِيْدُ اللّٰهُ اَنْ يُّحِقَّ الْحَقَّ بِكَلِمٰتِهٖ وَيَقْطَعَ دَابِرَ الْكٰفِرِيْنَۙ

wa-i*dz* ya'idukumu **al**l*aa*hu i*h*d*aa* **al***ththaa*\-ifatayni annah*aa *lakum watawadduuna anna ghayra* dzaa*ti **al**sysyawkati takuunu lakum wayuriidu **al**l

Dan (ingatlah) ketika Allah menjanjikan kepadamu bahwa salah satu dari dua golongan (yang kamu hadapi) adalah untukmu, sedang kamu menginginkan bahwa yang tidak mempunyai kekuatan senjatalah untukmu. Tetapi Allah hendak membenarkan yang benar dengan ayat-

8:8

# لِيُحِقَّ الْحَقَّ وَيُبْطِلَ الْبَاطِلَ وَلَوْ كَرِهَ الْمُجْرِمُوْنَۚ

liyu*h*iqqa **a**l*h*aqqa wayub*th*ila **a**lb*aath*ila walaw kariha **a**lmujrimuun**a**

agar Allah memperkuat yang hak (Islam) dan menghilangkan yang batil (syirik) walaupun orang-orang yang berdosa (musyrik) itu tidak menyukainya.

8:9

# اِذْ تَسْتَغِيْثُوْنَ رَبَّكُمْ فَاسْتَجَابَ لَكُمْ اَنِّيْ مُمِدُّكُمْ بِاَلْفٍ مِّنَ الْمَلٰۤىِٕكَةِ مُرْدِفِيْنَ

i*dz* tastaghiitsuuna rabbakum fa**i**staj*aa*ba lakum annii mumiddukum bi-alfin mina **a**lmal*aa*-ikati murdifiin**a**

(Ingatlah), ketika kamu memohon pertolongan kepada Tuhanmu, lalu diperkenankan-Nya bagimu, “Sungguh, Aku akan mendatangkan bala bantuan kepadamu dengan seribu malaikat yang datang berturut-turut.”

8:10

# وَمَا جَعَلَهُ اللّٰهُ اِلَّا بُشْرٰى وَلِتَطْمَىِٕنَّ بِهٖ قُلُوْبُكُمْۗ وَمَا النَّصْرُ اِلَّا مِنْ عِنْدِ اللّٰهِ ۗاِنَّ اللّٰهَ عَزِيْزٌ حَكِيْمٌ ࣖ

wam*aa* ja'alahu **al**l*aa*hu ill*aa* busyr*aa* walita*th*ma-inna bihi quluubukum wam*aa* **al**nna*sh*ru ill*aa* min 'indi **al**l*aa*hi inna **al**l*aa*

*Dan tidaklah Allah menjadikannya melainkan sebagai kabar gembira agar hatimu menjadi tenteram karenanya. Dan kemenangan itu hanyalah dari sisi Allah. Sungguh, Allah Mahaperkasa, Mahabijaksana.*

8:11

# اِذْ يُغَشِّيْكُمُ النُّعَاسَ اَمَنَةً مِّنْهُ وَيُنَزِّلُ عَلَيْكُمْ مِّنَ السَّمَاۤءِ مَاۤءً لِّيُطَهِّرَكُمْ بِهٖ وَيُذْهِبَ عَنْكُمْ رِجْزَ الشَّيْطٰنِ وَلِيَرْبِطَ عَلٰى قُلُوْبِكُمْ وَيُثَبِّتَ بِهِ الْاَقْدَامَۗ

i*dz* yughasysyiikumu **al**nnu'*aa*sa amanatan minhu wayunazzilu 'alaykum mina **al**ssam*aa*-i m*aa*-an liyu*th*ahhirakum bihi wayu*dz*hiba 'ankum rijza **al**sysyay*thaa*n

(Ingatlah), ketika Allah membuat kamu mengantuk untuk memberi ketenteraman dari-Nya, dan Allah menurunkan air (hujan) dari langit kepadamu untuk menyucikan kamu dengan (hujan) itu dan menghilangkan gangguan-gangguan setan dari dirimu dan untuk menguatkan

8:12

# اِذْ يُوْحِيْ رَبُّكَ اِلَى الْمَلٰۤىِٕكَةِ اَنِّيْ مَعَكُمْ فَثَبِّتُوا الَّذِيْنَ اٰمَنُوْاۗ سَاُلْقِيْ فِيْ قُلُوْبِ الَّذِيْنَ كَفَرُوا الرُّعْبَ فَاضْرِبُوْا فَوْقَ الْاَعْنَاقِ وَاضْرِبُوْا مِنْهُمْ كُلَّ بَنَانٍۗ

i*dz* yuu*h*ii rabbuka il*aa* **a**lmal*aa*-ikati annii ma'akum fatsabbituu **al**la*dz*iina *aa*manuu saulqii fii quluubi **al**la*dz*iina kafaruu **al**rru'ba fa

(Ingatlah), ketika Tuhanmu mewahyukan kepada para malaikat, “Sesungguhnya Aku bersama kamu, maka teguhkanlah (pendirian) orang-orang yang telah beriman.” Kelak akan Aku berikan rasa ketakutan ke dalam hati orang-orang kafir, maka pukullah di atas leher me

8:13

# ذٰلِكَ بِاَنَّهُمْ شَاۤقُّوا اللّٰهَ وَرَسُوْلَهٗۚ وَمَنْ يُّشَاقِقِ اللّٰهَ وَرَسُوْلَهٗ فَاِنَّ اللّٰهَ شَدِيْدُ الْعِقَابِ

*dzaa*lika bi-annahum sy*aa*qquu **al**l*aa*ha warasuulahu waman yusy*aa*qiqi **al**l*aa*ha warasuulahu fa-inna **al**l*aa*ha syadiidu **a**l'iq*aa*b**i**

(Ketentuan) yang demikian itu adalah karena sesungguhnya mereka menentang Allah dan Rasul-Nya; dan barangsiapa menentang Allah dan Rasul-Nya, sungguh, Allah sangat keras siksa-Nya.

8:14

# ذٰلِكُمْ فَذُوْقُوْهُ وَاَنَّ لِلْكٰفِرِيْنَ عَذَابَ النَّارِ

*dzaa*likum fa*dz*uuquuhu wa-anna lilk*aa*firiina 'a*dzaa*ba **al**nn*aa*r**i**

Demikianlah (hukuman dunia yang ditimpakan atasmu), maka rasakanlah hukuman itu. Sesungguhnya bagi orang-orang kafir ada (lagi) azab neraka.

8:15

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْٓا اِذَا لَقِيْتُمُ الَّذِيْنَ كَفَرُوْا زَحْفًا فَلَا تُوَلُّوْهُمُ الْاَدْبَارَۚ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu i*dzaa* laqiitumu **al**la*dz*iina kafaruu za*h*fan fal*aa* tuwalluuhumu **a**l-adb*aa*r**a**

Wahai orang yang beriman! Apabila kamu bertemu dengan orang-orang kafir yang akan menyerangmu, maka janganlah kamu berbalik membelakangi mereka (mundur).

8:16

# وَمَنْ يُّوَلِّهِمْ يَوْمَىِٕذٍ دُبُرَهٗٓ اِلَّا مُتَحَرِّفًا لِّقِتَالٍ اَوْ مُتَحَيِّزًا اِلٰى فِئَةٍ فَقَدْ بَاۤءَ بِغَضَبٍ مِّنَ اللّٰهِ وَمَأْوٰىهُ جَهَنَّمُ ۗ وَبِئْسَ الْمَصِيْرُ

waman yuwallihim yawma-i*dz*in duburahu ill*aa* muta*h*arrifan liqit*aa*lin aw muta*h*ayyizan il*aa* fi-atin faqad b*aa*-a bigha*dh*abin mina **al**l*aa*hi wama/w*aa*hu jahannamu wabi/sa

Dan barangsiapa mundur pada waktu itu, kecuali berbelok untuk (siasat) perang atau hendak menggabungkan diri dengan pasukan yang lain, maka sungguh, orang itu kembali dengan membawa kemurkaan dari Allah, dan tempatnya ialah neraka Jahanam, seburuk-buruk t

8:17

# فَلَمْ تَقْتُلُوْهُمْ وَلٰكِنَّ اللّٰهَ قَتَلَهُمْۖ وَمَا رَمَيْتَ اِذْ رَمَيْتَ وَلٰكِنَّ اللّٰهَ رَمٰىۚ وَلِيُبْلِيَ الْمُؤْمِنِيْنَ مِنْهُ بَلَاۤءً حَسَنًاۗ اِنَّ اللّٰهَ سَمِيْعٌ عَلِيْمٌ

falam taqtuluuhum wal*aa*kinna **al**l*aa*ha qatalahum wam*aa* ramayta i*dz* ramayta wal*aa*kinna **al**l*aa*ha ram*aa* waliyubliya **a**lmu/miniina minhu bal*aa*-an *h*as

Maka (sebenarnya) bukan kamu yang membunuh mereka, melainkan Allah yang membunuh mereka, dan bukan engkau yang melempar ketika engkau melempar, tetapi Allah yang melempar. (Allah berbuat demikian untuk membinasakan mereka) dan untuk memberi kemenangan kep

8:18

# ذٰلِكُمْ وَاَنَّ اللّٰهَ مُوْهِنُ كَيْدِ الْكٰفِرِيْنَ

*dzaa*likum wa-anna **al**l*aa*ha muuhinu kaydi **a**lk*aa*firiin**a**

Demikianlah (karunia Allah yang dilimpahkan kepadamu), dan sungguh, Allah melemahkan tipu daya orang-orang kafir.

8:19

# اِنْ تَسْتَفْتِحُوْا فَقَدْ جَاۤءَكُمُ الْفَتْحُۚ وَاِنْ تَنْتَهُوْا فَهُوَ خَيْرٌ لَّكُمْۚ وَاِنْ تَعُوْدُوْا نَعُدْۚ وَلَنْ تُغْنِيَ عَنْكُمْ فِئَتُكُمْ شَيْـًٔا وَّلَوْ كَثُرَتْۙ وَاَنَّ اللّٰهَ مَعَ الْمُؤْمِنِيْنَ ࣖ

in tastafti*h*uu faqad j*aa*-akumu **a**lfat*h*u wa-in tantahuu fahuwa khayrun lakum wa-in ta'uuduu na'ud walan tughniya 'ankum fi-atukum syay-an walaw katsurat wa-anna **al**l*aa*ha ma'a **a**lmu

Jika kamu meminta keputusan, maka sesungguhnya keputusan telah datang kepadamu; dan jika kamu berhenti (memusuhi Rasul), maka itulah yang lebih baik bagimu; dan jika kamu kembali, niscaya Kami kembali (memberi pertolongan); dan pasukanmu tidak akan dapat

8:20

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْٓا اَطِيْعُوا اللّٰهَ وَرَسُوْلَهٗ وَلَا تَوَلَّوْا عَنْهُ وَاَنْتُمْ تَسْمَعُوْنَ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu a*th*ii'uu **al**l*aa*ha warasuulahu wal*aa* tawallaw 'anhu wa-antum tasma'uun**a**

Wahai orang-orang yang beriman! Taatlah kepada Allah dan Rasul-Nya, dan janganlah kamu berpaling dari-Nya, padahal kamu mendengar (perintah-perintah-Nya),

8:21

# وَلَا تَكُوْنُوْا كَالَّذِيْنَ قَالُوْا سَمِعْنَا وَهُمْ لَا يَسْمَعُوْنَۚ

wal*aa* takuunuu ka**a**lla*dz*iina q*aa*luu sami'n*aa* wahum l*aa* yasma'uun**a**

dan janganlah kamu menjadi seperti orang-orang (munafik) yang berkata, “Kami mendengarkan,” padahal mereka tidak mendengarkan (karena hati mereka mengingkarinya).

8:22

# ۞ اِنَّ شَرَّ الدَّوَاۤبِّ عِنْدَ اللّٰهِ الصُّمُّ الْبُكْمُ الَّذِيْنَ لَا يَعْقِلُوْنَ

inna syarra **al**ddaw*aa*bbi 'inda **al**l*aa*hi **al***shsh*ummu **a**lbukmu **al**la*dz*iina l*aa* ya'qiluun**a**

Sesungguhnya makhluk bergerak yang bernyawa yang paling buruk dalam pandangan Allah ialah mereka yang tuli dan bisu (tidak mendengar dan memahami kebenaran) yaitu orang-orang yang tidak mengerti.

8:23

# وَلَوْ عَلِمَ اللّٰهُ فِيْهِمْ خَيْرًا لَّاَسْمَعَهُمْۗ وَلَوْ اَسْمَعَهُمْ لَتَوَلَّوْا وَّهُمْ مُّعْرِضُوْنَ

walaw 'alima **al**l*aa*hu fiihim khayran la-asma'ahum walaw asma'ahum latawallaw wahum mu'ri*dh*uun**a**

Dan sekiranya Allah mengetahui ada kebaikan pada mereka, tentu Dia jadikan mereka dapat mendengar. Dan jika Allah menjadikan mereka dapat mendengar, niscaya mereka berpaling, sedang mereka memalingkan diri.

8:24

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوا اسْتَجِيْبُوْا لِلّٰهِ وَلِلرَّسُوْلِ اِذَا دَعَاكُمْ لِمَا يُحْيِيْكُمْۚ وَاعْلَمُوْٓا اَنَّ اللّٰهَ يَحُوْلُ بَيْنَ الْمَرْءِ وَقَلْبِهٖ وَاَنَّهٗٓ اِلَيْهِ تُحْشَرُوْنَ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu istajiibuu lill*aa*hi wali**l**rrasuuli i*dzaa* da'*aa*kum lim*aa* yu*h*yiikum wa**i**'lamuu anna **al**l*aa*h

Wahai orang-orang yang beriman! Penuhilah seruan Allah dan Rasul, apabila dia menyerumu kepada sesuatu yang memberi kehidupan kepadamu, dan ketahuilah bahwa sesungguhnya Allah membatasi antara manusia dan hatinya dan sesungguhnya kepada-Nyalah kamu akan d

8:25

# وَاتَّقُوْا فِتْنَةً لَّا تُصِيْبَنَّ الَّذِيْنَ ظَلَمُوْا مِنْكُمْ خَاۤصَّةً ۚوَاعْلَمُوْٓا اَنَّ اللّٰهَ شَدِيْدُ الْعِقَابِ

wa**i**ttaquu fitnatan l*aa* tu*sh*iibanna **al**la*dz*iina *zh*alamuu minkum kh*aass*atan wa**i**'lamuu anna **al**l*aa*ha syadiidu **a**l'iq*aa*b

Dan peliharalah dirimu dari siksaan yang tidak hanya menimpa orang-orang yang zalim saja di antara kamu. Ketahuilah bahwa Allah sangat keras siksa-Nya.

8:26

# وَاذْكُرُوْٓا اِذْ اَنْتُمْ قَلِيْلٌ مُّسْتَضْعَفُوْنَ فِى الْاَرْضِ تَخَافُوْنَ اَنْ يَّتَخَطَّفَكُمُ النَّاسُ فَاٰوٰىكُمْ وَاَيَّدَكُمْ بِنَصْرِهٖ وَرَزَقَكُمْ مِّنَ الطَّيِّبٰتِ لَعَلَّكُمْ تَشْكُرُوْنَ

wa**u***dz*kuruu i*dz *antum qaliilun musta*dh*'afuuna fii **a**l-ar*dh*i takh*aa*fuuna an yatakha*ththh*afakumu **al**nn*aa*su fa*aa*w*aa*kum wa-ayyadakum bina*sh*ri

Dan ingatlah ketika kamu (para Muhajirin) masih (berjumlah) sedikit, lagi tertindas di bumi (Mekah), dan kamu takut orang-orang (Mekah) akan menculik kamu, maka Dia memberi kamu tempat menetap (Madinah) dan dijadikan-Nya kamu kuat dengan pertolongan-Nya d

8:27

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْا لَا تَخُوْنُوا اللّٰهَ وَالرَّسُوْلَ وَتَخُوْنُوْٓا اَمٰنٰتِكُمْ وَاَنْتُمْ تَعْلَمُوْنَ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu l*aa* takhuunuu **al**l*aa*ha wa**al**rrasuula watakhuunuu am*aa*n*aa*tikum wa-antum ta'lamuun**a**

Wahai orang-orang yang beriman! Janganlah kamu mengkhianati Allah dan Rasul dan (juga) janganlah kamu mengkhianati amanat yang dipercayakan kepadamu, sedang kamu mengetahui.

8:28

# وَاعْلَمُوْٓا اَنَّمَآ اَمْوَالُكُمْ وَاَوْلَادُكُمْ فِتْنَةٌ ۙوَّاَنَّ اللّٰهَ عِنْدَهٗٓ اَجْرٌ عَظِيْمٌ ࣖ

wa**i**'lamuu annam*aa* amw*aa*lukum wa-awl*aa*dukum fitnatun wa-anna **al**l*aa*ha 'indahu ajrun 'a*zh*iim**un**

Dan ketahuilah bahwa hartamu dan anak-anakmu itu hanyalah sebagai cobaan dan sesungguhnya di sisi Allah ada pahala yang besar.

8:29

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْٓا اِنْ تَتَّقُوا اللّٰهَ يَجْعَلْ لَّكُمْ فُرْقَانًا وَّيُكَفِّرْ عَنْكُمْ سَيِّاٰتِكُمْ وَيَغْفِرْ لَكُمْۗ وَاللّٰهُ ذُو الْفَضْلِ الْعَظِيْمِ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*m*aa*nuu in tattaquu **al**l*aa*ha yaj'al lakum furq*aa*nan wayukaffir 'ankum sayyi-*aa*tikum wayaghfir lakum wa**al**l*aa*hu *dz*

*Wahai orang-orang yang beriman! Jika kamu bertakwa kepada Allah, niscaya Dia akan memberikan furqan (kemampuan membedakan antara yang hak dan batil) kepadamu dan menghapus segala kesalahanmu dan mengampuni (dosa-dosa)mu. Allah memiliki karunia yang besar.*

8:30

# وَاِذْ يَمْكُرُ بِكَ الَّذِيْنَ كَفَرُوْا لِيُثْبِتُوْكَ اَوْ يَقْتُلُوْكَ اَوْ يُخْرِجُوْكَۗ وَيَمْكُرُوْنَ وَيَمْكُرُ اللّٰهُ ۗوَاللّٰهُ خَيْرُ الْمَاكِرِيْنَ

wa-i*dz* yamkuru bika **al**la*dz*iina kafaruu liyutsbituuka aw yaqtuluuka aw yukhrijuuka wayamkuruuna wayamkuru **al**l*aa*hu wa**al**l*aa*hu khayru **a**lm*aa*kiriin**a**

**Dan (ingatlah), ketika orang-orang kafir (Quraisy) memikirkan tipu daya terhadapmu (Muhammad) untuk menangkap dan memenjarakanmu atau membunuhmu, atau mengusirmu. Mereka membuat tipu daya dan Allah menggagalkan tipu daya itu. Allah adalah sebaik-baik pemb**

8:31

# وَاِذَا تُتْلٰى عَلَيْهِمْ اٰيٰتُنَا قَالُوْا قَدْ سَمِعْنَا لَوْ نَشَاۤءُ لَقُلْنَا مِثْلَ هٰذَآ ۙاِنْ هٰذَآ اِلَّآ اَسَاطِيْرُ الْاَوَّلِيْنَ

wa-i*dzaa* tutl*aa* 'alayhim *aa*y*aa*tun*aa* q*aa*luu qad sami'n*aa* law nasy*aa*u laquln*aa* mitsla h*aadzaa* in h*aadzaa* ill*aa* as*aath*iiru **a**l-awwaliin**a**

**Dan apabila ayat-ayat Kami dibacakan kepada mereka, mereka berkata, “Sesungguhnya kami telah mendengar (ayat-ayat seperti ini), jika kami menghendaki niscaya kami dapat membacakan yang seperti ini. (Al-Qur'an) ini tidak lain hanyalah dongeng orang-orang t**

8:32

# وَاِذْ قَالُوا اللهم اِنْ كَانَ هٰذَا هُوَ الْحَقَّ مِنْ عِنْدِكَ فَاَمْطِرْ عَلَيْنَا حِجَارَةً مِّنَ السَّمَاۤءِ اَوِ ائْتِنَا بِعَذَابٍ اَلِيْمٍ

wa-i*dz* q*aa*luu **al**l*aa*humma in k*aa*na h*aadzaa* huwa **a**l*h*aqqa min 'indika fa-am*th*ir 'alayn*aa* *h*ij*aa*ratan mina **al**ssam*aa*-i awi i/tin*aa*

Dan (ingatlah), ketika mereka (orang-orang musyrik) berkata, “Ya Allah, jika (Al-Qur'an) ini benar (wahyu) dari Engkau, maka hujanilah kami dengan batu dari langit, atau datangkanlah kepada kami azab yang pedih.”

8:33

# وَمَا كَانَ اللّٰهُ لِيُعَذِّبَهُمْ وَاَنْتَ فِيْهِمْۚ وَمَا كَانَ اللّٰهُ مُعَذِّبَهُمْ وَهُمْ يَسْتَغْفِرُوْنَ

wam*aa* k*aa*na **al**l*aa*hu liyu'a*dzdz*ibahum wa-anta fiihim wam*aa* k*aa*na **al**l*aa*hu mu'a*dzdz*ibahum wahum yastaghfiruun**a**

Tetapi Allah tidak akan menghukum mereka, selama engkau (Muhammad) berada di antara mereka. Dan tidaklah (pula) Allah akan menghukum mereka, sedang mereka (masih) memohon ampunan.

8:34

# وَمَا لَهُمْ اَلَّا يُعَذِّبَهُمُ اللّٰهُ وَهُمْ يَصُدُّوْنَ عَنِ الْمَسْجِدِ الْحَرَامِ وَمَا كَانُوْٓا اَوْلِيَاۤءَهٗۗ اِنْ اَوْلِيَاۤؤُهٗٓ اِلَّا الْمُتَّقُوْنَ وَلٰكِنَّ اَكْثَرَهُمْ لَا يَعْلَمُوْنَ

wam*aa* lahum **al**l*aa* yu'a*dzdz*ibahumu **al**l*aa*hu wahum ya*sh*udduuna 'ani **a**lmasjidi **a**l*h*ar*aa*mi wam*aa* k*aa*nuu awliy*aa*-ahu in awli

Dan mengapa Allah tidak menghukum mereka padahal mereka menghalang-halangi (orang) untuk (mendatangi) Masjidilharam dan mereka bukanlah orang-orang yang berhak menguasainya? Orang yang berhak menguasai(nya), hanyalah orang-orang yang bertakwa, tetapi keba

8:35

# وَمَا كَانَ صَلَاتُهُمْ عِنْدَ الْبَيْتِ اِلَّا مُكَاۤءً وَّتَصْدِيَةًۗ فَذُوْقُوا الْعَذَابَ بِمَا كُنْتُمْ تَكْفُرُوْنَ

wam*aa* k*aa*na *sh*al*aa*tuhum 'inda **a**lbayti ill*aa* muk*aa*-an wata*sh*diyatan fa*dz*uuquu **a**l'a*dzaa*ba bim*aa* kuntum takfuruun**a**

Dan salat mereka di sekitar Baitullah itu, tidak lain hanyalah siulan dan tepuk tangan. Maka rasakanlah azab disebabkan kekafiranmu itu.

8:36

# اِنَّ الَّذِيْنَ كَفَرُوْا يُنْفِقُوْنَ اَمْوَالَهُمْ لِيَصُدُّوْا عَنْ سَبِيْلِ اللّٰهِ ۗفَسَيُنْفِقُوْنَهَا ثُمَّ تَكُوْنُ عَلَيْهِمْ حَسْرَةً ثُمَّ يُغْلَبُوْنَ ەۗ وَالَّذِيْنَ كَفَرُوْٓا اِلٰى جَهَنَّمَ يُحْشَرُوْنَۙ

inna **al**la*dz*iina kafaruu yunfiquuna amw*aa*lahum liya*sh*udduu 'an sabiili **al**l*aa*hi fasayunfiquunah*aa* tsumma takuunu 'alayhim *h*asratan tsumma yughlabuuna wa**a**lla*dz*

*Sesungguhnya orang-orang yang kafir itu, menginfakkan harta mereka untuk menghalang-halangi (orang) dari jalan Allah. Mereka akan (terus) menginfakkan harta itu, kemudian mereka akan menyesal sendiri, dan akhirnya mereka akan dikalahkan. Ke dalam neraka J*

8:37

# لِيَمِيْزَ اللّٰهُ الْخَبِيْثَ مِنَ الطَّيِّبِ وَيَجْعَلَ الْخَبِيْثَ بَعْضَهٗ عَلٰى بَعْضٍ فَيَرْكُمَهٗ جَمِيْعًا فَيَجْعَلَهٗ فِيْ جَهَنَّمَۗ اُولٰۤىِٕكَ هُمُ الْخٰسِرُوْنَ ࣖ

liyamiiza **al**l*aa*hu **a**lkhabiitsa mina **al***ththh*ayyibi wayaj'ala **a**lkhabiitsa ba'*dh*ahu 'al*aa *ba'*dh*in fayarkumahu jamii'an fayaj'alahu fii jahannama ul*aa*

agar Allah memisahkan (golongan) yang buruk dari yang baik dan menjadikan (golongan) yang buruk itu sebagiannya di atas yang lain, lalu kesemuanya ditumpukkan-Nya, dan dimasukkan-Nya ke dalam neraka Jahanam. Mereka itulah orang-orang yang rugi.

8:38

# قُلْ لِّلَّذِيْنَ كَفَرُوْٓا اِنْ يَّنْتَهُوْا يُغْفَرْ لَهُمْ مَّا قَدْ سَلَفَۚ وَاِنْ يَّعُوْدُوْا فَقَدْ مَضَتْ سُنَّتُ الْاَوَّلِيْنَ

qul lilla*dz*iina kafaruu in yantahuu yughfar lahum m*aa* qad salafa wa-in ya'uuduu faqad ma*dh*at sunnatu **a**l-awwaliin**a**

Katakanlah kepada orang-orang yang kafir itu (Abu Sufyan dan kawan-kawannya), “Jika mereka berhenti (dari kekafirannya), niscaya Allah akan mengampuni dosa-dosa mereka yang telah lalu; dan jika mereka kembali lagi (memerangi Nabi) sungguh, berlaku (kepada

8:39

# وَقَاتِلُوْهُمْ حَتّٰى لَا تَكُوْنَ فِتْنَةٌ وَّيَكُوْنَ الدِّيْنُ كُلُّهٗ لِلّٰهِۚ فَاِنِ انْتَهَوْا فَاِنَّ اللّٰهَ بِمَا يَعْمَلُوْنَ بَصِيْرٌ

waq*aa*tiluuhum *h*att*aa* l*aa* takuuna fitnatun wayakuuna **al**ddiinu kulluhu lill*aa*hi fa-ini intahaw fa-inna **al**l*aa*ha bim*aa* ya'maluuna ba*sh*iir**un**

Dan perangilah mereka itu sampai tidak ada lagi fitnah, dan agama hanya bagi Allah semata. Jika mereka berhenti (dari kekafiran), maka sesungguhnya Allah Maha Melihat apa yang mereka kerjakan.

8:40

# وَاِنْ تَوَلَّوْا فَاعْلَمُوْٓا اَنَّ اللّٰهَ مَوْلٰىكُمْ ۗنِعْمَ الْمَوْلٰى وَنِعْمَ النَّصِيْرُ ۔

wa-in tawallaw fa**i**'lamuu anna **al**l*aa*ha mawl*aa*kum ni'ma **a**lmawl*aa* wani'ma **al**nna*sh*iir**u**

Dan jika mereka berpaling, maka ketahuilah bahwa sesungguhnya Allah pelindungmu. Dia adalah sebaik-baik pelindung dan sebaik-baik penolong.

8:41

# ۞ وَاعْلَمُوْٓا اَنَّمَا غَنِمْتُمْ مِّنْ شَيْءٍ فَاَنَّ لِلّٰهِ خُمُسَهٗ وَلِلرَّسُوْلِ وَلِذِى الْقُرْبٰى وَالْيَتٰمٰى وَالْمَسٰكِيْنِ وَابْنِ السَّبِيْلِ اِنْ كُنْتُمْ اٰمَنْتُمْ بِاللّٰهِ وَمَآ اَنْزَلْنَا عَلٰى عَبْدِنَا يَوْمَ الْفُرْقَانِ يَوْمَ ال

wa**i**'lamuu annam*aa* ghanimtum min syay-in fa-anna lill*aa*hi khumusahu wali**l**rrasuuli wali*dz*ii **a**lqurb*aa* wa**a**lyat*aa*m*aa* wa**a**lmas*aa*

Dan ketahuilah, sesungguhnya segala yang kamu peroleh sebagai rampasan perang, maka seperlima untuk Allah, Rasul, kerabat Rasul, anak yatim, orang miskin dan ibnu sabil, (demikian) jika kamu beriman kepada Allah dan kepada apa yang Kami turunkan kepada ha

8:42

# اِذْ اَنْتُمْ بِالْعُدْوَةِ الدُّنْيَا وَهُمْ بِالْعُدْوَةِ الْقُصْوٰى وَالرَّكْبُ اَسْفَلَ مِنْكُمْۗ وَلَوْ تَوَاعَدْتُّمْ لَاخْتَلَفْتُمْ فِى الْمِيْعٰدِۙ وَلٰكِنْ لِّيَقْضِيَ اللّٰهُ اَمْرًا كَانَ مَفْعُوْلًا ەۙ لِّيَهْلِكَ مَنْ هَلَكَ عَنْۢ بَيِّنَةٍ

i*dz* antum bi**a**l'udwati **al**dduny*aa* wahum bi**a**l'udwati **a**lqu*sh*w*aa* wa**al**rrakbu asfala minkum walaw taw*aa*'adtum la**i**khtalaftum f

(Yaitu) ketika kamu berada di pinggir lembah yang dekat dan mereka berada di pinggir lembah yang jauh sedang kafilah itu berada lebih rendah dari kamu. Sekiranya kamu mengadakan persetujuan (untuk menentukan hari pertempuran), niscaya kamu berbeda pendapa

8:43

# اِذْ يُرِيْكَهُمُ اللّٰهُ فِيْ مَنَامِكَ قَلِيْلًاۗ وَلَوْ اَرٰىكَهُمْ كَثِيْرًا لَّفَشِلْتُمْ وَلَتَنَازَعْتُمْ فِى الْاَمْرِ وَلٰكِنَّ اللّٰهَ سَلَّمَۗ اِنَّهٗ عَلِيْمٌۢ بِذَاتِ الصُّدُوْرِ

i*dz* yuriikahumu **al**l*aa*hu fii man*aa*mika qaliilan walaw ar*aa*kahum katsiiran lafasyiltum walatan*aa*za'tum fii **a**l-amri wal*aa*kinna **al**l*aa*ha sallama innahu 'aliimun

(Ingatlah) ketika Allah memperlihatkan mereka di dalam mimpimu (berjumlah) sedikit. Dan sekiranya Allah memperlihatkan mereka (berjumlah) banyak tentu kamu menjadi gentar dan tentu kamu akan berbantah-bantahan dalam urusan itu, tetapi Allah telah menyelam

8:44

# وَاِذْ يُرِيْكُمُوْهُمْ اِذِ الْتَقَيْتُمْ فِيْٓ اَعْيُنِكُمْ قَلِيْلًا وَّيُقَلِّلُكُمْ فِيْٓ اَعْيُنِهِمْ لِيَقْضِيَ اللّٰهُ اَمْرًا كَانَ مَفْعُوْلًا ۗوَاِلَى اللّٰهِ تُرْجَعُ الْاُمُوْرُ ࣖ

wa-i*dz* yuriikumuuhum i*dz*i iltaqaytum fii a'yunikum qaliilan wayuqallilukum fii a'yunihim liyaq*dh*iya **al**l*aa*hu amran k*aa*na maf'uulan wa-il*aa* **al**l*aa*hi turja'u **a**

Dan ketika Allah memperlihatkan mereka kepadamu, ketika kamu berjumpa dengan mereka berjumlah sedikit menurut penglihatan matamu dan kamu diperlihatkan-Nya berjumlah sedikit menurut penglihatan mereka, itu karena Allah berkehendak melaksanakan suatu urusa

8:45

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْٓا اِذَا لَقِيْتُمْ فِئَةً فَاثْبُتُوْا وَاذْكُرُوا اللّٰهَ كَثِيْرًا لَّعَلَّكُمْ تُفْلِحُوْنَۚ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu i*dzaa* laqiitum fi-atan fa**u**tsbutuu wa**u***dz*kuruu **al**l*aa*ha katsiiran la'allakum tufli*h*uun**a**

**Wahai orang-orang yang beriman! Apabila kamu bertemu pasukan (musuh), maka berteguh hatilah dan sebutlah (nama) Allah banyak-banyak (berzikir dan berdoa) agar kamu beruntung.**

8:46

# وَاَطِيْعُوا اللّٰهَ وَرَسُوْلَهٗ وَلَا تَنَازَعُوْا فَتَفْشَلُوْا وَتَذْهَبَ رِيْحُكُمْ وَاصْبِرُوْاۗ اِنَّ اللّٰهَ مَعَ الصّٰبِرِيْنَۚ

wa-a*th*ii'uu **al**l*aa*ha warasuulahu wal*aa* tan*aa*za'uu fatafsyaluu wata*dz*haba rii*h*ukum wa**i***sh*biruu inna **al**l*aa*ha ma'a **al***shshaa*biriin

Dan taatilah Allah dan Rasul-Nya dan janganlah kamu berselisih, yang menyebabkan kamu menjadi gentar dan kekuatanmu hilang dan bersabarlah. Sungguh, Allah beserta orang-orang sabar.

8:47

# وَلَا تَكُوْنُوْا كَالَّذِيْنَ خَرَجُوْا مِنْ دِيَارِهِمْ بَطَرًا وَّرِئَاۤءَ النَّاسِ وَيَصُدُّوْنَ عَنْ سَبِيْلِ اللّٰهِ ۗوَاللّٰهُ بِمَايَعْمَلُوْنَ مُحِيْطٌ

wal*aa* takuunuu ka**a**lla*dz*iina kharajuu min diy*aa*rihim ba*th*aran wari-*aa*-a **al**nn*aa*si waya*sh*udduuna 'an sabiili **al**l*aa*hi wa**al**l*aa*hu

Dan janganlah kamu seperti orang-orang yang keluar dari kampung halamannya dengan rasa angkuh dan ingin dipuji orang (ria) serta menghalang-halangi (orang) dari jalan Allah. Allah meliputi segala yang mereka kerjakan.

8:48

# وَاِذْ زَيَّنَ لَهُمُ الشَّيْطٰنُ اَعْمَالَهُمْ وَقَالَ لَا غَالِبَ لَكُمُ الْيَوْمَ مِنَ النَّاسِ وَاِنِّيْ جَارٌ لَّكُمْۚ فَلَمَّا تَرَاۤءَتِ الْفِئَتٰنِ نَكَصَ عَلٰى عَقِبَيْهِ وَقَالَ اِنِّيْ بَرِيْۤءٌ مِّنْكُمْ اِنِّيْٓ اَرٰى مَا لَا تَرَوْنَ اِنِّيْ

wa-i*dz* zayyana lahumu **al**sysyay*thaa*nu a'm*aa*lahum waq*aa*la l*aa* gh*aa*liba lakumu **a**lyawma mina **al**nn*aa*si wa-innii j*aa*run lakum falamm*aa* tar

Dan (ingatlah) ketika setan menjadikan terasa indah bagi mereka perbuatan (dosa) mereka dan mengatakan, “Tidak ada (orang) yang dapat mengalahkan kamu pada hari ini, dan sungguh, aku adalah penolongmu.” Maka ketika kedua pasukan itu telah saling melihat (

8:49

# اِذْ يَقُوْلُ الْمُنٰفِقُوْنَ وَالَّذِيْنَ فِيْ قُلُوْبِهِمْ مَّرَضٌ غَرَّ هٰٓؤُلَاۤءِ دِيْنُهُمْۗ وَمَنْ يَّتَوَكَّلْ عَلَى اللّٰهِ فَاِنَّ اللّٰهَ عَزِيْزٌ حَكِيْمٌ

i*dz* yaquulu **a**lmun*aa*fiquuna wa**a**lla*dz*iina fii quluubihim mara*dh*un gharra h*aa*ul*aa*-i diinuhum waman yatawakkal 'al*aa* **al**l*aa*hi fa-inna **al**

(Ingatlah), ketika orang-orang munafik dan orang-orang yang ada penyakit di dalam hatinya berkata, “Mereka itu (orang mukmin) ditipu agamanya.” (Allah berfirman), “Barangsiapa bertawakal kepada Allah, ketahuilah bahwa Allah Mahaperkasa, Mahabijaksana.”

8:50

# وَلَوْ تَرٰٓى اِذْ يَتَوَفَّى الَّذِيْنَ كَفَرُوا الْمَلٰۤىِٕكَةُ يَضْرِبُوْنَ وُجُوْهَهُمْ وَاَدْبَارَهُمْۚ وَذُوْقُوْا عَذَابَ الْحَرِيْقِ

walaw tar*aa* i*dz* yatawaff*aa* **al**la*dz*iina kafaruu **a**lmal*aa*-ikatu ya*dh*ribuuna wujuuhahum wa-adb*aa*rahum wa*dz*uuquu 'a*dzaa*ba **a**l*h*ariiq**i<**

Dan sekiranya kamu melihat ketika para malaikat mencabut nyawa orang-orang yang kafir sambil memukul wajah dan punggung mereka (dan berkata), “Rasakanlah olehmu siksa neraka yang membakar.”

8:51

# ذٰلِكَ بِمَا قَدَّمَتْ اَيْدِيْكُمْ وَاَنَّ اللّٰهَ لَيْسَ بِظَلَّامٍ لِّلْعَبِيْدِۙ

*dzaa*lika bim*aa* qaddamat aydiikum wa-anna **al**l*aa*ha laysa bi*zh*all*aa*min lil'abiid**i**

Demikian itu disebabkan oleh perbuatan tanganmu sendiri. Dan sesungguhnya Allah tidak menzalimi hamba-hamba-Nya,

8:52

# كَدَأْبِ اٰلِ فِرْعَوْنَۙ وَالَّذِيْنَ مِنْ قَبْلِهِمْۗ كَفَرُوْا بِاٰيٰتِ اللّٰهِ فَاَخَذَهُمُ اللّٰهُ بِذُنُوْبِهِمْۗ اِنَّ اللّٰهَ قَوِيٌّ شَدِيْدُ الْعِقَابِ

kada/bi *aa*li fir'awna wa**a**lla*dz*iina min qablihim kafaruu bi-*aa*y*aa*ti **al**l*aa*hi fa-akha*dz*ahumu **al**l*aa*hu bi*dz*unuubihim inna **al**l*aa*ha

(Keadaan mereka) serupa dengan keadaan pengikut Fir‘aun dan orang-orang yang sebelum mereka. Mereka mengingkari ayat-ayat Allah, maka Allah menyiksa mereka disebabkan dosa-dosanya. Sungguh, Allah Mahakuat lagi sangat keras siksa-Nya.

8:53

# ذٰلِكَ بِاَنَّ اللّٰهَ لَمْ يَكُ مُغَيِّرًا نِّعْمَةً اَنْعَمَهَا عَلٰى قَوْمٍ حَتّٰى يُغَيِّرُوْا مَا بِاَنْفُسِهِمْۙ وَاَنَّ اللّٰهَ سَمِيْعٌ عَلِيْمٌۙ

*dzaa*lika bi-anna **al**l*aa*ha lam yaku mughayyiran ni'matan an'amah*aa* 'al*aa* qawmin *h*att*aa* yughayyiruu m*aa* bi-anfusihim wa-anna **al**l*aa*ha samii'un 'aliim**un**

Yang demikian itu karena sesungguhnya Allah tidak akan mengubah suatu nikmat yang telah diberikan-Nya kepada suatu kaum, hingga kaum itu mengubah apa yang ada pada diri mereka sendiri. Sungguh, Allah Maha Mendengar, Maha Mengetahui,

8:54

# كَدَأْبِ اٰلِ فِرْعَوْنَۙ وَالَّذِيْنَ مِنْ قَبْلِهِمْۚ كَذَّبُوْا بِاٰيٰتِ رَبِّهِمْ فَاَهْلَكْنٰهُمْ بِذُنُوْبِهِمْ وَاَغْرَقْنَآ اٰلَ فِرْعَوْنَۚ وَكُلٌّ كَانُوْا ظٰلِمِيْنَ

kada/bi *aa*li fir'awna wa**a**lla*dz*iina min qablihim ka*dzdz*abuu bi-*aa*y*aa*ti rabbihim fa-ahlakn*aa*hum bi*dz*unuubihim wa-aghraqn*aa* *aa*la fir'awna wakullun k*aa*nuu *zhaa*limiin

(Keadaan mereka) serupa dengan keadaan pengikut Fir‘aun dan orang-orang yang sebelum mereka. Mereka mendustakan ayat-ayat Tuhannya, maka Kami membinasakan mereka disebabkan oleh dosa-dosanya dan Kami tenggelamkan Fir‘aun dan pengikut-pengikutnya; karena m

8:55

# اِنَّ شَرَّ الدَّوَاۤبِّ عِنْدَ اللّٰهِ الَّذِيْنَ كَفَرُوْا فَهُمْ لَا يُؤْمِنُوْنَۖ

inna syarra **al**ddaw*aa*bbi 'inda **al**l*aa*hi **al**la*dz*iina kafaruu fahum l*aa* yu/minuun**a**

Sesungguhnya makhluk bergerak yang bernyawa yang paling buruk dalam pandangan Allah ialah orang-orang kafir, karena mereka tidak beriman.

8:56

# الَّذِيْنَ عَاهَدْتَّ مِنْهُمْ ثُمَّ يَنْقُضُوْنَ عَهْدَهُمْ فِيْ كُلِّ مَرَّةٍ وَّهُمْ لَا يَتَّقُوْنَ

**al**la*dz*iina '*aa*hadta minhum tsumma yanqu*dh*uuna 'ahdahum fii kulli marratin wahum l*aa* yattaquun**a**

(Yaitu) orang-orang yang terikat perjanjian dengan kamu, kemudian setiap kali berjanji mereka mengkhianati janjinya, sedang mereka tidak takut (kepada Allah).

8:57

# فَاِمَّا تَثْقَفَنَّهُمْ فِى الْحَرْبِ فَشَرِّدْ بِهِمْ مَّنْ خَلْفَهُمْ لَعَلَّهُمْ يَذَّكَّرُوْنَ

fa-imm*aa* tatsqafannahum fii **a**l*h*arbi fasyarrid bihim man khalfahum la'allahum ya*dzdz*akkaruun**a**

Maka jika engkau (Muhammad) mengungguli mereka dalam peperangan, maka cerai-beraikanlah orang-orang yang di belakang mereka dengan (menumpas) mereka, agar mereka mengambil pelajaran.

8:58

# وَاِمَّا تَخَافَنَّ مِنْ قَوْمٍ خِيَانَةً فَانْۢبِذْ اِلَيْهِمْ عَلٰى سَوَاۤءٍۗ اِنَّ اللّٰهَ لَا يُحِبُّ الْخَاۤىِٕنِيْنَ ࣖ

wa-imm*aa* takh*aa*fanna min qawmin khiy*aa*natan fa**i**nbi*dz* ilayhim 'al*aa* sawa-in inna **al**l*aa*ha l*aa* yu*h*ibbu **a**lkh*aa*-iniin**a**

Dan jika engkau (Muhammad) khawatir akan (terjadinya) pengkhianatan dari suatu golongan, maka kembalikanlah perjanjian itu kepada mereka dengan cara yang jujur. Sungguh, Allah tidak menyukai orang yang berkhianat.

8:59

# وَلَا يَحْسَبَنَّ الَّذِيْنَ كَفَرُوْا سَبَقُوْاۗ اِنَّهُمْ لَا يُعْجِزُوْنَ

wal*aa* ya*h*sabanna **al**la*dz*iina kafaruu sabaquu innahum l*aa* yu'jizuun**a**

Dan janganlah orang-orang kafir mengira, bahwa mereka akan dapat lolos (dari kekuasaan Allah). Sungguh, mereka tidak dapat melemahkan (Allah).

8:60

# وَاَعِدُّوْا لَهُمْ مَّا اسْتَطَعْتُمْ مِّنْ قُوَّةٍ وَّمِنْ رِّبَاطِ الْخَيْلِ تُرْهِبُوْنَ بِهٖ عَدُوَّ اللّٰهِ وَعَدُوَّكُمْ وَاٰخَرِيْنَ مِنْ دُوْنِهِمْۚ لَا تَعْلَمُوْنَهُمْۚ اَللّٰهُ يَعْلَمُهُمْۗ وَمَا تُنْفِقُوْا مِنْ شَيْءٍ فِيْ سَبِيْلِ اللّٰه

wa-a'idduu lahum m*aa* ista*th*a'tum min quwwatin wamin rib*aath*i **a**lkhayli turhibuuna bihi 'aduwwa **al**l*aa*hi wa'aduwwakum wa*aa*khariina min duunihim l*aa* ta'lamuunahumu **al**

Dan persiapkanlah dengan segala kemampuan untuk menghadapi mereka dengan kekuatan yang kamu miliki dan dari pasukan berkuda yang dapat menggentarkan musuh Allah, musuhmu dan orang-orang selain mereka yang kamu tidak mengetahuinya; tetapi Allah mengetahuin

8:61

# ۞ وَاِنْ جَنَحُوْا لِلسَّلْمِ فَاجْنَحْ لَهَا وَتَوَكَّلْ عَلَى اللّٰهِ ۗاِنَّهٗ هُوَ السَّمِيْعُ الْعَلِيْمُ

wa-in jana*h*uu li**l**ssalmi fa**i**jna*h* lah*aa* watawakkal 'al*aa* **al**l*aa*hi innahu huwa **al**ssamii'u **a**l'aliim**u**

Tetapi jika mereka condong kepada perdamaian, maka terimalah dan bertawakallah kepada Allah. Sungguh, Dia Maha Mendengar, Maha Mengetahui.

8:62

# وَاِنْ يُّرِيْدُوْٓا اَنْ يَّخْدَعُوْكَ فَاِنَّ حَسْبَكَ اللّٰهُ ۗهُوَ الَّذِيْٓ اَيَّدَكَ بِنَصْرِهٖ وَبِالْمُؤْمِنِيْنَۙ

wa-in yuriiduu an yakhda'uuka fa-inna *h*asbaka **al**l*aa*hu huwa **al**la*dz*ii ayyadaka bina*sh*rihi wabi**a**lmu/miniin**a**

Dan jika mereka hendak menipumu, maka sesungguhnya cukuplah Allah (menjadi pelindung) bagimu. Dialah yang memberikan kekuatan kepadamu dengan pertolongan-Nya dan dengan (dukungan) orang-orang mukmin,

8:63

# وَاَلَّفَ بَيْنَ قُلُوْبِهِمْۗ لَوْاَنْفَقْتَ مَا فِى الْاَرْضِ جَمِيْعًا مَّآ اَلَّفْتَ بَيْنَ قُلُوْبِهِمْ وَلٰكِنَّ اللّٰهَ اَلَّفَ بَيْنَهُمْۗ اِنَّهٗ عَزِيْزٌ حَكِيْمٌ

wa-allafa bayna quluubihim law anfaqta m*aa* fii **a**l-ar*dh*i jamii'an m*aa* **al**lafta bayna quluubihim wal*aa*kinna **al**l*aa*ha **al**lafa baynahum innahu 'aziizun *h*

*dan Dia (Allah) yang mempersatukan hati mereka (orang yang beriman). Walaupun kamu menginfakkan semua (kekayaan) yang berada di bumi, niscaya kamu tidak dapat mempersatukan hati mereka, tetapi Allah telah mempersatukan hati mereka. Sungguh, Dia Mahaperkas*

8:64

# يٰٓاَيُّهَا النَّبِيُّ حَسْبُكَ اللّٰهُ وَمَنِ اتَّبَعَكَ مِنَ الْمُؤْمِنِيْنَ ࣖ

y*aa* ayyuh*aa* **al**nnabiyyu *h*asbuka **al**l*aa*hu wamani ittaba'aka mina **a**lmu/miniin**a**

Wahai Nabi (Muhammad)! Cukuplah Allah (menjadi pelindung) bagimu dan bagi orang-orang mukmin yang mengikutimu.

8:65

# يٰٓاَيُّهَا النَّبِيُّ حَرِّضِ الْمُؤْمِنِيْنَ عَلَى الْقِتَالِۗ اِنْ يَّكُنْ مِّنْكُمْ عِشْرُوْنَ صَابِرُوْنَ يَغْلِبُوْا مِائَتَيْنِۚ وَاِنْ يَّكُنْ مِّنْكُمْ مِّائَةٌ يَّغْلِبُوْٓا اَلْفًا مِّنَ الَّذِيْنَ كَفَرُوْا بِاَنَّهُمْ قَوْمٌ لَّا يَفْقَهُوْنَ

y*aa* ayyuh*aa* **al**nnabiyyu *h*arri*dh*i **a**lmu/miniina 'al*aa* **a**lqit*aa*li in yakun minkum 'isyruuna *shaa*biruuna yaghlibuu mi-atayni wa-in yakun minkum mi-atun yaghlibuu

Wahai Nabi (Muhammad)! Kobarkanlah semangat para mukmin untuk berperang. Jika ada dua puluh orang yang sabar di antara kamu, niscaya mereka dapat mengalahkan dua ratus orang musuh. Dan jika ada seratus orang (yang sabar) di antara kamu, niscaya mereka dap

8:66

# اَلْـٰٔنَ خَفَّفَ اللّٰهُ عَنْكُمْ وَعَلِمَ اَنَّ فِيْكُمْ ضَعْفًاۗ فَاِنْ يَّكُنْ مِّنْكُمْ مِّائَةٌ صَابِرَةٌ يَّغْلِبُوْا مِائَتَيْنِۚ وَاِنْ يَّكُنْ مِّنْكُمْ اَلْفٌ يَّغْلِبُوْٓا اَلْفَيْنِ بِاِذْنِ اللّٰهِ ۗوَاللّٰهُ مَعَ الصّٰبِرِيْنَ

al-*aa*na khaffafa **al**l*aa*hu 'ankum wa'alima anna fiikum *dh*a'fan fa-in yakun minkum mi-atun *shaa*biratun yaghlibuu mi-atayni wa-in yakun minkum **a**lfun yaghlibuu **a**lfayni bi-i*dz*

Sekarang Allah telah meringankan kamu karena Dia mengetahui bahwa ada kelemahan padamu. Maka jika di antara kamu ada seratus orang yang sabar, niscaya mereka dapat mengalahkan dua ratus (orang musuh); dan jika di antara kamu ada seribu orang (yang sabar),

8:67

# مَاكَانَ لِنَبِيٍّ اَنْ يَّكُوْنَ لَهٗٓ اَسْرٰى حَتّٰى يُثْخِنَ فِى الْاَرْضِۗ تُرِيْدُوْنَ عَرَضَ الدُّنْيَاۖ وَاللّٰهُ يُرِيْدُ الْاٰخِرَةَۗ وَاللّٰهُ عَزِيْزٌحَكِيْمٌ

m*aa* k*aa*na linabiyyin an yakuuna lahu asr*aa* *h*att*aa* yutskhina fii **a**l-ar*dh*i turiiduuna 'ara*dh*a **al**dduny*aa* wa**al**l*aa*hu yuriidu **a**l-<

Tidaklah pantas, bagi seorang nabi mempunyai tawanan sebelum dia dapat melumpuhkan musuhnya di bumi. Kamu menghendaki harta benda duniawi sedangkan Allah menghendaki (pahala) akhirat (untukmu). Allah Mahaperkasa, Mahabijaksana.

8:68

# لَوْلَاكِتٰبٌ مِّنَ اللّٰهِ سَبَقَ لَمَسَّكُمْ فِيْمَآ اَخَذْتُمْ عَذَابٌ عَظِيْمٌ

lawl*aa* kit*aa*bun mina **al**l*aa*hi sabaqa lamassakum fiim*aa* akha*dz*tum 'a*dzaa*bun 'a*zh*iim**un**

Sekiranya tidak ada ketetapan terdahulu dari Allah, niscaya kamu ditimpa siksaan yang besar karena (tebusan) yang kamu ambil.

8:69

# فَكُلُوْا مِمَّاغَنِمْتُمْ حَلٰلًا طَيِّبًاۖ وَّاتَّقُوا اللّٰهَ ۗاِنَّ اللّٰهَ غَفُوْرٌ رَّحِيْمٌ

fakuluu mimm*aa* ghanimtum *h*al*aa*lan *th*ayyiban wa**i**ttaquu **al**l*aa*ha inna **al**l*aa*ha ghafuurun ra*h*iim**un**

Maka makanlah dari sebagian rampasan perang yang telah kamu peroleh itu, sebagai makanan yang halal lagi baik, dan bertakwalah kepada Allah. Sungguh, Allah Maha Pengampun, Maha Penyayang.

8:70

# يٰٓاَيُّهَا النَّبِيُّ قُلْ لِّمَنْ فِيْٓ اَيْدِيْكُمْ مِّنَ الْاَسْرٰٓىۙ اِنْ يَّعْلَمِ اللّٰهُ فِيْ قُلُوْبِكُمْ خَيْرًا يُّؤْتِكُمْ خَيْرًا مِّمَّآ اُخِذَ مِنْكُمْ وَيَغْفِرْ لَكُمْۗ وَاللّٰهُ غَفُوْرٌ رَّحِيْمٌ ࣖ

y*aa* ayyuh*aa* **al**nnabiyyu qul liman fii aydiikum mina **a**l-asr*aa* in ya'lami **al**l*aa*hu fii quluubikum khayran yu/tikum khayran mimm*aa* ukhi*dz*a minkum wayaghfir lakum wa

Wahai Nabi (Muhammad)! Katakanlah kepada para tawanan perang yang ada di tanganmu, “Jika Allah mengetahui ada kebaikan di dalam hatimu, niscaya Dia akan memberikan yang lebih baik dari apa yang telah diambil darimu dan Dia akan mengampuni kamu.” Allah Mah

8:71

# وَاِنْ يُّرِيْدُوْا خِيَانَتَكَ فَقَدْ خَانُوا اللّٰهَ مِنْ قَبْلُ فَاَمْكَنَ مِنْهُمْ وَاللّٰهُ عَلِيْمٌ حَكِيْمٌ

wa-in yuriiduu khiy*aa*nataka faqad kh*aa*nuu **al**l*aa*ha min qablu fa-amkana minhum wa**al**l*aa*hu 'aliimun *h*akiim**un**

Tetapi jika mereka (tawanan itu) hendak mengkhianatimu (Muhammad) maka sesungguhnya sebelum itu pun mereka telah berkhianat kepada Allah, maka Dia memberikan kekuasaan kepadamu atas mereka. Allah Maha Mengetahui, Mahabijaksana.

8:72

# اِنَّ الَّذِيْنَ اٰمَنُوْا وَهَاجَرُوْا وَجَاهَدُوْا بِاَمْوَالِهِمْ وَاَنْفُسِهِمْ فِيْ سَبِيْلِ اللّٰهِ وَالَّذِيْنَ اٰوَوْا وَّنَصَرُوْٓا اُولٰۤىِٕكَ بَعْضُهُمْ اَوْلِيَاۤءُ بَعْضٍۗ وَالَّذِيْنَ اٰمَنُوْا وَلَمْ يُهَاجِرُوْا مَا لَكُمْ مِّنْ وَّلَايَتِ

inna **al**la*dz*iina *aa*manuu wah*aa*jaruu waj*aa*haduu bi-amw*aa*lihim wa-anfusihim fii sabiili **al**l*aa*hi wa**a**lla*dz*iina *aa*waw wana*sh*aruu ul*aa*-ika ba

Sesungguhnya orang-orang yang beriman dan berhijrah serta berjihad dengan harta dan jiwanya pada jalan Allah dan orang-orang yang memberikan tempat kediaman dan memberi pertolongan (kepada Muhajirin), mereka itu satu sama lain saling melindungi. Dan (terh

8:73

# وَالَّذِيْنَ كَفَرُوْا بَعْضُهُمْ اَوْلِيَاۤءُ بَعْضٍۗ اِلَّا تَفْعَلُوْهُ تَكُنْ فِتْنَةٌ فِى الْاَرْضِ وَفَسَادٌ كَبِيْرٌۗ

wa**a**lla*dz*iina kafaruu ba'*dh*uhum awliy*aa*u ba'*dh*in ill*aa* taf'aluuhu takun fitnatun fii **a**l-ar*dh*i wafas*aa*dun kabiir**un**

Dan orang-orang yang kafir, sebagian mereka melindungi sebagian yang lain. Jika kamu tidak melaksanakan apa yang telah diperintahkan Allah (saling melindungi), niscaya akan terjadi kekacauan di bumi dan kerusakan yang besar.

8:74

# وَالَّذِيْنَ اٰمَنُوْا وَهَاجَرُوْا وَجَاهَدُوْا فِيْ سَبِيْلِ اللّٰهِ وَالَّذِيْنَ اٰوَوْا وَّنَصَرُوْٓا اُولٰۤىِٕكَ هُمُ الْمُؤْمِنُوْنَ حَقًّاۗ لَهُمْ مَّغْفِرَةٌ وَّرِزْقٌ كَرِيْمٌ

wa**a**lla*dz*iina *aa*manuu wah*aa*jaruu waj*aa*haduu fii sabiili **al**l*aa*hi wa**a**lla*dz*iina *aa*waw wana*sh*aruu ul*aa*-ika humu **a**lmu/minuuna *h*

Dan orang-orang yang beriman dan berhijrah serta berjihad di jalan Allah, dan orang-orang yang memberi tempat kediaman dan memberi pertolongan (kepada orang Muhajirin), mereka itulah orang yang benar-benar beriman. Mereka memperoleh ampunan dan rezeki (ni

8:75

# وَالَّذِيْنَ اٰمَنُوْا مِنْۢ بَعْدُ وَهَاجَرُوْا وَجَاهَدُوْا مَعَكُمْ فَاُولٰۤىِٕكَ مِنْكُمْۗ وَاُولُوا الْاَرْحَامِ بَعْضُهُمْ اَوْلٰى بِبَعْضٍ فِيْ كِتٰبِ اللّٰهِ ۗاِنَّ اللّٰهَ بِكُلِّ شَيْءٍ عَلِيْمٌ ࣖ

wa**a**lla*dz*iina *aa*manuu min ba'du wah*aa*jaruu waj*aa*haduu ma'akum faul*aa*-ika minkum wauluu **a**l-ar*haa*mi ba'*dh*uhum awl*aa* biba'*dh*in fii kit*aa*bi **al**

Dan orang-orang yang beriman sesudah itu kemudian berhijrah serta berjihad bersamamu maka orang-orang itu termasuk golonganmu (juga). Orang-orang yang mempunyai hubungan kerabat itu sebagiannya lebih berhak terhadap sesamanya (daripada yang bukan kerabat) di dalam kitab Allah. Sesungguhnya Allah Maha Mengetahui segala sesuatu.

<!--EndFragment-->