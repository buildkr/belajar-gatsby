---
title: (48) Al-Fath - الفتح
date: 2021-10-27T04:08:13.362Z
ayat: 48
description: "Jumlah Ayat: 29 / Arti: Kemenangan"
---
<!--StartFragment-->

48:1

# اِنَّا فَتَحْنَا لَكَ فَتْحًا مُّبِيْنًاۙ

inn*aa* fata*h*n*aa* laka fat*h*an mubiin*aa**\*n**

Sungguh, Kami telah memberikan kepadamu kemenangan yang nyata.

48:2

# لِّيَغْفِرَ لَكَ اللّٰهُ مَا تَقَدَّمَ مِنْ ذَنْۢبِكَ وَمَا تَاَخَّرَ وَيُتِمَّ نِعْمَتَهٗ عَلَيْكَ وَيَهْدِيَكَ صِرَاطًا مُّسْتَقِيْمًاۙ

liyaghfira laka **al**l*aa*hu m*aa* taqaddama min *dz*anbika wam*aa* ta-akhkhara wayutimma ni'matahu 'alayka wayahdiyaka *sh*ir*aath*an mustaqiim*aa**\*n**

Agar Allah memberikan ampunan kepadamu (Muhammad) atas dosamu yang lalu dan yang akan datang serta menyempurnakan nikmat-Nya atasmu dan menunjukimu ke jalan yang lurus,

48:3

# وَّيَنْصُرَكَ اللّٰهُ نَصْرًا عَزِيْزًا

wayan*sh*uraka **al**l*aa*hu na*sh*ran 'aziiz*aa**\*n**

dan agar Allah menolongmu dengan pertolongan yang kuat (banyak).

48:4

# هُوَ الَّذِيْٓ اَنْزَلَ السَّكِيْنَةَ فِيْ قُلُوْبِ الْمُؤْمِنِيْنَ لِيَزْدَادُوْٓا اِيْمَانًا مَّعَ اِيْمَانِهِمْ ۗ وَلِلّٰهِ جُنُوْدُ السَّمٰوٰتِ وَالْاَرْضِۗ وَكَانَ اللّٰهُ عَلِيْمًا حَكِيْمًاۙ

huwa **al**la*dz*ii anzala **al**ssakiinata fii quluubi **a**lmu/miniina liyazd*aa*duu iim*aa*nan ma'a iim*aa*nihim walill*aa*hi junuudu **al**ssam*aa*w*aa*ti wa

Dialah yang telah menurunkan ketenangan ke dalam hati orang-orang mukmin untuk menambah keimanan atas keimanan mereka (yang telah ada). Dan milik Allah-lah bala tentara langit dan bumi, dan Allah Maha Mengetahui, Mahabijaksana;

48:5

# لِّيُدْخِلَ الْمُؤْمِنِيْنَ وَالْمُؤْمِنٰتِ جَنّٰتٍ تَجْرِيْ مِنْ تَحْتِهَا الْاَنْهٰرُ خٰلِدِيْنَ فِيْهَا وَيُكَفِّرَ عَنْهُمْ سَيِّاٰتِهِمْۗ وَكَانَ ذٰلِكَ عِنْدَ اللّٰهِ فَوْزًا عَظِيْمًاۙ

liyudkhila **a**lmu/miniina wa**a**lmu/min*aa*ti jann*aa*tin tajrii min ta*h*tih*aa* **a**l-anh*aa*ru kh*aa*lidiina fiih*aa* wayukaffira 'anhum sayyi-*aa*tihim wak*aa*na <

Agar Dia masukkan orang-orang mukmin laki-laki dan perempuan ke dalam surga yang mengalir di bawahnya sungai-sungai. Mereka kekal di dalamnya dan Dia akan menghapus kesalahan-kesalahan mereka. Dan yang demikian itu menurut Allah suatu keuntungan yang besa

48:6

# وَّيُعَذِّبَ الْمُنٰفِقِيْنَ وَالْمُنٰفِقٰتِ وَالْمُشْرِكِيْنَ وَالْمُشْرِكٰتِ الظَّاۤنِّيْنَ بِاللّٰهِ ظَنَّ السَّوْءِۗ عَلَيْهِمْ دَاۤىِٕرَةُ السَّوْءِۚ وَغَضِبَ اللّٰهُ عَلَيْهِمْ وَلَعَنَهُمْ وَاَعَدَّ لَهُمْ جَهَنَّمَۗ وَسَاۤءَتْ مَصِيْرًا

wayu'a*dzdz*iba **a**lmun*aa*fiqiina wa**a**lmun*aa*fiq*aa*ti wa**a**lmusyrikiina wa**a**lmusyrik*aa*ti **al***zhzhaa*nniina bi**al**l*aa*hi <

dan Dia mengazab orang-orang munafik laki-laki dan perempuan, dan (juga) orang-orang musyrik laki-laki dan perempuan yang berprasangka buruk terhadap Allah. Mereka akan mendapat giliran (azab) yang buruk dan Allah murka kepada mereka dan mengutuk mereka s

48:7

# وَلِلّٰهِ جُنُوْدُ السَّمٰوٰتِ وَالْاَرْضِۗ وَكَانَ اللّٰهُ عَزِيْزًا حَكِيْمًا

walill*aa*hi junuudu **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i wak*aa*na **al**l*aa*hu 'aziizan *h*akiim*aa**\*n**

Dan milik Allah bala tentara langit dan bumi. Dan Allah Mahaperkasa, Mahabijaksana.

48:8

# اِنَّآ اَرْسَلْنٰكَ شَاهِدًا وَّمُبَشِّرًا وَّنَذِيْرًاۙ

inn*aa* arsaln*aa*ka sy*aa*hidan wamubasysyiran wana*dz*iir*aa**\*n**

Sesungguhnya Kami mengutus engkau (Muhammad) sebagai saksi, pembawa berita gembira dan pemberi peringatan,

48:9

# لِّتُؤْمِنُوْا بِاللّٰهِ وَرَسُوْلِهٖ وَتُعَزِّرُوْهُ وَتُوَقِّرُوْهُۗ وَتُسَبِّحُوْهُ بُكْرَةً وَّاَصِيْلًا

litu/minuu bi**al**l*aa*hi warasuulihi watu'azziruuhu watuwaqqiruuhu watusabbi*h*uuhu bukratan wa-a*sh*iil*aa**\*n**

agar kamu semua beriman kepada Allah dan Rasul-Nya, menguatkan (agama)-Nya, membesarkan-Nya, dan bertasbih kepada-Nya pagi dan petang.

48:10

# اِنَّ الَّذِيْنَ يُبَايِعُوْنَكَ اِنَّمَا يُبَايِعُوْنَ اللّٰهَ ۗيَدُ اللّٰهِ فَوْقَ اَيْدِيْهِمْ ۚ فَمَنْ نَّكَثَ فَاِنَّمَا يَنْكُثُ عَلٰى نَفْسِهٖۚ وَمَنْ اَوْفٰى بِمَا عٰهَدَ عَلَيْهُ اللّٰهَ فَسَيُؤْتِيْهِ اَجْرًا عَظِيْمًا ࣖ

inna **al**la*dz*iina yub*aa*yi'uunaka innam*aa* yub*aa*yi'uuna **al**l*aa*ha yadu **al**l*aa*hi fawqa aydiihim faman nakatsa fa-innam*aa* yankutsu 'al*aa* nafsihi waman awf

Bahwasanya orang-orang yang berjanji setia kepadamu (Muhammad), sesungguhnya mereka hanya berjanji setia kepada Allah. Tangan Allah di atas tangan mereka, maka barangsiapa melanggar janji, maka sesungguhnya dia melanggar atas (janji) sendiri; dan barangsi

48:11

# سَيَقُوْلُ لَكَ الْمُخَلَّفُوْنَ مِنَ الْاَعْرَابِ شَغَلَتْنَآ اَمْوَالُنَا وَاَهْلُوْنَا فَاسْتَغْفِرْ لَنَا ۚيَقُوْلُوْنَ بِاَلْسِنَتِهِمْ مَّا لَيْسَ فِيْ قُلُوْبِهِمْۗ قُلْ فَمَنْ يَّمْلِكُ لَكُمْ مِّنَ اللّٰهِ شَيْـًٔا اِنْ اَرَادَ بِكُمْ ضَرًّا اَو

sayaquulu laka **a**lmukhallafuuna mina **a**l-a'r*aa*bi syaghalatn*aa* amw*aa*lun*aa* wa-ahluun*aa* fa**i**staghfir lan*aa* yaquuluuna bi-alsinatihim m*aa* laysa fii quluubihim qul

Orang-orang Badui yang tertinggal (tidak turut ke Hudaibiyah) akan berkata kepadamu, “Kami telah disibukkan oleh harta dan keluarga kami, maka mohonkanlah ampunan untuk kami.” Mereka mengucapkan sesuatu dengan mulutnya apa yang tidak ada dalam hatinya. Ka

48:12

# بَلْ ظَنَنْتُمْ اَنْ لَّنْ يَّنْقَلِبَ الرَّسُوْلُ وَالْمُؤْمِنُوْنَ اِلٰٓى اَهْلِيْهِمْ اَبَدًا وَّزُيِّنَ ذٰلِكَ فِيْ قُلُوْبِكُمْ وَظَنَنْتُمْ ظَنَّ السَّوْءِۚ وَكُنْتُمْ قَوْمًاۢ بُوْرًا

bal *zh*anantum an lan yanqaliba **al**rrasuulu wa**a**lmu/minuuna il*aa* ahliihim abadan wazuyyina *dzaa*lika fii quluubikum wa*zh*anantum *zh*anna **al**ssaw-i wakuntum qawman buur*aa*

Bahkan (semula) kamu menyangka bahwa Rasul dan orang-orang mukmin sekali-kali tidak akan kembali lagi kepada keluarga mereka selama-lamanya dan dijadikan terasa indah yang demikian itu di dalam hatimu, dan kamu telah berprasangka dengan prasangka yang bur

48:13

# وَمَنْ لَّمْ يُؤْمِنْۢ بِاللّٰهِ وَرَسُوْلِهٖ فَاِنَّآ اَعْتَدْنَا لِلْكٰفِرِيْنَ سَعِيْرًا

waman lam yu/min bi**al**l*aa*hi warasuulihi fa-inn*aa* a'tadn*aa* lilk*aa*firiina sa'iir*aa**\*n**

Dan barangsiapa tidak beriman kepada Allah dan Rasul-Nya, maka sesungguhnya Kami telah menyediakan untuk orang-orang kafir itu neraka yang menyala-nyala.

48:14

# وَلِلّٰهِ مُلْكُ السَّمٰوٰتِ وَالْاَرْضِۗ يَغْفِرُ لِمَنْ يَّشَاۤءُ وَيُعَذِّبُ مَنْ يَّشَاۤءُ ۗوَكَانَ اللّٰهُ غَفُوْرًا رَّحِيْمًا

walill*aa*hi mulku **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i yaghfiru liman yasy*aa*u wayu'a*dzdz*ibu man yasy*aa*u wak*aa*na **al**l*aa*hu ghafuuran ra*h*iim*aa*

Dan hanya milik Allah kerajaan langit dan bumi. Dia mengampuni siapa yang Dia kehendaki dan akan mengazab siapa yang Dia kehendaki. Dan Allah Maha Pengampun, Maha Penyayang.

48:15

# سَيَقُوْلُ الْمُخَلَّفُوْنَ اِذَا انْطَلَقْتُمْ اِلٰى مَغَانِمَ لِتَأْخُذُوْهَا ذَرُوْنَا نَتَّبِعْكُمْ ۚ يُرِيْدُوْنَ اَنْ يُّبَدِّلُوْا كَلٰمَ اللّٰهِ ۗ قُلْ لَّنْ تَتَّبِعُوْنَا كَذٰلِكُمْ قَالَ اللّٰهُ مِنْ قَبْلُ ۖفَسَيَقُوْلُوْنَ بَلْ تَحْسُدُوْنَن

sayaquulu **a**lmukhallafuuna i*dzaa* in*th*alaqtum il*aa* magh*aa*nima lita/khu*dz*uuh*aa* *dz*aruun*aa* nattabi'kum yuriiduuna an yubaddiluu kal*aa*ma **al**l*aa*hi qul lan tatt

Apabila kamu berangkat untuk mengambil barang rampasan, orang-orang Badui yang tertinggal itu akan berkata, “Biarkanlah kami mengikuti kamu.” Mereka hendak mengubah janji Allah. Katakanlah, “Kamu sekali-kali tidak (boleh) mengikuti kami. Demikianlah yang

48:16

# قُلْ لِّلْمُخَلَّفِيْنَ مِنَ الْاَعْرَابِ سَتُدْعَوْنَ اِلٰى قَوْمٍ اُولِيْ بَأْسٍ شَدِيْدٍ تُقَاتِلُوْنَهُمْ اَوْ يُسْلِمُوْنَ ۚ فَاِنْ تُطِيْعُوْا يُؤْتِكُمُ اللّٰهُ اَجْرًا حَسَنًا ۚ وَاِنْ تَتَوَلَّوْا كَمَا تَوَلَّيْتُمْ مِّنْ قَبْلُ يُعَذِّبْكُمْ عَ

qul lilmukhallafiina mina **a**l-a'r*aa*bi satud'awna il*aa* qawmin ulii ba/sin syadiidin tuq*aa*tiluunahum aw yuslimuuna fa-in tu*th*ii'uu yu/tikumu **al**l*aa*hu ajran *h*asanan wa-in tatawallaw ka

Katakanlah kepada orang-orang Badui yang tertinggal, “Kamu akan diajak untuk (memerangi) kaum yang mempunyai kekuatan yang besar, kamu harus memerangi mereka kecuali mereka menyerah. Jika kamu patuhi (ajakan itu) Allah akan memberimu pahala yang baik, tet

48:17

# لَيْسَ عَلَى الْاَعْمٰى حَرَجٌ وَّلَا عَلَى الْاَعْرَجِ حَرَجٌ وَّلَا عَلَى الْمَرِيْضِ حَرَجٌ ۗ وَمَنْ يُّطِعِ اللّٰهَ وَرَسُوْلَهٗ يُدْخِلْهُ جَنّٰتٍ تَجْرِيْ مِنْ تَحْتِهَا الْاَنْهٰرُ ۚ وَمَنْ يَّتَوَلَّ يُعَذِّبْهُ عَذَابًا اَلِيْمًا ࣖ

laysa 'al*aa* **a**l-a'm*aa* *h*arajun wal*aa* 'al*aa* **a**l-a'raji *h*arajun wal*aa* 'al*aa* **a**lmarii*dh*i *h*arajun waman yu*th*i'i **al**l

Tidak ada dosa atas orang-orang yang buta, atas orang-orang yang pincang, dan atas orang-orang yang sakit (apabila tidak ikut berperang). Barangsiapa taat kepada Allah dan Rasul-Nya, Dia akan memasukkannya ke dalam surga yang mengalir di bawahnya sungai-s

48:18

# ۞ لَقَدْ رَضِيَ اللّٰهُ عَنِ الْمُؤْمِنِيْنَ اِذْ يُبَايِعُوْنَكَ تَحْتَ الشَّجَرَةِ فَعَلِمَ مَا فِيْ قُلُوْبِهِمْ فَاَنْزَلَ السَّكِيْنَةَ عَلَيْهِمْ وَاَثَابَهُمْ فَتْحًا قَرِيْبًاۙ

laqad ra*dh*iya **al**l*aa*hu 'ani **a**lmu/miniina i*dz* yub*aa*yi'uunaka ta*h*ta **al**sysyajarati fa'alima m*aa* fii quluubihim fa-anzala **al**sakiinata 'alayhim wa-ats<

Sungguh, Allah telah meridai orang-orang mukmin ketika mereka berjanji setia kepadamu (Muhammad) di bawah pohon, Dia mengetahui apa yang ada dalam hati mereka lalu Dia memberikan ketenangan atas mereka dan memberi balasan dengan kemenangan yang dekat,

48:19

# وَّمَغَانِمَ كَثِيْرَةً يَّأْخُذُوْنَهَا ۗ وَكَانَ اللّٰهُ عَزِيْزًا حَكِيْمًا

wamagh*aa*nima katsiiratan ya/khu*dz*uunah*aa* wak*aa*na **al**l*aa*hu 'aziizan *h*akiim*aa**\*n**

dan harta rampasan perang yang banyak yang akan mereka peroleh. Dan Allah Mahaperkasa, Mahabijaksana.

48:20

# وَعَدَكُمُ اللّٰهُ مَغَانِمَ كَثِيْرَةً تَأْخُذُوْنَهَا فَعَجَّلَ لَكُمْ هٰذِهٖ وَكَفَّ اَيْدِيَ النَّاسِ عَنْكُمْۚ وَلِتَكُوْنَ اٰيَةً لِّلْمُؤْمِنِيْنَ وَيَهْدِيَكُمْ صِرَاطًا مُّسْتَقِيْمًاۙ

wa'adakumu **al**l*aa*hu magh*aa*nima katsiiratan ta/khu*dz*uunah*aa* fa'ajjala lakum h*aadz*ihi wakaffa aydiya **al**nn*aa*si 'ankum walitakuuna *aa*yatan lilmu/miniina wayahdiyakum *sh*ir

Allah menjanjikan kepadamu harta rampasan perang yang banyak yang dapat kamu ambil, maka Dia segerakan (harta rampasan perang) ini untukmu dan Dia menahan tangan manusia dari (membinasakan)mu (agar kamu mensyukuri-Nya) dan agar menjadi bukti bagi orang-or

48:21

# وَّاُخْرٰى لَمْ تَقْدِرُوْا عَلَيْهَا قَدْ اَحَاطَ اللّٰهُ بِهَا ۗوَكَانَ اللّٰهُ عَلٰى كُلِّ شَيْءٍ قَدِيْرًا

waukhr*aa* lam taqdiruu 'alayh*aa* qad a*hath*a **al**l*aa*hu bih*aa* wak*aa*na **al**l*aa*hu 'al*aa* kulli syay-in qadiir*aa**\*n**

Dan (kemenangan-kemenangan) atas negeri-negeri lain yang tidak dapat kamu perkirakan, tetapi sesungguhnya Allah telah menentukannya. Dan Allah Mahakuasa atas segala sesuatu.

48:22

# وَلَوْ قَاتَلَكُمُ الَّذِيْنَ كَفَرُوْا لَوَلَّوُا الْاَدْبَارَ ثُمَّ لَا يَجِدُوْنَ وَلِيًّا وَّلَا نَصِيْرًا

walaw q*aa*talakumu **al**la*dz*iina kafaruu lawallawuu **a**l-adb*aa*ra tsumma l*aa* yajiduuna waliyyan wal*aa* na*sh*iir*aa**\*n**

Dan sekiranya orang-orang yang kafir itu memerangi kamu pastilah mereka akan berbalik melarikan diri (kalah) dan mereka tidak akan mendapatkan pelindung dan penolong.

48:23

# سُنَّةَ اللّٰهِ الَّتِيْ قَدْ خَلَتْ مِنْ قَبْلُ ۖوَلَنْ تَجِدَ لِسُنَّةِ اللّٰهِ تَبْدِيْلًا

sunnata **al**l*aa*hi **al**latii qad khalat min qablu walan tajida lisunnati **al**l*aa*hi tabdiil*aa**\*n**

(Demikianlah) hukum Allah, yang telah berlaku sejak dahulu, kamu sekali-kali tidak akan menemukan perubahan pada hukum Allah itu.

48:24

# وَهُوَ الَّذِيْ كَفَّ اَيْدِيَهُمْ عَنْكُمْ وَاَيْدِيَكُمْ عَنْهُمْ بِبَطْنِ مَكَّةَ مِنْۢ بَعْدِ اَنْ اَظْفَرَكُمْ عَلَيْهِمْ ۗوَكَانَ اللّٰهُ بِمَا تَعْمَلُوْنَ بَصِيْرًا

wahuwa **al**la*dz*ii kaffa aydiyahum 'ankum wa-aydiyakum 'anhum biba*th*ni makkata min ba'di an a*zh*farakum 'alayhim wak*aa*na **al**l*aa*hu bim*aa* ta'maluuna ba*sh*iir*aa**\*n**

**Dan Dialah yang mencegah tangan mereka dari (membinasakan) kamu dan (mencegah) tangan kamu dari (membinasakan) mereka di tengah (kota) Mekah setelah Allah memenangkan kamu atas mereka. Dan Allah Maha Melihat apa yang kamu kerjakan.**

48:25

# هُمُ الَّذِيْنَ كَفَرُوْا وَصَدُّوْكُمْ عَنِ الْمَسْجِدِ الْحَرَامِ وَالْهَدْيَ مَعْكُوْفًا اَنْ يَّبْلُغَ مَحِلَّهٗ ۚوَلَوْلَا رِجَالٌ مُّؤْمِنُوْنَ وَنِسَاۤءٌ مُّؤْمِنٰتٌ لَّمْ تَعْلَمُوْهُمْ اَنْ تَطَـُٔوْهُمْ فَتُصِيْبَكُمْ مِّنْهُمْ مَّعَرَّةٌ ۢبِغَي

humu **al**la*dz*iina kafaruu wa*sh*adduukum 'ani **a**lmasjidi **a**l*h*ar*aa*mi wa**a**lhadya ma'kuufan an yablugha ma*h*illahu walawl*aa* rij*aa*lun mu/minuuna wanis

Merekalah orang-orang kafir yang menghalang-halangi kamu (masuk) Masjidilharam dan menghambat hewan-hewan kurban sampai ke tempat (penyembelihan)nya. Dan kalau bukanlah karena ada beberapa orang beriman laki-laki dan perempuan yang tidak kamu ketahui, ten

48:26

# اِذْ جَعَلَ الَّذِيْنَ كَفَرُوْا فِيْ قُلُوْبِهِمُ الْحَمِيَّةَ حَمِيَّةَ الْجَاهِلِيَّةِ فَاَنْزَلَ اللّٰهُ سَكِيْنَتَهٗ عَلٰى رَسُوْلِهٖ وَعَلَى الْمُؤْمِنِيْنَ وَاَلْزَمَهُمْ كَلِمَةَ التَّقْوٰى وَكَانُوْٓا اَحَقَّ بِهَا وَاَهْلَهَا ۗوَكَانَ اللّٰهُ بِ

i*dz* ja'ala **al**la*dz*iina kafaruu fii quluubihimu **a**l*h*amiyyata *h*amiyyata **a**lj*aa*hiliyyati fa-anzala **al**l*aa*hu sakiinatahu 'al*aa* rasuulihi wa'al*a*

Ketika orang-orang yang kafir menanamkan kesombongan dalam hati mereka (yaitu) kesombongan jahiliah, lalu Allah menurunkan ketenangan kepada Rasul-Nya, dan kepada orang-orang mukmin; dan (Allah) mewajibkan kepada mereka tetap taat menjalankan kalimat takw

48:27

# لَقَدْ صَدَقَ اللّٰهُ رَسُوْلَهُ الرُّءْيَا بِالْحَقِّ ۚ لَتَدْخُلُنَّ الْمَسْجِدَ الْحَرَامَ اِنْ شَاۤءَ اللّٰهُ اٰمِنِيْنَۙ مُحَلِّقِيْنَ رُءُوْسَكُمْ وَمُقَصِّرِيْنَۙ لَا تَخَافُوْنَ ۗفَعَلِمَ مَا لَمْ تَعْلَمُوْا فَجَعَلَ مِنْ دُوْنِ ذٰلِكَ فَتْحًا ق

laqad *sh*adaqa **al**l*aa*hu rasuulahu **al**rru/y*aa* bi**a**l*h*aqqi latadkhulunna **a**lmasjida **a**l*h*ar*aa*ma in sy*aa*-a **al**l*aa*

Sungguh, Allah akan membuktikan kepada Rasul-Nya tentang kebenaran mimpinya bahwa kamu pasti akan memasuki Masjidilharam, jika Allah menghendaki dalam keadaan aman, dengan menggundul rambut kepala dan memendekkannya, sedang kamu tidak merasa takut. Maka A

48:28

# هُوَ الَّذِيْٓ اَرْسَلَ رَسُوْلَهٗ بِالْهُدٰى وَدِيْنِ الْحَقِّ لِيُظْهِرَهٗ عَلَى الدِّيْنِ كُلِّهٖ ۗوَكَفٰى بِاللّٰهِ شَهِيْدًا

huwa **al**la*dz*ii arsala rasuulahu bi**a**lhud*aa* wadiini **a**l*h*aqqi liyu*zh*hirahu 'al*aa* **al**ddiini kullihi wakaf*aa* bi**al**l*aa*hi syahiid

Dialah yang mengutus Rasul-Nya dengan membawa petunjuk dan agama yang benar agar dimenangkan-Nya terhadap semua agama. Dan cukuplah Allah sebagai saksi.

48:29

# مُحَمَّدٌ رَّسُوْلُ اللّٰهِ ۗوَالَّذِيْنَ مَعَهٗٓ اَشِدَّاۤءُ عَلَى الْكُفَّارِ رُحَمَاۤءُ بَيْنَهُمْ تَرٰىهُمْ رُكَّعًا سُجَّدًا يَّبْتَغُوْنَ فَضْلًا مِّنَ اللّٰهِ وَرِضْوَانًا ۖ سِيْمَاهُمْ فِيْ وُجُوْهِهِمْ مِّنْ اَثَرِ السُّجُوْدِ ۗذٰلِكَ مَثَلُهُمْ

muḥammadur rasụlullāh, wallażīna ma’ahū asyiddā\`u ‘alal-kuffāri ruḥamā\`u bainahum tarāhum rukka’an sujjaday yabtagụna faḍlam minallāhi wa riḍwānan sīmāhum fī wujụhihim min aṡaris-sujụd, żālika maṡaluhum fit-taurāti wa maṡaluhum fil-injīl, kazar’in akhraja syaṭ`ahụ fa āzarahụ fastaglaẓa fastawā ‘alā sụqihī yu’jibuz-zurrā’a liyagīẓa bihimul-kuffār, wa’adallāhullażīna āmanụ wa ‘amiluṣ-ṣāliḥāti min-hum magfirataw wa ajran ‘aẓīmā

Muhammad itu adalah utusan Allah dan orang-orang yang bersama dengan dia adalah keras terhadap orang-orang kafir, tetapi berkasih sayang sesama mereka. Kamu lihat mereka ruku’ dan sujud mencari karunia Allah dan keridhaan-Nya, tanda-tanda mereka tampak pada muka mereka dari bekas sujud. Demikianlah sifat-sifat mereka dalam Taurat dan sifat-sifat mereka dalam Injil, yaitu seperti tanaman yang mengeluarkan tunasnya maka tunas itu menjadikan tanaman itu kuat lalu menjadi besarlah dia dan tegak lurus di atas pokoknya; tanaman itu menyenangkan hati penanam-penanamnya karena Allah hendak menjengkelkan hati orang-orang kafir (dengan kekuatan orang-orang mukmin). Allah menjanjikan kepada orang-orang yang beriman dan mengerjakan amal yang saleh di antara mereka ampunan dan pahala yang besar.

<!--EndFragment-->