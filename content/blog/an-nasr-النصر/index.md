---
title: (110) An-Nasr - النصر
date: 2021-10-27T04:18:57.324Z
ayat: 110
description: "Jumlah Ayat: 3 / Arti: Pertolongan"
---
<!--StartFragment-->

110:1

# اِذَا جَاۤءَ نَصْرُ اللّٰهِ وَالْفَتْحُۙ

i*dzaa* j*aa*-a na*sh*ru **al**l*aa*hi wa**a**lfat*h***u**

Apabila telah datang pertolongan Allah dan kemenangan,

110:2

# وَرَاَيْتَ النَّاسَ يَدْخُلُوْنَ فِيْ دِيْنِ اللّٰهِ اَفْوَاجًاۙ

wara-ayta **al**nn*aa*sa yadkhuluuna fii diini **al**l*aa*hi afw*aa*j*aa***n**

dan engkau melihat manusia berbondong-bondong masuk agama Allah,

110:3

# فَسَبِّحْ بِحَمْدِ رَبِّكَ وَاسْتَغْفِرْهُۗ اِنَّهٗ كَانَ تَوَّابًا ࣖ

fasabbi*h* bi*h*amdi rabbika wa**i**staghfirhu innahu k*aa*na taww*aa*b*aa***n**

maka bertasbihlah dalam dengan Tuhanmu dan mohonlah ampunan kepada-Nya. Sungguh, Dia Maha Penerima tobat.

<!--EndFragment-->