---
title: (60) Al-Mumtahanah - الممتحنة
date: 2021-10-27T04:15:18.701Z
ayat: 60
description: "Jumlah Ayat: 13 / Arti: Wanita Yang Diuji"
---
<!--StartFragment-->

60:1

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْا لَا تَتَّخِذُوْا عَدُوِّيْ وَعَدُوَّكُمْ اَوْلِيَاۤءَ تُلْقُوْنَ اِلَيْهِمْ بِالْمَوَدَّةِ وَقَدْ كَفَرُوْا بِمَا جَاۤءَكُمْ مِّنَ الْحَقِّۚ يُخْرِجُوْنَ الرَّسُوْلَ وَاِيَّاكُمْ اَنْ تُؤْمِنُوْا بِاللّٰهِ رَبِّكُمْۗ اِنْ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu l*aa* tattakhi*dz*uu 'aduwwii wa'aduwwakum awliy*aa*-a tulquuna ilayhim bi**a**lmawaddati waqad kafaruu bim*aa* j*aa*-akum mina **a**

**Wahai orang-orang yang beriman! Janganlah kamu menjadikan musuh-Ku dan musuhmu sebagai teman-teman setia sehingga kamu sampaikan kepada mereka (berita-berita Muhammad), karena rasa kasih sayang; padahal mereka telah ingkar kepada kebenaran yang disampaika**

60:2

# اِنْ يَّثْقَفُوْكُمْ يَكُوْنُوْا لَكُمْ اَعْدَاۤءً وَّيَبْسُطُوْٓا اِلَيْكُمْ اَيْدِيَهُمْ وَاَلْسِنَتَهُمْ بِالسُّوْۤءِ وَوَدُّوْا لَوْ تَكْفُرُوْنَۗ

in yatsqafuukum yakuunuu lakam a'd*aa*-an wayabsu*th*uu ilaykum aydiyahum wa-alsinatahum bi**al**ssuu-i wawadduu law takfuruun**a**

Jika mereka menangkapmu, niscaya mereka bertindak sebagai musuh bagimu lalu melepaskan tangan dan lidahnya kepadamu untuk menyakiti dan mereka ingin agar kamu (kembali) kafir.

60:3

# لَنْ تَنْفَعَكُمْ اَرْحَامُكُمْ وَلَآ اَوْلَادُكُمْ ۛيَوْمَ الْقِيٰمَةِ ۛيَفْصِلُ بَيْنَكُمْۗ وَاللّٰهُ بِمَا تَعْمَلُوْنَ بَصِيْرٌ

lan tanfa'akum ar*haa*mukum wal*aa* awl*aa*dukum yawma **a**lqiy*aa*mati yaf*sh*ilu baynakum wa**al**l*aa*hu bim*aa* ta'maluuna ba*sh*iir**un**

Kaum kerabatmu dan anak-anakmu tidak akan bermanfaat bagimu pada hari Kiamat. Dia akan memisahkan antara kamu. Dan Allah Maha Melihat apa yang kamu kerjakan.

60:4

# قَدْ كَانَتْ لَكُمْ اُسْوَةٌ حَسَنَةٌ فِيْٓ اِبْرٰهِيْمَ وَالَّذِيْنَ مَعَهٗۚ اِذْ قَالُوْا لِقَوْمِهِمْ اِنَّا بُرَءٰۤؤُا مِنْكُمْ وَمِمَّا تَعْبُدُوْنَ مِنْ دُوْنِ اللّٰهِ ۖ كَفَرْنَا بِكُمْ وَبَدَا بَيْنَنَا وَبَيْنَكُمُ الْعَدَاوَةُ وَالْبَغْضَاۤءُ اَ

qad k*aa*nat lakum uswatun *h*asanatun fii ibr*aa*hiima wa**a**lla*dz*iina ma'ahu i*dz* q*aa*luu liqawmihim inn*aa* bura*aa*u minkum wamimm*aa* ta'buduuna min duuni **al**l*aa*hi

Sungguh, telah ada suri teladan yang baik bagimu pada Ibrahim dan orang-orang yang bersama dengannya, ketika mereka berkata kepada kaumnya, “Sesungguhnya kami berlepas diri dari kamu dan dari apa yang kamu sembah selain Allah, kami mengingkari (kekafiran)

60:5

# رَبَّنَا لَا تَجْعَلْنَا فِتْنَةً لِّلَّذِيْنَ كَفَرُوْا وَاغْفِرْ لَنَا رَبَّنَاۚ اِنَّكَ اَنْتَ الْعَزِيْزُ الْحَكِيْمُ

rabban*aa* l*aa* taj'aln*aa* fitnatan lilla*dz*iina kafaruu wa**i**ghfir lan*aa* rabban*aa* innaka anta **a**l'aziizu **a**l*h*akiim**u**

Ya Tuhan kami, janganlah Engkau jadikan kami (sasaran) fitnah bagi orang-orang kafir. Dan ampunilah kami, ya Tuhan kami. Sesungguhnya Engkau yang Mahaperkasa, Mahabijaksana.”

60:6

# لَقَدْ كَانَ لَكُمْ فِيْهِمْ اُسْوَةٌ حَسَنَةٌ لِّمَنْ كَانَ يَرْجُو اللّٰهَ وَالْيَوْمَ الْاٰخِرَۗ وَمَنْ يَّتَوَلَّ فَاِنَّ اللّٰهَ هُوَ الْغَنِيُّ الْحَمِيْدُ ࣖ

laqad k*aa*na lakum fiihim uswatun *h*asanatun liman k*aa*na yarjuu **al**l*aa*ha wa**a**lyawma **a**l-*aa*khira waman yatawalla fa-inna **al**l*aa*ha huwa **a**

**Sungguh, pada mereka itu (Ibrahim dan umatnya) terdapat suri teladan yang baik bagimu; (yaitu) bagi orang yang mengharap (pahala) Allah dan (keselamatan pada) hari kemudian, dan barangsiapa berpaling, maka sesungguhnya Allah, Dialah Yang Mahakaya, Maha Te**

60:7

# ۞ عَسَى اللّٰهُ اَنْ يَّجْعَلَ بَيْنَكُمْ وَبَيْنَ الَّذِيْنَ عَادَيْتُمْ مِّنْهُمْ مَّوَدَّةًۗ وَاللّٰهُ قَدِيْرٌۗ وَاللّٰهُ غَفُوْرٌ رَّحِيْمٌ

'as*aa* **al**l*aa*hu an yaj'ala baynakum wabayna **al**la*dz*iina '*aa*daytum minhum mawaddatan wa**al**l*aa*hu qadiirun wa**al**l*aa*hu ghafuurun ra*h*iim**un**

**Mudah-mudahan Allah menimbulkan kasih sayang di antara kamu dengan orang-orang yang pernah kamu musuhi di antara mereka. Allah Mahakuasa. Dan Allah Maha Pengampun, Maha Penyayang.**

60:8

# لَا يَنْهٰىكُمُ اللّٰهُ عَنِ الَّذِيْنَ لَمْ يُقَاتِلُوْكُمْ فِى الدِّيْنِ وَلَمْ يُخْرِجُوْكُمْ مِّنْ دِيَارِكُمْ اَنْ تَبَرُّوْهُمْ وَتُقْسِطُوْٓا اِلَيْهِمْۗ اِنَّ اللّٰهَ يُحِبُّ الْمُقْسِطِيْنَ

l*aa* yanh*aa*kumu **al**l*aa*hu 'ani **al**la*dz*iina lam yuq*aa*tiluukum fii **al**ddiini walam yukhrijuukum min diy*aa*rikum an tabarruuhum watuqsi*th*uu ilayhim inna **al<**

Allah tidak melarang kamu berbuat baik dan berlaku adil terhadap orang-orang yang tidak memerangimu dalam urusan agama dan tidak mengusir kamu dari kampung halamanmu. Sesungguhnya Allah mencintai orang-orang yang berlaku adil.

60:9

# اِنَّمَا يَنْهٰىكُمُ اللّٰهُ عَنِ الَّذِيْنَ قَاتَلُوْكُمْ فِى الدِّيْنِ وَاَخْرَجُوْكُمْ مِّنْ دِيَارِكُمْ وَظَاهَرُوْا عَلٰٓى اِخْرَاجِكُمْ اَنْ تَوَلَّوْهُمْۚ وَمَنْ يَّتَوَلَّهُمْ فَاُولٰۤىِٕكَ هُمُ الظّٰلِمُوْنَ

innam*aa* yanh*aa*kumu **al**l*aa*hu 'ani **al**la*dz*iina q*aa*taluukum fii **al**ddiini wa-akhrajuukum min diy*aa*rikum wa*zhaa*haruu 'al*aa* ikhr*aa*jikum an tawallawhu

Sesungguhnya Allah hanya melarang kamu menjadikan mereka sebagai kawanmu orang-orang yang memerangi kamu dalam urusan agama dan mengusir kamu dari kampung halamanmu dan membantu (orang lain) untuk mengusirmu. Barangsiapa menjadikan mereka sebagai kawan, m

60:10

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْٓا اِذَا جَاۤءَكُمُ الْمُؤْمِنٰتُ مُهٰجِرٰتٍ فَامْتَحِنُوْهُنَّۗ اَللّٰهُ اَعْلَمُ بِاِيْمَانِهِنَّ فَاِنْ عَلِمْتُمُوْهُنَّ مُؤْمِنٰتٍ فَلَا تَرْجِعُوْهُنَّ اِلَى الْكُفَّارِۗ لَا هُنَّ حِلٌّ لَّهُمْ وَلَا هُمْ يَحِلُّوْن

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu i*dzaa* j*aa*-akumu **a**lmu/min*aa*tu muh*aa*jir*aa*tin fa**i**mta*h*inuuhunna **al**l*aa*hu a'lamu bi-i

Wahai orang-orang yang beriman! Apabila perempuan-perempuan mukmin datang berhijrah kepadamu, maka hendaklah kamu uji (keimanan) mereka. Allah lebih mengetahui tentang keimanan mereka; jika kamu telah mengetahui bahwa mereka (benar-benar) beriman maka jan

60:11

# وَاِنْ فَاتَكُمْ شَيْءٌ مِّنْ اَزْوَاجِكُمْ اِلَى الْكُفَّارِ فَعَاقَبْتُمْ فَاٰتُوا الَّذِيْنَ ذَهَبَتْ اَزْوَاجُهُمْ مِّثْلَ مَآ اَنْفَقُوْاۗ وَاتَّقُوا اللّٰهَ الَّذِيْٓ اَنْتُمْ بِهٖ مُؤْمِنُوْنَ

wa-in f*aa*takum syay-un min azw*aa*jikum il*aa* **a**lkuff*aa*ri fa'*aa*qabtum fa*aa*tuu **al**la*dz*iina *dz*ahabat azw*aa*juhum mitsla m*aa* anfaquu wa**i**ttaquu

Dan jika ada sesuatu (pengembalian mahar) yang belum kamu selesaikan dari istri-istrimu yang lari kepada orang-orang kafir, lalu kamu dapat mengalahkan mereka maka berikanlah (dari harta rampasan) kepada orang-orang yang istrinya lari itu sebanyak mahar y

60:12

# يٰٓاَيُّهَا النَّبِيُّ اِذَا جَاۤءَكَ الْمُؤْمِنٰتُ يُبَايِعْنَكَ عَلٰٓى اَنْ لَّا يُشْرِكْنَ بِاللّٰهِ شَيْـًٔا وَّلَا يَسْرِقْنَ وَلَا يَزْنِيْنَ وَلَا يَقْتُلْنَ اَوْلَادَهُنَّ وَلَا يَأْتِيْنَ بِبُهْتَانٍ يَّفْتَرِيْنَهٗ بَيْنَ اَيْدِيْهِنَّ وَاَرْجُل

y*aa* ayyuh*aa* **al**nnabiyyu i*dzaa* j*aa*-aka **a**lmu/min*aa*tu yub*aa*yi'naka 'al*aa* an l*aa* yusyrikna bi**al**l*aa*hi syay-an wal*aa* yasriqna wal*aa* ya

Wahai Nabi! Apabila perempuan-perempuan yang mukmin datang kepadamu untuk mengadakan bai‘at (janji setia), bahwa mereka tidak akan mempersekutukan sesuatu apa pun dengan Allah; tidak akan mencuri, tidak akan berzina, tidak akan membunuh anak-anaknya, tida

60:13

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْا لَا تَتَوَلَّوْا قَوْمًا غَضِبَ اللّٰهُ عَلَيْهِمْ قَدْ يَىِٕسُوْا مِنَ الْاٰخِرَةِ كَمَا يَىِٕسَ الْكُفَّارُ مِنْ اَصْحٰبِ الْقُبُوْرِ ࣖ

yā ayyuhallażīna āmanụ lā tatawallau qauman gaḍiballāhu ‘alaihim qad ya\`isụ minal-ākhirati kamā ya\`isal-kuffāru min aṣ-ḥābil-qubụr

Wahai orang-orang yang beriman! Janganlah kamu jadikan orang-orang yang dimurkai Allah sebagai penolongmu, sungguh, mereka telah putus asa terhadap akhirat sebagaimana orang-orang kafir yang telah berada dalam kubur juga berputus asa.

<!--EndFragment-->