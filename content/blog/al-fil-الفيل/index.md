---
title: (105) Al-Fil - الفيل
date: 2021-10-27T04:16:52.554Z
ayat: 105
description: "Jumlah Ayat: 5 / Arti: Gajah"
---
<!--StartFragment-->

105:1

# اَلَمْ تَرَ كَيْفَ فَعَلَ رَبُّكَ بِاَصْحٰبِ الْفِيْلِۗ

alam tara kayfa fa'ala rabbuka bi-a*sh*-*haa*bi **a**lfiil**i**

Tidakkah engkau (Muhammad) perhatikan bagaimana Tuhanmu telah bertindak terhadap pasukan bergajah?

105:2

# اَلَمْ يَجْعَلْ كَيْدَهُمْ فِيْ تَضْلِيْلٍۙ

alam yaj'al kaydahum fii ta*dh*liil**in**

Bukankah Dia telah menjadikan tipu daya mereka itu sia-sia?

105:3

# وَّاَرْسَلَ عَلَيْهِمْ طَيْرًا اَبَابِيْلَۙ

wa-arsala 'alayhim *th*ayran ab*aa*biil**a**

dan Dia mengirimkan kepada mereka burung yang berbondong-bondong,

105:4

# تَرْمِيْهِمْ بِحِجَارَةٍ مِّنْ سِجِّيْلٍۙ

tarmiihim bi*h*ij*aa*ratin min sijjiil**in**

yang melempari mereka dengan batu dari tanah liat yang dibakar,

105:5

# فَجَعَلَهُمْ كَعَصْفٍ مَّأْكُوْلٍ ࣖ

faja'alahum ka'a*sh*fin ma/kuul**in**

sehingga mereka dijadikan-Nya seperti daun-daun yang dimakan (ulat).

<!--EndFragment-->