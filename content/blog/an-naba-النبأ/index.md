---
title: (78) An-Naba' - النبأ
date: 2021-10-27T04:08:32.678Z
ayat: 78
description: "Jumlah Ayat: 40 / Arti: Berita Besar"
---
<!--StartFragment-->

78:1

# عَمَّ يَتَسَاۤءَلُوْنَۚ

'amma yatas*aa*-aluun**a**

Tentang apakah mereka saling bertanya-tanya?

78:2

# عَنِ النَّبَاِ الْعَظِيْمِۙ

'ani **al**nnaba-i **a**l'a*zh*iim**i**

Tentang berita yang besar (hari kebangkitan),

78:3

# الَّذِيْ هُمْ فِيْهِ مُخْتَلِفُوْنَۗ

**al**la*dz*ii hum fiihi mukhtalifuun**a**

yang dalam hal itu mereka berselisih.

78:4

# كَلَّا سَيَعْلَمُوْنَۙ

kall*aa* saya'lamuun**a**

Tidak! Kelak mereka akan mengetahui,

78:5

# ثُمَّ كَلَّا سَيَعْلَمُوْنَ

tsumma kall*aa* saya'lamuun**a**

sekali lagi tidak! Kelak mereka akan mengetahui.

78:6

# اَلَمْ نَجْعَلِ الْاَرْضَ مِهٰدًاۙ

alam naj'ali **a**l-ar*dh*a mih*aa*d*aa***n**

Bukankah Kami telah menjadikan bumi sebagai hamparan,

78:7

# وَّالْجِبَالَ اَوْتَادًاۖ

wa**a**ljib*aa*la awt*aa*d*aa***n**

dan gunung-gunung sebagai pasak?

78:8

# وَّخَلَقْنٰكُمْ اَزْوَاجًاۙ

wakhalaqn*aa*kum azw*aa*j*aa***n**

Dan Kami menciptakan kamu berpasang-pasangan,

78:9

# وَّجَعَلْنَا نَوْمَكُمْ سُبَاتًاۙ

waja'aln*aa* nawmakum sub*aa*t*aa***n**

dan Kami menjadikan tidurmu untuk istirahat,

78:10

# وَّجَعَلْنَا الَّيْلَ لِبَاسًاۙ

waja'aln*aa* **al**layla lib*aa*s*aa***n**

dan Kami menjadikan malam sebagai pakaian,

78:11

# وَّجَعَلْنَا النَّهَارَ مَعَاشًاۚ

waja'aln*aa* **al**nnah*aa*ra ma'*aa*sy*aa***n**

dan Kami menjadikan siang untuk mencari penghidupan,

78:12

# وَبَنَيْنَا فَوْقَكُمْ سَبْعًا شِدَادًاۙ

wabanayn*aa* fawqakum sab'an syid*aa*d*aa***n**

dan Kami membangun di atas kamu tujuh (langit) yang kokoh,

78:13

# وَّجَعَلْنَا سِرَاجًا وَّهَّاجًاۖ

waja'aln*aa* sir*aa*jan wahh*aa*j*aa***n**

dan Kami menjadikan pelita yang terang-benderang (matahari),

78:14

# وَّاَنْزَلْنَا مِنَ الْمُعْصِرٰتِ مَاۤءً ثَجَّاجًاۙ

wa-anzaln*aa* mina **a**lmu'*sh*ir*aa*ti m*aa*-an tsajj*aa*j*aa***n**

dan Kami turunkan dari awan, air hujan yang tercurah dengan hebatnya,

78:15

# لِّنُخْرِجَ بِهٖ حَبًّا وَّنَبَاتًاۙ

linukhrija bihi *h*abban wanab*aa*t*aa***n**

untuk Kami tumbuhkan dengan air itu biji-bijian dan tanam-tanaman,

78:16

# وَّجَنّٰتٍ اَلْفَافًاۗ

wajann*aa*tin **a**lf*aa*f*aa***n**

dan kebun-kebun yang rindang.

78:17

# اِنَّ يَوْمَ الْفَصْلِ كَانَ مِيْقَاتًاۙ

inna yawma **a**lfa*sh*li k*aa*na miiq*aa*t*aa***n**

Sungguh, hari keputusan adalah suatu waktu yang telah ditetapkan,

78:18

# يَّوْمَ يُنْفَخُ فِى الصُّوْرِ فَتَأْتُوْنَ اَفْوَاجًاۙ

yawma yunfakhu fii **al***shsh*uuri fata/tuuna afw*aa*j*aa***n**

(yaitu) pada hari (ketika) sangkakala ditiup, lalu kamu datang berbondong-bondong,

78:19

# وَّفُتِحَتِ السَّمَاۤءُ فَكَانَتْ اَبْوَابًاۙ

wafuti*h*ati **al**ssam*aa*u fak*aa*nat abw*aa*b*aa***n**

dan langit pun dibukalah, maka terdapatlah beberapa pintu,

78:20

# وَّسُيِّرَتِ الْجِبَالُ فَكَانَتْ سَرَابًاۗ

wasuyyirati **a**ljib*aa*lu fak*aa*nat sar*aa*b*aa***n**

dan gunung-gunung pun dijalankan sehingga menjadi fatamorgana.

78:21

# اِنَّ جَهَنَّمَ كَانَتْ مِرْصَادًاۙ

inna jahannama k*aa*nat mir*shaa*d*aa***n**

Sungguh, (neraka) Jahanam itu (sebagai) tempat mengintai (bagi penjaga yang mengawasi isi neraka),

78:22

# لِّلطّٰغِيْنَ مَاٰبًاۙ

li**l***ththaa*ghiina ma*aa*b*aa***n**

menjadi tempat kembali bagi orang-orang yang melampaui batas.

78:23

# لّٰبِثِيْنَ فِيْهَآ اَحْقَابًاۚ

l*aa*bitsiina fiih*aa* a*h*q*aa*b*aa***n**

Mereka tinggal di sana dalam masa yang lama,

78:24

# لَا يَذُوْقُوْنَ فِيْهَا بَرْدًا وَّلَا شَرَابًاۙ

l*aa* ya*dz*uuquuna fiih*aa* bardan wal*aa* syar*aa*b*aa***n**

mereka tidak merasakan kesejukan di dalamnya dan tidak (pula mendapat) minuman,

78:25

# اِلَّا حَمِيْمًا وَّغَسَّاقًاۙ

ill*aa* *h*amiiman waghass*aa*q*aa***n**

selain air yang mendidih dan nanah,

78:26

# جَزَاۤءً وِّفَاقًاۗ

jaz*aa*-an wif*aa*q*aa***n**

sebagai pembalasan yang setimpal.

78:27

# اِنَّهُمْ كَانُوْا لَا يَرْجُوْنَ حِسَابًاۙ

innahum k*aa*nuu l*aa* yarjuuna *h*is*aa*b*aa***n**

Sesungguhnya dahulu mereka tidak pernah mengharapkan perhitungan.

78:28

# وَّكَذَّبُوْا بِاٰيٰتِنَا كِذَّابًاۗ

waka*dzdz*abuu bi-*aa*y*aa*tin*aa* ki*dzdzaa*b*aa***n**

Dan mereka benar-benar mendustakan ayat-ayat Kami.

78:29

# وَكُلَّ شَيْءٍ اَحْصَيْنٰهُ كِتٰبًاۙ

wakulla syay-in a*hs*ayn*aa*hu kit*aa*b*aa***n**

Dan segala sesuatu telah Kami catat dalam suatu Kitab (buku catatan amalan manusia).

78:30

# فَذُوْقُوْا فَلَنْ نَّزِيْدَكُمْ اِلَّا عَذَابًا ࣖ

fa*dz*uuquu falan naziidakum ill*aa* 'a*dzaa*b*aa***n**

Maka karena itu rasakanlah! Maka tidak ada yang akan Kami tambahkan kepadamu selain azab.

78:31

# اِنَّ لِلْمُتَّقِيْنَ مَفَازًاۙ

inna lilmuttaqiina maf*aa*z*aa***n**

Sungguh, orang-orang yang bertakwa mendapat kemenangan,

78:32

# حَدَاۤىِٕقَ وَاَعْنَابًاۙ

*h*ad*aa*-iqa wa-a'n*aa*b*aa***n**

(yaitu) kebun-kebun dan buah anggur,

78:33

# وَّكَوَاعِبَ اَتْرَابًاۙ

wakaw*aa*'iba atr*aa*b*aa***n**

dan gadis-gadis montok yang sebaya,

78:34

# وَّكَأْسًا دِهَاقًاۗ

waka/san dih*aa*q*aa***n**

dan gelas-gelas yang penuh (berisi minuman).

78:35

# لَا يَسْمَعُوْنَ فِيْهَا لَغْوًا وَّلَا كِذَّابًا

l*aa* yasma'uuna fiih*aa* laghwan wal*aa* ki*dzdzaa*b*aa***n**

Di sana mereka tidak mendengar percakapan yang sia-sia maupun (perkataan) dusta.

78:36

# جَزَاۤءً مِّنْ رَّبِّكَ عَطَاۤءً حِسَابًاۙ

jaz*aa*-an min rabbika 'a*thaa*-an *h*is*aa*b*aa***n**

Sebagai balasan dan pemberian yang cukup banyak dari Tuhanmu,

78:37

# رَّبِّ السَّمٰوٰتِ وَالْاَرْضِ وَمَا بَيْنَهُمَا الرَّحْمٰنِ لَا يَمْلِكُوْنَ مِنْهُ خِطَابًاۚ

rabbi **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i wam*aa* baynahum*aa* **al**rra*h*m*aa*ni l*aa* yamlikuuna minhu khi*thaa*b*aa***n**

Tuhan (yang memelihara) langit dan bumi dan apa yang ada di antara keduanya; Yang Maha Pengasih, mereka tidak mampu berbicara dengan Dia.

78:38

# يَوْمَ يَقُوْمُ الرُّوْحُ وَالْمَلٰۤىِٕكَةُ صَفًّاۙ لَّا يَتَكَلَّمُوْنَ اِلَّا مَنْ اَذِنَ لَهُ الرَّحْمٰنُ وَقَالَ صَوَابًا

yawma yaquumu **al**rruu*h*u wa**a**lmal*aa*-ikatu *sh*affan l*aa* yatakallamuuna ill*aa* man a*dz*ina lahu **al**rra*h*m*aa*nu waq*aa*la *sh*aw*aa*b*aa*

Pada hari, ketika ruh dan para malaikat berdiri bersaf-saf, mereka tidak berkata-kata, kecuali siapa yang telah diberi izin kepadanya oleh Tuhan Yang Maha Pengasih dan dia hanya mengatakan yang benar.

78:39

# ذٰلِكَ الْيَوْمُ الْحَقُّۚ فَمَنْ شَاۤءَ اتَّخَذَ اِلٰى رَبِّهٖ مَاٰبًا

*dzaa*lika **a**lyawmu **a**l*h*aqqu faman sy*aa*-a ittakha*dz*a il*aa* rabbihi ma*aa*b*aa***n**

Itulah hari yang pasti terjadi. Maka barang siapa menghendaki, niscaya dia menempuh jalan kembali kepada Tuhannya.

78:40

# اِنَّآ اَنْذَرْنٰكُمْ عَذَابًا قَرِيْبًا ەۙ يَّوْمَ يَنْظُرُ الْمَرْءُ مَا قَدَّمَتْ يَدَاهُ وَيَقُوْلُ الْكٰفِرُ يٰلَيْتَنِيْ كُنْتُ تُرَابًا ࣖ

inn*aa* an*dz*arn*aa*kum 'a*dzaa*ban qariiban yawma yan*zh*uru **a**lmaru m*aa* qaddamat yad*aa*hu wayaquulu **a**lk*aa*firu y*aa* laytanii kuntu tur*aa*b*aa***n**

**Sesungguhnya Kami telah memperingatkan kepadamu (orang kafir) azab yang dekat, pada hari manusia melihat apa yang telah diperbuat oleh kedua tangannya; dan orang kafir berkata, “Alangkah baiknya seandainya dahulu aku jadi tanah.”**

<!--EndFragment-->