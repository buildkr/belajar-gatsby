---
title: (92) Al-Lail - الّيل
date: 2021-10-27T04:28:51.040Z
ayat: 92
description: "Jumlah Ayat: 21 / Arti: Malam"
---
<!--StartFragment-->

92:1

# وَالَّيْلِ اِذَا يَغْشٰىۙ

wa**a**llayli i*dzaa* yaghsy*aa*

Demi malam apabila menutupi (cahaya siang),

92:2

# وَالنَّهَارِ اِذَا تَجَلّٰىۙ

wa**al**nnah*aa*ri i*dzaa* tajall*aa*

demi siang apabila terang benderang,

92:3

# وَمَا خَلَقَ الذَّكَرَ وَالْاُنْثٰىٓ ۙ

wam*aa* khalaqa **al***dzdz*akara wa**a**l-unts*aa*

demi penciptaan laki-laki dan perempuan,

92:4

# اِنَّ سَعْيَكُمْ لَشَتّٰىۗ

inna sa'yakum lasyatt*aa*

sungguh, usahamu memang beraneka macam.

92:5

# فَاَمَّا مَنْ اَعْطٰى وَاتَّقٰىۙ

fa-amm*aa* man a'*thaa* wa**i**ttaq*aa*

Maka barangsiapa memberikan (hartanya di jalan Allah) dan bertakwa,

92:6

# وَصَدَّقَ بِالْحُسْنٰىۙ

wa*sh*addaqa bi**a**l*h*usn*aa*

dan membenarkan (adanya pahala) yang terbaik (surga),

92:7

# فَسَنُيَسِّرُهٗ لِلْيُسْرٰىۗ

fasanuyassiruhu lilyusr*aa*

maka akan Kami mudahkan baginya jalan menuju kemudahan (kebahagiaan),

92:8

# وَاَمَّا مَنْۢ بَخِلَ وَاسْتَغْنٰىۙ

wa-amm*aa* man bakhila wa**i**staghn*aa*

dan adapun orang yang kikir dan merasa dirinya cukup (tidak perlu pertolongan Allah),

92:9

# وَكَذَّبَ بِالْحُسْنٰىۙ

waka*dzdz*aba bi**a**l*h*usn*aa*

serta mendustakan (pahala) yang terbaik,

92:10

# فَسَنُيَسِّرُهٗ لِلْعُسْرٰىۗ

fasanuyassiruhu lil'usr*aa*

maka akan Kami mudahkan baginya jalan menuju kesukaran (kesengsaraan),

92:11

# وَمَا يُغْنِيْ عَنْهُ مَالُهٗٓ اِذَا تَرَدّٰىٓۙ

wam*aa* yughnii 'anhu m*aa*luhu i*dzaa* taradd*aa*

dan hartanya tidak bermanfaat baginya apabila dia telah binasa.

92:12

# اِنَّ عَلَيْنَا لَلْهُدٰىۖ

inna 'alayn*aa* lalhud*aa*

Sesungguhnya Kamilah yang memberi petunjuk,

92:13

# وَاِنَّ لَنَا لَلْاٰخِرَةَ وَالْاُوْلٰىۗ

wa-inna lan*aa* lal-*aa*khirata wa**a**l-uul*aa*

dan sesungguhnya milik Kamilah akhirat dan dunia itu.

92:14

# فَاَنْذَرْتُكُمْ نَارًا تَلَظّٰىۚ

fa-an*dz*artukum n*aa*ran tala*zhzhaa*

Maka Aku memperingatkan kamu dengan neraka yang menyala-nyala,

92:15

# لَا يَصْلٰىهَآ اِلَّا الْاَشْقَىۙ

l*aa* ya*sh*l*aa*h*aa* ill*aa* **a**l-asyq*aa*

yang hanya dimasuki oleh orang yang paling celaka,

92:16

# الَّذِيْ كَذَّبَ وَتَوَلّٰىۗ

**al**la*dz*ii ka*dzdz*aba watawall*aa*

yang mendustakan (kebenaran) dan berpaling (dari iman).

92:17

# وَسَيُجَنَّبُهَا الْاَتْقَىۙ

wasayujannabuh*aa* **a**l-atq*aa*

Dan akan dijauhkan darinya (neraka) orang yang paling bertakwa,

92:18

# الَّذِيْ يُؤْتِيْ مَالَهٗ يَتَزَكّٰىۚ

**al**la*dz*ii yu/tii m*aa*lahu yatazakk*aa*

yang menginfakkan hartanya (di jalan Allah) untuk membersihkan (dirinya),

92:19

# وَمَا لِاَحَدٍ عِنْدَهٗ مِنْ نِّعْمَةٍ تُجْزٰىٓۙ

wam*aa* li-a*h*adin 'indahu min ni'matin tujz*aa*

dan tidak ada seorang pun memberikan suatu nikmat padanya yang harus dibalasnya,

92:20

# اِلَّا ابْتِغَاۤءَ وَجْهِ رَبِّهِ الْاَعْلٰىۚ

ill*aa* ibtigh*aa*-a wajhi rabbihi **a**l-a'l*aa*

tetapi (dia memberikan itu semata-mata) karena mencari keridaan Tuhannya Yang Mahatinggi.

92:21

# وَلَسَوْفَ يَرْضٰى ࣖ

walasawfa yar*daa*

Dan niscaya kelak dia akan mendapat kesenangan (yang sempurna).

<!--EndFragment-->