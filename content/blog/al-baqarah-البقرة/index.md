---
title: (2) Al-Baqarah - البقرة
date: 2021-10-26T08:51:48.256Z
ayat: 2
description: "Jumlah Ayat: 286 / Arti: Sapi"
---
<!--StartFragment-->

2:1

# الۤمّۤ ۚ

alif-l*aa*m-miim

Alif Lam Mim.

2:2

# ذٰلِكَ الْكِتٰبُ لَا رَيْبَ ۛ فِيْهِ ۛ هُدًى لِّلْمُتَّقِيْنَۙ

*dzaa*lika **a**lkit*aa*bu l*aa* rayba fiihi hudan lilmuttaqiin**a**

Kitab (Al-Qur'an) ini tidak ada keraguan padanya; petunjuk bagi mereka yang bertakwa,

2:3

# الَّذِيْنَ يُؤْمِنُوْنَ بِالْغَيْبِ وَيُقِيْمُوْنَ الصَّلٰوةَ وَمِمَّا رَزَقْنٰهُمْ يُنْفِقُوْنَ ۙ

**al**la*dz*iina yu/minuuna bi**a**lghaybi wayuqiimuuna **al***shsh*al*aa*ta wamimm*aa *razaqn*aa*hum yunfiquun**a**

(yaitu) mereka yang beriman kepada yang gaib, melaksanakan salat, dan menginfakkan sebagian rezeki yang Kami berikan kepada mereka,

2:4

# وَالَّذِيْنَ يُؤْمِنُوْنَ بِمَآ اُنْزِلَ اِلَيْكَ وَمَآ اُنْزِلَ مِنْ قَبْلِكَ ۚ وَبِالْاٰخِرَةِ هُمْ يُوْقِنُوْنَۗ

wa**a**lla*dz*iina yu/minuuna bim*aa* unzila ilayka wam*aa* unzila min qablika wabi**a**l-*aa*khirati hum yuuqinuun**a**

dan mereka yang beriman kepada (Al-Qur'an) yang diturunkan kepadamu (Muhammad) dan (kitab-kitab) yang telah diturunkan sebelum engkau, dan mereka yakin akan adanya akhirat.

2:5

# اُولٰۤىِٕكَ عَلٰى هُدًى مِّنْ رَّبِّهِمْ ۙ وَاُولٰۤىِٕكَ هُمُ الْمُفْلِحُوْنَ

ul*aa*-ika 'al*aa* hudan min rabbihim waul*aa*-ika humu **a**lmufli*h*uun**a**

Merekalah yang mendapat petunjuk dari Tuhannya, dan mereka itulah orang-orang yang beruntung.

2:6

# اِنَّ الَّذِيْنَ كَفَرُوْا سَوَاۤءٌ عَلَيْهِمْ ءَاَنْذَرْتَهُمْ اَمْ لَمْ تُنْذِرْهُمْ لَا يُؤْمِنُوْنَ

inna **al**la*dz*iina kafaruu saw*aa*un 'alayhim a-an*dz*artahum am lam tun*dz*irhum l*aa* yu/minuun**a**

Sesungguhnya orang-orang kafir, sama saja bagi mereka, engkau (Muhammad) beri peringatan atau tidak engkau beri peringatan, mereka tidak akan beriman.

2:7

# خَتَمَ اللّٰهُ عَلٰى قُلُوْبِهِمْ وَعَلٰى سَمْعِهِمْ ۗ وَعَلٰٓى اَبْصَارِهِمْ غِشَاوَةٌ وَّلَهُمْ عَذَابٌ عَظِيْمٌ ࣖ

khatama **al**l*aa*hu 'al*aa* quluubihim wa'al*aa* sam'ihim wa'al*aa* ab*shaa*rihim ghisy*aa*watun walahum 'a*dzaa*bun 'a*zh*iim**un**

Allah telah mengunci hati dan pendengaran mereka, penglihatan mereka telah tertutup, dan mereka akan mendapat azab yang berat.

2:8

# وَمِنَ النَّاسِ مَنْ يَّقُوْلُ اٰمَنَّا بِاللّٰهِ وَبِالْيَوْمِ الْاٰخِرِ وَمَا هُمْ بِمُؤْمِنِيْنَۘ

wamina **al**nn*aa*si man yaquulu *aa*mann*aa* bi ill*aa*hi wa billyaumil akhiri wam*aa* hum bimu/miniin**a**

Dan di antara manusia ada yang berkata, “Kami beriman kepada Allah dan hari akhir,” padahal sesungguhnya mereka itu bukanlah orang-orang yang beriman.

2:9

# يُخٰدِعُوْنَ اللّٰهَ وَالَّذِيْنَ اٰمَنُوْا ۚ وَمَا يَخْدَعُوْنَ اِلَّآ اَنْفُسَهُمْ وَمَا يَشْعُرُوْنَۗ

yukh*aa*di'uuna **al**l*aa*ha wa**a**lla*dz*iina *aa*manuu wam*aa* yakhda'uuna ill*aa* anfusahum wam*aa* yasy'uruun**a**

Mereka menipu Allah dan orang-orang yang beriman, padahal mereka hanyalah menipu diri sendiri tanpa mereka sadari.

2:10

# فِيْ قُلُوْبِهِمْ مَّرَضٌۙ فَزَادَهُمُ اللّٰهُ مَرَضًاۚ وَلَهُمْ عَذَابٌ اَلِيْمٌ ۢ ەۙ بِمَا كَانُوْا يَكْذِبُوْنَ

fii quluubihim mara*dh*un faz*aa*dahumu **al**l*aa*hu mara*dh*an walahum 'a*dzaa*bun **a**liimun bim*aa* k*aa*nuu yak*dz*ibuun**a**

Dalam hati mereka ada penyakit, lalu Allah menambah penyakitnya itu; dan mereka mendapat azab yang pedih, karena mereka berdusta.

2:11

# وَاِذَا قِيْلَ لَهُمْ لَا تُفْسِدُوْا فِى الْاَرْضِۙ قَالُوْٓا اِنَّمَا نَحْنُ مُصْلِحُوْنَ

wa-i*dzaa* qiila lahum l*aa* tufsiduu fii **a**l-ar*dh*i q*aa*luu innam*aa* na*h*nu mu*sh*li*h*uun**a**

Dan apabila dikatakan kepada mereka, “Janganlah berbuat kerusakan di bumi!” Mereka menjawab, “Sesungguhnya kami justru orang-orang yang melakukan perbaikan.”

2:12

# اَلَآ اِنَّهُمْ هُمُ الْمُفْسِدُوْنَ وَلٰكِنْ لَّا يَشْعُرُوْنَ

al*aa* innahum humu **a**lmufsiduuna wal*aa*kin l*aa* yasy'uruun**a**

Ingatlah, sesungguhnya merekalah yang berbuat kerusakan, tetapi mereka tidak menyadari.

2:13

# وَاِذَا قِيْلَ لَهُمْ اٰمِنُوْا كَمَآ اٰمَنَ النَّاسُ قَالُوْٓا اَنُؤْمِنُ كَمَآ اٰمَنَ السُّفَهَاۤءُ ۗ اَلَآ اِنَّهُمْ هُمُ السُّفَهَاۤءُ وَلٰكِنْ لَّا يَعْلَمُوْنَ

wa-i*dzaa* qiila lahum *aa*minuu kam*aa* *aa*mana **al**nn*aa*su q*aa*luu anu/minu kam*aa* *aa*mana **al**ssufah*aa*u **a**l*aa* innahum humu **al**ssufa

Dan apabila dikatakan kepada mereka, “Berimanlah kamu sebagaimana orang lain telah beriman!” Mereka menjawab, “Apakah kami akan beriman seperti orang-orang yang kurang akal itu beriman?” Ingatlah, sesungguhnya mereka itulah orang-orang yang kurang akal, t

2:14

# وَاِذَا لَقُوا الَّذِيْنَ اٰمَنُوْا قَالُوْٓا اٰمَنَّا ۚ وَاِذَا خَلَوْا اِلٰى شَيٰطِيْنِهِمْ ۙ قَالُوْٓا اِنَّا مَعَكُمْ ۙاِنَّمَا نَحْنُ مُسْتَهْزِءُوْنَ

wa-i*dzaa* laquu **al**la*dz*iina *aa*manuu q*aa*luu *aa*mann*aa* wa-i*dzaa* khalaw il*aa* syay*aath*iinihim q*aa*luu inn*aa* ma'akum innam*aa* na*h*nu mustahzi-uun**a**

**Dan apabila mereka berjumpa dengan orang yang beriman, mereka berkata, “Kami telah beriman.” Tetapi apabila mereka kembali kepada setan-setan (para pemimpin) mereka, mereka berkata, “Sesungguhnya kami bersama kamu, kami hanya berolok-olok.”**

2:15

# اَللّٰهُ يَسْتَهْزِئُ بِهِمْ وَيَمُدُّهُمْ فِيْ طُغْيَانِهِمْ يَعْمَهُوْنَ

**al**l*aa*hu yastahzi-u bihim wayamudduhum fii *th*ughy*aa*nihim ya'mahuun**a**

Allah akan memperolok-olokkan mereka dan membiarkan mereka terombang-ambing dalam kesesatan.

2:16

# اُولٰۤىِٕكَ الَّذِيْنَ اشْتَرَوُا الضَّلٰلَةَ بِالْهُدٰىۖ فَمَا رَبِحَتْ تِّجَارَتُهُمْ وَمَا كَانُوْا مُهْتَدِيْنَ

ul*aa*-ika **al**la*dz*iina isytarawuu **al***dhdh*al*aa*lata bi**a**lhud*aa *fam*aa *rabi*h*at tij*aa*ratuhum wam*aa *k*aa*nuu muhtadiin**a**

Mereka itulah yang membeli kesesatan dengan petunjuk. Maka perdagangan mereka itu tidak beruntung dan mereka tidak mendapat petunjuk.

2:17

# مَثَلُهُمْ كَمَثَلِ الَّذِى اسْتَوْقَدَ نَارًا ۚ فَلَمَّآ اَضَاۤءَتْ مَا حَوْلَهٗ ذَهَبَ اللّٰهُ بِنُوْرِهِمْ وَتَرَكَهُمْ فِيْ ظُلُمٰتٍ لَّا يُبْصِرُوْنَ

matsaluhum kamatsali **al**la*dz*ii istawqada n*aa*ran falamm*aa* a*dhaa*-at m*aa* *h*awlahu *dz*ahaba **al**l*aa*hu binuurihim watarakahum fii *zh*ulum*aa*tin l*aa* yub*sh*

Perumpamaan mereka seperti orang-orang yang menyalakan api, setelah menerangi sekelilingnya, Allah melenyapkan cahaya (yang menyinari) mereka dan membiarkan mereka dalam kegelapan, tidak dapat melihat.

2:18

# صُمٌّ ۢ بُكْمٌ عُمْيٌ فَهُمْ لَا يَرْجِعُوْنَۙ

*sh*ummun bukmun 'umyun fahum l*aa* yarji'uun**a**

Mereka tuli, bisu dan buta, sehingga mereka tidak dapat kembali.

2:19

# اَوْ كَصَيِّبٍ مِّنَ السَّمَاۤءِ فِيْهِ ظُلُمٰتٌ وَّرَعْدٌ وَّبَرْقٌۚ يَجْعَلُوْنَ اَصَابِعَهُمْ فِيْٓ اٰذَانِهِمْ مِّنَ الصَّوَاعِقِ حَذَرَ الْمَوْتِۗ وَاللّٰهُ مُحِيْطٌۢ بِالْكٰفِرِيْنَ

aw ka*sh*ayyibin mina **al**ssam*aa*-i fiihi *zh*ulum*aa*tun wara'dun wabarqun yaj'aluuna a*shaa*bi'ahum fii *aatsaa*nihim mina **al***shsh*aw*aa*'iqi* h*a*dz*ara **a**

**Atau seperti (orang yang ditimpa) hujan lebat dari langit, yang disertai kegelapan, petir dan kilat. Mereka menyumbat telinga dengan jari-jarinya, (menghindari) suara petir itu karena takut mati. Allah meliputi orang-orang yang kafir.**

2:20

# يَكَادُ الْبَرْقُ يَخْطَفُ اَبْصَارَهُمْ ۗ كُلَّمَآ اَضَاۤءَ لَهُمْ مَّشَوْا فِيْهِ ۙ وَاِذَآ اَظْلَمَ عَلَيْهِمْ قَامُوْا ۗوَلَوْ شَاۤءَ اللّٰهُ لَذَهَبَ بِسَمْعِهِمْ وَاَبْصَارِهِمْ ۗ اِنَّ اللّٰهَ عَلٰى كُلِّ شَيْءٍ قَدِيْرٌ ࣖ

yak*aa*du **a**lbarqu yakh*th*afu ab*shaa*rahum kullam*aa* a*dhaa*-a lahum masyaw fiihi wa-i*dzaa* a*zh*lama 'alayhim q*aa*muu walaw sy*aa*-a **al**l*aa*hu la*dz*ahaba bisam'

Hampir saja kilat itu menyambar penglihatan mereka. Setiap kali (kilat itu) menyinari, mereka berjalan di bawah (sinar) itu, dan apabila gelap menerpa mereka, mereka berhenti. Sekiranya Allah menghendaki, niscaya Dia hilangkan pendengaran dan penglihatan

2:21

# يٰٓاَيُّهَا النَّاسُ اعْبُدُوْا رَبَّكُمُ الَّذِيْ خَلَقَكُمْ وَالَّذِيْنَ مِنْ قَبْلِكُمْ لَعَلَّكُمْ تَتَّقُوْنَۙ

y*aa* ayyuh*aa* **al**nn*aa*su u'buduu rabbakumu **al**la*dz*ii khalaqakum wa**a**lla*dz*iina min qablikum la'allakum tattaquun**a**

Wahai manusia! Sembahlah Tuhanmu yang telah menciptakan kamu dan orang-orang yang sebelum kamu, agar kamu bertakwa.

2:22

# الَّذِيْ جَعَلَ لَكُمُ الْاَرْضَ فِرَاشًا وَّالسَّمَاۤءَ بِنَاۤءً ۖوَّاَنْزَلَ مِنَ السَّمَاۤءِ مَاۤءً فَاَخْرَجَ بِهٖ مِنَ الثَّمَرٰتِ رِزْقًا لَّكُمْ ۚ فَلَا تَجْعَلُوْا لِلّٰهِ اَنْدَادًا وَّاَنْتُمْ تَعْلَمُوْنَ

**al**la*dz*ii ja'ala lakumu **a**l-ar*dh*a fir*aa*syan wa**al**ssam*aa*-a bin*aa*-an wa-anzala mina **al**ssam*aa*-i m*aa*-an fa-akhraja bihi mina **al**t

(Dialah) yang menjadikan bumi sebagai hamparan bagimu dan langit sebagai atap, dan Dialah yang menurunkan air (hujan) dari langit, lalu Dia hasilkan dengan (hujan) itu buah-buahan sebagai rezeki untukmu. Karena itu janganlah kamu mengadakan tandingan-tand

2:23

# وَاِنْ كُنْتُمْ فِيْ رَيْبٍ مِّمَّا نَزَّلْنَا عَلٰى عَبْدِنَا فَأْتُوْا بِسُوْرَةٍ مِّنْ مِّثْلِهٖ ۖ وَادْعُوْا شُهَدَاۤءَكُمْ مِّنْ دُوْنِ اللّٰهِ اِنْ كُنْتُمْ صٰدِقِيْنَ

wa-in kuntum fii raybin mimm*aa* nazzaln*aa* 'al*aa* 'abdin*aa* fa/tuu bisuuratin min mitslihi wa**u**d'uu syuhad*aa*-akum min duuni **al**l*aa*hi in kuntum *shaa*diqiin**a**

Dan jika kamu meragukan (Al-Qur'an) yang Kami turunkan kepada hamba Kami (Muhammad), maka buatlah satu surah semisal dengannya dan ajaklah penolong-penolongmu selain Allah, jika kamu orang-orang yang benar.

2:24

# فَاِنْ لَّمْ تَفْعَلُوْا وَلَنْ تَفْعَلُوْا فَاتَّقُوا النَّارَ الَّتِيْ وَقُوْدُهَا النَّاسُ وَالْحِجَارَةُ ۖ اُعِدَّتْ لِلْكٰفِرِيْنَ

fa-in lam taf'aluu walan taf'aluu fa**i**ttaquu **al**nn*aa*ra **al**latii waquuduh*aa* **al**nn*aa*su wa**a**l*h*ij*aa*ratu u'iddat lilk*aa*firiin**a**

**Jika kamu tidak mampu membuatnya, dan (pasti) tidak akan mampu, maka takutlah kamu akan api neraka yang bahan bakarnya manusia dan batu, yang disediakan bagi orang-orang kafir.**

2:25

# وَبَشِّرِ الَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِ اَنَّ لَهُمْ جَنّٰتٍ تَجْرِيْ مِنْ تَحْتِهَا الْاَنْهٰرُ ۗ كُلَّمَا رُزِقُوْا مِنْهَا مِنْ ثَمَرَةٍ رِّزْقًا ۙ قَالُوْا هٰذَا الَّذِيْ رُزِقْنَا مِنْ قَبْلُ وَاُتُوْا بِهٖ مُتَشَابِهًا ۗوَلَهُمْ فِيْه

wabasysyiri **al**la*dz*iina *aa*manuu wa'amiluu **al***shshaa*li*haa*ti anna lahum jann*aa*tin tajrii min ta*h*tih*aa* **a**l-anh*aa*ru kullam*aa *ruziquu minh*aa* m

Dan sampaikanlah kabar gembira kepada orang-orang yang beriman dan berbuat kebajikan, bahwa untuk mereka (disediakan) surga-surga yang mengalir di bawahnya sungai-sungai. Setiap kali mereka diberi rezeki buah-buahan dari surga, mereka berkata, “Inilah rez

2:26

# ۞ اِنَّ اللّٰهَ لَا يَسْتَحْيٖٓ اَنْ يَّضْرِبَ مَثَلًا مَّا بَعُوْضَةً فَمَا فَوْقَهَا ۗ فَاَمَّا الَّذِيْنَ اٰمَنُوْا فَيَعْلَمُوْنَ اَنَّهُ الْحَقُّ مِنْ رَّبِّهِمْ ۚ وَاَمَّا الَّذِيْنَ كَفَرُوْا فَيَقُوْلُوْنَ مَاذَآ اَرَادَ اللّٰهُ بِهٰذَا مَثَلًا ۘ

inna **al**l*aa*ha l*aa* yasta*h*yii an ya*dh*riba matsalan m*aa* ba'uu*dh*atan fam*aa* fawqah*aa* fa-amm*aa* **al**la*dz*iina *aa*manuu faya'lamuuna annahu **a**

**Sesungguhnya Allah tidak segan membuat perumpamaan seekor nyamuk atau yang lebih kecil dari itu. Adapun orang-orang yang beriman, mereka tahu bahwa itu kebenaran dari Tuhan. Tetapi mereka yang kafir berkata, “Apa maksud Allah dengan perumpamaan ini?” Deng**

2:27

# الَّذِيْنَ يَنْقُضُوْنَ عَهْدَ اللّٰهِ مِنْۢ بَعْدِ مِيْثَاقِهٖۖ وَيَقْطَعُوْنَ مَآ اَمَرَ اللّٰهُ بِهٖٓ اَنْ يُّوْصَلَ وَيُفْسِدُوْنَ فِى الْاَرْضِۗ اُولٰۤىِٕكَ هُمُ الْخٰسِرُوْنَ

**al**la*dz*iina yanqu*dh*uuna 'ahda **al**l*aa*hi min ba'di miits*aa*qihi wayaq*th*a'uuna m*aa* amara **al**l*aa*hu bihi an yuu*sh*ala wayufsiduuna fii **a**l-ar<

(yaitu) orang-orang yang melanggar perjanjian Allah setelah (perjanjian) itu diteguhkan, dan memutuskan apa yang diperintahkan Allah untuk disambungkan dan berbuat kerusakan di bumi. Mereka itulah orang-orang yang rugi.

2:28

# كَيْفَ تَكْفُرُوْنَ بِاللّٰهِ وَكُنْتُمْ اَمْوَاتًا فَاَحْيَاكُمْۚ ثُمَّ يُمِيْتُكُمْ ثُمَّ يُحْيِيْكُمْ ثُمَّ اِلَيْهِ تُرْجَعُوْنَ

kayfa takfuruuna biall*aa*hi wakuntum amw*aa*tan fa-a*h*y*aa*kum tsumma yumiitukum tsumma yu*h*yiikum tsumma ilayhi turja'uun**a**

Bagaimana kamu ingkar kepada Allah, padahal kamu (tadinya) mati, lalu Dia menghidupkan kamu, kemudian Dia mematikan kamu lalu Dia menghidupkan kamu kembali. Kemudian kepada-Nyalah kamu dikembalikan.

2:29

# هُوَ الَّذِيْ خَلَقَ لَكُمْ مَّا فِى الْاَرْضِ جَمِيْعًا ثُمَّ اسْتَوٰٓى اِلَى السَّمَاۤءِ فَسَوّٰىهُنَّ سَبْعَ سَمٰوٰتٍ ۗ وَهُوَ بِكُلِّ شَيْءٍ عَلِيْمٌ ࣖ

huwa **al**la*dz*ii khalaqa lakum m*aa* fii **a**l-ar*dh*i jamii'an tsumma istaw*aa* il*aa* **al**ssam*aa*-i fasaww*aa*hunna sab'a sam*aa*w*aa*tin wahuwa bikulli syay-in '

Dialah (Allah) yang menciptakan segala apa yang ada di bumi untukmu kemudian Dia menuju ke langit, lalu Dia menyempurnakannya menjadi tujuh langit. Dan Dia Maha Mengetahui segala sesuatu.

2:30

# وَاِذْ قَالَ رَبُّكَ لِلْمَلٰۤىِٕكَةِ ِانِّيْ جَاعِلٌ فِى الْاَرْضِ خَلِيْفَةً ۗ قَالُوْٓا اَتَجْعَلُ فِيْهَا مَنْ يُّفْسِدُ فِيْهَا وَيَسْفِكُ الدِّمَاۤءَۚ وَنَحْنُ نُسَبِّحُ بِحَمْدِكَ وَنُقَدِّسُ لَكَ ۗ قَالَ اِنِّيْٓ اَعْلَمُ مَا لَا تَعْلَمُوْنَ

wa-i*dz* q*aa*la rabbuka lilmal*aa*-ikati innii j*aa*'ilun fii **a**l-ar*dh*i khaliifatan q*aa*luu ataj'alu fiih*aa* man yufsidu fiih*aa* wayasfiku **al**ddim*aa*-a wana*h*nu nusa

Dan (ingatlah) ketika Tuhanmu berfirman kepada para malaikat, “Aku hendak menjadikan khalifah di bumi.” Mereka berkata, “Apakah Engkau hendak menjadikan orang yang merusak dan menumpahkan darah di sana, sedangkan kami bertasbih memuji-Mu dan menyucikan na

2:31

# وَعَلَّمَ اٰدَمَ الْاَسْمَاۤءَ كُلَّهَا ثُمَّ عَرَضَهُمْ عَلَى الْمَلٰۤىِٕكَةِ فَقَالَ اَنْۢبِـُٔوْنِيْ بِاَسْمَاۤءِ هٰٓؤُلَاۤءِ اِنْ كُنْتُمْ صٰدِقِيْنَ

wa'allama *aa*dama **a**l-asm*aa*-a kullah*aa* tsumma 'ara*dh*ahum 'al*aa* **a**lmal*aa*-ikati faq*aa*la anbi-uunii bi-asm*aa*-i h*aa*ul*aa*-i in kuntum *shaa*diqiin**a**

Dan Dia ajarkan kepada Adam nama-nama (benda) semuanya, kemudian Dia perlihatkan kepada para malaikat, seraya berfirman, “Sebutkan kepada-Ku nama semua (benda) ini, jika kamu yang benar!”

2:32

# قَالُوْا سُبْحٰنَكَ لَا عِلْمَ لَنَآ اِلَّا مَا عَلَّمْتَنَا ۗاِنَّكَ اَنْتَ الْعَلِيْمُ الْحَكِيْمُ

q*aa*luu sub*haa*naka l*aa* 'ilma lan*aa* ill*aa* m*aa* 'allamtan*aa* innaka anta **a**l'aliimu **a**l*h*akiim**u**

Mereka menjawab, “Mahasuci Engkau, tidak ada yang kami ketahui selain apa yang telah Engkau ajarkan kepada kami. Sungguh, Engkaulah Yang Maha Mengetahui, Mahabijaksana.”

2:33

# قَالَ يٰٓاٰدَمُ اَنْۢبِئْهُمْ بِاَسْمَاۤىِٕهِمْ ۚ فَلَمَّآ اَنْۢبَاَهُمْ بِاَسْمَاۤىِٕهِمْۙ قَالَ اَلَمْ اَقُلْ لَّكُمْ اِنِّيْٓ اَعْلَمُ غَيْبَ السَّمٰوٰتِ وَالْاَرْضِۙ وَاَعْلَمُ مَا تُبْدُوْنَ وَمَا كُنْتُمْ تَكْتُمُوْنَ

q*aa*la y*aa* *aa*damu anbi/hum bi-asm*aa*-ihim falamm*aa* anba-ahum bi-asm*aa*-ihim q*aa*la alam aqul lakum innii a'lamu ghayba **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i wa-a'lamu m

Dia (Allah) berfirman, “Wahai Adam! Beritahukanlah kepada mereka nama-nama itu!” Setelah dia (Adam) menyebutkan nama-namanya, Dia berfirman, “Bukankah telah Aku katakan kepadamu, bahwa Aku mengetahui rahasia langit dan bumi, dan Aku mengetahui apa yang ka

2:34

# وَاِذْ قُلْنَا لِلْمَلٰۤىِٕكَةِ اسْجُدُوْا لِاٰدَمَ فَسَجَدُوْٓا اِلَّآ اِبْلِيْسَۗ اَبٰى وَاسْتَكْبَرَۖ وَكَانَ مِنَ الْكٰفِرِيْنَ

wa-i*dz* quln*aa* lilmal*aa*-ikati usjuduu li-*aa*dama fasajaduu ill*aa* ibliisa ab*aa* wa**i**stakbara wak*aa*na mina **a**lk*aa*firiin**a**

Dan (ingatlah) ketika Kami berfirman kepada para malaikat, “Sujudlah kamu kepada Adam!” Maka mereka pun sujud kecuali Iblis. Ia menolak dan menyombongkan diri, dan ia termasuk golongan yang kafir.

2:35

# وَقُلْنَا يٰٓاٰدَمُ اسْكُنْ اَنْتَ وَزَوْجُكَ الْجَنَّةَ وَكُلَا مِنْهَا رَغَدًا حَيْثُ شِئْتُمَاۖ وَلَا تَقْرَبَا هٰذِهِ الشَّجَرَةَ فَتَكُوْنَا مِنَ الظّٰلِمِيْنَ

waquln*aa* y*aa* *aa*damu uskun anta wazawjuka **a**ljannata wakul*aa* minh*aa* raghadan *h*aytsu syi/tum*aa* wal*aa* taqrab*aa* h*aadz*ihi **al**sysyajarata fatakuun*aa* min

Dan Kami berfirman, “Wahai Adam! Tinggallah engkau dan istrimu di dalam surga, dan makanlah dengan nikmat (berbagai makanan) yang ada di sana sesukamu. (Tetapi) janganlah kamu dekati pohon ini, nanti kamu termasuk orang-orang yang zalim!”

2:36

# فَاَزَلَّهُمَا الشَّيْطٰنُ عَنْهَا فَاَخْرَجَهُمَا مِمَّا كَانَا فِيْهِ ۖ وَقُلْنَا اهْبِطُوْا بَعْضُكُمْ لِبَعْضٍ عَدُوٌّ ۚ وَلَكُمْ فِى الْاَرْضِ مُسْتَقَرٌّ وَّمَتَاعٌ اِلٰى حِيْنٍ

fa-azallahum*aa* **al**sysyay*thaa*nu 'anh*aa* fa-akhrajahum*aa* mimm*aa* k*aa*n*aa* fiihi waquln*aa* ihbi*th*uu ba'*dh*ukum liba'*dh*in 'aduwwun walakum fii **a**l-ar

Lalu setan memperdayakan keduanya dari surga sehingga keduanya dikeluarkan dari (segala kenikmatan) ketika keduanya di sana (surga). Dan Kami berfirman, “Turunlah kamu! Sebagian kamu menjadi musuh bagi yang lain. Dan bagi kamu ada tempat tinggal dan kesen

2:37

# فَتَلَقّٰٓى اٰدَمُ مِنْ رَّبِّهٖ كَلِمٰتٍ فَتَابَ عَلَيْهِ ۗ اِنَّهٗ هُوَ التَّوَّابُ الرَّحِيْمُ

fatalaqq*aa* *aa*damu min rabbihi kalim*aa*tin fat*aa*ba 'alayhi innahu huwa **al**ttaww*aa*bu **al**rra*h*iim**u**

Kemudian Adam menerima beberapa kalimat dari Tuhannya, lalu Dia pun menerima tobatnya. Sungguh, Allah Maha Penerima tobat, Maha Penyayang.

2:38

# قُلْنَا اهْبِطُوْا مِنْهَا جَمِيْعًا ۚ فَاِمَّا يَأْتِيَنَّكُمْ مِّنِّيْ هُدًى فَمَنْ تَبِعَ هُدَايَ فَلَا خَوْفٌ عَلَيْهِمْ وَلَا هُمْ يَحْزَنُوْنَ

quln*aa* ihbi*th*uu minh*aa* jamii'an fa-imm*aa* ya/tiyannakum minnii hudan faman tabi'a hud*aa*ya fal*aa* khawfun 'alayhim wal*aa* hum ya*h*zanuun**a**

Kami berfirman, “Turunlah kamu semua dari surga! Kemudian jika benar-benar datang petunjuk-Ku kepadamu, maka barangsiapa mengikuti petunjuk-Ku, tidak ada rasa takut pada mereka dan mereka tidak bersedih hati.”

2:39

# وَالَّذِيْنَ كَفَرُوْا وَكَذَّبُوْا بِاٰيٰتِنَآ اُولٰۤىِٕكَ اَصْحٰبُ النَّارِ ۚ هُمْ فِيْهَا خٰلِدُوْنَ ࣖ

wa**a**lla*dz*iina kafaruu waka*dzdz*abuu bi-*aa*y*aa*tin*aa* ul*aa*-ika a*sh*-*haa*bu **al**nn*aa*ri hum fiih*aa* kh*aa*liduun**a**

Adapun orang-orang yang kafir dan mendustakan ayat-ayat Kami, mereka itu penghuni neraka. Mereka kekal di dalamnya.

2:40

# يٰبَنِيْٓ اِسْرَاۤءِيْلَ اذْكُرُوْا نِعْمَتِيَ الَّتِيْٓ اَنْعَمْتُ عَلَيْكُمْ وَاَوْفُوْا بِعَهْدِيْٓ اُوْفِ بِعَهْدِكُمْۚ وَاِيَّايَ فَارْهَبُوْنِ

y*aa* banii isr*aa*-iila u*dz*kuruu ni'matiya **al**latii an'amtu 'alaykum wa-awfuu bi'ahdii uufi bi'ahdikum wa-iyy*aa*ya fa**i**rhabuun**i**

Wahai Bani Israil! Ingatlah nikmat-Ku yang telah Aku berikan kepadamu. Dan penuhilah janjimu kepada-Ku, niscaya Aku penuhi janji-Ku kepadamu, dan takutlah kepada-Ku saja.

2:41

# وَاٰمِنُوْا بِمَآ اَنْزَلْتُ مُصَدِّقًا لِّمَا مَعَكُمْ وَلَا تَكُوْنُوْٓا اَوَّلَ كَافِرٍۢ بِهٖ ۖ وَلَا تَشْتَرُوْا بِاٰيٰتِيْ ثَمَنًا قَلِيْلًا ۖوَّاِيَّايَ فَاتَّقُوْنِ

wa*aa*minuu bim*aa* anzaltu mu*sh*addiqan lim*aa* ma'akum wal*aa* takuunuu awwala k*aa*firin bihi wal*aa* tasytaruu bi-*aa*y*aa*tii tsamanan qaliilan wa-iyy*aa*ya fa**i**ttaquun**i**

**Dan berimanlah kamu kepada apa (Al-Qur'an) yang telah Aku turunkan yang membenarkan apa (Taurat) yang ada pada kamu, dan janganlah kamu menjadi orang yang pertama kafir kepadanya. Janganlah kamu jual ayat-ayat-Ku dengan harga murah, dan bertakwalah hanya**

2:42

# وَلَا تَلْبِسُوا الْحَقَّ بِالْبَاطِلِ وَتَكْتُمُوا الْحَقَّ وَاَنْتُمْ تَعْلَمُوْنَ

wal*aa* talbisuu **a**l*h*aqqa bi**a**lb*aath*ili wataktumuu **a**l*h*aqqa wa-antum ta'lamuun**a**

Dan janganlah kamu campuradukkan kebenaran dengan kebatilan dan (janganlah) kamu sembunyikan kebenaran, sedangkan kamu mengetahuinya.

2:43

# وَاَقِيْمُوا الصَّلٰوةَ وَاٰتُوا الزَّكٰوةَ وَارْكَعُوْا مَعَ الرَّاكِعِيْنَ

wa-aqiimuu **al***shsh*al*aa*ta wa*aa*tuu **al**zzak*aa*ta wa**i**rka'uu ma'a **al**rr*aa*ki'iin**a**

Dan laksanakanlah salat, tunaikanlah zakat, dan rukuklah beserta orang yang rukuk.

2:44

# ۞ اَتَأْمُرُوْنَ النَّاسَ بِالْبِرِّ وَتَنْسَوْنَ اَنْفُسَكُمْ وَاَنْتُمْ تَتْلُوْنَ الْكِتٰبَ ۗ اَفَلَا تَعْقِلُوْنَ

ata/muruuna **al**nn*aa*sa bi**a**lbirri watansawna anfusakum wa-antum tatluuna **a**lkit*aa*ba afal*aa* ta'qiluun**a**

Mengapa kamu menyuruh orang lain (mengerjakan) kebajikan, sedangkan kamu melupakan dirimu sendiri, padahal kamu membaca Kitab (Taurat)? Tidakkah kamu mengerti?

2:45

# وَاسْتَعِيْنُوْا بِالصَّبْرِ وَالصَّلٰوةِ ۗ وَاِنَّهَا لَكَبِيْرَةٌ اِلَّا عَلَى الْخٰشِعِيْنَۙ

wa**i**sta'iinuu bi**al***shsh*abri wa**al***shsh*al*aa*ti wa-innah*aa *lakabiiratun ill*aa *'al*aa* **a**lkh*aa*syi'iin**a**

Dan mohonlah pertolongan (kepada Allah) dengan sabar dan salat. Dan (salat) itu sungguh berat, kecuali bagi orang-orang yang khusyuk,

2:46

# الَّذِيْنَ يَظُنُّوْنَ اَنَّهُمْ مُّلٰقُوْا رَبِّهِمْ وَاَنَّهُمْ اِلَيْهِ رٰجِعُوْنَ ࣖ

**al**la*dz*iina ya*zh*unnuuna annahum mul*aa*quu rabbihim wa-annahum ilayhi r*aa*ji'uun**a**

(yaitu) mereka yang yakin, bahwa mereka akan menemui Tuhannya, dan bahwa mereka akan kembali kepada-Nya.

2:47

# يٰبَنِيْٓ اِسْرَاۤءِيْلَ اذْكُرُوْا نِعْمَتِيَ الَّتِيْٓ اَنْعَمْتُ عَلَيْكُمْ وَاَنِّيْ فَضَّلْتُكُمْ عَلَى الْعٰلَمِيْنَ

y*aa* banii isr*aa*-iila u*dz*kuruu ni'matiya **al**latii an'amtu 'alaykum wa-annii fa*dhdh*altukum 'al*aa* **a**l'*aa*lamiin**a**

Wahai Bani Israil! Ingatlah nikmat-Ku yang telah Aku berikan kepadamu, dan Aku telah melebihkan kamu dari semua umat yang lain di alam ini (pada masa itu).

2:48

# وَاتَّقُوْا يَوْمًا لَّا تَجْزِيْ نَفْسٌ عَنْ نَّفْسٍ شَيْـًٔا وَّلَا يُقْبَلُ مِنْهَا شَفَاعَةٌ وَّلَا يُؤْخَذُ مِنْهَا عَدْلٌ وَّلَا هُمْ يُنْصَرُوْنَ

wa**i**ttaquu yawman l*aa* tajzii nafsun 'an nafsin syay-an wal*aa* yuqbalu minh*aa* syaf*aa*'atun wal*aa* yu/kha*dz*u minh*aa* 'adlun wal*aa* hum yun*sh*aruun**a**

Dan takutlah kamu pada hari, (ketika) tidak seorang pun dapat membela orang lain sedikit pun. Sedangkan syafaat dan tebusan apa pun darinya tidak diterima dan mereka tidak akan ditolong.

2:49

# وَاِذْ نَجَّيْنٰكُمْ مِّنْ اٰلِ فِرْعَوْنَ يَسُوْمُوْنَكُمْ سُوْۤءَ الْعَذَابِ يُذَبِّحُوْنَ اَبْنَاۤءَكُمْ وَيَسْتَحْيُوْنَ نِسَاۤءَكُمْ ۗ وَفِيْ ذٰلِكُمْ بَلَاۤءٌ مِّنْ رَّبِّكُمْ عَظِيْمٌ

wa-i*dz* najjayn*aa*kum min *aa*li fir'awna yasuumuunakum suu-a **a**l'a*dzaa*bi yu*dz*abbi*h*uuna abn*aa*-akum wayasta*h*yuuna nis*aa*-akum wafii *dzaa*likum bal*aa*un min rabbikum 'a

Dan (ingatlah) ketika Kami menyelamatkan kamu dari (Fir‘aun dan) pengikut-pengikut Fir‘aun. Mereka menimpakan siksaan yang sangat berat kepadamu. Mereka menyembelih anak-anak laki-lakimu dan membiarkan hidup anak-anak perempuanmu. Dan pada yang demikian i

2:50

# وَاِذْ فَرَقْنَا بِكُمُ الْبَحْرَ فَاَنْجَيْنٰكُمْ وَاَغْرَقْنَآ اٰلَ فِرْعَوْنَ وَاَنْتُمْ تَنْظُرُوْنَ

wa-i*dz* faraqn*aa* bikumu **a**lba*h*ra fa-anjayn*aa*kum wa-aghraqn*aa* *aa*la fir'awna wa-antum tan*zh*uruun**a**

Dan (ingatlah) ketika Kami membelah laut untukmu, sehingga kamu dapat Kami selamatkan dan Kami tenggelamkan (Fir‘aun dan) pengikut-pengikut Fir‘aun, sedang kamu menyaksikan.

2:51

# وَاِذْ وٰعَدْنَا مُوْسٰىٓ اَرْبَعِيْنَ لَيْلَةً ثُمَّ اتَّخَذْتُمُ الْعِجْلَ مِنْۢ بَعْدِهٖ وَاَنْتُمْ ظٰلِمُوْنَ

wa-i*dz* w*aa*'adn*aa* muus*aa* arba'iina laylatan tsumma ittakha*dz*tumu **a**l'ijla min ba'dihi wa-antum *zhaa*limuun**a**

Dan (ingatlah) ketika Kami menjanjikan kepada Musa empat puluh malam. Kemudian kamu (Bani Israil) menjadikan (patung) anak sapi (sebagai sesembahan) setelah (kepergian)nya, dan kamu (menjadi) orang yang zalim.

2:52

# ثُمَّ عَفَوْنَا عَنْكُمْ مِّنْۢ بَعْدِ ذٰلِكَ لَعَلَّكُمْ تَشْكُرُوْنَ

tsumma 'afawn*aa* 'ankum min ba'di *dzaa*lika la'allakum tasykuruun**a**

Kemudian Kami memaafkan kamu setelah itu, agar kamu bersyukur.

2:53

# وَاِذْ اٰتَيْنَا مُوْسَى الْكِتٰبَ وَالْفُرْقَانَ لَعَلَّكُمْ تَهْتَدُوْنَ

wa-i*dz* *aa*tayn*aa* muus*aa* **a**lkit*aa*ba wa**a**lfurq*aa*na la'allakum tahtaduun**a**

Dan (ingatlah), ketika Kami memberikan kepada Musa Kitab dan Furqan, agar kamu memperoleh petunjuk.

2:54

# وَاِذْ قَالَ مُوْسٰى لِقَوْمِهٖ يٰقَوْمِ اِنَّكُمْ ظَلَمْتُمْ اَنْفُسَكُمْ بِاتِّخَاذِكُمُ الْعِجْلَ فَتُوْبُوْٓا اِلٰى بَارِىِٕكُمْ فَاقْتُلُوْٓا اَنْفُسَكُمْۗ ذٰلِكُمْ خَيْرٌ لَّكُمْ عِنْدَ بَارِىِٕكُمْۗ فَتَابَ عَلَيْكُمْ ۗ اِنَّهٗ هُوَ التَّوَّابُ الر

wa-i*dz* q*aa*la muus*aa* liqawmihi y*aa* qawmi innakum *zh*alamtum anfusakum bi**i**ttikh*aadz*ikumu **a**l'ijla fatuubuu il*aa* b*aa*ri-ikum fa**u**qtuluu anfusakum *dzaa*

*Dan (ingatlah) ketika Musa berkata kepada kaumnya, “Wahai kaumku! Kamu benar-benar telah menzalimi dirimu sendiri dengan menjadikan (patung) anak sapi (sebagai sesembahan), karena itu bertobatlah kepada Penciptamu dan bunuhlah dirimu. Itu lebih baik bagim*

2:55

# وَاِذْ قُلْتُمْ يٰمُوْسٰى لَنْ نُّؤْمِنَ لَكَ حَتّٰى نَرَى اللّٰهَ جَهْرَةً فَاَخَذَتْكُمُ الصّٰعِقَةُ وَاَنْتُمْ تَنْظُرُوْنَ

wa-i*dz* qultum y*aa* muus*aa* lan nu/mina laka *h*att*aa* nar*aa* **al**l*aa*ha jahratan fa-akha*dz*atkumu **al***shshaa*'iqatu wa-antum tan*zh*uruun**a**

Dan (ingatlah) ketika kamu berkata, “Wahai Musa! Kami tidak akan beriman kepadamu sebelum kami melihat Allah dengan jelas,” maka halilintar menyambarmu, sedang kamu menyaksikan.

2:56

# ثُمَّ بَعَثْنٰكُمْ مِّنْۢ بَعْدِ مَوْتِكُمْ لَعَلَّكُمْ تَشْكُرُوْنَ

tsumma ba'atsn*aa*kum min ba'di mawtikum la'allakum tasykuruun**a**

Kemudian, Kami membangkitkan kamu setelah kamu mati, agar kamu bersyukur.

2:57

# وَظَلَّلْنَا عَلَيْكُمُ الْغَمَامَ وَاَنْزَلْنَا عَلَيْكُمُ الْمَنَّ وَالسَّلْوٰى ۗ كُلُوْا مِنْ طَيِّبٰتِ مَا رَزَقْنٰكُمْ ۗ وَمَا ظَلَمُوْنَا وَلٰكِنْ كَانُوْٓا اَنْفُسَهُمْ يَظْلِمُوْنَ

wa*zh*allaln*aa* 'alaykumu **a**lgham*aa*ma wa-anzaln*aa* 'alaykumu **a**lmanna wa**al**ssalw*aa* kuluu min *th*ayyib*aa*ti m*aa* razaqn*aa*kum wam*aa* *zh*alamu

Dan Kami menaungi kamu dengan awan, dan Kami menurunkan kepadamu mann dan salwa. Makanlah (makanan) yang baik-baik dari rezeki yang telah Kami berikan kepadamu. Mereka tidak menzalimi Kami, tetapi justru merekalah yang menzalimi diri sendiri.

2:58

# وَاِذْ قُلْنَا ادْخُلُوْا هٰذِهِ الْقَرْيَةَ فَكُلُوْا مِنْهَا حَيْثُ شِئْتُمْ رَغَدًا وَّادْخُلُوا الْبَابَ سُجَّدًا وَّقُوْلُوْا حِطَّةٌ نَّغْفِرْ لَكُمْ خَطٰيٰكُمْ ۗ وَسَنَزِيْدُ الْمُحْسِنِيْنَ

wa-i*dz* quln*aa* udkhuluu h*aadz*ihi **a**lqaryata fakuluu minh*aa* *h*aytsu syi/tum raghadan wa**u**dkhuluu **a**lb*aa*ba sujjadan waquuluu *h*i*ththh*atun naghfir lakum kha<

Dan (ingatlah) ketika Kami berfirman, “Masuklah ke negeri ini (Baitulmaqdis), maka makanlah dengan nikmat (berbagai makanan) yang ada di sana sesukamu. Dan masukilah pintu gerbangnya sambil membungkuk, dan katakanlah, “Bebaskanlah kami (dari dosa-dosa kam

2:59

# فَبَدَّلَ الَّذِيْنَ ظَلَمُوْا قَوْلًا غَيْرَ الَّذِيْ قِيْلَ لَهُمْ فَاَنْزَلْنَا عَلَى الَّذِيْنَ ظَلَمُوْا رِجْزًا مِّنَ السَّمَاۤءِ بِمَا كَانُوْا يَفْسُقُوْنَ ࣖ

fabaddala **al**la*dz*iina *zh*alamuu qawlan ghayra **al**la*dz*ii qiila lahum fa-anzaln*aa* 'al*aa* **al**la*dz*iina *zh*alamuu rijzan mina **al**ssam*aa*-i bim

Lalu orang-orang yang zalim mengganti perintah dengan (perintah lain) yang tidak diperintahkan kepada mereka. Maka Kami turunkan malapetaka dari langit kepada orang-orang yang zalim itu, karena mereka (selalu) berbuat fasik.

2:60

# ۞ وَاِذِ اسْتَسْقٰى مُوْسٰى لِقَوْمِهٖ فَقُلْنَا اضْرِبْ بِّعَصَاكَ الْحَجَرَۗ فَانْفَجَرَتْ مِنْهُ اثْنَتَا عَشْرَةَ عَيْنًا ۗ قَدْ عَلِمَ كُلُّ اُنَاسٍ مَّشْرَبَهُمْ ۗ كُلُوْا وَاشْرَبُوْا مِنْ رِّزْقِ اللّٰهِ وَلَا تَعْثَوْا فِى الْاَرْضِ مُفْسِدِيْنَ

wa-i*dz*i istasq*aa* muus*aa* liqawmihi faquln*aa* i*dh*rib bi'a*shaa*ka **a**l*h*ajara fa**i**nfajarat minhu itsnat*aa* 'asyrata 'aynan qad 'alima kullu un*aa*sin masyrabahum kuluu wa

Dan (ingatlah) ketika Musa memohon air untuk kaumnya, lalu Kami berfirman, “Pukullah batu itu dengan tongkatmu!” Maka memancarlah daripadanya dua belas mata air. Setiap suku telah mengetahui tempat minumnya (masing-masing). Makan dan minumlah dari rezeki

2:61

# وَاِذْ قُلْتُمْ يٰمُوْسٰى لَنْ نَّصْبِرَ عَلٰى طَعَامٍ وَّاحِدٍ فَادْعُ لَنَا رَبَّكَ يُخْرِجْ لَنَا مِمَّا تُنْۢبِتُ الْاَرْضُ مِنْۢ بَقْلِهَا وَقِثَّاۤىِٕهَا وَفُوْمِهَا وَعَدَسِهَا وَبَصَلِهَا ۗ قَالَ اَتَسْتَبْدِلُوْنَ الَّذِيْ هُوَ اَدْنٰى بِالَّذِيْ

wa-i*dz* qultum y*aa* muus*aa* lan na*sh*bira 'al*aa* *th*a'*aa*min w*aah*idin fa**u**d'u lan*aa* rabbaka yukhrij lan*aa* mimm*aa* tunbitu **a**l-ar*dh*u min baqlih*aa*

Dan (ingatlah), ketika kamu berkata, “Wahai Musa! Kami tidak tahan hanya (makan) dengan satu macam makanan saja, maka mohonkanlah kepada Tuhanmu untuk kami, agar Dia memberi kami apa yang ditumbuhkan bumi, seperti: sayur-mayur, mentimun, bawang putih, kac

2:62

# اِنَّ الَّذِيْنَ اٰمَنُوْا وَالَّذِيْنَ هَادُوْا وَالنَّصٰرٰى وَالصَّابِــِٕيْنَ مَنْ اٰمَنَ بِاللّٰهِ وَالْيَوْمِ الْاٰخِرِ وَعَمِلَ صَالِحًا فَلَهُمْ اَجْرُهُمْ عِنْدَ رَبِّهِمْۚ وَلَا خَوْفٌ عَلَيْهِمْ وَلَا هُمْ يَحْزَنُوْنَ

inna **al**la*dz*iina *aa*manuu wa**a**lla*dz*iina h*aa*duu wa**al**nna*shaa*r*aa* wa**al***shshaa*bi-iina man* aa*mana biall*aa*hi wa**a**lyawmi

Sesungguhnya orang-orang yang beriman, orang-orang Yahudi, orang-orang Nasrani dan orang-orang sabi'in, siapa saja (di antara mereka) yang beriman kepada Allah dan hari akhir, dan melakukan kebajikan, mereka mendapat pahala dari Tuhannya, tidak ada rasa t

2:63

# وَاِذْ اَخَذْنَا مِيْثَاقَكُمْ وَرَفَعْنَا فَوْقَكُمُ الطُّوْرَۗ خُذُوْا مَآ اٰتَيْنٰكُمْ بِقُوَّةٍ وَّاذْكُرُوْا مَا فِيْهِ لَعَلَّكُمْ تَتَّقُوْنَ

wa-i*dz* akha*dz*n*aa* miits*aa*qakum warafa'n*aa* fawqakumu **al***ththh*uura khu*dz*uu m*aa* *aa*tayn*aa*kum biquwwatin wa**u***dz*kuruu m*aa* fiihi la'allakum tattaqu

Dan (ingatlah) ketika Kami mengambil janji kamu dan Kami angkat gunung (Sinai) di atasmu (seraya berfirman), “Pegang teguhlah apa yang telah Kami berikan kepadamu dan ingatlah apa yang ada di dalamnya, agar kamu bertakwa.”

2:64

# ثُمَّ تَوَلَّيْتُمْ مِّنْۢ بَعْدِ ذٰلِكَ فَلَوْلَا فَضْلُ اللّٰهِ عَلَيْكُمْ وَرَحْمَتُهٗ لَكُنْتُمْ مِّنَ الْخٰسِرِيْنَ

tsumma tawallaytum min ba'di *dzaa*lika falawl*aa* fa*dh*lu **al**l*aa*hi 'alaykum wara*h*matuhu lakuntum mina **a**lkh*aa*siriin**a**

Kemudian setelah itu kamu berpaling. Maka sekiranya bukan karena karunia Allah dan rahmat-Nya kepadamu, pasti kamu termasuk orang yang rugi.

2:65

# وَلَقَدْ عَلِمْتُمُ الَّذِيْنَ اعْتَدَوْا مِنْكُمْ فِى السَّبْتِ فَقُلْنَا لَهُمْ كُوْنُوْا قِرَدَةً خَاسِـِٕيْنَ

walaqad 'alimtumu **al**la*dz*iina i'tadaw minkum fii **al**ssabti faquln*aa* lahum kuunuu qiradatan kh*aa*si-iin**a**

Dan sungguh, kamu telah mengetahui orang-orang yang melakukan pelanggaran di antara kamu pada hari Sabat, lalu Kami katakan kepada mereka, “Jadilah kamu kera yang hina!”

2:66

# فَجَعَلْنٰهَا نَكَالًا لِّمَا بَيْنَ يَدَيْهَا وَمَا خَلْفَهَا وَمَوْعِظَةً لِّلْمُتَّقِيْنَ

faja'aln*aa*h*aa* nak*aa*lan lim*aa* bayna yadayh*aa* wam*aa* khalfah*aa* wamaw'i*zh*atan lilmuttaqiin**a**

Maka Kami jadikan (yang demikian) itu peringatan bagi orang-orang pada masa itu dan bagi mereka yang datang kemudian, serta menjadi pelajaran bagi orang-orang yang bertakwa.

2:67

# وَاِذْ قَالَ مُوْسٰى لِقَوْمِهٖٓ اِنَّ اللّٰهَ يَأْمُرُكُمْ اَنْ تَذْبَحُوْا بَقَرَةً ۗ قَالُوْٓا اَتَتَّخِذُنَا هُزُوًا ۗ قَالَ اَعُوْذُ بِاللّٰهِ اَنْ اَكُوْنَ مِنَ الْجٰهِلِيْنَ

wa-i*dz* q*aa*la muus*aa* liqawmihi inna **al**l*aa*ha ya/murukum an ta*dz*ba*h*uu baqaratan q*aa*luu atattakhi*dz*un*aa* huzuwan q*aa*la a'uu*dz*u biall*aa*hi an akuuna mina

Dan (ingatlah) ketika Musa berkata kepada kaumnya, “Allah memerintahkan kamu agar menyembelih seekor sapi betina.” Mereka bertanya, “Apakah engkau akan menjadikan kami sebagai ejekan?” Dia (Musa) menjawab, “Aku berlindung kepada Allah agar tidak termasuk

2:68

# قَالُوا ادْعُ لَنَا رَبَّكَ يُبَيِّنْ لَّنَا مَا هِيَ ۗ قَالَ اِنَّهٗ يَقُوْلُ اِنَّهَا بَقَرَةٌ لَّا فَارِضٌ وَّلَا بِكْرٌۗ عَوَانٌۢ بَيْنَ ذٰلِكَ ۗ فَافْعَلُوْا مَا تُؤْمَرُوْنَ

q*aa*luu ud'u lan*aa* rabbaka yubayyin lan*aa* m*aa* hiya q*aa*la innahu yaquulu innah*aa* baqaratun l*aa* f*aa*ri*dh*un wal*aa* bikrun 'aw*aa*nun bayna *dzaa*lika fa**i**f'aluu m

Mereka berkata, “Mohonkanlah kepada Tuhanmu untuk kami agar Dia menjelaskan kepada kami tentang (sapi betina) itu.” Dia (Musa) menjawab, “Dia (Allah) berfirman, bahwa sapi betina itu tidak tua dan tidak muda, (tetapi) pertengahan antara itu. Maka kerjakan

2:69

# قَالُوا ادْعُ لَنَا رَبَّكَ يُبَيِّنْ لَّنَا مَا لَوْنُهَا ۗ قَالَ اِنَّهٗ يَقُوْلُ اِنَّهَا بَقَرَةٌ صَفْرَاۤءُ فَاقِعٌ لَّوْنُهَا تَسُرُّ النّٰظِرِيْنَ

q*aa*luu ud'u lan*aa* rabbaka yubayyin lan*aa* m*aa* lawnuh*aa* q*aa*la innahu yaquulu innah*aa* baqaratun *sh*afr*aa*u f*aa*qi'un lawnuh*aa* tasurru **al**nn*aazh*iriin**a**

**Mereka berkata, “Mohonkanlah kepada Tuhanmu untuk kami agar Dia menjelaskan kepada kami apa warnanya.” Dia (Musa) menjawab, “Dia (Allah) berfirman, bahwa (sapi) itu adalah sapi betina yang kuning tua warnanya, yang menyenangkan orang-orang yang memandang(**

2:70

# قَالُوا ادْعُ لَنَا رَبَّكَ يُبَيِّنْ لَّنَا مَا هِيَۙ اِنَّ الْبَقَرَ تَشٰبَهَ عَلَيْنَاۗ وَاِنَّآ اِنْ شَاۤءَ اللّٰهُ لَمُهْتَدُوْنَ

q*aa*luu ud'u lan*aa* rabbaka yubayyin lan*aa* m*aa* hiya inna **a**lbaqara tasy*aa*baha 'alayn*aa* wa-inn*aa* in sy*aa*-a **al**l*aa*hu lamuhtaduun**a**

Mereka berkata, “Mohonkanlah kepada Tuhanmu untuk kami agar Dia menjelaskan kepada kami tentang (sapi betina) itu. (Karena) sesungguhnya sapi itu belum jelas bagi kami, dan jika Allah menghendaki, niscaya kami mendapat petunjuk.”

2:71

# قَالَ اِنَّهٗ يَقُوْلُ اِنَّهَا بَقَرَةٌ لَّا ذَلُوْلٌ تُثِيْرُ الْاَرْضَ وَلَا تَسْقِى الْحَرْثَۚ مُسَلَّمَةٌ لَّاشِيَةَ فِيْهَا ۗ قَالُوا الْـٰٔنَ جِئْتَ بِالْحَقِّ فَذَبَحُوْهَا وَمَا كَادُوْا يَفْعَلُوْنَ ࣖ

q*aa*la innahu yaquulu innah*aa* baqaratun l*aa* *dz*aluulun tutsiiru **a**l-ar*dh*a wal*aa* tasqii **a**l*h*artsa musallamatun l*aa* syiyata fiih*aa* q*aa*luu **a**l

Dia (Musa) menjawab, “Dia (Allah) berfirman, (sapi) itu adalah sapi betina yang belum pernah dipakai untuk membajak tanah dan tidak (pula) untuk mengairi tanaman, sehat, dan tanpa belang.” Mereka berkata, “Sekarang barulah engkau menerangkan (hal) yang se

2:72

# وَاِذْ قَتَلْتُمْ نَفْسًا فَادّٰرَءْتُمْ فِيْهَا ۗ وَاللّٰهُ مُخْرِجٌ مَّا كُنْتُمْ تَكْتُمُوْنَ ۚ

wa-i*dz* qataltum nafsan fa**i**dd*aa*ra/tum fiih*aa* wa**al**l*aa*hu mukhrijun m*aa* kuntum taktumuun**a**

Dan (ingatlah) ketika kamu membunuh seseorang, lalu kamu tuduh-menuduh tentang itu. Tetapi Allah menyingkapkan apa yang kamu sembunyikan.

2:73

# فَقُلْنَا اضْرِبُوْهُ بِبَعْضِهَاۗ كَذٰلِكَ يُحْيِ اللّٰهُ الْمَوْتٰى وَيُرِيْكُمْ اٰيٰتِهٖ لَعَلَّكُمْ تَعْقِلُوْنَ

faquln*aa* i*dh*ribuuhu biba'*dh*ih*aa* ka*dzaa*lika yu*h*yii **al**l*aa*hu **a**lmawt*aa* wayuriikum *aa*y*aa*tihi la'allakum ta'qiluun**a**

Lalu Kami berfirman, “Pukullah (mayat) itu dengan bagian dari (sapi) itu!” Demikianlah Allah menghidupkan (orang) yang telah mati, dan Dia memperlihatkan kepadamu tanda-tanda (kekuasaan-Nya) agar kamu mengerti.

2:74

# ثُمَّ قَسَتْ قُلُوْبُكُمْ مِّنْۢ بَعْدِ ذٰلِكَ فَهِيَ كَالْحِجَارَةِ اَوْ اَشَدُّ قَسْوَةً ۗ وَاِنَّ مِنَ الْحِجَارَةِ لَمَا يَتَفَجَّرُ مِنْهُ الْاَنْهٰرُ ۗ وَاِنَّ مِنْهَا لَمَا يَشَّقَّقُ فَيَخْرُجُ مِنْهُ الْمَاۤءُ ۗوَاِنَّ مِنْهَا لَمَا يَهْبِطُ مِن

tsumma qasat quluubukum min ba'di *dzaa*lika fahiya ka**a**l*h*ij*aa*rati aw asyaddu qaswatan wa-inna mina **a**l*h*ij*aa*rati lam*aa* yatafajjaru minhu **a**l-anh*aa*ru wa-inna min

Kemudian setelah itu hatimu menjadi keras, sehingga (hatimu) seperti batu, bahkan lebih keras. Padahal dari batu-batu itu pasti ada sungai-sungai yang (airnya) memancar daripadanya. Ada pula yang terbelah lalu keluarlah mata air daripadanya. Dan ada pula

2:75

# ۞ اَفَتَطْمَعُوْنَ اَنْ يُّؤْمِنُوْا لَكُمْ وَقَدْ كَانَ فَرِيْقٌ مِّنْهُمْ يَسْمَعُوْنَ كَلَامَ اللّٰهِ ثُمَّ يُحَرِّفُوْنَهٗ مِنْۢ بَعْدِ مَا عَقَلُوْهُ وَهُمْ يَعْلَمُوْنَ

afata*th*ma'uuna an yu/minuu lakum waqad k*aa*na fariiqun minhum yasma'uuna kal*aa*ma **al**l*aa*hi tsumma yu*h*arrifuunahu min ba'di m*aa* 'aqaluuhu wahum ya'lamuun**a**

Maka apakah kamu (Muslimin) sangat mengharapkan mereka akan percaya kepadamu, sedangkan segolongan dari mereka mendengar firman Allah, lalu mereka mengubahnya setelah memahaminya, padahal mereka mengetahuinya?

2:76

# وَاِذَا لَقُوا الَّذِيْنَ اٰمَنُوْا قَالُوْٓا اٰمَنَّاۚ وَاِذَا خَلَا بَعْضُهُمْ اِلٰى بَعْضٍ قَالُوْٓا اَتُحَدِّثُوْنَهُمْ بِمَا فَتَحَ اللّٰهُ عَلَيْكُمْ لِيُحَاۤجُّوْكُمْ بِهٖ عِنْدَ رَبِّكُمْ ۗ اَفَلَا تَعْقِلُوْنَ

wa-i*dzaa* laquu **al**la*dz*iina *aa*manuu q*aa*luu *aa*mann*aa* wa-i*dzaa* khal*aa* ba'*dh*uhum il*aa* ba'*dh*in q*aa*luu atu*h*additsuunahum bim*aa* fata*h*a

Dan apabila mereka berjumpa dengan orang-orang yang beriman, mereka berkata, “Kami telah beriman.” Tetapi apabila kembali kepada sesamanya, mereka bertanya, “Apakah akan kamu ceritakan kepada mereka apa yang telah diterangkan Allah kepadamu, sehingga mere

2:77

# اَوَلَا يَعْلَمُوْنَ اَنَّ اللّٰهَ يَعْلَمُ مَا يُسِرُّوْنَ وَمَا يُعْلِنُوْنَ

awa l*aa* ya'lamuuna anna **al**l*aa*ha ya'lamu m*aa* yusirruuna wam*aa* yu'linuun**a**

Dan tidakkah mereka tahu bahwa Allah mengetahui apa yang mereka sembunyikan dan apa yang mereka nyatakan?

2:78

# وَمِنْهُمْ اُمِّيُّوْنَ لَا يَعْلَمُوْنَ الْكِتٰبَ اِلَّآ اَمَانِيَّ وَاِنْ هُمْ اِلَّا يَظُنُّوْنَ

waminhum ummiyyuuna l*aa* ya'lamuuna **a**lkit*aa*ba ill*aa* am*aa*niyya wa-in hum ill*aa* ya*zh*unnuun**a**

Dan di antara mereka ada yang buta huruf, tidak memahami Kitab (Taurat), kecuali hanya berangan-angan dan mereka hanya menduga-duga.

2:79

# فَوَيْلٌ لِّلَّذِيْنَ يَكْتُبُوْنَ الْكِتٰبَ بِاَيْدِيْهِمْ ثُمَّ يَقُوْلُوْنَ هٰذَا مِنْ عِنْدِ اللّٰهِ لِيَشْتَرُوْا بِهٖ ثَمَنًا قَلِيْلًا ۗفَوَيْلٌ لَّهُمْ مِّمَّا كَتَبَتْ اَيْدِيْهِمْ وَوَيْلٌ لَّهُمْ مِّمَّا يَكْسِبُوْنَ

fawaylun lilla*dz*iina yaktubuuna **a**lkit*aa*ba bi-aydiihim tsumma yaquuluuna h*aadzaa* min 'indi **al**l*aa*hi liyasytaruu bihi tsamanan qaliilan fawaylun lahum mimm*aa* katabat aydiihim wawaylun lahum

Maka celakalah orang-orang yang menulis kitab dengan tangan mereka (sendiri), kemudian berkata, “Ini dari Allah,” (dengan maksud) untuk menjualnya dengan harga murah. Maka celakalah mereka, karena tulisan tangan mereka, dan celakalah mereka karena apa yan

2:80

# وَقَالُوْا لَنْ تَمَسَّنَا النَّارُ اِلَّآ اَيَّامًا مَّعْدُوْدَةً ۗ قُلْ اَتَّخَذْتُمْ عِنْدَ اللّٰهِ عَهْدًا فَلَنْ يُّخْلِفَ اللّٰهُ عَهْدَهٗٓ اَمْ تَقُوْلُوْنَ عَلَى اللّٰهِ مَا لَا تَعْلَمُوْنَ

waq*aa*luu lan tamassan*aa* **al**nn*aa*ru ill*aa* ayy*aa*man ma'duudatan qul attakha*dz*tum 'inda **al**l*aa*hi 'ahdan falan yukhlifa **al**l*aa*hu 'ahdahu am taquuluuna 'al

Dan mereka berkata, “Neraka tidak akan menyentuh kami, kecuali beberapa hari saja.” Katakanlah, “Sudahkah kamu menerima janji dari Allah, sehingga Allah tidak akan mengingkari janji-Nya, ataukah kamu mengatakan tentang Allah, sesuatu yang tidak kamu ketah

2:81

# بَلٰى مَنْ كَسَبَ سَيِّئَةً وَّاَحَاطَتْ بِهٖ خَطِيْۤـَٔتُهٗ فَاُولٰۤىِٕكَ اَصْحٰبُ النَّارِ ۚ هُمْ فِيْهَا خٰلِدُوْنَ

bal*aa* man kasaba sayyi-atan wa-a*hath*at bihi kha*th*ii-atuhu faul*aa*-ika a*sh*-*haa*bu **al**nn*aa*ri hum fiih*aa* kh*aa*liduun**a**

Bukan demikian! Barangsiapa berbuat keburukan, dan dosanya telah menenggelamkannya, maka mereka itu penghuni neraka. Mereka kekal di dalamnya.

2:82

# وَالَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِ اُولٰۤىِٕكَ اَصْحٰبُ الْجَنَّةِ ۚ هُمْ فِيْهَا خٰلِدُوْنَ ࣖ

wa**a**lla*dz*iina *aa*manuu wa'amiluu **al***shshaa*li*haa*ti ul*aa*\-ika a*sh*\-*haa*bu **a**ljannati hum fiih*aa *kh*aa*liduun**a**

Dan orang-orang yang beriman dan mengerjakan kebajikan, mereka itu penghuni surga. Mereka kekal di dalamnya.

2:83

# وَاِذْ اَخَذْنَا مِيْثَاقَ بَنِيْٓ اِسْرَاۤءِيْلَ لَا تَعْبُدُوْنَ اِلَّا اللّٰهَ وَبِالْوَالِدَيْنِ اِحْسَانًا وَّذِى الْقُرْبٰى وَالْيَتٰمٰى وَالْمَسٰكِيْنِ وَقُوْلُوْا لِلنَّاسِ حُسْنًا وَّاَقِيْمُوا الصَّلٰوةَ وَاٰتُوا الزَّكٰوةَۗ ثُمَّ تَوَلَّيْتُمْ

wa-i*dz* akha*dz*n*aa* miits*aa*qa banii isr*aa*-iila l*aa* ta'buduuna ill*aa* **al**l*aa*ha wabi**a**lw*aa*lidayni i*h*s*aa*nan wa*dz*ii **a**lqurb*aa*

*Dan (ingatlah) ketika Kami mengambil janji dari Bani Israil, “Janganlah kamu menyembah selain Allah, dan berbuat-baiklah kepada kedua orang tua, kerabat, anak-anak yatim, dan orang-orang miskin. Dan bertuturkatalah yang baik kepada manusia, laksanakanlah*

2:84

# وَاِذْ اَخَذْنَا مِيْثَاقَكُمْ لَا تَسْفِكُوْنَ دِمَاۤءَكُمْ وَلَا تُخْرِجُوْنَ اَنْفُسَكُمْ مِّنْ دِيَارِكُمْ ۖ ثُمَّ اَقْرَرْتُمْ وَاَنْتُمْ تَشْهَدُوْنَ

wa-i*dz* akha*dz*n*aa* miits*aa*qakum l*aa* tasfikuuna dim*aa*-akum wal*aa* tukhrijuuna anfusakum min diy*aa*rikum tsumma aqrartum wa-antum tasyhaduun**a**

Dan (ingatlah) ketika Kami mengambil janji kamu, “Janganlah kamu menumpahkan darahmu (membunuh orang), dan mengusir dirimu (saudara sebangsamu) dari kampung halamanmu.” Kemudian kamu berikrar dan bersaksi.

2:85

# ثُمَّ اَنْتُمْ هٰٓؤُلَاۤءِ تَقْتُلُوْنَ اَنْفُسَكُمْ وَتُخْرِجُوْنَ فَرِيْقًا مِّنْكُمْ مِّنْ دِيَارِهِمْۖ تَظٰهَرُوْنَ عَلَيْهِمْ بِالْاِثْمِ وَالْعُدْوَانِۗ وَاِنْ يَّأْتُوْكُمْ اُسٰرٰى تُفٰدُوْهُمْ وَهُوَ مُحَرَّمٌ عَلَيْكُمْ اِخْرَاجُهُمْ ۗ اَفَتُؤْمِ

tsumma antum h*aa*ul*aa*-i taqtuluuna anfusakum watukhrijuuna fariiqan minkum min diy*aa*rihim ta*zhaa*haruuna 'alayhim bi**a**l-itsmi wa**a**l'udw*aa*ni wa-in ya/tuukum us*aa*r*aa* tuf*aa*

Kemudian kamu (Bani Israil) membunuh dirimu (sesamamu), dan mengusir segolongan dari kamu dari kampung halamannya. Kamu saling membantu (menghadapi) mereka dalam kejahatan dan permusuhan. Dan jika mereka datang kepadamu sebagai tawanan, kamu tebus mereka,

2:86

# اُولٰۤىِٕكَ الَّذِيْنَ اشْتَرَوُا الْحَيٰوةَ الدُّنْيَا بِالْاٰخِرَةِ ۖ فَلَا يُخَفَّفُ عَنْهُمُ الْعَذَابُ وَلَا هُمْ يُنْصَرُوْنَ ࣖ

ul*aa*-ika **al**la*dz*iina isytarawuu **a**l*h*ay*aa*ta **al**dduny*aa* bi**a**l-*aa*khirati fal*aa* yukhaffafu 'anhumu **a**l'a*dzaa*bu wal*aa*

Mereka itulah orang-orang yang membeli kehidupan dunia dengan (kehidupan) akhirat. Maka tidak akan diringankan azabnya dan mereka tidak akan ditolong.

2:87

# وَلَقَدْ اٰتَيْنَا مُوْسَى الْكِتٰبَ وَقَفَّيْنَا مِنْۢ بَعْدِهٖ بِالرُّسُلِ ۖ وَاٰتَيْنَا عِيْسَى ابْنَ مَرْيَمَ الْبَيِّنٰتِ وَاَيَّدْنٰهُ بِرُوْحِ الْقُدُسِۗ اَفَكُلَّمَا جَاۤءَكُمْ رَسُوْلٌۢ بِمَا لَا تَهْوٰىٓ اَنْفُسُكُمُ اسْتَكْبَرْتُمْ ۚ فَفَرِيْق

walaqad *aa*tayn*aa* muus*aa* **a**lkit*aa*ba waqaffayn*aa* min ba'dihi bi**al**rrusuli wa*aa*tayn*aa* 'iis*aa* ibna maryama **a**lbayyin*aa*ti wa-ayyadn*aa*hu biruu<

Dan sungguh, Kami telah memberikan Kitab (Taurat) kepada Musa, dan Kami susulkan setelahnya dengan rasul-rasul, dan Kami telah berikan kepada Isa putra Maryam bukti-bukti kebenaran serta Kami perkuat dia dengan Rohulkudus (Jibril). Mengapa setiap rasul ya

2:88

# وَقَالُوْا قُلُوْبُنَا غُلْفٌ ۗ بَلْ لَّعَنَهُمُ اللّٰهُ بِكُفْرِهِمْ فَقَلِيْلًا مَّا يُؤْمِنُوْنَ

waq*aa*luu quluubun*aa* ghulfun bal la'anahumu **al**l*aa*hu bikufrihim faqaliilan m*aa* yu/minuun**a**

Dan mereka berkata, “Hati kami tertutup.” Tidak! Allah telah melaknat mereka itu karena keingkaran mereka, tetapi sedikit sekali mereka yang beriman.

2:89

# وَلَمَّا جَاۤءَهُمْ كِتٰبٌ مِّنْ عِنْدِ اللّٰهِ مُصَدِّقٌ لِّمَا مَعَهُمْۙ وَكَانُوْا مِنْ قَبْلُ يَسْتَفْتِحُوْنَ عَلَى الَّذِيْنَ كَفَرُوْاۚ فَلَمَّا جَاۤءَهُمْ مَّا عَرَفُوْا كَفَرُوْا بِهٖ ۖ فَلَعْنَةُ اللّٰهِ عَلَى الْكٰفِرِيْنَ

walamm*aa* j*aa*-ahum kit*aa*bun min 'indi **al**l*aa*hi mu*sh*addiqun lim*aa* ma'ahum wak*aa*nuu min qablu yastafti*h*uuna 'al*aa* **al**la*dz*iina kafaruu falamm*aa* j*aa*

Dan setelah sampai kepada mereka Kitab (Al-Qur'an) dari Allah yang membenarkan apa yang ada pada mereka sedangkan sebelumnya mereka memohon kemenangan atas orang-orang kafir, ternyata setelah sampai kepada mereka apa yang telah mereka ketahui itu, mereka

2:90

# بِئْسَمَا اشْتَرَوْا بِهٖٓ اَنْفُسَهُمْ اَنْ يَّكْفُرُوْا بِمَآ اَنْزَلَ اللّٰهُ بَغْيًا اَنْ يُّنَزِّلَ اللّٰهُ مِنْ فَضْلِهٖ عَلٰى مَنْ يَّشَاۤءُ مِنْ عِبَادِهٖ ۚ فَبَاۤءُوْ بِغَضَبٍ عَلٰى غَضَبٍۗ وَلِلْكٰفِرِيْنَ عَذَابٌ مُّهِيْنٌ

bi/sam*aa* isytaraw bihi anfusahum an yakfuruu bim*aa* anzala **al**l*aa*hu baghyan an yunazzila **al**l*aa*hu min fa*dh*lihi 'al*aa* man yasy*aa*u min 'ib*aa*dihi fab*aa*uu bigha*dh*

Sangatlah buruk (perbuatan) mereka menjual dirinya, dengan mengingkari apa yang diturunkan Allah, karena dengki bahwa Allah menurunkan karunia-Nya kepada siapa yang Dia kehendaki di antara hamba-hamba-Nya. Karena itulah mereka menanggung kemurkaan demi ke

2:91

# وَاِذَا قِيْلَ لَهُمْ اٰمِنُوْا بِمَآ اَنْزَلَ اللّٰهُ قَالُوْا نُؤْمِنُ بِمَآ اُنْزِلَ عَلَيْنَا وَيَكْفُرُوْنَ بِمَا وَرَاۤءَهٗ وَهُوَ الْحَقُّ مُصَدِّقًا لِّمَا مَعَهُمْ ۗ قُلْ فَلِمَ تَقْتُلُوْنَ اَنْۢبِيَاۤءَ اللّٰهِ مِنْ قَبْلُ اِنْ كُنْتُمْ مُّؤْ

wa-i*dzaa* qiila lahum *aa*minuu bim*aa* anzala **al**l*aa*hu q*aa*luu nu/minu bim*aa* unzila 'alayn*aa* wayakfuruuna bim*aa* war*aa*-ahu wahuwa **a**l*h*aqqu mu*sh*addiqan l

Dan apabila dikatakan kepada mereka, “Berimanlah kepada apa yang diturunkan Allah (Al-Qur'an),” mereka menjawab, “Kami beriman kepada apa yang diturunkan kepada kami.” Dan mereka ingkar kepada apa yang setelahnya, padahal (Al-Qur'an) itu adalah yang hak y

2:92

# ۞ وَلَقَدْ جَاۤءَكُمْ مُّوْسٰى بِالْبَيِّنٰتِ ثُمَّ اتَّخَذْتُمُ الْعِجْلَ مِنْۢ بَعْدِهٖ وَاَنْتُمْ ظٰلِمُوْنَ

walaqad j*aa*-akum muus*aa* bi**a**lbayyin*aa*ti tsumma ittakha*dz*tumu **a**l'ijla min ba'dihi wa-antum *zhaa*limuun**a**

Dan sungguh, Musa telah datang kepadamu dengan bukti-bukti kebenaran, kemudian kamu mengambil (patung) anak sapi (sebagai sesembahan) setelah (kepergian)nya, dan kamu (menjadi) orang-orang zalim.

2:93

# وَاِذْ اَخَذْنَا مِيْثَاقَكُمْ وَرَفَعْنَا فَوْقَكُمُ الطُّوْرَۗ خُذُوْا مَآ اٰتَيْنٰكُمْ بِقُوَّةٍ وَّاسْمَعُوْا ۗ قَالُوْا سَمِعْنَا وَعَصَيْنَا وَاُشْرِبُوْا فِيْ قُلُوْبِهِمُ الْعِجْلَ بِكُفْرِهِمْ ۗ قُلْ بِئْسَمَا يَأْمُرُكُمْ بِهٖٓ اِيْمَانُكُمْ اِ

wa-i*dz* akha*dz*n*aa* miits*aa*qakum warafa'n*aa* fawqakumu **al***ththh*uura khu*dz*uu m*aa* *aa*tayn*aa*kum biquwwatin wa**i**sma'uu q*aa*luu sami'n*aa* wa'a*sh*

Dan (ingatlah) ketika Kami mengambil janji kamu dan Kami angkat gunung (Sinai) di atasmu (seraya berfirman), “Pegang teguhlah apa yang Kami berikan kepadamu dan dengarkanlah!” Mereka menjawab, “Kami mendengarkan tetapi kami tidak menaati.” Dan diresapkanl

2:94

# قُلْ اِنْ كَانَتْ لَكُمُ الدَّارُ الْاٰخِرَةُ عِنْدَ اللّٰهِ خَالِصَةً مِّنْ دُوْنِ النَّاسِ فَتَمَنَّوُا الْمَوْتَ اِنْ كُنْتُمْ صٰدِقِيْنَ

qul in k*aa*nat lakumu **al**dd*aa*ru **a**l-*aa*khiratu 'inda **al**l*aa*hi kh*aa*li*sh*atan min duuni **al**nn*aa*si fatamannawuu **a**lmawta in kuntum <

Katakanlah (Muhammad), “Jika negeri akhirat di sisi Allah, khusus untukmu saja bukan untuk orang lain, maka mintalah kematian jika kamu orang yang benar.”

2:95

# وَلَنْ يَّتَمَنَّوْهُ اَبَدًاۢ بِمَا قَدَّمَتْ اَيْدِيْهِمْ ۗ وَاللّٰهُ عَلِيْمٌ ۢ بِالظّٰلِمِيْنَ

walan yatamannawhu abadan bim*aa* qaddamat aydiihim wa**al**l*aa*hu 'aliimun bia**l***zhzhaa*limiin**a**

Tetapi mereka tidak akan menginginkan kematian itu sama sekali, karena dosa-dosa yang telah dilakukan tangan-tangan mereka. Dan Allah Maha Mengetahui orang-orang zalim.

2:96

# وَلَتَجِدَنَّهُمْ اَحْرَصَ النَّاسِ عَلٰى حَيٰوةٍ ۛوَمِنَ الَّذِيْنَ اَشْرَكُوْا ۛيَوَدُّ اَحَدُهُمْ لَوْ يُعَمَّرُ اَلْفَ سَنَةٍۚ وَمَا هُوَ بِمُزَحْزِحِهٖ مِنَ الْعَذَابِ اَنْ يُّعَمَّرَۗ وَاللّٰهُ بَصِيْرٌۢ بِمَا يَعْمَلُوْنَ ࣖ

walatajidannahum a*h*ra*sh*a **al**nn*aa*si 'al*aa* *h*ay*aa*tin wamina **al**la*dz*iina asyrakuu yawaddu a*h*aduhum law yu'ammaru **a**lfa sanatin wam*aa* huwa bimuza*h*

Dan sungguh, engkau (Muhammad) akan mendapati mereka (orang-orang Yahudi), manusia yang paling tamak akan kehidupan (dunia), bahkan (lebih tamak) dari orang-orang musyrik. Masing-masing dari mereka, ingin diberi umur seribu tahun, padahal umur panjang itu

2:97

# قُلْ مَنْ كَانَ عَدُوًّا لِّجِبْرِيْلَ فَاِنَّهٗ نَزَّلَهٗ عَلٰى قَلْبِكَ بِاِذْنِ اللّٰهِ مُصَدِّقًا لِّمَا بَيْنَ يَدَيْهِ وَهُدًى وَّبُشْرٰى لِلْمُؤْمِنِيْنَ

qul man k*aa*na 'aduwwan lijibriila fa-innahu nazzalahu 'al*aa* qalbika bi-i*dz*ni **al**l*aa*hi mu*sh*addiqan lim*aa* bayna yadayhi wahudan wabusyr*aa* lilmu/miniin**a**

Katakanlah (Muhammad), “Barangsiapa menjadi musuh Jibril, maka (ketahuilah) bahwa dialah yang telah menurunkan (Al-Qur'an) ke dalam hatimu dengan izin Allah, membenarkan apa (kitab-kitab) yang terdahulu, dan menjadi petunjuk serta berita gembira bagi oran

2:98

# مَنْ كَانَ عَدُوًّا لِّلّٰهِ وَمَلٰۤىِٕكَتِهٖ وَرُسُلِهٖ وَجِبْرِيْلَ وَمِيْكٰىلَ فَاِنَّ اللّٰهَ عَدُوٌّ لِّلْكٰفِرِيْنَ

man k*aa*na 'aduwwan lill*aa*hi wamal*aa*-ikatihi warusulihi wajibriila wamiik*aa*la fa-inna **al**l*aa*ha 'aduwwun lilk*aa*firiin**a**

Barangsiapa menjadi musuh Allah, malaikat-malaikat-Nya, rasul-rasul-Nya, Jibril dan Mikail, maka sesungguhnya Allah musuh bagi orang-orang kafir.

2:99

# وَلَقَدْ اَنْزَلْنَآ اِلَيْكَ اٰيٰتٍۢ بَيِّنٰتٍۚ وَمَا يَكْفُرُ بِهَآ اِلَّا الْفٰسِقُوْنَ

walaqad anzaln*aa* ilayka *aa*y*aa*tin bayyin*aa*tin wam*aa* yakfuru bih*aa* ill*aa* **a**lf*aa*siquun**a**

Dan sungguh, Kami telah menurunkan ayat-ayat yang jelas kepadamu (Muhammad), dan tidaklah ada yang mengingkarinya selain orang-orang fasik.

2:100

# اَوَكُلَّمَا عٰهَدُوْا عَهْدًا نَّبَذَهٗ فَرِيْقٌ مِّنْهُمْ ۗ بَلْ اَكْثَرُهُمْ لَا يُؤْمِنُوْنَ

awa kullam*aa* '*aa*haduu 'ahdan naba*dz*ahu fariiqun minhum bal aktsaruhum l*aa* yu/minuun**a**

Dan mengapa setiap kali mereka mengikat janji, sekelompok mereka melanggarnya? Sedangkan sebagian besar mereka tidak beriman.

2:101

# وَلَمَّا جَاۤءَهُمْ رَسُوْلٌ مِّنْ عِنْدِ اللّٰهِ مُصَدِّقٌ لِّمَا مَعَهُمْ نَبَذَ فَرِيْقٌ مِّنَ الَّذِيْنَ اُوْتُوا الْكِتٰبَۙ كِتٰبَ اللّٰهِ وَرَاۤءَ ظُهُوْرِهِمْ كَاَنَّهُمْ لَا يَعْلَمُوْنَۖ

walammaa jaa-ahum rasuulun min 'indi allaahi mushaddiqun limaa ma'ahum nabadza fariiqun mina alladziina uutuu alkitaaba kitaaba allaahi waraa-a zhuhuurihim ka-annahum laa ya'lamuuna

Dan setelah datang kepada mereka seorang Rasul (Muhammad) dari Allah yang membenarkan apa yang ada pada mereka, sebagian dari orang-orang yang diberi Kitab (Taurat) melemparkan Kitab Allah itu ke belakang (punggung), seakan-akan mereka tidak tahu.

2:102

# وَاتَّبَعُوْا مَا تَتْلُوا الشَّيٰطِيْنُ عَلٰى مُلْكِ سُلَيْمٰنَ ۚ وَمَا كَفَرَ سُلَيْمٰنُ وَلٰكِنَّ الشَّيٰطِيْنَ كَفَرُوْا يُعَلِّمُوْنَ النَّاسَ السِّحْرَ وَمَآ اُنْزِلَ عَلَى الْمَلَكَيْنِ بِبَابِلَ هَارُوْتَ وَمَارُوْتَ ۗ وَمَا يُعَلِّمٰنِ مِنْ اَحَ

waittaba'uu maa tatluu alsysyayaathiinu 'alaa mulki sulaymaana wamaa kafara sulaymaanu walaakinna alsysyayaathiina kafaruu yu'allimuuna alnnaasa alssihra wamaa unzila 'alaa almalakayni bibaabila haaruuta wamaaruuta wamaa yu'allimaani min ahadin hattaa yaq

Dan mereka mengikuti apa yang dibaca oleh setan-setan pada masa kerajaan Sulaiman. Sulaiman itu tidak kafir tetapi setan-setan itulah yang kafir, mereka mengajarkan sihir kepada manusia dan apa yang diturunkan kepada dua malaikat di negeri Babilonia yaitu

2:103

# وَلَوْ اَنَّهُمْ اٰمَنُوْا وَاتَّقَوْا لَمَثُوْبَةٌ مِّنْ عِنْدِ اللّٰهِ خَيْرٌ ۗ لَوْ كَانُوْا يَعْلَمُوْنَ ࣖ

walaw annahum aamanuu waittaqaw lamatsuubatun min 'indi allaahi khayrun law kaanuu ya'lamuuna

Dan jika mereka beriman dan bertakwa, pahala dari Allah pasti lebih baik, sekiranya mereka tahu.

2:104

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْا لَا تَقُوْلُوْا رَاعِنَا وَقُوْلُوا انْظُرْنَا وَاسْمَعُوْا وَلِلْكٰفِرِيْنَ عَذَابٌ اَلِيْمٌ

yaa ayyuhaa alladziina aamanuu laa taquuluu raa'inaa waquuluu unzhurnaa waisma'uu walilkaafiriina 'adzaabun aliimun

Wahai orang-orang yang beriman! Janganlah kamu katakan, raa'inaa, tetapi katakanlah, “Unzhurnaa” dan dengarkanlah. Dan orang-orang kafir akan mendapat azab yang pedih.

2:105

# مَا يَوَدُّ الَّذِيْنَ كَفَرُوْا مِنْ اَهْلِ الْكِتٰبِ وَلَا الْمُشْرِكِيْنَ اَنْ يُّنَزَّلَ عَلَيْكُمْ مِّنْ خَيْرٍ مِّنْ رَّبِّكُمْ ۗ وَاللّٰهُ يَخْتَصُّ بِرَحْمَتِهٖ مَنْ يَّشَاۤءُ ۗ وَاللّٰهُ ذُو الْفَضْلِ الْعَظِيْمِ

maa yawaddu alladziina kafaruu min ahli alkitaabi walaa almusyrikiina an yunazzala 'alaykum min khayrin min rabbikum waallaahu yakhtashshu birahmatihi man yasyaau waallaahu dzuu alfadhli al'azhiimi

Orang-orang yang kafir dari Ahli Kitab dan orang-orang musyrik tidak menginginkan diturunkannya kepadamu suatu kebaikan dari Tuhanmu. Tetapi secara khusus Allah memberikan rahmat-Nya kepada orang yang Dia kehendaki. Dan Allah pemilik karunia yang besar.

2:106

# ۞ مَا نَنْسَخْ مِنْ اٰيَةٍ اَوْ نُنْسِهَا نَأْتِ بِخَيْرٍ مِّنْهَآ اَوْ مِثْلِهَا ۗ اَلَمْ تَعْلَمْ اَنَّ اللّٰهَ عَلٰى كُلِّ شَيْءٍ قَدِيْرٌ

maa nansakh min aayatin aw nunsihaa na/ti bikhayrin minhaa aw mitslihaa alam ta'lam anna allaaha 'alaa kulli syay-in qadiirun

Ayat yang Kami batalkan atau Kami hilangkan dari ingatan, pasti Kami ganti dengan yang lebih baik atau yang sebanding dengannya. Tidakkah kamu tahu bahwa Allah Mahakuasa atas segala sesuatu?

2:107

# اَلَمْ تَعْلَمْ اَنَّ اللّٰهَ لَهٗ مُلْكُ السَّمٰوٰتِ وَالْاَرْضِ ۗ وَمَا لَكُمْ مِّنْ دُوْنِ اللّٰهِ مِنْ وَّلِيٍّ وَّلَا نَصِيْرٍ

alam ta'lam anna allaaha lahu mulku alssamaawaati waal-ardhi wamaa lakum min duuni allaahi min waliyyin walaa nashiirin

Tidakkah kamu tahu bahwa Allah memiliki kerajaan langit dan bumi? Dan tidak ada bagimu pelindung dan penolong selain Allah.

2:108

# اَمْ تُرِيْدُوْنَ اَنْ تَسْـَٔلُوْا رَسُوْلَكُمْ كَمَا سُىِٕلَ مُوْسٰى مِنْ قَبْلُ ۗوَمَنْ يَّتَبَدَّلِ الْكُفْرَ بِالْاِيْمَانِ فَقَدْ ضَلَّ سَوَاۤءَ السَّبِيْلِ

am turiiduuna an tas-aluu rasuulakum kamaa su-ila muusaa min qablu waman yatabaddali alkufra bial-iimaani faqad dhalla sawaa-a alssabiili

Ataukah kamu hendak meminta kepada Rasulmu (Muhammad) seperti halnya Musa (pernah) diminta (Bani Israil) dahulu? Barangsiapa mengganti iman dengan kekafiran, maka sungguh, dia telah tersesat dari jalan yang lurus.

2:109

# وَدَّ كَثِيْرٌ مِّنْ اَهْلِ الْكِتٰبِ لَوْ يَرُدُّوْنَكُمْ مِّنْۢ بَعْدِ اِيْمَانِكُمْ كُفَّارًاۚ حَسَدًا مِّنْ عِنْدِ اَنْفُسِهِمْ مِّنْۢ بَعْدِ مَا تَبَيَّنَ لَهُمُ الْحَقُّ ۚ فَاعْفُوْا وَاصْفَحُوْا حَتّٰى يَأْتِيَ اللّٰهُ بِاَمْرِهٖ ۗ اِنَّ اللّٰهَ عَ

wadda katsiirun min ahli alkitaabi law yarudduunakum min ba'di iimaanikum kuffaaran hasadan min 'indi anfusihim min ba'di maa tabayyana lahumu alhaqqu fau'fuu waishfahuu hattaa ya/tiya allaahu bi-amrihi inna allaaha 'alaa kulli syay-in qadiirun

Banyak di antara Ahli Kitab menginginkan sekiranya mereka dapat mengembalikan kamu setelah kamu beriman, menjadi kafir kembali, karena rasa dengki dalam diri mereka, setelah kebenaran jelas bagi mereka. Maka maafkanlah dan berlapangdadalah, sampai Allah m

2:110

# وَاَقِيْمُوا الصَّلٰوةَ وَاٰتُوا الزَّكٰوةَ ۗ وَمَا تُقَدِّمُوْا لِاَنْفُسِكُمْ مِّنْ خَيْرٍ تَجِدُوْهُ عِنْدَ اللّٰهِ ۗ اِنَّ اللّٰهَ بِمَا تَعْمَلُوْنَ بَصِيْرٌ

wa-aqiimuu alshshalaata waaatuu alzzakaata wamaa tuqaddimuu li-anfusikum min khayrin tajiduuhu 'inda allaahi inna allaaha bimaa ta'maluuna bashiirun

Dan laksanakanlah salat dan tunaikanlah zakat. Dan segala kebaikan yang kamu kerjakan untuk dirimu, kamu akan mendapatkannya (pahala) di sisi Allah. Sungguh, Allah Maha Melihat apa yang kamu kerjakan.

2:111

# وَقَالُوْا لَنْ يَّدْخُلَ الْجَنَّةَ اِلَّا مَنْ كَانَ هُوْدًا اَوْ نَصٰرٰى ۗ تِلْكَ اَمَانِيُّهُمْ ۗ قُلْ هَاتُوْا بُرْهَانَكُمْ اِنْ كُنْتُمْ صٰدِقِيْنَ

waqaaluu lan yadkhula aljannata illaa man kaana huudan aw nashaaraa tilka amaaniyyuhum qul haatuu burhaanakum in kuntum shaadiqiina

Dan mereka (Yahudi dan Nasrani) berkata, “Tidak akan masuk surga kecuali orang Yahudi atau Nasrani.” Itu (hanya) angan-angan mereka. Katakanlah, “Tunjukkan bukti kebenaranmu jika kamu orang yang benar.”

2:112

# بَلٰى مَنْ اَسْلَمَ وَجْهَهٗ لِلّٰهِ وَهُوَ مُحْسِنٌ فَلَهٗٓ اَجْرُهٗ عِنْدَ رَبِّهٖۖ وَلَا خَوْفٌ عَلَيْهِمْ وَلَا هُمْ يَحْزَنُوْنَ ࣖ

balaa man aslama wajhahu lillaahi wahuwa muhsinun falahu ajruhu 'inda rabbihi walaa khawfun 'alayhim walaa hum yahzanuuna

Tidak! Barangsiapa menyerahkan diri sepenuhnya kepada Allah, dan dia berbuat baik, dia mendapat pahala di sisi Tuhannya dan tidak ada rasa takut pada mereka dan mereka tidak bersedih hati.

2:113

# وَقَالَتِ الْيَهُوْدُ لَيْسَتِ النَّصٰرٰى عَلٰى شَيْءٍۖ وَّقَالَتِ النَّصٰرٰى لَيْسَتِ الْيَهُوْدُ عَلٰى شَيْءٍۙ وَّهُمْ يَتْلُوْنَ الْكِتٰبَۗ كَذٰلِكَ قَالَ الَّذِيْنَ لَا يَعْلَمُوْنَ مِثْلَ قَوْلِهِمْ ۚ فَاللّٰهُ يَحْكُمُ بَيْنَهُمْ يَوْمَ الْقِيٰمَةِ

waqaalati alyahuudu laysati alnnashaaraa 'alaa syay-in waqaalati alnnashaaraa laysati alyahuudu 'alaa syay-in wahum yatluuna alkitaaba kadzaalika qaala alladziina laa ya'lamuuna mitsla qawlihim faallaahu yahkumu baynahum yawma alqiyaamati fiimaa kaanuu fi

Dan orang Yahudi berkata, “Orang Nasrani itu tidak memiliki sesuatu (pegangan),” dan orang-orang Nasrani (juga) berkata, “Orang-orang Yahudi tidak memiliki sesuatu (pegangan),” padahal mereka membaca Kitab. Demikian pula orang-orang yang tidak berilmu, be

2:114

# وَمَنْ اَظْلَمُ مِمَّنْ مَّنَعَ مَسٰجِدَ اللّٰهِ اَنْ يُّذْكَرَ فِيْهَا اسْمُهٗ وَسَعٰى فِيْ خَرَابِهَاۗ اُولٰۤىِٕكَ مَا كَانَ لَهُمْ اَنْ يَّدْخُلُوْهَآ اِلَّا خَاۤىِٕفِيْنَ ەۗ لَهُمْ فِى الدُّنْيَا خِزْيٌ وَّلَهُمْ فِى الْاٰخِرَةِ عَذَابٌ عَظِيْمٌ

waman azhlamu mimman mana'a masaajida allaahi an yudzkara fiihaa ismuhu wasa'aa fii kharaabihaa ulaa-ika maa kaana lahum an yadkhuluuhaa illaa khaa-ifiina lahum fii alddunyaa khizyun walahum fii al-aakhirati 'adzaabun 'azhiimun

Dan siapakah yang lebih zalim daripada orang yang melarang di dalam masjid-masjid Allah untuk menyebut nama-Nya, dan berusaha merobohkannya? Mereka itu tidak pantas memasukinya kecuali dengan rasa takut (kepada Allah). Mereka mendapat kehinaan di dunia da

2:115

# وَلِلّٰهِ الْمَشْرِقُ وَالْمَغْرِبُ فَاَيْنَمَا تُوَلُّوْا فَثَمَّ وَجْهُ اللّٰهِ ۗ اِنَّ اللّٰهَ وَاسِعٌ عَلِيْمٌ

walillaahi almasyriqu waalmaghribu fa-aynamaa tuwalluu fatsamma wajhu allaahi inna allaaha waasi'un 'aliimun

Dan milik Allah timur dan barat. Kemanapun kamu menghadap di sanalah wajah Allah. Sungguh, Allah Mahaluas, Maha Mengetahui.

2:116

# وَقَالُوا اتَّخَذَ اللّٰهُ وَلَدًا ۙسُبْحٰنَهٗ ۗ بَلْ لَّهٗ مَا فِى السَّمٰوٰتِ وَالْاَرْضِۗ كُلٌّ لَّهٗ قَانِتُوْنَ

waqaaluu itakhadza allaahu waladan subhaanahu bal lahu maa fii alssamaawaati waal-ardhi kullun lahu qaanituuna

Dan mereka berkata, “Allah mempunyai anak.” Mahasuci Allah, bahkan milik-Nyalah apa yang di langit dan di bumi. Semua tunduk kepada-Nya.

2:117

# بَدِيْعُ السَّمٰوٰتِ وَالْاَرْضِۗ وَاِذَا قَضٰٓى اَمْرًا فَاِنَّمَا يَقُوْلُ لَهٗ كُنْ فَيَكُوْنُ

badii'u alssamaawaati waal-ardhi wa-idzaa qadaa amran fa-innamaa yaquulu lahu kun fayakuunu

(Allah) pencipta langit dan bumi. Apabila Dia hendak menetapkan sesuatu, Dia hanya berkata kepadanya, “Jadilah!” Maka jadilah sesuatu itu.

2:118

# وَقَالَ الَّذِيْنَ لَا يَعْلَمُوْنَ لَوْلَا يُكَلِّمُنَا اللّٰهُ اَوْ تَأْتِيْنَآ اٰيَةٌ ۗ كَذٰلِكَ قَالَ الَّذِيْنَ مِنْ قَبْلِهِمْ مِّثْلَ قَوْلِهِمْ ۗ تَشَابَهَتْ قُلُوْبُهُمْ ۗ قَدْ بَيَّنَّا الْاٰيٰتِ لِقَوْمٍ يُّوْقِنُوْنَ

waqaala alladziina laa ya'lamuuna lawlaa yukallimunaa allaahu aw ta/tiinaa aayatun kadzaalika qaala alladziina min qablihim mitsla qawlihim tasyaabahat quluubuhum qad bayyannaa al-aayaati liqawmin yuuqinuuna

Dan orang-orang yang tidak mengetahui berkata, “Mengapa Allah tidak berbicara dengan kita atau datang tanda-tanda (kekuasaan-Nya) kepada kita?” Demikian pula orang-orang yang sebelum mereka telah berkata seperti ucapan mereka itu. Hati mereka serupa. Sesu

2:119

# اِنَّآ اَرْسَلْنٰكَ بِالْحَقِّ بَشِيْرًا وَّنَذِيْرًاۙ وَّلَا تُسْـَٔلُ عَنْ اَصْحٰبِ الْجَحِيْمِ

innaa arsalnaaka bialhaqqi basyiiran wanadziiran walaa tus-alu 'an ash-haabi aljahiimi

Sungguh, Kami telah mengutusmu (Muhammad) dengan kebenaran, sebagai pembawa berita gembira dan pemberi peringatan. Dan engkau tidak akan diminta (pertanggungjawaban) tentang penghuni-penghuni neraka.

2:120

# وَلَنْ تَرْضٰى عَنْكَ الْيَهُوْدُ وَلَا النَّصٰرٰى حَتّٰى تَتَّبِعَ مِلَّتَهُمْ ۗ قُلْ اِنَّ هُدَى اللّٰهِ هُوَ الْهُدٰى ۗ وَلَىِٕنِ اتَّبَعْتَ اَهْوَاۤءَهُمْ بَعْدَ الَّذِيْ جَاۤءَكَ مِنَ الْعِلْمِ ۙ مَا لَكَ مِنَ اللّٰهِ مِنْ وَّلِيٍّ وَّلَا نَصِيْرٍ

walan tardaa 'anka alyahuudu walaa alnnashaaraa hattaa tattabi'a millatahum qul inna hudaa allaahi huwa alhudaa wala-ini ittaba'ta ahwaa-ahum ba'da alladzii jaa-aka mina al'ilmi maa laka mina allaahi min waliyyin walaa nashiirin

Dan orang-orang Yahudi dan Nasrani tidak akan rela kepadamu (Muhammad) sebelum engkau mengikuti agama mereka. Katakanlah, “Sesungguhnya petunjuk Allah itulah petunjuk (yang sebenarnya).” Dan jika engkau mengikuti keinginan mereka setelah ilmu (kebenaran)

2:121

# اَلَّذِيْنَ اٰتَيْنٰهُمُ الْكِتٰبَ يَتْلُوْنَهٗ حَقَّ تِلَاوَتِهٖۗ اُولٰۤىِٕكَ يُؤْمِنُوْنَ بِهٖ ۗ وَمَنْ يَّكْفُرْ بِهٖ فَاُولٰۤىِٕكَ هُمُ الْخٰسِرُوْنَ ࣖ

alladziina aataynaahumu alkitaaba yatluunahu haqqa tilaawatihi ulaa-ika yu/minuuna bihi waman yakfur bihi faulaa-ika humu alkhaasiruuna

Orang-orang yang telah Kami beri Kitab, mereka membacanya sebagaimana mestinya, mereka itulah yang beriman kepadanya. Dan barangsiapa ingkar kepadanya, mereka itulah orang-orang yang rugi.

2:122

# يٰبَنِيْٓ اِسْرَاۤءِيْلَ اذْكُرُوْا نِعْمَتِيَ الَّتِيْٓ اَنْعَمْتُ عَلَيْكُمْ وَاَنِّيْ فَضَّلْتُكُمْ عَلَى الْعٰلَمِيْنَ

yaa banii israa-iila udzkuruu ni'matiya allatii an'amtu 'alaykum wa-annii fadhdhaltukum 'alaa al'aalamiina

Wahai Bani Israil! Ingatlah nikmat-Ku yang telah Aku berikan kepadamu dan Aku telah melebihkan kamu dari semua umat yang lain di alam ini (pada masa itu).

2:123

# وَاتَّقُوْا يَوْمًا لَّا تَجْزِيْ نَفْسٌ عَنْ نَّفْسٍ شَيْـًٔا وَّلَا يُقْبَلُ مِنْهَا عَدْلٌ وَّلَا تَنْفَعُهَا شَفَاعَةٌ وَّلَا هُمْ يُنْصَرُوْنَ

waittaquu yawman laa tajzii nafsun 'an nafsin syay-an walaa yuqbalu minhaa 'adlun walaa tanfa'uhaa syafaa'atun walaa hum yunsharuuna

Dan takutlah kamu pada hari, (ketika) tidak seorang pun dapat menggantikan (membela) orang lain sedikit pun, tebusan tidak diterima, bantuan tidak berguna baginya, dan mereka tidak akan ditolong.

2:124

# ۞ وَاِذِ ابْتَلٰٓى اِبْرٰهٖمَ رَبُّهٗ بِكَلِمٰتٍ فَاَتَمَّهُنَّ ۗ قَالَ اِنِّيْ جَاعِلُكَ لِلنَّاسِ اِمَامًا ۗ قَالَ وَمِنْ ذُرِّيَّتِيْ ۗ قَالَ لَا يَنَالُ عَهْدِى الظّٰلِمِيْنَ

wa-idzi ibtalaa ibraahiima rabbuhu bikalimaatin fa-atammahunna qaala innii jaa'iluka lilnnaasi imaaman qaala wamin dzurriyyatii qaala laa yanaalu 'ahdii alzhzhaalimiina

Dan (ingatlah), ketika Ibrahim diuji Tuhannya dengan beberapa kalimat, lalu dia melaksanakannya dengan sempurna. Dia (Allah) berfirman, “Sesungguhnya Aku menjadikan engkau sebagai pemimpin bagi seluruh manusia.” Dia (Ibrahim) berkata, “Dan (juga) dari ana

2:125

# وَاِذْ جَعَلْنَا الْبَيْتَ مَثَابَةً لِّلنَّاسِ وَاَمْنًاۗ وَاتَّخِذُوْا مِنْ مَّقَامِ اِبْرٰهٖمَ مُصَلًّىۗ وَعَهِدْنَآ اِلٰٓى اِبْرٰهٖمَ وَاِسْمٰعِيْلَ اَنْ طَهِّرَا بَيْتِيَ لِلطَّاۤىِٕفِيْنَ وَالْعٰكِفِيْنَ وَالرُّكَّعِ السُّجُوْدِ

wa-idz ja'alnaa albayta matsaabatan lilnnaasi wa-amnan waittakhidzuu min maqaami ibraahiima mushallan wa'ahidnaa ilaa ibraahiima wa-ismaa'iila an thahhiraa baytiya lilththaa-ifiina waal'aakifiina waalrrukka'i alssujuudi

Dan (ingatlah), ketika Kami menjadikan rumah (Ka’bah) tempat berkumpul dan tempat yang aman bagi manusia. Dan jadikanlah maqam Ibrahim itu tempat salat. Dan telah Kami perintahkan kepada Ibrahim dan Ismail, “Bersihkanlah rumah-Ku untuk orang-orang yang t

2:126

# وَاِذْ قَالَ اِبْرٰهٖمُ رَبِّ اجْعَلْ هٰذَا بَلَدًا اٰمِنًا وَّارْزُقْ اَهْلَهٗ مِنَ الثَّمَرٰتِ مَنْ اٰمَنَ مِنْهُمْ بِاللّٰهِ وَالْيَوْمِ الْاٰخِرِۗ قَالَ وَمَنْ كَفَرَ فَاُمَتِّعُهٗ قَلِيْلًا ثُمَّ اَضْطَرُّهٗٓ اِلٰى عَذَابِ النَّارِ ۗ وَبِئْسَ الْمَصِ

wa-idz qaala ibraahiimu rabbi ij'al haadzaa baladan aaminan waurzuq ahlahu mina altstsamaraati man aamana minhum biallaahi waalyawmi al-aakhiri qaala waman kafara faumatti'uhu qaliilan tsumma adtharruhu ilaa 'adzaabi alnnaari wabi/sa almashiiru

Dan (ingatlah) ketika Ibrahim berdoa, “Ya Tuhanku, jadikanlah (negeri Mekah) ini negeri yang aman dan berilah rezeki berupa buah-buahan kepada penduduknya, yaitu di antara mereka yang beriman kepada Allah dan hari kemudian,” Dia (Allah) berfirman, “Dan ke

2:127

# وَاِذْ يَرْفَعُ اِبْرٰهٖمُ الْقَوَاعِدَ مِنَ الْبَيْتِ وَاِسْمٰعِيْلُۗ رَبَّنَا تَقَبَّلْ مِنَّا ۗ اِنَّكَ اَنْتَ السَّمِيْعُ الْعَلِيْمُ

wa-idz yarfa'u ibraahiimu alqawaa'ida mina albayti wa-ismaa'iilu rabbanaa taqabbal minnaa innaka anta alssamii'u al'aliimu

Dan (ingatlah) ketika Ibrahim meninggikan pondasi Baitullah bersama Ismail, (seraya berdoa), “Ya Tuhan kami, terimalah (amal) dari kami. Sungguh, Engkaulah Yang Maha Mendengar, Maha Mengetahui.

2:128

# رَبَّنَا وَاجْعَلْنَا مُسْلِمَيْنِ لَكَ وَمِنْ ذُرِّيَّتِنَآ اُمَّةً مُّسْلِمَةً لَّكَۖ وَاَرِنَا مَنَاسِكَنَا وَتُبْ عَلَيْنَا ۚ اِنَّكَ اَنْتَ التَّوَّابُ الرَّحِيْمُ

rabbanaa waij'alnaa muslimayni laka wamin dzurriyyatinaa ummatan muslimatan laka wa-arinaa manaasikanaa watub 'alaynaa innaka anta alttawwaabu alrrahiimu

Ya Tuhan kami, jadikanlah kami orang yang berserah diri kepada-Mu, dan anak cucu kami (juga) umat yang berserah diri kepada-Mu dan tunjukkanlah kepada kami cara-cara melakukan ibadah (haji) kami, dan terimalah tobat kami. Sungguh, Engkaulah Yang Maha Pene

2:129

# رَبَّنَا وَابْعَثْ فِيْهِمْ رَسُوْلًا مِّنْهُمْ يَتْلُوْا عَلَيْهِمْ اٰيٰتِكَ وَيُعَلِّمُهُمُ الْكِتٰبَ وَالْحِكْمَةَ وَيُزَكِّيْهِمْ ۗ اِنَّكَ اَنْتَ الْعَزِيْزُ الْحَكِيْمُ ࣖ

rabbanaa waib'ats fiihim rasuulan minhum yatluu 'alayhim aayaatika wayu'allimuhumu alkitaaba waalhikmata wayuzakkiihim innaka anta al'aziizu alhakiimu

Ya Tuhan kami, utuslah di tengah mereka seorang rasul dari kalangan mereka sendiri, yang akan membacakan kepada mereka ayat-ayat-Mu dan mengajarkan Kitab dan Hikmah kepada mereka, dan menyucikan mereka. Sungguh, Engkaulah Yang Mahaperkasa, Mahabijaksana.”

2:130

# وَمَنْ يَّرْغَبُ عَنْ مِّلَّةِ اِبْرٰهٖمَ اِلَّا مَنْ سَفِهَ نَفْسَهٗ ۗوَلَقَدِ اصْطَفَيْنٰهُ فِى الدُّنْيَا ۚوَاِنَّهٗ فِى الْاٰخِرَةِ لَمِنَ الصّٰلِحِيْنَ

waman yarghabu 'an millati ibraahiima illaa man safiha nafsahu walaqadi isthafaynaahu fii alddunyaa wa-innahu fii al-aakhirati lamina alshshaalihiina

Dan orang yang membenci agama Ibrahim, hanyalah orang yang memperbodoh dirinya sendiri. Dan sungguh, Kami telah memilihnya (Ibrahim) di dunia ini. Dan sesungguhnya di akhirat dia termasuk orang-orang saleh.

2:131

# اِذْ قَالَ لَهٗ رَبُّهٗٓ اَسْلِمْۙ قَالَ اَسْلَمْتُ لِرَبِّ الْعٰلَمِيْنَ

idz qaala lahu rabbuhu aslim qaala aslamtu lirabbi al'aalamiina

(Ingatlah) ketika Tuhan berfirman kepadanya (Ibrahim), “Berserahdirilah!” Dia menjawab, “Aku berserah diri kepada Tuhan seluruh alam.”

2:132

# وَوَصّٰى بِهَآ اِبْرٰهٖمُ بَنِيْهِ وَيَعْقُوْبُۗ يٰبَنِيَّ اِنَّ اللّٰهَ اصْطَفٰى لَكُمُ الدِّيْنَ فَلَا تَمُوْتُنَّ اِلَّا وَاَنْتُمْ مُّسْلِمُوْنَ ۗ

wawashshaa bihaa ibraahiimu baniihi waya'quubu yaa baniyya inna allaaha isthafaa lakumu alddiina falaa tamuutunna illaa wa-antum muslimuuna

Dan Ibrahim mewasiatkan (ucapan) itu kepada anak-anaknya, demikian pula Yakub. “Wahai anak-anakku! Sesungguhnya Allah telah memilih agama ini untukmu, maka janganlah kamu mati kecuali dalam keadaan Muslim.”

2:133

# اَمْ كُنْتُمْ شُهَدَاۤءَ اِذْ حَضَرَ يَعْقُوْبَ الْمَوْتُۙ اِذْ قَالَ لِبَنِيْهِ مَا تَعْبُدُوْنَ مِنْۢ بَعْدِيْۗ قَالُوْا نَعْبُدُ اِلٰهَكَ وَاِلٰهَ اٰبَاۤىِٕكَ اِبْرٰهٖمَ وَاِسْمٰعِيْلَ وَاِسْحٰقَ اِلٰهًا وَّاحِدًاۚ وَنَحْنُ لَهٗ مُسْلِمُوْنَ

am kuntum syuhadaa-a idz hadhara ya'quuba almawtu idz qaala libaniihi maa ta'buduuna min ba'dii qaaluu na'budu ilaahaka wa-ilaaha aabaa-ika ibraahiima wa-ismaa'iila wa-ishaaqa ilaahan waahidan wanahnu lahu muslimuuna

Apakah kamu menjadi saksi saat maut akan menjemput Yakub, ketika dia berkata kepada anak-anaknya, “Apa yang kamu sembah sepeninggalku?” Mereka menjawab, “Kami akan menyembah Tuhanmu dan Tuhan nenek moyangmu yaitu Ibrahim, Ismail dan Ishak, (yaitu) Tuhan Y

2:134

# تِلْكَ اُمَّةٌ قَدْ خَلَتْ ۚ لَهَا مَا كَسَبَتْ وَلَكُمْ مَّا كَسَبْتُمْ ۚ وَلَا تُسْـَٔلُوْنَ عَمَّا كَانُوْا يَعْمَلُوْنَ

tilka ummatun qad khalat lahaa maa kasabat walakum maa kasabtum walaa tus-aluuna 'ammaa kaanu ya'maluuna

Itulah umat yang telah lalu. Baginya apa yang telah mereka usahakan dan bagimu apa yang telah kamu usahakan. Dan kamu tidak akan diminta (pertanggungjawaban) tentang apa yang dahulu mereka kerjakan.

2:135

# وَقَالُوْا كُوْنُوْا هُوْدًا اَوْ نَصٰرٰى تَهْتَدُوْا ۗ قُلْ بَلْ مِلَّةَ اِبْرٰهٖمَ حَنِيْفًا ۗوَمَا كَانَ مِنَ الْمُشْرِكِيْنَ

waqaaluu kuunuu huudan aw nashaaraa tahtaduu qul bal millata ibraahiima haniifan wamaa kaana mina almusyrikiina

Dan mereka berkata, “Jadilah kamu (penganut) Yahudi atau Nasrani, niscaya kamu mendapat petunjuk.” Katakanlah, “(Tidak!) Tetapi (kami mengikuti) agama Ibrahim yang lurus dan dia tidak termasuk golongan orang yang mempersekutukan Tuhan.”

2:136

# قُوْلُوْٓا اٰمَنَّا بِاللّٰهِ وَمَآ اُنْزِلَ اِلَيْنَا وَمَآ اُنْزِلَ اِلٰٓى اِبْرٰهٖمَ وَاِسْمٰعِيْلَ وَاِسْحٰقَ وَيَعْقُوْبَ وَالْاَسْبَاطِ وَمَآ اُوْتِيَ مُوْسٰى وَعِيْسٰى وَمَآ اُوْتِيَ النَّبِيُّوْنَ مِنْ رَّبِّهِمْۚ لَا نُفَرِّقُ بَيْنَ اَحَدٍ

quuluu aamannaa biallaahi wamaa unzila ilaynaa wamaa unzila ilaa ibraahiima wa-ismaa'iila wa-ishaaqa waya'quuba waal-asbaathi wamaa uutiya muusaa wa'iisaa wamaa uutiya alnnabiyyuuna min rabbihim laa nufarriqu bayna ahadin minhum wanahnu lahu muslimuuna

Katakanlah, “Kami beriman kepada Allah dan kepada apa yang diturunkan kepada kami, dan kepada apa yang diturunkan kepada Ibrahim, Ismail, Ishak, Yakub dan anak cucunya, dan kepada apa yang diberikan kepada Musa dan Isa serta kepada apa yang diberikan kepa

2:137

# فَاِنْ اٰمَنُوْا بِمِثْلِ مَآ اٰمَنْتُمْ بِهٖ فَقَدِ اهْتَدَوْا ۚوَاِنْ تَوَلَّوْا فَاِنَّمَا هُمْ فِيْ شِقَاقٍۚ فَسَيَكْفِيْكَهُمُ اللّٰهُ ۚوَهُوَ السَّمِيْعُ الْعَلِيْمُ ۗ

fa-in aamanuu bimitsli maa aamantum bihi faqadi ihtadaw wa-in tawallaw fa-innamaa hum fii syiqaaqin fasayakfiikahumu allaahu wahuwa alssamii'u al'aliimu

Maka jika mereka telah beriman sebagaimana yang kamu imani, sungguh, mereka telah mendapat petunjuk. Tetapi jika mereka berpaling, sesungguhnya mereka berada dalam permusuhan (denganmu), maka Allah mencukupkan engkau (Muhammad) terhadap mereka (dengan per

2:138

# صِبْغَةَ اللّٰهِ ۚ وَمَنْ اَحْسَنُ مِنَ اللّٰهِ صِبْغَةً ۖ وَّنَحْنُ لَهٗ عٰبِدُوْنَ

shibghata allaahi waman ahsanu mina allaahi shibghatan wanahnu lahu 'aabiduuna

Sibgah Allah.” Siapa yang lebih baik sibgah-nya daripada Allah? Dan kepada-Nya kami menyembah.

2:139

# قُلْ اَتُحَاۤجُّوْنَنَا فِى اللّٰهِ وَهُوَ رَبُّنَا وَرَبُّكُمْۚ وَلَنَآ اَعْمَالُنَا وَلَكُمْ اَعْمَالُكُمْۚ وَنَحْنُ لَهٗ مُخْلِصُوْنَ ۙ

qul atuhaajjuunanaa fii allaahi wahuwa rabbunaa warabbukum walanaa a'maalunaa walakum a'maalukum wanahnu lahu mukhlishuuna

Katakanlah (Muhammad), “Apakah kamu hendak berdebat dengan kami tentang Allah, padahal Dia adalah Tuhan kami dan Tuhan kamu. Bagi kami amalan kami, bagi kamu amalan kamu, dan hanya kepada-Nya kami dengan tulus mengabdikan diri.

2:140

# اَمْ تَقُوْلُوْنَ اِنَّ اِبْرٰهٖمَ وَاِسْمٰعِيْلَ وَاِسْحٰقَ وَيَعْقُوْبَ وَالْاَسْبَاطَ كَانُوْا هُوْدًا اَوْ نَصٰرٰى ۗ قُلْ ءَاَنْتُمْ اَعْلَمُ اَمِ اللّٰهُ ۗ وَمَنْ اَظْلَمُ مِمَّنْ كَتَمَ شَهَادَةً عِنْدَهٗ مِنَ اللّٰهِ ۗ وَمَا اللّٰهُ بِغَافِلٍ عَمَّ

am taquuluuna inna ibraahiima wa-ismaa'iila wa-ishaaqa waya'quuba waal-asbaatha kaanuu huudan aw nashaaraa qul a-antum a'lamu ami allaahu waman azhlamu mimman katama syahaadatan 'indahu mina allaahi wamaa allaahu bighaafilin 'ammaa ta'maluuna

Ataukah kamu (orang-orang Yahudi dan Nasrani) berkata bahwa Ibrahim, Ismail, Ishak, Yakub dan anak cucunya adalah penganut Yahudi atau Nasrani? Katakanlah, “Kamukah yang lebih tahu atau Allah, dan siapakah yang lebih zalim daripada orang yang menyembunyik

2:141

# تِلْكَ اُمَّةٌ قَدْ خَلَتْ ۚ لَهَا مَا كَسَبَتْ وَلَكُمْ مَّا كَسَبْتُمْ ۚ وَلَا تُسْـَٔلُوْنَ عَمَّا كَانُوْا يَعْمَلُوْنَ ࣖ ۔

tilka ummatun qad khalat lah*aa* m*aa* kasabat walakum m*aa* kasabtum wal*aa* tus-aluuna 'amm*aa* k*aa*nuu ya'maluun**a**

Itulah umat yang telah lalu. Baginya apa yang telah mereka usahakan dan bagimu apa yang telah kamu usahakan. Dan kamu tidak akan diminta (pertanggungjawaban) tentang apa yang dahulu mereka kerjakan.

2:142

# ۞ سَيَقُوْلُ السُّفَهَاۤءُ مِنَ النَّاسِ مَا وَلّٰىهُمْ عَنْ قِبْلَتِهِمُ الَّتِيْ كَانُوْا عَلَيْهَا ۗ قُلْ لِّلّٰهِ الْمَشْرِقُ وَالْمَغْرِبُۗ يَهْدِيْ مَنْ يَّشَاۤءُ اِلٰى صِرَاطٍ مُّسْتَقِيْمٍ

sayaquulu **al**ssufah*aa*u mina **al**nn*aa*si m*aa* wall*aa*hum 'an qiblatihimu **al**latii k*aa*nuu 'alayh*aa* qul lill*aa*hi **a**lmasyriqu wa**a**lmag

Orang-orang yang kurang akal di antara manusia akan berkata, “Apakah yang memalingkan mereka (Muslim) dari kiblat yang dahulu mereka (berkiblat) kepadanya?” Katakanlah (Muhammad), “Milik Allah-lah timur dan barat; Dia memberi petunjuk kepada siapa yang Di

2:143

# وَكَذٰلِكَ جَعَلْنٰكُمْ اُمَّةً وَّسَطًا لِّتَكُوْنُوْا شُهَدَاۤءَ عَلَى النَّاسِ وَيَكُوْنَ الرَّسُوْلُ عَلَيْكُمْ شَهِيْدًا ۗ وَمَا جَعَلْنَا الْقِبْلَةَ الَّتِيْ كُنْتَ عَلَيْهَآ اِلَّا لِنَعْلَمَ مَنْ يَّتَّبِعُ الرَّسُوْلَ مِمَّنْ يَّنْقَلِبُ عَلٰى

waka*dzaa*lika ja'aln*aa*kum ummatan wasa*th*an litakuunuu syuhad*aa*-a 'al*aa* **al**nn*aa*si wayakuuna **al**rrasuulu 'alaykum syahiidan wam*aa* ja'aln*aa* **a**lqiblata

Dan demikian pula Kami telah menjadikan kamu (umat Islam) ”umat pertengahan” agar kamu menjadi saksi atas (perbuatan) manusia dan agar Rasul (Muhammad) menjadi saksi atas (perbuatan) kamu. Kami tidak menjadikan kiblat yang (dahulu) kamu (berkiblat) kepada

2:144

# قَدْ نَرٰى تَقَلُّبَ وَجْهِكَ فِى السَّمَاۤءِۚ فَلَنُوَلِّيَنَّكَ قِبْلَةً تَرْضٰىهَا ۖ فَوَلِّ وَجْهَكَ شَطْرَ الْمَسْجِدِ الْحَرَامِ ۗ وَحَيْثُ مَا كُنْتُمْ فَوَلُّوْا وُجُوْهَكُمْ شَطْرَهٗ ۗ وَاِنَّ الَّذِيْنَ اُوْتُوا الْكِتٰبَ لَيَعْلَمُوْنَ اَنَّهُ

qad nar*aa* taqalluba wajhika fii **al**ssam*aa*-i falanuwalliyannaka qiblatan tar*daa*h*aa* fawalli wajhaka sya*th*ra **a**lmasjidi **a**l*h*ar*aa*mi wa*h*aytsu m*aa* kun

Kami melihat wajahmu (Muhammad) sering menengadah ke langit, maka akan Kami palingkan engkau ke kiblat yang engkau senangi. Maka hadapkanlah wajahmu ke arah Masjidilharam. Dan di mana saja engkau berada, hadapkanlah wajahmu ke arah itu. Dan sesungguhnya o

2:145

# وَلَىِٕنْ اَتَيْتَ الَّذِيْنَ اُوْتُوا الْكِتٰبَ بِكُلِّ اٰيَةٍ مَّا تَبِعُوْا قِبْلَتَكَ ۚ وَمَآ اَنْتَ بِتَابِعٍ قِبْلَتَهُمْ ۚ وَمَا بَعْضُهُمْ بِتَابِعٍ قِبْلَةَ بَعْضٍۗ وَلَىِٕنِ اتَّبَعْتَ اَهْوَاۤءَهُمْ مِّنْۢ بَعْدِ مَاجَاۤءَكَ مِنَ الْعِلْمِ ۙ ا

wala-in atayta **al**la*dz*iina uutuu **a**lkit*aa*ba bikulli *aa*yatin m*aa* tabi'uu qiblataka wam*aa* anta bit*aa*bi'in qiblatahum wam*aa* ba'*dh*uhum bit*aa*bi'in qiblata ba'*dh*

*Dan walaupun engkau (Muhammad) memberikan semua ayat (keterangan) kepada orang-orang yang diberi Kitab itu, mereka tidak akan mengikuti kiblatmu, dan engkau pun tidak akan mengikuti kiblat mereka. Sebagian mereka tidak akan mengikuti kiblat sebagian yang*

2:146

# اَلَّذِيْنَ اٰتَيْنٰهُمُ الْكِتٰبَ يَعْرِفُوْنَهٗ كَمَا يَعْرِفُوْنَ اَبْنَاۤءَهُمْ ۗ وَاِنَّ فَرِيْقًا مِّنْهُمْ لَيَكْتُمُوْنَ الْحَقَّ وَهُمْ يَعْلَمُوْنَ

**al**la*dz*iina *aa*tayn*aa*humu **a**lkit*aa*ba ya'rifuunahu kam*aa* ya'rifuuna abn*aa*-ahum wa-inna fariiqan minhum layaktumuuna **a**l*h*aqqa wahum ya'lamuun**a**

Orang-orang yang telah Kami beri Kitab (Taurat dan Injil) mengenalnya (Muhammad) seperti mereka mengenal anak-anak mereka sendiri. Sesungguhnya sebagian mereka pasti menyembunyikan kebenaran, padahal mereka mengetahui(nya).

2:147

# اَلْحَقُّ مِنْ رَّبِّكَ فَلَا تَكُوْنَنَّ مِنَ الْمُمْتَرِيْنَ ࣖ

al*h*aqqu min rabbika fal*aa* takuunanna mina **a**lmumtariin**a**

Kebenaran itu dari Tuhanmu, maka janganlah sekali-kali engkau (Muhammad) termasuk orang-orang yang ragu.

2:148

# وَلِكُلٍّ وِّجْهَةٌ هُوَ مُوَلِّيْهَا فَاسْتَبِقُوا الْخَيْرٰتِۗ اَيْنَ مَا تَكُوْنُوْا يَأْتِ بِكُمُ اللّٰهُ جَمِيْعًا ۗ اِنَّ اللّٰهَ عَلٰى كُلِّ شَيْءٍ قَدِيْرٌ

walikullin wijhatun huwa muwalliih*aa* fa**i**stabiquu **a**lkhayr*aa*ti aynam*aa* takuunuu ya/ti bikumu **al**l*aa*hu jamii'an inna **al**l*aa*ha 'al*aa* kulli syay-in qadi

Dan setiap umat mempunyai kiblat yang dia menghadap kepadanya. Maka berlomba-lombalah kamu dalam kebaikan. Di mana saja kamu berada, pasti Allah akan mengumpulkan kamu semuanya. Sungguh, Allah Mahakuasa atas segala sesuatu.

2:149

# وَمِنْ حَيْثُ خَرَجْتَ فَوَلِّ وَجْهَكَ شَطْرَ الْمَسْجِدِ الْحَرَامِ ۗ وَاِنَّهٗ لَلْحَقُّ مِنْ رَّبِّكَ ۗ وَمَا اللّٰهُ بِغَافِلٍ عَمَّا تَعْمَلُوْنَ

wamin *h*aytsu kharajta fawalli wajhaka sya*th*ra **a**lmasjidi **a**l*h*ar*aa*mi wa-innahu lal*h*aqqu min rabbika wam*aa* **al**l*aa*hu bigh*aa*filin 'amm*aa* ta'maluun

Dan dari manapun engkau (Muhammad) keluar, hadapkanlah wajahmu ke arah Masjidilharam, sesungguhnya itu benar-benar ketentuan dari Tuhanmu. Allah tidak lengah terhadap apa yang kamu kerjakan.

2:150

# وَمِنْ حَيْثُ خَرَجْتَ فَوَلِّ وَجْهَكَ شَطْرَ الْمَسْجِدِ الْحَرَامِ ۗ وَحَيْثُ مَا كُنْتُمْ فَوَلُّوْا وُجُوْهَكُمْ شَطْرَهٗ ۙ لِئَلَّا يَكُوْنَ لِلنَّاسِ عَلَيْكُمْ حُجَّةٌ اِلَّا الَّذِيْنَ ظَلَمُوْا مِنْهُمْ فَلَا تَخْشَوْهُمْ وَاخْشَوْنِيْ وَلِاُتِم

wamin *h*aytsu kharajta fawalli wajhaka sya*th*ra **a**lmasjidi **a**l*h*ar*aa*mi wa*h*aytsu m*aa* kuntum fawalluu wujuuhakum sya*th*rahu li-all*aa* yakuuna li**l**nn*aa*s

Dan dari manapun engkau (Muhammad) keluar, maka hadapkanlah wajahmu ke arah Masjidilharam. Dan di mana saja kamu berada, maka hadapkanlah wajahmu ke arah itu, agar tidak ada alasan bagi manusia (untuk menentangmu), kecuali orang-orang yang zalim di antara

2:151

# كَمَآ اَرْسَلْنَا فِيْكُمْ رَسُوْلًا مِّنْكُمْ يَتْلُوْا عَلَيْكُمْ اٰيٰتِنَا وَيُزَكِّيْكُمْ وَيُعَلِّمُكُمُ الْكِتٰبَ وَالْحِكْمَةَ وَيُعَلِّمُكُمْ مَّا لَمْ تَكُوْنُوْا تَعْلَمُوْنَۗ

kam*aa* arsaln*aa* fiikum rasuulan minkum yatluu 'alaykum *aa*y*aa*tin*aa* wayuzakkiikum wayu'allimukumu **a**lkit*aa*ba wa**a**l*h*ikmata wayu'allimukum m*aa* lam takuunuu ta'lamuun

Sebagaimana Kami telah mengutus kepadamu seorang Rasul (Muhammad) dari (kalangan) kamu yang membacakan ayat-ayat Kami, menyucikan kamu, dan mengajarkan kepadamu Kitab (Al-Qur'an) dan Hikmah (Sunnah), serta mengajarkan apa yang belum kamu ketahui.

2:152

# فَاذْكُرُوْنِيْٓ اَذْكُرْكُمْ وَاشْكُرُوْا لِيْ وَلَا تَكْفُرُوْنِ ࣖ

fa**u***dz*kuruunii a*dz*kurkum wa**u**sykuruu lii wal*aa* takfuruun**i**

Maka ingatlah kepada-Ku, Aku pun akan ingat kepadamu. Bersyukurlah kepada-Ku, dan janganlah kamu ingkar kepada-Ku.

2:153

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوا اسْتَعِيْنُوْا بِالصَّبْرِ وَالصَّلٰوةِ ۗ اِنَّ اللّٰهَ مَعَ الصّٰبِرِيْنَ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu ista'iinuu bi**al***shsh*abri wa**al***shsh*al*aa*ti inna **al**l*aa*ha ma'a **al***shshaa*biriin

Wahai orang-orang yang beriman! Mohonlah pertolongan (kepada Allah) dengan sabar dan salat. Sungguh, Allah beserta orang-orang yang sabar.

2:154

# وَلَا تَقُوْلُوْا لِمَنْ يُّقْتَلُ فِيْ سَبِيْلِ اللّٰهِ اَمْوَاتٌ ۗ بَلْ اَحْيَاۤءٌ وَّلٰكِنْ لَّا تَشْعُرُوْنَ

wal*aa* taquuluu liman yuqtalu fii sabiili **al**l*aa*hi amw*aa*tun bal a*h*y*aa*un wal*aa*kin l*aa* tasy'uruun**a**

Dan janganlah kamu mengatakan orang-orang yang terbunuh di jalan Allah (mereka) telah mati. Sebenarnya (mereka) hidup, tetapi kamu tidak menyadarinya.

2:155

# وَلَنَبْلُوَنَّكُمْ بِشَيْءٍ مِّنَ الْخَوْفِ وَالْجُوْعِ وَنَقْصٍ مِّنَ الْاَمْوَالِ وَالْاَنْفُسِ وَالثَّمَرٰتِۗ وَبَشِّرِ الصّٰبِرِيْنَ

walanabluwannakum bisyay-in mina **a**lkhawfi wa**a**ljuu'i wanaq*sh*in mina **a**l-amw*aa*li wa**a**l-anfusi wa**al**tstsamar*aa*ti wabasysyiri **al***shshaa*

*Dan Kami pasti akan menguji kamu dengan sedikit ketakutan, kelaparan, kekurangan harta, jiwa, dan buah-buahan. Dan sampaikanlah kabar gembira kepada orang-orang yang sabar,*

2:156

# اَلَّذِيْنَ اِذَآ اَصَابَتْهُمْ مُّصِيْبَةٌ ۗ قَالُوْٓا اِنَّا لِلّٰهِ وَاِنَّآ اِلَيْهِ رٰجِعُوْنَۗ

**al**la*dz*iina i*dzaa* a*shaa*bat-hum mu*sh*iibatun q*aa*luu inn*aa* lill*aa*hi wa-inn*aa* ilayhi r*aa*ji'uun**a**

(yaitu) orang-orang yang apabila ditimpa musibah, mereka berkata “Inna lillahi wa inna ilaihi raji‘un” (sesungguhnya kami milik Allah dan kepada-Nyalah kami kembali).

2:157

# اُولٰۤىِٕكَ عَلَيْهِمْ صَلَوٰتٌ مِّنْ رَّبِّهِمْ وَرَحْمَةٌ ۗوَاُولٰۤىِٕكَ هُمُ الْمُهْتَدُوْنَ

ul*aa*-ika 'alayhim *sh*alaw*aa*tun min rabbihim wara*h*matun waul*aa*-ika humu **a**lmuhtaduun**a**

Mereka itulah yang memperoleh ampunan dan rahmat dari Tuhannya, dan mereka itulah orang-orang yang mendapat petunjuk.

2:158

# ۞ اِنَّ الصَّفَا وَالْمَرْوَةَ مِنْ شَعَاۤىِٕرِ اللّٰهِ ۚ فَمَنْ حَجَّ الْبَيْتَ اَوِ اعْتَمَرَ فَلَا جُنَاحَ عَلَيْهِ اَنْ يَّطَّوَّفَ بِهِمَا ۗ وَمَنْ تَطَوَّعَ خَيْرًاۙ فَاِنَّ اللّٰهَ شَاكِرٌ عَلِيْمٌ

inna **al***shsh*af*aa *wa**a**lmarwata min sya'*aa*\-iri **al**l*aa*hi faman* h*ajja **a**lbayta awi i'tamara fal*aa *jun*aah*a 'alayhi an ya*ththh*awwafa bihim

Sesungguhnya Safa dan Marwah merupakan sebagian syi‘ar (agama) Allah. Maka barangsiapa beribadah haji ke Baitullah atau berumrah, tidak ada dosa baginya mengerjakan sa‘i antara keduanya. Dan barangsiapa dengan kerelaan hati mengerjakan kebajikan, maka All

2:159

# اِنَّ الَّذِيْنَ يَكْتُمُوْنَ مَآ اَنْزَلْنَا مِنَ الْبَيِّنٰتِ وَالْهُدٰى مِنْۢ بَعْدِ مَا بَيَّنّٰهُ لِلنَّاسِ فِى الْكِتٰبِۙ اُولٰۤىِٕكَ يَلْعَنُهُمُ اللّٰهُ وَيَلْعَنُهُمُ اللّٰعِنُوْنَۙ

inna **al**la*dz*iina yaktumuuna m*aa* anzaln*aa* mina **a**lbayyin*aa*ti wa**a**lhud*aa* min ba'di m*aa* bayyann*aa*hu li**l**nn*aa*si fii **a**lkit<

Sungguh, orang-orang yang menyembunyikan apa yang telah Kami turunkan berupa keterangan-keterangan dan petunjuk, setelah Kami jelaskan kepada manusia dalam Kitab (Al-Qur'an), mereka itulah yang dilaknat Allah dan dilaknat (pula) oleh mereka yang melaknat,

2:160

# اِلَّا الَّذِيْنَ تَابُوْا وَاَصْلَحُوْا وَبَيَّنُوْا فَاُولٰۤىِٕكَ اَتُوْبُ عَلَيْهِمْ ۚ وَاَنَا التَّوَّابُ الرَّحِيْمُ

ill*aa* **al**la*dz*iina t*aa*buu wa-a*sh*la*h*uu wabayyanuu faul*aa*-ika atuubu 'alayhim wa-an*aa* **al**ttaww*aa*bu **al**rra*h*iim**u**

kecuali mereka yang telah bertobat, mengadakan perbaikan dan menjelaskan(nya), mereka itulah yang Aku terima tobatnya dan Akulah Yang Maha Penerima tobat, Maha Penyayang.

2:161

# اِنَّ الَّذِيْنَ كَفَرُوْا وَمَاتُوْا وَهُمْ كُفَّارٌ اُولٰۤىِٕكَ عَلَيْهِمْ لَعْنَةُ اللّٰهِ وَالْمَلٰۤىِٕكَةِ وَالنَّاسِ اَجْمَعِيْنَۙ

inna **al**la*dz*iina kafaruu wam*aa*tuu wahum kuff*aa*run ul*aa*-ika 'alayhim la'natu **al**l*aa*hi wa**a**lmal*aa*-ikati wa**al**nn*aa*si ajma'iin**a**

Sungguh, orang-orang yang kafir dan mati dalam keadaan kafir, mereka itu mendapat laknat Allah, para malaikat dan manusia seluruhnya,

2:162

# خٰلِدِيْنَ فِيْهَا ۚ لَا يُخَفَّفُ عَنْهُمُ الْعَذَابُ وَلَا هُمْ يُنْظَرُوْنَ

kh*aa*lidiina fiih*aa* l*aa* yukhaffafu 'anhumu **a**l'a*dzaa*bu wal*aa* hum yun*zh*aruun**a**

mereka kekal di dalamnya (laknat), tidak akan diringankan azabnya, dan mereka tidak diberi penangguhan.

2:163

# وَاِلٰهُكُمْ اِلٰهٌ وَّاحِدٌۚ لَآاِلٰهَ اِلَّا هُوَ الرَّحْمٰنُ الرَّحِيْمُ ࣖ

wa-il*aa*hukum il*aa*hun w*aah*idun l*aa* il*aa*ha ill*aa* huwa **al**rra*h*m*aa*nu **al**rra*h*iim**u**

Dan Tuhan kamu adalah Tuhan Yang Maha Esa, tidak ada tuhan selain Dia, Yang Maha Pengasih, Maha Penyayang.

2:164

# اِنَّ فِيْ خَلْقِ السَّمٰوٰتِ وَالْاَرْضِ وَاخْتِلَافِ الَّيْلِ وَالنَّهَارِ وَالْفُلْكِ الَّتِيْ تَجْرِيْ فِى الْبَحْرِ بِمَا يَنْفَعُ النَّاسَ وَمَآ اَنْزَلَ اللّٰهُ مِنَ السَّمَاۤءِ مِنْ مَّاۤءٍ فَاَحْيَا بِهِ الْاَرْضَ بَعْدَ مَوْتِهَا وَبَثَّ فِيْهَ

inna fii khalqi **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i wa**i**khtil*aa*fi **al**layli wa**al**nnah*aa*ri wa**a**lfulki **al**latii tajrii f

Sesungguhnya pada penciptaan langit dan bumi, pergantian malam dan siang, kapal yang berlayar di laut dengan (muatan) yang bermanfaat bagi manusia, apa yang diturunkan Allah dari langit berupa air, lalu dengan itu dihidupkan-Nya bumi setelah mati (kering)

2:165

# وَمِنَ النَّاسِ مَنْ يَّتَّخِذُ مِنْ دُوْنِ اللّٰهِ اَنْدَادًا يُّحِبُّوْنَهُمْ كَحُبِّ اللّٰهِ ۗ وَالَّذِيْنَ اٰمَنُوْٓا اَشَدُّ حُبًّا لِّلّٰهِ ۙوَلَوْ يَرَى الَّذِيْنَ ظَلَمُوْٓا اِذْ يَرَوْنَ الْعَذَابَۙ اَنَّ الْقُوَّةَ لِلّٰهِ جَمِيْعًا ۙوَّاَنَّ ال

wamina **al**nn*aa*si man yattakhi*dz*u min duuni **al**l*aa*hi and*aa*dan yu*h*ibbuunahum ka*h*ubbi **al**l*aa*hi wa**a**lla*dz*iina *aa*manuu asyaddu *h*

*Dan di antara manusia ada orang yang menyembah tuhan selain Allah sebagai tandingan, yang mereka cintai seperti mencintai Allah. Adapun orang-orang yang beriman sangat besar cintanya kepada Allah. Sekiranya orang-orang yang berbuat zalim itu melihat, keti*

2:166

# اِذْ تَبَرَّاَ الَّذِيْنَ اتُّبِعُوْا مِنَ الَّذِيْنَ اتَّبَعُوْا وَرَاَوُا الْعَذَابَ وَتَقَطَّعَتْ بِهِمُ الْاَسْبَابُ

i*dz* tabarra-a **al**la*dz*iina ittubi'uu mina **al**la*dz*iina ittaba'uu wara-awuu **a**l'a*dzaa*ba wataqa*ththh*a'at bihimu **a**l-asb*aa*b**u**

(Yaitu) ketika orang-orang yang diikuti berlepas tangan dari orang-orang yang mengikuti, dan mereka melihat azab, dan (ketika) segala hubungan antara mereka terputus.

2:167

# وَقَالَ الَّذِيْنَ اتَّبَعُوْا لَوْ اَنَّ لَنَا كَرَّةً فَنَتَبَرَّاَ مِنْهُمْ ۗ كَمَا تَبَرَّءُوْا مِنَّا ۗ كَذٰلِكَ يُرِيْهِمُ اللّٰهُ اَعْمَالَهُمْ حَسَرٰتٍ عَلَيْهِمْ ۗ وَمَا هُمْ بِخَارِجِيْنَ مِنَ النَّارِ ࣖ

waq*aa*la **al**la*dz*iina ittaba'uu law anna lan*aa* karratan fanatabarra-a minhum kam*aa* tabarrauu minn*aa* ka*dzaa*lika yuriihimu **al**l*aa*hu a'm*aa*lahum *h*asar*aa*tin 'al

Dan orang-orang yang mengikuti berkata, “Sekiranya kami mendapat kesempatan (kembali ke dunia), tentu kami akan berlepas tangan dari mereka, sebagaimana mereka berlepas tangan dari kami.” Demikianlah Allah memperlihatkan kepada mereka amal per-buatan mere

2:168

# يٰٓاَيُّهَا النَّاسُ كُلُوْا مِمَّا فِى الْاَرْضِ حَلٰلًا طَيِّبًا ۖوَّلَا تَتَّبِعُوْا خُطُوٰتِ الشَّيْطٰنِۗ اِنَّهٗ لَكُمْ عَدُوٌّ مُّبِيْنٌ

y*aa* ayyuh*aa* **al**nn*aa*su kuluu mimm*aa* fii **a**l-ar*dh*i *h*al*aa*lan *th*ayyiban wal*aa* tattabi'uu khu*th*uw*aa*ti **al**sysyay*thaa*ni inn

Wahai manusia! Makanlah dari (makanan) yang halal dan baik yang terdapat di bumi, dan janganlah kamu mengikuti langkah-langkah setan. Sungguh, setan itu musuh yang nyata bagimu.

2:169

# اِنَّمَا يَأْمُرُكُمْ بِالسُّوْۤءِ وَالْفَحْشَاۤءِ وَاَنْ تَقُوْلُوْا عَلَى اللّٰهِ مَا لَا تَعْلَمُوْنَ

innam*aa* ya/murukum bi**al**ssuu-i wa**a**lfa*h*sy*aa*-i wa-an taquuluu 'al*aa* **al**l*aa*hi m*aa* l*aa* ta'lamuun**a**

Sesungguhnya (setan) itu hanya menyuruh kamu agar berbuat jahat dan keji, dan mengatakan apa yang tidak kamu ketahui tentang Allah.

2:170

# وَاِذَا قِيْلَ لَهُمُ اتَّبِعُوْا مَآ اَنْزَلَ اللّٰهُ قَالُوْا بَلْ نَتَّبِعُ مَآ اَلْفَيْنَا عَلَيْهِ اٰبَاۤءَنَا ۗ اَوَلَوْ كَانَ اٰبَاۤؤُهُمْ لَا يَعْقِلُوْنَ شَيْـًٔا وَّلَا يَهْتَدُوْنَ

wa-i*dzaa* qiila lahumu ittabi'uu m*aa* anzala **al**l*aa*hu q*aa*luu bal nattabi'u m*aa* **a**lfayn*aa* 'alayhi *aa*b*aa*-an*aa* awa law k*aa*na *aa*b*aa*uhum l*aa*

*Dan apabila dikatakan kepada mereka, “Ikutilah apa yang telah diturunkan Allah.” Mereka menjawab, “(Tidak!) Kami mengikuti apa yang kami dapati pada nenek moyang kami (melakukannya).” Padahal, nenek moyang mereka itu tidak mengetahui apa pun, dan tidak me*

2:171

# وَمَثَلُ الَّذِيْنَ كَفَرُوْا كَمَثَلِ الَّذِيْ يَنْعِقُ بِمَا لَا يَسْمَعُ اِلَّا دُعَاۤءً وَّنِدَاۤءً ۗ صُمٌّ ۢ بُكْمٌ عُمْيٌ فَهُمْ لَا يَعْقِلُوْنَ

wamatsalu **al**la*dz*iina kafaruu kamatsali **al**la*dz*ii yan'iqu bim*aa* l*aa* yasma'u ill*aa* du'*aa*-an wanid*aa*-an *sh*ummun bukmun 'umyun fahum l*aa* ya'qiluun**a**

**Dan perumpamaan bagi (penyeru) orang yang kafir adalah seperti (penggembala) yang meneriaki (binatang) yang tidak mendengar selain panggilan dan teriakan. (Mereka) tuli, bisu dan buta, maka mereka tidak mengerti.**

2:172

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْا كُلُوْا مِنْ طَيِّبٰتِ مَا رَزَقْنٰكُمْ وَاشْكُرُوْا لِلّٰهِ اِنْ كُنْتُمْ اِيَّاهُ تَعْبُدُوْنَ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu kuluu min *th*ayyib*aa*ti m*aa* razaqn*aa*kum wa**u**sykuruu lill*aa*hi in kuntum iyy*aa*hu ta'buduun**a**

Wahai orang-orang yang beriman! Makanlah dari rezeki yang baik yang Kami berikan kepada kamu dan bersyukurlah kepada Allah, jika kamu hanya menyembah kepada-Nya.

2:173

# اِنَّمَا حَرَّمَ عَلَيْكُمُ الْمَيْتَةَ وَالدَّمَ وَلَحْمَ الْخِنْزِيْرِ وَمَآ اُهِلَّ بِهٖ لِغَيْرِ اللّٰهِ ۚ فَمَنِ اضْطُرَّ غَيْرَ بَاغٍ وَّلَا عَادٍ فَلَآ اِثْمَ عَلَيْهِ ۗ اِنَّ اللّٰهَ غَفُوْرٌ رَّحِيْمٌ

innam*aa* *h*arrama 'alaykumu **a**lmaytata wa**al**ddama wala*h*ma **a**lkhinziiri wam*aa* uhilla bihi lighayri **al**l*aa*hi famani i*dth*urra ghayra b*aa*ghin wal

Sesungguhnya Dia hanya mengharamkan atasmu bangkai, darah, daging babi, dan (daging) hewan yang disembelih dengan (menyebut nama) selain Allah. Tetapi barangsiapa terpaksa (memakannya), bukan karena menginginkannya dan tidak (pula) melampaui batas, maka t

2:174

# اِنَّ الَّذِيْنَ يَكْتُمُوْنَ مَآ اَنْزَلَ اللّٰهُ مِنَ الْكِتٰبِ وَيَشْتَرُوْنَ بِهٖ ثَمَنًا قَلِيْلًاۙ اُولٰۤىِٕكَ مَا يَأْكُلُوْنَ فِيْ بُطُوْنِهِمْ اِلَّا النَّارَ وَلَا يُكَلِّمُهُمُ اللّٰهُ يَوْمَ الْقِيٰمَةِ وَلَا يُزَكِّيْهِمْ ۚوَلَهُمْ عَذَابٌ ا

inna **al**la*dz*iina yaktumuuna m*aa* anzala **al**l*aa*hu mina **a**lkit*aa*bi wayasytaruuna bihi tsamanan qaliilan ul*aa*-ika m*aa* ya/kuluuna fii bu*th*uunihim ill*aa*

Sungguh, orang-orang yang menyembunyikan apa yang telah diturunkan Allah, yaitu Kitab, dan menjualnya dengan harga murah, mereka hanya menelan api neraka ke dalam perutnya, dan Allah tidak akan menyapa mereka pada hari Kiamat, dan tidak akan menyucikan me

2:175

# اُولٰۤىِٕكَ الَّذِيْنَ اشْتَرَوُا الضَّلٰلَةَ بِالْهُدٰى وَالْعَذَابَ بِالْمَغْفِرَةِ ۚ فَمَآ اَصْبَرَهُمْ عَلَى النَّارِ

ul*aa*-ika **al**la*dz*iina isytarawuu **al***dhdh*al*aa*lata bi**a**lhud*aa *wa**a**l'a*dzaa*ba bi**a**lmaghfirati fam*aa *a*sh*barahum 'al*aa*

Mereka itulah yang membeli kesesatan dengan petunjuk dan azab dengan ampunan. Maka alangkah beraninya mereka menentang api neraka!

2:176

# ذٰلِكَ بِاَنَّ اللّٰهَ نَزَّلَ الْكِتٰبَ بِالْحَقِّ ۗ وَاِنَّ الَّذِيْنَ اخْتَلَفُوْا فِى الْكِتٰبِ لَفِيْ شِقَاقٍۢ بَعِيْدٍ ࣖ

*dzaa*lika bi-anna **al**l*aa*ha nazzala **a**lkit*aa*ba bi**a**l*h*aqqi wa-inna **al**la*dz*iina ikhtalafuu fii **a**lkit*aa*bi lafii syiq*aa*qin ba'iid

Yang demikian itu karena Allah telah menurunkan Kitab (Al-Qur'an) dengan (membawa) kebenaran, dan sesungguhnya orang-orang yang berselisih paham tentang (kebenaran) Kitab itu, mereka dalam perpecahan yang jauh.

2:177

# ۞ لَيْسَ الْبِرَّاَنْ تُوَلُّوْا وُجُوْهَكُمْ قِبَلَ الْمَشْرِقِ وَالْمَغْرِبِ وَلٰكِنَّ الْبِرَّ مَنْ اٰمَنَ بِاللّٰهِ وَالْيَوْمِ الْاٰخِرِ وَالْمَلٰۤىِٕكَةِ وَالْكِتٰبِ وَالنَّبِيّٖنَ ۚ وَاٰتَى الْمَالَ عَلٰى حُبِّهٖ ذَوِى الْقُرْبٰى وَالْيَتٰمٰى وَالْ

laysa **a**lbirra an tuwalluu wujuuhakum qibala **a**lmasyriqi wa**a**lmaghribi wal*aa*kinna **a**lbirra man *aa*mana bi**al**l*aa*hi wa**a**lyawmi **a**

**Kebajikan itu bukanlah menghadapkan wajahmu ke arah timur dan ke barat, tetapi kebajikan itu ialah (kebajikan) orang yang beriman kepada Allah, hari akhir, malaikat-malaikat, kitab-kitab, dan nabi-nabi dan memberikan harta yang dicintainya kepada kerabat,**

2:178

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْا كُتِبَ عَلَيْكُمُ الْقِصَاصُ فِى الْقَتْلٰىۗ اَلْحُرُّ بِالْحُرِّ وَالْعَبْدُ بِالْعَبْدِ وَالْاُنْثٰى بِالْاُنْثٰىۗ فَمَنْ عُفِيَ لَهٗ مِنْ اَخِيْهِ شَيْءٌ فَاتِّبَاعٌ ۢبِالْمَعْرُوْفِ وَاَدَاۤءٌ اِلَيْهِ بِاِحْسَانٍ ۗ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu kutiba 'alaykumu **a**lqi*sas*u fii **a**lqatl*aa* **a**l*h*urru bi**a**l*h*urri wa**a**l'abdu

Wahai orang-orang yang beriman! Diwajibkan atas kamu (melaksanakan) qisas berkenaan dengan orang yang dibunuh. Orang merdeka dengan orang merdeka, hamba sahaya dengan hamba sahaya, perempuan dengan perempuan. Tetapi barangsiapa memperoleh maaf dari saudar

2:179

# وَلَكُمْ فِى الْقِصَاصِ حَيٰوةٌ يّٰٓاُولِى الْاَلْبَابِ لَعَلَّكُمْ تَتَّقُوْنَ

walakum fii **a**lqi*sas*i *h*ay*aa*tun y*aa* ulii **a**l-alb*aa*bi la'allakum tattaquun**a**

Dan dalam qisas itu ada (jaminan) kehidupan bagimu, wahai orang-orang yang berakal, agar kamu bertakwa.

2:180

# كُتِبَ عَلَيْكُمْ اِذَا حَضَرَ اَحَدَكُمُ الْمَوْتُ اِنْ تَرَكَ خَيْرًا ۖ ۨالْوَصِيَّةُ لِلْوَالِدَيْنِ وَالْاَقْرَبِيْنَ بِالْمَعْرُوْفِۚ حَقًّا عَلَى الْمُتَّقِيْنَ ۗ

kutiba 'alaykum i*dzaa* *h*a*dh*ara a*h*adakumu **a**lmawtu in taraka khayran **a**lwa*sh*iyyatu lilw*aa*lidayni wa**a**l-aqrabiina bi**a**lma'ruufi *h*aqqan 'al*aa*

*Diwajibkan atas kamu, apabila maut hendak menjemput seseorang di antara kamu, jika dia meninggalkan harta, berwasiat untuk kedua orang tua dan karib kerabat dengan cara yang baik, (sebagai) kewajiban bagi orang-orang yang bertakwa.*

2:181

# فَمَنْۢ بَدَّلَهٗ بَعْدَمَا سَمِعَهٗ فَاِنَّمَآ اِثْمُهٗ عَلَى الَّذِيْنَ يُبَدِّلُوْنَهٗ ۗ اِنَّ اللّٰهَ سَمِيْعٌ عَلِيْمٌ ۗ

faman baddalahu ba'da m*aa* sami'ahu fa-innam*aa* itsmuhu 'al*aa* **al**la*dz*iina yubaddiluunahu inna **al**l*aa*ha samii'un 'aliim**un**

Barangsiapa mengubahnya (wasiat itu), setelah mendengarnya, maka sesungguhnya dosanya hanya bagi orang yang mengubahnya. Sungguh, Allah Maha Mendengar, Maha Mengetahui.

2:182

# فَمَنْ خَافَ مِنْ مُّوْصٍ جَنَفًا اَوْ اِثْمًا فَاَصْلَحَ بَيْنَهُمْ فَلَآ اِثْمَ عَلَيْهِ ۗ اِنَّ اللّٰهَ غَفُوْرٌ رَّحِيْمٌ ࣖ

faman kh*aa*fa min muu*sh*in janafan aw itsman fa-a*sh*la*h*a baynahum fal*aa* itsma 'alayhi inna **al**l*aa*ha ghafuurun ra*h*iim**un**

Tetapi barangsiapa khawatir bahwa pemberi wasiat (berlaku) berat sebelah atau berbuat salah, lalu dia mendamaikan antara mereka, maka dia tidak berdosa. Sungguh, Allah Maha Pengampun, Maha Penyayang.

2:183

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْا كُتِبَ عَلَيْكُمُ الصِّيَامُ كَمَا كُتِبَ عَلَى الَّذِيْنَ مِنْ قَبْلِكُمْ لَعَلَّكُمْ تَتَّقُوْنَۙ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu kutiba 'alaykumu **al***shsh*iy*aa*mu kam*aa *kutiba 'al*aa* **al**la*dz*iina min qablikum la'allakum tattaquun**a**

**Wahai orang-orang yang beriman! Diwajibkan atas kamu berpuasa sebagaimana diwajibkan atas orang sebelum kamu agar kamu bertakwa,**

2:184

# اَيَّامًا مَّعْدُوْدٰتٍۗ فَمَنْ كَانَ مِنْكُمْ مَّرِيْضًا اَوْ عَلٰى سَفَرٍ فَعِدَّةٌ مِّنْ اَيَّامٍ اُخَرَ ۗ وَعَلَى الَّذِيْنَ يُطِيْقُوْنَهٗ فِدْيَةٌ طَعَامُ مِسْكِيْنٍۗ فَمَنْ تَطَوَّعَ خَيْرًا فَهُوَ خَيْرٌ لَّهٗ ۗ وَاَنْ تَصُوْمُوْا خَيْرٌ لَّكُمْ

ayy*aa*man ma'duud*aa*tin faman k*aa*na minkum marii*dh*an aw 'al*aa* safarin fa'iddatun min ayy*aa*min ukhara wa'al*aa* **al**la*dz*iina yu*th*iiquunahu fidyatun *th*a'*aa*mu miskiinin f

(Yaitu) beberapa hari tertentu. Maka barangsiapa di antara kamu sakit atau dalam perjalanan (lalu tidak berpuasa), maka (wajib mengganti) sebanyak hari (yang dia tidak berpuasa itu) pada hari-hari yang lain. Dan bagi orang yang berat menjalankannya, wajib

2:185

# شَهْرُ رَمَضَانَ الَّذِيْٓ اُنْزِلَ فِيْهِ الْقُرْاٰنُ هُدًى لِّلنَّاسِ وَبَيِّنٰتٍ مِّنَ الْهُدٰى وَالْفُرْقَانِۚ فَمَنْ شَهِدَ مِنْكُمُ الشَّهْرَ فَلْيَصُمْهُ ۗ وَمَنْ كَانَ مَرِيْضًا اَوْ عَلٰى سَفَرٍ فَعِدَّةٌ مِّنْ اَيَّامٍ اُخَرَ ۗ يُرِيْدُ اللّٰهُ

syahru rama*daa*na **al**la*dz*ii unzila fiihi **a**lqur-*aa*nu hudan li**l**nn*aa*si wabayyin*aa*tin mina **a**lhud*aa* wa**a**lfurq*aa*ni faman syahida m

Bulan Ramadan adalah (bulan) yang di dalamnya diturunkan Al-Qur'an, sebagai petunjuk bagi manusia dan penjelasan-penjelasan mengenai petunjuk itu dan pembeda (antara yang benar dan yang batil). Karena itu, barangsiapa di antara kamu ada di bulan itu, maka

2:186

# وَاِذَا سَاَلَكَ عِبَادِيْ عَنِّيْ فَاِنِّيْ قَرِيْبٌ ۗ اُجِيْبُ دَعْوَةَ الدَّاعِ اِذَا دَعَانِۙ فَلْيَسْتَجِيْبُوْا لِيْ وَلْيُؤْمِنُوْا بِيْ لَعَلَّهُمْ يَرْشُدُوْنَ

wa-i*dzaa* sa-alaka 'ib*aa*dii 'annii fa-innii qariibun ujiibu da'wata **al**dd*aa*'i i*dzaa* da'*aa*ni falyastajiibuu lii walyu/minuu bii la'allahum yarsyuduun**a**

Dan apabila hamba-hamba-Ku bertanya kepadamu (Muhammad) tentang Aku, maka sesungguhnya Aku dekat. Aku Kabulkan permohonan orang yang berdoa apabila dia berdoa kepada-Ku. Hendaklah mereka itu memenuhi (perintah)-Ku dan beriman kepada-Ku, agar mereka memper

2:187

# اُحِلَّ لَكُمْ لَيْلَةَ الصِّيَامِ الرَّفَثُ اِلٰى نِسَاۤىِٕكُمْ ۗ هُنَّ لِبَاسٌ لَّكُمْ وَاَنْتُمْ لِبَاسٌ لَّهُنَّ ۗ عَلِمَ اللّٰهُ اَنَّكُمْ كُنْتُمْ تَخْتَانُوْنَ اَنْفُسَكُمْ فَتَابَ عَلَيْكُمْ وَعَفَا عَنْكُمْ ۚ فَالْـٰٔنَ بَاشِرُوْهُنَّ وَابْتَغُوْ

u*h*illa lakum laylata **al***shsh*iy*aa*mi **al**rrafatsu il*aa *nis*aa*\-ikum hunna lib*aa*sun lakum wa-antum lib*aa*sun lahunna 'alima **al**l*aa*hu annakum kuntum takht*aa*

Dihalalkan bagimu pada malam hari puasa bercampur dengan istrimu. Mereka adalah pakaian bagimu, dan kamu adalah pakaian bagi mereka. Allah mengetahui bahwa kamu tidak dapat menahan dirimu sendiri, tetapi Dia menerima tobatmu dan memaafkan kamu. Maka sekar

2:188

# وَلَا تَأْكُلُوْٓا اَمْوَالَكُمْ بَيْنَكُمْ بِالْبَاطِلِ وَتُدْلُوْا بِهَآ اِلَى الْحُكَّامِ لِتَأْكُلُوْا فَرِيْقًا مِّنْ اَمْوَالِ النَّاسِ بِالْاِثْمِ وَاَنْتُمْ تَعْلَمُوْنَ ࣖ

wal*aa* ta/kuluu amw*aa*lakum baynakum bi**a**lb*aath*ili watudluu bih*aa* il*aa* **a**l*h*ukk*aa*mi lita/kuluu fariiqan min amw*aa*li **al**nn*aa*si bi**a**l

Dan janganlah kamu makan harta di antara kamu dengan jalan yang batil, dan (janganlah) kamu menyuap dengan harta itu kepada para hakim, dengan maksud agar kamu dapat memakan sebagian harta orang lain itu dengan jalan dosa, padahal kamu mengetahui.

2:189

# ۞ يَسـَٔلُوْنَكَ عَنِ الْاَهِلَّةِ ۗ قُلْ هِيَ مَوَاقِيْتُ لِلنَّاسِ وَالْحَجِّ ۗ وَلَيْسَ الْبِرُّ بِاَنْ تَأْتُوا الْبُيُوْتَ مِنْ ظُهُوْرِهَا وَلٰكِنَّ الْبِرَّ مَنِ اتَّقٰىۚ وَأْتُوا الْبُيُوْتَ مِنْ اَبْوَابِهَا ۖ وَاتَّقُوا اللّٰهَ لَعَلَّكُمْ تُفْل

yas-aluunaka 'ani **a**l-ahillati qul hiya maw*aa*qiitu li**l**nn*aa*si wa**a**l*h*ajji walaysa **a**lbirru bi-an ta/tuu **a**lbuyuuta min *zh*uhuurih*aa* wal*aa*

*Mereka bertanya kepadamu (Muhammad) tentang bulan sabit. Katakanlah, “Itu adalah (penunjuk) waktu bagi manusia dan (ibadah) haji.” Dan bukanlah suatu kebajikan memasuki rumah dari atasnya, tetapi kebajikan adalah (kebajikan) orang yang bertakwa. Masukilah*

2:190

# وَقَاتِلُوْا فِيْ سَبِيْلِ اللّٰهِ الَّذِيْنَ يُقَاتِلُوْنَكُمْ وَلَا تَعْتَدُوْا ۗ اِنَّ اللّٰهَ لَا يُحِبُّ الْمُعْتَدِيْنَ

waq*aa*tiluu fii sabiili **al**l*aa*hi **al**la*dz*iina yuq*aa*tiluunakum wal*aa* ta'taduu inna **al**l*aa*ha l*aa* yu*h*ibbu **a**lmu'tadiin**a**

Dan perangilah di jalan Allah orang-orang yang memerangi kamu, tetapi jangan melampaui batas. Sungguh, Allah tidak menyukai orang-orang yang melampaui batas.

2:191

# وَاقْتُلُوْهُمْ حَيْثُ ثَقِفْتُمُوْهُمْ وَاَخْرِجُوْهُمْ مِّنْ حَيْثُ اَخْرَجُوْكُمْ وَالْفِتْنَةُ اَشَدُّ مِنَ الْقَتْلِ ۚ وَلَا تُقَاتِلُوْهُمْ عِنْدَ الْمَسْجِدِ الْحَرَامِ حَتّٰى يُقٰتِلُوْكُمْ فِيْهِۚ فَاِنْ قٰتَلُوْكُمْ فَاقْتُلُوْهُمْۗ كَذٰلِكَ جَ

wa**u**qtuluuhum *h*aytsu tsaqiftumuuhum wa-akhrijuuhum min *h*aytsu akhrajuukum wa**a**lfitnatu asyaddu mina **a**lqatli wal*aa* tuq*aa*tiluuhum 'inda **a**lmasjidi **a**

**Dan bunuhlah mereka di mana kamu temui mereka, dan usirlah mereka dari mana mereka telah mengusir kamu. Dan fitnah itu lebih kejam daripada pembunuhan. Dan janganlah kamu perangi mereka di Masjidilharam, kecuali jika mereka memerangi kamu di tempat itu. J**

2:192

# فَاِنِ انْتَهَوْا فَاِنَّ اللّٰهَ غَفُوْرٌ رَّحِيْمٌ

fa-ini intahaw fa-inna **al**l*aa*ha ghafuurun ra*h*iim**un**

Tetapi jika mereka berhenti, maka sungguh, Allah Maha Pengampun, Maha Penyayang.

2:193

# وَقٰتِلُوْهُمْ حَتّٰى لَا تَكُوْنَ فِتْنَةٌ وَّيَكُوْنَ الدِّيْنُ لِلّٰهِ ۗ فَاِنِ انْتَهَوْا فَلَا عُدْوَانَ اِلَّا عَلَى الظّٰلِمِيْنَ

waq*aa*tiluuhum *h*att*aa* l*aa* takuuna fitnatun wayakuuna **al**ddiinu lill*aa*hi fa-ini intahaw fal*aa* 'udw*aa*na ill*aa* 'al*aa* **al***zhzhaa*limiin**a**

Dan perangilah mereka itu sampai tidak ada lagi fitnah, dan agama hanya bagi Allah semata. Jika mereka berhenti, maka tidak ada (lagi) permusuhan, kecuali terhadap orang-orang zalim.

2:194

# اَلشَّهْرُ الْحَرَامُ بِالشَّهْرِ الْحَرَامِ وَالْحُرُمٰتُ قِصَاصٌۗ فَمَنِ اعْتَدٰى عَلَيْكُمْ فَاعْتَدُوْا عَلَيْهِ بِمِثْلِ مَا اعْتَدٰى عَلَيْكُمْ ۖ وَاتَّقُوا اللّٰهَ وَاعْلَمُوْٓا اَنَّ اللّٰهَ مَعَ الْمُتَّقِيْنَ

a**l**sysyahru **a**l*h*ar*aa*mu bi**al**sysyahri **a**l*h*ar*aa*mi wa**a**l*h*urum*aa*tu qi*sas*un famani i'tad*aa* 'alaykum fa**i**'tad

Bulan haram dengan bulan haram, dan (terhadap) sesuatu yang dihormati berlaku (hukum) qisas. Oleh sebab itu barangsiapa menyerang kamu, maka seranglah dia setimpal dengan serangannya terhadap kamu. Bertakwalah kepada Allah dan ketahuilah bahwa Allah beser

2:195

# وَاَنْفِقُوْا فِيْ سَبِيْلِ اللّٰهِ وَلَا تُلْقُوْا بِاَيْدِيْكُمْ اِلَى التَّهْلُكَةِ ۛ وَاَحْسِنُوْا ۛ اِنَّ اللّٰهَ يُحِبُّ الْمُحْسِنِيْنَ

wa-anfiquu fii sabiili **al**l*aa*hi wal*aa* tulquu bi-aydiikum il*aa* **al**ttahlukati wa-a*h*sinuu inna **al**l*aa*ha yu*h*ibbu **a**lmu*h*siniin**a**

Dan infakkanlah (hartamu) di jalan Allah, dan janganlah kamu jatuhkan (diri sendiri) ke dalam kebinasaan dengan tangan sendiri, dan berbuatbaiklah. Sungguh, Allah menyukai orang-orang yang berbuat baik.

2:196

# وَاَتِمُّوا الْحَجَّ وَالْعُمْرَةَ لِلّٰهِ ۗ فَاِنْ اُحْصِرْتُمْ فَمَا اسْتَيْسَرَ مِنَ الْهَدْيِۚ وَلَا تَحْلِقُوْا رُءُوْسَكُمْ حَتّٰى يَبْلُغَ الْهَدْيُ مَحِلَّهٗ ۗ فَمَنْ كَانَ مِنْكُمْ مَّرِيْضًا اَوْ بِهٖٓ اَذًى مِّنْ رَّأْسِهٖ فَفِدْيَةٌ مِّنْ صِيَ

wa-atimmuu **a**l*h*ajja wa**a**l'umrata lill*aa*hi fa-in u*hs*irtum fam*aa* istaysara mina **a**lhadyi wal*aa* ta*h*liquu ruuusakum *h*att*aa* yablugha **a**lhady

Dan sempurnakanlah ibadah haji dan umrah karena Allah. Tetapi jika kamu terkepung (oleh musuh), maka (sembelihlah) hadyu yang mudah didapat, dan jangan kamu mencukur kepalamu, sebelum hadyu sampai di tempat penyembelihannya. Jika ada di antara kamu yang s

2:197

# اَلْحَجُّ اَشْهُرٌ مَّعْلُوْمٰتٌ ۚ فَمَنْ فَرَضَ فِيْهِنَّ الْحَجَّ فَلَا رَفَثَ وَلَا فُسُوْقَ وَلَا جِدَالَ فِى الْحَجِّ ۗ وَمَا تَفْعَلُوْا مِنْ خَيْرٍ يَّعْلَمْهُ اللّٰهُ ۗ وَتَزَوَّدُوْا فَاِنَّ خَيْرَ الزَّادِ التَّقْوٰىۖ وَاتَّقُوْنِ يٰٓاُولِى الْا

al*h*ajju asyhurun ma'luum*aa*tun faman fara*dh*a fiihinna **a**l*h*ajja fal*aa* rafatsa wal*aa* fusuuqa wal*aa* jid*aa*la fii **a**l*h*ajji wam*aa* taf'aluu min khayrin ya'lamhu

(Musim) haji itu (pada) bulan-bulan yang telah dimaklumi. Barangsiapa mengerjakan (ibadah) haji dalam (bulan-bulan) itu, maka janganlah dia berkata jorok (rafats), berbuat maksiat dan bertengkar dalam (melakukan ibadah) haji. Segala yang baik yang kamu ke

2:198

# لَيْسَ عَلَيْكُمْ جُنَاحٌ اَنْ تَبْتَغُوْا فَضْلًا مِّنْ رَّبِّكُمْ ۗ فَاِذَآ اَفَضْتُمْ مِّنْ عَرَفَاتٍ فَاذْكُرُوا اللّٰهَ عِنْدَ الْمَشْعَرِ الْحَرَامِ ۖ وَاذْكُرُوْهُ كَمَا هَدٰىكُمْ ۚ وَاِنْ كُنْتُمْ مِّنْ قَبْلِهٖ لَمِنَ الضَّاۤلِّيْنَ

laysa 'alaykum jun*aah*un an tabtaghuu fa*dh*lan min rabbikum fa-i*dzaa* afa*dh*tum min 'araf*aa*tin fa**u***dz*kuruu **al**l*aa*ha 'inda **a**lmasy'ari **a**l*h*a

Bukanlah suatu dosa bagimu mencari karunia dari Tuhanmu. Maka apabila kamu bertolak dari Arafah, berzikirlah kepada Allah di Masy’arilharam. Dan berzikirlah kepada-Nya sebagaimana Dia telah memberi petunjuk kepadamu, sekalipun sebelumnya kamu benar-benar

2:199

# ثُمَّ اَفِيْضُوْا مِنْ حَيْثُ اَفَاضَ النَّاسُ وَاسْتَغْفِرُوا اللّٰهَ ۗ اِنَّ اللّٰهَ غَفُوْرٌ رَّحِيْمٌ

tsumma afii*dh*uu min *h*aytsu af*aad*a **al**nn*aa*su wa**i**staghfiruu **al**l*aa*ha inna **al**l*aa*ha ghafuurun ra*h*iim**un**

Kemudian bertolaklah kamu dari tempat orang banyak bertolak (Arafah) dan mohonlah ampunan kepada Allah. Sungguh, Allah Maha Pengampun, Maha Penyayang.

2:200

# فَاِذَا قَضَيْتُمْ مَّنَاسِكَكُمْ فَاذْكُرُوا اللّٰهَ كَذِكْرِكُمْ اٰبَاۤءَكُمْ اَوْ اَشَدَّ ذِكْرًا ۗ فَمِنَ النَّاسِ مَنْ يَّقُوْلُ رَبَّنَآ اٰتِنَا فِى الدُّنْيَا وَمَا لَهٗ فِى الْاٰخِرَةِ مِنْ خَلَاقٍ

fa-i*dzaa* qa*dh*aytum man*aa*sikakum fa**u***dz*kuruu **al**l*aa*ha ka*dz*ikrikum* aa*b*aa*\-akum aw asyadda* dz*ikran famina **al**nn*aa*si man yaquulu rabban*aa*

*Apabila kamu telah menyelesaikan ibadah haji, maka berzikirlah kepada Allah, sebagaimana kamu menyebut-nyebut nenek moyang kamu, bahkan berzikirlah lebih dari itu. Maka di antara manusia ada yang berdoa, “Ya Tuhan kami, berilah kami (kebaikan) di dunia,”*

2:201

# وَمِنْهُمْ مَّنْ يَّقُوْلُ رَبَّنَآ اٰتِنَا فِى الدُّنْيَا حَسَنَةً وَّفِى الْاٰخِرَةِ حَسَنَةً وَّقِنَا عَذَابَ النَّارِ

waminhum man yaquulu rabban*aa* *aa*tin*aa* fii **al**dduny*aa* *h*asanatan wafii **a**l-*aa*khirati *h*asanatan waqin*aa* 'a*dzaa*ba **al**nn*aa*r**i**

Dan di antara mereka ada yang berdoa, “Ya Tuhan kami, berilah kami kebaikan di dunia dan kebaikan di akhirat, dan lindungilah kami dari azab neraka.”

2:202

# اُولٰۤىِٕكَ لَهُمْ نَصِيْبٌ مِّمَّا كَسَبُوْا ۗ وَاللّٰهُ سَرِيْعُ الْحِسَابِ

ul*aa*-ika lahum na*sh*iibun mimm*aa* kasabuu wa**al**l*aa*hu sarii'u **a**l*h*is*aa*b**i**

Mereka itulah yang memperoleh bagian dari apa yang telah mereka kerjakan, dan Allah Mahacepat perhitungan-Nya.

2:203

# ۞ وَاذْكُرُوا اللّٰهَ فِيْٓ اَيَّامٍ مَّعْدُوْدٰتٍ ۗ فَمَنْ تَعَجَّلَ فِيْ يَوْمَيْنِ فَلَآ اِثْمَ عَلَيْهِ ۚوَمَنْ تَاَخَّرَ فَلَآ اِثْمَ عَلَيْهِۙ لِمَنِ اتَّقٰىۗ وَاتَّقُوا اللّٰهَ وَاعْلَمُوْٓا اَنَّكُمْ اِلَيْهِ تُحْشَرُوْنَ

wa**u***dz*kuruu **al**l*aa*ha fii ayy*aa*min ma'duud*aa*tin faman ta'ajjala fii yawmayni fal*aa *itsma 'alayhi waman ta-akhkhara fal*aa *itsma 'alayhi limani ittaq*aa* wa**i**ttaqu

Dan berzikirlah kepada Allah pada hari yang telah ditentukan jumlahnya. Barangsiapa mempercepat (meninggalkan Mina) setelah dua hari, maka tidak ada dosa baginya. Dan barangsiapa mengakhirkannya tidak ada dosa (pula) baginya, (yakni) bagi orang yang berta

2:204

# وَمِنَ النَّاسِ مَنْ يُّعْجِبُكَ قَوْلُهٗ فِى الْحَيٰوةِ الدُّنْيَا وَيُشْهِدُ اللّٰهَ عَلٰى مَا فِيْ قَلْبِهٖ ۙ وَهُوَ اَلَدُّ الْخِصَامِ

wamina **al**nn*aa*si man yu'jibuka qawluhu fii al*h*ay*aa*ti **al**dduny*aa* wayusyhidu **al**l*aa*ha 'al*aa* m*aa* fii qalbihi wahuwa aladdu alkhi*shaa*m**i**

Dan di antara manusia ada yang pembicaraannya tentang kehidupan dunia mengagumkan engkau (Muhammad), dan dia bersaksi kepada Allah mengenai isi hatinya, padahal dia adalah penentang yang paling keras.

2:205

# وَاِذَا تَوَلّٰى سَعٰى فِى الْاَرْضِ لِيُفْسِدَ فِيْهَا وَيُهْلِكَ الْحَرْثَ وَالنَّسْلَ ۗ وَ اللّٰهُ لَا يُحِبُّ الْفَسَادَ

wa-i*dzaa* tawall*aa* sa'*aa* fii **a**l-ar*dh*i liyufsida fiih*aa* wayuhlika **a**l*h*artsa wa**al**nnasla wa**al**l*aa*hu l*aa* yu*h*ibbu **a**

Dan apabila dia berpaling (dari engkau), dia berusaha untuk berbuat kerusakan di bumi, serta merusak tanam-tanaman dan ternak, sedang Allah tidak menyukai kerusakan.

2:206

# وَاِذَا قِيْلَ لَهُ اتَّقِ اللّٰهَ اَخَذَتْهُ الْعِزَّةُ بِالْاِثْمِ فَحَسْبُهٗ جَهَنَّمُ ۗ وَلَبِئْسَ الْمِهَادُ

wa-i*dzaa* qiila lahu ittaqi **al**l*aa*ha akha*dz*at-hu **a**l'izzatu bi**a**l-itsmi fa*h*asbuhu jahannamu walabi/sa **a**lmih*aa*d**u**

Dan apabila dikatakan kepadanya, “Bertakwalah kepada Allah,” bangkitlah kesombongannya untuk berbuat dosa. Maka pantaslah baginya neraka Jahanam, dan sungguh (Jahanam itu) tempat tinggal yang terburuk.

2:207

# وَمِنَ النَّاسِ مَنْ يَّشْرِيْ نَفْسَهُ ابْتِغَاۤءَ مَرْضَاتِ اللّٰهِ ۗوَاللّٰهُ رَءُوْفٌۢ بِالْعِبَادِ

wamina **al**nn*aa*si man yasyrii nafsahu ibtigh*aa*-a mar*daa*ti **al**l*aa*hi wa**al**l*aa*hu rauufun bi**a**l'ib*aa*d**i**

Dan di antara manusia ada orang yang mengorbankan dirinya untuk mencari keridaan Allah. Dan Allah Maha Penyantun kepada hamba-hamba-Nya.

2:208

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوا ادْخُلُوْا فِى السِّلْمِ كَاۤفَّةً ۖوَّلَا تَتَّبِعُوْا خُطُوٰتِ الشَّيْطٰنِۗ اِنَّهٗ لَكُمْ عَدُوٌّ مُّبِيْنٌ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu udkhuluu fii **al**ssilmi k*aa*ffatan wal*aa* tattabi'uu khu*th*uw*aa*ti **al**sysyay*thaa*ni innahu lakum 'aduwwun mubii

Wahai orang-orang yang beriman! Masuklah ke dalam Islam secara keseluruhan, dan janganlah kamu ikuti langkah-langkah setan. Sungguh, ia musuh yang nyata bagimu.

2:209

# فَاِنْ زَلَلْتُمْ مِّنْۢ بَعْدِ مَا جَاۤءَتْكُمُ الْبَيِّنٰتُ فَاعْلَمُوْٓا اَنَّ اللّٰهَ عَزِيْزٌ حَكِيْمٌ

fa-in zalaltum min ba'di m*aa* j*aa*-atkumu **a**lbayyin*aa*tu fa**i**'lamuu anna **al**l*aa*ha 'aziizun *h*akiim**un**

Tetapi jika kamu tergelincir setelah bukti-bukti yang nyata sampai kepadamu, ketahuilah bahwa Allah Maha-perkasa, Mahabijaksana.

2:210

# هَلْ يَنْظُرُوْنَ اِلَّآ اَنْ يَّأْتِيَهُمُ اللّٰهُ فِيْ ظُلَلٍ مِّنَ الْغَمَامِ وَالْمَلٰۤىِٕكَةُ وَقُضِيَ الْاَمْرُ ۗ وَاِلَى اللّٰهِ تُرْجَعُ الْاُمُوْرُ ࣖ

hal yan*zh*uruuna ill*aa* an ya/tiyahumu **al**l*aa*hu fii *zh*ulalin mina **a**lgham*aa*mi wa**a**lmal*aa*-ikatu waqu*dh*iya **a**l-amru wa-il*aa* **al**

**Tidak ada yang mereka tunggu-tunggu kecuali datangnya (azab) Allah bersama malaikat dalam naungan awan, sedangkan perkara (mereka) telah diputuskan. Dan kepada Allah-lah segala perkara dikembalikan.**

2:211

# سَلْ بَنِيْٓ اِسْرَاۤءِيْلَ كَمْ اٰتَيْنٰهُمْ مِّنْ اٰيَةٍ ۢ بَيِّنَةٍ ۗ وَمَنْ يُّبَدِّلْ نِعْمَةَ اللّٰهِ مِنْۢ بَعْدِ مَا جَاۤءَتْهُ فَاِنَّ اللّٰهَ شَدِيْدُ الْعِقَابِ

sal banii isr*aa*-iila kam *aa*tayn*aa*hum min *aa*yatin bayyinatin waman yubaddil ni'mata **al**l*aa*hi min ba'di m*aa* j*aa*-at-hu fa-inna **al**l*aa*ha syadiidu **a**l'iq

Tanyakanlah kepada Bani Israil, berapa banyak bukti nyata yang telah Kami berikan kepada mereka. Barangsiapa menukar nikmat Allah setelah (nikmat itu) datang kepadanya, maka sungguh, Allah sangat keras hukuman-Nya.

2:212

# زُيِّنَ لِلَّذِيْنَ كَفَرُوا الْحَيٰوةُ الدُّنْيَا وَيَسْخَرُوْنَ مِنَ الَّذِيْنَ اٰمَنُوْا ۘ وَالَّذِيْنَ اتَّقَوْا فَوْقَهُمْ يَوْمَ الْقِيٰمَةِ ۗ وَاللّٰهُ يَرْزُقُ مَنْ يَّشَاۤءُ بِغَيْرِ حِسَابٍ

zuyyina lilla*dz*iina kafaruu **a**l*h*ay*aa*tu **al**dduny*aa* wayaskharuuna mina **al**la*dz*iina *aa*manuu wa**a**lla*dz*iina ittaqaw fawqahum yawma **a**

**Kehidupan dunia dijadikan terasa indah dalam pandangan orang-orang yang kafir, dan mereka menghina orang-orang yang beriman. Padahal orang-orang yang bertakwa itu berada di atas mereka pada hari Kiamat. Dan Allah memberi rezeki kepada orang yang Dia kehen**

2:213

# كَانَ النَّاسُ اُمَّةً وَّاحِدَةً ۗ فَبَعَثَ اللّٰهُ النَّبِيّٖنَ مُبَشِّرِيْنَ وَمُنْذِرِيْنَ ۖ وَاَنْزَلَ مَعَهُمُ الْكِتٰبَ بِالْحَقِّ لِيَحْكُمَ بَيْنَ النَّاسِ فِيْمَا اخْتَلَفُوْا فِيْهِ ۗ وَمَا اخْتَلَفَ فِيْهِ اِلَّا الَّذِيْنَ اُوْتُوْهُ مِنْۢ بَ

k*aa*na **al**nn*aa*su ummatan w*aah*idatan faba'atsa **al**l*aa*hu **al**nnabiyyiina mubasysyiriina wamun*dz*iriina wa-anzala ma'ahumu **a**lkit*aa*ba bi**a**l

Manusia itu (dahulunya) satu umat. Lalu Allah mengutus para nabi (untuk) menyampaikan kabar gembira dan peringatan. Dan diturunkan-Nya bersama mereka Kitab yang mengandung kebenaran, untuk memberi keputusan di antara manusia tentang perkara yang mereka pe

2:214

# اَمْ حَسِبْتُمْ اَنْ تَدْخُلُوا الْجَنَّةَ وَلَمَّا يَأْتِكُمْ مَّثَلُ الَّذِيْنَ خَلَوْا مِنْ قَبْلِكُمْ ۗ مَسَّتْهُمُ الْبَأْسَاۤءُ وَالضَّرَّاۤءُ وَزُلْزِلُوْا حَتّٰى يَقُوْلَ الرَّسُوْلُ وَالَّذِيْنَ اٰمَنُوْا مَعَهٗ مَتٰى نَصْرُ اللّٰهِ ۗ اَلَآ اِنّ

am *h*asibtum an tadkhuluu **a**ljannata walamm*aa* ya/tikum matsalu **al**la*dz*iina khalaw min qablikum massat-humu **a**lba/s*aa*u wa**al***dhdh*arr*aa*u wazulziluu* h<*

Ataukah kamu mengira bahwa kamu akan masuk surga, padahal belum datang kepadamu (cobaan) seperti (yang dialami) orang-orang terdahulu sebelum kamu. Mereka ditimpa kemelaratan, penderitaan dan diguncang (dengan berbagai cobaan), sehingga Rasul dan orang-or

2:215

# يَسْـَٔلُوْنَكَ مَاذَا يُنْفِقُوْنَ ۗ قُلْ مَآ اَنْفَقْتُمْ مِّنْ خَيْرٍ فَلِلْوَالِدَيْنِ وَالْاَقْرَبِيْنَ وَالْيَتٰمٰى وَالْمَسٰكِيْنِ وَابْنِ السَّبِيْلِ ۗ وَمَا تَفْعَلُوْا مِنْ خَيْرٍ فَاِنَّ اللّٰهَ بِهٖ عَلِيْمٌ

yas-aluunaka m*aatsaa* yunfiquuna qul m*aa* anfaqtum min khayrin falilw*aa*lidayni wa**a**l-aqrabiina wa**a**lyat*aa*m*aa* wa**a**lmas*aa*kiini wa**i**bni **al**

**Mereka bertanya kepadamu (Muhammad) tentang apa yang harus mereka infakkan. Katakanlah, “Harta apa saja yang kamu infakkan, hendaknya diperuntukkan bagi kedua orang tua, kerabat, anak yatim, orang miskin dan orang yang dalam perjalanan.” Dan kebaikan apa**

2:216

# كُتِبَ عَلَيْكُمُ الْقِتَالُ وَهُوَ كُرْهٌ لَّكُمْ ۚ وَعَسٰٓى اَنْ تَكْرَهُوْا شَيْـًٔا وَّهُوَ خَيْرٌ لَّكُمْ ۚ وَعَسٰٓى اَنْ تُحِبُّوْا شَيْـًٔا وَّهُوَ شَرٌّ لَّكُمْ ۗ وَاللّٰهُ يَعْلَمُ وَاَنْتُمْ لَا تَعْلَمُوْنَ ࣖ

kutiba 'alaykumu **a**lqit*aa*lu wahuwa kurhun lakum wa'as*aa* an takrahuu syay-an wahuwa khayrun lakum wa'as*aa* an tu*h*ibbuu syay-an wahuwa syarrun lakum wa**al**l*aa*hu ya'lamu wa-antum l*aa* ta'

Diwajibkan atas kamu berperang, padahal itu tidak menyenangkan bagimu. Tetapi boleh jadi kamu tidak menyenangi sesuatu, padahal itu baik bagimu, dan boleh jadi kamu menyukai sesuatu, padahal itu tidak baik bagimu. Allah mengetahui, sedang kamu tidak menge

2:217

# يَسْـَٔلُوْنَكَ عَنِ الشَّهْرِ الْحَرَامِ قِتَالٍ فِيْهِۗ قُلْ قِتَالٌ فِيْهِ كَبِيْرٌ ۗ وَصَدٌّ عَنْ سَبِيْلِ اللّٰهِ وَكُفْرٌۢ بِهٖ وَالْمَسْجِدِ الْحَرَامِ وَاِخْرَاجُ اَهْلِهٖ مِنْهُ اَكْبَرُ عِنْدَ اللّٰهِ ۚ وَالْفِتْنَةُ اَكْبَرُ مِنَ الْقَتْلِ ۗ و

yas-aluunaka 'ani **al**sysyahri **a**l*h*ar*aa*mi qit*aa*lin fiihi qul qit*aa*lun fiihi kabiirun wa*sh*addun 'an sabiili **al**l*aa*hi wakufrun bihi wa**a**lmasjidi

Mereka bertanya kepadamu (Muhammad) tentang berperang pada bulan haram. Katakanlah, “Berperang dalam bulan itu adalah (dosa) besar. Tetapi menghalangi (orang) dari jalan Allah, ingkar kepada-Nya, (menghalangi orang masuk) Masjidilharam, dan mengusir pendu

2:218

# اِنَّ الَّذِيْنَ اٰمَنُوْا وَالَّذِيْنَ هَاجَرُوْا وَجَاهَدُوْا فِيْ سَبِيْلِ اللّٰهِ ۙ اُولٰۤىِٕكَ يَرْجُوْنَ رَحْمَتَ اللّٰهِ ۗوَاللّٰهُ غَفُوْرٌ رَّحِيْمٌ

inna **al**la*dz*iina *aa*manuu wa**a**lla*dz*iina h*aa*jaruu waj*aa*haduu fii sabiili **al**l*aa*hi ul*aa*-ika yarjuuna ra*h*mata **al**l*aa*hi wa**al<**

Sesungguhnya orang-orang yang beriman, dan orang-orang yang berhijrah dan berjihad di jalan Allah, mereka itulah yang mengharapkan rahmat Allah. Allah Maha Pengampun, Maha Penyayang.

2:219

# ۞ يَسْـَٔلُوْنَكَ عَنِ الْخَمْرِ وَالْمَيْسِرِۗ قُلْ فِيْهِمَآ اِثْمٌ كَبِيْرٌ وَّمَنَافِعُ لِلنَّاسِۖ وَاِثْمُهُمَآ اَكْبَرُ مِنْ نَّفْعِهِمَاۗ وَيَسْـَٔلُوْنَكَ مَاذَا يُنْفِقُوْنَ ەۗ قُلِ الْعَفْوَۗ كَذٰلِكَ يُبَيِّنُ اللّٰهُ لَكُمُ الْاٰيٰتِ لَعَلّ

yas-aluunaka 'ani **a**lkhamri wa**a**lmaysiri qul fiihim*aa* itsmun kabiirun waman*aa*fi'u li**l**nn*aa*si wa-itsmuhum*aa* akbaru min naf'ihim*aa* wayas-aluunaka m*aatsaa* yunfiquuna qu

Mereka menanyakan kepadamu (Muhammad) tentang khamar dan judi. Katakanlah, “Pada keduanya terdapat dosa besar dan beberapa manfaat bagi manusia. Tetapi dosanya lebih besar daripada manfaatnya.” Dan mereka menanyakan kepadamu (tentang) apa yang (harus) mer

2:220

# فِى الدُّنْيَا وَالْاٰخِرَةِ ۗ وَيَسْـَٔلُوْنَكَ عَنِ الْيَتٰمٰىۗ قُلْ اِصْلَاحٌ لَّهُمْ خَيْرٌ ۗ وَاِنْ تُخَالِطُوْهُمْ فَاِخْوَانُكُمْ ۗ وَاللّٰهُ يَعْلَمُ الْمُفْسِدَ مِنَ الْمُصْلِحِ ۗ وَلَوْ شَاۤءَ اللّٰهُ لَاَعْنَتَكُمْ اِنَّ اللّٰهَ عَزِيْزٌ حَكِيْ

fii **al**dduny*aa* wa**a**l-*aa*khirati wayas-aluunaka 'ani **a**lyat*aa*m*aa* qul i*sh*l*aah*un lahum khayrun wa-in tukh*aa*li*th*uuhum fa-ikhw*aa*nukum wa**al**

**Tentang dunia dan akhirat. Mereka menanyakan kepadamu (Muhammad) tentang anak-anak yatim. Katakanlah, “Memperbaiki keadaan mereka adalah baik!” Dan jika kamu mempergauli mereka, maka mereka adalah saudara-saudaramu. Allah mengetahui orang yang berbuat ker**

2:221

# وَلَا تَنْكِحُوا الْمُشْرِكٰتِ حَتّٰى يُؤْمِنَّ ۗ وَلَاَمَةٌ مُّؤْمِنَةٌ خَيْرٌ مِّنْ مُّشْرِكَةٍ وَّلَوْ اَعْجَبَتْكُمْ ۚ وَلَا تُنْكِحُوا الْمُشْرِكِيْنَ حَتّٰى يُؤْمِنُوْا ۗ وَلَعَبْدٌ مُّؤْمِنٌ خَيْرٌ مِّنْ مُّشْرِكٍ وَّلَوْ اَعْجَبَكُمْ ۗ اُولٰۤىِٕكَ

wal*aa* tanki*h*uu **a**lmusyrik*aa*ti *h*att*aa* yu/minna wala-amatun mu/minatun khayrun min musyrikatin walaw a'jabatkum wal*aa* tunki*h*uu **a**lmusyrikiina *h*att*aa* yu/minuu wala

Dan janganlah kamu nikahi perempuan musyrik, sebelum mereka beriman. Sungguh, hamba sahaya perempuan yang beriman lebih baik daripada perempuan musyrik meskipun dia menarik hatimu. Dan janganlah kamu nikahkan orang (laki-laki) musyrik (dengan perempuan ya

2:222

# وَيَسْـَٔلُوْنَكَ عَنِ الْمَحِيْضِ ۗ قُلْ هُوَ اَذًىۙ فَاعْتَزِلُوا النِّسَاۤءَ فِى الْمَحِيْضِۙ وَلَا تَقْرَبُوْهُنَّ حَتّٰى يَطْهُرْنَ ۚ فَاِذَا تَطَهَّرْنَ فَأْتُوْهُنَّ مِنْ حَيْثُ اَمَرَكُمُ اللّٰهُ ۗ اِنَّ اللّٰهَ يُحِبُّ التَّوَّابِيْنَ وَيُحِبُّ

wayas-aluunaka 'ani **a**lma*h*ii*dh*i qul huwa a*dz*an fa**i**'taziluu **al**nnis*aa*-a fii **a**lma*h*ii*dh*i wal*aa* taqrabuuhunna *h*att*aa* ya*th*hu

Dan mereka menanyakan kepadamu (Muhammad) tentang haid. Katakanlah, “Itu adalah sesuatu yang kotor.” Karena itu jauhilah istri pada waktu haid; dan jangan kamu dekati mereka sebelum mereka suci. Apabila mereka telah suci, campurilah mereka sesuai dengan (

2:223

# نِسَاۤؤُكُمْ حَرْثٌ لَّكُمْ ۖ فَأْتُوْا حَرْثَكُمْ اَنّٰى شِئْتُمْ ۖ وَقَدِّمُوْا لِاَنْفُسِكُمْ ۗ وَاتَّقُوا اللّٰهَ وَاعْلَمُوْٓا اَنَّكُمْ مُّلٰقُوْهُ ۗ وَبَشِّرِ الْمُؤْمِنِيْنَ

nis*aa*ukum *h*artsun lakum fa/tuu *h*artsakum ann*aa* syi/tum waqaddimuu li-anfusikum wa**i**ttaquu **al**l*aa*ha wa**i**'lamuu annakum mul*aa*quuhu wabasysyiri **a**lmu/mi

Istri-istrimu adalah ladang bagimu, maka datangilah ladangmu itu kapan saja dan dengan cara yang kamu sukai. Dan utamakanlah (yang baik) untuk dirimu. Bertakwalah kepada Allah dan ketahuilah bahwa kamu (kelak) akan menemui-Nya. Dan sampaikanlah kabar gemb

2:224

# وَلَا تَجْعَلُوا اللّٰهَ عُرْضَةً لِّاَيْمَانِكُمْ اَنْ تَبَرُّوْا وَتَتَّقُوْا وَتُصْلِحُوْا بَيْنَ النَّاسِۗ وَاللّٰهُ سَمِيْعٌ عَلِيْمٌ

wal*aa* taj'aluu **al**l*aa*ha 'ur*dh*atan li-aym*aa*nikum an tabarruu watattaquu watu*sh*li*h*uu bayna **al**nn*aa*si wa**al**l*aa*hu samii'un 'aliim**un**

Dan janganlah kamu jadikan (nama) Allah dalam sumpahmu sebagai penghalang untuk berbuat kebajikan, bertakwa dan menciptakan kedamaian di antara manusia. Allah Maha Mendengar, Maha Mengetahui.

2:225

# لَا يُؤَاخِذُكُمُ اللّٰهُ بِاللَّغْوِ فِيْٓ اَيْمَانِكُمْ وَلٰكِنْ يُّؤَاخِذُكُمْ بِمَا كَسَبَتْ قُلُوْبُكُمْ ۗ وَاللّٰهُ غَفُوْرٌ حَلِيْمٌ

l*aa* yu-*aa*khi*dz*ukumu **al**l*aa*hu bi**a**llaghwi fii aym*aa*nikum wal*aa*kin yu-*aa*khi*dz*ukum bim*aa* kasabat quluubukum wa**al**l*aa*hu ghafuurun *h*ali

Allah tidak menghukum kamu karena sumpahmu yang tidak kamu sengaja, tetapi Dia menghukum kamu karena niat yang terkandung dalam hatimu. Allah Maha Pengampun, Maha Penyantun.

2:226

# لِلَّذِيْنَ يُؤْلُوْنَ مِنْ نِّسَاۤىِٕهِمْ تَرَبُّصُ اَرْبَعَةِ اَشْهُرٍۚ فَاِنْ فَاۤءُوْ فَاِنَّ اللّٰهَ غَفُوْرٌ رَّحِيْمٌ

lilla*dz*iina yu/luuna min nis*aa*-ihim tarabbu*sh*u arba'ati asyhurin fa-in f*aa*uu fa-inna **al**l*aa*ha ghafuurun ra*h*iim**un**

Bagi orang yang meng-ila' istrinya harus menunggu empat bulan. Kemudian jika mereka kembali (kepada istrinya), maka sungguh, Allah Maha Pengampun, Maha Penyayang.

2:227

# وَاِنْ عَزَمُوا الطَّلَاقَ فَاِنَّ اللّٰهَ سَمِيْعٌ عَلِيْمٌ

wa-in 'azamuu **al***ththh*al*aa*qa fa-inna **al**l*aa*ha samii'un 'aliim**un**

Dan jika mereka berketetapan hati hendak menceraikan, maka sungguh, Allah Maha Mendengar, Maha Mengetahui.

2:228

# وَالْمُطَلَّقٰتُ يَتَرَبَّصْنَ بِاَنْفُسِهِنَّ ثَلٰثَةَ قُرُوْۤءٍۗ وَلَا يَحِلُّ لَهُنَّ اَنْ يَّكْتُمْنَ مَا خَلَقَ اللّٰهُ فِيْٓ اَرْحَامِهِنَّ اِنْ كُنَّ يُؤْمِنَّ بِاللّٰهِ وَالْيَوْمِ الْاٰخِرِۗ وَبُعُوْلَتُهُنَّ اَحَقُّ بِرَدِّهِنَّ فِيْ ذٰلِكَ اِنْ

wa**a**lmu*th*allaq*aa*tu yatarabba*sh*na bi-anfusihinna tsal*aa*tsata quruu-in wal*aa* ya*h*illu lahunna an yaktumna m*aa* khalaqa **al**l*aa*hu fii ar*haa*mihinna in kunna yu/minna b

Dan para istri yang diceraikan (wajib) menahan diri mereka (menunggu) tiga kali quru'. Tidak boleh bagi mereka menyembunyikan apa yang diciptakan Allah dalam rahim mereka, jika mereka beriman kepada Allah dan hari akhir. Dan para suami mereka lebih be

2:229

# اَلطَّلَاقُ مَرَّتٰنِ ۖ فَاِمْسَاكٌۢ بِمَعْرُوْفٍ اَوْ تَسْرِيْحٌۢ بِاِحْسَانٍ ۗ وَلَا يَحِلُّ لَكُمْ اَنْ تَأْخُذُوْا مِمَّآ اٰتَيْتُمُوْهُنَّ شَيْـًٔا اِلَّآ اَنْ يَّخَافَآ اَلَّا يُقِيْمَا حُدُوْدَ اللّٰهِ ۗ فَاِنْ خِفْتُمْ اَلَّا يُقِيْمَا حُدُوْدَ

a**l***ththh*al*aa*qu marrat*aa*ni fa-ims*aa*kun bima'ruufin aw tasrii*h*un bi-i*h*s*aa*nin wal*aa *ya*h*illu lakum an ta/khu*dz*uu mimm*aa* *aa*taytumuuhunna syay-an ill*aa* an

Talak (yang dapat dirujuk) itu dua kali. (Setelah itu suami dapat) menahan dengan baik, atau melepaskan dengan baik. Tidak halal bagi kamu mengambil kembali sesuatu yang telah kamu berikan kepada mereka, kecuali keduanya (suami dan istri) khawatir tidak m

2:230

# فَاِنْ طَلَّقَهَا فَلَا تَحِلُّ لَهٗ مِنْۢ بَعْدُ حَتّٰى تَنْكِحَ زَوْجًا غَيْرَهٗ ۗ فَاِنْ طَلَّقَهَا فَلَا جُنَاحَ عَلَيْهِمَآ اَنْ يَّتَرَاجَعَآ اِنْ ظَنَّآ اَنْ يُّقِيْمَا حُدُوْدَ اللّٰهِ ۗ وَتِلْكَ حُدُوْدُ اللّٰهِ يُبَيِّنُهَا لِقَوْمٍ يَّعْلَمُ

fa-in *th*allaqah*aa* fal*aa* ta*h*illu lahu min ba'du *h*att*aa* tanki*h*a zawjan ghayrahu fa-in *th*allaqah*aa* fal*aa* jun*aah*a 'alayhim*aa* an yatar*aa*ja'*aa* in *zh*ann*aa*

Kemudian jika dia menceraikannya (setelah talak yang kedua), maka perempuan itu tidak halal lagi baginya sebelum dia menikah dengan suami yang lain. Kemudian jika suami yang lain itu menceraikannya, maka tidak ada dosa bagi keduanya (suami pertama dan bek

2:231

# وَاِذَا طَلَّقْتُمُ النِّسَاۤءَ فَبَلَغْنَ اَجَلَهُنَّ فَاَمْسِكُوْهُنَّ بِمَعْرُوْفٍ اَوْ سَرِّحُوْهُنَّ بِمَعْرُوْفٍۗ وَلَا تُمْسِكُوْهُنَّ ضِرَارًا لِّتَعْتَدُوْا ۚ وَمَنْ يَّفْعَلْ ذٰلِكَ فَقَدْ ظَلَمَ نَفْسَهٗ ۗ وَلَا تَتَّخِذُوْٓا اٰيٰتِ اللّٰهِ هُز

wa-i*dzaa* *th*allaqtumu **al**nnis*aa*-a fabalaghna ajalahunna fa-amsikuuhunna bima'ruufin aw sarri*h*uuhunna bima'ruufin wal*aa* tumsikuuhunna *dh*ir*aa*ran lita'taduu waman yaf'al *dzaa*lika faqad

Dan apabila kamu menceraikan istri-istri (kamu), lalu sampai (akhir) idahnya, maka tahanlah mereka dengan cara yang baik, atau ceraikanlah mereka dengan cara yang baik (pula). Dan janganlah kamu tahan mereka dengan maksud jahat untuk menzalimi mereka. Bar

2:232

# وَاِذَا طَلَّقْتُمُ النِّسَاۤءَ فَبَلَغْنَ اَجَلَهُنَّ فَلَا تَعْضُلُوْهُنَّ اَنْ يَّنْكِحْنَ اَزْوَاجَهُنَّ اِذَا تَرَاضَوْا بَيْنَهُمْ بِالْمَعْرُوْفِ ۗ ذٰلِكَ يُوْعَظُ بِهٖ مَنْ كَانَ مِنْكُمْ يُؤْمِنُ بِاللّٰهِ وَالْيَوْمِ الْاٰخِرِ ۗ ذٰلِكُمْ اَزْكٰى

wa-i*dzaa* *th*allaqtumu **al**nnis*aa*-a fabalaghna ajalahunna fal*aa* ta'*dh*uluuhunna an yanki*h*na azw*aa*jahunna i*dzaa* tar*aad*aw baynahum bi**a**lma'ruufi *dzaa*lika yuu'a

Dan apabila kamu menceraikan istri-istri (kamu), lalu sampai idahnya, maka jangan kamu halangi mereka menikah (lagi) dengan calon suaminya, apabila telah terjalin kecocokan di antara mereka dengan cara yang baik. Itulah yang dinasihatkan kepada orang-oran

2:233

# ۞ وَالْوَالِدٰتُ يُرْضِعْنَ اَوْلَادَهُنَّ حَوْلَيْنِ كَامِلَيْنِ لِمَنْ اَرَادَ اَنْ يُّتِمَّ الرَّضَاعَةَ ۗ وَعَلَى الْمَوْلُوْدِ لَهٗ رِزْقُهُنَّ وَكِسْوَتُهُنَّ بِالْمَعْرُوْفِۗ لَا تُكَلَّفُ نَفْسٌ اِلَّا وُسْعَهَا ۚ لَا تُضَاۤرَّ وَالِدَةٌ ۢبِوَلَد

wa**a**lw*aa*lid*aa*tu yur*dh*i'na awl*aa*dahunna *h*awlayni k*aa*milayni liman ar*aa*da an yutimma **al**rra*daa*'ata wa'al*aa* **a**lmawluudi lahu rizquhunna wakiswatuhu

Dan ibu-ibu hendaklah menyusui anak-anaknya selama dua tahun penuh, bagi yang ingin menyusui secara sempurna. Dan kewajiban ayah menanggung nafkah dan pakaian mereka dengan cara yang patut. Seseorang tidak dibebani lebih dari kesanggupannya. Janganlah seo

2:234

# وَالَّذِيْنَ يُتَوَفَّوْنَ مِنْكُمْ وَيَذَرُوْنَ اَزْوَاجًا يَّتَرَبَّصْنَ بِاَنْفُسِهِنَّ اَرْبَعَةَ اَشْهُرٍ وَّعَشْرًا ۚ فاِذَا بَلَغْنَ اَجَلَهُنَّ فَلَا جُنَاحَ عَلَيْكُمْ فِيْمَا فَعَلْنَ فِيْٓ اَنْفُسِهِنَّ بِالْمَعْرُوْفِۗ وَاللّٰهُ بِمَا تَعْمَلُ

wa**a**lla*dz*iina yutawaffawna minkum waya*dz*aruuna azw*aa*jan yatarabba*sh*na bi-anfusihinna arba'ata asyhurin wa'asyran fa-i*dzaa* balaghna ajalahunna fal*aa* jun*aah*a 'alaykum fiim*aa* fa'alna fii

Dan orang-orang yang mati di antara kamu serta meninggalkan istri-istri hendaklah mereka (istri-istri) menunggu empat bulan sepuluh hari. Kemudian apabila telah sampai (akhir) idah mereka, maka tidak ada dosa bagimu mengenai apa yang mereka lakukan terhad

2:235

# وَلَا جُنَاحَ عَلَيْكُمْ فِيْمَا عَرَّضْتُمْ بِهٖ مِنْ خِطْبَةِ النِّسَاۤءِ اَوْ اَكْنَنْتُمْ فِيْٓ اَنْفُسِكُمْ ۗ عَلِمَ اللّٰهُ اَنَّكُمْ سَتَذْكُرُوْنَهُنَّ وَلٰكِنْ لَّا تُوَاعِدُوْهُنَّ سِرًّا اِلَّآ اَنْ تَقُوْلُوْا قَوْلًا مَّعْرُوْفًا ەۗ وَلَا تَ

wal*aa* jun*aah*a 'alaykum fiim*aa* 'arra*dh*tum bihi min khi*th*bati **al**nnis*aa*-i aw aknantum fii anfusikum 'alima **al**l*aa*hu annakum sata*dz*kuruunahunna wal*aa*kin l*aa*

Dan tidak ada dosa bagimu meminang perempuan-perempuan itu dengan sindiran atau kamu sembunyikan (keinginanmu) dalam hati. Allah mengetahui bahwa kamu akan menyebut-nyebut kepada mereka. Tetapi janganlah kamu membuat perjanjian (untuk menikah) dengan mere

2:236

# لَا جُنَاحَ عَلَيْكُمْ اِنْ طَلَّقْتُمُ النِّسَاۤءَ مَا لَمْ تَمَسُّوْهُنَّ اَوْ تَفْرِضُوْا لَهُنَّ فَرِيْضَةً ۖ وَّمَتِّعُوْهُنَّ عَلَى الْمُوْسِعِ قَدَرُهٗ وَعَلَى الْمُقْتِرِ قَدَرُهٗ ۚ مَتَاعًا ۢبِالْمَعْرُوْفِۚ حَقًّا عَلَى الْمُحْسِنِيْنَ

l*aa* jun*aah*a 'alaykum in *th*allaqtumu **al**nnis*aa*-a m*aa* lam tamassuuhunna aw tafri*dh*uu lahunna farii*dh*atan wamatti'uuhunna 'al*aa* **a**lmuusi'i qadaruhu wa'al*aa*

Tidak ada dosa bagimu, jika kamu menceraikan istri-istri kamu yang belum kamu sentuh (campuri) atau belum kamu tentukan maharnya. Dan hendaklah kamu beri mereka mut‘ah, bagi yang mampu menurut kemampuannya dan bagi yang tidak mampu menurut kesanggupannya,

2:237

# وَاِنْ طَلَّقْتُمُوْهُنَّ مِنْ قَبْلِ اَنْ تَمَسُّوْهُنَّ وَقَدْ فَرَضْتُمْ لَهُنَّ فَرِيْضَةً فَنِصْفُ مَا فَرَضْتُمْ اِلَّآ اَنْ يَّعْفُوْنَ اَوْ يَعْفُوَا الَّذِيْ بِيَدِهٖ عُقْدَةُ النِّكَاحِ ۗ وَاَنْ تَعْفُوْٓا اَقْرَبُ لِلتَّقْوٰىۗ وَلَا تَنْسَوُا

wa-in *th*allaqtumuuhunna min qabli an tamassuuhunna waqad fara*dh*tum lahunna farii*dh*atan fani*sh*fu m*aa* fara*dh*tum ill*aa* an ya'fuuna aw ya'fuwa **al**la*dz*ii biyadihi 'uqdatu **al**

**Dan jika kamu menceraikan mereka sebelum kamu sentuh (campuri), padahal kamu sudah menentukan Maharnya, maka (bayarlah) seperdua dari yang telah kamu tentukan, kecuali jika mereka (membebaskan) atau dibebaskan oleh orang yang akad nikah ada di tangannya.**

2:238

# حَافِظُوْا عَلَى الصَّلَوٰتِ وَالصَّلٰوةِ الْوُسْطٰى وَقُوْمُوْا لِلّٰهِ قٰنِتِيْنَ

*haa*fi*zh*uu 'al*aa* **al***shsh*alaw*aa*ti wa**al***shsh*al*aa*ti **a**lwus*thaa *waquumuu lill*aa*hi q*aa*nitiin**a**

Peliharalah semua salat dan salat wustha. Dan laksanakanlah (salat) karena Allah dengan khusyuk.

2:239

# فَاِنْ خِفْتُمْ فَرِجَالًا اَوْ رُكْبَانًا ۚ فَاِذَآ اَمِنْتُمْ فَاذْكُرُوا اللّٰهَ كَمَا عَلَّمَكُمْ مَّا لَمْ تَكُوْنُوْا تَعْلَمُوْنَ

fa-in khiftum farij*aa*lan aw rukb*aa*nan fa-i*dzaa* amintum fa**u***dz*kuruu **al**l*aa*ha kam*aa *'allamakum m*aa* lam takuunuu ta'lamuun**a**

Jika kamu takut (ada bahaya), salatlah sambil berjalan kaki atau berkendaraan. Kemudian apabila telah aman, maka ingatlah Allah (salatlah), sebagaimana Dia telah mengajarkan kepadamu apa yang tidak kamu ketahui.

2:240

# وَالَّذِيْنَ يُتَوَفَّوْنَ مِنْكُمْ وَيَذَرُوْنَ اَزْوَاجًاۖ وَّصِيَّةً لِّاَزْوَاجِهِمْ مَّتَاعًا اِلَى الْحَوْلِ غَيْرَ اِخْرَاجٍ ۚ فَاِنْ خَرَجْنَ فَلَا جُنَاحَ عَلَيْكُمْ فِيْ مَا فَعَلْنَ فِيْٓ اَنْفُسِهِنَّ مِنْ مَّعْرُوْفٍۗ وَاللّٰهُ عَزِيْزٌ حَكِي

wa**a**lla*dz*iina yutawaffawna minkum waya*dz*aruuna azw*aa*jan wa*sh*iyyatan li-azw*aa*jihim mat*aa*'an il*aa* **a**l*h*awli ghayra ikhr*aa*jin fa-in kharajna fal*aa* jun*aah<*

Dan orang-orang yang akan mati di antara kamu dan meninggalkan istri-istri, hendaklah membuat wasiat untuk istri-istrinya, (yaitu) nafkah sampai setahun tanpa mengeluarkannya (dari rumah). Tetapi jika mereka keluar (sendiri), maka tidak ada dosa bagimu (m

2:241

# وَلِلْمُطَلَّقٰتِ مَتَاعٌ ۢبِالْمَعْرُوْفِۗ حَقًّا عَلَى الْمُتَّقِيْنَ

walilmu*th*allaq*aa*ti mat*aa*'un bi**a**lma'ruufi *h*aqqan 'al*aa* **a**lmuttaqiin**a**

Dan bagi perempuan-perempuan yang diceraikan hendaklah diberi mut‘ah menurut cara yang patut, sebagai suatu kewajiban bagi orang yang bertakwa.

2:242

# كَذٰلِكَ يُبَيِّنُ اللّٰهُ لَكُمْ اٰيٰتِهٖ لَعَلَّكُمْ تَعْقِلُوْنَ ࣖ

ka*dzaa*lika yubayyinu **al**l*aa*hu lakum *aa*y*aa*tihi la'allakum ta'qiluun**a**

Demikianlah Allah menerangkan kepadamu ayat-ayat-Nya agar kamu mengerti.

2:243

# ۞ اَلَمْ تَرَ اِلَى الَّذِيْنَ خَرَجُوْا مِنْ دِيَارِهِمْ وَهُمْ اُلُوْفٌ حَذَرَ الْمَوْتِۖ فَقَالَ لَهُمُ اللّٰهُ مُوْتُوْا ۗ ثُمَّ اَحْيَاهُمْ ۗ اِنَّ اللّٰهَ لَذُوْ فَضْلٍ عَلَى النَّاسِ وَلٰكِنَّ اَكْثَرَ النَّاسِ لَا يَشْكُرُوْنَ

alam tara il*aa* **al**la*dz*iina kharajuu min diy*aa*rihim wahum uluufun *h*a*dz*ara **a**lmawti faq*aa*la lahumu **al**l*aa*hu muutuu tsumma a*h*y*aa*hum inna **al**

Tidakkah kamu memperhatikan orang-orang yang keluar dari kampung halamannya, sedang jumlahnya ribuan karena takut mati? Lalu Allah berfirman kepada mereka, “Matilah kamu!” Kemudian Allah menghidupkan mereka. Sesungguhnya Allah memberikan karunia kepada ma

2:244

# وَقَاتِلُوْا فِيْ سَبِيْلِ اللّٰهِ وَاعْلَمُوْٓا اَنَّ اللّٰهَ سَمِيْعٌ عَلِيْمٌ

waq*aa*tiluu fii sabiili **al**l*aa*hi wa**i**'lamuu anna **al**l*aa*ha samii'un 'aliim**un**

Dan berperanglah kamu di jalan Allah, dan ketahuilah bahwa Allah Maha Mendengar, Maha Mengetahui.

2:245

# مَنْ ذَا الَّذِيْ يُقْرِضُ اللّٰهَ قَرْضًا حَسَنًا فَيُضٰعِفَهٗ لَهٗٓ اَضْعَافًا كَثِيْرَةً ۗوَاللّٰهُ يَقْبِضُ وَيَبْصُۣطُۖ وَاِلَيْهِ تُرْجَعُوْنَ

man *dzaa* **al**la*dz*ii yuqri*dh*u **al**l*aa*ha qar*dh*an *h*asanan fayu*daa*'ifahu lahu a*dh*'*aa*fan katsiiratan wa**al**l*aa*hu yaqbi*dh*u wayabsu*th*

Barangsiapa meminjami Allah dengan pinjaman yang baik maka Allah melipatgandakan ganti kepadanya dengan banyak. Allah menahan dan melapangkan (rezeki) dan kepada-Nyalah kamu dikembalikan.

2:246

# اَلَمْ تَرَ اِلَى الْمَلَاِ مِنْۢ بَنِيْٓ اِسْرَاۤءِيْلَ مِنْۢ بَعْدِ مُوْسٰىۘ اِذْ قَالُوْا لِنَبِيٍّ لَّهُمُ ابْعَثْ لَنَا مَلِكًا نُّقَاتِلْ فِيْ سَبِيْلِ اللّٰهِ ۗ قَالَ هَلْ عَسَيْتُمْ اِنْ كُتِبَ عَلَيْكُمُ الْقِتَالُ اَلَّا تُقَاتِلُوْا ۗ قَالُوْا

alam tara il*aa* **a**lmala-i min banii isr*aa*-iila min ba'di muus*aa* i*dz* q*aa*luu linabiyyin lahumu ib'ats lan*aa* malikan nuq*aa*til fii sabiili **al**l*aa*hi q*aa*la hal 'asaytu

Tidakkah kamu perhatikan para pemuka Bani Israil setelah Musa wafat, ketika mereka berkata kepada seorang nabi mereka, “Angkatlah seorang raja untuk kami, niscaya kami berperang di jalan Allah.” Nabi mereka menjawab, “Jangan-jangan jika diwajibkan atasmu

2:247

# وَقَالَ لَهُمْ نَبِيُّهُمْ اِنَّ اللّٰهَ قَدْ بَعَثَ لَكُمْ طَالُوْتَ مَلِكًا ۗ قَالُوْٓا اَنّٰى يَكُوْنُ لَهُ الْمُلْكُ عَلَيْنَا وَنَحْنُ اَحَقُّ بِالْمُلْكِ مِنْهُ وَلَمْ يُؤْتَ سَعَةً مِّنَ الْمَالِۗ قَالَ اِنَّ اللّٰهَ اصْطَفٰىهُ عَلَيْكُمْ وَزَادَهٗ

waq*aa*la lahum nabiyyuhum inna **al**l*aa*ha qad ba'atsa lakum *thaa*luuta malikan q*aa*luu ann*aa* yakuunu lahu **a**lmulku 'alayn*aa* wana*h*nu a*h*aqqu bi**a**lmulki

Dan nabi mereka berkata kepada mereka, “Sesungguhnya Allah telah mengangkat Talut menjadi rajamu.” Mereka menjawab, “Bagaimana Talut memperoleh kerajaan atas kami, sedangkan kami lebih berhak atas kerajaan itu daripadanya, dan dia tidak diberi kekayaan ya

2:248

# وَقَالَ لَهُمْ نَبِيُّهُمْ اِنَّ اٰيَةَ مُلْكِهٖٓ اَنْ يَّأْتِيَكُمُ التَّابُوْتُ فِيْهِ سَكِيْنَةٌ مِّنْ رَّبِّكُمْ وَبَقِيَّةٌ مِّمَّا تَرَكَ اٰلُ مُوْسٰى وَاٰلُ هٰرُوْنَ تَحْمِلُهُ الْمَلٰۤىِٕكَةُ ۗ اِنَّ فِيْ ذٰلِكَ لَاٰيَةً لَّكُمْ اِنْ كُنْتُمْ مُّؤ

waq*aa*la lahum nabiyyuhum inna *aa*yata mulkihi an ya/tiyakumu **al**tt*aa*buutu fiihi sakiinatun min rabbikum wabaqiyyatun mimm*aa* taraka *aa*lu muus*aa* wa*aa*lu h*aa*ruuna ta*h*miluhu

Dan nabi mereka berkata kepada mereka, “Sesungguhnya tanda kerajaannya ialah datangnya Tabut kepadamu, yang di dalamnya terdapat ketenangan dari Tuhanmu dan sisa peninggalan keluarga Musa dan keluarga Harun, yang dibawa oleh malaikat. Sungguh, pada yang d

2:249

# فَلَمَّا فَصَلَ طَالُوْتُ بِالْجُنُوْدِ قَالَ اِنَّ اللّٰهَ مُبْتَلِيْكُمْ بِنَهَرٍۚ فَمَنْ شَرِبَ مِنْهُ فَلَيْسَ مِنِّيْۚ وَمَنْ لَّمْ يَطْعَمْهُ فَاِنَّهٗ مِنِّيْٓ اِلَّا مَنِ اغْتَرَفَ غُرْفَةً ۢبِيَدِهٖ ۚ فَشَرِبُوْا مِنْهُ اِلَّا قَلِيْلًا مِّنْهُمْ

falamm*aa* fa*sh*ala *thaa*luutu bi**a**ljunuudi q*aa*la inna **al**l*aa*ha mubtaliikum binaharin faman syariba minhu falaysa minnii waman lam ya*th*'amhu fa-innahu minnii ill*aa* mani ig

Maka ketika Talut membawa bala tentaranya, dia berkata, “Allah akan menguji kamu dengan sebuah sungai. Maka barangsiapa meminum (airnya), dia bukanlah pengikutku. Dan barangsiapa tidak meminumnya, maka dia adalah pengikutku kecuali menciduk seciduk dengan

2:250

# وَلَمَّا بَرَزُوْا لِجَالُوْتَ وَجُنُوْدِهٖ قَالُوْا رَبَّنَآ اَفْرِغْ عَلَيْنَا صَبْرًا وَّثَبِّتْ اَقْدَامَنَا وَانْصُرْنَا عَلَى الْقَوْمِ الْكٰفِرِيْنَ ۗ

walamm*aa* barazuu lij*aa*luuta wajunuudihi q*aa*luu rabban*aa* afrigh 'alayn*aa* *sh*abran watsabbit aqd*aa*man*aa* wa**u**n*sh*urn*aa* 'al*aa* **a**lqawmi **a**

**Dan ketika mereka maju melawan Jalut dan tentaranya, mereka berdoa, “Ya Tuhan kami, limpahkanlah kesabaran kepada kami, kukuhkanlah langkah kami dan tolonglah kami menghadapi orang-orang kafir.”**

2:251

# فَهَزَمُوْهُمْ بِاِذْنِ اللّٰهِ ۗوَقَتَلَ دَاوٗدُ جَالُوْتَ وَاٰتٰىهُ اللّٰهُ الْمُلْكَ وَالْحِكْمَةَ وَعَلَّمَهٗ مِمَّا يَشَاۤءُ ۗ وَلَوْلَا دَفْعُ اللّٰهِ النَّاسَ بَعْضَهُمْ بِبَعْضٍ لَّفَسَدَتِ الْاَرْضُ وَلٰكِنَّ اللّٰهَ ذُوْ فَضْلٍ عَلَى الْعٰلَمِي

fahazamuuhum bi-i*dz*ni **al**l*aa*hi waqatala d*aa*wuudu j*aa*luuta wa*aa*t*aa*hu **al**l*aa*hu **a**lmulka wa**a**l*h*ikmata wa'allamahu mimm*aa* yasy*aa<*

Maka mereka mengalahkannya dengan izin Allah, dan Dawud membunuh Jalut. Kemudian Allah memberinya (Dawud) kerajaan, dan hikmah, dan mengajarinya apa yang Dia kehendaki. Dan kalau Allah tidak melindungi sebagian manusia dengan sebagian yang lain, niscaya r

2:252

# تِلْكَ اٰيٰتُ اللّٰهِ نَتْلُوْهَا عَلَيْكَ بِالْحَقِّ ۗ وَاِنَّكَ لَمِنَ الْمُرْسَلِيْنَ ۔

tilka *aa*y*aa*tu **al**l*aa*hi natluuh*aa* 'alayka bi**a**l*h*aqqi wa-innaka lamina **a**lmursaliin**a**

Itulah ayat-ayat Allah, Kami bacakan kepadamu dengan benar dan engkau (Muhammad) adalah benar-benar seorang rasul.

2:253

# تِلْكَ الرُّسُلُ فَضَّلْنَا بَعْضَهُمْ عَلٰى بَعْضٍۘ مِنْهُمْ مَّنْ كَلَّمَ اللّٰهُ وَرَفَعَ بَعْضَهُمْ دَرَجٰتٍۗ وَاٰتَيْنَا عِيْسَى ابْنَ مَرْيَمَ الْبَيِّنٰتِ وَاَيَّدْنٰهُ بِرُوْحِ الْقُدُسِۗ وَلَوْ شَاۤءَ اللّٰهُ مَا اقْتَتَلَ الَّذِيْنَ مِنْۢ بَعْدِ

tilka **al**rrusulu fa*dhdh*aln*aa* ba'*dh*ahum 'al*aa* ba'*dh*in minhum man kallama **al**l*aa*hu warafa'a ba'*dh*ahum daraj*aa*tin wa*aa*tayn*aa* 'iis*aa* ibna maryama

Rasul-rasul itu Kami lebihkan sebagian mereka dari sebagian yang lain. Di antara mereka ada yang (langsung) Allah berfirman dengannya dan sebagian lagi ada yang ditinggikan-Nya beberapa derajat. Dan Kami beri Isa putra Maryam beberapa mukjizat dan Kami pe

2:254

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْٓا اَنْفِقُوْا مِمَّا رَزَقْنٰكُمْ مِّنْ قَبْلِ اَنْ يَّأْتِيَ يَوْمٌ لَّا بَيْعٌ فِيْهِ وَلَا خُلَّةٌ وَّلَا شَفَاعَةٌ ۗوَالْكٰفِرُوْنَ هُمُ الظّٰلِمُوْنَ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu anfiquu mimm*aa* razaqn*aa*kum min qabli an ya/tiya yawmun l*aa* bay'un fiihi wal*aa* khullatun wal*aa* syaf*aa*'atun wa**a**lk*aa*

Wahai orang-orang yang beriman! Infakkanlah sebagian dari rezeki yang telah Kami berikan kepadamu sebelum datang hari ketika tidak ada lagi jual beli, tidak ada lagi persahabatan dan tidak ada lagi syafaat. Orang-orang kafir itulah orang yang zalim.

2:255

# اَللّٰهُ لَآ اِلٰهَ اِلَّا هُوَۚ اَلْحَيُّ الْقَيُّوْمُ ەۚ لَا تَأْخُذُهٗ سِنَةٌ وَّلَا نَوْمٌۗ لَهٗ مَا فِى السَّمٰوٰتِ وَمَا فِى الْاَرْضِۗ مَنْ ذَا الَّذِيْ يَشْفَعُ عِنْدَهٗٓ اِلَّا بِاِذْنِهٖۗ يَعْلَمُ مَا بَيْنَ اَيْدِيْهِمْ وَمَا خَلْفَهُمْۚ وَلَ

**al**l*aa*hu l*aa* il*aa*ha ill*aa* huwa **a**l*h*ayyu **a**lqayyuumu l*aa* ta/khu*dz*uhu sinatun wal*aa* nawmun lahu m*aa* fii **al**ssam*aa*w*aa*

Allah, tidak ada tuhan selain Dia. Yang Mahahidup, Yang terus menerus mengurus (makhluk-Nya), tidak mengantuk dan tidak tidur. Milik-Nya apa yang ada di langit dan apa yang ada di bumi. Tidak ada yang dapat memberi syafaat di sisi-Nya tanpa izin-Nya. Dia

2:256

# لَآ اِكْرَاهَ فِى الدِّيْنِۗ قَدْ تَّبَيَّنَ الرُّشْدُ مِنَ الْغَيِّ ۚ فَمَنْ يَّكْفُرْ بِالطَّاغُوْتِ وَيُؤْمِنْۢ بِاللّٰهِ فَقَدِ اسْتَمْسَكَ بِالْعُرْوَةِ الْوُثْقٰى لَا انْفِصَامَ لَهَا ۗوَاللّٰهُ سَمِيْعٌ عَلِيْمٌ

l*aa* ikr*aa*ha fii **al**ddiini qad tabayyana **al**rrusydu mina **a**lghayyi faman yakfur bi**al***ththaa*ghuuti wayu/min bi**al**l*aa*hi faqadi istamsaka bi**a**

**Tidak ada paksaan dalam (menganut) agama (Islam), sesungguhnya telah jelas (perbedaan) antara jalan yang benar dengan jalan yang sesat. Barang siapa ingkar kepada Tagut dan beriman kepada Allah, maka sungguh, dia telah berpegang (teguh) pada tali yang san**

2:257

# اَللّٰهُ وَلِيُّ الَّذِيْنَ اٰمَنُوْا يُخْرِجُهُمْ مِّنَ الظُّلُمٰتِ اِلَى النُّوْرِۗ وَالَّذِيْنَ كَفَرُوْٓا اَوْلِيَاۤؤُهُمُ الطَّاغُوْتُ يُخْرِجُوْنَهُمْ مِّنَ النُّوْرِ اِلَى الظُّلُمٰتِۗ اُولٰۤىِٕكَ اَصْحٰبُ النَّارِۚ هُمْ فِيْهَا خٰلِدُوْنَ ࣖ

**al**l*aa*hu waliyyu **al**la*dz*iina *aa*manuu yukhrijuhum mina **al***zhzh*ulum*aa*ti il*aa* **al**nnuuri wa**a**lla*dz*iina kafaruu awliy*aa*uhumu

Allah pelindung orang yang beriman. Dia mengeluarkan mereka dari kegelapan kepada cahaya (iman). Dan orang-orang yang kafir, pelindung-pelindungnya adalah setan, yang mengeluarkan mereka dari cahaya kepada kegelapan. Mereka adalah penghuni neraka. Mereka

2:258

# اَلَمْ تَرَ اِلَى الَّذِيْ حَاۤجَّ اِبْرٰهٖمَ فِيْ رَبِّهٖٓ اَنْ اٰتٰىهُ اللّٰهُ الْمُلْكَ ۘ اِذْ قَالَ اِبْرٰهٖمُ رَبِّيَ الَّذِيْ يُحْيٖ وَيُمِيْتُۙ قَالَ اَنَا۠ اُحْيٖ وَاُمِيْتُ ۗ قَالَ اِبْرٰهٖمُ فَاِنَّ اللّٰهَ يَأْتِيْ بِالشَّمْسِ مِنَ الْمَشْرِقِ

alam tara il*aa* **al**la*dz*ii *haa*jja ibr*aa*hiima fii rabbihi an *aa*t*aa*hu **al**l*aa*hu **a**lmulka i*dz* q*aa*la ibr*aa*hiimu rabbiya **al**la

Tidakkah kamu memperhatikan orang yang mendebat Ibrahim mengenai Tuhannya, karena Allah telah memberinya kerajaan (kekuasaan). Ketika Ibrahim berkata, “Tuhanku ialah Yang menghidupkan dan mematikan,” dia berkata, “Aku pun dapat menghidupkan dan mematikan.

2:259

# اَوْ كَالَّذِيْ مَرَّ عَلٰى قَرْيَةٍ وَّهِيَ خَاوِيَةٌ عَلٰى عُرُوْشِهَاۚ قَالَ اَنّٰى يُحْيٖ هٰذِهِ اللّٰهُ بَعْدَ مَوْتِهَا ۚ فَاَمَاتَهُ اللّٰهُ مِائَةَ عَامٍ ثُمَّ بَعَثَهٗ ۗ قَالَ كَمْ لَبِثْتَ ۗ قَالَ لَبِثْتُ يَوْمًا اَوْ بَعْضَ يَوْمٍۗ قَالَ بَلْ

aw ka**a**lla*dz*ii marra 'al*aa* qaryatin wahiya kh*aa*wiyatun 'al*aa* 'uruusyih*aa* q*aa*la ann*aa* yu*h*yii h*aadz*ihi **al**l*aa*hu ba'da mawtih*aa* fa-am*aa*tahu

Atau seperti orang yang melewati suatu negeri yang (bangunan-bangunannya) telah roboh hingga menutupi (reruntuhan) atap-atapnya, dia berkata, “Bagaimana Allah menghidupkan kembali (negeri) ini setelah hancur?” Lalu Allah mematikannya (orang itu) selama se

2:260

# وَاِذْ قَالَ اِبْرٰهٖمُ رَبِّ اَرِنِيْ كَيْفَ تُحْيِ الْمَوْتٰىۗ قَالَ اَوَلَمْ تُؤْمِنْ ۗقَالَ بَلٰى وَلٰكِنْ لِّيَطْمَىِٕنَّ قَلْبِيْ ۗقَالَ فَخُذْ اَرْبَعَةً مِّنَ الطَّيْرِفَصُرْهُنَّ اِلَيْكَ ثُمَّ اجْعَلْ عَلٰى كُلِّ جَبَلٍ مِّنْهُنَّ جُزْءًا ثُمَّ

wa-i*dz* q*aa*la ibr*aa*hiimu rabbi arinii kayfa tu*h*yii **a**lmawt*aa* q*aa*la awa lam tu/min q*aa*la bal*aa* wal*aa*kin liya*th*ma-inna qalbii q*aa*la fakhu*dz* arba'atan mina

Dan (ingatlah) ketika Ibrahim berkata, “Ya Tuhanku, perlihatkanlah kepadaku bagaimana Engkau menghidupkan orang mati.” Allah berfirman, “Belum percayakah engkau?” Dia (Ibrahim) menjawab, “Aku percaya, tetapi agar hatiku tenang (mantap).” Dia (Allah) berfi

2:261

# مَثَلُ الَّذِيْنَ يُنْفِقُوْنَ اَمْوَالَهُمْ فِيْ سَبِيْلِ اللّٰهِ كَمَثَلِ حَبَّةٍ اَنْۢبَتَتْ سَبْعَ سَنَابِلَ فِيْ كُلِّ سُنْۢبُلَةٍ مِّائَةُ حَبَّةٍ ۗ وَاللّٰهُ يُضٰعِفُ لِمَنْ يَّشَاۤءُ ۗوَاللّٰهُ وَاسِعٌ عَلِيْمٌ

matsalu **al**la*dz*iina yunfiquuna amw*aa*lahum fii sabiili **al**l*aa*hi kamatsali *h*abbatin anbatat sab'a san*aa*bila fii kulli sunbulatin mi-atu *h*abbatin wa**al**l*aa*hu yu

Perumpamaan orang yang menginfakkan hartanya di jalan Allah seperti sebutir biji yang menumbuhkan tujuh tangkai, pada setiap tangkai ada seratus biji. Allah melipatgandakan bagi siapa yang Dia kehendaki, dan Allah Mahaluas, Maha Mengetahui.

2:262

# اَلَّذِيْنَ يُنْفِقُوْنَ اَمْوَالَهُمْ فِيْ سَبِيْلِ اللّٰهِ ثُمَّ لَا يُتْبِعُوْنَ مَآ اَنْفَقُوْا مَنًّا وَّلَآ اَذًىۙ لَّهُمْ اَجْرُهُمْ عِنْدَ رَبِّهِمْۚ وَلَا خَوْفٌ عَلَيْهِمْ وَلَا هُمْ يَحْزَنُوْنَ

**al**la*dz*iina yunfiquuna amw*aa*lahum fii sabiili **al**l*aa*hi tsumma l*aa* yutbi'uuna m*aa* anfaquu mannan wal*aa* a*dz*an lahum ajruhum 'inda rabbihim wal*aa* khawfun 'alayhim wal*a*

Orang yang menginfakkan hartanya di jalan Allah, kemudian tidak mengiringi apa yang dia infakkan itu dengan menyebut-nyebutnya dan menyakiti (perasaan penerima), mereka memperoleh pahala di sisi Tuhan mereka. Tidak ada rasa takut pada mereka dan mereka ti

2:263

# ۞ قَوْلٌ مَّعْرُوْفٌ وَّمَغْفِرَةٌ خَيْرٌ مِّنْ صَدَقَةٍ يَّتْبَعُهَآ اَذًى ۗ وَاللّٰهُ غَنِيٌّ حَلِيْمٌ

qawlun ma'ruufun wamaghfiratun khayrun min *sh*adaqatin yatba'uh*aa* a*dz*an wa**al**l*aa*hu ghaniyyun *h*aliim**un**

Perkataan yang baik dan pemberian maaf lebih baik daripada sedekah yang diiringi tindakan yang menyakiti. Allah Mahakaya, Maha Penyantun.

2:264

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْا لَا تُبْطِلُوْا صَدَقٰتِكُمْ بِالْمَنِّ وَالْاَذٰىۙ كَالَّذِيْ يُنْفِقُ مَالَهٗ رِئَاۤءَ النَّاسِ وَلَا يُؤْمِنُ بِاللّٰهِ وَالْيَوْمِ الْاٰخِرِۗ فَمَثَلُهٗ كَمَثَلِ صَفْوَانٍ عَلَيْهِ تُرَابٌ فَاَصَابَهٗ وَابِلٌ فَتَرَك

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu l*aa* tub*th*iluu *sh*adaq*aa*tikum bi**a**lmanni wa**a**l-a*dzaa* ka**a**lla*dz*ii yunfiqu m*aa*lahu ri-

Wahai orang-orang yang beriman! Janganlah kamu merusak sedekahmu dengan menyebut-nyebutnya dan menyakiti (perasaan penerima), seperti orang yang menginfakkan hartanya karena ria (pamer) kepada manusia dan dia tidak beriman kepada Allah dan hari akhir. Per

2:265

# وَمَثَلُ الَّذِيْنَ يُنْفِقُوْنَ اَمْوَالَهُمُ ابْتِغَاۤءَ مَرْضَاتِ اللّٰهِ وَتَثْبِيْتًا مِّنْ اَنْفُسِهِمْ كَمَثَلِ جَنَّةٍۢ بِرَبْوَةٍ اَصَابَهَا وَابِلٌ فَاٰتَتْ اُكُلَهَا ضِعْفَيْنِۚ فَاِنْ لَّمْ يُصِبْهَا وَابِلٌ فَطَلٌّ ۗوَاللّٰهُ بِمَا تَعْمَلُو

wamatsalu **al**la*dz*iina yunfiquuna amw*aa*lahumu ibtigh*aa*-a mar*daa*ti **al**l*aa*hi watatsbiitan minanfusihim kamatsali jannatin birabwatin a*shaa*bah*aa* w*aa*bilun fa*aa*tat uk

Dan perumpamaan orang yang menginfakkan hartanya untuk mencari rida Allah dan untuk memperteguh jiwa mereka, seperti sebuah kebun yang terletak di dataran tinggi yang disiram oleh hujan lebat, maka kebun itu menghasilkan buah-buahan dua kali lipat. Jika h

2:266

# اَيَوَدُّ اَحَدُكُمْ اَنْ تَكُوْنَ لَهٗ جَنَّةٌ مِّنْ نَّخِيْلٍ وَّاَعْنَابٍ تَجْرِيْ مِنْ تَحْتِهَا الْاَنْهٰرُۙ لَهٗ فِيْهَا مِنْ كُلِّ الثَّمَرٰتِۙ وَاَصَابَهُ الْكِبَرُ وَلَهٗ ذُرِّيَّةٌ ضُعَفَاۤءُۚ فَاَصَابَهَآ اِعْصَارٌ فِيْهِ نَارٌ فَاحْتَرَقَتْ

ayawaddu a*h*adukum an takuuna lahu jannatun min nakhiilin wa-a'n*aa*bin tajrii min ta*h*tih*aa* **a**l-anh*aa*ru lahu fiih*aa* min kulli **al**tstsamar*aa*ti wa-a*shaa*bahu **a**

**Adakah salah seorang di antara kamu yang ingin memiliki kebun kurma dan anggur yang mengalir di bawahnya sungai-sungai, di sana dia memiliki segala macam buah-buahan, kemudian datanglah masa tuanya sedang dia memiliki keturunan yang masih kecil-kecil. Lal**

2:267

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْٓا اَنْفِقُوْا مِنْ طَيِّبٰتِ مَا كَسَبْتُمْ وَمِمَّآ اَخْرَجْنَا لَكُمْ مِّنَ الْاَرْضِ ۗ وَلَا تَيَمَّمُوا الْخَبِيْثَ مِنْهُ تُنْفِقُوْنَ وَلَسْتُمْ بِاٰخِذِيْهِ اِلَّآ اَنْ تُغْمِضُوْا فِيْهِ ۗ وَاعْلَمُوْٓا اَنَّ اللّ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu anfiquu min *th*ayyib*aa*ti m*aa* kasabtum wamimm*aa* akhrajn*aa* lakum mina **a**l-ar*dh*i wal*aa* tayammamuu **a**lk

Wahai orang-orang yang beriman! Infakkanlah sebagian dari hasil usahamu yang baik-baik dan sebagian dari apa yang Kami keluarkan dari bumi untukmu. Janganlah kamu memilih yang buruk untuk kamu keluarkan, padahal kamu sendiri tidak mau mengambilnya melaink

2:268

# اَلشَّيْطٰنُ يَعِدُكُمُ الْفَقْرَ وَيَأْمُرُكُمْ بِالْفَحْشَاۤءِ ۚ وَاللّٰهُ يَعِدُكُمْ مَّغْفِرَةً مِّنْهُ وَفَضْلًا ۗ وَاللّٰهُ وَاسِعٌ عَلِيْمٌ ۖ

a**l**sysyay*thaa*nu ya'idukumu **a**lfaqra waya/murukum bi**a**lfa*h*sy*aa*-i wa**al**l*aa*hu ya'idukum maghfiratan minhu wafa*dh*lan wa**al**l*aa*hu w<

Setan menjanjikan (menakut-nakuti) kemiskinan kepadamu dan menyuruh kamu berbuat keji (kikir), sedangkan Allah menjanjikan ampunan dan karunia-Nya kepadamu. Dan Allah Mahaluas, Maha Mengetahui.

2:269

# يُّؤْتِى الْحِكْمَةَ مَنْ يَّشَاۤءُ ۚ وَمَنْ يُّؤْتَ الْحِكْمَةَ فَقَدْ اُوْتِيَ خَيْرًا كَثِيْرًا ۗ وَمَا يَذَّكَّرُ اِلَّآ اُولُوا الْاَلْبَابِ

yu/tii **a**l*h*ikmata man yasy*aa*u waman yu/ta **a**l*h*ikmata faqad uutiya khayran katsiiran wam*aa* ya*dzdz*akkaru ill*aa* uluu **a**l-alb*aa*b**i**

Dia memberikan hikmah kepada siapa yang Dia kehendaki. Barangsiapa diberi hikmah, sesungguhnya dia telah diberi kebaikan yang banyak. Dan tidak ada yang dapat mengambil pelajaran kecuali orang-orang yang mempunyai akal sehat.

2:270

# وَمَآ اَنْفَقْتُمْ مِّنْ نَّفَقَةٍ اَوْ نَذَرْتُمْ مِّنْ نَّذْرٍ فَاِنَّ اللّٰهَ يَعْلَمُهٗ ۗ وَمَا لِلظّٰلِمِيْنَ مِنْ اَنْصَارٍ

wam*aa* anfaqtum min nafaqatin aw na*dz*artum min na*dz*rin fa-inna **al**l*aa*ha ya'lamuhu wam*aa* li**l***zhzhaa*limiina min an*shaa*r**in**

Dan apa pun infak yang kamu berikan atau nazar yang kamu janjikan, maka sungguh, Allah mengetahuinya. Dan bagi orang zalim tidak ada seorang penolong pun

2:271

# اِنْ تُبْدُوا الصَّدَقٰتِ فَنِعِمَّا هِيَۚ وَاِنْ تُخْفُوْهَا وَتُؤْتُوْهَا الْفُقَرَاۤءَ فَهُوَ خَيْرٌ لَّكُمْ ۗ وَيُكَفِّرُ عَنْكُمْ مِّنْ سَيِّاٰتِكُمْ ۗ وَاللّٰهُ بِمَا تَعْمَلُوْنَ خَبِيْرٌ

in tubduu **al***shsh*adaq*aa*ti fani'imm*aa *hiya wa-in tukhfuuh*aa *watu/tuuh*aa* **a**lfuqar*aa*\-a fahuwa khayrun lakum wayukaffiru 'ankum min sayyi-*aa*tikum wa**al**l*aa*h

Jika kamu menampakkan sedekah-sedekahmu, maka itu baik. Dan jika kamu menyembunyikannya dan memberikannya kepada orang-orang fakir, maka itu lebih baik bagimu dan Allah akan menghapus sebagian kesalahan-kesalahanmu. Dan Allah Mahateliti apa yang kamu kerj

2:272

# ۞ لَيْسَ عَلَيْكَ هُدٰىهُمْ وَلٰكِنَّ اللّٰهَ يَهْدِيْ مَنْ يَّشَاۤءُ ۗوَمَا تُنْفِقُوْا مِنْ خَيْرٍ فَلِاَنْفُسِكُمْ ۗوَمَا تُنْفِقُوْنَ اِلَّا ابْتِغَاۤءَ وَجْهِ اللّٰهِ ۗوَمَا تُنْفِقُوْا مِنْ خَيْرٍ يُّوَفَّ اِلَيْكُمْ وَاَنْتُمْ لَا تُظْلَمُوْنَ

laysa 'alayka hud*aa*hum wal*aa*kinna **al**l*aa*ha yahdii man yasy*aa*u wam*aa* tunfiquu min khayrin fali-anfusikum wam*aa* tunfiquuna ill*aa* ibtigh*aa*-a wajhi **al**l*aa*hi wam*a*

Bukanlah kewajibanmu (Muhammad) menjadikan mereka mendapat petunjuk, tetapi Allah-lah yang memberi petunjuk kepada siapa yang Dia kehendaki. Apa pun harta yang kamu infakkan, maka (kebaikannya) untuk dirimu sendiri. Dan janganlah kamu berinfak melainkan k

2:273

# لِلْفُقَرَاۤءِ الَّذِيْنَ اُحْصِرُوْا فِيْ سَبِيْلِ اللّٰهِ لَا يَسْتَطِيْعُوْنَ ضَرْبًا فِى الْاَرْضِۖ يَحْسَبُهُمُ الْجَاهِلُ اَغْنِيَاۤءَ مِنَ التَّعَفُّفِۚ تَعْرِفُهُمْ بِسِيْمٰهُمْۚ لَا يَسْـَٔلُوْنَ النَّاسَ اِلْحَافًا ۗوَمَا تُنْفِقُوْا مِنْ خَيْ

lilfuqar*aa*-i **al**la*dz*iina u*hs*iruu fii sabiili **al**l*aa*hi l*aa* yasta*th*ii'uuna *dh*arban fii **a**l-ar*dh*i ya*h*sabuhumu **a**lj*aa*hilu agh

(Apa yang kamu infakkan) adalah untuk orang-orang fakir yang terhalang (usahanya karena jihad) di jalan Allah, sehingga dia yang tidak dapat berusaha di bumi; (orang lain) yang tidak tahu, menyangka bahwa mereka adalah orang-orang kaya karena mereka menja

2:274

# اَلَّذِيْنَ يُنْفِقُوْنَ اَمْوَالَهُمْ بِالَّيْلِ وَالنَّهَارِ سِرًّا وَّعَلَانِيَةً فَلَهُمْ اَجْرُهُمْ عِنْدَ رَبِّهِمْۚ وَلَا خَوْفٌ عَلَيْهِمْ وَلَا هُمْ يَحْزَنُوْنَ

**al**la*dz*iina yunfiquuna amw*aa*lahum bi**a**llayli wa**al**nnah*aa*ri sirran wa'al*aa*niyatan falahum ajruhum 'inda rabbihim wal*aa* khawfun 'alayhim wal*aa* hum ya*h*zanuun

Orang-orang yang menginfakkan hartanya malam dan siang hari (secara) sembunyi-sembunyi maupun terang-terangan, mereka mendapat pahala di sisi Tuhannya. Tidak ada rasa takut pada mereka dan mereka tidak bersedih hati.

2:275

# اَلَّذِيْنَ يَأْكُلُوْنَ الرِّبٰوا لَا يَقُوْمُوْنَ اِلَّا كَمَا يَقُوْمُ الَّذِيْ يَتَخَبَّطُهُ الشَّيْطٰنُ مِنَ الْمَسِّۗ ذٰلِكَ بِاَنَّهُمْ قَالُوْٓا اِنَّمَا الْبَيْعُ مِثْلُ الرِّبٰواۘ وَاَحَلَّ اللّٰهُ الْبَيْعَ وَحَرَّمَ الرِّبٰواۗ فَمَنْ جَاۤءَهٗ

**al**la*dz*iina ya/kuluuna **al**rrib*aa* l*aa* yaquumuuna ill*aa* kam*aa* yaquumu **al**la*dz*ii yatakhabba*th*uhu **al**sysyay*thaa*nu mina **a**

**Orang-orang yang memakan riba tidak dapat berdiri melainkan seperti berdirinya orang yang kemasukan setan karena gila. Yang demikian itu karena mereka berkata bahwa jual beli sama dengan riba. Padahal Allah telah menghalalkan jual beli dan mengharamkan ri**

2:276

# يَمْحَقُ اللّٰهُ الرِّبٰوا وَيُرْبِى الصَّدَقٰتِ ۗ وَاللّٰهُ لَا يُحِبُّ كُلَّ كَفَّارٍ اَثِيْمٍ

yam*h*aqu **al**l*aa*hu **al**rrib*aa* wayurbii **al***shsh*adaq*aa*ti wa**al**l*aa*hu l*aa *yu*h*ibbu kulla kaff*aa*rin atsiim**in**

Allah memusnahkan riba dan menyuburkan sedekah. Allah tidak menyukai setiap orang yang tetap dalam kekafiran dan bergelimang dosa.

2:277

# اِنَّ الَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِ وَاَقَامُوا الصَّلٰوةَ وَاٰتَوُا الزَّكٰوةَ لَهُمْ اَجْرُهُمْ عِنْدَ رَبِّهِمْۚ وَلَا خَوْفٌ عَلَيْهِمْ وَلَا هُمْ يَحْزَنُوْنَ

inna **al**la*dz*iina *aa*manuu wa'amiluu **al***shshaa*li*haa*ti wa-aq*aa*muu **al***shsh*al*aa*ta wa*aa*tawuu **al**zzak*aa*ta lahum ajruhum 'inda rabbihim

Sungguh, orang-orang yang beriman, mengerjakan kebajikan, melaksanakan salat dan menunaikan zakat, mereka mendapat pahala di sisi Tuhannya. Tidak ada rasa takut pada mereka dan mereka tidak bersedih hati.

2:278

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوا اتَّقُوا اللّٰهَ وَذَرُوْا مَا بَقِيَ مِنَ الرِّبٰوٓا اِنْ كُنْتُمْ مُّؤْمِنِيْنَ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu ittaquu **al**l*aa*ha wa*dz*aruu m*aa* baqiya mina **al**rrib*aa* in kuntum mu/miniin**a**

Wahai orang-orang yang beriman! Bertakwalah kepada Allah dan tinggalkan sisa riba (yang belum dipungut) jika kamu orang beriman.

2:279

# فَاِنْ لَّمْ تَفْعَلُوْا فَأْذَنُوْا بِحَرْبٍ مِّنَ اللّٰهِ وَرَسُوْلِهٖۚ وَاِنْ تُبْتُمْ فَلَكُمْ رُءُوْسُ اَمْوَالِكُمْۚ لَا تَظْلِمُوْنَ وَلَا تُظْلَمُوْنَ

fa-in lam taf'aluu fa/*dz*anuu bi*h*arbin mina **al**l*aa*hi warasuulihi wa-in tubtum falakum ruuusu amw*aa*likum l*aa* ta*zh*limuuna wal*aa* tu*zh*lamuun**a**

Jika kamu tidak melaksanakannya, maka umumkanlah perang dari Allah dan Rasul-Nya. Tetapi jika kamu bertobat, maka kamu berhak atas pokok hartamu. Kamu tidak berbuat zalim (merugikan) dan tidak dizalimi (dirugikan).

2:280

# وَاِنْ كَانَ ذُوْ عُسْرَةٍ فَنَظِرَةٌ اِلٰى مَيْسَرَةٍ ۗ وَاَنْ تَصَدَّقُوْا خَيْرٌ لَّكُمْ اِنْ كُنْتُمْ تَعْلَمُوْنَ

wa-in k*aa*na *dz*uu 'usratin fana*zh*iratun il*aa* maysaratin wa-an ta*sh*addaquu khayrun lakum in kuntum ta'lamuun**a**

Dan jika (orang berutang itu) dalam kesulitan, maka berilah tenggang waktu sampai dia memperoleh kelapangan. Dan jika kamu menyedekahkan, itu lebih baik bagimu, jika kamu mengetahui.

2:281

# وَاتَّقُوْا يَوْمًا تُرْجَعُوْنَ فِيْهِ اِلَى اللّٰهِ ۗثُمَّ تُوَفّٰى كُلُّ نَفْسٍ مَّا كَسَبَتْ وَهُمْ لَا يُظْلَمُوْنَ ࣖ

wa**i**ttaquu yawman turja'uuna fiihi il*aa* **al**l*aa*hi tsumma tuwaff*aa* kullu nafsin m*aa* kasabat wahum l*aa* yu*zh*lamuun**a**

Dan takutlah pada hari (ketika) kamu semua dikembalikan kepada Allah. Kemudian setiap orang diberi balasan yang sempurna sesuai dengan apa yang telah dilakukannya, dan mereka tidak dizalimi (dirugikan).

2:282

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْٓا اِذَا تَدَايَنْتُمْ بِدَيْنٍ اِلٰٓى اَجَلٍ مُّسَمًّى فَاكْتُبُوْهُۗ وَلْيَكْتُبْ بَّيْنَكُمْ كَاتِبٌۢ بِالْعَدْلِۖ وَلَا يَأْبَ كَاتِبٌ اَنْ يَّكْتُبَ كَمَا عَلَّمَهُ اللّٰهُ فَلْيَكْتُبْۚ وَلْيُمْلِلِ الَّذِيْ عَلَيْهِ ا

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu i*dzaa* tad*aa*yantum bidaynin il*aa* ajalin musamman fa**u**ktubuuhu walyaktub baynakum k*aa*tibun bi**a**l'adli wal*aa* ya/ba

Wahai orang-orang yang beriman! Apabila kamu melakukan utang piutang untuk waktu yang ditentukan, hendaklah kamu menuliskannya. Dan hendaklah seorang penulis di antara kamu menuliskannya dengan benar. Janganlah penulis menolak untuk menuliskannya sebagaim

2:283

# ۞ وَاِنْ كُنْتُمْ عَلٰى سَفَرٍ وَّلَمْ تَجِدُوْا كَاتِبًا فَرِهٰنٌ مَّقْبُوْضَةٌ ۗفَاِنْ اَمِنَ بَعْضُكُمْ بَعْضًا فَلْيُؤَدِّ الَّذِى اؤْتُمِنَ اَمَانَتَهٗ وَلْيَتَّقِ اللّٰهَ رَبَّهٗ ۗ وَلَا تَكْتُمُوا الشَّهَادَةَۗ وَمَنْ يَّكْتُمْهَا فَاِنَّهٗٓ اٰثِم

wa-in kuntum 'al*aa* safarin walam tajiduu k*aa*tiban farih*aa*nun maqbuu*dh*atun fa-in amina ba'*dh*ukum ba'*dh*an falyu-addi **al**la*dz*iii/tumina am*aa*natahu walyattaqi **al**l

Dan jika kamu dalam perjalanan sedang kamu tidak mendapatkan seorang penulis, maka hendaklah ada barang jaminan yang dipegang. Tetapi, jika sebagian kamu mempercayai sebagian yang lain, hendaklah yang dipercayai itu menunaikan amanatnya (utangnya) dan hen

2:284

# لِلّٰهِ مَا فِى السَّمٰوٰتِ وَمَا فِى الْاَرْضِ ۗ وَاِنْ تُبْدُوْا مَا فِيْٓ اَنْفُسِكُمْ اَوْ تُخْفُوْهُ يُحَاسِبْكُمْ بِهِ اللّٰهُ ۗ فَيَغْفِرُ لِمَنْ يَّشَاۤءُ وَيُعَذِّبُ مَنْ يَّشَاۤءُ ۗ وَاللّٰهُ عَلٰى كُلِّ شَيْءٍ قَدِيْرٌ

lill*aa*hi m*aa* fii **al**ssam*aa*w*aa*ti wam*aa* fii **a**l-ar*dh*i wa-in tubduu m*aa* fii anfusikum aw tukhfuuhu yu*haa*sibkum bihi **al**l*aa*hu fayaghfiru liman yasy<

Milik Allah-lah apa yang ada di langit dan apa yang ada di bumi. Jika kamu nyatakan apa yang ada di dalam hatimu atau kamu sembunyikan, niscaya Allah memperhitungkannya (tentang perbuatan itu) bagimu. Dia mengampuni siapa yang Dia kehendaki dan mengazab s

2:285

# اٰمَنَ الرَّسُوْلُ بِمَآ اُنْزِلَ اِلَيْهِ مِنْ رَّبِّهٖ وَالْمُؤْمِنُوْنَۗ كُلٌّ اٰمَنَ بِاللّٰهِ وَمَلٰۤىِٕكَتِهٖ وَكُتُبِهٖ وَرُسُلِهٖۗ لَا نُفَرِّقُ بَيْنَ اَحَدٍ مِّنْ رُّسُلِهٖ ۗ وَقَالُوْا سَمِعْنَا وَاَطَعْنَا غُفْرَانَكَ رَبَّنَا وَاِلَيْكَ ال

*aa*mana **al**rrasuulu bim*aa* unzila ilayhi min rabbihi wa**a**lmu/minuuna kullun *aa*mana bi**al**l*aa*hi wamal*aa*-ikatihi wakutubihi warusulihi l*aa* nufarriqu bayna a*h*adin m

Rasul (Muhammad) beriman kepada apa yang diturunkan kepadanya (Al-Qur'an) dari Tuhannya, demikian pula orang-orang yang beriman. Semua beriman kepada Allah, malaikat-malaikat-Nya, kitab-kitab-Nya dan rasul-rasul-Nya. (Mereka berkata), “Kami tidak membeda-

2:286

# لَا يُكَلِّفُ اللّٰهُ نَفْسًا اِلَّا وُسْعَهَا ۗ لَهَا مَا كَسَبَتْ وَعَلَيْهَا مَا اكْتَسَبَتْ ۗ رَبَّنَا لَا تُؤَاخِذْنَآ اِنْ نَّسِيْنَآ اَوْ اَخْطَأْنَا ۚ رَبَّنَا وَلَا تَحْمِلْ عَلَيْنَآ اِصْرًا كَمَا حَمَلْتَهٗ عَلَى الَّذِيْنَ مِنْ قَبْلِنَا ۚ

l*aa* yukallifu **al**l*aa*hu nafsan ill*aa* wus'ah*aa* lah*aa* m*aa* kasabat wa'alayh*aa* m*aa* iktasabat rabban*aa* l*aa* tu-*aa*khi*dz*n*aa* in nasiin*aa* aw akh*th*

*Allah tidak membebani seseorang melainkan sesuai dengan kesanggupannya. Dia mendapat (pahala) dari (kebajikan) yang dikerjakannya dan dia mendapat (siksa) dari (kejahatan) yang diperbuatnya. (Mereka berdoa), “Ya Tuhan kami, janganlah Engkau hukum kami jika kami lupa atau kami tersalah. Ya Tuhan kami, janganlah Engkau bebankan kepada kami beban yang berat sebagaimana Engkau bebankan kepada orang-orang sebelum kami. Ya Tuhan kami, janganlah Engkau pikulkan kepada kami apa yang tak sanggup kami memikulnya. Beri maaflah kami; ampunilah kami; dan rahmatilah kami. Engkaulah Penolong kami, maka tolonglah kami terhadap kaum yang kafir”.*

<!--EndFragment-->