---
title: (94) Asy-Syarh - الشرح
date: 2021-10-27T04:30:16.074Z
ayat: 94
description: "Jumlah Ayat: 8 / Arti: Lapang"
---
<!--StartFragment-->

94:1

# اَلَمْ نَشْرَحْ لَكَ صَدْرَكَۙ

alam nasyra*h* laka *sh*adrak**a**

Bukankah Kami telah melapangkan dadamu (Muhammad)?

94:2

# وَوَضَعْنَا عَنْكَ وِزْرَكَۙ

wawa*dh*a'n*aa* 'anka wizrak**a**

dan Kami pun telah menurunkan bebanmu darimu,

94:3

# الَّذِيْٓ اَنْقَضَ ظَهْرَكَۙ

**al**la*dz*ii anqa*dh*a *zh*ahrak**a**

yang memberatkan punggungmu,

94:4

# وَرَفَعْنَا لَكَ ذِكْرَكَۗ

warafa'n*aa* laka *dz*ikrak**a**

dan Kami tinggikan sebutan (nama)mu bagimu.

94:5

# فَاِنَّ مَعَ الْعُسْرِ يُسْرًاۙ

fa-inna ma'a **a**l'usri yusr*aa***n**

Maka sesungguhnya beserta kesulitan ada kemudahan,

94:6

# اِنَّ مَعَ الْعُسْرِ يُسْرًاۗ

inna ma'a **a**l'usri yusr*aa***n**

sesungguhnya beserta kesulitan itu ada kemudahan.

94:7

# فَاِذَا فَرَغْتَ فَانْصَبْۙ

fa-i*dzaa* faraghta fa**i**n*sh*ab

Maka apabila engkau telah selesai (dari sesuatu urusan), tetaplah bekerja keras (untuk urusan yang lain),

94:8

# وَاِلٰى رَبِّكَ فَارْغَبْ ࣖ

wa-il*aa* rabbika fa**i**rghab

dan hanya kepada Tuhanmulah engkau berharap.

<!--EndFragment-->