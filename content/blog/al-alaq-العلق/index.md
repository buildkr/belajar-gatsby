---
title: (96) Al-'Alaq - العلق
date: 2021-10-27T04:31:56.509Z
ayat: 96
description: "Jumlah Ayat: 19 / Arti: Segumpal Darah"
---
<!--StartFragment-->

96:1

# اِقْرَأْ بِاسْمِ رَبِّكَ الَّذِيْ خَلَقَۚ

iqra/ bi-ismi rabbika **al**la*dz*ii khalaq**a**

Bacalah dengan (menyebut) nama Tuhanmu yang menciptakan,

96:2

# خَلَقَ الْاِنْسَانَ مِنْ عَلَقٍۚ

khalaqa **a**l-ins*aa*na min 'alaq**in**

Dia telah menciptakan manusia dari segumpal darah.

96:3

# اِقْرَأْ وَرَبُّكَ الْاَكْرَمُۙ

iqra/ warabbuka **a**l-akram**u**

Bacalah, dan Tuhanmulah Yang Mahamulia,

96:4

# الَّذِيْ عَلَّمَ بِالْقَلَمِۙ

**al**la*dz*ii 'allama bi**a**lqalam**i**

Yang mengajar (manusia) dengan pena.

96:5

# عَلَّمَ الْاِنْسَانَ مَا لَمْ يَعْلَمْۗ

'allama **a**l-ins*aa*na m*aa* lam ya'lam

Dia mengajarkan manusia apa yang tidak diketahuinya.

96:6

# كَلَّآ اِنَّ الْاِنْسَانَ لَيَطْغٰىٓ ۙ

kall*aa* inna **a**l-ins*aa*na laya*th*gh*aa*

Sekali-kali tidak! Sungguh, manusia itu benar-benar melampaui batas,

96:7

# اَنْ رَّاٰهُ اسْتَغْنٰىۗ

an ra*aa*hu istaghn*aa*

apabila melihat dirinya serba cukup.

96:8

# اِنَّ اِلٰى رَبِّكَ الرُّجْعٰىۗ

inna il*aa* rabbika **al**rruj'*aa*

Sungguh, hanya kepada Tuhanmulah tempat kembali(mu).

96:9

# اَرَاَيْتَ الَّذِيْ يَنْهٰىۙ

ara-ayta **al**la*dz*ii yanh*aa*

Bagaimana pendapatmu tentang orang yang melarang?

96:10

# عَبْدًا اِذَا صَلّٰىۗ

'abdan i*dzaa* *sh*all*aa*

seorang hamba ketika dia melaksanakan salat,

96:11

# اَرَاَيْتَ اِنْ كَانَ عَلَى الْهُدٰىٓۙ

ara-ayta in k*aa*na 'al*aa* **a**lhud*aa*

bagaimana pendapatmu jika dia (yang dilarang salat itu) berada di atas kebenaran (petunjuk),

96:12

# اَوْ اَمَرَ بِالتَّقْوٰىۗ

aw amara bi**al**ttaqw*aa*

atau dia menyuruh bertakwa (kepada Allah)?

96:13

# اَرَاَيْتَ اِنْ كَذَّبَ وَتَوَلّٰىۗ

ara-ayta in ka*dzdz*aba watawall*aa*

Bagaimana pendapatmu jika dia (yang melarang) itu mendustakan dan berpaling?

96:14

# اَلَمْ يَعْلَمْ بِاَنَّ اللّٰهَ يَرٰىۗ

alam ya'lam bi-anna **al**l*aa*ha yar*aa*

Tidakkah dia mengetahui bahwa sesungguhnya Allah melihat (segala perbuatannya)?

96:15

# كَلَّا لَىِٕنْ لَّمْ يَنْتَهِ ەۙ لَنَسْفَعًاۢ بِالنَّاصِيَةِۙ

kall*aa* la-in lam yantahi lanasfa'an bi**al**nn*aas*iya**ti**

Sekali-kali tidak! Sungguh, jika dia tidak berhenti (berbuat demikian) niscaya Kami tarik ubun-ubunnya, (ke dalam neraka),

96:16

# نَاصِيَةٍ كَاذِبَةٍ خَاطِئَةٍۚ

n*aas*iyatin k*aadz*ibatin kh*aath*i-a**tin**

yaitu) ubun-ubun orang yang mendustakan dan durhaka.

96:17

# فَلْيَدْعُ نَادِيَهٗۙ

falyad'u n*aa*diyah**u**

Maka biarlah dia memanggil golongannya (untuk menolongnya),

96:18

# سَنَدْعُ الزَّبَانِيَةَۙ

sanad'u **al**zzab*aa*niya**ta**

Kelak Kami akan memanggil Malaikat Zabaniyah, (penyiksa orang-orang yang berdosa),

96:19

# كَلَّاۗ لَا تُطِعْهُ وَاسْجُدْ وَاقْتَرِبْ ۩ ࣖ

kall*aa* l*aa* tu*th*i'hu wa**u**sjud wa**i**qtarib

sekali-kali tidak! Janganlah kamu patuh kepadanya; dan sujudlah serta dekatkanlah (dirimu kepada Allah).

<!--EndFragment-->