---
title: (18) Al-Kahf - الكهف
date: 2021-10-27T03:44:18.679Z
ayat: 18
description: "Jumlah Ayat: 110 / Arti: Goa"
---
<!--StartFragment-->

18:1

# اَلْحَمْدُ لِلّٰهِ الَّذِيْٓ اَنْزَلَ عَلٰى عَبْدِهِ الْكِتٰبَ وَلَمْ يَجْعَلْ لَّهٗ عِوَجًا ۜ

al*h*amdu lill*aa*hi **al**la*dz*ii anzala 'al*aa* 'abdihi **a**lkit*aa*ba walam yaj'al lahu 'iwaj*aa**\*n**

Segala puji bagi Allah yang telah menurunkan Kitab (Al-Qur'an) kepada hamba-Nya dan Dia tidak menjadikannya bengkok;

18:2

# قَيِّمًا لِّيُنْذِرَ بَأْسًا شَدِيْدًا مِّنْ لَّدُنْهُ وَيُبَشِّرَ الْمُؤْمِنِيْنَ الَّذِيْنَ يَعْمَلُوْنَ الصّٰلِحٰتِ اَنَّ لَهُمْ اَجْرًا حَسَنًاۙ

qayyiman liyun*dz*ira ba/san syadiidan min ladunhu wayubasysyira **a**lmu/miniina **al**la*dz*iina ya'maluuna **al***shshaa*li*haa*ti anna lahum ajran* h*asan*aa**\*n**

sebagai bimbingan yang lurus, untuk memperingatkan akan siksa yang sangat pedih dari sisi-Nya dan memberikan kabar gembira kepada orang-orang mukmin yang mengerjakan kebajikan bahwa mereka akan mendapat balasan yang baik,

18:3

# مَّاكِثِيْنَ فِيْهِ اَبَدًاۙ

m*aa*kitsiina fiihi abad*aa**\*n**

mereka kekal di dalamnya untuk selama-lamanya.

18:4

# وَّيُنْذِرَ الَّذِيْنَ قَالُوا اتَّخَذَ اللّٰهُ وَلَدًاۖ

wayun*dz*ira **al**la*dz*iina q*aa*luu ittakha*dz*a **al**l*aa*hu walad*aa**\*n**

Dan untuk memperingatkan kepada orang yang berkata, “Allah mengambil seorang anak.”

18:5

# مَّا لَهُمْ بِهٖ مِنْ عِلْمٍ وَّلَا لِاٰبَاۤىِٕهِمْۗ كَبُرَتْ كَلِمَةً تَخْرُجُ مِنْ اَفْوَاهِهِمْۗ اِنْ يَّقُوْلُوْنَ اِلَّا كَذِبًا

m*aa* lahum bihi min 'ilmin wal*aa* li-*aa*b*aa*-ihim kaburat kalimatan takhruju min afw*aa*hihim in yaquuluuna ill*aa* ka*dz*ib*aa**\*n**

Mereka sama sekali tidak mempunyai pengetahuan tentang hal itu, begitu pula nenek moyang mereka. Alangkah jeleknya kata-kata yang keluar dari mulut mereka; mereka hanya mengatakan (sesuatu) kebohongan belaka.

18:6

# فَلَعَلَّكَ بَاخِعٌ نَّفْسَكَ عَلٰٓى اٰثَارِهِمْ اِنْ لَّمْ يُؤْمِنُوْا بِهٰذَا الْحَدِيْثِ اَسَفًا

fala'allaka b*aa*khi'un nafsaka 'al*aa* *aa*ts*aa*rihim in lam yu/minuu bih*aadzaa* **a**l*h*adiitsi asaf*aa**\*n**

Maka barangkali engkau (Muhammad) akan mencelakakan dirimu karena bersedih hati setelah mereka berpaling, sekiranya mereka tidak beriman kepada keterangan ini (Al-Qur'an).

18:7

# اِنَّا جَعَلْنَا مَا عَلَى الْاَرْضِ زِيْنَةً لَّهَا لِنَبْلُوَهُمْ اَيُّهُمْ اَحْسَنُ عَمَلًا

inn*aa* ja'aln*aa* m*aa* 'al*aa* **a**l-ar*dh*i ziinatan lah*aa* linabluwahum ayyuhum a*h*sanu 'amal*aa**\*n**

Sesungguhnya Kami telah menjadikan apa yang ada di bumi sebagai perhiasan baginya, untuk Kami menguji mereka, siapakah di antaranya yang terbaik perbuatannya.

18:8

# وَاِنَّا لَجَاعِلُوْنَ مَا عَلَيْهَا صَعِيْدًا جُرُزًاۗ

wa-inn*aa* laj*aa*'iluuna m*aa* 'alayh*aa* *sh*a'iidan juruz*aa**\*n**

Dan Kami benar-benar akan menjadikan (pula) apa yang di atasnya menjadi tanah yang tandus lagi kering.

18:9

# اَمْ حَسِبْتَ اَنَّ اَصْحٰبَ الْكَهْفِ وَالرَّقِيْمِ كَانُوْا مِنْ اٰيٰتِنَا عَجَبًا

am *h*asibta anna a*sh*-*haa*ba **a**lkahfi wa**al**rraqiimi k*aa*nuu min *aa*y*aa*tin*aa* 'ajab*aa**\*n**

Apakah engkau mengira bahwa orang yang mendiami gua, dan (yang mempunyai) raqim itu, termasuk tanda-tanda (kebesaran) Kami yang menakjubkan?

18:10

# اِذْ اَوَى الْفِتْيَةُ اِلَى الْكَهْفِ فَقَالُوْا رَبَّنَآ اٰتِنَا مِنْ لَّدُنْكَ رَحْمَةً وَّهَيِّئْ لَنَا مِنْ اَمْرِنَا رَشَدًا

i*dz* aw*aa* **a**lfityatu il*aa* **a**lkahfi faq*aa*luu rabban*aa* *aa*tin*aa* min ladunka ra*h*matan wahayyi/ lan*aa* min amrin*aa* rasyad*aa**\*n**

(Ingatlah) ketika pemuda-pemuda itu berlindung ke dalam gua lalu mereka berdoa, “Ya Tuhan kami. Berikanlah rahmat kepada kami dari sisi-Mu dan sempurnakanlah petunjuk yang lurus bagi kami dalam urusan kami.”

18:11

# فَضَرَبْنَا عَلٰٓى اٰذَانِهِمْ فِى الْكَهْفِ سِنِيْنَ عَدَدًاۙ

fa*dh*arabn*aa* 'al*aa* *aatsaa*nihim fii **a**lkahfi siniina 'adad*aa**\*n**

Maka Kami tutup telinga mereka di dalam gua itu, selama beberapa tahun.

18:12

# ثُمَّ بَعَثْنٰهُمْ لِنَعْلَمَ اَيُّ الْحِزْبَيْنِ اَحْصٰى لِمَا لَبِثُوْٓا اَمَدًا ࣖ

tsumma ba'atsn*aa*hum lina'lama ayyu **a**l*h*izbayni a*hsaa* lim*aa* labitsuu amad*aa**\*n**

Kemudian Kami bangunkan mereka, agar Kami mengetahui manakah di antara ke dua golongan itu yang lebih tepat dalam menghitung berapa lamanya mereka tinggal (dalam gua itu).

18:13

# نَحْنُ نَقُصُّ عَلَيْكَ نَبَاَهُمْ بِالْحَقِّۗ اِنَّهُمْ فِتْيَةٌ اٰمَنُوْا بِرَبِّهِمْ وَزِدْنٰهُمْ هُدًىۖ

na*h*nu naqu*shsh*u 'alayka naba-ahum bi**a**l*h*aqqi innahum fityatun *aa*manuu birabbihim wazidn*aa*hum hud*aa**\*n**

Kami ceritakan kepadamu (Muhammad) kisah mereka dengan sebenarnya. Sesungguhnya mereka adalah pemuda-pemuda yang beriman kepada Tuhan mereka, dan Kami tambahkan petunjuk kepada mereka.

18:14

# وَّرَبَطْنَا عَلٰى قُلُوْبِهِمْ اِذْ قَامُوْا فَقَالُوْا رَبُّنَا رَبُّ السَّمٰوٰتِ وَالْاَرْضِ لَنْ نَّدْعُوَا۟ مِنْ دُوْنِهٖٓ اِلٰهًا لَّقَدْ قُلْنَآ اِذًا شَطَطًا

waraba*th*n*aa* 'al*aa* quluubihim i*dz* q*aa*muu faq*aa*luu rabbun*aa* rabbu **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i lan nad'uwa min duunihi il*aa*han laqad quln*aa* i

Dan Kami teguhkan hati mereka ketika mereka berdiri lalu mereka berkata, “Tuhan kami adalah Tuhan langit dan bumi; kami tidak menyeru tuhan selain Dia. Sungguh, kalau kami berbuat demikian, tentu kami telah mengucapkan perkataan yang sangat jauh dari kebe

18:15

# هٰٓؤُلَاۤءِ قَوْمُنَا اتَّخَذُوْا مِنْ دُوْنِهٖٓ اٰلِهَةًۗ لَوْلَا يَأْتُوْنَ عَلَيْهِمْ بِسُلْطٰنٍۢ بَيِّنٍۗ فَمَنْ اَظْلَمُ مِمَّنِ افْتَرٰى عَلَى اللّٰهِ كَذِبًاۗ

h*aa*ul*aa*-i qawmun*aa* ittakha*dz*uu min duunihi *aa*lihatan lawl*aa* ya/tuuna 'alayhim bisul*thaa*nin bayyinin faman a*zh*lamu mimmani iftar*aa* 'al*aa* **al**l*aa*hi ka*dz*

*Mereka itu kaum kami yang telah menjadikan tuhan-tuhan (untuk disembah) selain Dia. Mengapa mereka tidak mengemukakan alasan yang jelas (tentang kepercayaan mereka)? Maka siapakah yang lebih zalim daripada orang yang mengada-adakan kebohongan terhadap All*

18:16

# وَاِذِ اعْتَزَلْتُمُوْهُمْ وَمَا يَعْبُدُوْنَ اِلَّا اللّٰهَ فَأْوٗٓا اِلَى الْكَهْفِ يَنْشُرْ لَكُمْ رَبُّكُمْ مِّنْ رَّحْمَتِهٖ وَيُهَيِّئْ لَكُمْ مِّنْ اَمْرِكُمْ مِّرْفَقًا

wa-i*dz*i i'tazaltumuuhum wam*aa* ya'buduuna ill*aa* **al**l*aa*ha fa/wuu il*aa* **a**lkahfi yansyur lakum rabbukum min ra*h*matihi wayuhayyi/ lakum min amrikum mirfaq*aa**\*n**

Dan apabila kamu meninggalkan mereka dan apa yang mereka sembah selain Allah, maka carilah tempat berlindung ke dalam gua itu, niscaya Tuhanmu akan melimpahkan sebagian rahmat-Nya kepadamu dan menyediakan sesuatu yang berguna bagimu dalam urusanmu.

18:17

# ۞ وَتَرَى الشَّمْسَ اِذَا طَلَعَتْ تَّزَاوَرُ عَنْ كَهْفِهِمْ ذَاتَ الْيَمِيْنِ وَاِذَا غَرَبَتْ تَّقْرِضُهُمْ ذَاتَ الشِّمَالِ وَهُمْ فِيْ فَجْوَةٍ مِّنْهُۗ ذٰلِكَ مِنْ اٰيٰتِ اللّٰهِ ۗمَنْ يَّهْدِ اللّٰهُ فَهُوَ الْمُهْتَدِ وَمَنْ يُّضْلِلْ فَلَنْ تَجِد

watar*aa* **al**sysyamsa i*dzaa* *th*ala'at taz*aa*waru 'an kahfihim *dzaa*ta **a**lyamiini wa-i*dzaa* gharabat taqri*dh*uhum *dzaa*ta **al**sysyim*aa*li wahum fii fajwati

Dan engkau akan melihat matahari ketika terbit, condong dari gua mereka ke sebelah kanan, dan apabila matahari itu terbenam, menjauhi mereka ke sebelah kiri sedang mereka berada dalam tempat yang luas di dalam (gua) itu. Itulah sebagian dari tanda-tanda (

18:18

# وَتَحْسَبُهُمْ اَيْقَاظًا وَّهُمْ رُقُوْدٌ ۖوَّنُقَلِّبُهُمْ ذَاتَ الْيَمِيْنِ وَذَاتَ الشِّمَالِ ۖوَكَلْبُهُمْ بَاسِطٌ ذِرَاعَيْهِ بِالْوَصِيْدِۗ لَوِ اطَّلَعْتَ عَلَيْهِمْ لَوَلَّيْتَ مِنْهُمْ فِرَارًا وَّلَمُلِئْتَ مِنْهُمْ رُعْبًا

wata*h*sabuhum ayq*aats*an wahum ruquudun wanuqallibuhum *dzaa*ta **a**lyamiini wa*dzaa*ta **al**sysyim*aa*li wakalbuhum b*aa*si*th*un *dz*ir*aa*'ayhi bi**a**lwa*sh*i

Dan engkau mengira mereka itu tidak tidur, padahal mereka tidur; dan Kami bolak-balikkan mereka ke kanan dan ke kiri, sedang anjing mereka membentangkan kedua lengannya di depan pintu gua. Dan jika kamu menyaksikan mereka tentu kamu akan berpaling melarik

18:19

# وَكَذٰلِكَ بَعَثْنٰهُمْ لِيَتَسَاۤءَلُوْا بَيْنَهُمْۗ قَالَ قَاۤىِٕلٌ مِّنْهُمْ كَمْ لَبِثْتُمْۗ قَالُوْا لَبِثْنَا يَوْمًا اَوْ بَعْضَ يَوْمٍۗ قَالُوْا رَبُّكُمْ اَعْلَمُ بِمَا لَبِثْتُمْۗ فَابْعَثُوْٓا اَحَدَكُمْ بِوَرِقِكُمْ هٰذِهٖٓ اِلَى الْمَدِيْنَةِ

waka*dzaa*lika ba'atsn*aa*hum liyatas*aa*-aluu baynahum q*aa*la q*aa*-ilun minhum kam labitstum q*aa*luu labitsn*aa* yawman aw ba'*dh*a yawmin q*aa*luu rabbukum a'lamu bim*aa* labitstum fa**i**

**Dan demikianlah Kami bangunkan mereka, agar di antara mereka saling bertanya. Salah seorang di antara mereka berkata, “Sudah berapa lama kamu berada (di sini)?” Mereka menjawab, “Kita berada (di sini) sehari atau setengah hari.” Berkata (yang lain lagi),**

18:20

# اِنَّهُمْ اِنْ يَّظْهَرُوْا عَلَيْكُمْ يَرْجُمُوْكُمْ اَوْ يُعِيْدُوْكُمْ فِيْ مِلَّتِهِمْ وَلَنْ تُفْلِحُوْٓا اِذًا اَبَدًا

innahum in ya*zh*haruu 'alaykum yarjumuukum aw yu'iiduukum fii millatihim walan tufli*h*uu i*dz*an abad*aa**\*n**

Sesungguhnya jika mereka dapat mengetahui tempatmu, niscaya mereka akan melempari kamu dengan batu, atau memaksamu kembali kepada agama mereka, dan jika demikian niscaya kamu tidak akan beruntung selama-lamanya.”

18:21

# وَكَذٰلِكَ اَعْثَرْنَا عَلَيْهِمْ لِيَعْلَمُوْٓا اَنَّ وَعْدَ اللّٰهِ حَقٌّ وَّاَنَّ السَّاعَةَ لَا رَيْبَ فِيْهَاۚ اِذْ يَتَنَازَعُوْنَ بَيْنَهُمْ اَمْرَهُمْ فَقَالُوا ابْنُوْا عَلَيْهِمْ بُنْيَانًاۗ رَبُّهُمْ اَعْلَمُ بِهِمْۗ قَالَ الَّذِيْنَ غَلَبُوْا

waka*dzaa*lika a'tsarn*aa* 'alayhim liya'lamuu anna wa'da **al**l*aa*hi *h*aqqun wa-anna **al**s*aa*'ata l*aa* rayba fiih*aa* i*dz* yatan*aa*za'uuna baynahum amrahum faq*aa*luu ib

Dan demikian (pula) Kami perlihatkan (manusia) dengan mereka, agar mereka tahu, bahwa janji Allah benar, dan bahwa (kedatangan) hari Kiamat tidak ada keraguan padanya. Ketika mereka berselisih tentang urusan mereka, maka mereka berkata, “Dirikanlah sebuah

18:22

# سَيَقُوْلُوْنَ ثَلٰثَةٌ رَّابِعُهُمْ كَلْبُهُمْۚ وَيَقُوْلُوْنَ خَمْسَةٌ سَادِسُهُمْ كَلْبُهُمْ رَجْمًاۢ بِالْغَيْبِۚ وَيَقُوْلُوْنَ سَبْعَةٌ وَّثَامِنُهُمْ كَلْبُهُمْ ۗقُلْ رَّبِّيْٓ اَعْلَمُ بِعِدَّتِهِمْ مَّا يَعْلَمُهُمْ اِلَّا قَلِيْلٌ ەۗ فَلَا تُمَا

sayaquuluuna tsal*aa*tsatun r*aa*bi'uhum kalbuhum wayaquuluuna khamsatun s*aa*disuhum kalbuhum rajman bi**a**lghaybi wayaquuluuna sab'atun wats*aa*minuhum kalbuhum qul rabbii a'lamu bi'iddatihim m*aa* ya'lamuhum ill

Nanti (ada orang yang akan) mengatakan, ”(Jumlah mereka) tiga (orang), yang ke empat adalah anjingnya,” dan (yang lain) mengatakan, “(Jumlah mereka) lima (orang), yang ke enam adalah anjingnya,” sebagai terkaan terhadap yang gaib; dan (yang lain lagi) men

18:23

# وَلَا تَقُوْلَنَّ لِشَا۟يْءٍ اِنِّيْ فَاعِلٌ ذٰلِكَ غَدًاۙ

wal*aa* taquulanna lisyay-in innii f*aa*'ilun *dzaa*lika ghad*aa**\*n**

Dan jangan sekali-kali engkau mengatakan terhadap sesuatu, “Aku pasti melakukan itu besok pagi,”

18:24

# اِلَّآ اَنْ يَّشَاۤءَ اللّٰهُ ۖوَاذْكُرْ رَّبَّكَ اِذَا نَسِيْتَ وَقُلْ عَسٰٓى اَنْ يَّهْدِيَنِ رَبِّيْ لِاَقْرَبَ مِنْ هٰذَا رَشَدًا

ill*aa* an yasy*aa*-a **al**l*aa*hu wa**u***dz*kur rabbaka i*dzaa *nasiita waqul 'as*aa *an yahdiyani rabbii li-aqraba min h*aadzaa *rasyad*aa**\*n**

kecuali (dengan mengatakan), “Insya Allah.” Dan ingatlah kepada Tuhanmu apabila engkau lupa dan katakanlah, “Mudah-mudahan Tuhanku akan memberiku petunjuk kepadaku agar aku yang lebih dekat (kebenarannya) daripada ini.”

18:25

# وَلَبِثُوْا فِيْ كَهْفِهِمْ ثَلٰثَ مِائَةٍ سِنِيْنَ وَازْدَادُوْا تِسْعًا

walabitsuu fii kahfihim tsal*aa*tsa mi-atin siniina wa**i**zd*aa*duu tis'*aa**\*n**

Dan mereka tinggal dalam gua selama tiga ratus tahun dan ditambah sembilan tahun.

18:26

# قُلِ اللّٰهُ اَعْلَمُ بِمَا لَبِثُوْا ۚ لَهٗ غَيْبُ السَّمٰوٰتِ وَالْاَرْضِۗ اَبْصِرْ بِهٖ وَاَسْمِعْۗ مَا لَهُمْ مِّنْ دُوْنِهٖ مِنْ وَّلِيٍّۗ وَلَا يُشْرِكُ فِيْ حُكْمِهٖٓ اَحَدًا

quli **al**l*aa*hu a'lamu bim*aa* labitsuu lahu ghaybu **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i ab*sh*ir bihi wa-asmi' m*aa* lahum min duunihi min waliyyin wal*aa* yusyriku fii <

Katakanlah, “Allah lebih mengetahui berapa lamanya mereka tinggal (di gua); milik-Nya semua yang tersembunyi di langit dan di bumi. Alangkah terang penglihatan-Nya dan alangkah tajam pendengaran-Nya; tidak ada seorang pelindung pun bagi mereka selain Dia;

18:27

# وَاتْلُ مَآ اُوْحِيَ اِلَيْكَ مِنْ كِتَابِ رَبِّكَۗ لَا مُبَدِّلَ لِكَلِمٰتِهٖۗ وَلَنْ تَجِدَ مِنْ دُوْنِهٖ مُلْتَحَدًا

wa**u**tlu m*aa* uu*h*iya ilayka min kit*aa*bi rabbika l*aa* mubaddila likalim*aa*tihi walan tajida min duunihi multa*h*ad*aa**\*n**

Dan bacakanlah (Muhammad) apa yang diwahyukan kepadamu, yaitu Kitab Tuhanmu (Al-Qur'an). Tidak ada yang dapat mengubah kalimat-kalimat-Nya. Dan engkau tidak akan dapat menemukan tempat berlindung selain kepada-Nya.

18:28

# وَاصْبِرْ نَفْسَكَ مَعَ الَّذِيْنَ يَدْعُوْنَ رَبَّهُمْ بِالْغَدٰوةِ وَالْعَشِيِّ يُرِيْدُوْنَ وَجْهَهٗ وَلَا تَعْدُ عَيْنٰكَ عَنْهُمْۚ تُرِيْدُ زِيْنَةَ الْحَيٰوةِ الدُّنْيَاۚ وَلَا تُطِعْ مَنْ اَغْفَلْنَا قَلْبَهٗ عَنْ ذِكْرِنَا وَاتَّبَعَ هَوٰىهُ وَكَا

wa**i***sh*bir nafsaka ma'a **al**la*dz*iina yad'uuna rabbahum bi**a**lghad*aa*ti wa**a**l'asyiyyi yuriiduuna wajhahu wal*aa *ta'du 'ayn*aa*ka 'anhum turiidu ziinata **a**

**Dan bersabarlah engkau (Muhammad) bersama orang yang menyeru Tuhannya pada pagi dan senja hari dengan mengharap keridaan-Nya; dan janganlah kedua matamu berpaling dari mereka (karena) mengharapkan perhiasan kehidupan dunia; dan janganlah engkau mengikuti**

18:29

# وَقُلِ الْحَقُّ مِنْ رَّبِّكُمْۗ فَمَنْ شَاۤءَ فَلْيُؤْمِنْ وَّمَنْ شَاۤءَ فَلْيَكْفُرْۚ اِنَّآ اَعْتَدْنَا لِلظّٰلِمِيْنَ نَارًاۙ اَحَاطَ بِهِمْ سُرَادِقُهَاۗ وَاِنْ يَّسْتَغِيْثُوْا يُغَاثُوْا بِمَاۤءٍ كَالْمُهْلِ يَشْوِى الْوُجُوْهَۗ بِئْسَ الشَّرَابُ

waquli **a**l*h*aqqu min rabbikum faman sy*aa*-a falyu/min waman sy*aa*-a falyakfur inn*aa* a'tadn*aa* li**l***zhzhaa*limiina n*aa*ran a*hath*a bihim sur*aa*diquh*aa* wa-in yastag

Dan katakanlah (Muhammad), “Kebenaran itu datangnya dari Tuhanmu; barangsiapa menghendaki (beriman) hendaklah dia beriman, dan barangsiapa menghendaki (kafir) biarlah dia kafir.” Sesungguhnya Kami telah menyediakan neraka bagi orang zalim, yang gejolaknya

18:30

# اِنَّ الَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِ اِنَّا لَا نُضِيْعُ اَجْرَ مَنْ اَحْسَنَ عَمَلًاۚ

inna **al**la*dz*iina *aa*manuu wa'amiluu **al***shshaa*li*haa*ti inn*aa *l*aa *nu*dh*ii'u ajra man a*h*sana 'amal*aa**\*n**

Sungguh, mereka yang beriman dan mengerjakan kebajikan, Kami benar-benar tidak akan menyia-nyiakan pahala orang yang mengerjakan perbuatan yang baik itu.

18:31

# اُولٰۤىِٕكَ لَهُمْ جَنّٰتُ عَدْنٍ تَجْرِيْ مِنْ تَحْتِهِمُ الْاَنْهٰرُ يُحَلَّوْنَ فِيْهَا مِنْ اَسَاوِرَ مِنْ ذَهَبٍ وَّيَلْبَسُوْنَ ثِيَابًا خُضْرًا مِّنْ سُنْدُسٍ وَّاِسْتَبْرَقٍ مُّتَّكِىِٕيْنَ فِيْهَا عَلَى الْاَرَاۤىِٕكِۗ نِعْمَ الثَّوَابُۗ وَحَسُنَ

ul*aa*-ika lahum jann*aa*tu 'adnin tajrii min ta*h*tihimu **a**l-anh*aa*ru yu*h*allawna fiih*aa* min as*aa*wira min *dz*ahabin way**a**lbasuuna tsiy*aa*ban khu*dh*ran min sundusin

Mereka itulah yang memperoleh Surga ‘Adn, yang mengalir di bawahnya sungai-sungai; (dalam surga itu) mereka diberi hiasan gelang emas dan mereka memakai pakaian hijau dari sutera halus dan sutera tebal, sedang mereka duduk sambil bersandar di atas dipan-d

18:32

# ۞ وَاضْرِبْ لَهُمْ مَّثَلًا رَّجُلَيْنِ جَعَلْنَا لِاَحَدِهِمَا جَنَّتَيْنِ مِنْ اَعْنَابٍ وَّحَفَفْنٰهُمَا بِنَخْلٍ وَّجَعَلْنَا بَيْنَهُمَا زَرْعًاۗ

wa**i***dh*rib lahum matsalan rajulayni ja'aln*aa *li-a*h*adihim*aa *jannatayni min a'n*aa*bin wa*h*afafn*aa*hum*aa *binakhlin waja'aln*aa *baynahum*aa *zar'*aa**\*n**

Dan berikanlah (Muhammad) kepada mereka sebuah perumpamaan, dua orang laki-laki, yang seorang (yang kafir) Kami beri dua buah kebun anggur dan Kami kelilingi kedua kebun itu dengan pohon-pohon kurma dan di antara keduanya (kebun itu) Kami buatkan ladang.

18:33

# كِلْتَا الْجَنَّتَيْنِ اٰتَتْ اُكُلَهَا وَلَمْ تَظْلِمْ مِّنْهُ شَيْـًٔاۙ وَّفَجَّرْنَا خِلٰلَهُمَا نَهَرًاۙ

kilt*aa* **a**ljannatayni *aa*tat ukulah*aa* walam ta*zh*lim minhu syay-an wafajjarn*aa* khil*aa*lahum*aa* nahar*aa**\*n**

Kedua kebun itu menghasilkan buahnya, dan tidak berkurang (buahnya) sedikit pun, dan di celah-celah kedua kebun itu Kami alirkan sungai,

18:34

# وَّكَانَ لَهٗ ثَمَرٌۚ فَقَالَ لِصَاحِبِهٖ وَهُوَ يُحَاوِرُهٗٓ اَنَا۠ اَكْثَرُ مِنْكَ مَالًا وَّاَعَزُّ نَفَرًا

wak*aa*na lahu tsamarun faq*aa*la li*shaah*ibihi wahuwa yu*haa*wiruhu an*aa* aktsaru minka m*aa*lan wa-a'azzu nafar*aa**\*n**

dan dia memiliki kekayaan besar, maka dia berkata kepada kawannya (yang beriman) ketika bercakap-cakap dengan dia, “Hartaku lebih banyak daripada hartamu dan pengikutku lebih kuat.”

18:35

# وَدَخَلَ جَنَّتَهٗ وَهُوَ ظَالِمٌ لِّنَفْسِهٖۚ قَالَ مَآ اَظُنُّ اَنْ تَبِيْدَ هٰذِهٖٓ اَبَدًاۙ

wadakhala jannatahu wahuwa *zhaa*limun linafsihi q*aa*la m*aa* a*zh*unnu an tabiida h*aadz*ihi abad*aa**\*n**

Dan dia memasuki kebunnya dengan sikap merugikan dirinya sendiri (karena angkuh dan kafir); dia berkata, “Aku kira kebun ini tidak akan binasa selama-lamanya,

18:36

# وَّمَآ اَظُنُّ السَّاعَةَ قَاۤىِٕمَةً وَّلَىِٕنْ رُّدِدْتُّ اِلٰى رَبِّيْ لَاَجِدَنَّ خَيْرًا مِّنْهَا مُنْقَلَبًا

wam*aa* a*zh*unnu **al**ss*aa*'ata q*aa*-imatan wala-in rudidtu il*aa* rabbii la-ajidanna khayran minh*aa* munqalab*aa**\*n**

dan aku kira hari Kiamat itu tidak akan datang, dan sekiranya aku dikembalikan kepada Tuhanku, pasti aku akan mendapat tempat kembali yang lebih baik dari pada ini.”

18:37

# قَالَ لَهٗ صَاحِبُهٗ وَهُوَ يُحَاوِرُهٗٓ اَكَفَرْتَ بِالَّذِيْ خَلَقَكَ مِنْ تُرَابٍ ثُمَّ مِنْ نُّطْفَةٍ ثُمَّ سَوّٰىكَ رَجُلًاۗ

q*aa*la lahu *shaah*ibuhu wahuwa yu*haa*wiruhu akafarta bi**a**lla*dz*ii khalaqaka min tur*aa*bin tsumma min nu*th*fatin tsumma saww*aa*ka rajul*aa**\*n**

Kawannya (yang beriman) berkata kepadanya sambil bercakap-cakap dengannya, “Apakah engkau ingkar kepada (Tuhan) yang menciptakan engkau dari tanah, kemudian dari setetes air mani, lalu Dia menjadikan engkau seorang laki-laki yang sempurna?

18:38

# لٰكِنَّا۠ هُوَ اللّٰهُ رَبِّيْ وَلَآ اُشْرِكُ بِرَبِّيْٓ اَحَدًا

l*aa*kinna huwa **al**l*aa*hu rabbii wal*aa* usyriku birabbii a*h*ad*aa**\*n**

Tetapi aku (percaya bahwa), Dialah Allah, Tuhanku, dan aku tidak mempersekutukan Tuhanku dengan sesuatu pun.

18:39

# وَلَوْلَآ اِذْ دَخَلْتَ جَنَّتَكَ قُلْتَ مَا شَاۤءَ اللّٰهُ ۙ لَا قُوَّةَ اِلَّا بِاللّٰهِ ۚاِنْ تَرَنِ اَنَا۠ اَقَلَّ مِنْكَ مَالًا وَّوَلَدًاۚ

walawl*aa* i*dz* dakhalta jannataka qulta m*aa* sy*aa*-a **al**l*aa*hu l*aa* quwwata ill*aa* bi**al**l*aa*hi in tarani an*aa* aqalla minka m*aa*lan wawalad*aa**\*n**

**Dan mengapa ketika engkau memasuki kebunmu tidak mengucapkan ”Masya Allah, la quwwata illa billah” (Sungguh, atas kehendak Allah, semua ini terwujud), tidak ada kekuatan kecuali dengan (pertolongan) Allah, sekalipun engkau anggap harta dan keturunanku leb**

18:40

# فَعَسٰى رَبِّيْٓ اَنْ يُّؤْتِيَنِ خَيْرًا مِّنْ جَنَّتِكَ وَيُرْسِلَ عَلَيْهَا حُسْبَانًا مِّنَ السَّمَاۤءِ فَتُصْبِحَ صَعِيْدًا زَلَقًاۙ

fa'as*aa* rabbii an yu/tiyani khayran min jannatika wayursila 'alayh*aa* *h*usb*aa*nan mina **al**ssam*aa*-i fatu*sh*bi*h*a *sh*a'iidan zalaq*aa**\*n**

Maka mudah-mudahan Tuhanku, akan memberikan kepadaku (kebun) yang lebih baik dari kebunmu (ini); dan Dia mengirimkan petir dari langit ke kebunmu, sehingga (kebun itu) menjadi tanah yang licin,

18:41

# اَوْ يُصْبِحَ مَاۤؤُهَا غَوْرًا فَلَنْ تَسْتَطِيْعَ لَهٗ طَلَبًا

aw yu*sh*bi*h*a m*aa*uh*aa* ghawran falan tasta*th*ii'a lahu *th*alab*aa**\*n**

atau airnya menjadi surut ke dalam tanah, maka engkau tidak akan dapat menemukannya lagi.”

18:42

# وَاُحِيْطَ بِثَمَرِهٖ فَاَصْبَحَ يُقَلِّبُ كَفَّيْهِ عَلٰى مَآ اَنْفَقَ فِيْهَا وَهِيَ خَاوِيَةٌ عَلٰى عُرُوْشِهَا وَيَقُوْلُ يٰلَيْتَنِيْ لَمْ اُشْرِكْ بِرَبِّيْٓ اَحَدًا

wau*h*ii*th*a bitsamarihi fa-a*sh*ba*h*a yuqallibu kaffayhi 'al*aa* m*aa* anfaqa fiih*aa* wahiya kh*aa*wiyatun 'al*aa* 'uruusyih*aa* wayaquulu y*aa* laytanii lam usyrik birabbii a*h*ad*aa*

Dan harta kekayaannya dibinasakan, lalu dia membolak-balikkan kedua telapak tangannya (tanda menyesal) terhadap apa yang telah dia belanjakan untuk itu, sedang pohon anggur roboh bersama penyangganya (para-para) lalu dia berkata, “Betapa sekiranya dahulu

18:43

# وَلَمْ تَكُنْ لَّهٗ فِئَةٌ يَّنْصُرُوْنَهٗ مِنْ دُوْنِ اللّٰهِ وَمَا كَانَ مُنْتَصِرًاۗ

walam takun lahu fi-atun yan*sh*uruunahu min duuni **al**l*aa*hi wam*aa* k*aa*na munta*sh*ir*aa**\*n**

Dan tidak ada (lagi) baginya segolongan pun yang dapat menolongnya selain Allah; dan dia pun tidak akan dapat membela dirinya.

18:44

# هُنَالِكَ الْوَلَايَةُ لِلّٰهِ الْحَقِّۗ هُوَ خَيْرٌ ثَوَابًا وَّخَيْرٌ عُقْبًا ࣖ

hun*aa*lika **a**lwal*aa*yatu lill*aa*hi **a**l*h*aqqi huwa khayrun tsaw*aa*ban wakhayrun 'uqb*aa**\*n**

Di sana, pertolongan itu hanya dari Allah Yang Mahabenar. Dialah (pemberi) pahala terbaik dan (pemberi) balasan terbaik.

18:45

# وَاضْرِبْ لَهُمْ مَّثَلَ الْحَيٰوةِ الدُّنْيَا كَمَاۤءٍ اَنْزَلْنٰهُ مِنَ السَّمَاۤءِ فَاخْتَلَطَ بِهٖ نَبَاتُ الْاَرْضِ فَاَصْبَحَ هَشِيْمًا تَذْرُوْهُ الرِّيٰحُ ۗوَكَانَ اللّٰهُ عَلٰى كُلِّ شَيْءٍ مُّقْتَدِرًا

wa**i***dh*rib lahum matsala **a**l*h*ay*aa*ti **al**dduny*aa *kam*aa*\-in anzaln*aa*hu mina **al**ssam*aa*\-i fa**i**khtala*th*a bihi nab*aa*tu

Dan buatkanlah untuk mereka (manusia) perumpamaan kehidupan dunia ini, ibarat air (hujan) yang Kami turunkan dari langit, sehingga menyuburkan tumbuh-tumbuhan di bumi, kemudian (tumbuh-tumbuhan) itu menjadi kering yang diterbangkan oleh angin. Dan Allah M

18:46

# اَلْمَالُ وَالْبَنُوْنَ زِيْنَةُ الْحَيٰوةِ الدُّنْيَاۚ وَالْبٰقِيٰتُ الصّٰلِحٰتُ خَيْرٌ عِنْدَ رَبِّكَ ثَوَابًا وَّخَيْرٌ اَمَلًا

alm*aa*lu wa**a**lbanuuna ziinatu **a**l*h*ay*aa*ti **al**dduny*aa* wa**a**lb*aa*qiy*aa*tu **al***shshaa*li*haa*tu khayrun 'inda rabbika tsaw*aa*

Harta dan anak-anak adalah perhiasan kehidupan dunia tetapi amal kebajikan yang terus-menerus adalah lebih baik pahalanya di sisi Tuhanmu serta lebih baik untuk menjadi harapan.

18:47

# وَيَوْمَ نُسَيِّرُ الْجِبَالَ وَتَرَى الْاَرْضَ بَارِزَةًۙ وَّحَشَرْنٰهُمْ فَلَمْ نُغَادِرْ مِنْهُمْ اَحَدًاۚ

wayawma nusayyiru **a**ljib*aa*la watar*aa* **a**l-ar*dh*a b*aa*rizatan wa*h*asyarn*aa*hum falam nugh*aa*dir minhum a*h*ad*aa**\*n**

Dan (ingatlah) pada hari (ketika) Kami perjalankan gunung-gunung dan engkau akan melihat bumi itu rata dan Kami kumpulkan mereka (seluruh manusia), dan tidak Kami tinggalkan seorang pun dari mereka.

18:48

# وَعُرِضُوْا عَلٰى رَبِّكَ صَفًّاۗ لَقَدْ جِئْتُمُوْنَا كَمَا خَلَقْنٰكُمْ اَوَّلَ مَرَّةٍۢ ۖبَلْ زَعَمْتُمْ اَلَّنْ نَّجْعَلَ لَكُمْ مَّوْعِدًا

wa'uri*dh*uu 'al*aa* rabbika *sh*affan laqad ji/tumuun*aa* kam*aa* khalaqn*aa*kum awwala marratin bal za'amtum **al**lan naj'ala lakum maw'id*aa**\*n**

Dan mereka akan dibawa ke hadapan Tuhanmu dengan berbaris. (Allah berfirman), “Sesungguhnya kamu datang kepada Kami, sebagaimana Kami menciptakan kamu pada pertama kali; bahkan kamu menganggap bahwa Kami tidak akan menetapkan bagi kamu waktu (berbangkit u

18:49

# وَوُضِعَ الْكِتٰبُ فَتَرَى الْمُجْرِمِيْنَ مُشْفِقِيْنَ مِمَّا فِيْهِ وَيَقُوْلُوْنَ يٰوَيْلَتَنَا مَالِ هٰذَا الْكِتٰبِ لَا يُغَادِرُ صَغِيْرَةً وَّلَا كَبِيْرَةً اِلَّآ اَحْصٰىهَاۚ وَوَجَدُوْا مَا عَمِلُوْا حَاضِرًاۗ وَلَا يَظْلِمُ رَبُّكَ اَحَدًا ࣖ

wawu*dh*i'a **a**lkit*aa*bu fatar*aa* **a**lmujrimiina musyfiqiina mimm*aa* fiihi wayaquuluuna y*aa* waylatan*aa* m*aa* lih*aadzaa* **a**lkit*aa*bi l*aa* yugh*aa*

*Dan diletakkanlah kitab (catatan amal), lalu engkau akan melihat orang yang berdosa merasa ketakutan terhadap apa yang (tertulis) di dalamnya, dan mereka berkata, “Betapa celaka kami, kitab apakah ini, tidak ada yang tertinggal, yang kecil dan yang besar*

18:50

# وَاِذْ قُلْنَا لِلْمَلٰۤىِٕكَةِ اسْجُدُوْا لِاٰدَمَ فَسَجَدُوْٓا اِلَّآ اِبْلِيْسَۗ كَانَ مِنَ الْجِنِّ فَفَسَقَ عَنْ اَمْرِ رَبِّهٖۗ اَفَتَتَّخِذُوْنَهٗ وَذُرِّيَّتَهٗٓ اَوْلِيَاۤءَ مِنْ دُوْنِيْ وَهُمْ لَكُمْ عَدُوٌّۗ بِئْسَ لِلظّٰلِمِيْنَ بَدَلًا

wa-i*dz* quln*aa* lilmal*aa*-ikati usjuduu li-*aa*dama fasajaduu ill*aa* ibliisa k*aa*na mina **a**ljinni fafasaqa 'an amri rabbihi afatattakhi*dz*uunahu wa*dz*urriyyatahu awliy*aa*-a min duunii wa

Dan (ingatlah) ketika Kami berfirman kepada para malaikat, “Sujudlah kamu kepada Adam!” Maka mereka pun sujud kecuali Iblis. Dia adalah dari (golongan) jin, maka dia mendurhakai perintah Tuhannya. Pantaskah kamu menjadikan dia dan keturunannya sebagai pem

18:51

# ۞ مَآ اَشْهَدْتُّهُمْ خَلْقَ السَّمٰوٰتِ وَالْاَرْضِ وَلَا خَلْقَ اَنْفُسِهِمْۖ وَمَا كُنْتُ مُتَّخِذَ الْمُضِلِّيْنَ عَضُدًا

m*aa* asyhadtuhum khalqa **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i wal*aa* khalqa anfusihim wam*aa* kuntu muttakhi*dz*a **a**lmu*dh*illiina 'a*dh*ud*aa**\*n**

**Aku tidak menghadirkan mereka (Iblis dan anak cucunya) untuk menyaksikan penciptaan langit dan bumi dan tidak (pula) penciptaan diri mereka sendiri; dan Aku tidak menjadikan orang yang menyesatkan itu sebagai penolong.**

18:52

# وَيَوْمَ يَقُوْلُ نَادُوْا شُرَكَاۤءِيَ الَّذِيْنَ زَعَمْتُمْ فَدَعَوْهُمْ فَلَمْ يَسْتَجِيْبُوْا لَهُمْ وَجَعَلْنَا بَيْنَهُمْ مَّوْبِقًا

wayawma yaquulu n*aa*duu syurak*aa*-iya **al**la*dz*iina za'amtum fada'awhum falam yastajiibuu lahum waja'aln*aa* baynahum mawbiq*aa**\*n**

Dan (ingatlah) pada hari (ketika) Dia berfirman, “Panggillah olehmu sekutu-sekutu-Ku yang kamu anggap itu.” Mereka lalu memanggilnya, tetapi mereka (sekutu-sekutu) tidak membalas (seruan) mereka dan Kami adakan untuk mereka tempat kebinasaan (neraka).

18:53

# وَرَاَ الْمُجْرِمُوْنَ النَّارَ فَظَنُّوْٓا اَنَّهُمْ مُّوَاقِعُوْهَا وَلَمْ يَجِدُوْا عَنْهَا مَصْرِفًا ࣖ

wara*aa* **a**lmujrimuuna **al**nn*aa*ra fa*zh*annuu annahum muw*aa*qi'uuh*aa* walam yajiduu 'anh*aa* ma*sh*rif*aa**\*n**

Dan orang yang berdosa melihat neraka, lalu mereka menduga, bahwa mereka akan jatuh ke dalamnya, dan mereka tidak menemukan tempat berpaling darinya.

18:54

# وَلَقَدْ صَرَّفْنَا فِيْ هٰذَا الْقُرْاٰنِ لِلنَّاسِ مِنْ كُلِّ مَثَلٍۗ وَكَانَ الْاِنْسَانُ اَكْثَرَ شَيْءٍ جَدَلًا

walaqad *sh*arrafn*aa* fii h*aadzaa* **a**lqur-*aa*ni li**l**nn*aa*si min kulli matsalin wak*aa*na **a**l-ins*aa*nu aktsara syay-in jadal*aa**\*n**

Dan sesungguhnya Kami telah menjelaskan berulang-ulang kepada manusia dalam Al-Qur'an ini dengan bermacam-macam perumpamaan. Tetapi manusia adalah memang yang paling banyak membantah.

18:55

# وَمَا مَنَعَ النَّاسَ اَنْ يُّؤْمِنُوْٓا اِذْ جَاۤءَهُمُ الْهُدٰى وَيَسْتَغْفِرُوْا رَبَّهُمْ اِلَّآ اَنْ تَأْتِيَهُمْ سُنَّةُ الْاَوَّلِيْنَ اَوْ يَأْتِيَهُمُ الْعَذَابُ قُبُلًا

wam*aa* mana'a **al**nn*aa*sa an yu/minuu i*dz* j*aa*-ahumu **a**lhud*aa* wayastaghfiruu rabbahum ill*aa* an ta/tiyahum sunnatu **a**l-awwaliina aw ya/tiyahumu **a**l'a*dz*

Dan tidak ada (sesuatu pun) yang menghalangi manusia untuk beriman ketika petunjuk telah datang kepada mereka dan memohon ampunan kepada Tuhannya, kecuali (keinginan menanti) datangnya hukum (Allah yang telah berlaku pada) umat yang terdahulu atau datangn

18:56

# وَمَا نُرْسِلُ الْمُرْسَلِيْنَ اِلَّا مُبَشِّرِيْنَ وَمُنْذِرِيْنَۚ وَيُجَادِلُ الَّذِيْنَ كَفَرُوْا بِالْبَاطِلِ لِيُدْحِضُوْا بِهِ الْحَقَّ وَاتَّخَذُوْٓا اٰيٰتِيْ وَمَآ اُنْذِرُوْا هُزُوًا

wam*aa* nursilu **a**lmursaliina ill*aa* mubasysyiriina wamun*dz*iriina wayuj*aa*dilu **al**la*dz*iina kafaruu bi**a**lb*aath*ili liyud*h*i*dh*uu bihi **a**l*h*

*Dan Kami tidak mengutus rasul-rasul melainkan sebagai pembawa kabar gembira dan pemberi peringatan; tetapi orang yang kafir membantah dengan (cara) yang batil agar dengan demikian mereka dapat melenyapkan yang hak (kebenaran), dan mereka menjadikan ayat-a*

18:57

# وَمَنْ اَظْلَمُ مِمَّنْ ذُكِّرَ بِاٰيٰتِ رَبِّهٖ فَاَعْرَضَ عَنْهَا وَنَسِيَ مَا قَدَّمَتْ يَدَاهُۗ اِنَّا جَعَلْنَا عَلٰى قُلُوْبِهِمْ اَكِنَّةً اَنْ يَّفْقَهُوْهُ وَفِيْٓ اٰذَانِهِمْ وَقْرًاۗ وَاِنْ تَدْعُهُمْ اِلَى الْهُدٰى فَلَنْ يَّهْتَدُوْٓا اِذًا ا

waman a*zh*lamu mimman *dz*ukkira bi-*aa*y*aa*ti rabbihi fa-a'ra*dh*a 'anh*aa* wanasiya m*aa* qaddamat yad*aa*hu inn*aa* ja'aln*aa* 'al*aa* quluubihim akinnatan an yafqahuuhu wafii *aatsaa*nihim

Dan siapakah yang lebih zalim dari pada orang yang telah diperingatkan dengan ayat-ayat Tuhannya, lalu dia berpaling darinya dan melupakan apa yang telah dikerjakan oleh kedua tangannya? Sungguh, Kami telah menjadikan hati mereka tertutup, (sehingga merek

18:58

# وَرَبُّكَ الْغَفُوْرُ ذُو الرَّحْمَةِۗ لَوْ يُؤَاخِذُهُمْ بِمَا كَسَبُوْا لَعَجَّلَ لَهُمُ الْعَذَابَۗ بَلْ لَّهُمْ مَّوْعِدٌ لَّنْ يَّجِدُوْا مِنْ دُوْنِهٖ مَوْىِٕلًا

warabbuka **a**lghafuuru *dz*uu **al**rra*h*mati law yu-*aa*khi*dz*uhum bim*aa* kasabuu la'ajjala lahumu **a**l'a*dzaa*ba bal lahum maw'idun lan yajiduu min duunihi maw-il*aa*

Dan Tuhanmu Maha Pengampun, memiliki kasih sayang. Jika Dia hendak menyiksa mereka karena perbuatan mereka, tentu Dia akan menyegerakan siksa bagi mereka. Tetapi bagi mereka ada waktu tertentu (untuk mendapat siksa) yang mereka tidak akan menemukan tempat

18:59

# وَتِلْكَ الْقُرٰٓى اَهْلَكْنٰهُمْ لَمَّا ظَلَمُوْا وَجَعَلْنَا لِمَهْلِكِهِمْ مَّوْعِدًا ࣖ

watilka **a**lqur*aa* ahlakn*aa*hum lamm*aa* *zh*alamuu waja'aln*aa* limahlikihim maw'id*aa**\*n**

Dan (penduduk) negeri itu telah Kami binasakan ketika mereka berbuat zalim, dan telah Kami tetapkan waktu tertentu bagi kebinasaan mereka.

18:60

# وَاِذْ قَالَ مُوْسٰى لِفَتٰىهُ لَآ اَبْرَحُ حَتّٰٓى اَبْلُغَ مَجْمَعَ الْبَحْرَيْنِ اَوْ اَمْضِيَ حُقُبًا

wa-i*dz* q*aa*la muus*aa* lifat*aa*hu l*aa* abra*h*u *h*att*aa* ablugha majma'a **a**lba*h*rayni aw am*dh*iya *h*uqub*aa**\*n**

Dan (ingatlah) ketika Musa berkata kepada pembantunya, “Aku tidak akan berhenti (berjalan) sebelum sampai ke pertemuan dua laut; atau aku akan berjalan (terus sampai) bertahun-tahun.”

18:61

# فَلَمَّا بَلَغَا مَجْمَعَ بَيْنِهِمَا نَسِيَا حُوْتَهُمَا فَاتَّخَذَ سَبِيْلَهٗ فِى الْبَحْرِ سَرَبًا

falamm*aa* balagh*aa* majma'a baynihim*aa* nasiy*aa* *h*uutahum*aa* fa**i**ttakha*dz*a sabiilahu fii **a**lba*h*ri sarab*aa**\*n**

Maka ketika mereka sampai ke pertemuan dua laut itu, mereka lupa ikannya, lalu (ikan) itu melompat mengambil jalannya ke laut itu.

18:62

# فَلَمَّا جَاوَزَا قَالَ لِفَتٰىهُ اٰتِنَا غَدَاۤءَنَاۖ لَقَدْ لَقِيْنَا مِنْ سَفَرِنَا هٰذَا نَصَبًا

falamm*aa* j*aa*waz*aa* q*aa*la lifat*aa*hu *aa*tin*aa* ghad*aa*-an*aa* laqad laqiin*aa* min safarin*aa* h*aadzaa* na*sh*ab*aa**\*n**

Maka ketika mereka telah melewati (tempat itu), Musa berkata kepada pembantunya, “Bawalah kemari makanan kita; sungguh kita telah merasa letih karena perjalanan kita ini.”

18:63

# قَالَ اَرَاَيْتَ اِذْ اَوَيْنَآ اِلَى الصَّخْرَةِ فَاِنِّيْ نَسِيْتُ الْحُوْتَۖ وَمَآ اَنْسٰىنِيْهُ اِلَّا الشَّيْطٰنُ اَنْ اَذْكُرَهٗۚ وَاتَّخَذَ سَبِيْلَهٗ فِى الْبَحْرِ عَجَبًا

q*aa*la ara-ayta i*dz* awayn*aa* il*aa* **al***shsh*akhrati fa-innii nasiitu **a**l*h*uuta wam*aa *ans*aa*niihu ill*aa* **al**sysyay*thaa*nu an a*dz*kurah

Dia (pembantunya) menjawab, “Tahukah engkau ketika kita mencari tempat berlindung di batu tadi, maka aku lupa (menceritakan tentang) ikan itu dan tidak ada yang membuat aku lupa untuk mengingatnya kecuali setan, dan (ikan) itu mengambil jalannya ke laut d

18:64

# قَالَ ذٰلِكَ مَا كُنَّا نَبْغِۖ فَارْتَدَّا عَلٰٓى اٰثَارِهِمَا قَصَصًاۙ

q*aa*la *dzaa*lika m*aa* kunn*aa* nabghi fa**i**rtadd*aa* 'al*aa* *aa*ts*aa*rihim*aa* qa*sh*a*shaa**\*n**

Dia (Musa) berkata, “Itulah (tempat) yang kita cari.” Lalu keduanya kembali, mengikuti jejak mereka semula.

18:65

# فَوَجَدَا عَبْدًا مِّنْ عِبَادِنَآ اٰتَيْنٰهُ رَحْمَةً مِّنْ عِنْدِنَا وَعَلَّمْنٰهُ مِنْ لَّدُنَّا عِلْمًا

fawajad*aa* 'abdan min 'ib*aa*din*aa* *aa*tayn*aa*hu ra*h*matan min 'indin*aa* wa'allamn*aa*hu min ladunn*aa* 'ilm*aa**\*n**

Lalu mereka berdua bertemu dengan seorang hamba di antara hamba-hamba Kami, yang telah Kami berikan rahmat kepadanya dari sisi Kami, dan yang telah Kami ajarkan ilmu kepadanya dari sisi Kami.

18:66

# قَالَ لَهٗ مُوسٰى هَلْ اَتَّبِعُكَ عَلٰٓى اَنْ تُعَلِّمَنِ مِمَّا عُلِّمْتَ رُشْدًا

q*aa*la lahu muus*aa* hal attabi'uka 'al*aa* an tu'allimani mimm*aa* 'ullimta rusyd*aa**\*n**

Musa berkata kepadanya, “Bolehkah aku mengikutimu agar engkau mengajarkan kepadaku (ilmu yang benar) yang telah diajarkan kepadamu (untuk menjadi) petunjuk?”

18:67

# قَالَ اِنَّكَ لَنْ تَسْتَطِيْعَ مَعِيَ صَبْرًا

q*aa*la innaka lan tasta*th*ii'a ma'iya *sh*abr*aa**\*n**

Dia menjawab, “Sungguh, engkau tidak akan sanggup sabar bersamaku.

18:68

# وَكَيْفَ تَصْبِرُ عَلٰى مَا لَمْ تُحِطْ بِهٖ خُبْرًا

wakayfa ta*sh*biru 'al*aa* m*aa* lam tu*h*i*th* bihi khubr*aa**\*n**

Dan bagaimana engkau akan dapat bersabar atas sesuatu, sedang engkau belum mempunyai pengetahuan yang cukup tentang hal itu?”

18:69

# قَالَ سَتَجِدُنِيْٓ اِنْ شَاۤءَ اللّٰهُ صَابِرًا وَّلَآ اَعْصِيْ لَكَ اَمْرًا

q*aa*la satajidunii in sy*aa*-a **al**l*aa*hu *shaa*biran wal*aa* a'*sh*ii laka amr*aa**\*n**

Dia (Musa) berkata, “Insya Allah akan engkau dapati aku orang yang sabar, dan aku tidak akan menentangmu dalam urusan apa pun.”

18:70

# قَالَ فَاِنِ اتَّبَعْتَنِيْ فَلَا تَسْـَٔلْنِيْ عَنْ شَيْءٍ حَتّٰٓى اُحْدِثَ لَكَ مِنْهُ ذِكْرًا ࣖ

q*aa*la fa-ini ittaba'tanii fal*aa* tas-alnii 'an syay-in *h*att*aa* u*h*ditsa laka minhu *dz*ikr*aa**\*n**

Dia berkata, “Jika engkau mengikutiku, maka janganlah engkau menanyakan kepadaku tentang sesuatu apa pun, sampai aku menerangkannya kepadamu.”

18:71

# فَانْطَلَقَاۗ حَتّٰٓى اِذَا رَكِبَا فِى السَّفِيْنَةِ خَرَقَهَاۗ قَالَ اَخَرَقْتَهَا لِتُغْرِقَ اَهْلَهَاۚ لَقَدْ جِئْتَ شَيْـًٔا اِمْرًا

fa**i**n*th*alaq*aa* *h*att*aa* i*dzaa* rakib*aa* fii **al**ssafiinati kharaqah*aa* q*aa*la akharaqtah*aa* litughriqa ahlah*aa* laqad ji/ta syay-an imr*aa**\*n**

Maka berjalanlah keduanya, hingga ketika keduanya menaiki perahu lalu dia melubanginya. Dia (Musa) berkata, “Mengapa engkau melubangi perahu itu, apakah untuk menenggelamkan penumpangnya?” Sungguh, engkau telah berbuat suatu kesalahan yang besar.

18:72

# قَالَ اَلَمْ اَقُلْ اِنَّكَ لَنْ تَسْتَطِيْعَ مَعِيَ صَبْرًا

q*aa*la alam aqul innaka lan tasta*th*ii'a ma'iya *sh*abr*aa**\*n**

Dia berkata, “Bukankah sudah aku katakan, bahwa sesungguhnya engkau tidak akan mampu sabar bersamaku?”

18:73

# قَالَ لَا تُؤَاخِذْنِيْ بِمَا نَسِيْتُ وَلَا تُرْهِقْنِيْ مِنْ اَمْرِيْ عُسْرًا

q*aa*la l*aa* tu-*aa*khi*dz*nii bim*aa* nasiitu wal*aa* turhiqnii min amrii 'usr*aa**\*n**

Dia (Musa) berkata, “Janganlah engkau menghukum aku karena kelupaanku dan janganlah engkau membebani aku dengan suatu kesulitan dalam urusanku.”

18:74

# فَانْطَلَقَا ۗحَتّٰٓى اِذَا لَقِيَا غُلٰمًا فَقَتَلَهٗ ۙقَالَ اَقَتَلْتَ نَفْسًا زَكِيَّةً؈ۢبِغَيْرِ نَفْسٍۗ لَقَدْ جِئْتَ شَيْـًٔا نُكْرًا ۔

fa**i**n*th*alaq*aa* *h*att*aa* i*dzaa* laqiy*aa* ghul*aa*man faqatalahu q*aa*la aqatalta nafsan zakiyyatan bighayri nafsin laqad ji/ta syay-an nukr*aa**\*n**

Maka berjalanlah keduanya; hingga ketika keduanya berjumpa dengan seorang anak muda, maka dia membunuhnya. Dia (Musa) berkata, “Mengapa engkau bunuh jiwa yang bersih, bukan karena dia membunuh orang lain? Sungguh, engkau telah melakukan sesuatu yang sanga

18:75

# ۞ قَالَ اَلَمْ اَقُلْ لَّكَ اِنَّكَ لَنْ تَسْتَطِيْعَ مَعِيَ صَبْرًا

q*aa*la alam aqul laka innaka lan tasta*th*ii'a ma'iya *sh*abr*aa**\*n**

Dia berkata, “Bukankah sudah kukatakan kepadamu, bahwa engkau tidak akan mampu sabar bersamaku?”

18:76

# قَالَ اِنْ سَاَلْتُكَ عَنْ شَيْءٍۢ بَعْدَهَا فَلَا تُصٰحِبْنِيْۚ قَدْ بَلَغْتَ مِنْ لَّدُنِّيْ عُذْرًا

q*aa*la in sa-altuka 'an syay-in ba'dah*aa* fal*aa* tu*shaah*ibnii qad balaghta min ladunnii 'u*dz*r*aa**\*n**

Dia (Musa) berkata, “Jika aku bertanya kepadamu tentang sesuatu setelah ini, maka jangan lagi engkau memperbolehkan aku menyertaimu, sesungguhnya engkau sudah cukup (bersabar) menerima alasan dariku.”

18:77

# فَانْطَلَقَا ۗحَتّٰىٓ اِذَآ اَتَيَآ اَهْلَ قَرْيَةِ ِۨاسْتَطْعَمَآ اَهْلَهَا فَاَبَوْا اَنْ يُّضَيِّفُوْهُمَا فَوَجَدَا فِيْهَا جِدَارًا يُّرِيْدُ اَنْ يَّنْقَضَّ فَاَقَامَهٗ ۗقَالَ لَوْ شِئْتَ لَتَّخَذْتَ عَلَيْهِ اَجْرًا

fa**i**n*th*alaq*aa* *h*att*aa* i*dzaa* atay*aa* ahla qaryatin ista*th*'am*aa* ahlah*aa* fa-abaw an yu*dh*ayyifuuhum*aa* fawajad*aa* fiih*aa* jid*aa*ran yuriidu an yanqa

Maka keduanya berjalan; hingga ketika keduanya sampai kepada penduduk suatu negeri, mereka berdua meminta dijamu oleh penduduknya, tetapi mereka (penduduk negeri itu) tidak mau menjamu mereka, kemudian keduanya mendapatkan dinding rumah yang hampir roboh

18:78

# قَالَ هٰذَا فِرَاقُ بَيْنِيْ وَبَيْنِكَۚ سَاُنَبِّئُكَ بِتَأْوِيْلِ مَا لَمْ تَسْتَطِعْ عَّلَيْهِ صَبْرًا

q*aa*la h*aadzaa* fir*aa*qu baynii wabaynika sa-unabbi-uka bita/wiili m*aa* lam tasta*th*i' 'alayhi *sh*abr*aa**\*n**

Dia berkata, “Inilah perpisahan antara aku dengan engkau; aku akan memberikan penjelasan kepadamu atas perbuatan yang engkau tidak mampu sabar terhadapnya.

18:79

# اَمَّا السَّفِيْنَةُ فَكَانَتْ لِمَسٰكِيْنَ يَعْمَلُوْنَ فِى الْبَحْرِ فَاَرَدْتُّ اَنْ اَعِيْبَهَاۗ وَكَانَ وَرَاۤءَهُمْ مَّلِكٌ يَّأْخُذُ كُلَّ سَفِيْنَةٍ غَصْبًا

amm*aa* **al**ssafiinatu fak*aa*nat limas*aa*kiina ya'maluuna fii **a**lba*h*ri fa-aradtu an a'iibah*aa* wak*aa*na war*aa*-ahum malikun ya/khu*dz*u kulla safiinatin gha*sh*b*aa*

Adapun perahu itu adalah milik orang miskin yang bekerja di laut; aku bermaksud merusaknya, karena di hadapan mereka ada seorang raja yang akan merampas setiap perahu.

18:80

# وَاَمَّا الْغُلٰمُ فَكَانَ اَبَوَاهُ مُؤْمِنَيْنِ فَخَشِيْنَآ اَنْ يُّرْهِقَهُمَا طُغْيَانًا وَّكُفْرًا ۚ

wa-amm*aa* **a**lghul*aa*mu fak*aa*na abaw*aa*hu mu/minayni fakhasyiin*aa* an yurhiqahum*aa* *th*ughy*aa*nan wakufr*aa**\*n**

Dan adapun anak muda (kafir) itu, kedua orang tuanya mukmin, dan kami khawatir kalau dia akan memaksa kedua orang tuanya kepada kesesatan dan kekafiran.

18:81

# فَاَرَدْنَآ اَنْ يُّبْدِلَهُمَا رَبُّهُمَا خَيْرًا مِّنْهُ زَكٰوةً وَّاَقْرَبَ رُحْمًا

fa-aradn*aa* an yubdilahum*aa* rabbuhum*aa* khayran minhu zak*aa*tan wa-aqraba ru*h*m*aa**\*n**

Kemudian kami menghendaki, sekiranya Tuhan mereka menggantinya dengan (seorang anak) lain yang lebih baik kesuciannya daripada (anak) itu dan lebih sayang (kepada ibu bapaknya).

18:82

# وَاَمَّا الْجِدَارُ فَكَانَ لِغُلٰمَيْنِ يَتِيْمَيْنِ فِى الْمَدِيْنَةِ وَكَانَ تَحْتَهٗ كَنْزٌ لَّهُمَا وَكَانَ اَبُوْهُمَا صَالِحًا ۚفَاَرَادَ رَبُّكَ اَنْ يَّبْلُغَآ اَشُدَّهُمَا وَيَسْتَخْرِجَا كَنْزَهُمَا رَحْمَةً مِّنْ رَّبِّكَۚ وَمَا فَعَلْتُهٗ عَ

wa-amm*aa* **a**ljid*aa*ru fak*aa*na lighul*aa*mayni yatiimayni fii **a**lmadiinati wak*aa*na ta*h*tahu kanzun lahum*aa* wak*aa*na abuuhum*aa* *shaa*li*h*an fa-ar*aa*da

. Dan adapun dinding rumah itu adalah milik dua anak yatim di kota itu, yang di bawahnya tersimpan harta bagi mereka berdua, dan ayahnya seorang yang saleh. Maka Tuhanmu menghendaki agar keduanya sampai dewasa dan keduanya mengeluarkan simpanannya itu seb

18:83

# وَيَسْـَٔلُوْنَكَ عَنْ ذِى الْقَرْنَيْنِۗ قُلْ سَاَتْلُوْا عَلَيْكُمْ مِّنْهُ ذِكْرًا ۗ

wayas-aluunaka 'an *dz*ii **a**lqarnayni qul sa-atluu 'alaykum minhu *dz*ikr*aa**\*n**

Dan mereka bertanya kepadamu (Muhammad) tentang Zulkarnain. Katakanlah, “Akan kubacakan kepadamu kisahnya.”

18:84

# اِنَّا مَكَّنَّا لَهٗ فِى الْاَرْضِ وَاٰتَيْنٰهُ مِنْ كُلِّ شَيْءٍ سَبَبًا ۙ

inn*aa* makkann*aa* lahu fii **a**l-ar*dh*i wa*aa*tayn*aa*hu min kulli syay-in sabab*aa**\*n**

Sungguh, Kami telah memberi kedudukan kepadanya di bumi, dan Kami telah memberikan jalan kepadanya (untuk mencapai) segala sesuatu,

18:85

# فَاَتْبَعَ سَبَبًا

fa-atba'a sabab*aa**\*n**

maka dia pun menempuh suatu jalan.

18:86

# حَتّٰىٓ اِذَا بَلَغَ مَغْرِبَ الشَّمْسِ وَجَدَهَا تَغْرُبُ فِيْ عَيْنٍ حَمِئَةٍ وَّوَجَدَ عِنْدَهَا قَوْمًا ەۗ قُلْنَا يٰذَا الْقَرْنَيْنِ اِمَّآ اَنْ تُعَذِّبَ وَاِمَّآ اَنْ تَتَّخِذَ فِيْهِمْ حُسْنًا

*h*att*aa* i*dzaa* balagha maghriba **al**sysyamsi wajadah*aa* taghrubu fii 'aynin *h*ami-atin wawajada 'indah*aa* qawman quln*aa* y*aa* *dzaa* **a**lqarnayni imm*aa* an tu'a*dz*

Hingga ketika dia telah sampai di tempat matahari terbenam, dia melihatnya (matahari) terbenam di dalam laut yang berlumpur hitam, dan di sana ditemukannya suatu kaum (tidak beragama). Kami berfirman, “Wahai Zulkarnain! Engkau boleh menghukum atau berbua

18:87

# قَالَ اَمَّا مَنْ ظَلَمَ فَسَوْفَ نُعَذِّبُهٗ ثُمَّ يُرَدُّ اِلٰى رَبِّهٖ فَيُعَذِّبُهٗ عَذَابًا نُّكْرًا

q*aa*la amm*aa* man *zh*alama fasawfa nu'a*dzdz*ibuhu tsumma yuraddu il*aa* rabbihi fayu'a*dzdz*ibuhu 'a*dzaa*ban nukr*aa**\*n**

Dia (Zulkarnain) berkata, “Barangsiapa berbuat zalim, kami akan menghukumnya, lalu dia akan dikembalikan kepada Tuhannya, kemudian Tuhan mengazabnya dengan azab yang sangat keras.

18:88

# وَاَمَّا مَنْ اٰمَنَ وَعَمِلَ صَالِحًا فَلَهٗ جَزَاۤءً ۨالْحُسْنٰىۚ وَسَنَقُوْلُ لَهٗ مِنْ اَمْرِنَا يُسْرًا ۗ

wa-amm*aa* man *aa*mana wa'amila *shaa*li*h*an falahu jaz*aa*-an **a**l*h*usn*aa* wasanaquulu lahu min amrin*aa* yusr*aa**\*n**

Adapun orang yang beriman dan mengerjakan kebajikan, maka dia mendapat (pahala) yang terbaik sebagai balasan, dan akan kami sampaikan kepadanya perintah kami yang mudah-mudah.”

18:89

# ثُمَّ اَتْبَعَ سَبَبًا

tsumma atba'a sabab*aa**\*n**

Kemudian dia menempuh suatu jalan (yang lain).

18:90

# حَتّٰىٓ اِذَا بَلَغَ مَطْلِعَ الشَّمْسِ وَجَدَهَا تَطْلُعُ عَلٰى قَوْمٍ لَّمْ نَجْعَلْ لَّهُمْ مِّنْ دُوْنِهَا سِتْرًا ۙ

*h*att*aa* i*dzaa* balagha ma*th*li'a **al**sysyamsi wajadah*aa* ta*th*lu'u 'al*aa* qawmin lam naj'al lahum min duunih*aa* sitr*aa**\*n**

Hingga ketika dia sampai di tempat terbit matahari (sebelah timur) didapatinya (matahari) bersinar di atas suatu kaum yang tidak Kami buatkan suatu pelindung bagi mereka dari (cahaya matahari) itu,

18:91

# كَذٰلِكَۗ وَقَدْ اَحَطْنَا بِمَا لَدَيْهِ خُبْرًا

ka*dzaa*lika waqad a*h*a*th*n*aa* bim*aa* ladayhi khubr*aa**\*n**

demikianlah, dan sesungguhnya Kami mengetahui segala sesuatu yang ada padanya (Zulkarnain).

18:92

# ثُمَّ اَتْبَعَ سَبَبًا

tsumma atba'a sabab*aa**\*n**

Kemudian dia menempuh suatu jalan (yang lain lagi).

18:93

# حَتّٰىٓ اِذَا بَلَغَ بَيْنَ السَّدَّيْنِ وَجَدَ مِنْ دُوْنِهِمَا قَوْمًاۙ لَّا يَكَادُوْنَ يَفْقَهُوْنَ قَوْلًا

*h*att*aa* i*dzaa* balagha bayna **al**ssaddayni wajada min duunihim*aa* qawman l*aa* yak*aa*duuna yafqahuuna qawl*aa**\*n**

Hingga ketika dia sampai di antara dua gunung, didapatinya di belakang (kedua gunung itu) suatu kaum yang hampir tidak memahami pembicaraan.

18:94

# قَالُوْا يٰذَا الْقَرْنَيْنِ اِنَّ يَأْجُوْجَ وَمَأْجُوْجَ مُفْسِدُوْنَ فِى الْاَرْضِ فَهَلْ نَجْعَلُ لَكَ خَرْجًا عَلٰٓى اَنْ تَجْعَلَ بَيْنَنَا وَبَيْنَهُمْ سَدًّا

q*aa*luu y*aa* *dzaa* **a**lqarnayni inna ya/juuja wama/juuja mufsiduuna fii **a**l-ar*dh*i fahal naj'alu laka kharjan 'al*aa* an taj'ala baynan*aa* wabaynahum sadd*aa**\*n**

Mereka berkata, “Wahai Zulkarnain! Sungguh, Yakjuj dan Makjuj itu (makhluk yang) berbuat kerusakan di bumi, maka bolehkah kami membayarmu imbalan agar engkau membuatkan dinding penghalang antara kami dan mereka?”

18:95

# قَالَ مَا مَكَّنِّيْ فِيْهِ رَبِّيْ خَيْرٌ فَاَعِيْنُوْنِيْ بِقُوَّةٍ اَجْعَلْ بَيْنَكُمْ وَبَيْنَهُمْ رَدْمًا ۙ

q*aa*la m*aa* makkannii fiihi rabbii khayrun fa-a'iinuunii biquwwatin aj'al baynakum wabaynahum radm*aa**\*n**

Dia (Zulkarnain) berkata, “Apa yang telah dianugerahkan Tuhan kepadaku lebih baik (daripada imbalanmu), maka bantulah aku dengan kekuatan, agar aku dapat membuatkan dinding penghalang antara kamu dan mereka.

18:96

# اٰتُوْنِيْ زُبَرَ الْحَدِيْدِۗ حَتّٰىٓ اِذَا سَاوٰى بَيْنَ الصَّدَفَيْنِ قَالَ انْفُخُوْا ۗحَتّٰىٓ اِذَا جَعَلَهٗ نَارًاۙ قَالَ اٰتُوْنِيْٓ اُفْرِغْ عَلَيْهِ قِطْرًا ۗ

*aa*tuunii zubara **a**l*h*adiidi *h*att*aa* i*dzaa* s*aa*w*aa* bayna **al***sh*adafayni q*aa*la unfukhuu* h*att*aa *i*dzaa *ja'alahu n*aa*ran q*aa*la* aa*

*Berilah aku potongan-potongan besi!” Hingga ketika (potongan) besi itu telah (terpasang) sama rata dengan kedua (puncak) gunung itu, dia (Zulkarnain) berkata, “Tiuplah (api itu)!” Ketika (besi) itu sudah menjadi (merah seperti) api, dia pun berkata, “Ber*

18:97

# فَمَا اسْطَاعُوْٓا اَنْ يَّظْهَرُوْهُ وَمَا اسْتَطَاعُوْا لَهٗ نَقْبًا

fam*aa* is*thaa*'uu an ya*zh*haruuhu wam*aa* ista*thaa*'uu lahu naqb*aa**\*n**

Maka mereka (Yakjuj dan Makjuj) tidak dapat mendakinya dan tidak dapat (pula) melubanginya.

18:98

# قَالَ هٰذَا رَحْمَةٌ مِّنْ رَّبِّيْۚ فَاِذَا جَاۤءَ وَعْدُ رَبِّيْ جَعَلَهٗ دَكَّاۤءَۚ وَكَانَ وَعْدُ رَبِّيْ حَقًّا ۗ

q*aa*la h*aadzaa* ra*h*matun min rabbii fa-i*dzaa* j*aa*-a wa'du rabbii ja'alahu dakk*aa*-a wak*aa*na wa'du rabbii *h*aqq*aa**\*n**

Dia (Zulkarnain) berkata, “(Dinding) ini adalah rahmat dari Tuhanku, maka apabila janji Tuhanku sudah datang, Dia akan menghancurluluhkannya; dan janji Tuhanku itu benar.”

18:99

# ۞ وَتَرَكْنَا بَعْضَهُمْ يَوْمَىِٕذٍ يَّمُوْجُ فِيْ بَعْضٍ وَّنُفِخَ فِى الصُّوْرِ فَجَمَعْنٰهُمْ جَمْعًا ۙ

watarakn*aa* ba'*dh*ahum yawma-i*dz*in yamuuju fii ba'*dh*in wanufikha fii **al***shsh*uuri fajama'n*aa*hum jam'*aa**\*n**

Dan pada hari itu Kami biarkan mereka (Yakjuj dan Makjuj) berbaur antara satu dengan yang lain, dan (apabila) sangkakala ditiup (lagi), akan Kami kumpulkan mereka semuanya.

18:100

# وَّعَرَضْنَا جَهَنَّمَ يَوْمَىِٕذٍ لِّلْكٰفِرِيْنَ عَرْضًا ۙ

wa'ara*dh*n*aa* jahannama yawma-i*dz*in lilk*aa*firiina 'ar*daa**\*n**

Dan Kami perlihatkan (neraka) Jahanam dengan jelas pada hari itu kepada orang kafir,

18:101

# ۨالَّذِيْنَ كَانَتْ اَعْيُنُهُمْ فِيْ غِطَاۤءٍ عَنْ ذِكْرِيْ وَكَانُوْا لَا يَسْتَطِيْعُوْنَ سَمْعًا ࣖ

**al**la*dz*iina k*aa*nat a'yunuhum fii ghi*thaa*-in 'an *dz*ikrii wak*aa*nuu l*aa* yasta*th*ii'uuna sam'*aa**\*n**

(yaitu) orang yang mata (hati)nya dalam keadaan tertutup (tidak mampu) dari memperhatikan tanda-tanda (kebesaran)-Ku, dan mereka tidak sanggup mendengar.

18:102

# اَفَحَسِبَ الَّذِيْنَ كَفَرُوْٓا اَنْ يَّتَّخِذُوْا عِبَادِيْ مِنْ دُوْنِيْٓ اَوْلِيَاۤءَ ۗاِنَّآ اَعْتَدْنَا جَهَنَّمَ لِلْكٰفِرِيْنَ نُزُلًا

afa*h*asiba **al**la*dz*iina kafaruu an yattakhi*dz*uu 'ib*aa*dii min duunii awliy*aa*-a inn*aa* a'tadn*aa* jahannama lilk*aa*firiina nuzul*aa**\*n**

Maka apakah orang kafir menyangka bahwa mereka (dapat) mengambil hamba-hamba-Ku menjadi penolong selain Aku? Sungguh, Kami telah menyediakan (neraka) Jahanam sebagai tempat tinggal bagi orang-orang kafir.

18:103

# قُلْ هَلْ نُنَبِّئُكُمْ بِالْاَخْسَرِيْنَ اَعْمَالًا ۗ

qul hal nunabbi-ukum bi**a**l-akhsariina a'm*aa*l*aa**\*n**

Katakanlah (Muhammad), “Apakah perlu Kami beritahukan kepadamu tentang orang yang paling rugi perbuatannya?”

18:104

# اَلَّذِيْنَ ضَلَّ سَعْيُهُمْ فِى الْحَيٰوةِ الدُّنْيَا وَهُمْ يَحْسَبُوْنَ اَنَّهُمْ يُحْسِنُوْنَ صُنْعًا

**al**la*dz*iina *dh*alla sa'yuhum fii **a**l*h*ay*aa*ti **al**dduny*aa* wahum ya*h*sabuuna annahum yu*h*sinuuna *sh*un'*aa**\*n**

(Yaitu) orang yang sia-sia perbuatannya dalam kehidupan dunia, sedangkan mereka mengira telah berbuat sebaik-baiknya.

18:105

# اُولٰۤىِٕكَ الَّذِيْنَ كَفَرُوْا بِاٰيٰتِ رَبِّهِمْ وَلِقَاۤىِٕهٖ فَحَبِطَتْ اَعْمَالُهُمْ فَلَا نُقِيْمُ لَهُمْ يَوْمَ الْقِيٰمَةِ وَزْنًا

ul*aa*-ika **al**la*dz*iina kafaruu bi-*aa*y*aa*ti rabbihim waliq*aa*-ihi fa*h*abi*th*at a'm*aa*luhum fal*aa* nuqiimu lahum yawma **a**lqiy*aa*mati wazn*aa**\*n**

Mereka itu adalah orang yang mengingkari ayat-ayat Tuhan mereka dan (tidak percaya) terhadap pertemuan dengan-Nya. Maka sia-sia amal mereka, dan Kami tidak memberikan penimbangan terhadap (amal) mereka pada hari Kiamat.

18:106

# ذٰلِكَ جَزَاۤؤُهُمْ جَهَنَّمُ بِمَا كَفَرُوْا وَاتَّخَذُوْٓا اٰيٰتِيْ وَرُسُلِيْ هُزُوًا

*dzaa*lika jaz*aa*uhum jahannamu bim*aa* kafaruu wa**i**ttakha*dz*uu *aa*y*aa*tii warusulii huzuw*aa**\*n**

Demikianlah, balasan mereka itu neraka Jahanam, karena kekafiran mereka, dan karena mereka menjadikan ayat-ayat-Ku dan rasul-rasul-Ku sebagai bahan olok-olok.

18:107

# اِنَّ الَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِ كَانَتْ لَهُمْ جَنّٰتُ الْفِرْدَوْسِ نُزُلًا ۙ

inna **al**la*dz*iina *aa*manuu wa'amiluu **al***shshaa*li*haa*ti k*aa*nat lahum jann*aa*tu **a**lfirdawsi nuzul*aa**\*n**

Sungguh, orang yang beriman dan mengerjakan kebajikan, untuk mereka disediakan surga Firdaus sebagai tempat tinggal,

18:108

# خٰلِدِيْنَ فِيْهَا لَا يَبْغُوْنَ عَنْهَا حِوَلًا

kh*aa*lidiina fiih*aa* l*aa* yabghuuna 'anh*aa* *h*iwal*aa**\*n**

mereka kekal di dalamnya, mereka tidak ingin pindah dari sana.

18:109

# قُلْ لَّوْ كَانَ الْبَحْرُ مِدَادًا لِّكَلِمٰتِ رَبِّيْ لَنَفِدَ الْبَحْرُ قَبْلَ اَنْ تَنْفَدَ كَلِمٰتُ رَبِّيْ وَلَوْ جِئْنَا بِمِثْلِهٖ مَدَدًا

qul law k*aa*na **a**lba*h*ru mid*aa*dan likalim*aa*ti rabbii lanafida **a**lba*h*ru qabla an tanfada kalim*aa*tu rabbii walaw ji/n*aa* bimitslihi madad*aa**\*n**

Katakanlah (Muhammad), “Seandainya lautan menjadi tinta untuk (menulis) kalimat-kalimat Tuhanku, maka pasti habislah lautan itu sebelum selesai (penulisan) kalimat-kalimat Tuhanku, meskipun Kami datangkan tambahan sebanyak itu (pula).”

18:110

# قُلْ اِنَّمَآ اَنَا۠ بَشَرٌ مِّثْلُكُمْ يُوْحٰٓى اِلَيَّ اَنَّمَآ اِلٰهُكُمْ اِلٰهٌ وَّاحِدٌۚ فَمَنْ كَانَ يَرْجُوْا لِقَاۤءَ رَبِّهٖ فَلْيَعْمَلْ عَمَلًا صَالِحًا وَّلَا يُشْرِكْ بِعِبَادَةِ رَبِّهٖٓ اَحَدًا ࣖ

qul innam*aa* an*aa* basyarun mitslukum yuu*haa* ilayya annam*aa* il*aa*hukum il*aa*hun w*aah*idun faman k*aa*na yarjuu liq*aa*-a rabbihi falya'mal 'amalan *shaa*li*h*an wal*aa* yusyrik bi'ib*a*

Katakanlah: Sesungguhnya aku ini manusia biasa seperti kamu, yang diwahyukan kepadaku: “Bahwa sesungguhnya Tuhan kamu itu adalah Tuhan yang Esa”. Barangsiapa mengharap perjumpaan dengan Tuhannya, maka hendaklah ia mengerjakan amal yang saleh dan janganlah ia mempersekutukan seorangpun dalam beribadat kepada Tuhannya”.

<!--EndFragment-->