---
title: (77) Al-Mursalat - المرسلٰت
date: 2021-10-27T04:08:07.664Z
ayat: 77
description: "Jumlah Ayat: 50 / Arti: Malaikat Yang Diutus"
---
<!--StartFragment-->

77:1

# وَالْمُرْسَلٰتِ عُرْفًاۙ

wa**a**lmursal*aa*ti 'urf*aa***n**

Demi (malaikat-malaikat) yang diutus untuk membawa kebaikan,

77:2

# فَالْعٰصِفٰتِ عَصْفًاۙ

fa**a**l'*aas*if*aa*ti 'a*sh*f*aa***n**

dan (malaikat-malaikat) yang terbang dengan kencangnya,

77:3

# وَّالنّٰشِرٰتِ نَشْرًاۙ

wa**al**nn*aa*syir*aa*ti nasyr*aa***n**

dan (malaikat-malaikat) yang menyebarkan (rahmat Allah) dengan seluas-luasnya,

77:4

# فَالْفٰرِقٰتِ فَرْقًاۙ

fa**a**lf*aa*riq*aa*ti farq*aa***n**

dan (malaikat-malaikat) yang membedakan (antara yang baik dan yang buruk) dengan sejelas-jelasnya,

77:5

# فَالْمُلْقِيٰتِ ذِكْرًاۙ

fa**a**lmulqiy*aa*ti *dz*ikr*aa***n**

dan (malaikat-malaikat) yang menyampaikan wahyu,

77:6

# عُذْرًا اَوْ نُذْرًاۙ

'u*dz*ran aw nu*dz*ra**n**

untuk menolak alasan-alasan atau memberi peringatan.

77:7

# اِنَّمَا تُوْعَدُوْنَ لَوَاقِعٌۗ

innam*aa* tuu'aduuna law*aa*qi'**un**

Sungguh, apa yang dijanjikan kepadamu pasti terjadi.

77:8

# فَاِذَا النُّجُوْمُ طُمِسَتْۙ

fa-i*dzaa* **al**nnujuumu *th*umisath

Maka apabila bintang-bintang dihapuskan,

77:9

# وَاِذَا السَّمَاۤءُ فُرِجَتْۙ

wa-i*dzaa* **al**ssam*aa*u furijath

dan apabila langit terbelah,

77:10

# وَاِذَا الْجِبَالُ نُسِفَتْۙ

wa-i*dzaa* **a**ljib*aa*lu nusifath

dan apabila gunung-gunung dihancurkan menjadi debu,

77:11

# وَاِذَا الرُّسُلُ اُقِّتَتْۗ

wa-i*dzaa* **al**rrusulu uqqitat

dan apabila rasul-rasul telah ditetapkan waktunya.

77:12

# لِاَيِّ يَوْمٍ اُجِّلَتْۗ

li-ayyi yawmin ujjilat

(Niscaya dikatakan kepada mereka), “Sampai hari apakah ditangguhkan (azab orang-orang kafir itu)?”

77:13

# لِيَوْمِ الْفَصْلِۚ

liyawmi **a**lfa*sh*l**i**

Sampai hari keputusan.

77:14

# وَمَآ اَدْرٰىكَ مَا يَوْمُ الْفَصْلِۗ

wam*aa* adr*aa*ka m*aa* yawmu **a**lfa*sh*l**i**

Dan tahukah kamu apakah hari ke-putusan itu?

77:15

# وَيْلٌ يَّوْمَىِٕذٍ لِّلْمُكَذِّبِيْنَ

waylun yawma-i*dz*in lilmuka*dzdz*ibiin**a**

Celakalah pada hari itu, bagi mereka yang mendustakan (kebenaran).

77:16

# اَلَمْ نُهْلِكِ الْاَوَّلِيْنَۗ

alam nuhliki **a**l-awwaliin**a**

Bukankah telah Kami binasakan orang-orang yang dahulu?

77:17

# ثُمَّ نُتْبِعُهُمُ الْاٰخِرِيْنَ

tsumma nutbi'uhumu **a**l-*aa*khiriin**a**

Lalu Kami susulkan (azab Kami terhadap) orang-orang yang datang kemudian.

77:18

# كَذٰلِكَ نَفْعَلُ بِالْمُجْرِمِيْنَ

ka*dzaa*lika naf'alu bi**a**lmujrimiin**a**

Demikianlah Kami perlakukan orang-orang yang berdosa.

77:19

# وَيْلٌ يَّوْمَىِٕذٍ لِّلْمُكَذِّبِيْنَ

waylun yawma-i*dz*in lilmuka*dzdz*ibiin**a**

Celakalah pada hari itu, bagi mereka yang mendustakan (kebenaran).

77:20

# اَلَمْ نَخْلُقْكُّمْ مِّنْ مَّاۤءٍ مَّهِيْنٍۙ

alam nakhluqkum min m*aa*-in mahiin**in**

Bukankah Kami menciptakan kamu dari air yang hina (mani),

77:21

# فَجَعَلْنٰهُ فِيْ قَرَارٍ مَّكِيْنٍ

faja'aln*aa*hu fii qar*aa*rin makiin**in**

kemudian Kami letakkan ia dalam tempat yang kokoh (rahim),

77:22

# اِلٰى قَدَرٍ مَّعْلُوْمٍۙ

il*aa* qadarin ma'luum**in**

sampai waktu yang ditentukan,

77:23

# فَقَدَرْنَاۖ فَنِعْمَ الْقٰدِرُوْنَ

faqadarn*aa* fani'ma **a**lq*aa*diruun**a**

lalu Kami tentukan (bentuknya), maka (Kamilah) sebaik-baik yang menentukan.

77:24

# وَيْلٌ يَّوْمَىِٕذٍ لِّلْمُكَذِّبِيْنَ

waylun yawma-i*dz*in lilmuka*dzdz*ibiin**a**

Celakalah pada hari itu, bagi mereka yang mendustakan (kebenaran).

77:25

# اَلَمْ نَجْعَلِ الْاَرْضَ كِفَاتًاۙ

alam naj'ali **a**l-ar*dh*a kif*aa*t*aa***n**

Bukankah Kami jadikan bumi untuk (tempat) berkumpul,

77:26

# اَحْيَاۤءً وَّاَمْوَاتًاۙ

a*h*y*aa*-an wa-amw*aa*t*aa***n**

bagi yang masih hidup dan yang sudah mati?

77:27

# وَّجَعَلْنَا فِيْهَا رَوَاسِيَ شٰمِخٰتٍ وَّاَسْقَيْنٰكُمْ مَّاۤءً فُرَاتًاۗ

waja'aln*aa* fiih*aa* raw*aa*siya sy*aa*mikh*aa*tin wa-asqayn*aa*kum m*aa*-an fur*aa*t*aa***n**

Dan Kami jadikan padanya gunung-gunung yang tinggi, dan Kami beri minum kamu dengan air tawar?

77:28

# وَيْلٌ يَّوْمَىِٕذٍ لِّلْمُكَذِّبِيْنَ

waylun yawma-i*dz*in lilmuka*dzdz*ibiin**a**

Celakalah pada hari itu, bagi mereka yang mendustakan (kebenaran).

77:29

# اِنْطَلِقُوْٓا اِلٰى مَا كُنْتُمْ بِهٖ تُكَذِّبُوْنَۚ

in*th*aliquu il*aa* m*aa* kuntum bihi tuka*dzdz*ibuun**a**

(Akan dikatakan), “Pergilah kamu mendapatkan apa (azab) yang dahulu kamu dustakan.

77:30

# اِنْطَلِقُوْٓا اِلٰى ظِلٍّ ذِيْ ثَلٰثِ شُعَبٍ

in*th*aliquu il*aa* *zh*illin *dz*ii tsal*aa*tsi syu'ab**in**

Pergilah kamu mendapatkan naungan (asap api neraka) yang mempunyai tiga cabang,

77:31

# لَا ظَلِيْلٍ وَّلَا يُغْنِيْ مِنَ اللَّهَبِۗ

*aa* *zh*aliilin wal*aa* yughnii mina **al**lahab**i**

yang tidak melindungi dan tidak pula menolak nyala api neraka.”

77:32

# اِنَّهَا تَرْمِيْ بِشَرَرٍ كَالْقَصْرِۚ

innah*aa* tarmii bisyararin ka**a**lqa*sh*r**i**

Sungguh, (neraka) itu menyemburkan bunga api (sebesar dan setinggi) istana,

77:33

# كَاَنَّهٗ جِمٰلَتٌ صُفْرٌۗ

ka-annahu jim*aa*latun *sh*ufr**un**

seakan-akan iring-iringan unta yang kuning.

77:34

# وَيْلٌ يَّوْمَىِٕذٍ لِّلْمُكَذِّبِيْنَ

waylun yawma-i*dz*in lilmuka*dzdz*ibiin**a**

Celakalah pada hari itu, bagi mereka yang mendustakan (kebenaran).

77:35

# هٰذَا يَوْمُ لَا يَنْطِقُوْنَۙ

h*aadzaa* yawmu l*aa* yan*th*iquun**a**

Inilah hari, saat mereka tidak dapat berbicara,

77:36

# وَلَا يُؤْذَنُ لَهُمْ فَيَعْتَذِرُوْنَ

wal*aa* yu/*dz*anu lahum faya'ta*dz*iruun**a**

dan tidak diizinkan kepada mereka mengemukakan alasan agar mereka dimaafkan.

77:37

# وَيْلٌ يَّوْمَىِٕذٍ لِّلْمُكَذِّبِيْنَ

waylun yawma-i*dz*in lilmuka*dzdz*ibiin**a**

Celakalah pada hari itu, bagi mereka yang mendustakan (kebenaran).

77:38

# هٰذَا يَوْمُ الْفَصْلِ جَمَعْنٰكُمْ وَالْاَوَّلِيْنَ

h*aadzaa* yawmu **a**lfa*sh*li jama'n*aa*kum wa**a**l-awwaliin**a**

Inilah hari keputusan; (pada hari ini) Kami kumpulkan kamu dan orang-orang yang terdahulu.

77:39

# فَاِنْ كَانَ لَكُمْ كَيْدٌ فَكِيْدُوْنِ

fa-in k*aa*na lakum kaydun fakiiduun**i**

Maka jika kamu punya tipu daya, maka lakukanlah (tipu daya) itu terhadap-Ku.

77:40

# وَيْلٌ يَّوْمَىِٕذٍ لِّلْمُكَذِّبِيْنَ ࣖ

waylun yawma-i*dz*in lilmuka*dzdz*ibiin**a**

Celakalah pada hari itu, bagi mereka yang mendustakan (kebenaran).

77:41

# اِنَّ الْمُتَّقِيْنَ فِيْ ظِلٰلٍ وَّعُيُوْنٍۙ

inna **a**lmuttaqiina fii *zh*il*aa*lin wa'uyuun**in**

Sungguh, orang-orang yang bertakwa berada dalam naungan (pepohonan surga yang teduh) dan (di sekitar) mata air,

77:42

# وَّفَوَاكِهَ مِمَّا يَشْتَهُوْنَۗ

wafaw*aa*kiha mimm*aa* yasytahuun**a**

dan buah-buahan yang mereka sukai.

77:43

# كُلُوْا وَاشْرَبُوْا هَنِيْۤـًٔا ۢبِمَا كُنْتُمْ تَعْمَلُوْنَ

kuluu wa**i**syrabuu hanii-an bim*aa* kuntum ta'maluun**a**

(Katakan kepada mereka), “Makan dan minumlah dengan rasa nikmat sebagai balasan dari apa yang telah kamu kerjakan.”

77:44

# اِنَّا كَذٰلِكَ نَجْزِى الْمُحْسِنِيْنَ

inn*aa* ka*dzaa*lika najzii **a**lmu*h*siniin**a**

Sungguh, demikianlah Kami memberi balasan kepada orang-orang yang berbuat baik.

77:45

# وَيْلٌ يَّوْمَىِٕذٍ لِّلْمُكَذِّبِيْنَ

waylun yawma-i*dz*in lilmuka*dzdz*ibiin**a**

Celakalah pada hari itu, bagi mereka yang mendustakan (kebenaran).

77:46

# كُلُوْا وَتَمَتَّعُوْا قَلِيْلًا اِنَّكُمْ مُّجْرِمُوْنَ

kuluu watamatta'uu qaliilan innakum mujrimuun**a**

(Katakan kepada orang-orang kafir), “Makan dan bersenang-senanglah kamu (di dunia) sebentar, sesungguhnya kamu orang-orang durhaka!”

77:47

# وَيْلٌ يَّوْمَىِٕذٍ لِّلْمُكَذِّبِيْنَ

waylun yawma-i*dz*in lilmuka*dzdz*ibiin**a**

Celakalah pada hari itu, bagi mereka yang mendustakan (kebenaran).

77:48

# وَاِذَا قِيْلَ لَهُمُ ارْكَعُوْا لَا يَرْكَعُوْنَ

wa-i*dzaa* qiila lahumu irka'uu l*aa* yarka'uun**a**

Dan apabila dikatakan kepada mereka, “Rukuklah,” mereka tidak mau rukuk.

77:49

# وَيْلٌ يَّوْمَىِٕذٍ لِّلْمُكَذِّبِيْنَ

waylun yawma-i*dz*in lilmuka*dzdz*ibiin**a**

Celakalah pada hari itu, bagi mereka yang mendustakan (kebenaran)!

77:50

# فَبِاَيِّ حَدِيْثٍۢ بَعْدَهٗ يُؤْمِنُوْنَ ࣖ ۔

fabi-ayyi *h*adiitsin ba'dahu yu/minuun**a**

Maka kepada ajaran manakah (selain Al-Qur'an) ini mereka akan beriman?

<!--EndFragment-->