---
title: (50) Qaf - ق
date: 2021-10-27T04:10:19.446Z
ayat: 50
description: "Jumlah Ayat: 45 / Arti: Qaf"
---
<!--StartFragment-->

50:1

# قۤ ۗوَالْقُرْاٰنِ الْمَجِيْدِ ۖ

q*aa*f wa**a**lqur-*aa*ni **a**lmajiid**i**

Qaf. Demi Al-Qur'an yang mulia.

50:2

# بَلْ عَجِبُوْٓا اَنْ جَاۤءَهُمْ مُّنْذِرٌ مِّنْهُمْ فَقَالَ الْكٰفِرُوْنَ هٰذَا شَيْءٌ عَجِيْبٌ ۚ

bal 'ajibuu an j*aa*-ahum mun*dz*irun minhum faq*aa*la **a**lk*aa*firuuna h*aadzaa* syay-un 'ajiib**un**

(Mereka tidak menerimanya) bahkan mereka tercengang karena telah datang kepada mereka seorang pemberi peringatan dari (kalangan) mereka sendiri, maka berkatalah orang-orang kafir, “Ini adalah suatu yang sangat ajaib.”

50:3

# ءَاِذَا مِتْنَا وَكُنَّا تُرَابًا ۚ ذٰلِكَ رَجْعٌۢ بَعِيْدٌ

a-i*dzaa* mitn*aa* wakunn*aa* tur*aa*ban *dzaa*lika raj'un ba'iid**un**

Apakah apabila kami telah mati dan sudah menjadi tanah (akan kembali lagi)? Itu adalah suatu pengembalian yang tidak mungkin.

50:4

# قَدْ عَلِمْنَا مَا تَنْقُصُ الْاَرْضُ مِنْهُمْ ۚوَعِنْدَنَا كِتٰبٌ حَفِيْظٌ

qad 'alimn*aa* m*aa* tanqu*sh*u **a**l-ar*dh*u minhum wa'indan*aa* kit*aa*bun *h*afii*zh***un**

Sungguh, Kami telah mengetahui apa yang ditelan bumi dari (tubuh) mereka, sebab pada Kami ada kitab (catatan) yang terpelihara baik.

50:5

# بَلْ كَذَّبُوْا بِالْحَقِّ لَمَّا جَاۤءَهُمْ فَهُمْ فِيْٓ اَمْرٍ مَّرِيْجٍ

bal ka*dzdz*abuu bi**a**l*h*aqqi lamm*aa* j*aa*-ahum fahum fii amrin mariij**in**

Bahkan mereka telah mendustakan kebenaran ketika (kebenaran itu) datang kepada mereka, maka mereka berada dalam keadaan kacau balau.

50:6

# اَفَلَمْ يَنْظُرُوْٓا اِلَى السَّمَاۤءِ فَوْقَهُمْ كَيْفَ بَنَيْنٰهَا وَزَيَّنّٰهَا وَمَا لَهَا مِنْ فُرُوْجٍ

afalam yan*zh*uruu il*aa* **al**ssam*aa*-i fawqahum kayfa banayn*aa*h*aa* wazayyann*aa*h*aa* wam*aa* lah*aa* min furuuj**in**

Maka tidakkah mereka memperhatikan langit yang ada di atas mereka, bagaimana cara Kami membangunnya dan menghiasinya dan tidak terdapat retak-retak sedikit pun?

50:7

# وَالْاَرْضَ مَدَدْنٰهَا وَاَلْقَيْنَا فِيْهَا رَوَاسِيَ وَاَنْۢبَتْنَا فِيْهَا مِنْ كُلِّ زَوْجٍۢ بَهِيْجٍۙ

wa**a**l-ar*dh*a madadn*aa*h*aa* wa-alqayn*aa* fiih*aa* raw*aa*siya wa-anbatn*aa* fiih*aa* min kulli zawjin bahiij**in**

Dan bumi yang Kami hamparkan dan Kami pancangkan di atasnya gunung-gunung yang kokoh dan Kami tumbuhkan di atasnya tanam-tanaman yang indah,

50:8

# تَبْصِرَةً وَّذِكْرٰى لِكُلِّ عَبْدٍ مُّنِيْبٍ

tab*sh*iratan wa*dz*ikr*aa* likulli 'abdin muniib**in**

untuk menjadi pelajaran dan peringatan bagi setiap hamba yang kembali (tunduk kepada Allah).

50:9

# وَنَزَّلْنَا مِنَ السَّمَاۤءِ مَاۤءً مُّبٰرَكًا فَاَنْۢبَتْنَا بِهٖ جَنّٰتٍ وَّحَبَّ الْحَصِيْدِۙ

wanazzaln*aa* mina **al**ssam*aa*-i m*aa*-an mub*aa*rakan fa-anbatn*aa* bihi jann*aa*tin wa*h*abba **a**l*h*a*sh*iid**i**

Dan dari langit Kami turunkan air yang memberi berkah lalu Kami tumbuhkan dengan (air) itu pepohonan yang rindang dan biji-bijian yang dapat dipanen.

50:10

# وَالنَّخْلَ بٰسِقٰتٍ لَّهَا طَلْعٌ نَّضِيْدٌۙ

wa**al**nnakhla b*aa*siq*aa*tin lah*aa* *th*al'un na*dh*iid**un**

Dan pohon kurma yang tinggi-tinggi yang mempunyai mayang yang bersusun-susun,

50:11

# رِّزْقًا لِّلْعِبَادِۙ وَاَحْيَيْنَا بِهٖ بَلْدَةً مَّيْتًاۗ كَذٰلِكَ الْخُرُوْجُ

rizqan lil'ib*aa*di wa-a*h*yayn*aa* bihi baldatan maytan ka*dzaa*lika **a**lkhuruuj**u**

(sebagai) rezeki bagi hamba-hamba (Kami), dan Kami hidupkan dengan (air) itu negeri yang mati (tandus). Seperti itulah terjadinya kebangkitan (dari kubur).

50:12

# كَذَّبَتْ قَبْلَهُمْ قَوْمُ نُوْحٍ وَّاَصْحٰبُ الرَّسِّ وَثَمُوْدُ

ka*dzdz*abat qablahum qawmu nuu*h*in wa-a*sh*-*haa*bu **al**rrassi watsamuud**u**

Sebelum mereka, kaum Nuh, penduduk Rass dan Samud telah mendustakan (rasul-rasul),

50:13

# وَعَادٌ وَّفِرْعَوْنُ وَاِخْوَانُ لُوْطٍۙ

wa'*aa*dun wafir'awnu wa-ikhw*aa*nu luu*th***in**

dan (demikian juga) kaum ‘Ad, kaum Fir‘aun dan kaum Lut,

50:14

# وَّاَصْحٰبُ الْاَيْكَةِ وَقَوْمُ تُبَّعٍۗ كُلٌّ كَذَّبَ الرُّسُلَ فَحَقَّ وَعِيْدِ

wa-a*sh*-*haa*bu **a**l-aykati waqawmu tubba'in kullun ka*dzdz*aba **al**rrusula fa*h*aqqa wa'iid**i**

dan (juga) penduduk Aikah serta kaum Tubba‘. Semuanya telah mendustakan rasul-rasul maka berlakulah ancaman-Ku (atas mereka).

50:15

# اَفَعَيِيْنَا بِالْخَلْقِ الْاَوَّلِۗ بَلْ هُمْ فِيْ لَبْسٍ مِّنْ خَلْقٍ جَدِيْدٍ ࣖ

afa'ayiin*aa* bi**a**lkhalqi **a**l-awwali bal hum fii labsin min khalqin jadiid**in**

Maka apakah Kami letih dengan penciptaan yang pertama? (Sama sekali tidak) bahkan mereka dalam keadaan ragu-ragu tentang penciptaan yang baru.

50:16

# وَلَقَدْ خَلَقْنَا الْاِنْسَانَ وَنَعْلَمُ مَا تُوَسْوِسُ بِهٖ نَفْسُهٗ ۖوَنَحْنُ اَقْرَبُ اِلَيْهِ مِنْ حَبْلِ الْوَرِيْدِ

walaqad khalaqn*aa* **a**l-ins*aa*na wana'lamu m*aa* tuwaswisu bihi nafsuhu wana*h*nu aqrabu ilayhi min *h*abli **a**lwariid**i**

Dan sungguh, Kami telah menciptakan manusia dan mengetahui apa yang dibisikkan oleh hatinya, dan Kami lebih dekat kepadanya daripada urat lehernya.

50:17

# اِذْ يَتَلَقَّى الْمُتَلَقِّيٰنِ عَنِ الْيَمِيْنِ وَعَنِ الشِّمَالِ قَعِيْدٌ

i*dz* yatalaqq*aa* **a**lmutalaqqiy*aa*ni 'ani **a**lyamiini wa'ani **al**sysyim*aa*li qa'iid**un**

(Ingatlah) ketika dua malaikat mencatat (perbuatannya), yang satu duduk di sebelah kanan dan yang lain di sebelah kiri.

50:18

# مَا يَلْفِظُ مِنْ قَوْلٍ اِلَّا لَدَيْهِ رَقِيْبٌ عَتِيْدٌ

m*aa* yalfi*zh*u min qawlin ill*aa* ladayhi raqiibun 'atiid**un**

Tidak ada suatu kata yang diucapkannya melainkan ada di sisinya malaikat pengawas yang selalu siap (mencatat).

50:19

# وَجَاۤءَتْ سَكْرَةُ الْمَوْتِ بِالْحَقِّ ۗذٰلِكَ مَا كُنْتَ مِنْهُ تَحِيْدُ

waj*aa*-at sakratu **a**lmawti bi**a**l*h*aqqi *dzaa*lika m*aa* kunta minhu ta*h*iid**u**

Dan datanglah sakaratul maut dengan sebenar-benarnya. Itulah yang dahulu hendak kamu hindari.

50:20

# وَنُفِخَ فِى الصُّوْرِۗ ذٰلِكَ يَوْمُ الْوَعِيْدِ

wanufikha fii **al***shsh*uuri *dzaa*lika yawmu **a**lwa'iid**i**

Dan ditiuplah sangkakala. Itulah hari yang diancamkan.

50:21

# وَجَاۤءَتْ كُلُّ نَفْسٍ مَّعَهَا سَاۤىِٕقٌ وَّشَهِيْدٌ

waj*aa*-at kullu nafsin ma'ah*aa* s*aa*-iqun wasyahiid**un**

Setiap orang akan datang bersama (malaikat) penggiring dan (malaikat) saksi.

50:22

# لَقَدْ كُنْتَ فِيْ غَفْلَةٍ مِّنْ هٰذَا فَكَشَفْنَا عَنْكَ غِطَاۤءَكَ فَبَصَرُكَ الْيَوْمَ حَدِيْدٌ

laqad kunta fii ghaflatin min h*aadzaa* fakasyafn*aa* 'anka ghi*thaa*-aka faba*sh*aruka **a**lyawma *h*adiid**un**

Sungguh, kamu dahulu lalai tentang (peristiwa) ini, maka Kami singkapkan tutup (yang menutupi) matamu, sehingga penglihatanmu pada hari ini sangat tajam.

50:23

# وَقَالَ قَرِيْنُهٗ هٰذَا مَا لَدَيَّ عَتِيْدٌۗ

waq*aa*la qariinuhu h*aadzaa* m*aa* ladayya 'atiid**un**

Dan (malaikat) yang menyertainya berkata, “Inilah (catatan perbuatan) yang ada padaku.”

50:24

# اَلْقِيَا فِيْ جَهَنَّمَ كُلَّ كَفَّارٍ عَنِيْدٍ

alqiy*aa* fii jahannama kulla kaff*aa*rin 'aniid**in**

(Allah berfirman), “Lemparkanlah olehmu berdua ke dalam neraka Jahanam semua orang yang sangat ingkar dan keras kepala,

50:25

# مَنَّاعٍ لِّلْخَيْرِ مُعْتَدٍ مُّرِيْبٍۙ

mann*aa*'in lilkhayri mu'tadin muriib**in**

yang sangat enggan melakukan kebajikan, melampaui batas dan bersikap ragu-ragu,

50:26

# ۨالَّذِيْ جَعَلَ مَعَ اللّٰهِ اِلٰهًا اٰخَرَ فَاَلْقِيٰهُ فِى الْعَذَابِ الشَّدِيْدِ

**al**la*dz*ii ja'ala ma'a **al**l*aa*hi il*aa*han *aa*khara fa-alqiy*aa*hu fii **a**l'a*dzaa*bi **al**sysyadiid**i**

yang mempersekutukan Allah dengan tuhan lain, maka lemparkanlah dia ke dalam azab yang keras.”

50:27

# ۞ قَالَ قَرِيْنُهٗ رَبَّنَا مَآ اَطْغَيْتُهٗ وَلٰكِنْ كَانَ فِيْ ضَلٰلٍۢ بَعِيْدٍ

q*aa*la qariinuhu rabban*aa* m*aa* a*th*ghaytuhu wal*aa*kin k*aa*na fii *dh*al*aa*lin ba'iid**in**

(Setan) yang menyertainya berkata (pula), “Ya Tuhan kami, aku tidak menyesatkannya tetapi dia sendiri yang berada dalam kesesatan yang jauh.”

50:28

# قَالَ لَا تَخْتَصِمُوْا لَدَيَّ وَقَدْ قَدَّمْتُ اِلَيْكُمْ بِالْوَعِيْدِ

q*aa*la l*aa* takhta*sh*imuu ladayya waqad qaddamtu ilaykum bi**a**lwa'iid**i**

(Allah) berfirman, “Janganlah kamu bertengkar di hadapan-Ku, dan sungguh, dahulu Aku telah memberikan ancaman kepadamu.

50:29

# مَا يُبَدَّلُ الْقَوْلُ لَدَيَّ وَمَآ اَنَا۠ بِظَلَّامٍ لِّلْعَبِيْدِ ࣖ

m*aa* yubaddalu **a**lqawlu ladayya wam*aa* an*aa* bi*zh*all*aa*min lil'abiid**i**

Keputusan-Ku tidak dapat diubah dan Aku tidak menzalimi hamba-hamba-Ku.”

50:30

# يَوْمَ نَقُوْلُ لِجَهَنَّمَ هَلِ امْتَلَـْٔتِ وَتَقُوْلُ هَلْ مِنْ مَّزِيْدٍ

yawma naquulu lijahannama hali imtala/ti wataquulu hal min maziid**in**

(Ingatlah) pada hari (ketika) Kami bertanya kepada Jahanam, “Apakah kamu sudah penuh?” Ia menjawab, “Masih adakah tambahan?”

50:31

# وَاُزْلِفَتِ الْجَنَّةُ لِلْمُتَّقِيْنَ غَيْرَ بَعِيْدٍ

wauzlifati **a**ljannatu lilmuttaqiina ghayra ba'iid**in**

Sedangkan surga didekatkan kepada orang-orang yang bertakwa pada tempat yang tidak jauh (dari mereka).

50:32

# هٰذَا مَا تُوْعَدُوْنَ لِكُلِّ اَوَّابٍ حَفِيْظٍۚ

h*aadzaa* m*aa* tuu'aduuna likulli aww*aa*bin *h*afii*zh***in**

(Kepada mereka dikatakan), “Inilah nikmat yang dijanjikan kepadamu, (yaitu) kepada setiap hamba yang senantiasa bertobat (kepada Allah) dan memelihara (semua peraturan-peraturan-Nya).

50:33

# مَنْ خَشِيَ الرَّحْمٰنَ بِالْغَيْبِ وَجَاۤءَ بِقَلْبٍ مُّنِيْبٍۙ

man khasyiya **al**rra*h*m*aa*na bi**a**lghaybi waj*aa*-a biqalbin muniib**in**

(Yaitu) orang yang takut kepada Allah Yang Maha Pengasih sekalipun tidak kelihatan (olehnya) dan dia datang dengan hati yang bertobat,

50:34

# ۨادْخُلُوْهَا بِسَلٰمٍ ۗذٰلِكَ يَوْمُ الْخُلُوْدِ

udkhuluuh*aa* bisal*aa*min *dzaa*lika yawmu **a**lkhuluud**i**

masuklah ke (dalam surga) dengan aman dan damai. Itulah hari yang abadi.”

50:35

# لَهُمْ مَّا يَشَاۤءُوْنَ فِيْهَا وَلَدَيْنَا مَزِيْدٌ

lahum m*aa* yasy*aa*uuna fiih*aa* waladayn*aa* maziid**un**

Mereka di dalamnya memperoleh apa yang mereka kehendaki, dan pada Kami ada tambahannya.

50:36

# وَكَمْ اَهْلَكْنَا قَبْلَهُمْ مِّنْ قَرْنٍ هُمْ اَشَدُّ مِنْهُمْ بَطْشًا فَنَقَّبُوْا فِى الْبِلَادِۗ هَلْ مِنْ مَّحِيْصٍ

wakam ahlakn*aa* qablahum min qarnin hum asyaddu minhum ba*th*syan fanaqqabuu fii **a**lbil*aa*di hal min ma*h*ii*sh***in**

Dan betapa banyak umat yang telah Kami binasakan sebelum mereka, (padahal) mereka lebih hebat kekuatannya daripada mereka (umat yang belakangan) ini. Mereka pernah menjelajah di beberapa negeri. Adakah tempat pelarian (dari kebinasaan bagi mereka)?

50:37

# اِنَّ فِيْ ذٰلِكَ لَذِكْرٰى لِمَنْ كَانَ لَهٗ قَلْبٌ اَوْ اَلْقَى السَّمْعَ وَهُوَ شَهِيْدٌ

inna fii *dzaa*lika la*dz*ikr*aa* liman k*aa*na lahu qalbun aw **a**lq*aa* **al**ssam'a wahuwa syahiid**un**

Sungguh, pada yang demikian itu pasti terdapat peringatan bagi orang-orang yang mempunyai hati atau yang menggunakan pendengarannya, sedang dia menyaksikannya.

50:38

# وَلَقَدْ خَلَقْنَا السَّمٰوٰتِ وَالْاَرْضَ وَمَا بَيْنَهُمَا فِيْ سِتَّةِ اَيَّامٍۖ وَّمَا مَسَّنَا مِنْ لُّغُوْبٍ

walaqad khalaqn*aa* **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*a wam*aa* baynahum*aa* fii sittati ayy*aa*min wam*aa* massan*aa* min lughuub**in**

Dan sungguh, Kami telah menciptakan langit dan bumi dan apa yang ada antara keduanya dalam enam masa, dan Kami tidak merasa letih sedikit pun.

50:39

# فَاصْبِرْ عَلٰى مَا يَقُوْلُوْنَ وَسَبِّحْ بِحَمْدِ رَبِّكَ قَبْلَ طُلُوْعِ الشَّمْسِ وَقَبْلَ الْغُرُوْبِ

fa**i***sh*bir 'al*aa* m*aa* yaquuluuna wasabbi*h* bi*h*amdi rabbika qabla *th*uluu'i **al**sysyamsi waqabla **a**lghuruub**i**

Maka bersabarlah engkau (Muhammad) terhadap apa yang mereka katakan dan bertasbihlah dengan memuji Tuhanmu sebelum matahari terbit dan sebelum terbenam.

50:40

# وَمِنَ الَّيْلِ فَسَبِّحْهُ وَاَدْبَارَ السُّجُوْدِ

wamina **al**layli fasabbi*h*hu wa-adb*aa*ra **al**ssujuud**i**

Dan bertasbihlah kepada-Nya pada malam hari dan setiap selesai salat.

50:41

# وَاسْتَمِعْ يَوْمَ يُنَادِ الْمُنَادِ مِنْ مَّكَانٍ قَرِيْبٍ

wa**i**stami' yawma yun*aa*di **a**lmun*aa*di min mak*aa*nin qariib**in**

Dan dengarkanlah (seruan) pada hari (ketika) penyeru (malaikat) menyeru dari tempat yang dekat.

50:42

# يَوْمَ يَسْمَعُوْنَ الصَّيْحَةَ بِالْحَقِّ ۗذٰلِكَ يَوْمُ الْخُرُوْجِ

yawma yasma'uuna **al***shsh*ay*h*ata bi**a**l*h*aqqi *dzaa*lika yawmu **a**lkhuruuj**i**

(Yaitu) pada hari (ketika) mereka men-dengar suara dahsyat dengan sebenarnya. Itulah hari keluar (dari kubur).

50:43

# اِنَّا نَحْنُ نُحْيٖ وَنُمِيْتُ وَاِلَيْنَا الْمَصِيْرُۙ

inn*aa* na*h*nu nu*h*yii wanumiitu wa-ilayn*aa* **a**lma*sh*iir**u**

Sungguh, Kami yang menghidupkan dan mematikan dan kepada Kami tempat kembali (semua makhluk).

50:44

# يَوْمَ تَشَقَّقُ الْاَرْضُ عَنْهُمْ سِرَاعًا ۗذٰلِكَ حَشْرٌ عَلَيْنَا يَسِيْرٌ

yawma tasyaqqaqu **a**l-ar*dh*u 'anhum sir*aa*'an *dzaa*lika *h*asyrun 'alayn*aa* yasiir**un**

(Yaitu) pada hari (ketika) bumi terbelah, mereka keluar dengan cepat. Yang demikian itu adalah pengumpulan yang mudah bagi Kami.

50:45

# نَحْنُ اَعْلَمُ بِمَا يَقُوْلُوْنَ وَمَآ اَنْتَ عَلَيْهِمْ بِجَبَّارٍۗ فَذَكِّرْ بِالْقُرْاٰنِ مَنْ يَّخَافُ وَعِيْدِ ࣖ

na*h*nu a'lamu bim*aa* yaquuluuna wam*aa* anta 'alayhim bijabb*aa*rin fa*dz*akkir bi**a**lqur-*aa*ni man yakh*aa*fu wa'iid**i**

Kami lebih mengetahui tentang apa yang mereka katakan, dan engkau (Muhammad) bukanlah seorang pemaksa terhadap mereka. Maka berilah peringatan dengan Al-Qur'an kepada siapa pun yang takut kepada ancaman-Ku.

<!--EndFragment-->