---
title: (59) Al-Hasyr - الحشر
date: 2021-10-27T04:14:47.246Z
ayat: 59
description: "Jumlah Ayat: 24 / Arti: Pengusiran"
---
<!--StartFragment-->

59:1

# سَبَّحَ لِلّٰهِ مَا فِى السَّمٰوٰتِ وَمَا فِى الْاَرْضِۚ وَهُوَ الْعَزِيْزُ الْحَكِيْمُ

sabba*h*a lill*aa*hi m*aa* fii **al**ssam*aa*w*aa*ti wam*aa* fii **a**l-ar*dh*i wahuwa **a**l'aziizu **a**l*h*akiim**u**

Apa yang ada di langit dan apa yang ada di bumi bertasbih kepada Allah; dan Dialah Yang Mahaperkasa, Mahabijaksana.

59:2

# هُوَ الَّذِيْٓ اَخْرَجَ الَّذِيْنَ كَفَرُوْا مِنْ اَهْلِ الْكِتٰبِ مِنْ دِيَارِهِمْ لِاَوَّلِ الْحَشْرِۗ مَا ظَنَنْتُمْ اَنْ يَّخْرُجُوْا وَظَنُّوْٓا اَنَّهُمْ مَّانِعَتُهُمْ حُصُوْنُهُمْ مِّنَ اللّٰهِ فَاَتٰىهُمُ اللّٰهُ مِنْ حَيْثُ لَمْ يَحْتَسِبُوْا وَ

huwa **al**la*dz*ii akhraja **al**la*dz*iina kafaruu min ahli **a**lkit*aa*bi min diy*aa*rihim li-awwali **a**l*h*asyri m*aa* *zh*anantum an yakhrujuu wa*zh*annuu

Dialah yang mengeluarkan orang-orang kafir di antara Ahli Kitab dari kampung halamannya pada saat pengusiran yang pertama. Kamu tidak menyangka, bahwa mereka akan keluar dan mereka pun yakin, benteng-benteng mereka akan dapat mempertahankan mereka dari (s

59:3

# وَلَوْلَآ اَنْ كَتَبَ اللّٰهُ عَلَيْهِمُ الْجَلَاۤءَ لَعَذَّبَهُمْ فِى الدُّنْيَاۗ وَلَهُمْ فِى الْاٰخِرَةِ عَذَابُ النَّارِ

walawl*aa* an kataba **al**l*aa*hu 'alayhimu **a**ljal*aa*-a la'a*dzdz*abahum fii **al**dduny*aa* walahum fii **a**l-*aa*khirati 'a*dzaa*bu **al**nn*aa*

*Dan sekiranya tidak karena Allah telah menetapkan pengusiran terhadap mereka, pasti Allah mengazab mereka di dunia. Dan di akhirat mereka akan mendapat azab neraka.*









59:4

# ذٰلِكَ بِاَنَّهُمْ شَاۤقُّوا اللّٰهَ وَرَسُوْلَهٗ ۖوَمَنْ يُّشَاۤقِّ اللّٰهَ فَاِنَّ اللّٰهَ شَدِيْدُ الْعِقَابِ

*dzaa*lika bi-annahum sy*aa*qquu **al**l*aa*ha warasuulahu waman yusy*aa*qqi **al**l*aa*ha fa-inna **al**l*aa*ha syadiidu **a**l'iq*aa*b**i**

Yang demikian itu karena sesungguhnya mereka menentang Allah dan Rasul-Nya. Barangsiapa menentang Allah, maka sesungguhnya Allah sangat keras hukuman-Nya.

59:5

# مَا قَطَعْتُمْ مِّنْ لِّيْنَةٍ اَوْ تَرَكْتُمُوْهَا قَاۤىِٕمَةً عَلٰٓى اُصُوْلِهَا فَبِاِذْنِ اللّٰهِ وَلِيُخْزِيَ الْفٰسِقِيْنَ

m*aa* qa*th*a'tum min liinatin aw taraktumuuh*aa* q*aa*-imatan 'al*aa* u*sh*uulih*aa* fabi-i*dz*ni **al**l*aa*hi waliyukhziya **a**lf*aa*siqiin**a**

Apa yang kamu tebang di antara pohon kurma (milik orang-orang kafir) atau yang kamu biarkan (tumbuh) berdiri di atas pokoknya, maka (itu terjadi) dengan izin Allah; dan karena Dia hendak memberikan kehinaan kepada orang-orang fasik.

59:6

# وَمَآ اَفَاۤءَ اللّٰهُ عَلٰى رَسُوْلِهٖ مِنْهُمْ فَمَآ اَوْجَفْتُمْ عَلَيْهِ مِنْ خَيْلٍ وَّلَا رِكَابٍ وَّلٰكِنَّ اللّٰهَ يُسَلِّطُ رُسُلَهٗ عَلٰى مَنْ يَّشَاۤءُۗ وَاللّٰهُ عَلٰى كُلِّ شَيْءٍ قَدِيْرٌ

wam*aa* af*aa*-a **al**l*aa*hu 'al*aa* rasuulihi minhum fam*aa* awjaftum 'alayhi min khaylin wal*aa* rik*aa*bin wal*aa*kinna **al**l*aa*ha yusalli*th*u rusulahu 'al*aa* man y

Dan harta rampasan fai' dari mereka yang diberikan Allah kepada Rasul-Nya, kamu tidak memerlukan kuda atau unta untuk mendapatkannya, tetapi Allah memberikan kekuasaan kepada rasul-rasul-Nya terhadap siapa yang Dia kehendaki. Dan Allah Mahakuasa atas sega

59:7

# مَآ اَفَاۤءَ اللّٰهُ عَلٰى رَسُوْلِهٖ مِنْ اَهْلِ الْقُرٰى فَلِلّٰهِ وَلِلرَّسُوْلِ وَلِذِى الْقُرْبٰى وَالْيَتٰمٰى وَالْمَسٰكِيْنِ وَابْنِ السَّبِيْلِۙ كَيْ لَا يَكُوْنَ دُوْلَةً ۢ بَيْنَ الْاَغْنِيَاۤءِ مِنْكُمْۗ وَمَآ اٰتٰىكُمُ الرَّسُوْلُ فَخُذُوْهُ

m*aa* af*aa*-a **al**l*aa*hu 'al*aa* rasuulihi min ahli **a**lqur*aa* falill*aa*hi wali**l**rrasuuli wali*dz*ii **a**lqurb*aa* wa**a**lyat*aa*m

Harta rampasan (fai') dari mereka yang diberikan Allah kepada Rasul-Nya (yang berasal) dari penduduk beberapa negeri, adalah untuk Allah, Rasul, kerabat (Rasul), anak-anak yatim, orang-orang miskin dan untuk orang-orang yang dalam perjalanan, agar harta i

59:8

# لِلْفُقَرَاۤءِ الْمُهٰجِرِيْنَ الَّذِيْنَ اُخْرِجُوْا مِنْ دِيَارِهِمْ وَاَمْوَالِهِمْ يَبْتَغُوْنَ فَضْلًا مِّنَ اللّٰهِ وَرِضْوَانًا وَّيَنْصُرُوْنَ اللّٰهَ وَرَسُوْلَهٗ ۗ اُولٰۤىِٕكَ هُمُ الصّٰدِقُوْنَۚ

lilfuqar*aa*-i **a**lmuh*aa*jiriina **al**la*dz*iina ukhrijuu min diy*aa*rihim wa-amw*aa*lihim yabtaghuuna fa*dh*lan mina **al**l*aa*hi wari*dh*w*aa*nan wayan*sh*uruu

(Harta rampasan itu juga) untuk orang-orang fakir yang berhijrah yang terusir dari kampung halamannya dan meninggalkan harta bendanya demi mencari karunia dari Allah dan keridaan(-Nya) dan (demi) menolong (agama) Allah dan Rasul-Nya. Mereka itulah orang-o

59:9

# وَالَّذِيْنَ تَبَوَّءُو الدَّارَ وَالْاِيْمَانَ مِنْ قَبْلِهِمْ يُحِبُّوْنَ مَنْ هَاجَرَ اِلَيْهِمْ وَلَا يَجِدُوْنَ فِيْ صُدُوْرِهِمْ حَاجَةً مِّمَّآ اُوْتُوْا وَيُؤْثِرُوْنَ عَلٰٓى اَنْفُسِهِمْ وَلَوْ كَانَ بِهِمْ خَصَاصَةٌ ۗوَمَنْ يُّوْقَ شُحَّ نَفْس

wa**a**lla*dz*iina tabawwauu **al**dd*aa*ra wa**a**l-iim*aa*na min qablihim yu*h*ibbuuna man h*aa*jara ilayhim wal*aa* yajiduuna fii *sh*uduurihim *haa*jatan mimm*aa* uutu

Dan orang-orang (Ansar) yang telah menempati kota Madinah dan telah beriman sebelum (kedatangan) mereka (Muhajirin), mereka mencintai orang yang berhijrah ke tempat mereka. Dan mereka tidak menaruh keinginan dalam hati mereka terhadap apa yang diberikan k

59:10

# وَالَّذِيْنَ جَاۤءُوْ مِنْۢ بَعْدِهِمْ يَقُوْلُوْنَ رَبَّنَا اغْفِرْ لَنَا وَلِاِخْوَانِنَا الَّذِيْنَ سَبَقُوْنَا بِالْاِيْمَانِ وَلَا تَجْعَلْ فِيْ قُلُوْبِنَا غِلًّا لِّلَّذِيْنَ اٰمَنُوْا رَبَّنَآ اِنَّكَ رَءُوْفٌ رَّحِيْمٌ ࣖ

wa**a**lla*dz*iina j*aa*uu min ba'dihim yaquuluuna rabban*aa* ighfir lan*aa* wali-ikhw*aa*nin*aa* **al**la*dz*iina sabaquun*aa* bi**a**l-iim*aa*ni wal*aa* taj'al fii

Dan orang-orang yang datang sesudah mereka (Muhajirin dan Ansar), mereka berdoa, “Ya Tuhan kami, ampunilah kami dan saudara-saudara kami yang telah beriman lebih dahulu dari kami, dan janganlah Engkau tanamkan kedengkian dalam hati kami terhadap orang-ora

59:11

# ۞ اَلَمْ تَرَ اِلَى الَّذِيْنَ نَافَقُوْا يَقُوْلُوْنَ لِاِخْوَانِهِمُ الَّذِيْنَ كَفَرُوْا مِنْ اَهْلِ الْكِتٰبِ لَىِٕنْ اُخْرِجْتُمْ لَنَخْرُجَنَّ مَعَكُمْ وَلَا نُطِيْعُ فِيْكُمْ اَحَدًا اَبَدًاۙ وَّاِنْ قُوْتِلْتُمْ لَنَنْصُرَنَّكُمْۗ وَاللّٰهُ يَشْهَ

alam tara il*aa* **al**la*dz*iina n*aa*faquu yaquuluuna li-ikhw*aa*nihimu **al**la*dz*iina kafaruu min ahli **a**lkit*aa*bi la-in ukhrijtum lanakhrujanna ma'akum wal*aa* nu*th*

Tidakkah engkau memperhatikan orang-orang munafik yang berkata kepada saudara-saudaranya yang kafir di antara Ahli Kitab, “Sungguh, jika kamu diusir niscaya kami pun akan keluar bersama kamu; dan kami selama-lamanya tidak akan patuh kepada siapa pun demi

59:12

# لَىِٕنْ اُخْرِجُوْا لَا يَخْرُجُوْنَ مَعَهُمْۚ وَلَىِٕنْ قُوْتِلُوْا لَا يَنْصُرُوْنَهُمْۚ وَلَىِٕنْ نَّصَرُوْهُمْ لَيُوَلُّنَّ الْاَدْبَارَۙ ثُمَّ لَا يُنْصَرُوْنَ

la-in ukhrijuu l*aa* yakhrujuuna ma'ahum wala-in quutiluu l*aa* yan*sh*uruunahum wala-in na*sh*aruuhum layuwallunna **a**l-adb*aa*ra tsumma l*aa* yun*sh*aruun**a**

Sungguh, jika mereka diusir, orang-orang munafik itu tidak akan keluar bersama mereka, dan jika mereka di-perangi; mereka (juga) tidak akan menolongnya; dan kalau pun mereka menolongnya pastilah mereka akan berpaling lari ke belakang, kemudian mereka tida

59:13

# لَاَنْتُمْ اَشَدُّ رَهْبَةً فِيْ صُدُوْرِهِمْ مِّنَ اللّٰهِ ۗذٰلِكَ بِاَنَّهُمْ قَوْمٌ لَّا يَفْقَهُوْنَ

la-antum asyaddu rahbatan fii *sh*uduurihim mina **al**l*aa*hi *dzaa*lika bi-annahum qawmun l*aa* yafqahuun**a**

Sesungguhnya dalam hati mereka, kamu (Muslimin) lebih ditakuti daripada Allah. Yang demikian itu karena mereka orang-orang yang tidak mengerti.

59:14

# لَا يُقَاتِلُوْنَكُمْ جَمِيْعًا اِلَّا فِيْ قُرًى مُّحَصَّنَةٍ اَوْ مِنْ وَّرَاۤءِ جُدُرٍۗ بَأْسُهُمْ بَيْنَهُمْ شَدِيْدٌ ۗ تَحْسَبُهُمْ جَمِيْعًا وَّقُلُوْبُهُمْ شَتّٰىۗ ذٰلِكَ بِاَنَّهُمْ قَوْمٌ لَّا يَعْقِلُوْنَۚ

l*aa* yuq*aa*tiluunakum jamii'an ill*aa* fii quran mu*h*a*shsh*anatin aw min war*aa*-i judurin ba/suhum baynahum syadiidun ta*h*sabuhum jamii'an waquluubuhum syatt*aa* *dzaa*lika bi-annahum qawmun l*aa* ya

Mereka tidak akan memerangi kamu (secara) bersama-sama, kecuali di negeri-negeri yang berbenteng atau di balik tembok. Permusuhan antara sesama mereka sangat hebat. Kamu kira mereka itu bersatu padahal hati mereka terpecah belah. Yang demikian itu karena

59:15

# كَمَثَلِ الَّذِيْنَ مِنْ قَبْلِهِمْ قَرِيْبًا ذَاقُوْا وَبَالَ اَمْرِهِمْۚ وَلَهُمْ عَذَابٌ اَلِيْمٌۚ

kamatsali **al**la*dz*iina min qablihim qariiban *dzaa*quu wab*aa*la amrihim walahum 'a*dzaa*bun **a**liim**un**

(Mereka) seperti orang-orang yang sebelum mereka (Yahudi) belum lama berselang, telah merasakan akibat buruk (terusir) disebabkan perbuatan mereka sendiri. Dan mereka akan men-dapat azab yang pedih.

59:16

# كَمَثَلِ الشَّيْطٰنِ اِذْ قَالَ لِلْاِنْسَانِ اكْفُرْۚ فَلَمَّا كَفَرَ قَالَ اِنِّيْ بَرِيْۤءٌ مِّنْكَ اِنِّيْٓ اَخَافُ اللّٰهَ رَبَّ الْعٰلَمِيْنَ

kamatsali **al**sysyay*thaa*ni i*dz* q*aa*la lil-ins*aa*ni ukfur falamm*aa* kafara q*aa*la innii barii-un minka innii akh*aa*fu **al**l*aa*ha rabba **a**l'*aa*lamii

(Bujukan orang-orang munafik itu) seperti (bujukan) setan ketika ia berkata kepada manusia, “Kafirlah kamu!” Kemudian ketika manusia itu menjadi kafir ia berkata, “Sesungguhnya aku berlepas diri dari kamu, karena sesungguhnya aku takut kepada Allah, Tuhan

59:17

# فَكَانَ عَاقِبَتَهُمَآ اَنَّهُمَا فِى النَّارِ خَالِدَيْنِ فِيْهَاۗ وَذٰلِكَ جَزٰۤؤُا الظّٰلِمِيْنَ ࣖ

fak*aa*na '*aa*qibatahum*aa* annahum*aa* fii **al**nn*aa*ri kh*aa*lidayni fiih*aa* wa*dzaa*lika jaz*aa*u **al***zhzhaa*limiin**a**

Maka kesudahan bagi keduanya, bahwa keduanya masuk ke dalam neraka, kekal di dalamnya. Demikianlah balasan bagi orang-orang zalim.

59:18

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوا اتَّقُوا اللّٰهَ وَلْتَنْظُرْ نَفْسٌ مَّا قَدَّمَتْ لِغَدٍۚ وَاتَّقُوا اللّٰهَ ۗاِنَّ اللّٰهَ خَبِيْرٌ ۢبِمَا تَعْمَلُوْنَ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu ittaquu **al**l*aa*ha waltan*zh*ur nafsun m*aa* qaddamat lighadin wa**i**ttaquu **al**l*aa*ha inna **al**l<

Wahai orang-orang yang beriman! Bertakwalah kepada Allah dan hendaklah setiap orang memperhatikan apa yang telah diperbuatnya untuk hari esok (akhirat), dan bertakwalah kepada Allah. Sungguh, Allah Mahateliti terhadap apa yang kamu kerjakan.

59:19

# وَلَا تَكُوْنُوْا كَالَّذِيْنَ نَسُوا اللّٰهَ فَاَنْسٰىهُمْ اَنْفُسَهُمْۗ اُولٰۤىِٕكَ هُمُ الْفٰسِقُوْنَ

wal*aa* takuunuu ka**a**lla*dz*iina nasuu **al**l*aa*ha fa-ans*aa*hum anfusahum ul*aa*-ika humu **a**lf*aa*siquun**a**

Dan janganlah kamu seperti orang-orang yang lupa kepada Allah, sehingga Allah menjadikan mereka lupa akan diri sendiri. Mereka itulah orang-orang fasik.

59:20

# لَا يَسْتَوِيْٓ اَصْحٰبُ النَّارِ وَاَصْحٰبُ الْجَنَّةِۗ اَصْحٰبُ الْجَنَّةِ هُمُ الْفَاۤىِٕزُوْنَ

l*aa* yastawii a*sh*-*haa*bu **al**nn*aa*ri wa-a*sh*-*haa*bu **a**ljannati a*sh*-*haa*bu **a**ljannati humu **a**lf*aa*-izuun**a**

Tidak sama para penghuni neraka dengan para penghuni surga; para penghuni surga itulah orang-orang yang memperoleh kemenangan.

59:21

# لَوْ اَنْزَلْنَا هٰذَا الْقُرْاٰنَ عَلٰى جَبَلٍ لَّرَاَيْتَهٗ خَاشِعًا مُّتَصَدِّعًا مِّنْ خَشْيَةِ اللّٰهِ ۗوَتِلْكَ الْاَمْثَالُ نَضْرِبُهَا لِلنَّاسِ لَعَلَّهُمْ يَتَفَكَّرُوْنَ

law anzaln*aa* h*aadzaa* **a**lqur-*aa*na 'al*aa* jabalin lara-aytahu kh*aa*syi'an muta*sh*addi'an min khasyyati **al**l*aa*hi watilka **a**l-amts*aa*lu na*dh*ribuh*aa*

*Sekiranya Kami turunkan Al-Qur'an ini kepada sebuah gunung, pasti kamu akan melihatnya tunduk terpecah belah disebabkan takut kepada Allah. Dan perumpamaan-perumpamaan itu Kami buat untuk manusia agar mereka berpikir.*









59:22

# هُوَ اللّٰهُ الَّذِيْ لَآ اِلٰهَ اِلَّا هُوَۚ عَالِمُ الْغَيْبِ وَالشَّهَادَةِۚ هُوَ الرَّحْمٰنُ الرَّحِيْمُ

huwa **al**l*aa*hu **al**la*dz*ii l*aa* il*aa*ha ill*aa* huwa '*aa*limu **a**lghaybi wa**al**sysyah*aa*dati huwa **al**rra*h*m*aa*nu **al**

**Dialah Allah tidak ada tuhan selain Dia. Yang Mengetahui yang gaib dan yang nyata, Dialah Yang Maha Pengasih, Maha Penyayang.**









59:23

# هُوَ اللّٰهُ الَّذِيْ لَآ اِلٰهَ اِلَّا هُوَ ۚ اَلْمَلِكُ الْقُدُّوْسُ السَّلٰمُ الْمُؤْمِنُ الْمُهَيْمِنُ الْعَزِيْزُ الْجَبَّارُ الْمُتَكَبِّرُۗ سُبْحٰنَ اللّٰهِ عَمَّا يُشْرِكُوْنَ

huwa **al**l*aa*hu **al**la*dz*ii l*aa* il*aa*ha ill*aa* huwa **a**lmaliku **a**lqudduusu **al**ssal*aa*mu **a**lmu/minu **a**lmuhaymi

Dialah Allah tidak ada tuhan selain Dia. Maharaja, Yang Mahasuci, Yang Mahasejahtera, Yang Menjaga Keamanan, Pemelihara Keselamatan, Yang Mahaperkasa, Yang Mahakuasa, Yang Memiliki Segala Keagungan, Mahasuci Allah dari apa yang mereka persekutukan.

59:24

# هُوَ اللّٰهُ الْخَالِقُ الْبَارِئُ الْمُصَوِّرُ لَهُ الْاَسْمَاۤءُ الْحُسْنٰىۗ يُسَبِّحُ لَهٗ مَا فِى السَّمٰوٰتِ وَالْاَرْضِۚ وَهُوَ الْعَزِيْزُ الْحَكِيْمُ ࣖ

huwa **al**l*aa*hu **a**lkh*aa*liqu **a**lb*aa*ri-u **a**lmu*sh*awwiru lahu **a**l-asm*aa*u **a**l*h*usn*aa* yusabbi*h*u lahu m*aa*

Dialah Allah Yang Menciptakan, Yang Mengadakan, Yang Membentuk Rupa, Dia memiliki nama-nama yang indah. Apa yang di langit dan di bumi bertasbih kepada-Nya. Dan Dialah Yang Mahaperkasa, Mahabijaksana.

<!--EndFragment-->