---
title: (56) Al-Waqi'ah - الواقعة
date: 2021-10-27T04:13:27.685Z
ayat: 56
description: "Jumlah Ayat: 96 / Arti: Hari Kiamat"
---
<!--StartFragment-->

56:1

# اِذَا وَقَعَتِ الْوَاقِعَةُۙ

i*dzaa* waqa'ati **a**lw*aa*qi'at**u**

Apabila terjadi hari Kiamat,

56:2

# لَيْسَ لِوَقْعَتِهَا كَاذِبَةٌ ۘ

laysa liwaq'atih*aa* k*aadz*iba**tun**

terjadinya tidak dapat didustakan (disangkal).

56:3

# خَافِضَةٌ رَّافِعَةٌ

kh*aa*fi*dh*atun r*aa*fi'a**tun**

(Kejadian itu) merendahkan (satu golongan) dan meninggikan (golongan yang lain).

56:4

# اِذَا رُجَّتِ الْاَرْضُ رَجًّاۙ

i*dzaa* rujjati **a**l-ar*dh*u rajj*aa***n**

Apabila bumi diguncangkan sedahsyat-dahsyatnya,

56:5

# وَّبُسَّتِ الْجِبَالُ بَسًّاۙ

wabussati **a**ljib*aa*lu bass*aa***n**

dan gunung-gunung dihancurluluhkan sehancur-hancurnya,

56:6

# فَكَانَتْ هَبَاۤءً مُّنْۢبَثًّاۙ

fak*aa*nat hab*aa*-an munbatsts*aa***n**

maka jadilah ia debu yang beterbangan,

56:7

# وَّكُنْتُمْ اَزْوَاجًا ثَلٰثَةً ۗ

wakuntum azw*aa*jan tsal*aa*tsa**tan**

dan kamu menjadi tiga golongan,

56:8

# فَاَصْحٰبُ الْمَيْمَنَةِ ەۙ مَآ اَصْحٰبُ الْمَيْمَنَةِ ۗ

fa-a*sh*-*haa*bu **a**lmaymanati m*aa* a*sh*-*haa*bu **a**lmaymanat**i**

yaitu golongan kanan, alangkah mulianya golongan kanan itu,

56:9

# وَاَصْحٰبُ الْمَشْـَٔمَةِ ەۙ مَآ اَصْحٰبُ الْمَشْـَٔمَةِ ۗ

wa-a*sh*-*haa*bu **a**lmasy-amati m*aa* a*sh*-*haa*bu **a**lmasy-amat**i**

dan golongan kiri, alangkah sengsaranya golongan kiri itu,

56:10

# وَالسّٰبِقُوْنَ السّٰبِقُوْنَۙ

wa**al**ss*aa*biquuna **al**ss*aa*biquun**a**

dan orang-orang yang paling dahulu (beriman), merekalah yang paling dahulu (masuk surga).

56:11

# اُولٰۤىِٕكَ الْمُقَرَّبُوْنَۚ

ul*aa*-ika **a**lmuqarrabuun**a**

Mereka itulah orang yang dekat (kepada Allah),

56:12

# فِيْ جَنّٰتِ النَّعِيْمِ

fii jann*aa*ti **al**nna'iim**i**

Berada dalam surga kenikmatan,

56:13

# ثُلَّةٌ مِّنَ الْاَوَّلِيْنَۙ

tsullatun mina **a**l-awwaliin**a**

segolongan besar dari orang-orang yang terdahulu,

56:14

# وَقَلِيْلٌ مِّنَ الْاٰخِرِيْنَۗ

waqaliilun mina **a**l-*aa*khiriinaa

dan segolongan kecil dari orang-orang yang kemudian.

56:15

# عَلٰى سُرُرٍ مَّوْضُوْنَةٍۙ

'al*aa* sururin maw*dh*uuna**tin**

Mereka berada di atas dipan-dipan yang bertahtakan emas dan permata,

56:16

# مُّتَّكِـِٕيْنَ عَلَيْهَا مُتَقٰبِلِيْنَ

muttaki-iina 'alayh*aa* mutaq*aa*biliin**a**

mereka bersandar di atasnya berhadap-hadapan.

56:17

# يَطُوْفُ عَلَيْهِمْ وِلْدَانٌ مُّخَلَّدُوْنَۙ

ya*th*uufu 'alayhim wild*aa*nun mukhalladuun**a**

Mereka dikelilingi oleh anak-anak muda yang tetap muda,

56:18

# بِاَكْوَابٍ وَّاَبَارِيْقَۙ وَكَأْسٍ مِّنْ مَّعِيْنٍۙ

bi-akw*aa*bin wa-ab*aa*riiqa waka/sin min ma'iin**in**

dengan membawa gelas, cerek dan sloki (piala) berisi minuman yang diambil dari air yang mengalir,

56:19

# لَّا يُصَدَّعُوْنَ عَنْهَا وَلَا يُنْزِفُوْنَۙ

l*aa* yu*sh*adda'uuna 'anh*aa* wal*aa* yunzifuun**a**

mereka tidak pening karenanya dan tidak pula mabuk,

56:20

# وَفَاكِهَةٍ مِّمَّا يَتَخَيَّرُوْنَۙ

waf*aa*kihatin mimm*aa* yatakhayyaruun**a**

dan buah-buahan apa pun yang mereka pilih,

56:21

# وَلَحْمِ طَيْرٍ مِّمَّا يَشْتَهُوْنَۗ

wala*h*mi *th*ayrin mimm*aa* yasytahuun**a**

dan daging burung apa pun yang mereka inginkan.

56:22

# وَحُوْرٌ عِيْنٌۙ

wa*h*uurun 'iin**un**

Dan ada bidadari-bidadari yang bermata indah,

56:23

# كَاَمْثَالِ اللُّؤْلُؤِ الْمَكْنُوْنِۚ

ka-amts*aa*li **al**lu/lui **a**lmaknuun**i**

laksana mutiara yang tersimpan baik.

56:24

# جَزَاۤءًۢ بِمَا كَانُوْا يَعْمَلُوْنَ

jaz*aa*-an bim*aa* k*aa*nuu ya'maluun**a**

Sebagai balasan atas apa yang mereka kerjakan.

56:25

# لَا يَسْمَعُوْنَ فِيْهَا لَغْوًا وَّلَا تَأْثِيْمًاۙ

l*aa* yasma'uuna fiih*aa* laghwan wal*aa* ta/tsiim*aa***n**

Di sana mereka tidak mendengar percakapan yang sia-sia maupun yang menimbulkan dosa,

56:26

# اِلَّا قِيْلًا سَلٰمًا سَلٰمًا

ill*aa* qiilan sal*aa*man sal*aa*m*aa***n**

tetapi mereka mendengar ucapan salam.

56:27

# وَاَصْحٰبُ الْيَمِينِ ەۙ مَآ اَصْحٰبُ الْيَمِيْنِۗ

wa-a*sh*-*haa*bu **a**lyamiini m*aa* a*sh*-*haa*bu **a**lyamiin**i**

Dan golongan kanan, siapakah golongan kanan itu.

56:28

# فِيْ سِدْرٍ مَّخْضُوْدٍۙ

fii sidrin makh*dh*uud**in**

(Mereka) berada di antara pohon bidara yang tidak berduri,

56:29

# وَّطَلْحٍ مَّنْضُوْدٍۙ

wa*th*al*h*in man*dh*uud**in**

dan pohon pisang yang bersusun-susun (buahnya),

56:30

# وَّظِلٍّ مَّمْدُوْدٍۙ

wa*zh*illin mamduud**in**

dan naungan yang terbentang luas,

56:31

# وَّمَاۤءٍ مَّسْكُوْبٍۙ

wam*aa*-in maskuub**in**

dan air yang mengalir terus-menerus,

56:32

# وَّفَاكِهَةٍ كَثِيْرَةٍۙ

waf*aa*kihatin katsiira**tin**

dan buah-buahan yang banyak,

56:33

# لَّا مَقْطُوْعَةٍ وَّلَا مَمْنُوْعَةٍۙ

l*aa* maq*th*uu'atin wal*aa* mamnuu'a**tin**

yang tidak berhenti berbuah dan tidak terlarang mengambilnya,

56:34

# وَّفُرُشٍ مَّرْفُوْعَةٍۗ

wafurusyin marfuu'a**tin**

dan kasur-kasur yang tebal lagi empuk.

56:35

# اِنَّآ اَنْشَأْنٰهُنَّ اِنْشَاۤءًۙ

inn*aa* ansya/n*aa*hunna insy*aa***n**

Kami menciptakan mereka (bidadari-bidadari itu) secara langsung,

56:36

# فَجَعَلْنٰهُنَّ اَبْكَارًاۙ

faja'aln*aa*hunna abk*aa*r*aa***n**

lalu Kami jadikan mereka perawan-perawan,

56:37

# عُرُبًا اَتْرَابًاۙ

'uruban atr*aa*b*aa***n**

yang penuh cinta (dan) sebaya umurnya,

56:38

# لِّاَصْحٰبِ الْيَمِيْنِۗ ࣖ

li-a*sh*-*haa*bi **a**lyamiin**i**

untuk golongan kanan,

56:39

# ثُلَّةٌ مِّنَ الْاَوَّلِيْنَۙ

tsullatun mina **a**l-awwaliin**a**

segolongan besar dari orang-orang yang terdahulu,

56:40

# وَثُلَّةٌ مِّنَ الْاٰخِرِيْنَۗ

watsullatun mina **a**l-*aa*khiriin**a**

dan segolongan besar pula dari orang yang kemudian.

56:41

# وَاَصْحٰبُ الشِّمَالِ ەۙ مَآ اَصْحٰبُ الشِّمَالِۗ

wa-a*sh*-*haa*bu **al**sysyim*aa*li m*aa* a*sh*-*haa*bu **al**sysyim*aa*l**i**

Dan golongan kiri, alangkah sengsaranya golongan kiri itu.

56:42

# فِيْ سَمُوْمٍ وَّحَمِيْمٍۙ

fii samuumin wa*h*amiim**in**

(Mereka) dalam siksaan angin yang sangat panas dan air yang mendidih,

56:43

# وَّظِلٍّ مِّنْ يَّحْمُوْمٍۙ

wa*zh*illin min ya*h*muum**in**

dan naungan asap yang hitam,

56:44

# لَّا بَارِدٍ وَّلَا كَرِيْمٍ

l*aa* b*aa*ridin wal*aa* kariim**in**

tidak sejuk dan tidak menyenangkan.

56:45

# اِنَّهُمْ كَانُوْا قَبْلَ ذٰلِكَ مُتْرَفِيْنَۚ

innahum k*aa*nuu qabla *dzaa*lika mutrafiin**a**

Sesungguhnya mereka sebelum itu (dahulu) hidup bermewah-mewah,

56:46

# وَكَانُوْا يُصِرُّوْنَ عَلَى الْحِنْثِ الْعَظِيْمِۚ

wak*aa*nuu yu*sh*irruuna 'al*aa* **a**l*h*intsi **a**l'a*zh*iim**i**

dan mereka terus-menerus mengerjakan dosa yang besar,

56:47

# وَكَانُوْا يَقُوْلُوْنَ ەۙ اَىِٕذَا مِتْنَا وَكُنَّا تُرَابًا وَّعِظَامًا ءَاِنَّا لَمَبْعُوْثُوْنَۙ

wak*aa*nuu yaquuluuna a-i*dzaa* mitn*aa* wakunn*aa* tur*aa*ban wa'i*zhaa*man a-inn*aa* lamab'uutsuun**a**

dan mereka berkata, “Apabila kami sudah mati, menjadi tanah dan tulang-belulang, apakah kami benar-benar akan dibangkitkan kembali?

56:48

# اَوَاٰبَاۤؤُنَا الْاَوَّلُوْنَ

awa *aa*b*aa*un*aa* **a**l-awwaluun**a**

Apakah nenek moyang kami yang terdahulu (dibangkitkan pula)?”

56:49

# قُلْ اِنَّ الْاَوَّلِيْنَ وَالْاٰخِرِيْنَۙ

qul inna **a**l-awwaliina wa**a**l-*aa*khiriin**a**

Katakanlah, “(Ya), sesungguhnya orang-orang yang terdahulu dan yang kemudian,

56:50

# لَمَجْمُوْعُوْنَۙ اِلٰى مِيْقَاتِ يَوْمٍ مَّعْلُوْمٍ

lamajmuu'uuna il*aa* miiq*aa*ti yawmin ma'luum**in**

pasti semua akan dikumpulkan pada waktu tertentu, pada hari yang sudah dimaklumi.

56:51

# ثُمَّ اِنَّكُمْ اَيُّهَا الضَّاۤ لُّوْنَ الْمُكَذِّبُوْنَۙ

tsumma innakum ayyuh*aa* **al***dhdhaa*lluuna **a**lmuka*dzdz*ibuun**a**

Kemudian sesungguhnya kamu, wahai orang-orang yang sesat lagi mendustakan!

56:52

# لَاٰكِلُوْنَ مِنْ شَجَرٍ مِّنْ زَقُّوْمٍۙ

la*aa*kiluuna min syajarin min zaqquum**in**

pasti akan memakan pohon zaqqum,

56:53

# فَمَالِـُٔوْنَ مِنْهَا الْبُطُوْنَۚ

fam*aa*li-uuna minh*aa* **a**lbu*th*uun**a**

maka akan penuh perutmu dengannya.

56:54

# فَشَارِبُوْنَ عَلَيْهِ مِنَ الْحَمِيْمِۚ

fasy*aa*ribuuna 'alayhi mina **a**l*h*amiim**i**

Setelah itu kamu akan meminum air yang sangat panas.

56:55

# فَشَارِبُوْنَ شُرْبَ الْهِيْمِۗ

fasy*aa*ribuuna syurba **a**lhiim**i**

Maka kamu minum seperti unta (yang sangat haus) minum.

56:56

# هٰذَا نُزُلُهُمْ يَوْمَ الدِّيْنِۗ

h*aadzaa* nuzuluhum yawma **al**ddiin**i**

Itulah hidangan untuk mereka pada hari pembalasan.”

56:57

# نَحْنُ خَلَقْنٰكُمْ فَلَوْلَا تُصَدِّقُوْنَ

na*h*nu khalaqn*aa*kum falawl*aa* tu*sh*addiquun**a**

Kami telah menciptakan kamu, mengapa kamu tidak membenarkan (hari berbangkit)?

56:58

# اَفَرَءَيْتُمْ مَّا تُمْنُوْنَۗ

afara-aytum m*aa* tumnuun**a**

Maka adakah kamu perhatikan, tentang (benih manusia) yang kamu pancarkan.

56:59

# ءَاَنْتُمْ تَخْلُقُوْنَهٗٓ اَمْ نَحْنُ الْخَالِقُوْنَ

a-antum takhluquunahu am na*h*nu **a**lkh*aa*liquun**a**

Kamukah yang menciptakannya, ataukah Kami penciptanya?

56:60

# نَحْنُ قَدَّرْنَا بَيْنَكُمُ الْمَوْتَ وَمَا نَحْنُ بِمَسْبُوْقِيْنَۙ

na*h*nu qaddarn*aa* baynakumu **a**lmawta wam*aa* na*h*nu bimasbuuqiin**a**

Kami telah menentukan kematian masing-masing kamu dan Kami tidak lemah,

56:61

# عَلٰٓى اَنْ نُّبَدِّلَ اَمْثَالَكُمْ وَنُنْشِئَكُمْ فِيْ مَا لَا تَعْلَمُوْنَ

'al*aa* an nubaddila amts*aa*lakum wanunsyi-akum fii m*aa* l*aa* ta'lamuun**a**

untuk menggantikan kamu dengan orang-orang yang seperti kamu (di dunia) dan membangkitkan kamu kelak (di akhirat) dalam keadaan yang tidak kamu ketahui.

56:62

# وَلَقَدْ عَلِمْتُمُ النَّشْاَةَ الْاُوْلٰى فَلَوْلَا تَذَكَّرُوْنَ

walaqad 'alimtumu **al**nnasy-ata **a**l-uul*aa* falawl*aa* ta*dz*akkaruun**a**

Dan sungguh, kamu telah tahu penciptaan yang pertama, mengapa kamu tidak mengambil pelajaran?

56:63

# اَفَرَءَيْتُمْ مَّا تَحْرُثُوْنَۗ

afara-aytum m*aa* ta*h*rutsuun**a**

Pernahkah kamu perhatikan benih yang kamu tanam?

56:64

# ءَاَنْتُمْ تَزْرَعُوْنَهٗٓ اَمْ نَحْنُ الزَّارِعُوْنَ

a-antum tazra'uunahu am na*h*nu **al**zz*aa*ri'uun**a**

Kamukah yang menumbuhkannya ataukah Kami yang menumbuhkan?

56:65

# لَوْ نَشَاۤءُ لَجَعَلْنٰهُ حُطَامًا فَظَلْتُمْ تَفَكَّهُوْنَۙ

law nasy*aa*u laja'aln*aa*hu *h*u*thaa*man fa*zh*altum tafakkahuun**a**

Sekiranya Kami kehendaki, niscaya Kami hancurkan sampai lumat; maka kamu akan heran tercengang,

56:66

# اِنَّا لَمُغْرَمُوْنَۙ

inn*aa* lamughramuun**a**

(sambil berkata), “Sungguh, kami benar-benar menderita kerugian,

56:67

# بَلْ نَحْنُ مَحْرُوْمُوْنَ

bal na*h*nu ma*h*ruumuun**a**

bahkan kami tidak mendapat hasil apa pun.”

56:68

# اَفَرَءَيْتُمُ الْمَاۤءَ الَّذِيْ تَشْرَبُوْنَۗ

afara-aytumu **a**lm*aa*-a **al**la*dz*ii tasyrabuun**a**

Pernahkah kamu memperhatikan air yang kamu minum?

56:69

# ءَاَنْتُمْ اَنْزَلْتُمُوْهُ مِنَ الْمُزْنِ اَمْ نَحْنُ الْمُنْزِلُوْنَ

a-antum anzaltumuuhu mina **a**lmuzni am na*h*nu **a**lmunziluun**a**

Kamukah yang menurunkannya dari awan ataukah Kami yang menurunkan?

56:70

# لَوْ نَشَاۤءُ جَعَلْنٰهُ اُجَاجًا فَلَوْلَا تَشْكُرُوْنَ

law nasy*aa*u ja'aln*aa*hu uj*aa*jan falawl*aa* tasykuruun**a**

Sekiranya Kami menghendaki, niscaya Kami menjadikannya asin, mengapa kamu tidak bersyukur?

56:71

# اَفَرَءَيْتُمُ النَّارَ الَّتِيْ تُوْرُوْنَۗ

afara-aytumu **al**nn*aa*ra **al**latii tuuruun**a**

Maka pernahkah kamu memperhatikan tentang api yang kamu nyalakan (dengan kayu)?

56:72

# ءَاَنْتُمْ اَنْشَأْتُمْ شَجَرَتَهَآ اَمْ نَحْنُ الْمُنْشِـُٔوْنَ

a-antum ansya/tum syajaratah*aa* am na*h*nu **a**lmunsyi-uun**a**

Kamukah yang menumbuhkan kayu itu ataukah Kami yang menumbuhkan?

56:73

# نَحْنُ جَعَلْنٰهَا تَذْكِرَةً وَّمَتَاعًا لِّلْمُقْوِيْنَۚ

na*h*nu ja'aln*aa*h*aa* ta*dz*kiratan wamat*aa*'an lilmuqwiin**a**

Kami menjadikannya (api itu) untuk peringatan dan bahan yang berguna bagi musafir.

56:74

# فَسَبِّحْ بِاسْمِ رَبِّكَ الْعَظِيْمِ ࣖ

fasabbi*h* bi**i**smi rabbika **a**l'a*zh*iim**i**

Maka bertasbihlah dengan (menyebut) nama Tuhanmu Yang Mahabesar.

56:75

# فَلَآ اُقْسِمُ بِمَوٰقِعِ النُّجُوْمِ

fal*aa* uqsimu bimaw*aa*qi'i **al**nnujuum**i**

Lalu Aku bersumpah dengan tempat beredarnya bintang-bintang.

56:76

# وَاِنَّهٗ لَقَسَمٌ لَّوْ تَعْلَمُوْنَ عَظِيْمٌۙ

wa-innahu laqasamun law ta'lamuuna 'a*zh*iim**un**

Dan sesungguhnya itu benar-benar sumpah yang besar sekiranya kamu mengetahui,

56:77

# اِنَّهٗ لَقُرْاٰنٌ كَرِيْمٌۙ

innahu laqur-*aa*nun kariim**un**

dan (ini) sesungguhnya Al-Qur'an yang sangat mulia,

56:78

# فِيْ كِتٰبٍ مَّكْنُوْنٍۙ

fii kit*aa*bin maknuun**in**

dalam Kitab yang terpelihara (Lauh Mahfuzh),

56:79

# لَّا يَمَسُّهٗٓ اِلَّا الْمُطَهَّرُوْنَۙ

l*aa* yamassuhu ill*aa* **a**lmu*th*ahharuun**a**

tidak ada yang menyentuhnya selain hamba-hamba yang disucikan.

56:80

# تَنْزِيْلٌ مِّنْ رَّبِّ الْعٰلَمِيْنَ

tanziilun min rabbi **a**l'*aa*lamiin**a**

Diturunkan dari Tuhan seluruh alam.

56:81

# اَفَبِهٰذَا الْحَدِيْثِ اَنْتُمْ مُّدْهِنُوْنَ

afabih*aadzaa* **a**l*h*adiitsi antum mudhinuun**a**

Apakah kamu menganggap remeh berita ini (Al-Qur'an),

56:82

# وَتَجْعَلُوْنَ رِزْقَكُمْ اَنَّكُمْ تُكَذِّبُوْنَ

wataj'aluuna rizqakum annakum tuka*dzdz*ibuun**a**

dan kamu menjadikan rezeki yang kamu terima (dari Allah) justru untuk mendustakan(-Nya).

56:83

# فَلَوْلَآ اِذَا بَلَغَتِ الْحُلْقُوْمَۙ

falawl*aa* i*dzaa* balaghati **a**l*h*ulquum**a**

Maka kalau begitu mengapa (tidak mencegah) ketika (nyawa) telah sampai di kerongkongan,

56:84

# وَاَنْتُمْ حِيْنَىِٕذٍ تَنْظُرُوْنَۙ

wa-antum *h*iina-i*dz*in tan*zh*uruunaa

dan kamu ketika itu melihat,

56:85

# وَنَحْنُ اَقْرَبُ اِلَيْهِ مِنْكُمْ وَلٰكِنْ لَّا تُبْصِرُوْنَ

wana*h*nu aqrabu ilayhi minkum wal*aa*kin l*aa* tub*sh*iruun**a**

dan Kami lebih dekat kepadanya daripada kamu, tetapi kamu tidak melihat,

56:86

# فَلَوْلَآ اِنْ كُنْتُمْ غَيْرَ مَدِيْنِيْنَۙ

falawl*aa* in kuntum ghayra madiiniin**a**

maka mengapa jika kamu memang tidak dikuasai (oleh Allah),

56:87

# تَرْجِعُوْنَهَآ اِنْ كُنْتُمْ صٰدِقِيْنَ

tarji'uunah*aa* in kuntum *shaa*diqiin**a**

kamu tidak mengembalikannya (nyawa itu) jika kamu orang yang benar?

56:88

# فَاَمَّآ اِنْ كَانَ مِنَ الْمُقَرَّبِيْنَۙ

fa-amm*aa* in k*aa*na mina **a**lmuqarrabiin**a**

Jika dia (orang yang mati) itu termasuk yang didekatkan (kepada Allah),

56:89

# فَرَوْحٌ وَّرَيْحَانٌ ەۙ وَّجَنَّتُ نَعِيْمٍ

faraw*h*un waray*haa*nun wajannatu na'iim**in**

maka dia memperoleh ketenteraman dan rezeki serta surga (yang penuh) kenikmatan.

56:90

# وَاَمَّآ اِنْ كَانَ مِنْ اَصْحٰبِ الْيَمِيْنِۙ

wa-amm*aa* in k*aa*na min a*sh*-*haa*bi **a**lyamiin**i**

Dan adapun jika dia termasuk golongan kanan,

56:91

# فَسَلٰمٌ لَّكَ مِنْ اَصْحٰبِ الْيَمِيْنِۗ

fasal*aa*mun laka min a*sh*-*haa*bi **a**lyamiin**i**

maka, “Salam bagimu (wahai) dari golongan kanan!” (sambut malaikat).

56:92

# وَاَمَّآ اِنْ كَانَ مِنَ الْمُكَذِّبِيْنَ الضَّاۤلِّيْنَۙ

wa-amm*aa* in k*aa*na mina **a**lmuka*dzdz*ibiina **al***dhdhaa*lliin**a**

Dan adapun jika dia termasuk golongan orang yang mendustakan dan sesat,

56:93

# فَنُزُلٌ مِّنْ حَمِيْمٍۙ

fanuzulun min *h*amiim**in**

maka dia disambut siraman air yang mendidih,

56:94

# وَّتَصْلِيَةُ جَحِيْمٍ

wata*sh*liyatu ja*h*iim**in**

dan dibakar di dalam neraka.

56:95

# اِنَّ هٰذَا لَهُوَ حَقُّ الْيَقِيْنِۚ

inna h*aadzaa* lahuwa *h*aqqu **a**lyaqiin**i**

Sungguh, inilah keyakinan yang benar.

56:96

# فَسَبِّحْ بِاسْمِ رَبِّكَ الْعَظِيْمِ ࣖ

fasabbi*h* bi**i**smi rabbika **a**l'a*zh*iim**i**

Maka bertasbihlah dengan (menyebut) nama Tuhanmu Yang Mahabesar.

<!--EndFragment-->