---
title: (20) Taha - طٰهٰ
date: 2021-10-27T03:48:21.858Z
ayat: 20
description: "Jumlah Ayat: 135 / Arti: Taha"
---
<!--StartFragment-->

20:1

# طٰهٰ ۚ

*thaa*-h*aa*

Thaha

20:2

# مَآ اَنْزَلْنَا عَلَيْكَ الْقُرْاٰنَ لِتَشْقٰٓى ۙ

m*aa* anzaln*aa* 'alayka **a**lqur-*aa*na litasyq*aa*

Kami tidak menurunkan Al-Qur'an ini kepadamu (Muhammad) agar engkau menjadi susah;

20:3

# اِلَّا تَذْكِرَةً لِّمَنْ يَّخْشٰى ۙ

ill*aa* ta*dz*kiratan liman yakhsy*aa*

melainkan sebagai peringatan bagi orang yang takut (kepada Allah),

20:4

# تَنْزِيْلًا مِّمَّنْ خَلَقَ الْاَرْضَ وَالسَّمٰوٰتِ الْعُلٰى ۗ

tanziilan mimman khalaqa **a**l-ar*dh*a wa**al**ssam*aa*w*aa*ti **a**l'ul*aa*

diturunkan dari (Allah) yang menciptakan bumi dan langit yang tinggi,

20:5

# اَلرَّحْمٰنُ عَلَى الْعَرْشِ اسْتَوٰى

a**l**rra*h*m*aa*nu 'al*aa* **a**l'arsyi istaw*aa*

(yaitu) Yang Maha Pengasih, yang bersemayam di atas ‘Arsy.

20:6

# لَهٗ مَا فِى السَّمٰوٰتِ وَمَا فِى الْاَرْضِ وَمَا بَيْنَهُمَا وَمَا تَحْتَ الثَّرٰى

lahu m*aa* fii **al**ssam*aa*w*aa*ti wam*aa* fii **a**l-ar*dh*i wam*aa* baynahum*aa* wam*aa* ta*h*ta **al**tstsar*aa*

Milik-Nyalah apa yang ada di langit, apa yang ada di bumi, apa yang ada di antara keduanya, dan apa yang ada di bawah tanah.

20:7

# وَاِنْ تَجْهَرْ بِالْقَوْلِ فَاِنَّهٗ يَعْلَمُ السِّرَّ وَاَخْفٰى

wa-in tajhar bi**a**lqawli fa-innahu ya'lamu **al**ssirra wa-akhf*aa*

Dan jika engkau mengeraskan ucapanmu, sungguh, Dia mengetahui rahasia dan yang lebih tersembunyi.

20:8

# اَللّٰهُ لَآ اِلٰهَ اِلَّا هُوَۗ لَهُ الْاَسْمَاۤءُ الْحُسْنٰى

**al**l*aa*hu l*aa* il*aa*ha ill*aa* huwa lahu **a**l-asm*aa*u **a**l*h*usn*aa*

(Dialah) Allah, tidak ada tuhan selain Dia, yang mempunyai nama-nama yang terbaik.

20:9

# وَهَلْ اَتٰىكَ حَدِيْثُ مُوْسٰى ۘ

wahal at*aa*ka *h*adiitsu muus*aa*

Dan apakah telah sampai kepadamu kisah Musa?

20:10

# اِذْ رَاٰ نَارًا فَقَالَ لِاَهْلِهِ امْكُثُوْٓا اِنِّيْ اٰنَسْتُ نَارًا لَّعَلِّيْٓ اٰتِيْكُمْ مِّنْهَا بِقَبَسٍ اَوْ اَجِدُ عَلَى النَّارِ هُدًى

i*dz* ra*aa* n*aa*ran faq*aa*la li-ahlihi umkutsuu innii *aa*nastu n*aa*ran la'allii *aa*tiikum minh*aa* biqabasin aw ajidu 'al*aa* **al**nn*aa*ri hud*aa***n**

Ketika dia (Musa) melihat api, lalu dia berkata kepada keluarganya, “Tinggallah kamu (di sini), sesungguhnya aku melihat api, mudah-mudahan aku dapat membawa sedikit nyala api kepadamu atau aku akan mendapat petunjuk di tempat api itu.”

20:11

# فَلَمَّآ اَتٰىهَا نُوْدِيَ يٰمُوْسٰٓى ۙ

falamm*aa* at*aa*h*aa* nuudiya y*aa* muus*aa*

Maka ketika dia mendatanginya (ke tempat api itu) dia dipanggil, “Wahai Musa!

20:12

# اِنِّيْٓ اَنَا۠ رَبُّكَ فَاخْلَعْ نَعْلَيْكَۚ اِنَّكَ بِالْوَادِ الْمُقَدَّسِ طُوًى ۗ

innii an*aa* rabbuka fa**i**khla' na'layka innaka bi**a**lw*aa*di **a**lmuqaddasi *th*uw*aa***n**

Sungguh, Aku adalah Tuhanmu, maka lepaskan kedua terompahmu. Karena sesungguhnya engkau berada di lembah yang suci, Tuwa.

20:13

# وَاَنَا اخْتَرْتُكَ فَاسْتَمِعْ لِمَا يُوْحٰى

wa-an*aa* ikhtartuka fa**i**stami' lim*aa* yuu*haa*

Dan Aku telah memilih engkau, maka dengarkanlah apa yang akan diwahyukan (kepadamu).

20:14

# اِنَّنِيْٓ اَنَا اللّٰهُ لَآ اِلٰهَ اِلَّآ اَنَا۠ فَاعْبُدْنِيْۙ وَاَقِمِ الصَّلٰوةَ لِذِكْرِيْ

innanii an*aa* **al**l*aa*hu l*aa* il*aa*ha ill*aa* an*aa* fa**u**'budnii wa-aqimi **al***shsh*al*aa*ta li*dz*ikrii

Sungguh, Aku ini Allah, tidak ada tuhan selain Aku, maka sembahlah Aku dan laksanakanlah salat untuk mengingat Aku.

20:15

# اِنَّ السَّاعَةَ اٰتِيَةٌ اَكَادُ اُخْفِيْهَا لِتُجْزٰى كُلُّ نَفْسٍۢ بِمَا تَسْعٰى

inna **al**ss*aa*'ata *aa*tiyatun ak*aa*du ukhfiih*aa* litujz*aa* kullu nafsin bim*aa* tas'*aa*

Sungguh, hari Kiamat itu akan datang, Aku merahasiakan (waktunya) agar setiap orang dibalas sesuai dengan apa yang telah dia usahakan.

20:16

# فَلَا يَصُدَّنَّكَ عَنْهَا مَنْ لَّا يُؤْمِنُ بِهَا وَاتَّبَعَ هَوٰىهُ فَتَرْدٰى

fal*aa* ya*sh*uddannaka 'anh*aa* man l*aa* yu/minu bih*aa* wa**i**ttaba'a haw*aa*hu fatard*aa*

Maka janganlah engkau dipalingkan dari (Kiamat itu) oleh orang yang tidak beriman kepadanya dan oleh orang yang mengikuti keinginannya, yang menyebabkan engkau binasa.”

20:17

# وَمَا تِلْكَ بِيَمِيْنِكَ يٰمُوْسٰى

wam*aa* tilka biyamiinika y*aa* muus*aa*

”Dan apakah yang ada di tangan kananmu, wahai Musa? ”

20:18

# قَالَ هِيَ عَصَايَۚ اَتَوَكَّؤُا عَلَيْهَا وَاَهُشُّ بِهَا عَلٰى غَنَمِيْ وَلِيَ فِيْهَا مَاٰرِبُ اُخْرٰى

q*aa*la hiya 'a*shaa*ya atawakkau 'alayh*aa* wa-ahusysyu bih*aa* 'al*aa* ghanamii waliya fiih*aa* ma*aa*ribu ukhr*aa*

Dia (Musa) berkata, “Ini adalah tongkatku, aku bertumpu padanya, dan aku merontokkan (daun-daun) dengannya untuk (makanan) kambingku, dan bagiku masih ada lagi manfaat yang lain.”

20:19

# قَالَ اَلْقِهَا يٰمُوْسٰى

q*aa*la **a**lqih*aa* y*aa* muus*aa*

Dia (Allah) berfirman, “Lemparkanlah ia, wahai Musa!”

20:20

# فَاَلْقٰىهَا فَاِذَا هِيَ حَيَّةٌ تَسْعٰى

fa-alq*aa*h*aa* fa-i*dzaa* hiya *h*ayyatun tas'*aa*

Lalu (Musa) melemparkan tongkat itu, maka tiba-tiba ia menjadi seekor ular yang merayap dengan cepat.

20:21

# قَالَ خُذْهَا وَلَا تَخَفْۗ سَنُعِيْدُهَا سِيْرَتَهَا الْاُوْلٰى

q*aa*la khu*dz*h*aa* wal*aa* takhaf sanu'iiduh*aa* siiratah*aa* **a**l-uul*aa*

Dia (Allah) berfirman, “Peganglah ia dan jangan takut, Kami akan mengembalikannya kepada keadaannya semula,

20:22

# وَاضْمُمْ يَدَكَ اِلٰى جَنَاحِكَ تَخْرُجْ بَيْضَاۤءَ مِنْ غَيْرِ سُوْۤءٍ اٰيَةً اُخْرٰىۙ

wa**u***dh*mum yadaka il*aa* jan*aah*ika takhruj bay*dhaa*-a min ghayri suu-in *aa*yatan ukhr*aa*

dan kepitlah tanganmu ke ketiakmu, niscaya ia keluar menjadi putih (bercahaya) tanpa cacat, sebagai mukjizat yang lain,

20:23

# لِنُرِيَكَ مِنْ اٰيٰتِنَا الْكُبْرٰى ۚ

linuriyaka min *aa*y*aa*tin*aa* **a**lkubr*aa*

untuk Kami perlihatkan kepadamu (sebagian) dari tanda-tanda kebesaran Kami yang sangat besar,

20:24

# اِذْهَبْ اِلٰى فِرْعَوْنَ اِنَّهٗ طَغٰى ࣖ

i*dz*hab il*aa* fir'awna innahu *th*agh*aa*

Pergilah kepada Fir‘aun; dia benar-benar telah melampaui batas.”

20:25

# قَالَ رَبِّ اشْرَحْ لِيْ صَدْرِيْ ۙ

q*aa*la rabbi isyra*h* lii *sh*adrii

Dia (Musa) berkata, “Ya Tuhanku, lapangkanlah dadaku,

20:26

# وَيَسِّرْ لِيْٓ اَمْرِيْ ۙ

wayassir lii amrii

dan mudahkanlah untukku urusanku,

20:27

# وَاحْلُلْ عُقْدَةً مِّنْ لِّسَانِيْ ۙ

wa**u***h*lul 'uqdatan min lis*aa*nii

dan lepaskanlah kekakuan dari lidahku,

20:28

# يَفْقَهُوْا قَوْلِيْ ۖ

yafqahuu qawlii

agar mereka mengerti perkataanku,

20:29

# وَاجْعَلْ لِّيْ وَزِيْرًا مِّنْ اَهْلِيْ ۙ

yafqahuu qawlii

dan jadikanlah untukku seorang pembantu dari keluargaku,

20:30

# هٰرُوْنَ اَخِى ۙ

h*aa*ruuna akhii

(yaitu) Harun, saudaraku,

20:31

# اشْدُدْ بِهٖٓ اَزْرِيْ ۙ

usydud bihi azrii

teguhkanlah kekuatanku dengan (adanya) dia,

20:32

# وَاَشْرِكْهُ فِيْٓ اَمْرِيْ ۙ

wa-asyrik-hu fii amrii

dan jadikanlah dia teman dalam urusanku,

20:33

# كَيْ نُسَبِّحَكَ كَثِيْرًا ۙ

kay nusabbi*h*aka katsiir*aa***n**

agar kami banyak bertasbih kepada-Mu,

20:34

# وَّنَذْكُرَكَ كَثِيْرًا ۗ

wana*dz*kuraka katsiir*aa***n**

dan banyak mengingat-Mu,

20:35

# اِنَّكَ كُنْتَ بِنَا بَصِيْرًا

innaka kunta bin*aa* ba*sh*iir*aa***n**

sesungguhnya Engkau Maha Melihat (keadaan) kami.”

20:36

# قَالَ قَدْ اُوْتِيْتَ سُؤْلَكَ يٰمُوْسٰى

q*aa*la qad uutiita su/laka y*aa* muus*aa*

Dia (Allah) berfirman, “Sungguh, telah diperkenankan permintaanmu, wahai Musa!

20:37

# وَلَقَدْ مَنَنَّا عَلَيْكَ مَرَّةً اُخْرٰىٓ ۙ

walaqad manann*aa* 'alayka marratan ukhr*aa*

Dan sungguh, Kami telah memberi nikmat kepadamu pada kesempatan yang lain (sebelum ini),

20:38

# اِذْ اَوْحَيْنَآ اِلٰٓى اُمِّكَ مَا يُوْحٰىٓ ۙ

i*dz* aw*h*ayn*aa* il*aa* ummika m*aa* yuu*haa*

(yaitu) ketika Kami mengilhamkan kepada ibumu sesuatu yang diilhamkan,

20:39

# اَنِ اقْذِفِيْهِ فِى التَّابُوْتِ فَاقْذِفِيْهِ فِى الْيَمِّ فَلْيُلْقِهِ الْيَمُّ بِالسَّاحِلِ يَأْخُذْهُ عَدُوٌّ لِّيْ وَعَدُوٌّ لَّهٗ ۗوَاَلْقَيْتُ عَلَيْكَ مَحَبَّةً مِّنِّيْ ەۚ وَلِتُصْنَعَ عَلٰى عَيْنِيْ ۘ

ani iq*dz*ifiihi fii **al**tt*aa*buuti fa**i**q*dz*ifiihi fii **a**lyammi falyulqihi **a**lyammu bi**al**ss*aah*ili ya/khu*dz*hu 'aduwwun lii wa'aduwwun lahu wa-alqa

(yaitu), letakkanlah dia (Musa) di dalam peti, kemudian hanyutkanlah dia ke sungai (Nil), maka biarlah (arus) sungai itu membawanya ke tepi, dia akan diambil oleh (Fir‘aun) musuh-Ku dan musuhnya. Aku telah melimpahkan kepadamu kasih sayang yang datang dar

20:40

# اِذْ تَمْشِيْٓ اُخْتُكَ فَتَقُوْلُ هَلْ اَدُلُّكُمْ عَلٰى مَنْ يَّكْفُلُهٗ ۗفَرَجَعْنٰكَ اِلٰٓى اُمِّكَ كَيْ تَقَرَّ عَيْنُهَا وَلَا تَحْزَنَ ەۗ وَقَتَلْتَ نَفْسًا فَنَجَّيْنٰكَ مِنَ الْغَمِّ وَفَتَنّٰكَ فُتُوْنًا ەۗ فَلَبِثْتَ سِنِيْنَ فِيْٓ اَهْلِ مَدْي

i*dz* tamsyii ukhtuka fataquulu hal adullukum 'al*aa* man yakfuluhu faraja'n*aa*ka il*aa* ummika kay taqarra 'aynuh*aa* wal*aa* ta*h*zana waqatalta nafsan fanajjayn*aa*ka mina **a**lghammi wafatann*aa*

(Yaitu) ketika saudara perempuanmu berjalan, lalu dia berkata (kepada keluarga Fir‘aun), ‘Bolehkah saya menunjukkan kepadamu orang yang akan memeliharanya?’ Maka Kami mengembalikanmu kepada ibumu, agar senang hatinya dan tidak bersedih hati. Dan engkau pe







20:41

# وَاصْطَنَعْتُكَ لِنَفْسِيْۚ

wa**i***sth*ana'tuka linafsii

dan Aku telah memilihmu (menjadi rasul) untuk diri-Ku.

20:42

# اِذْهَبْ اَنْتَ وَاَخُوْكَ بِاٰيٰتِيْ وَلَا تَنِيَا فِيْ ذِكْرِيْۚ

i*dz*hab anta wa-akhuuka bi-*aa*y*aa*tii wal*aa* taniy*aa* fii *dz*ikrii

Pergilah engkau beserta saudaramu dengan membawa tanda-tanda (kekuasaan)-Ku, dan janganlah kamu berdua lalai mengingat-Ku;

20:43

# اِذْهَبَآ اِلٰى فِرْعَوْنَ اِنَّهٗ طَغٰىۚ

i*dz*hab*aa* il*aa* fir'awna innahu *th*agh*aa*

pergilah kamu berdua kepada Fir‘aun, karena dia benar-benar telah melampaui batas;

20:44

# فَقُوْلَا لَهٗ قَوْلًا لَّيِّنًا لَّعَلَّهٗ يَتَذَكَّرُ اَوْ يَخْشٰى

faquul*aa* lahu qawlan layyinan la'allahu yata*dz*akkaru aw yakhsy*aa*

maka berbicaralah kamu berdua kepadanya (Fir‘aun) dengan kata-kata yang lemah lembut, mudah-mudahan dia sadar atau takut.

20:45

# قَالَا رَبَّنَآ اِنَّنَا نَخَافُ اَنْ يَّفْرُطَ عَلَيْنَآ اَوْ اَنْ يَّطْغٰى

q*aa*l*aa* rabban*aa* innan*aa* nakh*aa*fu an yafru*th*a 'alayn*aa* aw an ya*th*gh*aa*

Keduanya berkata, “Ya Tuhan kami, sungguh, kami khawatir dia akan segera menyiksa kami atau akan bertambah melampaui batas,”

20:46

# قَالَ لَا تَخَافَآ اِنَّنِيْ مَعَكُمَآ اَسْمَعُ وَاَرٰى

q*aa*la l*aa* takh*aa*f*aa* innanii ma'akum*aa* asma'u wa-ar*aa*

Dia (Allah) berfirman, “Janganlah kamu berdua khawatir, sesungguhnya Aku bersama kamu berdua, Aku mendengar dan melihat.

20:47

# فَأْتِيٰهُ فَقُوْلَآ اِنَّا رَسُوْلَا رَبِّكَ فَاَرْسِلْ مَعَنَا بَنِيْٓ اِسْرَاۤءِيْلَ ەۙ وَلَا تُعَذِّبْهُمْۗ قَدْ جِئْنٰكَ بِاٰيَةٍ مِّنْ رَّبِّكَ ۗوَالسَّلٰمُ عَلٰى مَنِ اتَّبَعَ الْهُدٰى

fa/tiy*aa*hu faquul*aa* inn*aa* rasuul*aa* rabbika fa-arsil ma'an*aa* banii isr*aa*-iila wal*aa* tu'a*dzdz*ibhum qad ji/n*aa*ka bi-*aa*yatin min rabbika wa**al**ssal*aa*mu 'al*aa* ma

Maka pergilah kamu berdua kepadanya (Fir‘aun) dan katakanlah, “Sungguh, kami berdua adalah utusan Tuhanmu, maka lepaskanlah Bani Israil bersama kami dan janganlah engkau menyiksa mereka. Sungguh, kami datang kepadamu dengan membawa bukti (atas kerasulan k

20:48

# اِنَّا قَدْ اُوْحِيَ اِلَيْنَآ اَنَّ الْعَذَابَ عَلٰى مَنْ كَذَّبَ وَتَوَلّٰى

inn*aa* qad uu*h*iya ilayn*aa* anna **a**l'a*dzaa*ba 'al*aa* man ka*dzdz*aba watawall*aa*

Sungguh, telah diwahyukan kepada kami bahwa siksa itu (ditimpakan) pada siapa pun yang mendustakan (ajaran agama yang kami bawa) dan berpaling (tidak mempedulikannya).”

20:49

# قَالَ فَمَنْ رَّبُّكُمَا يٰمُوْسٰى

q*aa*la faman rabbukum*aa* y*aa* muus*aa*

Dia (Fir‘aun) berkata, “Siapakah Tuhanmu berdua, wahai Musa?”

20:50

# قَالَ رَبُّنَا الَّذِيْٓ اَعْطٰى كُلَّ شَيْءٍ خَلْقَهٗ ثُمَّ هَدٰى

q*aa*la rabbun*aa* **al**la*dz*ii a'*thaa* kulla syay-in khalqahu tsumma had*aa*

Dia (Musa) menjawab, “Tuhan kami ialah (Tuhan) yang telah memberikan bentuk kejadian kepada segala sesuatu, kemudian memberinya petunjuk.”

20:51

# قَالَ فَمَا بَالُ الْقُرُوْنِ الْاُوْلٰى

q*aa*la fam*aa* b*aa*lu **a**lquruuni **a**l-uul*aa*

Dia (Fir‘aun) berkata, “Jadi bagaimana keadaan umat-umat yang dahulu?”

20:52

# قَالَ عِلْمُهَا عِنْدَ رَبِّيْ فِيْ كِتٰبٍۚ لَا يَضِلُّ رَبِّيْ وَلَا يَنْسَىۖ

q*aa*la 'ilmuh*aa* 'inda rabbii fii kit*aa*bin l*aa* ya*dh*illu rabbii wal*aa* yans*aa*

Dia (Musa) menjawab, “Pengetahuan tentang itu ada pada Tuhanku, di dalam sebuah Kitab (Lauh Mahfuzh), Tuhanku tidak akan salah ataupun lupa;

20:53

# الَّذِيْ جَعَلَ لَكُمُ الْاَرْضَ مَهْدًا وَّسَلَكَ لَكُمْ فِيْهَا سُبُلًا وَّاَنْزَلَ مِنَ السَّمَاۤءِ مَاۤءًۗ فَاَخْرَجْنَا بِهٖٓ اَزْوَاجًا مِّنْ نَّبَاتٍ شَتّٰى

**al**la*dz*ii ja'ala lakumu **a**l-ar*dh*a mahdan wasalaka lakum fiih*aa* subulan wa-anzala mina **al**ssam*aa*-i m*aa*-an fa-akhrajn*aa* bihi azw*aa*jan min nab*aa*tin syatt<

(Tuhan) yang telah menjadikan bumi sebagai hamparan bagimu, dan menjadikan jalan-jalan di atasnya bagimu, dan yang menurunkan air (hujan) dari langit.” Kemudian Kami tumbuhkan dengannya (air hujan itu) berjenis-jenis aneka macam tumbuh-tumbuhan.

20:54

# كُلُوْا وَارْعَوْا اَنْعَامَكُمْ ۗاِنَّ فِيْ ذٰلِكَ لَاٰيٰتٍ لِّاُولِى النُّهٰى ࣖ

kuluu wa**i**r'aw an'*aa*makum inna fii *dzaa*lika la*aa*y*aa*tin li-ulii **al**nnuh*aa*

Makanlah dan gembalakanlah hewan-hewanmu. Sungguh, pada yang demikian itu, terdapat tanda-tanda (kebesaran Allah) bagi orang yang berakal.

20:55

# ۞ مِنْهَا خَلَقْنٰكُمْ وَفِيْهَا نُعِيْدُكُمْ وَمِنْهَا نُخْرِجُكُمْ تَارَةً اُخْرٰى

minh*aa* khalaqn*aa*kum wafiih*aa* nu'iidukum waminh*aa* nukhrijukum t*aa*ratan ukhr*aa*

Darinya (tanah) itulah Kami menciptakan kamu dan kepadanyalah Kami akan mengembalikan kamu dan dari sanalah Kami akan mengeluarkan kamu pada waktu yang lain.

20:56

# وَلَقَدْ اَرَيْنٰهُ اٰيٰتِنَا كُلَّهَا فَكَذَّبَ وَاَبٰى

walaqad arayn*aa*hu *aa*y*aa*tin*aa* kullah*aa* faka*dzdz*aba wa-ab*aa*

Dan sungguh, Kami telah memperlihatkan kepadanya (Fir‘aun) tanda-tanda (kebesaran) Kami semuanya, ternyata dia mendustakan dan enggan (menerima kebenaran).

20:57

# قَالَ اَجِئْتَنَا لِتُخْرِجَنَا مِنْ اَرْضِنَا بِسِحْرِكَ يٰمُوْسٰى

q*aa*la aji/tan*aa* litukhrijan*aa* min ar*dh*in*aa* bisi*h*rika y*aa* muus*aa*

Dia (Fir‘aun) berkata, “Apakah engkau datang kepada kami untuk mengusir kami dari negeri kami dengan sihirmu, wahai Musa?

20:58

# فَلَنَأْتِيَنَّكَ بِسِحْرٍ مِّثْلِهٖ فَاجْعَلْ بَيْنَنَا وَبَيْنَكَ مَوْعِدًا لَّا نُخْلِفُهٗ نَحْنُ وَلَآ اَنْتَ مَكَانًا سُوًى

falana/tiyannaka bisi*h*rin mitslihi fa**i**j'al baynan*aa* wabaynaka maw'idan l*aa* nukhlifuhu na*h*nu wal*aa* anta mak*aa*nan suw*aa***n**

Maka kami pun pasti akan mendatangkan sihir semacam itu kepadamu, maka buatlah suatu perjanjian untuk pertemuan antara kami dan engkau yang kami tidak akan menyalahinya dan tidak (pula) engkau, di suatu tempat yang terbuka.”

20:59

# قَالَ مَوْعِدُكُمْ يَوْمُ الزِّيْنَةِ وَاَنْ يُّحْشَرَ النَّاسُ ضُحًى

q*aa*la maw'idukum yawmu **al**zziinati wa-an yu*h*syara **al**nn*aa*su *dh*u*haa***n**

Dia (Musa) berkata, “(Perjanjian) waktu (untuk pertemuan kami dengan kamu itu) ialah pada hari raya dan hendaklah orang-orang dikumpulkan pada pagi hari (duha).”

20:60

# فَتَوَلّٰى فِرْعَوْنُ فَجَمَعَ كَيْدَهٗ ثُمَّ اَتٰى

fatawall*aa* fir'awnu fajama'a kaydahu tsumma at*aa*

Maka Fir‘aun meninggalkan (tempat itu), lalu mengatur tipu dayanya, kemudian dia datang kembali (pada hari yang ditentukan).

20:61

# قَالَ لَهُمْ مُّوْسٰى وَيْلَكُمْ لَا تَفْتَرُوْا عَلَى اللّٰهِ كَذِبًا فَيُسْحِتَكُمْ بِعَذَابٍۚ وَقَدْ خَابَ مَنِ افْتَرٰى

q*aa*la lahum muus*aa* waylakum l*aa* taftaruu 'al*aa* **al**l*aa*hi ka*dz*iban fayus*h*itakum bi'a*dzaa*bin waqad kh*aa*ba mani iftar*aa*

Musa berkata kepada mereka (para pesihir), “Celakalah kamu! Janganlah kamu mengada-adakan kedustaan terhadap Allah, nanti Dia membinasakan kamu dengan azab.” Dan sungguh rugi orang yang mengada-adakan kedustaan.

20:62

# فَتَنَازَعُوْٓا اَمْرَهُمْ بَيْنَهُمْ وَاَسَرُّوا النَّجْوٰى

fatan*aa*za'uu amrahum baynahum wa-asarruu **al**nnajw*aa*

Maka mereka berbantah-bantahan tentang urusan mereka dan mereka merahasiakan percakapan (mereka).

20:63

# قَالُوْٓا اِنْ هٰذٰنِ لَسٰحِرَانِ يُرِيْدَانِ اَنْ يُّخْرِجٰكُمْ مِّنْ اَرْضِكُمْ بِسِحْرِهِمَا وَيَذْهَبَا بِطَرِيْقَتِكُمُ الْمُثْلٰى

q*aa*luu in h*aadzaa*ni las*aah*ir*aa*ni yuriid*aa*ni an yukhrij*aa*kum min ar*dh*ikum bisi*h*rihim*aa* waya*dz*hab*aa* bi*th*ariiqatikumu **a**lmutsl*aa*

Mereka (para pesihir) berkata, “Sesungguhnya dua orang ini adalah pesihir yang hendak mengusirmu (Fir‘aun) dari negerimu dengan sihir mereka berdua, dan hendak melenyapkan adat kebiasaanmu yang utama.

20:64

# فَاَجْمِعُوْا كَيْدَكُمْ ثُمَّ ائْتُوْا صَفًّاۚ وَقَدْ اَفْلَحَ الْيَوْمَ مَنِ اسْتَعْلٰى

fa-ajmi'uu kaydakum tsumma i/tuu *sh*affan waqad afla*h*a **a**lyawma mani ista'l*aa*

Maka kumpulkanlah segala tipu daya (sihir) kamu, kemudian datanglah dengan berbaris, dan sungguh, beruntung orang yang menang pada hari ini.”

20:65

# قَالُوْا يٰمُوْسٰٓى اِمَّآ اَنْ تُلْقِيَ وَاِمَّآ اَنْ نَّكُوْنَ اَوَّلَ مَنْ اَلْقٰى

q*aa*luu y*aa* muus*aa* imm*aa* an tulqiya wa-imm*aa* an nakuuna awwala man **a**lq*aa*

Mereka berkata, “Wahai Musa! Apakah engkau yang melemparkan (dahulu) atau kami yang lebih dahulu melemparkan?”

20:66

# قَالَ بَلْ اَلْقُوْاۚ فَاِذَا حِبَالُهُمْ وَعِصِيُّهُمْ يُخَيَّلُ اِلَيْهِ مِنْ سِحْرِهِمْ اَنَّهَا تَسْعٰى

q*aa*la bal **a**lquu fa-i*dzaa* *h*ib*aa*luhum wa'i*sh*iyyuhum yukhayyalu ilayhi min si*h*rihim annah*aa* tas'*aa*

Dia (Musa) berkata, “Silakan kamu melemparkan!” Maka tiba-tiba tali-tali dan tongkat-tongkat mereka terbayang olehnya (Musa) seakan-akan ia merayap cepat, karena sihir mereka.

20:67

# فَاَوْجَسَ فِيْ نَفْسِهٖ خِيْفَةً مُّوْسٰى

fa-awjasa fii nafsihi khiifatan muus*aa*

Maka Musa merasa takut dalam hatinya.

20:68

# قُلْنَا لَا تَخَفْ اِنَّكَ اَنْتَ الْاَعْلٰى

quln*aa* l*aa* takhaf innaka anta **a**l-a'l*aa*

Kami berfirman, “Jangan takut! Sungguh, engkaulah yang unggul (menang).

20:69

# وَاَلْقِ مَا فِيْ يَمِيْنِكَ تَلْقَفْ مَا صَنَعُوْاۗ اِنَّمَا صَنَعُوْا كَيْدُ سٰحِرٍۗ وَلَا يُفْلِحُ السّٰحِرُ حَيْثُ اَتٰى

wa-alqi m*aa* fii yamiinika talqaf m*aa* *sh*ana'uu innam*aa* *sh*ana'uu kaydu s*aah*irin wal*aa* yufli*h*u **al**ss*aah*iru *h*aytsu at*aa*

Dan lemparkan apa yang ada di tangan kananmu, niscaya ia akan menelan apa yang mereka buat. Apa yang mereka buat itu hanyalah tipu daya pesihir (belaka). Dan tidak akan menang pesihir itu, dari mana pun ia datang.”

20:70

# فَاُلْقِيَ السَّحَرَةُ سُجَّدًا قَالُوْٓا اٰمَنَّا بِرَبِّ هٰرُوْنَ وَمُوْسٰى

faulqiya **al**ssa*h*aratu sujjadan q*aa*luu *aa*mann*aa* birabbi h*aa*ruuna wamuus*aa*

Lalu para pesihir itu merunduk bersujud, seraya berkata, “Kami telah percaya kepada Tuhannya Harun dan Musa.”

20:71

# قَالَ اٰمَنْتُمْ لَهٗ قَبْلَ اَنْ اٰذَنَ لَكُمْۗ اِنَّهٗ لَكَبِيْرُكُمُ الَّذِيْ عَلَّمَكُمُ السِّحْرَۚ فَلَاُقَطِّعَنَّ اَيْدِيَكُمْ وَاَرْجُلَكُمْ مِّنْ خِلَافٍ وَّلَاُصَلِّبَنَّكُمْ فِيْ جُذُوْعِ النَّخْلِۖ وَلَتَعْلَمُنَّ اَيُّنَآ اَشَدُّ عَذَابًا وّ

q*aa*la *aa*mantum lahu qabla an *aadz*ana lakum innahu lakabiirukumu **al**la*dz*ii 'allamakumu **al**ssi*h*ra falauqa*ththh*i'anna aydiyakum wa-arjulakum min khil*aa*fin walau*sh*allibann

Dia (Fir‘aun) berkata, “Apakah kamu telah beriman kepadanya (Musa) sebelum aku memberi izin kepadamu? Sesungguhnya dia itu pemimpinmu yang mengajarkan sihir kepadamu. Maka sungguh, akan kupotong tangan dan kakimu secara bersilang, dan sungguh, akan aku sa

20:72

# قَالُوْا لَنْ نُّؤْثِرَكَ عَلٰى مَا جَاۤءَنَا مِنَ الْبَيِّنٰتِ وَالَّذِيْ فَطَرَنَا فَاقْضِ مَآ اَنْتَ قَاضٍۗ اِنَّمَا تَقْضِيْ هٰذِهِ الْحَيٰوةَ الدُّنْيَا ۗ

q*aa*luu lan nu/tsiraka 'al*aa* m*aa* j*aa*-an*aa* mina **a**lbayyin*aa*ti wa**a**lla*dz*ii fa*th*aran*aa* fa**i**q*dh*i m*aa* anta q*aad*in innam*aa*

Mereka (para pesihir) berkata, “Kami tidak akan memilih (tunduk) kepadamu atas bukti-bukti nyata (mukjizat), yang telah datang kepada kami dan atas (Allah) yang telah menciptakan kami. Maka putuskanlah yang hendak engkau putuskan. Sesungguhnya engkau hany

20:73

# اِنَّآ اٰمَنَّا بِرَبِّنَا لِيَغْفِرَ لَنَا خَطٰيٰنَا وَمَآ اَكْرَهْتَنَا عَلَيْهِ مِنَ السِّحْرِۗ وَاللّٰهُ خَيْرٌ وَّاَبْقٰى

inn*aa* *aa*mann*aa* birabbin*aa* liyaghfira lan*aa* kha*thaa*y*aa*n*aa* wam*aa* akrahtan*aa* 'alayhi mina **al**ssi*h*ri wa**al**l*aa*hu khayrun wa-abq*aa*

Kami benar-benar telah beriman kepada Tuhan kami, agar Dia mengampuni kesalahan-kesalahan kami dan sihir yang telah engkau paksakan kepada kami. Dan Allah lebih baik (pahala-Nya) dan lebih kekal (azab-Nya).”

20:74

# اِنَّهٗ مَنْ يَّأْتِ رَبَّهٗ مُجْرِمًا فَاِنَّ لَهٗ جَهَنَّمَ ۗ لَا يَمُوْتُ فِيْهَا وَلَا يَحْيٰى

innahu man ya/ti rabbahu mujriman fa-inna lahu jahannama l*aa* yamuutu fiih*aa* wal*aa* ya*h*y*aa*

Sesungguhnya barang siapa datang kepada Tuhannya dalam keadaan berdosa, maka sungguh, baginya adalah neraka Jahanam. Dia tidak mati (terus merasakan azab) di dalamnya dan tidak (pula) hidup (tidak dapat bertobat).

20:75

# وَمَنْ يَّأْتِهٖ مُؤْمِنًا قَدْ عَمِلَ الصّٰلِحٰتِ فَاُولٰۤىِٕكَ لَهُمُ الدَّرَجٰتُ الْعُلٰى ۙ

waman ya/tihi mu/minan qad 'amila **al***shshaa*li*haa*ti faul*aa*-ika lahumu **al**ddaraj*aa*tu **a**l'ul*aa*

Tetapi barang siapa datang kepada-Nya dalam keadaan beriman, dan telah mengerjakan kebajikan, maka mereka itulah orang yang memperoleh derajat yang tinggi (mulia),

20:76

# جَنّٰتُ عَدْنٍ تَجْرِيْ مِنْ تَحْتِهَا الْاَنْهٰرُ خٰلِدِيْنَ فِيْهَا ۗوَذٰلِكَ جَزٰۤؤُا مَنْ تَزَكّٰى ࣖ

jann*aa*tu '*aa*dnin tajrii min ta*h*tih*aa* **a**l-anh*aa*ru kh*aa*lidiina fiih*aa* wa*dzaa*lika jaz*aa*u man tazakk*aa*

(yaitu) surga-surga ‘Adn, yang mengalir di bawahnya sungai-sungai, mereka kekal di dalamnya. Itulah balasan bagi orang yang menyucikan diri.

20:77

# وَلَقَدْ اَوْحَيْنَآ اِلٰى مُوْسٰٓى اَنْ اَسْرِ بِعِبَادِيْ فَاضْرِبْ لَهُمْ طَرِيْقًا فِى الْبَحْرِ يَبَسًاۙ لَّا تَخٰفُ دَرَكًا وَّلَا تَخْشٰى

walaqad aw*h*ayn*aa* il*aa* muus*aa* an asri bi'ib*aa*dii fa**i***dh*rib lahum *th*ariiqan fii **a**lba*h*ri yabasan l*aa* takh*aa*fu darakan wal*aa* takhsy*aa*

Dan sungguh, telah Kami wahyukan kepada Musa, “Pergilah bersama hamba-hamba-Ku (Bani Israil) pada malam hari, dan pukullah (buatlah) untuk mereka jalan yang kering di laut itu, (engkau) tidak perlu takut akan tersusul dan tidak perlu khawatir (akan tengge

20:78

# فَاَتْبَعَهُمْ فِرْعَوْنُ بِجُنُوْدِهٖ فَغَشِيَهُمْ مِّنَ الْيَمِّ مَا غَشِيَهُمْ ۗ

fa-atba'ahum fir'awnu bijunuudihi faghasyiyahum mina **a**lyammi m*aa* ghasyiyahum

Kemudian Fir‘aun dengan bala tentaranya mengejar mereka, tetapi mereka digulung ombak laut yang menenggelamkan mereka.

20:79

# وَاَضَلَّ فِرْعَوْنُ قَوْمَهٗ وَمَا هَدٰى

wa-a*dh*alla fir'awnu qawmahu wam*aa* had*aa*

Dan Fir‘aun telah menyesatkan kaumnya dan tidak memberi petunjuk.

20:80

# يٰبَنِيْٓ اِسْرَاۤءِيْلَ قَدْ اَنْجَيْنٰكُمْ مِّنْ عَدُوِّكُمْ وَوٰعَدْنٰكُمْ جَانِبَ الطُّوْرِ الْاَيْمَنَ وَنَزَّلْنَا عَلَيْكُمُ الْمَنَّ وَالسَّلْوٰى

y*aa* banii isr*aa*-iila qad anjayn*aa*kum min 'aduwwikum waw*aa*'adn*aa*kum j*aa*niba **al***ththh*uuri **a**l-aymana wanazzaln*aa* 'alaykumu **a**lmanna wa**al**

Wahai Bani Israil! Sungguh, Kami telah menyelamatkan kamu dari musuhmu, dan Kami telah mengadakan perjanjian dengan kamu (untuk bermunajat) di sebelah kanan gunung itu (gunung Sinai) dan Kami telah menurunkan kepada kamu mann dan salwa.

20:81

# كُلُوْا مِنْ طَيِّبٰتِ مَا رَزَقْنٰكُمْۙ وَلَا تَطْغَوْا فِيْهِ فَيَحِلَّ عَلَيْكُمْ غَضَبِيْۚ وَمَنْ يَّحْلِلْ عَلَيْهِ غَضَبِيْ فَقَدْ هَوٰى

kuluu min *th*ayyib*aa*ti m*aa* razaqn*aa*kum wal*aa* ta*th*ghaw fiihi faya*h*illa 'alaykum gha*dh*abii waman ya*h*lil 'alayhi gha*dh*abii faqad haw*aa*

Makanlah dari rezeki yang baik-baik yang telah Kami berikan kepadamu, dan janganlah melampaui batas, yang menyebabkan kemurkaan-Ku menimpamu. Barangsiapa ditimpa kemurkaan-Ku, maka sungguh, binasalah dia.

20:82

# وَاِنِّي لَغَفَّارٌ لِّمَنْ تَابَ وَاٰمَنَ وَعَمِلَ صَالِحًا ثُمَّ اهْتَدٰى

wa-innii laghaff*aa*run liman t*aa*ba wa*aa*mana wa'amila *shaa*li*h*an tsumma ihtad*aa*

Dan sungguh, Aku Maha Pengampun bagi yang bertobat, beriman dan berbuat kebajikan, kemudian tetap dalam petunjuk.

20:83

# ۞ وَمَآ اَعْجَلَكَ عَنْ قَوْمِكَ يٰمُوْسٰى

wam*aa* a'jalaka 'an qawmika y*aa* muus*aa*

“Dan mengapa engkau datang lebih cepat daripada kaummu, wahai Musa?”

20:84

# قَالَ هُمْ اُولَاۤءِ عَلٰٓى اَثَرِيْ وَعَجِلْتُ اِلَيْكَ رَبِّ لِتَرْضٰى

q*aa*la hum ul*aa*-i 'al*aa* atsarii wa'ajiltu ilayka rabbi litar*daa*

Dia (Musa) berkata, “Itu mereka sedang menyusul aku dan aku bersegera kepada-Mu, Ya Tuhanku, agar Engkau rida (kepadaku).”

20:85

# قَالَ فَاِنَّا قَدْ فَتَنَّا قَوْمَكَ مِنْۢ بَعْدِكَ وَاَضَلَّهُمُ السَّامِرِيُّ

q*aa*la fa-inn*aa* qad fatann*aa* qawmaka min ba'dika wa-a*dh*allahumu **al**ss*aa*miriy**yu**

Dia (Allah) berfirman, “Sungguh, Kami telah menguji kaummu setelah engkau tinggalkan, dan mereka telah disesatkan oleh Samiri.”

20:86

# فَرَجَعَ مُوْسٰٓى اِلٰى قَوْمِهٖ غَضْبَانَ اَسِفًا ەۚ قَالَ يٰقَوْمِ اَلَمْ يَعِدْكُمْ رَبُّكُمْ وَعْدًا حَسَنًا ەۗ اَفَطَالَ عَلَيْكُمُ الْعَهْدُ اَمْ اَرَدْتُّمْ اَنْ يَّحِلَّ عَلَيْكُمْ غَضَبٌ مِّنْ رَّبِّكُمْ فَاَخْلَفْتُمْ مَّوْعِدِيْ

faraja'a muus*aa* il*aa* qawmihi gha*dh*b*aa*na asifan q*aa*la y*aa* qawmi alam ya'idkum rabbukum wa'dan *h*asanan afa*thaa*la 'alaykumu al'ahdu am aradtum an ya*h*illa 'alaykum gha*dh*abun min rabb

Kemudian Musa kembali kepada kaumnya dengan marah dan bersedih hati. Dia (Musa) berkata, “Wahai kaumku! Bukankah Tuhanmu telah menjanjikan kepadamu suatu janji yang baik? Apakah terlalu lama masa perjanjian itu bagimu atau kamu menghendaki agar kemurkaan

20:87

# قَالُوْا مَآ اَخْلَفْنَا مَوْعِدَكَ بِمَلْكِنَا وَلٰكِنَّا حُمِّلْنَآ اَوْزَارًا مِّنْ زِيْنَةِ الْقَوْمِ فَقَذَفْنٰهَا فَكَذٰلِكَ اَلْقَى السَّامِرِيُّ ۙ

q*aa*luu m*aa* akhlafn*aa* maw'idaka bimalkin*aa* wal*aa*kinn*aa* *h*ummiln*aa* awz*aa*ran min ziinati **a**lqawmi faqa*dz*afn*aa*h*aa* faka*dzaa*lika **a**lq*aa<*

Mereka berkata, “Kami tidak melanggar perjanjianmu dengan kemauan kami sendiri, tetapi kami harus membawa beban berat dari perhiasan kaum (Fir‘aun) itu, kemudian kami melemparkannya (ke dalam api), dan demikian pula Samiri melemparkannya,







20:88

# فَاَخْرَجَ لَهُمْ عِجْلًا جَسَدًا لَّهٗ خُوَارٌ فَقَالُوْا هٰذَآ اِلٰهُكُمْ وَاِلٰهُ مُوْسٰى ەۙ فَنَسِيَ ۗ

fa-akhraja lahum 'ijlan jasadan lahu khuw*aa*run faq*aa*luu h*aadzaa* il*aa*hukum wa-il*aa*hu muus*aa* fanasiy**a**

kemudian (dari lubang api itu) dia (Samiri) mengeluarkan (patung) anak sapi yang bertubuh dan bersuara untuk mereka, maka mereka berkata, “Inilah Tuhanmu dan Tuhannya Musa, tetapi dia (Musa) telah lupa.”

20:89

# اَفَلَا يَرَوْنَ اَلَّا يَرْجِعُ اِلَيْهِمْ قَوْلًا ەۙ وَّلَا يَمْلِكُ لَهُمْ ضَرًّا وَّلَا نَفْعًا ࣖ

afal*aa* yarawna **al**l*aa* yarji'u ilayhim qawlan wal*aa* yamliku lahum *dh*arran wal*aa* naf'*aa***n**

Maka tidakkah mereka memperhatikan bahwa (patung anak sapi itu) tidak dapat memberi jawaban kepada mereka, dan tidak kuasa menolak mudarat mau-pun mendatangkan manfaat kepada mereka?

20:90

# وَلَقَدْ قَالَ لَهُمْ هٰرُوْنُ مِنْ قَبْلُ يٰقَوْمِ اِنَّمَا فُتِنْتُمْ بِهٖۚ وَاِنَّ رَبَّكُمُ الرَّحْمٰنُ فَاتَّبِعُوْنِيْ وَاَطِيْعُوْٓا اَمْرِيْ

walaqad q*aa*la lahum h*aa*ruunu min qablu y*aa* qawmi innam*aa* futintum bihi wa-inna rabbakumu **al**rra*h*m*aa*nu fa**i**ttabi'uunii wa-a*th*ii'uu amrii

Dan sungguh, sebelumnya Harun telah berkata kepada mereka, “Wahai kaumku! Sesungguhnya kamu hanya sekedar diberi cobaan (dengan patung anak sapi) itu dan sungguh, Tuhanmu ialah (Allah) Yang Maha Pengasih, maka ikutilah aku dan taatilah perintahku.”

20:91

# قَالُوْا لَنْ نَّبْرَحَ عَلَيْهِ عٰكِفِيْنَ حَتّٰى يَرْجِعَ اِلَيْنَا مُوْسٰى

q*aa*luu lan nabra*h*a 'alayhi '*aa*kifiina *h*att*aa* yarji'a ilayn*aa* muus*aa*

Mereka menjawab, “Kami tidak akan meninggalkannya (dan) tetap menyembahnya (patung anak sapi) sampai Musa kembali kepada kami.”

20:92

# قَالَ يٰهٰرُوْنُ مَا مَنَعَكَ اِذْ رَاَيْتَهُمْ ضَلُّوْٓا ۙ

q*aa*la y*aa* h*aa*ruunu m*aa* mana'aka i*dz* ra-aytahum *dh*alluu

Dia (Musa) berkata, “Wahai Harun! Apa yang menghalangimu ketika engkau melihat mereka telah sesat,

20:93

# اَلَّا تَتَّبِعَنِۗ اَفَعَصَيْتَ اَمْرِيْ

**al**l*aa* tattabi'ani afa'a*sh*ayta amrii

(sehingga) engkau tidak mengikuti aku? Apakah engkau telah (sengaja) melanggar perintahku?”

20:94

# قَالَ يَبْنَؤُمَّ لَا تَأْخُذْ بِلِحْيَتِيْ وَلَا بِرَأْسِيْۚ اِنِّيْ خَشِيْتُ اَنْ تَقُوْلَ فَرَّقْتَ بَيْنَ بَنِيْٓ اِسْرَاۤءِيْلَ وَلَمْ تَرْقُبْ قَوْلِيْ

q*aa*la yabnaumma l*aa* ta/khu*dz* bili*h*yatii wal*aa* bira/sii innii khasyiitu an taquula farraqta bayna banii isr*aa*-iila walam tarqub qawlii

Dia (Harun) menjawab, “Wahai putra ibuku! Janganlah engkau pegang janggutku dan jangan (pula) kepalaku. Aku sungguh khawatir engkau akan berkata (kepadaku), ‘Engkau telah memecah belah antara Bani Israil dan engkau tidak memelihara amanatku.’”

20:95

# قَالَ فَمَا خَطْبُكَ يٰسَامِرِيُّ

q*aa*la fam*aa* kha*th*buka y*aa* s*aa*miriy**yu**

Dia (Musa) berkata, “Apakah yang mendorongmu (berbuat demikian) wahai Samiri?”

20:96

# قَالَ بَصُرْتُ بِمَا لَمْ يَبْصُرُوْا بِهٖ فَقَبَضْتُ قَبْضَةً مِّنْ اَثَرِ الرَّسُوْلِ فَنَبَذْتُهَا وَكَذٰلِكَ سَوَّلَتْ لِيْ نَفْسِيْ

q*aa*la ba*sh*urtu bim*aa* lam yab*sh*uruu bihi faqaba*dh*tu qab*dh*atan min atsari **al**rrasuuli fanaba*dz*tuh*aa* waka*dzaa*lika sawwalat lii nafsii

Dia (Samiri) menjawab, “Aku mengetahui sesuatu yang tidak mereka ketahui, jadi aku ambil segenggam (tanah dari) jejak rasul lalu aku melemparkannya (ke dalam api itu), demikianlah nafsuku membujukku.”

20:97

# قَالَ فَاذْهَبْ فَاِنَّ لَكَ فِى الْحَيٰوةِ اَنْ تَقُوْلَ لَا مِسَاسَۖ وَاِنَّ لَكَ مَوْعِدًا لَّنْ تُخْلَفَهٗۚ وَانْظُرْ اِلٰٓى اِلٰهِكَ الَّذِيْ ظَلْتَ عَلَيْهِ عَاكِفًا ۗ لَنُحَرِّقَنَّهٗ ثُمَّ لَنَنْسِفَنَّهٗ فِى الْيَمِّ نَسْفًا

q*aa*la fa**i***dz*hab fa-inna laka fii **a**l*h*ay*aa*ti an taquula l*aa* mis*aa*sa wa-inna laka maw'idan lan tukhlafahu wa**u**n*zh*ur il*aa* il*aa*hika **al**

**Dia (Musa) berkata, “Pergilah kau! Maka sesungguhnya di dalam kehidupan (di dunia) engkau (hanya dapat) mengatakan, ‘Janganlah menyentuh (aku),’. Dan engkau pasti mendapat (hukuman) yang telah dijanjikan (di akhirat) yang tidak akan dapat engkau hindari,**









20:98

# اِنَّمَآ اِلٰهُكُمُ اللّٰهُ الَّذِيْ لَآ اِلٰهَ اِلَّا هُوَۗ وَسِعَ كُلَّ شَيْءٍ عِلْمًا

innam*aa* il*aa*hukumu **al**l*aa*hu **al**la*dz*ii l*aa* il*aa*ha ill*aa* huwa wasi'a kulla syay-in 'ilm*aa***n**

Sungguh, Tuhanmu hanyalah Allah, tidak ada tuhan selain Dia. Pengetahuan-Nya meliputi segala sesuatu.”

20:99

# كَذٰلِكَ نَقُصُّ عَلَيْكَ مِنْ اَنْۢبَاۤءِ مَا قَدْ سَبَقَۚ وَقَدْ اٰتَيْنٰكَ مِنْ لَّدُنَّا ذِكْرًا ۚ

ka*dzaa*lika naqu*shsh*u 'alayka min anb*aa*-i m*aa* qad sabaqa waqad *aa*tayn*aa*ka min ladunn*aa* tsikra**n**

Demikianlah Kami kisahkan kepadamu (Muhammad) sebagian kisah (umat) yang telah lalu, dan sungguh, telah Kami berikan kepadamu suatu peringatan (Al-Qur'an) dari sisi Kami.

20:100

# مَنْ اَعْرَضَ عَنْهُ فَاِنَّهٗ يَحْمِلُ يَوْمَ الْقِيٰمَةِ وِزْرًا

man a'ra*dh*a 'anhu fa-innahu ya*h*milu yawma **a**lqiy*aa*mati wizr*aa***n**

Barangsiapa berpaling darinya (Al-Qur'an), maka sesungguhnya dia akan memikul beban yang berat (dosa) pada hari Kiamat,

20:101

# خٰلِدِيْنَ فِيْهِ ۗوَسَاۤءَ لَهُمْ يَوْمَ الْقِيٰمَةِ حِمْلًاۙ

kh*aa*lidiina fiihi was*aa*-a lahum yawma **a**lqiy*aa*mati *h*iml*aa***n**

mereka kekal di dalam keadaan itu. Dan sungguh buruk beban dosa itu bagi mereka pada hari Kiamat,

20:102

# يَّوْمَ يُنْفَخُ فِى الصُّوْرِ وَنَحْشُرُ الْمُجْرِمِيْنَ يَوْمَىِٕذٍ زُرْقًا ۖ

yawma yunfakhu fii **al***shsh*uuri wana*h*syuru **a**lmujrimiina yawma-i*dz*in zurq*aa***n**

pada hari (Kiamat) sangkakala ditiup (yang kedua kali) dan pada hari itu Kami kumpulkan orang-orang yang berdosa dengan (wajah) biru muram,

20:103

# يَّتَخَافَتُوْنَ بَيْنَهُمْ اِنْ لَّبِثْتُمْ اِلَّا عَشْرًا

yatakh*aa*fatuuna baynahum in labitstum ill*aa* 'asyr*aa***n**

mereka saling berbisik satu sama lain, “Kamu tinggal (di dunia) tidak lebih dari sepuluh (hari).”

20:104

# نَحْنُ اَعْلَمُ بِمَا يَقُوْلُوْنَ اِذْ يَقُوْلُ اَمْثَلُهُمْ طَرِيْقَةً اِنْ لَّبِثْتُمْ اِلَّا يَوْمًا ࣖ

na*h*nu a'lamu bim*aa* yaquuluuna i*dz* yaquulu amtsaluhum *th*ariiqatan in labitstum ill*aa* yawm*aa***n**

Kami lebih mengetahui apa yang akan mereka katakan, ketika orang yang paling lurus jalannya mengatakan, “Kamu tinggal (di dunia), tidak lebih dari sehari saja.”

20:105

# وَيَسْـَٔلُوْنَكَ عَنِ الْجِبَالِ فَقُلْ يَنْسِفُهَا رَبِّيْ نَسْفًا ۙ

wayas-aluunaka 'ani **a**ljib*aa*li faqul yansifuh*aa* rabbii nasf*aa***n**

Dan mereka bertanya kepadamu (Muhammad) tentang gunung-gunung, maka katakanlah, “Tuhanku akan menghancurkannya (pada hari Kiamat) sehancur-hancurnya,

20:106

# فَيَذَرُهَا قَاعًا صَفْصَفًا ۙ

faya*dz*aruh*aa* q*aa*'an *sh*af*sh*af*aa***n**

kemudian Dia akan menjadikan (bekas gunung-gunung) itu rata sama sekali,

20:107

# لَّا تَرٰى فِيْهَا عِوَجًا وَّلَآ اَمْتًا ۗ

l*aa* tar*aa* fiih*aa* 'iwajan wal*aa* amt*aa***n**

(sehingga) kamu tidak akan melihat lagi ada tempat yang rendah dan yang tinggi di sana.”

20:108

# يَوْمَىِٕذٍ يَّتَّبِعُوْنَ الدَّاعِيَ لَا عِوَجَ لَهٗ ۚوَخَشَعَتِ الْاَصْوَاتُ لِلرَّحْمٰنِ فَلَا تَسْمَعُ اِلَّا هَمْسًا

yawma-i*dz*in yattabi'uuna **al**dd*aa*'iya l*aa* 'iwaja lahu wakhasya'ati **a**l-a*sh*w*aa*tu li**l**rra*h*m*aa*ni fal*aa* tasma'u ill*aa* hams*aa***n**

Pada hari itu mereka mengikuti (panggilan) penyeru (malaikat) tanpa berbelok-belok (membantah); dan semua suara tunduk merendah kepada Tuhan Yang Maha Pengasih, sehingga yang kamu dengar hanyalah bisik-bisik.

20:109

# يَوْمَىِٕذٍ لَّا تَنْفَعُ الشَّفَاعَةُ اِلَّا مَنْ اَذِنَ لَهُ الرَّحْمٰنُ وَرَضِيَ لَهٗ قَوْلًا

yawma-i*dz*in l*aa* tanfa'u **al**sysyaf*aa*'atu ill*aa* man a*dz*ina lahu **al**rra*h*m*aa*nu wara*dh*iya lahu qawl*aa***n**

Pada hari itu tidak berguna syafaat (pertolongan), kecuali dari orang yang telah diberi izin oleh Tuhan Yang Maha Pengasih, dan Dia ridai perkataannya.

20:110

# يَعْلَمُ مَا بَيْنَ اَيْدِيْهِمْ وَمَا خَلْفَهُمْ وَلَا يُحِيْطُوْنَ بِهٖ عِلْمًا

ya'lamu m*aa* bayna aydiihim wam*aa* khalfahum wal*aa* yu*h*ii*th*uuna bihi 'ilm*aa***n**

Dia (Allah) mengetahui apa yang di hadapan mereka (yang akan terjadi) dan apa yang di belakang mereka (yang telah terjadi), sedang ilmu mereka tidak dapat meliputi ilmu-Nya.

20:111

# ۞ وَعَنَتِ الْوُجُوْهُ لِلْحَيِّ الْقَيُّوْمِۗ وَقَدْ خَابَ مَنْ حَمَلَ ظُلْمًا

wa'anati **a**lwujuuhu lil*h*ayyi **a**lqayyuumi waqad kh*aa*ba man *h*amala *zh*ulm*aa***n**

Dan semua wajah tertunduk di hadapan (Allah) Yang Hidup dan Yang Berdiri Sendiri. Sungguh rugi orang yang melakukan kezaliman.

20:112

# وَمَنْ يَّعْمَلْ مِنَ الصّٰلِحٰتِ وَهُوَ مُؤْمِنٌ فَلَا يَخٰفُ ظُلْمًا وَّلَا هَضْمًا

waman ya'mal mina **al***shshaa*li*haa*ti wahuwa mu/minun fal*aa* yakh*aa*fu *zh*ulman wal*aa* ha*dh*m*aa***n**

Dan barang siapa mengerjakan kebajikan sedang dia (dalam keadaan) beriman, maka dia tidak khawatir akan perlakuan zalim (terhadapnya) dan tidak (pula khawatir) akan pengurangan haknya.

20:113

# وَكَذٰلِكَ اَنْزَلْنٰهُ قُرْاٰنًا عَرَبِيًّا وَّصَرَّفْنَا فِيْهِ مِنَ الْوَعِيْدِ لَعَلَّهُمْ يَتَّقُوْنَ اَوْ يُحْدِثُ لَهُمْ ذِكْرًا

waka*dzaa*lika anzaln*aa*hu qur-*aa*nan 'arabiyyan wa*sh*arrafn*aa* fiihi mina **a**lwa'iidi la'allahum yattaquuna aw yu*h*ditsu lahum *dz*ikr*aa***n**

Dan demikianlah Kami menurunkan Al-Qur'an dalam bahasa Arab, dan Kami telah menjelaskan berulang-ulang di dalamnya sebagian dari ancaman, agar mereka bertakwa, atau agar (Al-Qur'an) itu memberi pengajaran bagi mereka.

20:114

# فَتَعٰلَى اللّٰهُ الْمَلِكُ الْحَقُّۚ وَلَا تَعْجَلْ بِالْقُرْاٰنِ مِنْ قَبْلِ اَنْ يُّقْضٰٓى اِلَيْكَ وَحْيُهٗ ۖوَقُلْ رَّبِّ زِدْنِيْ عِلْمًا

fata'*aa*l*aa* **al**l*aa*hu **a**lmaliku **a**l*h*aqqu wal*aa* ta'jal bi**a**lqur-*aa*ni min qabli an yuq*daa* ilayka wa*h*yuhu waqul rabbi zidnii 'ilm*aa*

Maka Mahatinggi Allah, Raja yang sebenar-benarnya. Dan janganlah engkau (Muhammad) tergesa-gesa (membaca) Al-Qur'an sebelum selesai diwahyukan kepadamu, dan katakanlah, “Ya Tuhanku, tambahkanlah ilmu kepadaku. ”

20:115

# وَلَقَدْ عَهِدْنَآ اِلٰٓى اٰدَمَ مِنْ قَبْلُ فَنَسِيَ وَلَمْ نَجِدْ لَهٗ عَزْمًا ࣖ

walaqad 'ahidn*aa* il*aa* *aa*dama min qablu fanasiya walam najid lahu 'azm*aa***n**

Dan sungguh telah Kami pesankan kepada Adam dahulu, tetapi dia lupa, dan Kami tidak dapati kemauan yang kuat padanya.

20:116

# وَاِذْ قُلْنَا لِلْمَلٰۤىِٕكَةِ اسْجُدُوْا لِاٰدَمَ فَسَجَدُوْٓا اِلَّآ اِبْلِيْسَ اَبٰى ۗ

wa-i*dz* quln*aa* lilmal*aa*-ikati usjuduu li-*aa*dama fasajaduu ill*aa* ibliisa ab*aa*

Dan (ingatlah) ketika Kami berfirman kepada para malaikat, “Sujudlah kamu kepada Adam!” Lalu mereka pun sujud kecuali Iblis; dia menolak.

20:117

# فَقُلْنَا يٰٓاٰدَمُ اِنَّ هٰذَا عَدُوٌّ لَّكَ وَلِزَوْجِكَ فَلَا يُخْرِجَنَّكُمَا مِنَ الْجَنَّةِ فَتَشْقٰى

faquln*aa* y*aa* *aa*damu inna h*aadzaa* 'aduwwun laka walizawjika fal*aa* yukhrijannakum*aa* mina **a**ljannati fatasyq*aa*

Kemudian Kami berfirman, “Wahai Adam! Sungguh ini (Iblis) musuh bagimu dan bagi istrimu, maka sekali-kali jangan sampai dia mengeluarkan kamu berdua dari surga, nanti kamu celaka.

20:118

# اِنَّ لَكَ اَلَّا تَجُوْعَ فِيْهَا وَلَا تَعْرٰى ۙ

inna laka **al**l*aa* tajuu'a fiih*aa* wal*aa* ta'r*aa*

Sungguh, ada (jaminan) untukmu di sana, engkau tidak akan kelaparan dan tidak akan telanjang.

20:119

# وَاَنَّكَ لَا تَظْمَؤُا فِيْهَا وَلَا تَضْحٰى

wa-annaka l*aa* ta*zh*mau fiih*aa* wal*aa* ta*dhaa*

Dan sungguh, di sana engkau tidak akan merasa dahaga dan tidak akan ditimpa panas matahari.”

20:120

# فَوَسْوَسَ اِلَيْهِ الشَّيْطٰنُ قَالَ يٰٓاٰدَمُ هَلْ اَدُلُّكَ عَلٰى شَجَرَةِ الْخُلْدِ وَمُلْكٍ لَّا يَبْلٰى

fawaswasa ilayhi **al**sysyay*thaa*nu q*aa*la y*aa* *aa*damu hal adulluka 'al*aa* syajarati **a**lkhuldi wamulkin l*aa* yabl*aa*

Kemudian setan membisikkan (pikiran jahat) kepadanya, dengan berkata, “Wahai Adam! Maukah aku tunjukkan kepadamu pohon keabadian (khuldi) dan kerajaan yang tidak akan binasa?”

20:121

# فَاَكَلَا مِنْهَا فَبَدَتْ لَهُمَا سَوْاٰتُهُمَا وَطَفِقَا يَخْصِفٰنِ عَلَيْهِمَا مِنْ وَّرَقِ الْجَنَّةِۚ وَعَصٰٓى اٰدَمُ رَبَّهٗ فَغَوٰى ۖ

fa-akal*aa* minh*aa* fabadat lahum*aa* saw-*aa*tuhum*aa* wa*th*afiq*aa* yakh*sh*if*aa*ni 'alayhim*aa* min waraqi **a**ljannati wa'a*shaa* *aa*damu rabbahu faghaw*aa*

Lalu keduanya memakannya, lalu tampaklah oleh keduanya aurat mereka dan mulailah keduanya menutupinya dengan daun-daun (yang ada di) surga, dan telah durhakalah Adam kepada Tuhannya, dan sesatlah dia.

20:122

# ثُمَّ اجْتَبٰىهُ رَبُّهٗ فَتَابَ عَلَيْهِ وَهَدٰى

tsumma ijtab*aa*hu rabbuhu fat*aa*ba 'alayhi wahad*aa*

Kemudian Tuhannya memilih dia, maka Dia menerima tobatnya dan memberinya petunjuk.

20:123

# قَالَ اهْبِطَا مِنْهَا جَمِيعًاۢ بَعْضُكُمْ لِبَعْضٍ عَدُوٌّ ۚفَاِمَّا يَأْتِيَنَّكُمْ مِّنِّيْ هُدًى ەۙ فَمَنِ اتَّبَعَ هُدٰيَ فَلَا يَضِلُّ وَلَا يَشْقٰى

q*aa*la ihbi*thaa* minh*aa* jamii'an ba'*dh*ukum liba'*dh*in 'aduwwun fa-imm*aa* ya/tiyannakum minnii hudan famani ittaba'a hud*aa*ya fal*aa* ya*dh*illu wal*aa* yasyq*aa*

Dia (Allah) berfirman, “Turunlah kamu berdua dari surga bersama-sama, sebagian kamu menjadi musuh bagi sebagian yang lain. Jika datang kepadamu petunjuk dari-Ku, maka (ketahuilah) barang siapa mengikuti petunjuk-Ku, dia tidak akan sesat dan tidak akan cel

20:124

# وَمَنْ اَعْرَضَ عَنْ ذِكْرِيْ فَاِنَّ لَهٗ مَعِيْشَةً ضَنْكًا وَّنَحْشُرُهٗ يَوْمَ الْقِيٰمَةِ اَعْمٰى

waman a'ra*dh*a 'an *dz*ikrii fa-inna lahu ma'iisyatan *dh*ank*aa***n** wana*h*syuruhu yawma **a**lqiy*aa*mati a'm*aa***n**

Dan barang siapa berpaling dari peringatan-Ku, maka sungguh, dia akan menjalani kehidupan yang sempit, dan Kami akan mengumpulkannya pada hari Kiamat dalam keadaan buta.”

20:125

# قَالَ رَبِّ لِمَ حَشَرْتَنِيْٓ اَعْمٰى وَقَدْ كُنْتُ بَصِيْرًا

q*aa*la rabbi lima *h*asyartanii a'm*aa* waqad kuntu ba*sh*iir*aa***n**

Dia berkata, “Ya Tuhanku, mengapa Engkau kumpulkan aku dalam keadaan buta, padahal dahulu aku dapat melihat?”

20:126

# قَالَ كَذٰلِكَ اَتَتْكَ اٰيٰتُنَا فَنَسِيْتَهَاۚ وَكَذٰلِكَ الْيَوْمَ تُنْسٰى

q*aa*la ka*dzaa*lika atatka *aa*y*aa*tun*aa* fanasiitah*aa* waka*dzaa*lika **a**lyawma tuns*aa*

Dia (Allah) berfirman, “Demikianlah, dahulu telah datang kepadamu ayat-ayat Kami, dan kamu mengabaikannya, jadi begitu (pula) pada hari ini kamu diabaikan.”

20:127

# وَكَذٰلِكَ نَجْزِيْ مَنْ اَسْرَفَ وَلَمْ يُؤْمِنْۢ بِاٰيٰتِ رَبِّهٖۗ وَلَعَذَابُ الْاٰخِرَةِ اَشَدُّ وَاَبْقٰى

waka*dzaa*lika najzii man asrafa walam yu/min bi-*aa*y*aa*ti rabbihi wala'a*dzaa*bu **a**l-*aa*khirati asyaddu wa-abq*aa*

Dan demikianlah Kami membalas orang yang melampaui batas dan tidak percaya kepada ayat-ayat Tuhannya. Sungguh, azab di akhirat itu lebih berat dan lebih kekal.

20:128

# اَفَلَمْ يَهْدِ لَهُمْ كَمْ اَهْلَكْنَا قَبْلَهُمْ مِّنَ الْقُرُوْنِ يَمْشُوْنَ فِيْ مَسٰكِنِهِمْۗ اِنَّ فِيْ ذٰلِكَ لَاٰيٰتٍ لِّاُولِى النُّهٰى ࣖ

afalam yahdi lahum kam ahlakn*aa* qablahum mina **a**lquruuni yamsyuuna fii mas*aa*kinihim inna fii *dzaa*lika la*aa*y*aa*tin li-ulii **al**nnuh*aa*

Maka tidakkah menjadi petunjuk bagi mereka (orang-orang musyrik) berapa banyak (generasi) sebelum mereka yang telah Kami binasakan, padahal mereka melewati (bekas-bekas) tempat tinggal mereka (umat-umat itu)? Sungguh, pada yang demikian itu terdapat tanda

20:129

# وَلَوْلَا كَلِمَةٌ سَبَقَتْ مِنْ رَّبِّكَ لَكَانَ لِزَامًا وَّاَجَلٌ مُّسَمًّى ۗ

walawl*aa* kalimatun sabaqat min rabbika lak*aa*na liz*aa*man wa-ajalun musamm*aa***n**

Dan kalau tidak ada suatu ketetapan terdahulu dari Tuhanmu serta tidak ada batas yang telah ditentukan (ajal), pasti (siksaan itu) menimpa mereka.

20:130

# فَاصْبِرْ عَلٰى مَا يَقُوْلُوْنَ وَسَبِّحْ بِحَمْدِ رَبِّكَ قَبْلَ طُلُوْعِ الشَّمْسِ وَقَبْلَ غُرُوْبِهَا ۚوَمِنْ اٰنَاۤئِ الَّيْلِ فَسَبِّحْ وَاَطْرَافَ النَّهَارِ لَعَلَّكَ تَرْضٰى

fa**i***sh*bir 'al*aa* m*aa* yaquuluuna wasabbi*h* bi*h*amdi rabbika qabla *th*uluu'i **al**sysyamsi waqabla ghuruubih*aa* wamin *aa*n*aa*-i **al**layli fasabbi*h* wa

Maka sabarlah engkau (Muhammad) atas apa yang mereka katakan, dan bertasbihlah dengan memuji Tuhanmu, sebelum matahari terbit, dan sebelum terbenam; dan bertasbihlah (pula) pada waktu tengah malam dan di ujung siang hari, agar engkau merasa tenang.

20:131

# وَلَا تَمُدَّنَّ عَيْنَيْكَ اِلٰى مَا مَتَّعْنَا بِهٖٓ اَزْوَاجًا مِّنْهُمْ زَهْرَةَ الْحَيٰوةِ الدُّنْيَا ەۙ لِنَفْتِنَهُمْ فِيْهِ ۗوَرِزْقُ رَبِّكَ خَيْرٌ وَّاَبْقٰى

wal*aa* tamuddanna 'aynayka il*aa* m*aa* matta'n*aa* bihi azw*aa*jan minhum zahrata **a**l*h*ay*aa*ti **al**dduny*aa* linaftinahum fiihi warizqu rabbika khayrun wa-abq*aa*

Dan janganlah engkau tujukan pandangan matamu kepada kenikmatan yang telah Kami berikan kepada beberapa golongan dari mereka, (sebagai) bunga kehidupan dunia agar Kami uji mereka dengan (kesenangan) itu. Karunia Tuhanmu lebih baik dan lebih kekal.

20:132

# وَأْمُرْ اَهْلَكَ بِالصَّلٰوةِ وَاصْطَبِرْ عَلَيْهَاۗ لَا نَسْـَٔلُكَ رِزْقًاۗ نَحْنُ نَرْزُقُكَۗ وَالْعَاقِبَةُ لِلتَّقْوٰى

wa/mur ahlaka bi**al***shsh*al*aa*ti wa**i***sth*abir 'alayh*aa* l*aa* nas-aluka rizqan na*h*nu narzuquka wa**a**l'*aa*qibatu li**l**ttaqw*aa*

Dan perintahkanlah keluargamu melaksanakan salat dan sabar dalam mengerjakannya. Kami tidak meminta rezeki kepadamu, Kamilah yang memberi rezeki kepadamu. Dan akibat (yang baik di akhirat) adalah bagi orang yang bertakwa.

20:133

# وَقَالُوْا لَوْلَا يَأْتِيْنَا بِاٰيَةٍ مِّنْ رَّبِّهٖۗ اَوَلَمْ تَأْتِهِمْ بَيِّنَةُ مَا فِى الصُّحُفِ الْاُولٰى

waq*aa*luu lawl*aa* ya/tiin*aa* bi-*aa*yatin min rabbihi awa lam ta/tihim bayyinatu m*aa* fii **al***shsh*u*h*ufi **a**l-uul*aa*

Dan mereka berkata, “Mengapa dia tidak membawa tanda (bukti) kepada kami dari Tuhannya?” Bukankah telah datang kepada mereka bukti (yang nyata) sebagaimana yang tersebut di dalam kitab-kitab yang dahulu?

20:134

# وَلَوْ اَنَّآ اَهْلَكْنٰهُمْ بِعَذَابٍ مِّنْ قَبْلِهٖ لَقَالُوْا رَبَّنَا لَوْلَآ اَرْسَلْتَ اِلَيْنَا رَسُوْلًا فَنَتَّبِعَ اٰيٰتِكَ مِنْ قَبْلِ اَنْ نَّذِلَّ وَنَخْزٰى

walaw ann*aa* ahlakn*aa*hum bi'a*dzaa*bin min qablihi laq*aa*luu rabban*aa* lawl*aa* arsalta ilayn*aa* rasuulan fanattabi'a *aa*y*aa*tika min qabli an na*dz*illa wanakhz*aa*

Dan kalau mereka Kami binasakan dengan suatu siksaan sebelumnya (Al-Qur'an itu diturunkan), tentulah mereka berkata, “Ya Tuhan kami, mengapa tidak Engkau utus seorang rasul kepada kami, sehingga kami mengikuti ayat-ayat-Mu sebelum kami menjadi hina dan re

20:135

# قُلْ كُلٌّ مُّتَرَبِّصٌ فَتَرَبَّصُوْاۚ فَسَتَعْلَمُوْنَ مَنْ اَصْحٰبُ الصِّرَاطِ السَّوِيِّ وَمَنِ اهْتَدٰى ࣖ ۔

qul kullun mutarabbi*sh*un fatarabba*sh*uu fasata'lamuuna man a*sh*-*haa*bu **al***shsh*ir*aath*i **al**ssawiyyi wamani ihtad*aa*

Katakanlah (Muhammad), “Masing-masing (kita) menanti, maka nantikanlah olehmu! Dan kelak kamu akan mengetahui, siapa yang menempuh jalan yang lurus, dan siapa yang telah mendapat petunjuk.”

<!--EndFragment-->