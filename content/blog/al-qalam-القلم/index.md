---
title: (68) Al-Qalam - القلم
date: 2021-10-27T04:05:20.805Z
ayat: 68
description: "Jumlah Ayat: 52 / Arti: Pena"
---
<!--StartFragment-->

68:1

# نۤ ۚوَالْقَلَمِ وَمَا يَسْطُرُوْنَۙ

nuun wa**a**lqalami wam*aa* yas*th*uruun**a**

Nun. Demi pena dan apa yang mereka tuliskan,

68:2

# مَآ اَنْتَ بِنِعْمَةِ رَبِّكَ بِمَجْنُوْنٍ

m*aa* anta bini'mati rabbika bimajnuun**in**

dengan karunia Tuhanmu engkau (Muhammad) bukanlah orang gila.

68:3

# وَاِنَّ لَكَ لَاَجْرًا غَيْرَ مَمْنُوْنٍۚ

wa-inna laka la-ajran ghayra mamnuun**in**

Dan sesungguhnya engkau pasti mendapat pahala yang besar yang tidak putus-putusnya.

68:4

# وَاِنَّكَ لَعَلٰى خُلُقٍ عَظِيْمٍ

wa-innaka la'al*aa* khuluqin 'a*zh*iim**in**

Dan sesungguhnya engkau benar-benar berbudi pekerti yang luhur.

68:5

# فَسَتُبْصِرُ وَيُبْصِرُوْنَۙ

fasatub*sh*iru wayub*sh*iruun**a**

Maka kelak engkau akan melihat dan mereka (orang-orang kafir) pun akan melihat,

68:6

# بِاَيِّىكُمُ الْمَفْتُوْنُ

bi-ayyikumu **a**lmaftuun**u**

siapa di antara kamu yang gila?

68:7

# اِنَّ رَبَّكَ هُوَ اَعْلَمُ بِمَنْ ضَلَّ عَنْ سَبِيْلِهٖۖ وَهُوَ اَعْلَمُ بِالْمُهْتَدِيْنَ

inna rabbaka huwa a'lamu biman *dh*alla 'an sabiilihi wahuwa a'lamu bi**a**lmuhtadiin**a**

Sungguh, Tuhanmu, Dialah yang paling mengetahui siapa yang sesat dari jalan-Nya; dan Dialah yang paling mengetahui siapa orang yang mendapat petunjuk.

68:8

# فَلَا تُطِعِ الْمُكَذِّبِيْنَ

fal*aa* tu*th*i'i **a**lmuka*dzdz*ibiin**a**

Maka janganlah engkau patuhi orang-orang yang mendustakan (ayat-ayat Allah).

68:9

# وَدُّوْا لَوْ تُدْهِنُ فَيُدْهِنُوْنَۚ

wadduu law tudhinu fayudhinuun**a**

Mereka menginginkan agar engkau bersikap lunak maka mereka bersikap lunak (pula).

68:10

# وَلَا تُطِعْ كُلَّ حَلَّافٍ مَّهِيْنٍۙ

wal*aa* tu*th*i' kulla *h*all*aa*fin mahiin**in**

Dan janganlah engkau patuhi setiap orang yang suka bersumpah dan suka menghina,

68:11

# هَمَّازٍ مَّشَّاۤءٍۢ بِنَمِيْمٍۙ

hamm*aa*zin masysy*aa*-in binamiim**in**

suka mencela, yang kian ke mari menyebarkan fitnah,

68:12

# مَّنَّاعٍ لِّلْخَيْرِ مُعْتَدٍ اَثِيْمٍۙ

mann*aa*'in lilkhayri mu'tadin atsiim**in**

yang merintangi segala yang baik, yang melampaui batas dan banyak dosa,

68:13

# عُتُلٍّۢ بَعْدَ ذٰلِكَ زَنِيْمٍۙ

'utullin ba'da *dzaa*lika zaniim**in**

yang bertabiat kasar, selain itu juga terkenal kejahatannya,

68:14

# اَنْ كَانَ ذَا مَالٍ وَّبَنِيْنَۗ

an k*aa*na *dzaa* m*aa*lin wabaniin**a**

karena dia kaya dan banyak anak.

68:15

# اِذَا تُتْلٰى عَلَيْهِ اٰيٰتُنَا قَالَ اَسَاطِيْرُ الْاَوَّلِيْنَۗ

i*dzaa* tutl*aa* 'alayhi *aa*y*aa*tun*aa* q*aa*la as*aath*iiru **a**l-awwaliin**a**

Apabila ayat-ayat Kami dibacakan kepadanya, dia berkata, “(Ini adalah) dongeng-dongeng orang dahulu.”

68:16

# سَنَسِمُهٗ عَلَى الْخُرْطُوْمِ

sanasimuhu 'al*aa* **a**lkhur*th*uum**i**

Kelak dia akan Kami beri tanda pada belalai(nya).

68:17

# اِنَّا بَلَوْنٰهُمْ كَمَا بَلَوْنَآ اَصْحٰبَ الْجَنَّةِۚ اِذْ اَقْسَمُوْا لَيَصْرِمُنَّهَا مُصْبِحِيْنَۙ

inn*aa* balawn*aa*hum kam*aa* balawn*aa* a*sh*-*haa*ba **a**ljannati i*dz* aqsamuu laya*sh*rimunnah*aa* mu*sh*bi*h*iin**a**

Sungguh, Kami telah menguji mereka (orang musyrik Mekah) sebagaimana Kami telah menguji pemilik-pemilik kebun, ketika mereka bersumpah pasti akan memetik (hasil)nya pada pagi hari,

68:18

# وَلَا يَسْتَثْنُوْنَ

wal*aa* yastatsnuun**a**

tetapi mereka tidak menyisihkan (dengan mengucapkan, “Insya Allah”).

68:19

# فَطَافَ عَلَيْهَا طَاۤىِٕفٌ مِّنْ رَّبِّكَ وَهُمْ نَاۤىِٕمُوْنَ

fa*thaa*fa 'alayh*aa* *thaa*-ifun min rabbika wahum n*aa*-imuun**a**

Lalu kebun itu ditimpa bencana (yang datang) dari Tuhanmu ketika mereka sedang tidur.

68:20

# فَاَصْبَحَتْ كَالصَّرِيْمِۙ

fa-a*sh*ba*h*at ka**al***shsh*ariim**i**

Maka jadilah kebun itu hitam seperti malam yang gelap gulita,

68:21

# فَتَنَادَوْا مُصْبِحِيْنَۙ

fatan*aa*daw mu*sh*bi*h*iin**a**

lalu pada pagi hari mereka saling memanggil.

68:22

# اَنِ اغْدُوْا عَلٰى حَرْثِكُمْ اِنْ كُنْتُمْ صَارِمِيْنَ

ani ighduu 'al*aa* *h*artsikum in kuntum *shaa*rimiin**a**

”Pergilah pagi-pagi ke kebunmu jika kamu hendak memetik hasil.”

68:23

# فَانْطَلَقُوْا وَهُمْ يَتَخَافَتُوْنَۙ

fa**i**n*th*alaquu wahum yatakh*aa*fatuun**a**

Maka mereka pun berangkat sambil berbisik-bisik.

68:24

# اَنْ لَّا يَدْخُلَنَّهَا الْيَوْمَ عَلَيْكُمْ مِّسْكِيْنٌۙ

an l*aa* yadkhulannah*aa* **a**lyawma 'alaykum miskiin**un**

”Pada hari ini jangan sampai ada orang miskin masuk ke dalam kebunmu.”

68:25

# وَّغَدَوْا عَلٰى حَرْدٍ قَادِرِيْنَ

waghadaw 'al*aa* *h*ardin q*aa*diriin**a**

Dan berangkatlah mereka pada pagi hari dengan niat menghalangi (orang-orang miskin) padahal mereka mampu (menolongnya).

68:26

# فَلَمَّا رَاَوْهَا قَالُوْٓا اِنَّا لَضَاۤلُّوْنَۙ

falamm*aa* ra-awh*aa* q*aa*luu inn*aa* la*daa*lluun**a**

Maka ketika mereka melihat kebun itu, mereka berkata, “Sungguh, kita ini benar-benar orang-orang yang sesat,

68:27

# بَلْ نَحْنُ مَحْرُوْمُوْنَ

bal na*h*nu ma*h*ruumuun**a**

bahkan kita tidak memperoleh apa pun,”

68:28

# قَالَ اَوْسَطُهُمْ اَلَمْ اَقُلْ لَّكُمْ لَوْلَا تُسَبِّحُوْنَ

q*aa*la awsa*th*uhum alam aqul lakum lawl*aa* tusabbi*h*uun**a**

berkatalah seorang yang paling bijak di antara mereka, “Bukankah aku telah mengatakan kepadamu, mengapa kamu tidak bertasbih (kepada Tuhanmu).”

68:29

# قَالُوْا سُبْحٰنَ رَبِّنَآ اِنَّا كُنَّا ظٰلِمِيْنَ

q*aa*luu sub*haa*na rabbin*aa* inn*aa* kunn*aa* *zhaa*limiin**a**

Mereka mengucapkan, “Mahasuci Tuhan kami, sungguh, kami adalah orang-orang yang zalim.”

68:30

# فَاَقْبَلَ بَعْضُهُمْ عَلٰى بَعْضٍ يَّتَلَاوَمُوْنَ

fa-aqbala ba'*dh*uhum 'al*aa* ba'*dh*in yatal*aa*wamuun**a**

Lalu mereka saling berhadapan dan saling menyalahkan.

68:31

# قَالُوْا يٰوَيْلَنَآ اِنَّا كُنَّا طٰغِيْنَ

q*aa*luu y*aa* waylan*aa* inn*aa* kunn*aa* *thaa*ghiin**a**

Mereka berkata, “Celaka kita! Sesungguhnya kita orang-orang yang melampaui batas.

68:32

# عَسٰى رَبُّنَآ اَنْ يُّبْدِلَنَا خَيْرًا مِّنْهَآ اِنَّآ اِلٰى رَبِّنَا رَاغِبُوْنَ

'as*aa* rabbun*aa* an yubdilan*aa* khayran minh*aa* inn*aa* il*aa* rabbin*aa* r*aa*ghibuun**a**

Mudah-mudahan Tuhan memberikan ganti kepada kita dengan (kebun) yang lebih baik daripada yang ini, sungguh, kita mengharapkan ampunan dari Tuhan kita.”

68:33

# كَذٰلِكَ الْعَذَابُۗ وَلَعَذَابُ الْاٰخِرَةِ اَكْبَرُۘ لَوْ كَانُوْا يَعْلَمُوْنَ ࣖ

ka*dzaa*lika **a**l'a*dzaa*bu wala'a*dzaa*bu **a**l-*aa*khirati akbaru law k*aa*nuu ya'lamuun**a**

Seperti itulah azab (di dunia). Dan sungguh, azab akhirat lebih besar se-kiranya mereka mengetahui.

68:34

# اِنَّ لِلْمُتَّقِيْنَ عِنْدَ رَبِّهِمْ جَنّٰتِ النَّعِيْمِ

inna lilmuttaqiina 'inda rabbihim jann*aa*ti **al**nna'iim**i**

Sungguh, bagi orang-orang yang bertakwa (disediakan) surga yang penuh kenikmatan di sisi Tuhannya.

68:35

# اَفَنَجْعَلُ الْمُسْلِمِيْنَ كَالْمُجْرِمِيْنَۗ

afanaj'alu **a**lmuslimiina ka**a**lmujrimiin**a**

Apakah patut Kami memperlakukan orang-orang Islam itu seperti orang-orang yang berdosa (orang kafir)?

68:36

# مَا لَكُمْۗ كَيْفَ تَحْكُمُوْنَۚ

m*aa* lakum kayfa ta*h*kumuun**a**

Mengapa kamu (berbuat demikian)? Bagaimana kamu mengambil keputusan?

68:37

# اَمْ لَكُمْ كِتٰبٌ فِيْهِ تَدْرُسُوْنَۙ

am lakum kit*aa*bun fiihi tadrusuun**a**

Atau apakah kamu mempunyai kitab (yang diturunkan Allah) yang kamu pelajari?

68:38

# اِنَّ لَكُمْ فِيْهِ لَمَا تَخَيَّرُوْنَۚ

inna lakum fiihi lam*aa* takhayyaruun**a**

sesungguhnya kamu dapat memilih apa saja yang ada di dalamnya.

68:39

# اَمْ لَكُمْ اَيْمَانٌ عَلَيْنَا بَالِغَةٌ اِلٰى يَوْمِ الْقِيٰمَةِۙ اِنَّ لَكُمْ لَمَا تَحْكُمُوْنَۚ

am lakum aym*aa*nun 'alayn*aa* b*aa*lighatun il*aa* yawmi **a**lqiy*aa*mati inna lakum lam*aa* ta*h*kumuun**a**

Atau apakah kamu memperoleh (janji-janji yang diperkuat dengan) sumpah dari Kami, yang tetap berlaku sampai hari Kiamat; bahwa kamu dapat mengambil keputusan (sekehendakmu)?

68:40

# سَلْهُمْ اَيُّهُمْ بِذٰلِكَ زَعِيْمٌۚ

salhum ayyuhum bi*dzaa*lika za'iim**un**

Tanyakanlah kepada mereka, “Siapakah di antara mereka yang bertanggung jawab terhadap (keputusan yang diambil itu)?”

68:41

# اَمْ لَهُمْ شُرَكَاۤءُۚ فَلْيَأْتُوْا بِشُرَكَاۤىِٕهِمْ اِنْ كَانُوْا صٰدِقِيْنَ

am lahum syurak*aa*u falya/tuu bisyurak*aa*-ihim in k*aa*nuu *shaa*diqiin**a**

Atau apakah mereka mempunyai sekutu-sekutu? Kalau begitu hendaklah mereka mendatangkan sekutu-sekutunya jika mereka orang-orang yang benar.

68:42

# يَوْمَ يُكْشَفُ عَنْ سَاقٍ وَّيُدْعَوْنَ اِلَى السُّجُوْدِ فَلَا يَسْتَطِيْعُوْنَۙ

yawma yuksyafu 'an s*aa*qin wayud'awna il*aa* **al**ssujuudi fal*aa* yasta*th*ii'uun**a**

(Ingatlah) pada hari ketika betis disingkapkan dan mereka diseru untuk bersujud; maka mereka tidak mampu,

68:43

# خَاشِعَةً اَبْصَارُهُمْ تَرْهَقُهُمْ ذِلَّةٌ ۗوَقَدْ كَانُوْا يُدْعَوْنَ اِلَى السُّجُوْدِ وَهُمْ سَالِمُوْنَ

kh*aa*syi'atan ab*shaa*ruhum tarhaquhum *dz*illatun waqad k*aa*nuu yud'awna il*aa* **al**ssujuudi wahum s*aa*limuun**a**

pandangan mereka tertunduk ke bawah, diliputi kehinaan. Dan sungguh, dahulu (di dunia) mereka telah diseru untuk bersujud pada waktu mereka sehat (tetapi mereka tidak melakukan).

68:44

# فَذَرْنِيْ وَمَنْ يُّكَذِّبُ بِهٰذَا الْحَدِيْثِۗ سَنَسْتَدْرِجُهُمْ مِّنْ حَيْثُ لَا يَعْلَمُوْنَۙ

fa*dz*arnii waman yuka*dzdz*ibu bih*aadzaa* **a**l*h*adiitsi sanastadrijuhum min *h*aytsu l*aa* ya'lamuun**a**

Maka serahkanlah kepada-Ku (urusannya) dan orang-orang yang mendustakan perkataan ini (Al-Qur'an). Kelak akan Kami hukum mereka berangsur-angsur dari arah yang tidak mereka ketahui,

68:45

# وَاُمْلِيْ لَهُمْۗ اِنَّ كَيْدِيْ مَتِيْنٌ

waumlii lahum inna kaydii matiin**un**

dan Aku memberi tenggang waktu kepada mereka. Sungguh, rencana-Ku sangat teguh.

68:46

# اَمْ تَسْـَٔلُهُمْ اَجْرًا فَهُمْ مِّنْ مَّغْرَمٍ مُّثْقَلُوْنَۚ

am tas-aluhum ajran fahum min maghramin mutsqaluun**a**

Ataukah engkau (Muhammad) meminta imbalan kepada mereka, sehingga mereka dibebani dengan utang?

68:47

# اَمْ عِنْدَهُمُ الْغَيْبُ فَهُمْ يَكْتُبُوْنَ

am 'indahumu **a**lghaybu fahum yaktubuun**a**

Ataukah mereka mengetahui yang gaib, lalu mereka menuliskannya?

68:48

# فَاصْبِرْ لِحُكْمِ رَبِّكَ وَلَا تَكُنْ كَصَاحِبِ الْحُوْتِۘ اِذْ نَادٰى وَهُوَ مَكْظُوْمٌۗ

fa**i***sh*bir li*h*ukmi rabbika wal*aa* takun ka*shaah*ibi **a**l*h*uuti i*dz* n*aa*d*aa* wahuwa mak*zh*uum**un**

Maka bersabarlah engkau (Muhammad) terhadap ketetapan Tuhanmu, dan janganlah engkau seperti (Yunus) orang yang berada dalam (perut) ikan ketika dia berdoa dengan hati sedih.

68:49

# لَوْلَآ اَنْ تَدَارَكَهٗ نِعْمَةٌ مِّنْ رَّبِّهٖ لَنُبِذَ بِالْعَرَاۤءِ وَهُوَ مَذْمُوْمٌ

lawl*aa* an tad*aa*rakahu ni'matun min rabbihi lanubi*dz*a bi**a**l'ar*aa*-i wahuwa ma*dz*muum**un**

Sekiranya dia tidak segera mendapat nikmat dari Tuhannya, pastilah dia dicampakkan ke tanah tandus dalam keadaan tercela.

68:50

# فَاجْتَبٰىهُ رَبُّهٗ فَجَعَلَهٗ مِنَ الصّٰلِحِيْنَ

fa**i**jtab*aa*hu rabbuhu faja'alahu mina **al***shshaa*li*h*iin**a**

Lalu Tuhannya memilihnya dan menjadikannya termasuk orang yang saleh.

68:51

# وَاِنْ يَّكَادُ الَّذِيْنَ كَفَرُوْا لَيُزْلِقُوْنَكَ بِاَبْصَارِهِمْ لَمَّا سَمِعُوا الذِّكْرَ وَيَقُوْلُوْنَ اِنَّهٗ لَمَجْنُوْنٌ ۘ

wa-in yak*aa*du **al**la*dz*iina kafaruu layuzliquunaka bi-ab*shaa*rihim lamm*aa* sami'uu **al***dzdz*ikra wayaquuluuna innahu lamajnuun**un**

Dan sungguh, orang-orang kafir itu hampir-hampir menggelincirkanmu dengan pandangan mata mereka, ketika mereka mendengar Al-Qur'an dan mereka berkata, “Dia (Muhammad) itu benar-benar orang gila.”

68:52

# وَمَا هُوَ اِلَّا ذِكْرٌ لِّلْعٰلَمِيْنَ ࣖ

wam*aa* huwa ill*aa* *dz*ikrun lil'*aa*lamiin**a**

Padahal (Al-Qur'an) itu tidak lain adalah peringatan bagi seluruh alam.

<!--EndFragment-->