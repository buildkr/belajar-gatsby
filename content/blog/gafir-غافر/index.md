---
title: (40) Gafir - غافر
date: 2021-10-27T03:58:09.381Z
ayat: 40
description: "Jumlah Ayat: 85 / Arti: Maha Pengampun"
---
<!--StartFragment-->

40:1

# حٰمۤ ۚ

*haa*-miim

Ha Mim

40:2

# تَنْزِيْلُ الْكِتٰبِ مِنَ اللّٰهِ الْعَزِيْزِ الْعَلِيْمِۙ

tanziilu **a**lkit*aa*bi mina **al**l*aa*hi **a**l'aziizi **a**l'aliim**i**

Kitab ini (Al-Qur'an) diturunkan dari Allah Yang Mahaperkasa, Maha Mengetahui,

40:3

# غَافِرِ الذَّنْۢبِ وَقَابِلِ التَّوْبِ شَدِيْدِ الْعِقَابِ ذِى الطَّوْلِۗ لَآ اِلٰهَ اِلَّا هُوَ ۗاِلَيْهِ الْمَصِيْرُ

gh*aa*firi **al***dzdz*anbi waq*aa*bili **al**ttawbi syadiidi **a**l'iq*aa*bi *dz*ii **al***ththh*awli l*aa* il*aa*ha ill*aa* huwa ilayhi **a**lm

yang mengampuni dosa dan menerima tobat dan keras hukuman-Nya; yang memiliki karunia. Tidak ada tuhan selain Dia. Hanya kepada-Nyalah (semua makhluk) kembali.

40:4

# مَا يُجَادِلُ فِيْٓ اٰيٰتِ اللّٰهِ اِلَّا الَّذِيْنَ كَفَرُوْا فَلَا يَغْرُرْكَ تَقَلُّبُهُمْ فِى الْبِلَادِ

m*aa* yuj*aa*dilu fii *aa*y*aa*ti **al**l*aa*hi ill*aa* **al**la*dz*iina kafaruu fal*aa* yaghrurka taqallubuhum fii **a**lbil*aa*d**i**

Tidak ada yang memperdebatkan tentang ayat-ayat Allah, kecuali orang-orang yang kafir. Karena itu janganlah engkau (Muhammad) tertipu oleh keberhasilan usaha mereka di seluruh negeri.

40:5

# كَذَّبَتْ قَبْلَهُمْ قَوْمُ نُوحٍ وَّالْاَحْزَابُ مِنْۢ بَعْدِهِمْ ۖوَهَمَّتْ كُلُّ اُمَّةٍۢ بِرَسُوْلِهِمْ لِيَأْخُذُوْهُ وَجَادَلُوْا بِالْبَاطِلِ لِيُدْحِضُوا بِهِ الْحَقَّ فَاَخَذْتُهُمْ ۗفَكَيْفَ كَانَ عِقَابِ

ka*dzdz*abat qablahum qawmu nuu*h*in wa**a**l-a*h*z*aa*bu min ba'dihim wahammat kullu ummatin birasuulihim liya/khu*dz*uuhu waj*aa*daluu bi**a**lb*aath*ili liyud*h*i*dh*uu bihi

Sebelum mereka, kaum Nuh dan golongan-golongan yang bersekutu setelah mereka telah mendustakan (rasul) dan setiap umat telah merencanakan (tipu daya) terhadap rasul mereka untuk menawannya dan mereka membantah dengan (alasan) yang batil untuk melenyapkan

40:6

# وَكَذٰلِكَ حَقَّتْ كَلِمَتُ رَبِّكَ عَلَى الَّذِيْنَ كَفَرُوْٓا اَنَّهُمْ اَصْحٰبُ النَّارِۘ

waka*dzaa*lika *h*aqqat kalimatu rabbika 'al*aa* **al**la*dz*iina kafaruu annahum a*sh*-*haa*bu **al**nn*aa*r**i**

Dan demikianlah telah pasti berlaku ketetapan Tuhanmu terhadap orang-orang kafir, (yaitu) sesungguhnya mereka adalah penghuni neraka.

40:7

# اَلَّذِيْنَ يَحْمِلُوْنَ الْعَرْشَ وَمَنْ حَوْلَهٗ يُسَبِّحُوْنَ بِحَمْدِ رَبِّهِمْ وَيُؤْمِنُوْنَ بِهٖ وَيَسْتَغْفِرُوْنَ لِلَّذِيْنَ اٰمَنُوْاۚ رَبَّنَا وَسِعْتَ كُلَّ شَيْءٍ رَّحْمَةً وَّعِلْمًا فَاغْفِرْ لِلَّذِيْنَ تَابُوْا وَاتَّبَعُوْا سَبِيْلَكَ و

**al**la*dz*iina ya*h*miluuna **a**l'arsya waman *h*awlahu yusabbi*h*uuna bi*h*amdi rabbihim wayu/minuuna bihi wayastaghfiruuna lilla*dz*iina *aa*manuu rabban*aa* wasi'ta kulla syay-in ra

(Malaikat-malaikat) yang memikul ‘Arsy dan (malaikat) yang berada di sekelilingnya bertasbih dengan memuji Tuhannya dan mereka beriman kepada-Nya serta memohonkan ampunan untuk orang-orang yang beriman (seraya berkata), “Ya Tuhan kami, rahmat dan ilmu yan

40:8

# رَبَّنَا وَاَدْخِلْهُمْ جَنّٰتِ عَدْنِ ِۨالَّتِيْ وَعَدْتَّهُمْ وَمَنْ صَلَحَ مِنْ اٰبَاۤىِٕهِمْ وَاَزْوَاجِهِمْ وَذُرِّيّٰتِهِمْ ۗاِنَّكَ اَنْتَ الْعَزِيْزُ الْحَكِيْمُۙ

rabban*aa* wa-adkhilhum jann*aa*ti 'adnin **al**latii wa'adtahum waman *sh*ala*h*a min *aa*b*aa*-ihim wa-azw*aa*jihim wa*dz*urriyy*aa*tihim innaka anta **a**l'aziizu **a**

Ya Tuhan kami, masukkanlah mereka ke dalam surga ‘Adn yang telah Engkau janjikan kepada mereka, dan orang yang saleh di antara nenek moyang mereka, istri-istri, dan keturunan mereka. Sungguh, Engkaulah Yang Mahaperkasa, Mahabijaksana,

40:9

# وَقِهِمُ السَّيِّاٰتِۗ وَمَنْ تَقِ السَّيِّاٰتِ يَوْمَىِٕذٍ فَقَدْ رَحِمْتَهٗ ۗوَذٰلِكَ هُوَ الْفَوْزُ الْعَظِيْمُ ࣖ

waqihimu **al**ssayyi-*aa*ti waman taqi **al**ssayyi-*aa*ti yawma-i*dz*in faqad ra*h*imtahu wa*dzaa*lika huwa **a**lfawzu **a**l'a*zh*iim**u**

dan peliharalah mereka dari (bencana) kejahatan. Dan orang-orang yang Engkau pelihara dari (bencana) kejahatan pada hari itu, maka sungguh, Engkau telah menganugerahkan rahmat kepadanya dan demikian itulah kemenangan yang agung.”

40:10

# اِنَّ الَّذِيْنَ كَفَرُوْا يُنَادَوْنَ لَمَقْتُ اللّٰهِ اَكْبَرُ مِنْ مَّقْتِكُمْ اَنْفُسَكُمْ اِذْ تُدْعَوْنَ اِلَى الْاِيْمَانِ فَتَكْفُرُوْنَ

inna **al**la*dz*iina kafaruu yun*aa*dawna lamaqtu **al**l*aa*hi akbaru min maqtikum anfusakum i*dz* tud'awna il*aa* **a**l-iim*aa*ni fatakfuruun**a**

Sesungguhnya orang-orang yang kafir, kepada mereka (pada hari Kiamat) diserukan, “Sungguh, kebencian Allah (kepadamu) jauh lebih besar daripada kebencianmu kepada dirimu sendiri, ketika kamu diseru untuk beriman lalu kamu mengingkarinya.”

40:11

# قَالُوْا رَبَّنَآ اَمَتَّنَا اثْنَتَيْنِ وَاَحْيَيْتَنَا اثْنَتَيْنِ فَاعْتَرَفْنَا بِذُنُوْبِنَا فَهَلْ اِلٰى خُرُوْجٍ مِّنْ سَبِيْلٍ

q*aa*luu rabban*aa* amattan*aa* itsnatayni wa-a*h*yaytan*aa* itsnatayni fa**i**'tarafn*aa* bi*dz*unuubin*aa* fahal il*aa* khuruujin min sabiil**in**

Mereka menjawab, “Ya Tuhan kami, Engkau telah mematikan kami dua kali dan telah menghidupkan kami dua kali (pula), lalu kami mengakui dosa-dosa kami. Maka adakah jalan (bagi kami) untuk keluar (dari neraka)?”

40:12

# ذٰلِكُمْ بِاَنَّهٗٓ اِذَا دُعِيَ اللّٰهُ وَحْدَهٗ كَفَرْتُمْۚ وَاِنْ يُّشْرَكْ بِهٖ تُؤْمِنُوْا ۗفَالْحُكْمُ لِلّٰهِ الْعَلِيِّ الْكَبِيْرِ

*dzaa*likum bi-annahu i*dzaa* du'iya **al**l*aa*hu wa*h*dahu kafartum wa-in yusyrak bihi tu/minuu fa**a**l*h*ukmu lill*aa*hi **a**l'aliyyi **a**lkabiir**i**

Yang demikian itu karena sesungguhnya kamu mengingkari apabila diseru untuk menyembah Allah saja. Dan jika Allah dipersekutukan, kamu percaya. Maka keputusan (sekarang ini) adalah pada Allah Yang Mahatinggi, Mahabesar.

40:13

# هُوَ الَّذِيْ يُرِيْكُمْ اٰيٰتِهٖ وَيُنَزِّلُ لَكُمْ مِّنَ السَّمَاۤءِ رِزْقًا ۗوَمَا يَتَذَكَّرُ اِلَّا مَنْ يُّنِيْبُ

huwa **al**la*dz*ii yuriikum *aa*y*aa*tihi wayunazzilu lakum mina **al**ssam*aa*-i rizqan wam*aa* yata*dz*akkaru ill*aa* man yuniib**u**

Dialah yang memperlihatkan tanda-tanda (kekuasaan)-Nya kepadamu dan menurunkan rezeki dari langit untukmu. Dan tidak lain yang mendapat pelajaran hanyalah orang-orang yang kembali (kepada Allah).

40:14

# فَادْعُوا اللّٰهَ مُخْلِصِيْنَ لَهُ الدِّيْنَ وَلَوْ كَرِهَ الْكٰفِرُوْنَ

fa**u**d'uu **al**l*aa*ha mukhli*sh*iina lahu **al**ddiina walaw kariha **a**lk*aa*firuun**a**

Maka sembahlah Allah dengan tulus ikhlas beragama kepada-Nya, meskipun orang-orang kafir tidak menyukai(nya).

40:15

# رَفِيْعُ الدَّرَجٰتِ ذُو الْعَرْشِۚ يُلْقِى الرُّوْحَ مِنْ اَمْرِهٖ عَلٰى مَنْ يَّشَاۤءُ مِنْ عِبَادِهٖ لِيُنْذِرَ يَوْمَ التَّلَاقِۙ

rafii'u **al**ddaraj*aa*ti *dz*uu **a**l'arsyi yulqii **al**rruu*h*a min amrihi 'al*aa* man yasy*aa*u min 'ib*aa*dihi liyun*dz*ira yawma **al**ttal*aa*q**i**

**(Dialah) Yang Mahatinggi derajat-Nya, yang memiliki ’Arsy, yang menurunkan wahyu dengan perintah-Nya kepada siapa yang Dia kehendaki di antara hamba-hamba-Nya, agar memperingatkan (manusia) tentang hari pertemuan (hari Kiamat),**









40:16

# يَوْمَ هُمْ بَارِزُوْنَ ۚ لَا يَخْفٰى عَلَى اللّٰهِ مِنْهُمْ شَيْءٌ ۗلِمَنِ الْمُلْكُ الْيَوْمَ ۗ لِلّٰهِ الْوَاحِدِ الْقَهَّارِ

yawma hum b*aa*rizuuna l*aa* yakhf*aa* 'al*aa* **al**l*aa*hi minhum syay-un limani **a**lmulku **a**lyawma lill*aa*hi **a**lw*aah*idi **a**lqahh*aa*r<

(yaitu) pada hari (ketika) mereka keluar (dari kubur); tidak sesuatu pun keadaan mereka yang tersembunyi di sisi Allah. (Lalu Allah berfirman), “Milik siapakah kerajaan pada hari ini?” Milik Allah Yang Maha Esa, Maha Mengalahkan.

40:17

# اَلْيَوْمَ تُجْزٰى كُلُّ نَفْسٍۢ بِمَا كَسَبَتْ ۗ لَا ظُلْمَ الْيَوْمَ ۗاِنَّ اللّٰهَ سَرِيْعُ الْحِسَابِ

alyawma tujz*aa* kullu nafsin bim*aa* kasabat l*aa* *zh*ulma **a**lyawma inna **al**l*aa*ha sarii'u **a**l*h*is*aa*b**i**

Pada hari ini setiap jiwa diberi balasan sesuai dengan apa yang telah dikerjakannya. Tidak ada yang dirugikan pada hari ini. Sungguh, Allah sangat cepat perhitungan-Nya.

40:18

# وَاَنْذِرْهُمْ يَوْمَ الْاٰزِفَةِ اِذِ الْقُلُوْبُ لَدَى الْحَنَاجِرِ كَاظِمِيْنَ ەۗ مَا لِلظّٰلِمِيْنَ مِنْ حَمِيْمٍ وَّلَا شَفِيْعٍ يُّطَاعُۗ

wa-an*dz*irhum yawma **a**l-*aa*zifati i*dz*i **a**lquluubu lad*aa* **a**l*h*an*aa*jiri k*aats*imiina m*aa* li**l***zhzhaa*limiina min *h*amiimin wal*a*

Dan berilah mereka peringatan akan hari yang semakin dekat (hari Kiamat, yaitu) ketika hati (menyesak) sampai di kerongkongan karena menahan kesedihan. Tidak ada seorang pun teman setia bagi orang yang zalim dan tidak ada baginya seorang penolong yang dit







40:19

# يَعْلَمُ خَاۤىِٕنَةَ الْاَعْيُنِ وَمَا تُخْفِى الصُّدُوْرُ

ya'lamu kh*aa*-inata **a**l-a'yuni wam*aa* tukhfii **al***shsh*uduur**u**

Dia mengetahui (pandangan) mata yang khianat dan apa yang tersembunyi dalam dada.

40:20

# وَاللّٰهُ يَقْضِيْ بِالْحَقِّ ۗوَالَّذِيْنَ يَدْعُوْنَ مِنْ دُوْنِهٖ لَا يَقْضُوْنَ بِشَيْءٍ ۗاِنَّ اللّٰهَ هُوَ السَّمِيْعُ الْبَصِيْرُ ࣖ

wa**al**l*aa*hu yaq*dh*ii bi**a**l*h*aqqi wa**a**lla*dz*iina yad'uuna min duunihi l*aa* yaq*dh*uuna bisyay-in inna **al**l*aa*ha huwa **al**ssamii'u

Dan Allah memutuskan dengan kebenaran. Sedang mereka yang disembah selain-Nya tidak mampu memutuskan dengan sesuatu apa pun. Sesungguhnya Allah, Dialah Yang Maha Mendengar, Maha Melihat.

40:21

# ۞ اَوَلَمْ يَسِيْرُوْا فِى الْاَرْضِ فَيَنْظُرُوْا كَيْفَ كَانَ عَاقِبَةُ الَّذِيْنَ كَانُوْا مِنْ قَبْلِهِمْ ۗ كَانُوْا هُمْ اَشَدَّ مِنْهُمْ قُوَّةً وَّاٰثَارًا فِى الْاَرْضِ فَاَخَذَهُمُ اللّٰهُ بِذُنُوْبِهِمْ ۗوَمَا كَانَ لَهُمْ مِّنَ اللّٰهِ مِنْ وَّ

awa lam yasiiruu fii **a**l-ar*dh*i fayan*zh*uruu kayfa k*aa*na '*aa*qibatu **al**la*dz*iina k*aa*nuu min qablihim k*aa*nuu hum asyadda minhum quwwatan wa*aa*ts*aa*ran fii **a**

**Dan apakah mereka tidak mengadakan perjalanan di bumi, lalu memperhatikan bagaimana kesudahan orang-orang yang sebelum mereka? Orang-orang itu lebih hebat kekuatannya daripada mereka dan (lebih banyak) peninggalan-peninggalan (peradaban)nya di bumi, tetap**









40:22

# ذٰلِكَ بِاَنَّهُمْ كَانَتْ تَّأْتِيْهِمْ رُسُلُهُمْ بِالْبَيِّنٰتِ فَكَفَرُوْا فَاَخَذَهُمُ اللّٰهُ ۗاِنَّهٗ قَوِيٌّ شَدِيْدُ الْعِقَابِ

*dzaa*lika bi-annahum k*aa*nat ta/tiihim rusuluhum bi**a**lbayyin*aa*ti fakafaruu fa-akha*dz*ahumu **al**l*aa*hu innahu qawiyyun syadiidu **a**l'iq*aa*b**i**

Yang demikian itu adalah karena sesungguhnya rasul-rasul telah datang kepada mereka dengan membawa bukti-bukti yang nyata lalu mereka ingkar; maka Allah mengazab mereka. Sungguh, Dia Mahakuat, Mahakeras hukuman-Nya.

40:23

# وَلَقَدْ اَرْسَلْنَا مُوسٰى بِاٰيٰتِنَا وَسُلْطٰنٍ مُّبِيْنٍۙ

walaqad arsaln*aa* muus*aa* bi-*aa*y*aa*tin*aa* wasul*thaa*nin mubiin**in**

Dan sungguh, Kami telah mengutus Musa dengan membawa ayat-ayat Kami dan keterangan yang nyata,

40:24

# اِلٰى فِرْعَوْنَ وَهَامٰنَ وَقَارُوْنَ فَقَالُوْا سٰحِرٌ كَذَّابٌ

il*aa* fir'awna wah*aa*m*aa*na waq*aa*ruuna faq*aa*luu s*aah*irun ka*dzdzaa*b**un**

kepada Fir‘aun, Haman dan Karun; lalu mereka berkata, “(Musa) itu seorang pesihir dan pendusta.”

40:25

# فَلَمَّا جَاۤءَهُمْ بِالْحَقِّ مِنْ عِنْدِنَا قَالُوا اقْتُلُوْٓا اَبْنَاۤءَ الَّذِيْنَ اٰمَنُوْا مَعَهٗ وَاسْتَحْيُوْا نِسَاۤءَهُمْ ۗوَمَا كَيْدُ الْكٰفِرِيْنَ اِلَّا فِيْ ضَلٰلٍ

falamm*aa* j*aa*-ahum bi**a**l*h*aqqi min 'indin*aa* q*aa*luu uqtuluu abn*aa*-a **al**la*dz*iina *aa*manuu ma'ahu wa**i**sta*h*yuu nis*aa*-ahum wam*aa* kaydu

Maka ketika dia (Musa) datang kepada mereka membawa kebenaran dari Kami, mereka berkata, “Bunuhlah anak-anak laki-laki dari orang-orang yang beriman bersama dia dan biarkan hidup perempuan-perempuan mereka.” Namun tipu daya orang-orang kafir itu sia-sia b

40:26

# وَقَالَ فِرْعَوْنُ ذَرُوْنِيْٓ اَقْتُلْ مُوْسٰى وَلْيَدْعُ رَبَّهٗ ۚاِنِّيْٓ اَخَافُ اَنْ يُّبَدِّلَ دِيْنَكُمْ اَوْ اَنْ يُّظْهِرَ فِى الْاَرْضِ الْفَسَادَ

waq*aa*la fir'awnu *dz*aruunii aqtul muus*aa* walyad'u rabbahu innii akh*aa*fu an yubaddila diinakum aw an yu*zh*hira fii **a**l-ar*dh*i **a**lfas*aa*d**a**

Dan Fir‘aun berkata (kepada pembesar-pembesarnya), “Biar aku yang membunuh Musa dan suruh dia memohon kepada Tuhannya. Sesungguhnya aku khawatir dia akan menukar agamamu atau menimbulkan kerusakan di bumi.”

40:27

# وَقَالَ مُوْسٰىٓ اِنِّيْ عُذْتُ بِرَبِّيْ وَرَبِّكُمْ مِّنْ كُلِّ مُتَكَبِّرٍ لَّا يُؤْمِنُ بِيَوْمِ الْحِسَابِ ࣖ

waq*aa*la muus*aa* innii 'u*dz*tu birabbii warabbikum min kulli mutakabbirin l*aa* yu/minu biyawmi **a**l*h*is*aa*b**i**

Dan (Musa) berkata, “Sesungguhnya aku berlindung kepada Tuhanku dan Tuhanmu dari setiap orang yang menyombongkan diri yang tidak beriman kepada hari perhitungan.”

40:28

# وَقَالَ رَجُلٌ مُّؤْمِنٌۖ مِّنْ اٰلِ فِرْعَوْنَ يَكْتُمُ اِيْمَانَهٗٓ اَتَقْتُلُوْنَ رَجُلًا اَنْ يَّقُوْلَ رَبِّيَ اللّٰهُ وَقَدْ جَاۤءَكُمْ بِالْبَيِّنٰتِ مِنْ رَّبِّكُمْ ۗوَاِنْ يَّكُ كَاذِبًا فَعَلَيْهِ كَذِبُهٗ ۚوَاِنْ يَّكُ صَادِقًا يُّصِبْكُمْ بَعْ

waq*aa*la rajulun mu/minun min *aa*li fir'awna yaktumu iim*aa*nahu ataqtuluuna rajulan an yaquula rabbiyya **al**l*aa*hu waqad j*aa*-akum bi**a**lbayyin*aa*ti min rabbikum wa-in yaku k*aadz*iban

Dan seseorang yang beriman di antara keluarga Fir‘aun yang menyembunyikan imannya berkata, “Apakah kamu akan membunuh seseorang karena dia berkata, “Tuhanku adalah Allah,” padahal sungguh, dia telah datang kepadamu dengan membawa bukti-bukti yang nyata da

40:29

# يٰقَوْمِ لَكُمُ الْمُلْكُ الْيَوْمَ ظَاهِرِيْنَ فِى الْاَرْضِۖ فَمَنْ يَّنْصُرُنَا مِنْۢ بَأْسِ اللّٰهِ اِنْ جَاۤءَنَا ۗقَالَ فِرْعَوْنُ مَآ اُرِيْكُمْ اِلَّا مَآ اَرٰى وَمَآ اَهْدِيْكُمْ اِلَّا سَبِيْلَ الرَّشَادِ

y*aa* qawmi lakumu **a**lmulku **a**lyawma *zhaa*hiriina fii **a**l-ar*dh*i faman yan*sh*urun*aa* min ba/si **al**l*aa*hi in j*aa*-an*aa* q*aa*la fir'awnu m

Wahai kaumku! Pada hari ini kerajaan ada padamu dengan berkuasa di bumi, tetapi siapa yang akan menolong kita dari azab Allah jika (azab itu) menimpa kita?” Fir‘aun berkata, “Aku hanya mengemukakan kepadamu, apa yang aku pandang baik; dan aku hanya menunj

40:30

# وَقَالَ الَّذِيْٓ اٰمَنَ يٰقَوْمِ اِنِّيْٓ اَخَافُ عَلَيْكُمْ مِّثْلَ يَوْمِ الْاَحْزَابِۙ

waq*aa*la **al**la*dz*ii *aa*mana y*aa* qawmi innii akh*aa*fu 'alaykum mitsla yawmi **a**l-a*h*z*aa*b**i**

Dan orang yang beriman itu berkata, “Wahai kaumku! Sesungguhnya aku khawatir kamu akan ditimpa (bencana) seperti hari kehancuran golongan yang bersekutu,

40:31

# مِثْلَ دَأْبِ قَوْمِ نُوْحٍ وَّعَادٍ وَّثَمُوْدَ وَالَّذِيْنَ مِنْۢ بَعْدِهِمْ ۗوَمَا اللّٰهُ يُرِيْدُ ظُلْمًا لِّلْعِبَادِ

mitsla da/bi qawmi nuu*h*in wa'*aa*din watsamuuda wa**a**lla*dz*iina min ba'dihim wam*aa* **al**l*aa*hu yuriidu *zh*ulman lil'ib*aa*d**i**

(yakni) seperti kebiasaan kaum Nuh, ’Ad, samud dan orang-orang yang datang setelah mereka. Padahal Allah tidak menghendaki kezaliman terhadap hamba-hamba-Nya.”

40:32

# وَيٰقَوْمِ اِنِّيْٓ اَخَافُ عَلَيْكُمْ يَوْمَ التَّنَادِۙ

way*aa* qawmi innii akh*aa*fu 'alaykum yawma **al**ttan*aa*d**i**

Dan wahai kaumku! Sesungguhnya aku benar-benar khawatir terhadapmu akan (siksaan) hari saling memanggil,

40:33

# يَوْمَ تُوَلُّوْنَ مُدْبِرِيْنَۚ مَا لَكُمْ مِّنَ اللّٰهِ مِنْ عَاصِمٍۚ وَمَنْ يُّضْلِلِ اللّٰهُ فَمَا لَهٗ مِنْ هَادٍ

yawma tuwalluuna mudbiriina m*aa* lakum mina **al**l*aa*hi min '*aas*imin waman yu*dh*lili **al**l*aa*hu fam*aa* lahu min h*aa*d**in**

(yaitu) pada hari (ketika) kamu berpaling ke belakang (lari), tidak ada seorang pun yang mampu menyelamatkan kamu dari (azab) Allah. Dan barangsiapa dibiarkan sesat oleh Allah, niscaya tidak ada sesuatu pun yang mampu memberi petunjuk.”

40:34

# وَلَقَدْ جَاۤءَكُمْ يُوْسُفُ مِنْ قَبْلُ بِالْبَيِّنٰتِ فَمَا زِلْتُمْ فِيْ شَكٍّ مِّمَّا جَاۤءَكُمْ بِهٖ ۗحَتّٰىٓ اِذَا هَلَكَ قُلْتُمْ لَنْ يَّبْعَثَ اللّٰهُ مِنْۢ بَعْدِهٖ رَسُوْلًا ۗ كَذٰلِكَ يُضِلُّ اللّٰهُ مَنْ هُوَ مُسْرِفٌ مُّرْتَابٌۙ

walaqad j*aa*-akum yuusufu min qablu bi**a**lbayyin*aa*ti fam*aa* ziltum fii syakkin mimm*aa* j*aa*-akum bihi *h*att*aa* i*dzaa* halaka qultum lan yab'atsa **al**l*aa*hu min ba'dihi ra

Dan sungguh, sebelum itu Yusuf telah datang kepadamu dengan membawa bukti-bukti yang nyata, tetapi kamu senantiasa meragukan apa yang dibawanya, bahkan ketika dia wafat, kamu berkata, “Allah tidak akan mengirim seorang rasul pun setelahnya.” Demikianlah A

40:35

# ۨالَّذِيْنَ يُجَادِلُوْنَ فِيْٓ اٰيٰتِ اللّٰهِ بِغَيْرِ سُلْطٰنٍ اَتٰىهُمْۗ كَبُرَ مَقْتًا عِنْدَ اللّٰهِ وَعِنْدَ الَّذِيْنَ اٰمَنُوْا ۗ كَذٰلِكَ يَطْبَعُ اللّٰهُ عَلٰى كُلِّ قَلْبِ مُتَكَبِّرٍ جَبَّارٍ

**al**la*dz*iina yuj*aa*diluuna fii *aa*y*aa*ti **al**l*aa*hi bighayri sul*thaa*nin at*aa*hum kabura maqtan 'inda **al**l*aa*hi wa'inda **al**la*dz*iin

(yaitu) orang-orang yang memperdebatkan ayat-ayat Allah tanpa alasan yang sampai kepada mereka. Sangat besar kemurkaan (bagi mereka) di sisi Allah dan orang-orang yang beriman. Demikianlah Allah mengunci hati setiap orang yang sombong dan berlaku sewenang

40:36

# وَقَالَ فِرْعَوْنُ يٰهَامٰنُ ابْنِ لِيْ صَرْحًا لَّعَلِّيْٓ اَبْلُغُ الْاَسْبَابَۙ

waq*aa*la fir'awnu y*aa* h*aa*m*aa*nu ibni lii *sh*ar*h*an la'allii ablughu **a**l-asb*aa*b**a**

Dan Fir‘aun berkata, “Wahai Haman! Buatkanlah untukku sebuah bangunan yang tinggi agar aku sampai ke pintu-pintu,

40:37

# اَسْبَابَ السَّمٰوٰتِ فَاَطَّلِعَ اِلٰٓى اِلٰهِ مُوْسٰى وَاِنِّيْ لَاَظُنُّهٗ كَاذِبًا ۗوَكَذٰلِكَ زُيِّنَ لِفِرْعَوْنَ سُوْۤءُ عَمَلِهٖ وَصُدَّ عَنِ السَّبِيْلِ ۗوَمَا كَيْدُ فِرْعَوْنَ اِلَّا فِيْ تَبَابٍ ࣖ

asb*aa*ba **al**ssam*aa*w*aa*ti fa-a*ththh*ali'a il*aa* il*aa*hi muus*aa* wa-innii la-a*zh*unnuhu k*aadz*iban waka*dzaa*lika zuyyina lifir'awna suu-u 'amalihi wa*sh*udda 'ani **al**

**(yaitu) pintu-pintu langit, agar aku dapat melihat Tuhannya Musa, tetapi aku tetap memandangnya seorang pendusta.” Dan demikianlah dijadikan terasa indah bagi Fir‘aun perbuatan buruknya itu, dan dia tertutup dari jalan (yang benar); dan tipu daya Fir‘aun**









40:38

# وَقَالَ الَّذِيْٓ اٰمَنَ يٰقَوْمِ اتَّبِعُوْنِ اَهْدِكُمْ سَبِيْلَ الرَّشَادِۚ

waq*aa*la **al**la*dz*ii *aa*mana y*aa* qawmi ittabi'uuni ahdikum sabiila **al**rrasy*aa*d**i**

Dan orang yang beriman itu berkata, “Wahai kaumku! Ikutilah aku, aku akan menunjukkan kepadamu jalan yang benar.

40:39

# يٰقَوْمِ اِنَّمَا هٰذِهِ الْحَيٰوةُ الدُّنْيَا مَتَاعٌ ۖوَّاِنَّ الْاٰخِرَةَ هِيَ دَارُ الْقَرَارِ

y*aa* qawmi innam*aa* h*aadz*ihi **a**l*h*ay*aa*tu **al**dduny*aa* mat*aa*'un wa-inna **a**l-*aa*khirata hiya d*aa*ru **a**lqar*aa*r**i**

Wahai kaumku! Sesungguhnya kehidupan dunia ini hanyalah kesenangan (sementara) dan sesungguhnya akhirat itulah negeri yang kekal.

40:40

# مَنْ عَمِلَ سَيِّئَةً فَلَا يُجْزٰىٓ اِلَّا مِثْلَهَاۚ وَمَنْ عَمِلَ صَالِحًا مِّنْ ذَكَرٍ اَوْ اُنْثٰى وَهُوَ مُؤْمِنٌ فَاُولٰۤىِٕكَ يَدْخُلُوْنَ الْجَنَّةَ يُرْزَقُوْنَ فِيْهَا بِغَيْرِ حِسَابٍ

man 'amila sayyi-atan fal*aa* yujz*aa* ill*aa* mitslah*aa* waman 'amila *shaa*li*h*an min *dz*akarin aw unts*aa* wahuwa mu/minun faul*aa*-ika yadkhuluuna **a**ljannata yurzaquuna fiih*aa* bigh

Barangsiapa mengerjakan perbuatan jahat, maka dia akan dibalas sebanding dengan kejahatan itu. Dan barang siapa mengerjakan kebajikan, baik laki-laki maupun perempuan sedangkan dia dalam keadaan beriman, maka mereka akan masuk surga, mereka diberi rezeki

40:41

# ۞ وَيٰقَوْمِ مَا لِيْٓ اَدْعُوْكُمْ اِلَى النَّجٰوةِ وَتَدْعُوْنَنِيْٓ اِلَى النَّارِۗ

way*aa* qawmi m*aa*lii ad'uukum il*aa* **al**nnaj*aa*ti watad'uunanii il*aa* **al**nn*aa*r**i**

Dan wahai kaumku! Bagaimanakah ini, aku menyerumu kepada keselamatan, tetapi kamu menyeruku ke neraka?

40:42

# تَدْعُوْنَنِيْ لِاَكْفُرَ بِاللّٰهِ وَاُشْرِكَ بِهٖ مَا لَيْسَ لِيْ بِهٖ عِلْمٌ وَّاَنَا۠ اَدْعُوْكُمْ اِلَى الْعَزِيْزِ الْغَفَّارِ

tad'uunanii li-akfura bi**al**l*aa*hi wausyrika bihi m*aa* laysa lii bihi 'ilmun wa-an*aa* ad'uukum il*aa* **a**l'aziizi **a**lghaff*aa*r**i**

(Mengapa) kamu menyeruku agar kafir kepada Allah dan mempersekutukan-Nya dengan sesuatu yang aku tidak mempunyai ilmu tentang itu, padahal aku menyerumu (beriman) kepada Yang Mahaperkasa, Maha Pengampun?

40:43

# لَا جَرَمَ اَنَّمَا تَدْعُوْنَنِيْٓ اِلَيْهِ لَيْسَ لَهٗ دَعْوَةٌ فِى الدُّنْيَا وَلَا فِى الْاٰخِرَةِ وَاَنَّ مَرَدَّنَآ اِلَى اللّٰهِ وَاَنَّ الْمُسْرِفِيْنَ هُمْ اَصْحٰبُ النَّارِ

l*aa* jarama annam*aa* tad'uunanii ilayhi laysa lahu da'watun fii **al**dduny*aa* wal*aa* fii **a**l-*aa*khirati wa-anna maraddan*aa* il*aa* **al**l*aa*hi wa-anna **a**

**Sudah pasti bahwa apa yang kamu serukan aku kepadanya bukanlah suatu seruan yang berguna baik di dunia maupun di akhirat. Dan sesungguhnya tempat kembali kita pasti kepada Allah, dan sesungguhnya orang-orang yang melampaui batas, mereka itu akan menjadi p**









40:44

# فَسَتَذْكُرُوْنَ مَآ اَقُوْلُ لَكُمْۗ وَاُفَوِّضُ اَمْرِيْٓ اِلَى اللّٰهِ ۗاِنَّ اللّٰهَ بَصِيْرٌ ۢبِالْعِبَادِ

fasata*dz*kuruuna m*aa* aquulu lakum waufawwi*dh*u amrii il*aa* **al**l*aa*hi inna **al**l*aa*ha ba*sh*iirun bi**a**l'ib*aa*d**i**

Maka kelak kamu akan ingat kepada apa yang kukatakan kepadamu. Dan aku menyerahkan urusanku kepada Allah. Sungguh, Allah Maha Melihat akan hamba-hamba-Nya.”

40:45

# فَوَقٰىهُ اللّٰهُ سَيِّاٰتِ مَا مَكَرُوْا وَحَاقَ بِاٰلِ فِرْعَوْنَ سُوْۤءُ الْعَذَابِۚ

fawaq*aa*hu **al**l*aa*hu sayyi-*aa*ti m*aa* makaruu wa*haa*qa bi-*aa*li fir'awna suu-u **a**l'a*dzaa*b**i**

Maka Allah memeliharanya dari kejahatan tipu daya mereka, sedangkan Fir‘aun beserta kaumnya dikepung oleh azab yang sangat buruk.

40:46

# اَلنَّارُ يُعْرَضُوْنَ عَلَيْهَا غُدُوًّا وَّعَشِيًّا ۚوَيَوْمَ تَقُوْمُ السَّاعَةُ ۗ اَدْخِلُوْٓا اٰلَ فِرْعَوْنَ اَشَدَّ الْعَذَابِ

a**l**nn*aa*ru yu'ra*dh*uuna 'alayh*aa* ghuduwwan wa'asyiyyan wayawma taquumu **al**ss*aa*'atu adkhiluu *aa*la fir'awna asyadda **a**l'a*dzaa*b**i**

Kepada mereka diperlihatkan neraka, pada pagi dan petang, dan pada hari terjadinya Kiamat. (Lalu kepada malaikat diperintahkan), “Masukkanlah Fir‘aun dan kaumnya ke dalam azab yang sangat keras!”

40:47

# وَاِذْ يَتَحَاۤجُّوْنَ فِى النَّارِ فَيَقُوْلُ الضُّعَفٰۤؤُ لِلَّذِيْنَ اسْتَكْبَرُوْٓا اِنَّا كُنَّا لَكُمْ تَبَعًا فَهَلْ اَنْتُمْ مُّغْنُوْنَ عَنَّا نَصِيْبًا مِّنَ النَّارِ

wa-i*dz* yata*haa*jjuuna fii **al**nn*aa*ri fayaquulu **al***dhdh*u'af*aa*u lilla*dz*iina istakbaruu inn*aa* kunn*aa* lakum taba'an fahal antum mughnuuna 'ann*aa* na*sh*iiban mina

Dan (Ingatlah), ketika mereka berbantah-bantahan dalam neraka, maka orang yang lemah berkata kepada orang-orang yang menyombongkan diri, “Sesungguhnya kami dahulu adalah pengikut-pengikutmu, maka dapatkah kamu melepaskan sebagian (azab) api neraka yang me

40:48

# قَالَ الَّذِيْنَ اسْتَكْبَرُوْٓا اِنَّا كُلٌّ فِيْهَآ اِنَّ اللّٰهَ قَدْ حَكَمَ بَيْنَ الْعِبَادِ

q*aa*la **al**la*dz*iina istakbaruu inn*aa* kullun fiih*aa* inna **al**l*aa*ha qad *h*akama bayna **a**l'ib*aa*d**i**

Orang-orang yang menyombongkan diri menjawab, “Sesungguhnya kita semua sama-sama dalam neraka karena Allah telah menetapkan keputusan antara hamba-hamba-(Nya).”

40:49

# وَقَالَ الَّذِيْنَ فِى النَّارِ لِخَزَنَةِ جَهَنَّمَ ادْعُوْا رَبَّكُمْ يُخَفِّفْ عَنَّا يَوْمًا مِّنَ الْعَذَابِ

waq*aa*la **al**la*dz*iina fii **al**nn*aa*ri likhazanati jahannama ud'uu rabbakum yukhaffif 'ann*aa* yawman mina **a**l'a*dzaa*b**i**

Dan orang-orang yang berada dalam neraka berkata kepada penjaga-penjaga neraka Jahanam, “Mohonkanlah kepada Tuhanmu agar Dia meringankan azab atas kami sehari saja.”

40:50

# قَالُوْٓا اَوَلَمْ تَكُ تَأْتِيْكُمْ رُسُلُكُمْ بِالْبَيِّنٰتِ ۗقَالُوْا بَلٰىۗ قَالُوْا فَادْعُوْا ۚوَمَا دُعٰۤؤُا الْكٰفِرِيْنَ اِلَّا فِيْ ضَلٰلٍ ࣖ

q*aa*luu awa lam taku ta/tiikum rusulukum bi**a**lbayyin*aa*ti q*aa*luu bal*aa* q*aa*luu fa**u**d'uu wam*aa* du'*aa*u **a**lk*aa*firiina ill*aa* fii *dh*al*aa*l<

Maka (penjaga-penjaga Jahanam) berkata, “Apakah rasul-rasul belum datang kepadamu dengan membawa bukti-bukti yang nyata?” Mereka menjawab, “Benar, sudah datang.” (Penjaga-penjaga Jahanam) berkata, “Berdoalah kamu (sendiri!)” Namun doa orang-orang kafir it

40:51

# اِنَّا لَنَنْصُرُ رُسُلَنَا وَالَّذِيْنَ اٰمَنُوْا فِى الْحَيٰوةِ الدُّنْيَا وَيَوْمَ يَقُوْمُ الْاَشْهَادُۙ

inn*aa* lanan*sh*uru rusulan*aa* wa**a**lla*dz*iina *aa*manuu fii **a**l*h*ay*aa*ti **al**dduny*aa* wayawma yaquumu **a**l-asyh*aa*d**u**

Sesungguhnya Kami akan menolong rasul-rasul Kami dan orang-orang yang beriman dalam kehidupan dunia dan pada hari tampilnya para saksi (hari Kiamat),

40:52

# يَوْمَ لَا يَنْفَعُ الظّٰلِمِيْنَ مَعْذِرَتُهُمْ وَلَهُمُ اللَّعْنَةُ وَلَهُمْ سُوْۤءُ الدَّارِ

yawma l*aa* yanfa'u **al***zhzhaa*limiina ma'*dz*iratuhum walahumu **al**la'natu walahum suu-u **al**dd*aa*r**i**

(yaitu) hari ketika permintaan maaf tidak berguna bagi orang-orang zalim dan mereka mendapat laknat dan tempat tinggal yang buruk.

40:53

# وَلَقَدْاٰتَيْنَا مُوْسٰى الْهُدٰى وَاَوْرَثْنَا بَنِيْٓ اِسْرَاۤءِيْلَ الْكِتٰبَۙ

walaqad *aa*tayn*aa* muus*aa* **a**lhud*aa* wa-awratsn*aa* banii isr*aa*-iila **a**lkit*aa*b**a**

Dan sungguh, Kami telah memberikan petunjuk kepada Musa; dan mewariskan Kitab (Taurat) kepada Bani Israil,

40:54

# هُدًى وَّذِكْرٰى لِاُولِى الْاَلْبَابِ

hudan wa*dz*ikr*aa* li-ulii **a**l-alb*aa*b**i**

untuk menjadi petunjuk dan peringatan bagi orang-orang yang berpikiran sehat.

40:55

# فَاصْبِرْ اِنَّ وَعْدَ اللّٰهِ حَقٌّ وَّاسْتَغْفِرْ لِذَنْۢبِكَ وَسَبِّحْ بِحَمْدِ رَبِّكَ بِالْعَشِيِّ وَالْاِبْكَارِ

fa**i***sh*bir inna wa'da **al**l*aa*hi *h*aqqun wa**i**staghfir li*dz*anbika wasabbi*h* bi*h*amdi rabbika bi**a**l'asyiyyi wa**a**l-ibk*aa*r**i**

**Maka bersabarlah kamu, sesungguhnya janji Allah itu benar, dan mohonlah ampun untuk dosamu dan bertasbihlah seraya memuji Tuhanmu pada waktu petang dan pagi.**









40:56

# اِنَّ الَّذِيْنَ يُجَادِلُوْنَ فِيْٓ اٰيٰتِ اللّٰهِ بِغَيْرِ سُلْطٰنٍ اَتٰىهُمْ ۙاِنْ فِيْ صُدُوْرِهِمْ اِلَّا كِبْرٌ مَّا هُمْ بِبَالِغِيْهِۚ فَاسْتَعِذْ بِاللّٰهِ ۗاِنَّهٗ هُوَ السَّمِيْعُ الْبَصِيْرُ

inna **al**la*dz*iina yuj*aa*diluuna fii *aa*y*aa*ti **al**l*aa*hi bighayri sul*thaa*nin at*aa*hum in fii *sh*uduurihim ill*aa* kibrun m*aa* hum bib*aa*lighiihi fa

Sesungguhnya orang-orang yang memperdebatkan ayat-ayat Allah tanpa alasan (bukti) yang sampai kepada mereka, yang ada dalam dada mereka hanyalah (keinginan akan) kebesaran yang tidak akan mereka capai, maka mintalah perlindungan kepada Allah. Sungguh, Dia

40:57

# لَخَلْقُ السَّمٰوٰتِ وَالْاَرْضِ اَكْبَرُ مِنْ خَلْقِ النَّاسِ وَلٰكِنَّ اَكْثَرَ النَّاسِ لَا يَعْلَمُوْنَ

lakhalqu **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i akbaru min khalqi **al**nn*aa*si wal*aa*kinna aktsara **al**nn*aa*si l*aa* ya'lamuun**a**

Sungguh, penciptaan langit dan bumi itu lebih besar daripada penciptaan manusia, akan tetapi kebanyakan manusia tidak mengetahui.

40:58

# وَمَا يَسْتَوِى الْاَعْمٰى وَالْبَصِيْرُ ەۙ وَالَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِ وَلَا الْمُسِيْۤئُ ۗقَلِيْلًا مَّا تَتَذَكَّرُوْنَ

wam*aa* yastawii **a**l-a'm*aa* wa**a**lba*sh*iiru wa**a**lla*dz*iina *aa*manuu wa'amiluu **al***shshaa*li*haa*ti wal*aa* **a**lmusii-u qaliilan m*a*

Dan tidak sama orang yang buta dengan orang yang melihat, dan tidak (sama) pula orang-orang yang beriman dan mengerjakan kebajikan dengan orang-orang yang berbuat kejahatan. Hanya sedikit sekali yang kamu ambil pelajaran.







40:59

# اِنَّ السَّاعَةَ لَاٰتِيَةٌ لَّا رَيْبَ فِيْهَا ۖوَلٰكِنَّ اَكْثَرَ النَّاسِ لَا يُؤْمِنُوْنَ

inna **al**ss*aa*'ata la*aa*tiyatun l*aa* rayba fiih*aa* wal*aa*kinna aktsara **al**nn*aa*si l*aa* yu/minuun**a**

Sesungguhnya hari Kiamat pasti akan datang, tidak ada keraguan tentangnya, akan tetapi kebanyakan manusia tidak beriman.

40:60

# وَقَالَ رَبُّكُمُ ادْعُوْنِيْٓ اَسْتَجِبْ لَكُمْ ۗاِنَّ الَّذِيْنَ يَسْتَكْبِرُوْنَ عَنْ عِبَادَتِيْ سَيَدْخُلُوْنَ جَهَنَّمَ دَاخِرِيْنَ ࣖࣖࣖ

waq*aa*la rabbukumu ud'uunii astajib lakum inna **al**la*dz*iina yastakbiruuna 'an 'ib*aa*datii sayadkhuluuna jahannama d*aa*khiriin**a**

Dan Tuhanmu berfirman, “Berdoalah kepada-Ku, niscaya akan Aku perkenankan bagimu. Sesungguhnya orang-orang yang sombong tidak mau menyembah-Ku akan masuk neraka Jahanam dalam keadaan hina dina.”

40:61

# اَللّٰهُ الَّذِيْ جَعَلَ لَكُمُ الَّيْلَ لِتَسْكُنُوْا فِيْهِ وَالنَّهَارَ مُبْصِرًا ۗاِنَّ اللّٰهَ لَذُوْ فَضْلٍ عَلَى النَّاسِ وَلٰكِنَّ اَكْثَرَ النَّاسِ لَا يَشْكُرُوْنَ

**al**l*aa*hu **al**la*dz*ii ja'ala lakumu **al**layla litaskunuu fiihi wa**al**nnah*aa*ra mub*sh*iran inna **al**l*aa*ha la*dz*uu fa*dh*lin 'al*aa* <

Allah-lah yang menjadikan malam untukmu agar kamu beristirahat padanya; (dan menjadikan) siang terang benderang. Sungguh, Allah benar-benar memiliki karunia yang dilimpahkan kepada manusia, tetapi kebanyakan manusia tidak bersyukur.

40:62

# ذٰلِكُمُ اللّٰهُ رَبُّكُمْ خَالِقُ كُلِّ شَيْءٍۘ لَآ اِلٰهَ اِلَّا هُوَ ۖفَاَنّٰى تُؤْفَكُوْنَ

*dzaa*likumu **al**l*aa*hu rabbukum kh*aa*liqu kulli syay-in l*aa* il*aa*ha ill*aa* huwa fa-ann*aa* tu/fakuun**a**

Demikianlah Allah, Tuhanmu, Pencipta segala sesuatu, tidak ada tuhan selain Dia; maka bagaimanakah kamu dapat dipalingkan?

40:63

# كَذٰلِكَ يُؤْفَكُ الَّذِيْنَ كَانُوْا بِاٰيٰتِ اللّٰهِ يَجْحَدُوْنَ

ka*dzaa*lika yu/faku **al**la*dz*iina k*aa*nuu bi-*aa*y*aa*ti **al**l*aa*hi yaj*h*aduun**a**

Demikianlah orang-orang yang selalu mengingkari ayat-ayat Allah dipalingkan.

40:64

# اَللّٰهُ الَّذِيْ جَعَلَ لَكُمُ الْاَرْضَ قَرَارًا وَّالسَّمَاۤءَ بِنَاۤءً وَّصَوَّرَكُمْ فَاَحْسَنَ صُوَرَكُمْ وَرَزَقَكُمْ مِّنَ الطَّيِّبٰتِ ۗذٰلِكُمُ اللّٰهُ رَبُّكُمْ ۚ فَتَبٰرَكَ اللّٰهُ رَبُّ الْعٰلَمِيْنَ

**al**l*aa*hu **al**la*dz*ii ja'ala lakumu **a**l-ar*dh*a qar*aa*ran wa**al**ssam*aa*-a bin*aa*-an wa*sh*awwarakum fa-a*h*sana *sh*uwarakum warazaqakum mina <

Allah-lah yang menjadikan bumi untukmu sebagai tempat menetap dan langit sebagai atap, dan membentukmu lalu memperindah rupamu serta memberimu rezeki dari yang baik-baik. Demikianlah Allah, Tuhanmu, Mahasuci Allah, Tuhan seluruh alam.

40:65

# هُوَ الْحَيُّ لَآ اِلٰهَ اِلَّا هُوَ فَادْعُوْهُ مُخْلِصِيْنَ لَهُ الدِّيْنَ ۗ اَلْحَمْدُ لِلّٰهِ رَبِّ الْعٰلَمِيْنَ

huwa **a**l*h*ayyu l*aa* il*aa*ha ill*aa* huwa fa**u**d'uuhu mukhli*sh*iina lahu **al**ddiina **a**l*h*amdu lill*aa*hi rabbi **a**l'*aa*lamiin

Dialah yang hidup kekal, tidak ada tuhan selain Dia; maka sembahlah Dia dengan tulus ikhlas beragama kepada-Nya. Segala puji bagi Allah Tuhan seluruh alam.







40:66

# ۞ قُلْ اِنِّيْ نُهِيْتُ اَنْ اَعْبُدَ الَّذِيْنَ تَدْعُوْنَ مِنْ دُوْنِ اللّٰهِ لَمَّا جَاۤءَنِيَ الْبَيِّنٰتُ مِنْ رَّبِّيْ وَاُمِرْتُ اَنْ اُسْلِمَ لِرَبِّ الْعٰلَمِيْنَ

qul innii nuhiitu an a'buda **al**la*dz*iina tad'uuna min duuni **al**l*aa*hi lamm*aa* j*aa*-aniya **a**lbayyin*aa*tu min rabbii waumirtu an uslima lirabbi **a**l'*aa*lamiin

Katakanlah (Muhammad), “Sungguh, aku dilarang menyembah sembahan yang kamu sembah selain Allah, setelah datang kepadaku keterangan-keterangan dari Tuhanku; dan aku diperintahkan agar berserah diri kepada Tuhan seluruh alam.”

40:67

# هُوَ الَّذِيْ خَلَقَكُمْ مِّنْ تُرَابٍ ثُمَّ مِنْ نُّطْفَةٍ ثُمَّ مِنْ عَلَقَةٍ ثُمَّ يُخْرِجُكُمْ طِفْلًا ثُمَّ لِتَبْلُغُوْٓا اَشُدَّكُمْ ثُمَّ لِتَكُوْنُوْا شُيُوْخًا ۚوَمِنْكُمْ مَّنْ يُّتَوَفّٰى مِنْ قَبْلُ وَلِتَبْلُغُوْٓا اَجَلًا مُّسَمًّى وَّلَعَل

huwa **al**la*dz*ii khalaqakum min tur*aa*bin tsumma min nu*th*fatin tsumma min 'alaqatin tsumma yukhrijukum *th*iflan tsumma litablughuu asyuddakum tsumma litakuunuu syuyuukhan waminkum man yutawaff*aa* min qablu wal

Dialah yang menciptakanmu dari tanah, kemudian dari setetes mani, lalu dari segumpal darah, kemudian kamu dilahirkan sebagai seorang anak, kemudian dibiarkan kamu sampai dewasa, lalu menjadi tua. Tetapi di antara kamu ada yang dimatikan sebelum itu. (Kami

40:68

# هُوَ الَّذِيْ يُحْيٖ وَيُمِيْتُۚ فَاِذَا قَضٰىٓ اَمْرًا فَاِنَّمَا يَقُوْلُ لَهٗ كُنْ فَيَكُوْنُ ࣖ

huwa **al**la*dz*ii yu*h*yii wayumiitu fa-i*dzaa* qa*daa* amran fa-innam*aa* yaquulu lahu kun fayakuun**u**

Dialah yang menghidupkan dan mematikan. Maka apabila Dia hendak menetapkan sesuatu urusan, Dia hanya berkata kepadanya, “Jadilah!” Maka jadilah sesuatu itu.

40:69

# اَلَمْ تَرَ اِلَى الَّذِيْنَ يُجَادِلُوْنَ فِيْٓ اٰيٰتِ اللّٰهِ ۗاَنّٰى يُصْرَفُوْنَۚ

alam tara il*aa* **al**la*dz*iina yuj*aa*diluuna fii *aa*y*aa*ti **al**l*aa*hi ann*aa* yu*sh*rafuun**a**

Apakah kamu tidak memperhatikan orang-orang yang (selalu) membantah ayat-ayat Allah? Bagaimana mereka dapat dipalingkan?

40:70

# اَلَّذِيْنَ كَذَّبُوْا بِالْكِتٰبِ وَبِمَآ اَرْسَلْنَا بِهٖ رُسُلَنَا ۗفَسَوْفَ يَعْلَمُوْنَۙ

**al**la*dz*iina ka*dzdz*abuu bi**a**lkit*aa*bi wabim*aa* arsaln*aa* bihi rusulan*aa* fasawfa ya'lamuun**a**

(Yaitu) orang-orang yang mendustakan Kitab (Al-Qur'an) dan wahyu yang dibawa oleh rasul-rasul Kami yang telah Kami utus. Kelak mereka akan mengetahui,

40:71

# اِذِ الْاَغْلٰلُ فِيْٓ اَعْنَاقِهِمْ وَالسَّلٰسِلُۗ يُسْحَبُوْنَۙ

i*dz*i **a**l-aghl*aa*lu fii a'n*aa*qihim wa**al**ssal*aa*silu yus*h*abuun**a**

ketika belenggu dan rantai dipasang di leher mereka, seraya mereka diseret,

40:72

# فِى الْحَمِيْمِ ەۙ ثُمَّ فِى النَّارِ يُسْجَرُوْنَۚ

fii **a**l*h*amiimi tsumma fii **al**nn*aa*ri yusjaruun**a**

ke dalam air yang sangat panas, kemudian mereka dibakar dalam api,

40:73

# ثُمَّ قِيْلَ لَهُمْ اَيْنَ مَا كُنْتُمْ تُشْرِكُوْنَۙ

tsumma qiila lahum ayna m*aa* kuntum tusyrikuun**a**

kemudian dikatakan kepada mereka, “Manakah berhala-berhala yang selalu kamu persekutukan,

40:74

# مِنْ دُوْنِ اللّٰهِ ۗقَالُوْا ضَلُّوْا عَنَّا بَلْ لَّمْ نَكُنْ نَّدْعُوْا مِنْ قَبْلُ شَيْـًٔاۚ كَذٰلِكَ يُضِلُّ اللّٰهُ الْكٰفِرِيْنَ

min duuni **al**l*aa*hi q*aa*luu *dh*alluu 'ann*aa* bal lam nakun nad'uu min qablu syay-an ka*dzaa*lika yu*dh*illu **al**l*aa*hu **a**lk*aa*firiin**a**

(yang kamu sembah) selain Allah?” Mereka menjawab, “Mereka telah hilang lenyap dari kami, bahkan kami dahulu tidak pernah menyembah sesuatu.” Demikianlah Allah membiarkan sesat orang-orang kafir.

40:75

# ذٰلِكُمْ بِمَا كُنْتُمْ تَفْرَحُوْنَ فِى الْاَرْضِ بِغَيْرِ الْحَقِّ وَبِمَا كُنْتُمْ تَمْرَحُوْنَ

*dzaa*likum bim*aa* kuntum tafra*h*uuna fii **a**l-ar*dh*i bighayri **a**l*h*aqqi wabim*aa* kuntum tamra*h*uun**a**

Yang demikian itu disebabkan karena kamu bersuka ria di bumi (tanpa) mengindahkan kebenaran dan karena kamu selalu bersuka ria (dalam kemaksiatan).

40:76

# اُدْخُلُوْٓا اَبْوَابَ جَهَنَّمَ خٰلِدِيْنَ فِيْهَا ۚفَبِئْسَ مَثْوَى الْمُتَكَبِّرِيْنَ

udkhuluu abw*aa*ba jahannama kh*aa*lidiina fiih*aa* fabi/sa matsw*aa* **a**lmutakabbiriin**a**

(Dikatakan kepada mereka), “Masuklah kamu ke pintu-pintu neraka Jahanam, dan kamu kekal di dalamnya. Maka itulah seburuk-buruk tempat bagi orang-orang yang sombong.”

40:77

# فَاصْبِرْ اِنَّ وَعْدَ اللّٰهِ حَقٌّ ۚفَاِمَّا نُرِيَنَّكَ بَعْضَ الَّذِيْ نَعِدُهُمْ اَوْ نَتَوَفَّيَنَّكَ فَاِلَيْنَا يُرْجَعُوْنَ

fa**i***sh*bir inna wa'da **al**l*aa*hi *h*aqqun fa-imm*aa* nuriyannaka ba'*dh*a **al**la*dz*ii na'iduhum aw natawaffayannaka fa-ilayn*aa* yurja'uun**a**

Maka bersabarlah engkau (Muhammad), sesungguhnya janji Allah itu benar. Meskipun Kami perlihatkan kepadamu sebagian siksa yang Kami ancamkan kepada mereka, atau pun Kami wafatkan engkau (sebelum ajal menimpa mereka), namun kepada Kamilah mereka dikembalik

40:78

# وَلَقَدْ اَرْسَلْنَا رُسُلًا مِّنْ قَبْلِكَ مِنْهُمْ مَّنْ قَصَصْنَا عَلَيْكَ وَمِنْهُمْ مَّنْ لَّمْ نَقْصُصْ عَلَيْكَ ۗوَمَا كَانَ لِرَسُوْلٍ اَنْ يَّأْتِيَ بِاٰيَةٍ اِلَّا بِاِذْنِ اللّٰهِ ۚفَاِذَا جَاۤءَ اَمْرُ اللّٰهِ قُضِيَ بِالْحَقِّ وَخَسِرَ هُنَال

walaqad arsaln*aa* rusulan min qablika minhum man qa*sh*a*sh*n*aa* 'alayka waminhum man lam naq*sh*u*sh* 'alayka wam*aa* k*aa*na lirasuulin an ya/tiya bi-*aa*yatin ill*aa* bi-i*dz*ni **al**

**Dan sungguh, Kami telah mengutus beberapa rasul sebelum engkau (Muhammad), di antara mereka ada yang Kami ceritakan kepadamu dan di antaranya ada (pula) yang tidak Kami ceritakan kepadamu. Tidak ada seorang rasul membawa suatu mukjizat, kecuali seizin All**









40:79

# اَللّٰهُ الَّذِيْ جَعَلَ لَكُمُ الْاَنْعَامَ لِتَرْكَبُوْا مِنْهَا وَمِنْهَا تَأْكُلُوْنَۖ

**al**l*aa*hu **al**la*dz*ii ja'ala lakumu **a**l-an'*aa*ma litarkabuu minh*aa* waminh*aa* ta/kuluun**a**

Allah-lah yang menjadikan hewan ternak untukmu, sebagian untuk kamu kendarai dan sebagian lagi kamu makan.

40:80

# وَلَكُمْ فِيْهَا مَنَافِعُ وَلِتَبْلُغُوْا عَلَيْهَا حَاجَةً فِيْ صُدُوْرِكُمْ وَعَلَيْهَا وَعَلَى الْفُلْكِ تُحْمَلُوْنَۗ

walakum fiih*aa* man*aa*fi'u walitablughuu 'alayh*aa* *haa*jatan fii *sh*uduurikum wa'alayh*aa* wa'al*aa* **a**lfulki tu*h*maluun**a**

Dan bagi kamu (ada lagi) manfaat-manfaat yang lain padanya (hewan ternak itu) dan agar kamu mencapai suatu keperluan (tujuan) yang tersimpan dalam hatimu (dengan mengendarainya). Dan dengan mengendarai binatang-binatang itu, dan di atas kapal mereka diang

40:81

# وَيُرِيْكُمْ اٰيٰتِهٖۖ فَاَيَّ اٰيٰتِ اللّٰهِ تُنْكِرُوْنَ

wayuriikum *aa*y*aa*tihi fa-ayya *aa*y*aa*ti **al**l*aa*hi tunkiruun**a**

Dan Dia memperlihatkan tanda-tanda (kebesaran-Nya) kepadamu. Lalu tanda-tanda (kebesaran) Allah yang mana yang kamu ingkari?

40:82

# اَفَلَمْ يَسِيْرُوْا فِى الْاَرْضِ فَيَنْظُرُوْا كَيْفَ كَانَ عَاقِبَةُ الَّذِيْنَ مِنْ قَبْلِهِمْ ۗ كَانُوْٓا اَكْثَرَ مِنْهُمْ وَاَشَدَّ قُوَّةً وَّاٰثَارًا فِى الْاَرْضِ فَمَآ اَغْنٰى عَنْهُمْ مَّا كَانُوْا يَكْسِبُوْنَ

afalam yasiiruu fii **a**l-ar*dh*i fayan*zh*uruu kayfa k*aa*na '*aa*qibatu **al**la*dz*iina min qablihim k*aa*nuu aktsara minhum wa-asyadda quwwatan wa*aa*ts*aa*ran fii **a**l-

Maka apakah mereka tidak mengadakan perjalanan di bumi, lalu mereka memperhatikan bagaimana kesudahan orang-orang yang sebelum mereka. Mereka itu lebih banyak dan lebih hebat kekuatannya serta (lebih banyak) peninggalan-peninggalan peradabannya di bumi, m

40:83

# فَلَمَّا جَاۤءَتْهُمْ رُسُلُهُمْ بِالْبَيِّنٰتِ فَرِحُوْا بِمَا عِنْدَهُمْ مِّنَ الْعِلْمِ وَحَاقَ بِهِمْ مَّا كَانُوْا بِهٖ يَسْتَهْزِءُوْنَ

falamm*aa* j*aa*-at-hum rusuluhum bi**a**lbayyin*aa*ti fari*h*uu bim*aa* 'indahum mina **a**l'ilmi wa*haa*qa bihim m*aa* k*aa*nuu bihi yastahzi-uun**a**

Maka ketika para rasul datang kepada mereka dengan membawa bukti-bukti yang nyata, mereka merasa senang dengan ilmu yang ada pada mereka dan mereka dikepung oleh (azab) yang dahulu mereka memperolok-olokkannya.

40:84

# فَلَمَّا رَاَوْا بَأْسَنَاۗ قَالُوْٓا اٰمَنَّا بِاللّٰهِ وَحْدَهٗ وَكَفَرْنَا بِمَا كُنَّا بِهٖ مُشْرِكِيْنَ

falamm*aa* ra-aw ba/san*aa* q*aa*luu *aa*mann*aa* bi**al**l*aa*hi wa*h*dahu wakafarn*aa* bim*aa* kunn*aa* bihi musyrikiin**a**

Maka ketika mereka melihat azab Kami, mereka berkata, “Kami hanya beriman kepada Allah saja dan kami ingkar kepada sembahan-sembahan yang telah kami persekutukan dengan Allah.”

40:85

# فَلَمْ يَكُ يَنْفَعُهُمْ اِيْمَانُهُمْ لَمَّا رَاَوْا بَأْسَنَا ۗسُنَّتَ اللّٰهِ الَّتِيْ قَدْ خَلَتْ فِيْ عِبَادِهِۚ وَخَسِرَ هُنَالِكَ الْكٰفِرُوْنَ ࣖ

falam yaku yanfa'uhum iim*aa*nuhum lamm*aa* ra-aw ba/san*aa* sunnata **al**l*aa*hi **al**latii qad khalat fii 'ib*aa*dihi wakhasira hun*aa*lika **a**lk*aa*firuun**a**

Maka iman mereka ketika mereka telah melihat azab Kami tidak berguna lagi bagi mereka. Itulah (ketentuan) Allah yang telah berlaku terhadap hamba-hamba-Nya. Dan ketika itu rugilah orang-orang kafir.

<!--EndFragment-->