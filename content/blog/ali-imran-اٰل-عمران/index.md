---
title: (3) Ali 'Imran - اٰل عمران
date: 2021-10-27T03:08:03.869Z
ayat: 3
description: "Jumlah Ayat: 200 / Arti: Keluarga Imran"
---
<!--StartFragment-->

3:1

# الۤمّۤ

alif-l*aa*m-miim

Alif Lam Mim.

3:2

# اَللّٰهُ لَآ اِلٰهَ اِلَّا هُوَ الْحَيُّ الْقَيُّوْمُۗ

**al**l*aa*hu l*aa* il*aa*ha ill*aa* huwa **a**l*h*ayyu **a**lqayyuum**u**

Allah, tidak ada tuhan selain Dia. Yang Mahahidup, Yang terus-menerus mengurus (makhluk-Nya).

3:3

# نَزَّلَ عَلَيْكَ الْكِتٰبَ بِالْحَقِّ مُصَدِّقًا لِّمَا بَيْنَ يَدَيْهِ وَاَنْزَلَ التَّوْرٰىةَ وَالْاِنْجِيْلَۙ

nazzala 'alayka **a**lkit*aa*ba bi**a**l*h*aqqi mu*sh*addiqan lim*aa* bayna yadayhi wa-anzala **al**ttawr*aa*ta wa**a**l-injiil**a**

Dia menurunkan Kitab (Al-Qur'an) kepadamu (Muhammad) yang mengandung kebenaran, membenarkan (kitab-kitab) sebelumnya, dan menurunkan Taurat dan Injil,

3:4

# مِنْ قَبْلُ هُدًى لِّلنَّاسِ وَاَنْزَلَ الْفُرْقَانَ ەۗ اِنَّ الَّذِيْنَ كَفَرُوْا بِاٰيٰتِ اللّٰهِ لَهُمْ عَذَابٌ شَدِيْدٌ ۗوَاللّٰهُ عَزِيْزٌ ذُو انْتِقَامٍۗ

min qablu hudan li**l**nn*aa*si wa-anzala **a**lfurq*aa*na inna **al**la*dz*iina kafaruu bi-*aa*y*aa*ti **al**l*aa*hi lahum 'a*dzaa*bun syadiidun wa**al**l

sebelumnya, sebagai petunjuk bagi manusia, dan Dia menurunkan Al-Furqan. Sungguh, orang-orang yang ingkar terhadap ayat-ayat Allah akan memperoleh azab yang berat. Allah Mahaperkasa lagi mempunyai hukuman.

3:5

# اِنَّ اللّٰهَ لَا يَخْفٰى عَلَيْهِ شَيْءٌ فِى الْاَرْضِ وَلَا فِى السَّمَاۤءِ

inna **al**l*aa*ha l*aa* yakhf*aa* 'alayhi syay-un fii **a**l-ar*dh*i wal*aa* fii **al**ssam*aa*/-**i**

Bagi Allah tidak ada sesuatu pun yang tersembunyi di bumi dan di langit.

3:6

# هُوَ الَّذِيْ يُصَوِّرُكُمْ فِى الْاَرْحَامِ كَيْفَ يَشَاۤءُ ۗ لَآ اِلٰهَ اِلَّا هُوَ الْعَزِيْزُ الْحَكِيْمُ

huwa **al**la*dz*ii yu*sh*awwirukum fii **a**l-ar*haa*mi kayfa yasy*aa*u l*aa* il*aa*ha ill*aa* huwa **a**l'aziizu **a**l*h*akiim**u**

Dialah yang membentuk kamu dalam rahim menurut yang Dia kehendaki. Tidak ada tuhan selain Dia. Yang Mahaperkasa, Mahabijaksana.

3:7

# هُوَ الَّذِيْٓ اَنْزَلَ عَلَيْكَ الْكِتٰبَ مِنْهُ اٰيٰتٌ مُّحْكَمٰتٌ هُنَّ اُمُّ الْكِتٰبِ وَاُخَرُ مُتَشٰبِهٰتٌ ۗ فَاَمَّا الَّذِيْنَ فِيْ قُلُوْبِهِمْ زَيْغٌ فَيَتَّبِعُوْنَ مَا تَشَابَهَ مِنْهُ ابْتِغَاۤءَ الْفِتْنَةِ وَابْتِغَاۤءَ تَأْوِيْلِهٖۚ وَمَا

huwa **al**la*dz*ii anzala 'alayka **a**lkit*aa*ba minhu *aa*y*aa*tun mu*h*kam*aa*tun hunna ummu **a**lkit*aa*bi waukharu mutasy*aa*bih*aa*tun fa-amm*aa* **al**

**Dialah yang menurunkan Kitab (Al-Qur'an) kepadamu (Muhammad). Di antaranya ada ayat-ayat yang muhkamat, itulah pokok-pokok Kitab (Al-Qur'an) dan yang lain mutasyabihat. Adapun orang-orang yang dalam hatinya condong pada kesesatan, mereka mengikuti yang mu**









3:8

# رَبَّنَا لَا تُزِغْ قُلُوْبَنَا بَعْدَ اِذْ هَدَيْتَنَا وَهَبْ لَنَا مِنْ لَّدُنْكَ رَحْمَةً ۚاِنَّكَ اَنْتَ الْوَهَّابُ

rabban*aa* l*aa* tuzigh quluuban*aa* ba'da i*dz* hadaytan*aa* wahab lan*aa* min ladunka ra*h*matan innaka anta **a**lwahh*aa*b**u**

(Mereka berdoa), “Ya Tuhan kami, janganlah Engkau condongkan hati kami kepada kesesatan setelah Engkau berikan petunjuk kepada kami, dan karuniakanlah kepada kami rahmat dari sisi-Mu, sesungguhnya Engkau Maha Pemberi.”

3:9

# رَبَّنَآ اِنَّكَ جَامِعُ النَّاسِ لِيَوْمٍ لَّا رَيْبَ فِيْهِ ۗاِنَّ اللّٰهَ لَا يُخْلِفُ الْمِيْعَادَ ࣖ

rabban*aa* innaka j*aa*mi'u **al**nn*aa*si liyawmin l*aa* rayba fiihi inna **al**l*aa*ha l*aa* yukhlifu **a**lmii'*aa*d**a**

”Ya Tuhan kami, Engkaulah yang mengumpulkan manusia pada hari yang tidak ada keraguan padanya.” Sungguh, Allah tidak menyalahi janji.

3:10

# اِنَّ الَّذِيْنَ كَفَرُوْا لَنْ تُغْنِيَ عَنْهُمْ اَمْوَالُهُمْ وَلَآ اَوْلَادُهُمْ مِّنَ اللّٰهِ شَيْـًٔا ۗوَاُولٰۤىِٕكَ هُمْ وَقُوْدُ النَّارِۗ

inna **al**la*dz*iina kafaruu lan tughniya 'anhum amw*aa*luhum wal*aa* awl*aa*duhum mina **al**l*aa*hi syay-an waul*aa*-ika hum waquudu **al**nn*aa*r**i**

Sesungguhnya orang-orang yang kafir, bagi mereka tidak akan berguna sedikit pun harta benda dan anak-anak mereka terhadap (azab) Allah. Dan mereka itu (menjadi) bahan bakar api neraka.

3:11

# كَدَأْبِ اٰلِ فِرْعَوْنَۙ وَالَّذِيْنَ مِنْ قَبْلِهِمْۗ كَذَّبُوْا بِاٰيٰتِنَاۚ فَاَخَذَهُمُ اللّٰهُ بِذُنُوْبِهِمْ ۗ وَاللّٰهُ شَدِيْدُ الْعِقَابِ

kada/bi *aa*li fir'awna wa**a**lla*dz*iina min qablihim ka*dzdz*abuu bi-*aa*y*aa*tin*aa* fa-akha*dz*ahumu **al**l*aa*hu bi*dz*unuubihim wa**al**l*aa*hu syadiidu

(Keadaan mereka) seperti keadaan pengikut Fir’aun dan orang-orang yang sebelum mereka. Mereka mendustakan ayat-ayat Kami, maka Allah menyiksa mereka disebabkan dosa-dosanya. Allah sangat berat hukuman-Nya.

3:12

# قُلْ لِّلَّذِيْنَ كَفَرُوْا سَتُغْلَبُوْنَ وَتُحْشَرُوْنَ اِلٰى جَهَنَّمَ ۗ وَبِئْسَ الْمِهَادُ

qul lilla*dz*iina kafaruu satughlabuuna watu*h*syaruuna il*aa* jahannama wabi/sa **a**lmih*aa*d**u**

Katakanlah (Muhammad) kepada orang-orang yang kafir, “Kamu (pasti) akan dikalahkan dan digiring ke dalam neraka Jahanam. Dan itulah seburuk-buruk tempat tinggal.”

3:13

# قَدْ كَانَ لَكُمْ اٰيَةٌ فِيْ فِئَتَيْنِ الْتَقَتَا ۗفِئَةٌ تُقَاتِلُ فِيْ سَبِيْلِ اللّٰهِ وَاُخْرٰى كَافِرَةٌ يَّرَوْنَهُمْ مِّثْلَيْهِمْ رَأْيَ الْعَيْنِ ۗوَاللّٰهُ يُؤَيِّدُ بِنَصْرِهٖ مَنْ يَّشَاۤءُ ۗ اِنَّ فِيْ ذٰلِكَ لَعِبْرَةً لِّاُولِى الْاَبْصَا

qad k*aa*na lakum *aa*yatun fii fi-atayni iltaqat*aa* fi-atun tuq*aa*tilu fii sabiili **al**l*aa*hi waukhr*aa* k*aa*firatun yarawnahum mitslayhim ra/ya **a**l'ayni wa**al**l*aa*

*Sungguh, telah ada tanda bagi kamu pada dua golongan yang berhadap-hadapan. Satu golongan berperang di jalan Allah dan yang lain (golongan) kafir yang melihat dengan mata kepala, bahwa mereka (golongan Muslim) dua kali lipat mereka. Allah menguatkan denga*









3:14

# زُيِّنَ لِلنَّاسِ حُبُّ الشَّهَوٰتِ مِنَ النِّسَاۤءِ وَالْبَنِيْنَ وَالْقَنَاطِيْرِ الْمُقَنْطَرَةِ مِنَ الذَّهَبِ وَالْفِضَّةِ وَالْخَيْلِ الْمُسَوَّمَةِ وَالْاَنْعَامِ وَالْحَرْثِ ۗ ذٰلِكَ مَتَاعُ الْحَيٰوةِ الدُّنْيَا ۗوَاللّٰهُ عِنْدَهٗ حُسْنُ الْمَاٰ

zuyyina li**l**nn*aa*si *h*ubbu **al**sysyahaw*aa*ti mina **al**nnis*aa*-i wa**a**lbaniina wa**a**lqan*aath*iiri **a**lmuqan*th*arati mina

Dijadikan terasa indah dalam pandangan manusia cinta terhadap apa yang diinginkan, berupa perempuan-perempuan, anak-anak, harta benda yang bertumpuk dalam bentuk emas dan perak, kuda pilihan, hewan ternak dan sawah ladang. Itulah kesenangan hidup di dunia







3:15

# ۞ قُلْ اَؤُنَبِّئُكُمْ بِخَيْرٍ مِّنْ ذٰلِكُمْ ۗ لِلَّذِيْنَ اتَّقَوْا عِنْدَ رَبِّهِمْ جَنّٰتٌ تَجْرِيْ مِنْ تَحْتِهَا الْاَنْهٰرُ خٰلِدِيْنَ فِيْهَا وَاَزْوَاجٌ مُّطَهَّرَةٌ وَّرِضْوَانٌ مِّنَ اللّٰهِ ۗ وَاللّٰهُ بَصِيْرٌۢ بِالْعِبَادِۚ

qul a-unabbi-ukum bikhayrin min *dzaa*likum lilla*dz*iina ittaqaw 'inda rabbihim jann*aa*tun tajrii min ta*h*tih*aa* **a**l-anh*aa*ru kh*aa*lidiina fiih*aa* wa-azw*aa*jun mu*th*ahharatun wari<

Katakanlah, “Maukah aku kabarkan kepadamu apa yang lebih baik dari yang demikian itu?” Bagi orang-orang yang bertakwa (tersedia) di sisi Tuhan mereka surga-surga yang mengalir di bawahnya sungai-sungai, mereka kekal di dalamnya, dan pasangan-pasangan yang

3:16

# اَلَّذِيْنَ يَقُوْلُوْنَ رَبَّنَآ اِنَّنَآ اٰمَنَّا فَاغْفِرْ لَنَا ذُنُوْبَنَا وَقِنَا عَذَابَ النَّارِۚ

**al**la*dz*iina yaquuluuna rabban*aa* innan*aa* *aa*mann*aa* fa**i**ghfir lan*aa* *dz*unuuban*aa* waqin*aa* 'a*dzaa*ba **al**nn*aa*r**i**

(Yaitu) orang-orang yang berdoa, “Ya Tuhan kami, kami benar-benar beriman, maka ampunilah dosa-dosa kami dan lindungilah kami dari azab neraka.”

3:17

# اَلصّٰبِرِيْنَ وَالصّٰدِقِيْنَ وَالْقٰنِتِيْنَ وَالْمُنْفِقِيْنَ وَالْمُسْتَغْفِرِيْنَ بِالْاَسْحَارِ

a**l***shshaa*biriina wa**al***shshaa*diqiina wa**a**lq*aa*nitiina wa**a**lmunfiqiina wa**a**lmustaghfiriina bi**a**l-as*haa*r**i**

(Juga) orang yang sabar, orang yang benar, orang yang taat, orang yang menginfakkan hartanya, dan orang yang memohon ampunan pada waktu sebelum fajar.

3:18

# شَهِدَ اللّٰهُ اَنَّهٗ لَآ اِلٰهَ اِلَّا هُوَۙ وَالْمَلٰۤىِٕكَةُ وَاُولُوا الْعِلْمِ قَاۤىِٕمًاۢ بِالْقِسْطِۗ لَآ اِلٰهَ اِلَّا هُوَ الْعَزِيْزُ الْحَكِيْمُ

syahida **al**l*aa*hu annahu l*aa* il*aa*ha ill*aa* huwa wa**a**lmal*aa*-ikatu wauluu **a**l'ilmi q*aa*-iman bi**a**lqis*th*i l*aa* il*aa*ha ill*aa* huwa

Allah menyatakan bahwa tidak ada tuhan selain Dia; (demikian pula) para malaikat dan orang berilmu yang menegakkan keadilan, tidak ada tuhan selain Dia, Yang Mahaperkasa, Maha-bijaksana.

3:19

# اِنَّ الدِّيْنَ عِنْدَ اللّٰهِ الْاِسْلَامُ ۗ وَمَا اخْتَلَفَ الَّذِيْنَ اُوْتُوا الْكِتٰبَ اِلَّا مِنْۢ بَعْدِ مَا جَاۤءَهُمُ الْعِلْمُ بَغْيًاۢ بَيْنَهُمْ ۗوَمَنْ يَّكْفُرْ بِاٰيٰتِ اللّٰهِ فَاِنَّ اللّٰهَ سَرِيْعُ الْحِسَابِ

inna **al**ddiina 'inda **al**l*aa*hi **a**l-isl*aa*mu wam*aa* ikhtalafa **al**la*dz*iina uutuu **a**lkit*aa*ba ill*aa* min ba'di m*aa* j*aa*-ahumu

Sesungguhnya agama di sisi Allah ialah Islam. Tidaklah berselisih orang-orang yang telah diberi Kitab kecuali setelah mereka memperoleh ilmu, karena kedengkian di antara mereka. Barangsiapa ingkar terhadap ayat-ayat Allah, maka sungguh, Allah sangat cepat

3:20

# فَاِنْ حَاۤجُّوْكَ فَقُلْ اَسْلَمْتُ وَجْهِيَ لِلّٰهِ وَمَنِ اتَّبَعَنِ ۗوَقُلْ لِّلَّذِيْنَ اُوْتُوا الْكِتٰبَ وَالْاُمِّيّٖنَ ءَاَسْلَمْتُمْ ۗ فَاِنْ اَسْلَمُوْا فَقَدِ اهْتَدَوْا ۚ وَاِنْ تَوَلَّوْا فَاِنَّمَا عَلَيْكَ الْبَلٰغُ ۗ وَاللّٰهُ بَصِيْرٌۢ ب

fa-in *haa*jjuuka faqul aslamtu wajhiya lill*aa*hi wamani ittaba'ani waqul lilla*dz*iina uutuu **a**lkit*aa*ba wa**a**l**\-**ummiyyiina a-aslamtum fa-in aslamuu faqadi ihtadaw wa-in tawallaw fa-inn

Kemudian jika mereka membantah engkau (Muhammad) katakanlah, “Aku berserah diri kepada Allah dan (demikian pula) orang-orang yang mengikutiku.” Dan katakanlah kepada orang-orang yang telah diberi Kitab dan kepada orang-orang buta huruf, ”Sudahkah kamu mas

3:21

# اِنَّ الَّذِيْنَ يَكْفُرُوْنَ بِاٰيٰتِ اللّٰهِ وَيَقْتُلُوْنَ النَّبِيّٖنَ بِغَيْرِحَقٍّۖ وَّيَقْتُلُوْنَ الَّذِيْنَ يَأْمُرُوْنَ بِالْقِسْطِ مِنَ النَّاسِۙ فَبَشِّرْهُمْ بِعَذَابٍ اَلِيْمٍ

inna **al**la*dz*iina yakfuruuna bi-*aa*y*aa*ti **al**l*aa*hi wayaqtuluuna **al**nnabiyyiina bighayri *h*aqqin wayaqtuluuna **al**la*dz*iina ya/muruuna bi**a**l

Sesungguhnya orang-orang yang mengingkari ayat-ayat Allah dan membunuh para nabi tanpa hak (alasan yang benar) dan membunuh orang-orang yang menyuruh manusia berbuat adil, sampaikanlah kepada mereka kabar gembira yaitu azab yang pedih.

3:22

# اُولٰۤىِٕكَ الَّذِيْنَ حَبِطَتْ اَعْمَالُهُمْ فِى الدُّنْيَا وَالْاٰخِرَةِ ۖ وَمَا لَهُمْ مِّنْ نّٰصِرِيْنَ

ul*aa*-ika **al**la*dz*iina *h*abi*th*at a'm*aa*luhum fii **al**dduny*aa* wa**a**l-*aa*khirati wam*aa* lahum min n*aas*iriin**a**

Mereka itulah orang-orang yang sia-sia pekerjaannya di dunia dan di akhirat, dan mereka tidak memperoleh penolong.

3:23

# اَلَمْ تَرَ اِلَى الَّذِيْنَ اُوْتُوْا نَصِيْبًا مِّنَ الْكِتٰبِ يُدْعَوْنَ اِلٰى كِتٰبِ اللّٰهِ لِيَحْكُمَ بَيْنَهُمْ ثُمَّ يَتَوَلّٰى فَرِيْقٌ مِّنْهُمْ وَهُمْ مُّعْرِضُوْنَ

alam tara il*aa* **al**la*dz*iina uutuu na*sh*iiban mina **a**lkit*aa*bi yud'awna il*aa* kit*aa*bi **al**l*aa*hi liya*h*kuma baynahum tsumma yatawall*aa* fariiqun minhum w

Tidakkah engkau memperhatikan orang-orang yang telah diberi bagian Kitab (Taurat)? Mereka diajak (berpegang) pada Kitab Allah untuk memutuskan (perkara) di antara mereka. Kemudian sebagian dari mereka berpaling seraya menolak (kebenaran).

3:24

# ذٰلِكَ بِاَنَّهُمْ قَالُوْا لَنْ تَمَسَّنَا النَّارُ اِلَّآ اَيَّامًا مَّعْدُوْدٰتٍ ۖ وَّغَرَّهُمْ فِيْ دِيْنِهِمْ مَّا كَانُوْا يَفْتَرُوْنَ

*dzaa*lika bi-annahum q*aa*luu lan tamassan*aa* **al**nn*aa*ru ill*aa* ayy*aa*man ma'duud*aa*tin wagharrahum fii diinihim m*aa* k*aa*nuu yaftaruun**a**

Hal itu adalah karena mereka berkata, “Api neraka tidak akan menyentuh kami kecuali beberapa hari saja.” Mereka teperdaya dalam agama mereka oleh apa yang mereka ada-adakan.

3:25

# فَكَيْفَ اِذَا جَمَعْنٰهُمْ لِيَوْمٍ لَّا رَيْبَ فِيْهِۗ وَوُفِّيَتْ كُلُّ نَفْسٍ مَّا كَسَبَتْ وَهُمْ لَا يُظْلَمُوْنَ

fakayfa i*dzaa* jama'n*aa*hum liyawmin l*aa* rayba fiihi wawuffiyat kullu nafsin m*aa* kasabat wahum l*aa* yu*zh*lamuun**a**

Bagaimana jika (nanti) mereka Kami kumpulkan pada hari (Kiamat) yang tidak diragukan terjadinya dan kepada setiap jiwa diberi balasan yang sempurna sesuai dengan apa yang telah dikerjakannya dan mereka tidak dizalimi (dirugikan)?

3:26

# قُلِ اللهم مٰلِكَ الْمُلْكِ تُؤْتِى الْمُلْكَ مَنْ تَشَاۤءُ وَتَنْزِعُ الْمُلْكَ مِمَّنْ تَشَاۤءُۖ وَتُعِزُّ مَنْ تَشَاۤءُ وَتُذِلُّ مَنْ تَشَاۤءُ ۗ بِيَدِكَ الْخَيْرُ ۗ اِنَّكَ عَلٰى كُلِّ شَيْءٍ قَدِيْرٌ

quli **al**l*aa*humma m*aa*lika **a**lmulki tu/tii **a**lmulka man tasy*aa*u watanzi'u **a**lmulka mimman tasy*aa*u watu'izzu man tasy*aa*u watu*dz*illu man tasy*aa*u b

Katakanlah (Muhammad), “Wahai Tuhan pemilik kekuasaan, Engkau berikan kekuasaan kepada siapa pun yang Engkau kehendaki, dan Engkau cabut kekuasaan dari siapa pun yang Engkau kehendaki. Engkau muliakan siapa pun yang Engkau kehendaki dan Engkau hinakan sia

3:27

# تُوْلِجُ الَّيْلَ فِى النَّهَارِ وَتُوْلِجُ النَّهَارَ فِى الَّيْلِ وَتُخْرِجُ الْحَيَّ مِنَ الْمَيِّتِ وَتُخْرِجُ الْمَيِّتَ مِنَ الْحَيِّ وَتَرْزُقُ مَنْ تَشَاۤءُ بِغَيْرِ حِسَابٍ

tuuliju **al**layla fii **al**nnah*aa*ri watuuliju **al**nnah*aa*ra fii **al**layli watukhriju **a**l*h*ayya mina **a**lmayyiti watukhriju **a**lmayy

Engkau masukkan malam ke dalam siang dan Engkau masukkan siang ke dalam malam. Dan Engkau keluarkan yang hidup dari yang mati, dan Engkau keluarkan yang mati dari yang hidup. Dan Engkau berikan rezeki kepada siapa yang Engkau kehendaki tanpa perhitungan.”

3:28

# لَا يَتَّخِذِ الْمُؤْمِنُوْنَ الْكٰفِرِيْنَ اَوْلِيَاۤءَ مِنْ دُوْنِ الْمُؤْمِنِيْنَۚ وَمَنْ يَّفْعَلْ ذٰلِكَ فَلَيْسَ مِنَ اللّٰهِ فِيْ شَيْءٍ اِلَّآ اَنْ تَتَّقُوْا مِنْهُمْ تُقٰىةً ۗ وَيُحَذِّرُكُمُ اللّٰهُ نَفْسَهٗ ۗ وَاِلَى اللّٰهِ الْمَصِيْرُ

l*aa* yattakhi*dz*i **a**lmu/minuuna **a**lk*aa*firiina awliy*aa*-a min duuni **a**lmu/miniina waman yaf'al *dzaa*lika falaysa mina **al**l*aa*hi fii syay-in ill*aa* an

Janganlah orang-orang beriman menjadikan orang kafir sebagai pemimpin, melainkan orang-orang beriman. Barang siapa berbuat demikian, niscaya dia tidak akan memperoleh apa pun dari Allah, kecuali karena (siasat) menjaga diri dari sesuatu yang kamu takuti d

3:29

# قُلْ اِنْ تُخْفُوْا مَا فِيْ صُدُوْرِكُمْ اَوْ تُبْدُوْهُ يَعْلَمْهُ اللّٰهُ ۗوَيَعْلَمُ مَا فِى السَّمٰوٰتِ وَمَا فِى الْاَرْضِۗ وَاللّٰهُ عَلٰى كُلِّ شَيْءٍ قَدِيْرٌ

qul in tukhfuu m*aa* fii *sh*uduurikum aw tubduuhu ya'lamhu **al**l*aa*hu waya'lamu m*aa* fii **al**ssam*aa*w*aa*ti wam*aa* fii **a**l-ar*dh*i wa**al**l*aa*hu

Katakanlah, “Jika kamu sembunyikan apa yang ada dalam hatimu atau kamu nyatakan, Allah pasti mengetahuinya.” Dia mengetahui apa yang ada di langit dan apa yang ada di bumi. Allah Mahakuasa atas segala sesuatu.

3:30

# يَوْمَ تَجِدُ كُلُّ نَفْسٍ مَّا عَمِلَتْ مِنْ خَيْرٍ مُّحْضَرًا ۛوَمَا عَمِلَتْ مِنْ سُوْۤءٍ ۛ تَوَدُّ لَوْ اَنَّ بَيْنَهَا وَبَيْنَهٗٓ اَمَدًاۢ بَعِيْدًا ۗوَيُحَذِّرُكُمُ اللّٰهُ نَفْسَهٗ ۗوَاللّٰهُ رَءُوْفٌۢ بِالْعِبَادِ ࣖ

yawma tajidu kullu nafsin m*aa* 'amilat min khayrin mu*hd*aran wam*aa* 'amilat min suu-in tawaddu law anna baynah*aa* wabaynahu amadan ba'iidan wayu*h*a*dzdz*irukumu **al**l*aa*hu nafsahu wa**al**

**(Ingatlah) pada hari (ketika) setiap jiwa mendapatkan (balasan) atas kebajikan yang telah dikerjakan dihadapkan kepadanya, (begitu juga balasan) atas kejahatan yang telah dia kerjakan. Dia berharap sekiranya ada jarak yang jauh antara dia dengan (hari) it**









3:31

# قُلْ اِنْ كُنْتُمْ تُحِبُّوْنَ اللّٰهَ فَاتَّبِعُوْنِيْ يُحْبِبْكُمُ اللّٰهُ وَيَغْفِرْ لَكُمْ ذُنُوْبَكُمْ ۗ وَاللّٰهُ غَفُوْرٌ رَّحِيْمٌ

qul in kuntum tu*h*ibbuuna **al**l*aa*ha fa**i**ttabi'uunii yu*h*bibkumu **al**l*aa*hu wayaghfir lakum *dz*unuubakum wa**al**l*aa*hu ghafuurun ra*h*iim**un**

**Katakanlah (Muhammad), “Jika kamu mencintai Allah, ikutilah aku, niscaya Allah mencintaimu dan mengampuni dosa-dosamu.” Allah Maha Pengampun, Maha Penyayang.**









3:32

# قُلْ اَطِيْعُوا اللّٰهَ وَالرَّسُوْلَ ۚ فَاِنْ تَوَلَّوْا فَاِنَّ اللّٰهَ لَا يُحِبُّ الْكٰفِرِيْنَ

qul a*th*ii'uu **al**l*aa*ha wa**al**rrasuula fa-in tawallaw fa-inna **al**l*aa*ha l*aa* yu*h*ibbu **a**lk*aa*firiin**a**

Katakanlah (Muhammad), “Taatilah Allah dan Rasul. Jika kamu berpaling, ketahuilah bahwa Allah tidak menyukai orang-orang kafir.”

3:33

# ۞ اِنَّ اللّٰهَ اصْطَفٰىٓ اٰدَمَ وَنُوْحًا وَّاٰلَ اِبْرٰهِيْمَ وَاٰلَ عِمْرَانَ عَلَى الْعٰلَمِيْنَۙ

inna **al**l*aa*ha i*sth*af*aa* *aa*dama wanuu*h*an wa*aa*la ibr*aa*hiima wa*aa*la 'imr*aa*na 'al*aa* **a**l'*aa*lamiin**a**

Sesungguhnya Allah telah memilih Adam, Nuh, keluarga Ibrahim dan keluarga Imran melebihi segala umat (pada masa masing-masing),

3:34

# ذُرِّيَّةً ۢ بَعْضُهَا مِنْۢ بَعْضٍۗ وَاللّٰهُ سَمِيْعٌ عَلِيْمٌۚ

*dz*urriyyatan ba'*dh*uh*aa* min ba'*dh*in wa**al**l*aa*hu samii'un 'aliim**un**

(sebagai) satu keturunan, sebagiannya adalah (keturunan) dari sebagian yang lain. Allah Maha Mendengar, Maha Mengetahui.

3:35

# اِذْ قَالَتِ امْرَاَتُ عِمْرَانَ رَبِّ اِنِّيْ نَذَرْتُ لَكَ مَا فِيْ بَطْنِيْ مُحَرَّرًا فَتَقَبَّلْ مِنِّيْ ۚ اِنَّكَ اَنْتَ السَّمِيْعُ الْعَلِيْمُ

i*dz* q*aa*lati imra-atu 'imr*aa*na rabbi innii na*dz*artu laka m*aa* fii ba*th*nii mu*h*arraran fataqabbal minnii innaka anta **al**ssamii'u **a**l'aliim**u**

(Ingatlah), ketika istri Imran berkata, “Ya Tuhanku, sesungguhnya aku bernazar kepada-Mu, apa (janin) yang dalam kandunganku (kelak) menjadi hamba yang mengabdi (kepada-Mu), maka terimalah (nazar itu) dariku. Sungguh, Engkaulah Yang Maha Mendengar, Maha M

3:36

# فَلَمَّا وَضَعَتْهَا قَالَتْ رَبِّ اِنِّيْ وَضَعْتُهَآ اُنْثٰىۗ وَاللّٰهُ اَعْلَمُ بِمَا وَضَعَتْۗ وَلَيْسَ الذَّكَرُ كَالْاُنْثٰى ۚ وَاِنِّيْ سَمَّيْتُهَا مَرْيَمَ وَاِنِّيْٓ اُعِيْذُهَا بِكَ وَذُرِّيَّتَهَا مِنَ الشَّيْطٰنِ الرَّجِيْمِ

falamm*aa* wa*dh*a'at-h*aa* q*aa*lat rabbi innii wa*dh*a'tuh*aa* unts*aa* wa**al**l*aa*hu a'lamu bim*aa* wa*dh*a'at walaysa **al***dzdz*akaru ka**a**lunts*aa*

*Maka ketika melahirkannya, dia berkata, “Ya Tuhanku, aku telah melahirkan anak perempuan.” Padahal Allah lebih tahu apa yang dia lahirkan, dan laki-laki tidak sama dengan perempuan. ”Dan aku memberinya nama Maryam, dan aku mohon perlindungan-Mu untuknya d*









3:37

# فَتَقَبَّلَهَا رَبُّهَا بِقَبُوْلٍ حَسَنٍ وَّاَنْۢبَتَهَا نَبَاتًا حَسَنًاۖ وَّكَفَّلَهَا زَكَرِيَّا ۗ كُلَّمَا دَخَلَ عَلَيْهَا زَكَرِيَّا الْمِحْرَابَۙ وَجَدَ عِنْدَهَا رِزْقًا ۚ قَالَ يٰمَرْيَمُ اَنّٰى لَكِ هٰذَا ۗ قَالَتْ هُوَ مِنْ عِنْدِ اللّٰهِ ۗ اِ

fataqabbalah*aa* rabbuh*aa* biqabuulin *h*asanin wa-anbatah*aa* nab*aa*tan *h*asanan wakaffalah*aa* zakariyy*aa* kullam*aa* dakhala 'alayh*aa* zakariyy*aa* **a**lmi*h*r*aa*ba wa

Maka Dia (Allah) menerimanya dengan penerimaan yang baik, membesarkannya dengan pertumbuhan yang baik dan menyerahkan pemeliharaannya kepada Zakaria. Setiap kali Zakaria masuk menemuinya di mihrab (kamar khusus ibadah), dia dapati makanan di sisinya. Dia

3:38

# هُنَالِكَ دَعَا زَكَرِيَّا رَبَّهٗ ۚ قَالَ رَبِّ هَبْ لِيْ مِنْ لَّدُنْكَ ذُرِّيَّةً طَيِّبَةً ۚ اِنَّكَ سَمِيْعُ الدُّعَاۤءِ

hun*aa*lika da'*aa* zakariyy*aa* rabbahu q*aa*la rabbi hab lii min ladunka *dz*urriyyatan *th*ayyibatan innaka samii'u **al**ddu'*aa*/-**i**

Di sanalah Zakaria berdoa kepada Tuhannya. Dia berkata, “Ya Tuhanku, berilah aku keturunan yang baik dari sisi-Mu, sesungguhnya Engkau Maha Mendengar doa.”

3:39

# فَنَادَتْهُ الْمَلٰۤىِٕكَةُ وَهُوَ قَاۤىِٕمٌ يُّصَلِّيْ فِى الْمِحْرَابِۙ اَنَّ اللّٰهَ يُبَشِّرُكَ بِيَحْيٰى مُصَدِّقًاۢ بِكَلِمَةٍ مِّنَ اللّٰهِ وَسَيِّدًا وَّحَصُوْرًا وَّنَبِيًّا مِّنَ الصّٰلِحِيْنَ

fan*aa*dat-hu **a**lmal*aa*-ikatu wahuwa q*aa*-imun yu*sh*allii fii **a**lmi*h*r*aa*bi anna **al**l*aa*ha yubasysyiruka biya*h*y*aa* mu*sh*addiqan bikalimatin mina

Kemudian para malaikat memanggilnya, ketika dia berdiri melaksanakan salat di mihrab, “Allah menyampaikan kabar gembira kepadamu dengan (kelahiran) Yahya, yang membenarkan sebuah kalimat (firman) dari Allah, panutan, berkemampuan menahan diri (dari hawa n

3:40

# قَالَ رَبِّ اَنّٰى يَكُوْنُ لِيْ غُلٰمٌ وَّقَدْ بَلَغَنِيَ الْكِبَرُ وَامْرَاَتِيْ عَاقِرٌ ۗ قَالَ كَذٰلِكَ اللّٰهُ يَفْعَلُ مَا يَشَاۤءُ

q*aa*la rabbi ann*aa* yakuunu lii ghul*aa*mun waqad balaghaniya **a**lkibaru wa**i**mra-atii '*aa*qirun q*aa*la ka*dzaa*lika **al**l*aa*hu yaf'alu m*aa* yasy*aa*/**u**

Dia (Zakaria) berkata, “Ya Tuhanku, bagaimana aku bisa mendapat anak sedang aku sudah sangat tua dan istriku pun mandul?” Dia (Allah) berfirman, “Demikianlah, Allah berbuat apa yang Dia kehendaki.”







3:41

# قَالَ رَبِّ اجْعَلْ لِّيْٓ اٰيَةً ۗ قَالَ اٰيَتُكَ اَلَّا تُكَلِّمَ النَّاسَ ثَلٰثَةَ اَيَّامٍ اِلَّا رَمْزًا ۗ وَاذْكُرْ رَّبَّكَ كَثِيْرًا وَّسَبِّحْ بِالْعَشِيِّ وَالْاِبْكَارِ ࣖ

q*aa*la rabbi ij'al lii *aa*yatan q*aa*la *aa*yatuka **al**l*aa* tukallima **al**nn*aa*sa tsal*aa*tsata ayy*aa*min ill*aa* ramzan wa**u***dz*kur rabbaka katsiiran was

Dia (Zakaria) berkata, “Ya Tuhanku, berilah aku suatu tanda.” Allah berfirman, “Tanda bagimu, adalah bahwa engkau tidak berbicara dengan manusia selama tiga hari, kecuali dengan isyarat. Dan sebutlah (nama) Tuhanmu banyak-banyak, dan bertasbihlah (memuji-

3:42

# وَاِذْ قَالَتِ الْمَلٰۤىِٕكَةُ يٰمَرْيَمُ اِنَّ اللّٰهَ اصْطَفٰىكِ وَطَهَّرَكِ وَاصْطَفٰىكِ عَلٰى نِسَاۤءِ الْعٰلَمِيْنَ

wa-i*dz* q*aa*lati **a**lmal*aa*-ikatu y*aa* maryamu inna **al**l*aa*ha i*sth*af*aa*ki wa*th*ahharaki wa**i***sth*af*aa*ki 'al*aa* nis*aa*-i **a**

**Dan (ingatlah) ketika para malaikat berkata, “Wahai Maryam! Sesungguhnya Allah telah memilihmu, menyucikanmu, dan melebihkanmu di atas segala perempuan di seluruh alam (pada masa itu).**









3:43

# يٰمَرْيَمُ اقْنُتِيْ لِرَبِّكِ وَاسْجُدِيْ وَارْكَعِيْ مَعَ الرَّاكِعِيْنَ

y*aa* maryamu uqnutii lirabbiki wa**u**sjudii wa**i**rka'ii ma'a **al**rr*aa*ki'iin**a**

Wahai Maryam! Taatilah Tuhanmu, sujud dan rukuklah bersama orang-orang yang rukuk.”

3:44

# ذٰلِكَ مِنْ اَنْۢبَاۤءِ الْغَيْبِ نُوْحِيْهِ اِلَيْكَ ۗوَمَا كُنْتَ لَدَيْهِمْ اِذْ يُلْقُوْنَ اَقْلَامَهُمْ اَيُّهُمْ يَكْفُلُ مَرْيَمَۖ وَمَا كُنْتَ لَدَيْهِمْ اِذْ يَخْتَصِمُوْنَ

*dzaa*lika min anb*aa*-i **a**lghaybi nuu*h*iihi ilayka wam*aa* kunta ladayhim i*dz* yulquuna aql*aa*mahum ayyuhum yakfulu maryama wam*aa* kunta ladayhim i*dz* yakhta*sh*imuun**a**

Itulah sebagian dari berita-berita gaib yang Kami wahyukan kepadamu (Muhammad), padahal engkau tidak bersama mereka ketika mereka melemparkan pena mereka (untuk mengundi) siapa di antara mereka yang akan memelihara Maryam. Dan engkau pun tidak bersama mer

3:45

# اِذْ قَالَتِ الْمَلٰۤىِٕكَةُ يٰمَرْيَمُ اِنَّ اللّٰهَ يُبَشِّرُكِ بِكَلِمَةٍ مِّنْهُۖ اسْمُهُ الْمَسِيْحُ عِيْسَى ابْنُ مَرْيَمَ وَجِيْهًا فِى الدُّنْيَا وَالْاٰخِرَةِ وَمِنَ الْمُقَرَّبِيْنَۙ

i*dz* q*aa*lati **a**lmal*aa*-ikatu y*aa* maryamu inna **al**l*aa*ha yubasysyiruki bikalimatin minhu ismuhu **a**lmasii*h*u 'iis*aa* ibnu maryama wajiihan fii **al**ddu

(Ingatlah), ketika para malaikat berkata, “Wahai Maryam! Sesungguhnya Allah menyampaikan kabar gembira kepadamu tentang sebuah kalimat (fir-man) dari-Nya (yaitu seorang putra), namanya Al-Masih Isa putra Maryam, seorang terkemuka di dunia dan di akhirat,

3:46

# وَيُكَلِّمُ النَّاسَ فِى الْمَهْدِ وَكَهْلًا وَّمِنَ الصّٰلِحِيْنَ

wayukallimu **al**nn*aa*sa fii **a**lmahdi wakahlan wamina **al***shshaa*li*h*iin**a**

dan dia berbicara dengan manusia (sewaktu) dalam buaian dan ketika sudah dewasa, dan dia termasuk di antara orang-orang saleh.”

3:47

# قَالَتْ رَبِّ اَنّٰى يَكُوْنُ لِيْ وَلَدٌ وَّلَمْ يَمْسَسْنِيْ بَشَرٌ ۗ قَالَ كَذٰلِكِ اللّٰهُ يَخْلُقُ مَا يَشَاۤءُ ۗاِذَا قَضٰٓى اَمْرًا فَاِنَّمَا يَقُوْلُ لَهٗ كُنْ فَيَكُوْنُ

q*aa*lat rabbi ann*aa* yakuunu lii waladun walam yamsasnii basyarun q*aa*la ka*dzaa*liki **al**l*aa*hu yakhluqu m*aa* yasy*aa*u i*dzaa* qa*daa* amran fa-innam*aa* yaquulu lahu kun fayakuun

Dia (Maryam) berkata, “Ya Tuhanku, bagaimana mungkin aku akan mempunyai anak, padahal tidak ada seorang laki-laki pun yang menyentuhku?” Dia (Allah) berfirman, “Demikianlah Allah menciptakan apa yang Dia kehendaki. Apabila Dia hendak menetapkan sesuatu, D

3:48

# وَيُعَلِّمُهُ الْكِتٰبَ وَالْحِكْمَةَ وَالتَّوْرٰىةَ وَالْاِنْجِيْلَۚ

wayu'allimuhu **a**lkit*aa*ba wa**a**l*h*ikmata wa**al**ttawr*aa*ta wa**a**l-injiil**a**

Dan Dia (Allah) mengajarkan kepadanya (Isa) Kitab, Hikmah, Taurat, dan Injil.

3:49

# وَرَسُوْلًا اِلٰى بَنِيْٓ اِسْرَاۤءِيْلَ ەۙ اَنِّيْ قَدْ جِئْتُكُمْ بِاٰيَةٍ مِّنْ رَّبِّكُمْ ۙاَنِّيْٓ اَخْلُقُ لَكُمْ مِّنَ الطِّيْنِ كَهَيْـَٔةِ الطَّيْرِ فَاَنْفُخُ فِيْهِ فَيَكُوْنُ طَيْرًاۢ بِاِذْنِ اللّٰهِ ۚوَاُبْرِئُ الْاَكْمَهَ وَالْاَبْرَصَ وَاُ

warasuulan il*aa* banii isr*aa*-iila annii qad ji/tukum bi-*aa*yatin min rabbikum annii akhluqu lakum mina **al***ththh*iini kahay-ati **al***ththh*ayri fa-anfukhu fiihi fayakuunu *th*ayran bi-i*dz*

Dan sebagai Rasul kepada Bani Israil (dia berkata), “Aku telah datang kepada kamu dengan sebuah tanda (mukjizat) dari Tuhanmu, yaitu aku membuatkan bagimu (sesuatu) dari tanah berbentuk seperti burung, lalu aku meniupnya, maka ia menjadi seekor burung den







3:50

# وَمُصَدِّقًا لِّمَا بَيْنَ يَدَيَّ مِنَ التَّوْرٰىةِ وَلِاُحِلَّ لَكُمْ بَعْضَ الَّذِيْ حُرِّمَ عَلَيْكُمْ وَجِئْتُكُمْ بِاٰيَةٍ مِّنْ رَّبِّكُمْۗ فَاتَّقُوا اللّٰهَ وَاَطِيْعُوْنِ

wamu*sh*addiqan lim*aa* bayna yadayya mina **al**ttawr*aa*ti wali-u*h*illa lakum ba'*dh*a **al**la*dz*ii *h*urrima 'alaykum waji/tukum bi-*aa*yatin min rabbikum fa**i**ttaquu <

Dan sebagai seorang yang membenarkan Taurat yang datang sebelumku, dan agar aku menghalalkan bagi kamu sebagian dari yang telah diharamkan untukmu. Dan aku datang kepadamu membawa suatu tanda (mukjizat) dari Tuhanmu. Karena itu, bertakwalah kepada Allah d

3:51

# اِنَّ اللّٰهَ رَبِّيْ وَرَبُّكُمْ فَاعْبُدُوْهُ ۗهٰذَا صِرَاطٌ مُّسْتَقِيْمٌ

inna **al**l*aa*ha rabbii warabbukum fa**u**'buduuhu h*aadzaa* *sh*ir*aath*un mustaqiim**un**

Sesungguhnya Allah itu Tuhanku dan Tuhanmu, karena itu sembahlah Dia. Inilah jalan yang lurus.”

3:52

# ۞ فَلَمَّآ اَحَسَّ عِيْسٰى مِنْهُمُ الْكُفْرَ قَالَ مَنْ اَنْصَارِيْٓ اِلَى اللّٰهِ ۗ قَالَ الْحَوَارِيُّوْنَ نَحْنُ اَنْصَارُ اللّٰهِ ۚ اٰمَنَّا بِاللّٰهِ ۚ وَاشْهَدْ بِاَنَّا مُسْلِمُوْنَ

falamm*aa* a*h*assa 'iis*aa* minhumu **a**lkufra q*aa*la man an*shaa*rii il*aa* **al**l*aa*hi q*aa*la **a**l*h*aw*aa*riyyuuna na*h*nu an*shaa*ru **al<**

Maka ketika Isa merasakan keingkaran mereka (Bani Israil), dia berkata, “Siapakah yang akan menjadi penolong untuk (menegakkan agama) Allah?” Para Hawariyyun (sahabat setianya) menjawab, “Kamilah penolong (agama) Allah. Kami beriman kepada Allah, dan saks







3:53

# رَبَّنَآ اٰمَنَّا بِمَآ اَنْزَلْتَ وَاتَّبَعْنَا الرَّسُوْلَ فَاكْتُبْنَا مَعَ الشّٰهِدِيْنَ

rabban*aa* *aa*mann*aa* bim*aa* anzalta wa**i**ttaba'n*aa* **al**rrasuula fa**u**ktubn*aa* ma'a **al**sysy*aa*hidiin**a**

Ya Tuhan kami, kami telah beriman kepada apa yang Engkau turunkan dan kami telah mengikuti Rasul, karena itu tetapkanlah kami bersama golongan orang yang memberikan kesaksian.”

3:54

# وَمَكَرُوْا وَمَكَرَ اللّٰهُ ۗوَاللّٰهُ خَيْرُ الْمَاكِرِيْنَ ࣖ

wamakaruu wamakara **al**l*aa*hu wa**al**l*aa*hu khayru **a**lm*aa*kir

Dan mereka (orang-orang kafir) membuat tipu daya, maka Allah pun membalas tipu daya. Dan Allah sebaik-baik pembalas tipu daya.

3:55

# اِذْ قَالَ اللّٰهُ يٰعِيْسٰٓى اِنِّيْ مُتَوَفِّيْكَ وَرَافِعُكَ اِلَيَّ وَمُطَهِّرُكَ مِنَ الَّذِيْنَ كَفَرُوْا وَجَاعِلُ الَّذِيْنَ اتَّبَعُوْكَ فَوْقَ الَّذِيْنَ كَفَرُوْٓا اِلٰى يَوْمِ الْقِيٰمَةِ ۚ ثُمَّ اِلَيَّ مَرْجِعُكُمْ فَاَحْكُمُ بَيْنَكُمْ فِيْ

i*dz* q*aa*la **al**l*aa*hu y*aa* 'iis*aa* innii mutawaffiika war*aa*fi'uka ilayya wamu*th*ahhiruka mina **al**la*dz*iina kafaruu waj*aa*'ilu **al**la*dz*iina ittaba'

(Ingatlah), ketika Allah berfirman, “Wahai Isa! Aku mengambilmu dan mengangkatmu kepada-Ku, serta menyucikanmu dari orang-orang yang kafir, dan menjadikan orang-orang yang mengikutimu di atas orang-orang yang kafir hingga hari Kiamat. Kemudian kepada-Ku e

3:56

# فَاَمَّا الَّذِيْنَ كَفَرُوْا فَاُعَذِّبُهُمْ عَذَابًا شَدِيْدًا فِى الدُّنْيَا وَالْاٰخِرَةِۖ وَمَا لَهُمْ مِّنْ نّٰصِرِيْنَ

fa-amm*aa* **al**la*dz*iina kafaruu fau'a*dzdz*ibuhum 'a*dzaa*ban syadiidan fii **al**dduny*aa* wa**a**l-*aa*khirati wam*aa* lahum min n*aas*iriin**a**

Maka adapun orang-orang yang kafir, maka akan Aku azab mereka dengan azab yang sangat keras di dunia dan di akhirat, sedang mereka tidak memperoleh penolong.

3:57

# وَاَمَّا الَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِ فَيُوَفِّيْهِمْ اُجُوْرَهُمْ ۗ وَاللّٰهُ لَا يُحِبُّ الظّٰلِمِيْنَ

wa-amm*aa* **al**la*dz*iina *aa*manuu wa'amiluu **al***shshaa*li*haa*ti fayuwaffiihim ujuurahum wa**al**l*aa*hu l*aa* yu*h*ibbu **al***zhzhaa*limiin**a<**

Dan adapun orang yang beriman dan melakukan kebajikan, maka Dia akan memberikan pahala kepada mereka dengan sempurna. Dan Allah tidak menyukai orang zalim.







3:58

# ذٰلِكَ نَتْلُوْهُ عَلَيْكَ مِنَ الْاٰيٰتِ وَالذِّكْرِ الْحَكِيْمِ

*dzaa*lika natluuhu 'alayka mina **a**l-*aa*y*aa*ti wa**al***dzdz*ikri **a**l*h*akiim**i**

Demikianlah Kami bacakan kepadamu (Muhammad) sebagian ayat-ayat dan peringatan yang penuh hikmah.

3:59

# اِنَّ مَثَلَ عِيْسٰى عِنْدَ اللّٰهِ كَمَثَلِ اٰدَمَ ۗ خَلَقَهٗ مِنْ تُرَابٍ ثُمَّ قَالَ لَهٗ كُنْ فَيَكُوْنُ

inna matsala 'iis*aa* 'inda **al**l*aa*hi kamatsali *aa*dama khalaqahu min tur*aa*bin tsumma q*aa*la lahu kun fayakuun**u**

Sesungguhnya perumpamaan (penciptaan) Isa bagi Allah, seperti (penciptaan) Adam. Dia menciptakannya dari tanah, kemudian Dia berkata kepadanya, “Jadilah!” Maka jadilah sesuatu itu.

3:60

# اَلْحَقُّ مِنْ رَّبِّكَ فَلَا تَكُنْ مِّنَ الْمُمْتَرِيْنَ

al*h*aqqu min rabbika fal*aa* takun mina **a**lmumtariin**a**

Kebenaran itu dari Tuhanmu, karena itu janganlah engkau (Muhammad) termasuk orang-orang yang ragu.

3:61

# فَمَنْ حَاۤجَّكَ فِيْهِ مِنْۢ بَعْدِ مَا جَاۤءَكَ مِنَ الْعِلْمِ فَقُلْ تَعَالَوْا نَدْعُ اَبْنَاۤءَنَا وَاَبْنَاۤءَكُمْ وَنِسَاۤءَنَا وَنِسَاۤءَكُمْ وَاَنْفُسَنَا وَاَنْفُسَكُمْۗ ثُمَّ نَبْتَهِلْ فَنَجْعَلْ لَّعْنَتَ اللّٰهِ عَلَى الْكٰذِبِيْنَ

faman *haa*jjaka fiihi min ba'di m*aa* j*aa*-aka mina **a**l'ilmi faqul ta'*aa*law nad'u abn*aa*-an*aa* wa-abn*aa*-akum wanis*aa*-an*aa* wanis*aa*-akum wa-anfusan*aa* wa-anfusakum tsumma

Siapa yang membantahmu dalam hal ini setelah engkau memperoleh ilmu, katakanlah (Muhammad), “Marilah kita panggil anak-anak kami dan anak-anak kamu, istri-istri kami dan istri-istrimu, kami sendiri dan kamu juga, kemudian marilah kita bermubahalah agar la

3:62

# اِنَّ هٰذَا لَهُوَ الْقَصَصُ الْحَقُّ ۚ وَمَا مِنْ اِلٰهٍ اِلَّا اللّٰهُ ۗوَاِنَّ اللّٰهَ لَهُوَ الْعَزِيْزُ الْحَكِيْمُ

inna h*aadzaa* lahuwa **a**lqa*sh*a*sh*u **a**l*h*aqqu wam*aa* min il*aa*hin ill*aa* **al**l*aa*hu wa-inna **al**l*aa*ha lahuwa **a**l'aziizu

Sungguh, ini adalah kisah yang benar. Tidak ada tuhan selain Allah, dan sungguh, Allah Mahaperkasa, Mahabijaksana.

3:63

# فَاِنْ تَوَلَّوْا فَاِنَّ اللّٰهَ عَلِيْمٌ ۢبِالْمُفْسِدِيْنَ ࣖ

fa-in tawallaw fa-inna **al**l*aa*ha 'aliimun bi**a**lmufsidiin**a**

Kemudian jika mereka berpaling, maka (ketahuilah) bahwa Allah Maha Mengetahui orang-orang yang berbuat kerusakan.

3:64

# قُلْ يٰٓاَهْلَ الْكِتٰبِ تَعَالَوْا اِلٰى كَلِمَةٍ سَوَاۤءٍۢ بَيْنَنَا وَبَيْنَكُمْ اَلَّا نَعْبُدَ اِلَّا اللّٰهَ وَلَا نُشْرِكَ بِهٖ شَيْـًٔا وَّلَا يَتَّخِذَ بَعْضُنَا بَعْضًا اَرْبَابًا مِّنْ دُوْنِ اللّٰهِ ۗ فَاِنْ تَوَلَّوْا فَقُوْلُوا اشْهَدُوْا بِ

qul y*aa* ahla **a**lkit*aa*bi ta'*aa*law il*aa* kalimatin saw*aa*-in baynan*aa* wabaynakum **al**l*aa* na'buda ill*aa* **al**l*aa*ha wal*aa* nusyrika bihi syay-an wa

Katakanlah (Muhammad), “Wahai Ahli Kitab! Marilah (kita) menuju kepada satu kalimat (pegangan) yang sama antara kami dan kamu, bahwa kita tidak menyembah selain Allah dan kita tidak mempersekutukan-Nya dengan sesuatu pun, dan bahwa kita tidak menjadikan s

3:65

# يٰٓاَهْلَ الْكِتٰبِ لِمَ تُحَاۤجُّوْنَ فِيْٓ اِبْرٰهِيْمَ وَمَآ اُنْزِلَتِ التَّوْرٰىةُ وَالْاِنْجِيْلُ اِلَّا مِنْۢ بَعْدِهٖۗ اَفَلَا تَعْقِلُوْنَ

yaa ahla alkitaabi lima tuhaajjuuna fii ibraahiima wamaa unzilati alttawraatu waal-injiilu illaa min ba'dihi afalaa ta'qiluuna

Wahai Ahli Kitab! Mengapa kamu berbantah-bantahan tentang Ibrahim, padahal Taurat dan Injil diturunkan setelah dia (Ibrahim)? Apakah kamu tidak mengerti?

3:66

# هٰٓاَنْتُمْ هٰٓؤُلَاۤءِ حَاجَجْتُمْ فِيْمَا لَكُمْ بِهٖ عِلْمٌ فَلِمَ تُحَاۤجُّوْنَ فِيْمَا لَيْسَ لَكُمْ بِهٖ عِلْمٌ ۗ وَاللّٰهُ يَعْلَمُ واَنْتُمْ لَا تَعْلَمُوْنَ

h*aa* antum h*aa*ul*aa*-i *haa*jajtum fiim*aa* lakum bihi 'ilmun falima tu*haa*jjuuna fiim*aa* laysa lakum bihi 'ilmun wa**al**l*aa*hu ya'lamu wa-antum l*aa* ta'lamuun**a**

Begitulah kamu! Kamu berbantah-bantahan tentang apa yang kamu ketahui, tetapi mengapa kamu berbantah-bantahan juga tentang apa yang tidak kamu ketahui? Allah mengetahui sedang kamu tidak mengetahui.

3:67

# مَاكَانَ اِبْرٰهِيْمُ يَهُوْدِيًّا وَّلَا نَصْرَانِيًّا وَّلٰكِنْ كَانَ حَنِيْفًا مُّسْلِمًاۗ وَمَا كَانَ مِنَ الْمُشْرِكِيْنَ

maa kaana ibraahiimu yahuudiyyan walaa nashraaniyyan walaakin kaana haniifan musliman wamaa kaana mina almusyrikiina

Ibrahim bukanlah seorang Yahudi dan bukan (pula) seorang Nasrani, tetapi dia adalah seorang yang lurus, Muslim dan dia tidaklah termasuk orang-orang musyrik.

3:68

# اِنَّ اَوْلَى النَّاسِ بِاِبْرٰهِيْمَ لَلَّذِيْنَ اتَّبَعُوْهُ وَهٰذَا النَّبِيُّ وَالَّذِيْنَ اٰمَنُوْا ۗ وَاللّٰهُ وَلِيُّ الْمُؤْمِنِيْنَ

inna awl*aa* **al**nn*aa*si bi-ibr*aa*hiima lalla*dz*iina ittaba'uuhu wah*aadzaa* **al**nnabiyyu wa**a**lla*dz*iina *aa*manuu wa**al**l*aa*hu waliyyu **a**

**Orang yang paling dekat kepada Ibrahim ialah orang yang mengikutinya, dan Nabi ini (Muhammad), dan orang yang beriman. Allah adalah pelindung orang-orang yang beriman.**









3:69

# وَدَّتْ طَّاۤىِٕفَةٌ مِّنْ اَهْلِ الْكِتٰبِ لَوْ يُضِلُّوْنَكُمْۗ وَمَا يُضِلُّوْنَ اِلَّآ اَنْفُسَهُمْ وَمَا يَشْعُرُوْنَ

waddat *thaa*-ifatun min ahli **a**lkit*aa*bi law yu*dh*illuunakum wam*aa* yu*dh*illuuna ill*aa* anfusahum wam*aa* yasy'uruun**a**

Segolongan Ahli Kitab ingin menyesatkan kamu. Padahal (sesungguhnya), mereka tidak menyesatkan melainkan diri mereka sendiri, tetapi mereka tidak menyadari.

3:70

# يٰٓاَهْلَ الْكِتٰبِ لِمَ تَكْفُرُوْنَ بِاٰيٰتِ اللّٰهِ وَاَنْتُمْ تَشْهَدُوْنَ

y*aa* ahla **a**lkit*aa*bi lima takfuruuna bi-*aa*y*aa*ti **al**l*aa*hi wa-antum tasyhaduun**a**

Wahai Ahli Kitab! Mengapa kamu mengingkari ayat-ayat Allah, padahal kamu mengetahui (kebenarannya)?

3:71

# يٰٓاَهْلَ الْكِتٰبِ لِمَ تَلْبِسُوْنَ الْحَقَّ بِالْبَاطِلِ وَتَكْتُمُوْنَ الْحَقَّ وَاَنْتُمْ تَعْلَمُوْنَ ࣖ

y*aa* ahla **a**lkit*aa*bi lima talbisuuna **a**l*h*aqqa bi**a**lb*aath*ili wataktumuuna **a**l*h*aqqa wa-antum ta'lamuun**a**

Wahai Ahli Kitab! Mengapa kamu mencampuradukkan kebenaran dengan kebatilan, dan kamu menyembunyikan kebenaran, padahal kamu mengetahui?

3:72

# وَقَالَتْ طَّاۤىِٕفَةٌ مِّنْ اَهْلِ الْكِتٰبِ اٰمِنُوْا بِالَّذِيْٓ اُنْزِلَ عَلَى الَّذِيْنَ اٰمَنُوْا وَجْهَ النَّهَارِ وَاكْفُرُوْٓا اٰخِرَهٗ لَعَلَّهُمْ يَرْجِعُوْنَۚ

waq*aa*lat *thaa*-ifatun min ahli **a**lkit*aa*bi *aa*minuu bi**a**lla*dz*ii unzila 'al*aa* **al**la*dz*iina *aa*manuu wajha **al**nnah*aa*ri wa

Dan segolongan Ahli Kitab berkata (kepada sesamanya), “Berimanlah kamu kepada apa yang diturunkan kepada orang-orang beriman pada awal siang dan ingkarilah di akhirnya, agar mereka kembali (kepada kekafiran).

3:73

# وَلَا تُؤْمِنُوْٓا اِلَّا لِمَنْ تَبِعَ دِيْنَكُمْ ۗ قُلْ اِنَّ الْهُدٰى هُدَى اللّٰهِ ۙ اَنْ يُّؤْتٰىٓ اَحَدٌ مِّثْلَ مَآ اُوْتِيْتُمْ اَوْ يُحَاۤجُّوْكُمْ عِنْدَ رَبِّكُمْ ۗ قُلْ اِنَّ الْفَضْلَ بِيَدِ اللّٰهِ ۚ يُؤْتِيْهِ مَنْ يَّشَاۤءُ ۗوَاللّٰهُ وَ

walaa tu/minuu illaa liman tabi'a diinakum qul inna alhudaa hudaa allaahi an yu/taa ahadun mitsla maa uutiitum aw yuhaajjuukum 'inda rabbikum qul inna alfadhla biyadi allaahi yu/tiihi man yasyaau waallaahu waasi'un 'aliimun

Dan janganlah kamu percaya selain kepada orang yang mengikuti agamamu.” Katakanlah (Muhammad), “Sesungguhnya petunjuk itu hanyalah petunjuk Allah. (Janganlah kamu percaya) bahwa seseorang akan diberi seperti apa yang diberikan kepada kamu, atau bahwa mere

3:74

# يَخْتَصُّ بِرَحْمَتِهٖ مَنْ يَّشَاۤءُ ۗوَاللّٰهُ ذُو الْفَضْلِ الْعَظِيْمِ

yakhta*shsh*u bira*h*matihi man yasy*aa*u wa**al**l*aa*hu *dz*uu **a**lfa*dh*li **a**l'a*zh*iim**i**

Dia menentukan rahmat-Nya kepada siapa yang Dia kehendaki. Dan Allah memiliki karunia yang besar.

3:75

# ۞ وَمِنْ اَهْلِ الْكِتٰبِ مَنْ اِنْ تَأْمَنْهُ بِقِنْطَارٍ يُّؤَدِّهٖٓ اِلَيْكَۚ وَمِنْهُمْ مَّنْ اِنْ تَأْمَنْهُ بِدِيْنَارٍ لَّا يُؤَدِّهٖٓ اِلَيْكَ اِلَّا مَا دُمْتَ عَلَيْهِ قَاۤىِٕمًا ۗ ذٰلِكَ بِاَنَّهُمْ قَالُوْا لَيْسَ عَلَيْنَا فِى الْاُمِّيّٖنَ س

wamin ahli **a**lkit*aa*bi man in ta/manhu biqin*thaa*rin yu-addihi ilayka waminhum man in ta/manhu bidiin*aa*rin l*aa* yu-addihi ilayka ill*aa* m*aa* dumta 'alayhi q*aa*-iman *dzaa*lika bi-annah

Dan di antara Ahli Kitab ada yang jika engkau percayakan kepadanya harta yang banyak, niscaya dia mengembalikannya kepadamu. Tetapi ada (pula) di antara mereka yang jika engkau percayakan kepadanya satu dinar, dia tidak mengembalikannya kepadamu, kecuali

3:76

# بَلٰى مَنْ اَوْفٰى بِعَهْدِهٖ وَاتَّقٰى فَاِنَّ اللّٰهَ يُحِبُّ الْمُتَّقِيْنَ

bal*aa* man awf*aa* bi'ahdihi wa**i**ttaq*aa* fa-inna **al**l*aa*ha yu*h*ibbu **a**lmuttaqiin**a**

Sebenarnya barangsiapa menepati janji dan bertakwa, maka sungguh, Allah mencintai orang-orang yang bertakwa.

3:77

# اِنَّ الَّذِيْنَ يَشْتَرُوْنَ بِعَهْدِ اللّٰهِ وَاَيْمَانِهِمْ ثَمَنًا قَلِيْلًا اُولٰۤىِٕكَ لَا خَلَاقَ لَهُمْ فِى الْاٰخِرَةِ وَلَا يُكَلِّمُهُمُ اللّٰهُ وَلَا يَنْظُرُ اِلَيْهِمْ يَوْمَ الْقِيٰمَةِ وَلَا يُزَكِّيْهِمْ ۖ وَلَهُمْ عَذَابٌ اَلِيْمٌ

inna **al**la*dz*iina yasytaruuna bi'ahdi **al**l*aa*hi wa-aym*aa*nihim tsamanan qaliilan ul*aa*-ika l*aa* khal*aa*qa lahum fii **a**l-*aa*khirati wal*aa* yukallimuhumu

Sesungguhnya orang-orang yang memperjualbelikan janji Allah dan sumpah-sumpah mereka dengan harga murah, mereka itu tidak memperoleh bagian di akhirat, Allah tidak akan menyapa mereka, tidak akan memperhatikan mereka pada hari Kiamat, dan tidak akan menyu

3:78

# وَاِنَّ مِنْهُمْ لَفَرِيْقًا يَّلْوٗنَ اَلْسِنَتَهُمْ بِالْكِتٰبِ لِتَحْسَبُوْهُ مِنَ الْكِتٰبِ وَمَا هُوَ مِنَ الْكِتٰبِۚ وَيَقُوْلُوْنَ هُوَ مِنْ عِنْدِ اللّٰهِ وَمَا هُوَ مِنْ عِنْدِ اللّٰهِ ۚ وَيَقُوْلُوْنَ عَلَى اللّٰهِ الْكَذِبَ وَهُمْ يَعْلَمُوْنَ

wa-inna minhum lafariiqan yalwuuna **a**lsinatahum bi**a**lkit*aa*bi lita*h*sabuuhu mina **a**lkit*aa*bi wam*aa* huwa mina **a**lkit*aa*bi wayaquuluuna huwa min 'indi **al<**

Dan sungguh, di antara mereka niscaya ada segolongan yang memutarbalikkan lidahnya membaca Kitab, agar kamu menyangka (yang mereka baca) itu sebagian dari Kitab, padahal itu bukan dari Kitab dan mereka berkata, “Itu dari Allah,” padahal itu bukan dari All







3:79

# مَا كَانَ لِبَشَرٍ اَنْ يُّؤْتِيَهُ اللّٰهُ الْكِتٰبَ وَالْحُكْمَ وَالنُّبُوَّةَ ثُمَّ يَقُوْلَ لِلنَّاسِ كُوْنُوْا عِبَادًا لِّيْ مِنْ دُوْنِ اللّٰهِ وَلٰكِنْ كُوْنُوْا رَبَّانِيّٖنَ بِمَا كُنْتُمْ تُعَلِّمُوْنَ الْكِتٰبَ وَبِمَا كُنْتُمْ تَدْرُسُوْنَ ۙ

m*aa* k*aa*na libasyarin an yu/tiyahu **al**l*aa*hu **a**lkit*aa*ba wa**a**l*h*ukma wa**al**nnubuwwata tsumma yaquula li**l**nn*aa*si kuunuu 'ib*aa*dan lii

Tidak mungkin bagi seseorang yang telah diberi kitab oleh Allah, serta hikmah dan kenabian, kemudian dia berkata kepada manusia, “Jadilah kamu penyembahku, bukan penyembah Allah,” tetapi (dia berkata), “Jadilah kamu pengabdi-pengabdi Allah, karena kamu me

3:80

# وَلَا يَأْمُرَكُمْ اَنْ تَتَّخِذُوا الْمَلٰۤىِٕكَةَ وَالنَّبِيّٖنَ اَرْبَابًا ۗ اَيَأْمُرُكُمْ بِالْكُفْرِ بَعْدَ اِذْ اَنْتُمْ مُّسْلِمُوْنَ ࣖ

wal*aa* ya/murakum an tattakhi*dz*uu **a**lmal*aa*-ikata wa**al**nnabiyyiina arb*aa*ban aya/murukum bi**a**lkufri ba'da i*dz* antum muslimuun**a**

dan tidak (mungkin pula baginya) menyuruh kamu menjadikan para malaikat dan para nabi sebagai Tuhan. Apakah (patut) dia menyuruh kamu menjadi kafir setelah kamu menjadi Muslim?

3:81

# وَاِذْ اَخَذَ اللّٰهُ مِيْثَاقَ النَّبِيّٖنَ لَمَآ اٰتَيْتُكُمْ مِّنْ كِتٰبٍ وَّحِكْمَةٍ ثُمَّ جَاۤءَكُمْ رَسُوْلٌ مُّصَدِّقٌ لِّمَا مَعَكُمْ لَتُؤْمِنُنَّ بِهٖ وَلَتَنْصُرُنَّهٗ ۗ قَالَ ءَاَقْرَرْتُمْ وَاَخَذْتُمْ عَلٰى ذٰلِكُمْ اِصْرِيْ ۗ قَالُوْٓا اَق

wa-idz akhadza allaahu miitsaaqa alnnabiyyiina lamaa aataytukum min kitaabin wahikmatin tsumma jaa-akum rasuulun mushaddiqun limaa ma'akum latu/minunna bihi walatanshurunnahu qaala a-aqrartum wa-akhadztum 'alaa dzaalikum ishrii qaaluu aqrarnaa qaala faisy

Dan (ingatlah), ketika Allah mengambil perjanjian dari para nabi, “Manakala Aku memberikan kitab dan hikmah kepadamu lalu datang kepada kamu seorang Rasul yang membenarkan apa yang ada pada kamu, niscaya kamu akan sungguh-sungguh beriman kepadanya dan men

3:82

# فَمَنْ تَوَلّٰى بَعْدَ ذٰلِكَ فَاُولٰۤىِٕكَ هُمُ الْفٰسِقُوْنَ

faman tawall*aa* ba'da *dzaa*lika faul*aa*-ika humu **a**lf*aa*siquun**a**

Maka barangsiapa berpaling setelah itu, maka mereka itulah orang yang fasik.

3:83

# اَفَغَيْرَ دِيْنِ اللّٰهِ يَبْغُوْنَ وَلَهٗ ٓ اَسْلَمَ مَنْ فِى السَّمٰوٰتِ وَالْاَرْضِ طَوْعًا وَّكَرْهًا وَّاِلَيْهِ يُرْجَعُوْنَ

afaghayra diini **al**l*aa*hi yabghuuna walahu aslama man fii **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i *th*aw'an wakarhan wa-ilayhi yurja'uun**a**afaghayra diini **al**

**Maka mengapa mereka mencari agama yang lain selain agama Allah, padahal apa yang di langit dan di bumi berserah diri kepada-Nya, (baik) dengan suka maupun terpaksa, dan hanya kepada-Nya mereka dikembalikan?**









3:84

# قُلْ اٰمَنَّا بِاللّٰهِ وَمَآ اُنْزِلَ عَلَيْنَا وَمَآ اُنْزِلَ عَلٰٓى اِبْرٰهِيْمَ وَاِسْمٰعِيْلَ وَاِسْحٰقَ وَيَعْقُوْبَ وَالْاَسْبَاطِ وَمَآ اُوْتِيَ مُوْسٰى وَعِيْسٰى وَالنَّبِيُّوْنَ مِنْ رَّبِّهِمْۖ لَا نُفَرِّقُ بَيْنَ اَحَدٍ مِّنْهُمْۖ وَنَحْن

qul *aa*mann*aa* bi**al**l*aa*hi wam*aa* unzila 'alayn*aa* wam*aa* unzila 'al*aa* ibr*aa*hiima wa-ism*aa*'iila wa-is*haa*qa waya'quuba wa**a**l-asb*aath*i wam*aa* uutiya

Katakanlah (Muhammad), “Kami beriman kepada Allah dan kepada apa yang diturunkan kepada kami dan yang diturunkan kepada Ibrahim, Ismail, Ishak, Yakub, dan anak cucunya, dan apa yang diberikan kepada Musa, Isa dan para nabi dari Tuhan mereka. Kami tidak me

3:85

# وَمَنْ يَّبْتَغِ غَيْرَ الْاِسْلَامِ دِيْنًا فَلَنْ يُّقْبَلَ مِنْهُۚ وَهُوَ فِى الْاٰخِرَةِ مِنَ الْخٰسِرِيْنَ

waman yabtaghi ghayra **a**l-isl*aa*mi diinan falan yuqbala minhu wahuwa fii **a**l-*aa*khirati mina **a**lkh*aa*siriin**a**

Dan barangsiapa mencari agama selain Islam, dia tidak akan diterima, dan di akhirat dia termasuk orang yang rugi.

3:86

# كَيْفَ يَهْدِى اللّٰهُ قَوْمًا كَفَرُوْا بَعْدَ اِيْمَانِهِمْ وَشَهِدُوْٓا اَنَّ الرَّسُوْلَ حَقٌّ وَّجَاۤءَهُمُ الْبَيِّنٰتُ ۗ وَاللّٰهُ لَا يَهْدِى الْقَوْمَ الظّٰلِمِيْنَ

kayfa yahdii **al**l*aa*hu qawman kafaruu ba'da iim*aa*nihim wasyahiduu anna **al**rrasuula *h*aqqun waj*aa*-ahumu **a**lbayyin*aa*tu wa**al**l*aa*hu l*aa* yahdii

Bagaimana Allah akan memberi petunjuk kepada suatu kaum yang kafir setelah mereka beriman, serta mengakui bahwa Rasul (Muhammad) itu benar-benar (rasul), dan bukti-bukti yang jelas telah sampai kepada mereka? Allah tidak memberi petunjuk kepada orang zali

3:87

# اُولٰۤىِٕكَ جَزَاۤؤُهُمْ اَنَّ عَلَيْهِمْ لَعْنَةَ اللّٰهِ وَالْمَلٰۤىِٕكَةِ وَالنَّاسِ اَجْمَعِيْنَۙ

ul*aa*-ika jaz*aa*uhum anna 'alayhim la'nata **al**l*aa*hi wa**a**lmal*aa*-ikati wa**al**nn*aa*si ajma'iin**a**

Mereka itu, balasannya ialah ditimpa laknat Allah, para malaikat, dan manusia seluruhnya,

3:88

# خٰلِدِيْنَ فِيْهَا ۚ لَا يُخَفَّفُ عَنْهُمُ الْعَذَابُ وَلَا هُمْ يُنْظَرُوْنَۙ

kh*aa*lidiina fiih*aa* l*aa* yukhaffafu 'anhumu **a**l'a*dzaa*bu wal*aa* hum yun*zh*aruun**a**

mereka kekal di dalamnya, tidak akan diringankan azabnya, dan mereka tidak diberi penangguhan,

3:89

# اِلَّا الَّذِيْنَ تَابُوْا مِنْۢ بَعْدِ ذٰلِكَ وَاَصْلَحُوْاۗ فَاِنَّ اللّٰهَ غَفُوْرٌ رَّحِيْمٌ

ill*aa* **al**la*dz*iina t*aa*buu min ba'di *dzaa*lika wa-a*sh*la*h*uu fa-inna **al**l*aa*ha ghafuurun ra*h*iim**un**

kecuali orang-orang yang bertobat setelah itu, dan melakukan perbaikan, maka sungguh, Allah Maha Pengampun, Maha Penyayang.

3:90

# اِنَّ الَّذِيْنَ كَفَرُوْا بَعْدَ اِيْمَانِهِمْ ثُمَّ ازْدَادُوْا كُفْرًا لَّنْ تُقْبَلَ تَوْبَتُهُمْ ۚ وَاُولٰۤىِٕكَ هُمُ الضَّاۤلُّوْنَ

inna **al**la*dz*iina kafaruu ba'da iim*aa*nihim tsumma izd*aa*duu kufran lan tuqbala tawbatuhum waul*aa*-ika humu **al***dhdhaa*lluun**a**

Sungguh, orang-orang yang kafir setelah beriman, kemudian bertambah kekafirannya, tidak akan diterima tobatnya, dan mereka itulah orang-orang yang sesat.

3:91

# اِنَّ الَّذِيْنَ كَفَرُوْا وَمَاتُوْا وَهُمْ كُفَّارٌ فَلَنْ يُّقْبَلَ مِنْ اَحَدِهِمْ مِّلْءُ الْاَرْضِ ذَهَبًا وَّلَوِ افْتَدٰى بِهٖۗ اُولٰۤىِٕكَ لَهُمْ عَذَابٌ اَلِيْمٌ وَّمَا لَهُمْ مِّنْ نّٰصِرِيْنَ ࣖ ۔

inna **al**la*dz*iina kafaruu wam*aa*tuu wahum kuff*aa*run falan yuqbala min a*h*adihim milu **a**l-ar*dh*i *dz*ahaban walawi iftad*aa* bihi ul*aa*-ika lahum 'a*dzaa*bun **a**

**Sungguh, orang-orang yang kafir dan mati dalam kekafiran, tidak akan diterima (tebusan) dari seseorang di antara mereka sekalipun (berupa) emas sepenuh bumi, sekiranya dia hendak menebus diri dengannya. Mereka itulah orang-orang yang mendapat azab yang pe**









3:92

# لَنْ تَنَالُوا الْبِرَّ حَتّٰى تُنْفِقُوْا مِمَّا تُحِبُّوْنَ ۗوَمَا تُنْفِقُوْا مِنْ شَيْءٍ فَاِنَّ اللّٰهَ بِهٖ عَلِيْمٌ

lan tan*aa*luu **a**lbirra *h*att*aa* tunfiquu mimm*aa* tu*h*ibbuuna wam*aa* tunfiquu min syay-in fa-inna **al**l*aa*ha bihi 'aliim**un**

Kamu tidak akan memperoleh kebajikan, sebelum kamu menginfakkan sebagian harta yang kamu cintai. Dan apa pun yang kamu infakkan, tentang hal itu sungguh, Allah Maha Mengetahui.

3:93

# ۞ كُلُّ الطَّعَامِ كَانَ حِلًّا لِّبَنِيْٓ اِسْرَاۤءِيْلَ اِلَّا مَا حَرَّمَ اِسْرَاۤءِيْلُ عَلٰى نَفْسِهٖ مِنْ قَبْلِ اَنْ تُنَزَّلَ التَّوْرٰىةُ ۗ قُلْ فَأْتُوْا بِالتَّوْرٰىةِ فَاتْلُوْهَآ اِنْ كُنْتُمْ صٰدِقِيْنَ

kullu **al***ththh*a'*aa*mi k*aa*na *h*illan libanii isr*aa*-iila ill*aa* m*aa* *h*arrama isr*aa*-iilu 'al*aa* nafsihi min qabli an tunazzala **al**ttawr*aa*tu qul fa/tuu bi<

Semua makanan itu halal bagi Bani Israil, kecuali makanan yang diharamkan oleh Israil (Yakub) atas dirinya sebelum Taurat diturunkan. Katakanlah (Muhammad), “Maka bawalah Taurat lalu bacalah, jika kamu orang-orang yang benar.”

3:94

# فَمَنِ افْتَرٰى عَلَى اللّٰهِ الْكَذِبَ مِنْۢ بَعْدِ ذٰلِكَ فَاُولٰۤىِٕكَ هُمُ الظّٰلِمُوْنَ

famani iftar*aa* 'al*aa* **al**l*aa*hi **a**lka*dz*iba min ba'di *dzaa*lika faul*aa*-ika humu **al***zhaa*limuun**a**

Maka barangsiapa mengada-adakan kebohongan terhadap Allah setelah itu, maka mereka itulah orang-orang zalim.

3:95

# قُلْ صَدَقَ اللّٰهُ ۗ فَاتَّبِعُوْا مِلَّةَ اِبْرٰهِيْمَ حَنِيْفًاۗ وَمَا كَانَ مِنَ الْمُشْرِكِيْنَ

qul *sh*adaqa **al**l*aa*hu fa**i**ttabi'uu millata ibr*aa*hiima *h*aniifan wam*aa* k*aa*na mina **a**lmusyrikiin**a**

Katakanlah (Muhammad), “Benarlah (segala yang difirmankan) Allah.” Maka ikutilah agama Ibrahim yang lurus, dan dia tidaklah termasuk orang musyrik.

3:96

# اِنَّ اَوَّلَ بَيْتٍ وُّضِعَ لِلنَّاسِ لَلَّذِيْ بِبَكَّةَ مُبٰرَكًا وَّهُدًى لِّلْعٰلَمِيْنَۚ

inna awwala baytin wu*dh*i'a li**l**nn*aa*si lalla*dz*ii bibakkata mub*aa*rakan wahudan lil'*aa*lamiin**a**

Sesungguhnya rumah (ibadah) pertama yang dibangun untuk manusia, ialah (Baitullah) yang di Bakkah (Mekah) yang diberkahi dan menjadi petunjuk bagi seluruh alam.

3:97

# فِيْهِ اٰيٰتٌۢ بَيِّنٰتٌ مَّقَامُ اِبْرٰهِيْمَ ەۚ وَمَنْ دَخَلَهٗ كَانَ اٰمِنًا ۗ وَلِلّٰهِ عَلَى النَّاسِ حِجُّ الْبَيْتِ مَنِ اسْتَطَاعَ اِلَيْهِ سَبِيْلًا ۗ وَمَنْ كَفَرَ فَاِنَّ اللّٰهَ غَنِيٌّ عَنِ الْعٰلَمِيْنَ

fiihi *aa*y*aa*tun bayyin*aa*tun maq*aa*mu ibr*aa*hiima waman dakhalahu k*aa*na *aa*minan walill*aa*hi 'al*aa* **al**nn*aa*si *h*ijju **a**lbayti mani ista*thaa*'

Di sana terdapat tanda-tanda yang jelas, (di antaranya) maqam Ibrahim. Barangsiapa memasukinya (Baitullah) amanlah dia. Dan (di antara) kewajiban manusia terhadap Allah adalah melaksanakan ibadah haji ke Baitullah, yaitu bagi orang-orang yang mampu mengad

3:98

# قُلْ يٰٓاَهْلَ الْكِتٰبِ لِمَ تَكْفُرُوْنَ بِاٰيٰتِ اللّٰهِ وَاللّٰهُ شَهِيْدٌ عَلٰى مَا تَعْمَلُوْنَ

qul y*aa* ahla **a**lkit*aa*bi lima takfuruuna bi-*aa*y*aa*ti **al**l*aa*hi wa**al**l*aa*hu syahiidun 'al*aa* m*aa* ta'maluun**a**

Katakanlah (Muhammad), “Wahai Ahli Kitab! Mengapa kamu mengingkari ayat-ayat Allah, padahal Allah Maha Menyaksikan apa yang kamu kerjakan?”

3:99

# قُلْ يٰٓاَهْلَ الْكِتٰبِ لِمَ تَصُدُّوْنَ عَنْ سَبِيْلِ اللّٰهِ مَنْ اٰمَنَ تَبْغُوْنَهَا عِوَجًا وَّاَنْتُمْ شُهَدَاۤءُ ۗ وَمَا اللّٰهُ بِغَافِلٍ عَمَّا تَعْمَلُوْنَ

qul y*aa* ahla **a**lkit*aa*bi lima ta*sh*udduuna 'an sabiili **al**l*aa*hi man *aa*mana tabghuunah*aa* 'iwajan wa-antum syuhad*aa*u wam*aa* **al**l*aa*hu bigh*aa*fil

Katakanlah (Muhammad), “Wahai Ahli Kitab! Mengapa kamu menghalang-halangi orang-orang yang beriman dari jalan Allah, kamu menghendakinya (jalan Allah) bengkok, padahal kamu menyaksikan?” Dan Allah tidak lengah terhadap apa yang kamu kerjakan.

3:100

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْٓا اِنْ تُطِيْعُوْا فَرِيْقًا مِّنَ الَّذِيْنَ اُوْتُوا الْكِتٰبَ يَرُدُّوْكُمْ بَعْدَ اِيْمَانِكُمْ كٰفِرِيْنَ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu in tu*th*ii'uu fariiqan mina **al**la*dz*iina uutuu **a**lkit*aa*ba yarudduukum ba'da iim*aa*nikum k*aa*firiin**a**

Wahai orang-orang yang beriman! Jika kamu mengikuti sebagian dari orang yang diberi Kitab, niscaya mereka akan mengembalikan kamu menjadi orang kafir setelah beriman.

3:101

# وَكَيْفَ تَكْفُرُوْنَ وَاَنْتُمْ تُتْلٰى عَلَيْكُمْ اٰيٰتُ اللّٰهِ وَفِيْكُمْ رَسُوْلُهٗ ۗ وَمَنْ يَّعْتَصِمْ بِاللّٰهِ فَقَدْ هُدِيَ اِلٰى صِرَاطٍ مُّسْتَقِيْمٍ ࣖ

wakayfa takfuruuna wa-antum tutl*aa* 'alaykum *aa*y*aa*tu **al**l*aa*hi wafiikum rasuuluhu waman ya'ta*sh*im bi**al**l*aa*hi faqad hudiya il*aa* *sh*ir*aath*in mustaqiim**in**

**Dan bagaimana kamu (sampai) menjadi kafir, padahal ayat-ayat Allah dibacakan kepada kamu, dan Rasul-Nya (Muhammad) pun berada di tengah-tengah kamu? Barangsiapa berpegang teguh kepada (agama) Allah, maka sungguh, dia diberi petunjuk kepada jalan yang luru**









3:102

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوا اتَّقُوا اللّٰهَ حَقَّ تُقٰىتِهٖ وَلَا تَمُوْتُنَّ اِلَّا وَاَنْتُمْ مُّسْلِمُوْنَ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu ittaquu **al**l*aa*ha *h*aqqa tuq*aa*tihi wal*aa* tamuutunna ill*aa* wa-antum muslimuun**a**

Wahai orang-orang yang beriman! Bertakwalah kepada Allah sebenar-benar takwa kepada-Nya dan janganlah kamu mati kecuali dalam keadaan Muslim.

3:103

# وَاعْتَصِمُوْا بِحَبْلِ اللّٰهِ جَمِيْعًا وَّلَا تَفَرَّقُوْا ۖوَاذْكُرُوْا نِعْمَتَ اللّٰهِ عَلَيْكُمْ اِذْ كُنْتُمْ اَعْدَاۤءً فَاَلَّفَ بَيْنَ قُلُوْبِكُمْ فَاَصْبَحْتُمْ بِنِعْمَتِهٖٓ اِخْوَانًاۚ وَكُنْتُمْ عَلٰى شَفَا حُفْرَةٍ مِّنَ النَّارِ فَاَنْقَ

wa**i**'ta*sh*imuu bi*h*abli **al**l*aa*hi jamii'an wal*aa* tafarraquu wa**u***dz*kuruu ni'mata **al**l*aa*hi 'alaykum i*dz* kuntum a'd*aa*-an fa-allafa bayna qulu

Dan berpegangteguhlah kamu semuanya pada tali (agama) Allah, dan janganlah kamu bercerai berai, dan ingatlah nikmat Allah kepadamu ketika kamu dahulu (masa jahiliah) bermusuhan, lalu Allah mempersatukan hatimu, sehingga dengan karunia-Nya kamu menjadi ber

3:104

# وَلْتَكُنْ مِّنْكُمْ اُمَّةٌ يَّدْعُوْنَ اِلَى الْخَيْرِ وَيَأْمُرُوْنَ بِالْمَعْرُوْفِ وَيَنْهَوْنَ عَنِ الْمُنْكَرِ ۗ وَاُولٰۤىِٕكَ هُمُ الْمُفْلِحُوْنَ

waltakun minkum ummatun yad'uuna il*aa* **a**lkhayri waya/muruuna bi**a**lma'ruufi wayanhawna 'ani **a**lmunkari waul*aa*-ika humu **a**lmufli*h*uun**a**

Dan hendaklah di antara kamu ada segolongan orang yang menyeru kepada kebajikan, menyuruh (berbuat) yang makruf, dan mencegah dari yang mungkar. Dan mereka itulah orang-orang yang beruntung.

3:105

# وَلَا تَكُوْنُوْا كَالَّذِيْنَ تَفَرَّقُوْا وَاخْتَلَفُوْا مِنْۢ بَعْدِ مَا جَاۤءَهُمُ الْبَيِّنٰتُ ۗ وَاُولٰۤىِٕكَ لَهُمْ عَذَابٌ عَظِيْمٌ ۙ

wal*aa* takuunuu ka**a**lla*dz*iina tafarraquu wa**i**khtalafuu min ba'di m*aa* j*aa*-ahumu **a**lbayyin*aa*tu waul*aa*-ika lahum 'a*dzaa*bun 'a*zh*iim**un**

Dan janganlah kamu menjadi seperti orang-orang yang bercerai berai dan berselisih setelah sampai kepada mereka keterangan yang jelas. Dan Mereka itulah orang-orang yang mendapat azab yang berat,

3:106

# يَّوْمَ تَبْيَضُّ وُجُوْهٌ وَّتَسْوَدُّ وُجُوْهٌ ۚ فَاَمَّا الَّذِيْنَ اسْوَدَّتْ وُجُوْهُهُمْۗ اَ كَفَرْتُمْ بَعْدَ اِيْمَانِكُمْ فَذُوْقُوا الْعَذَابَ بِمَا كُنْتُمْ تَكْفُرُوْنَ

yawma tabya*dhdh*u wujuuhun wataswaddu wujuuhun fa-amm*aa* **al**la*dz*iina iswaddat wujuuhuhum akafartum ba'da iim*aa*nikum fa*dz*uuquu **a**l'a*dzaa*ba bim*aa* kuntum takfuruun**a**

**pada hari itu ada wajah yang putih berseri, dan ada pula wajah yang hitam muram. Adapun orang-orang yang berwajah hitam muram (kepada mereka dikatakan), “Mengapa kamu kafir setelah beriman? Karena itu rasakanlah azab disebabkan kekafiranmu itu.”**









3:107

# وَاَمَّا الَّذِيْنَ ابْيَضَّتْ وُجُوْهُهُمْ فَفِيْ رَحْمَةِ اللّٰهِ ۗ هُمْ فِيْهَا خٰلِدُوْنَ

wa-amm*aa* **al**la*dz*iina ibya*dhdh*at wujuuhuhum fafii ra*h*mati **al**l*aa*hi hum fiih*aa* kh*aa*liduun**a**

Dan adapun orang-orang yang berwajah putih berseri, mereka berada dalam rahmat Allah (surga); mereka kekal di dalamnya.

3:108

# تِلْكَ اٰيٰتُ اللّٰهِ نَتْلُوْهَا عَلَيْكَ بِالْحَقِّ ۗ وَمَا اللّٰهُ يُرِيْدُ ظُلْمًا لِّلْعٰلَمِيْنَ

tilka *aa*y*aa*tu **al**l*aa*hi natluuh*aa* 'alayka bi**a**l*h*aqqi wam*aa* **al**l*aa*hu yuriidu *zh*ulman lil'*aa*lamiin**a**

Itulah ayat-ayat Allah yang Kami bacakan kepada kamu dengan benar, dan Allah tidaklah berkehendak menzalimi (siapa pun) di seluruh alam.

3:109

# وَلِلّٰهِ مَا فِى السَّمٰوٰتِ وَمَا فِى الْاَرْضِ ۗوَاِلَى اللّٰهِ تُرْجَعُ الْاُمُوْرُ ࣖ

walill*aa*hi m*aa* fii **al**ssam*aa*w*aa*ti wam*aa* fii **a**l-ar*dh*i wa-il*aa* **al**l*aa*hi turja'u **a**l-umuur**u**

Dan milik Allah-lah apa yang ada di langit dan apa yang ada di bumi, dan hanya kepada Allah segala urusan dikembalikan.

3:110

# كُنْتُمْ خَيْرَ اُمَّةٍ اُخْرِجَتْ لِلنَّاسِ تَأْمُرُوْنَ بِالْمَعْرُوْفِ وَتَنْهَوْنَ عَنِ الْمُنْكَرِ وَتُؤْمِنُوْنَ بِاللّٰهِ ۗ وَلَوْ اٰمَنَ اَهْلُ الْكِتٰبِ لَكَانَ خَيْرًا لَّهُمْ ۗ مِنْهُمُ الْمُؤْمِنُوْنَ وَاَكْثَرُهُمُ الْفٰسِقُوْنَ

kuntum khayra ummatin ukhrijat li**l**nn*aa*si ta/muruuna bi**a**lma'ruufi watanhawna 'ani **a**lmunkari watu/minuuna bi**al**l*aa*hi walaw *aa*mana ahlu **a**lkit*aa*bi

Kamu (umat Islam) adalah umat terbaik yang dilahirkan untuk manusia, (karena kamu) menyuruh (berbuat) yang makruf, dan mencegah dari yang mungkar, dan beriman kepada Allah. Sekiranya Ahli Kitab beriman, tentulah itu lebih baik bagi mereka. Di antara merek

3:111

# لَنْ يَّضُرُّوْكُمْ اِلَّآ اَذًىۗ وَاِنْ يُّقَاتِلُوْكُمْ يُوَلُّوْكُمُ الْاَدْبَارَۗ ثُمَّ لَا يُنْصَرُوْنَ

lan ya*dh*urruukum ill*aa* a*dz*an wa-in yuq*aa*tiluukum yuwalluukumu **a**l-adb*aa*ra tsumma l*aa* yun*sh*aruun**a**

Mereka tidak akan membahayakan kamu, kecuali gangguan-gangguan kecil saja, dan jika mereka memerangi kamu, niscaya mereka mundur berbalik ke belakang (kalah). Selanjutnya mereka tidak mendapat pertolongan.

3:112

# ضُرِبَتْ عَلَيْهِمُ الذِّلَّةُ اَيْنَ مَا ثُقِفُوْٓا اِلَّا بِحَبْلٍ مِّنَ اللّٰهِ وَحَبْلٍ مِّنَ النَّاسِ وَبَاۤءُوْ بِغَضَبٍ مِّنَ اللّٰهِ وَضُرِبَتْ عَلَيْهِمُ الْمَسْكَنَةُ ۗ ذٰلِكَ بِاَنَّهُمْ كَانُوْا يَكْفُرُوْنَ بِاٰيٰتِ اللّٰهِ وَيَقْتُلُوْنَ الْ

*dh*uribat 'alayhimu **al***dzdz*illatu ayna m*aa* tsuqifuu ill*aa* bi*h*ablin mina **al**l*aa*hi wa*h*ablin mina **al**nn*aa*si wab*aa*uu bigha*dh*abin mina

Mereka diliputi kehinaan di mana saja mereka berada, kecuali jika mereka (berpegang) pada tali (agama) Allah dan tali (perjanjian) dengan manusia. Mereka mendapat murka dari Allah dan (selalu) diliputi kesengsaraan. Yang demikian itu karena mereka menging







3:113

# ۞ لَيْسُوْا سَوَاۤءً ۗ مِنْ اَهْلِ الْكِتٰبِ اُمَّةٌ قَاۤىِٕمَةٌ يَّتْلُوْنَ اٰيٰتِ اللّٰهِ اٰنَاۤءَ الَّيْلِ وَهُمْ يَسْجُدُوْنَ

laysuu saw*aa*-an min ahli **a**lkit*aa*bi ummatun q*aa*-imatun yatluuna *aa*y*aa*ti **al**l*aa*hi *aa*n*aa*-a **al**layli wahum yasjuduun**a**

Mereka itu tidak (seluruhnya) sama. Di antara Ahli Kitab ada golongan yang jujur, mereka membaca ayat-ayat Allah pada malam hari, dan mereka (juga) bersujud (salat).

3:114

# يُؤْمِنُوْنَ بِاللّٰهِ وَالْيَوْمِ الْاٰخِرِ وَيَأْمُرُوْنَ بِالْمَعْرُوْفِ وَيَنْهَوْنَ عَنِ الْمُنْكَرِ وَيُسَارِعُوْنَ فِى الْخَيْرٰتِۗ وَاُولٰۤىِٕكَ مِنَ الصّٰلِحِيْنَ

yu/minuuna bi**al**l*aa*hi wa**a**lyawmi **a**l-*aa*khiri waya/muruuna bi**a**lma'ruufi wayanhawna 'ani **a**lmunkari wayus*aa*ri'uuna fii **a**lkhayr*aa*ti

Mereka beriman kepada Allah dan hari akhir, menyuruh (berbuat) yang makruf, dan mencegah dari yang mungkar dan bersegera (mengerjakan) berbagai kebajikan. Mereka termasuk orang-orang saleh.

3:115

# وَمَا يَفْعَلُوْا مِنْ خَيْرٍ فَلَنْ يُّكْفَرُوْهُ ۗ وَاللّٰهُ عَلِيْمٌ ۢبِالْمُتَّقِيْنَ

wam*aa* yaf'aluu min khayrin falan yukfaruuhu wa**al**l*aa*hu 'aliimun bi**a**lmuttaqiin**a**

Dan kebajikan apa pun yang mereka kerjakan, tidak ada yang mengingkarinya. Dan Allah Maha Mengetahui orang-orang yang bertakwa.

3:116

# اِنَّ الَّذِيْنَ كَفَرُوْا لَنْ تُغْنِيَ عَنْهُمْ اَمْوَالُهُمْ وَلَآ اَوْلَادُهُمْ مِّنَ اللّٰهِ شَيْـًٔا ۗ وَاُولٰۤىِٕكَ اَصْحٰبُ النَّارِ ۚ هُمْ فِيْهَا خٰلِدُوْنَ

inna **al**la*dz*iina kafaruu lan tughniya 'anhum amw*aa*luhum wal*aa* awl*aa*duhum mina **al**l*aa*hi syay-an waul*aa*-ika a*sh*-*haa*bu **al**nn*aa*ri hum fiih*aa*

Sesungguhnya orang-orang kafir, baik harta maupun anak-anak mereka, sedikit pun tidak dapat menolak azab Allah. Mereka itu penghuni neraka, (dan) mereka kekal di dalamnya.

3:117

# مَثَلُ مَا يُنْفِقُوْنَ فِيْ هٰذِهِ الْحَيٰوةِ الدُّنْيَا كَمَثَلِ رِيْحٍ فِيْهَا صِرٌّ اَصَابَتْ حَرْثَ قَوْمٍ ظَلَمُوْٓا اَنْفُسَهُمْ فَاَهْلَكَتْهُ ۗ وَمَا ظَلَمَهُمُ اللّٰهُ وَلٰكِنْ اَنْفُسَهُمْ يَظْلِمُوْنَ

matsalu m*aa* yunfiquuna fii h*aadz*ihi **a**l*h*ay*aa*ti **al**dduny*aa* kamatsali rii*h*in fiih*aa* *sh*irrun a*shaa*bat *h*artsa qawmin *zh*alamuu anfusahum fa-ahlakat-hu

Perumpamaan harta yang mereka infakkan di dalam kehidupan dunia ini, ibarat angin yang mengandung hawa sangat dingin, yang menimpa tanaman (milik) suatu kaum yang menzalimi diri sendiri, lalu angin itu merusaknya. Allah tidak menzalimi mereka, tetapi mere

3:118

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْا لَا تَتَّخِذُوْا بِطَانَةً مِّنْ دُوْنِكُمْ لَا يَأْلُوْنَكُمْ خَبَالًاۗ وَدُّوْا مَا عَنِتُّمْۚ قَدْ بَدَتِ الْبَغْضَاۤءُ مِنْ اَفْوَاهِهِمْۖ وَمَا تُخْفِيْ صُدُوْرُهُمْ اَكْبَرُ ۗ قَدْ بَيَّنَّا لَكُمُ الْاٰيٰتِ اِنْ كُ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu l*aa* tattakhi*dz*uu bi*thaa*natan min duunikum l*aa* ya/luunakum khab*aa*lan wadduu m*aa* 'anittum qad badati **a**lbagh*daa*

*Wahai orang-orang yang beriman! Janganlah kamu menjadikan teman orang-orang yang di luar kalanganmu (seagama) sebagai teman kepercayaanmu, (karena) mereka tidak henti-hentinya menyusahkan kamu. Mereka mengharapkan kehancuranmu. Sungguh, telah nyata kebenc*









3:119

# هٰٓاَنْتُمْ اُولَاۤءِ تُحِبُّوْنَهُمْ وَلَا يُحِبُّوْنَكُمْ وَتُؤْمِنُوْنَ بِالْكِتٰبِ كُلِّهٖۚ وَاِذَا لَقُوْكُمْ قَالُوْٓا اٰمَنَّاۖ وَاِذَا خَلَوْا عَضُّوْا عَلَيْكُمُ الْاَنَامِلَ مِنَ الْغَيْظِ ۗ قُلْ مُوْتُوْا بِغَيْظِكُمْ ۗ اِنَّ اللّٰهَ عَلِيْمٌ ۢ

h*aa* antum ul*aa*-i tu*h*ibbuunahum wal*aa* yu*h*ibbuunakum watu/minuuna bi**a**lkit*aa*bi kullihi wa-i*dzaa* laquukum q*aa*luu *aa*mann*aa* wa-i*dzaa* khalaw 'a*dhdh*uu 'alaykumu <

Beginilah kamu! Kamu menyukai mereka, padahal mereka tidak menyukaimu, dan kamu beriman kepada semua kitab. Apabila mereka berjumpa kamu, mereka berkata, “Kami beriman,” dan apabila mereka menyendiri, mereka menggigit ujung jari karena marah dan benci kep

3:120

# اِنْ تَمْسَسْكُمْ حَسَنَةٌ تَسُؤْهُمْۖ وَاِنْ تُصِبْكُمْ سَيِّئَةٌ يَّفْرَحُوْا بِهَا ۗ وَاِنْ تَصْبِرُوْا وَتَتَّقُوْا لَا يَضُرُّكُمْ كَيْدُهُمْ شَيْـًٔا ۗ اِنَّ اللّٰهَ بِمَا يَعْمَلُوْنَ مُحِيْطٌ ࣖ

in tamsaskum *h*asanatun tasu/hum wa-in tu*sh*ibkum sayyi-atun yafra*h*uu bih*aa* wa-in ta*sh*biruu watattaquu l*aa* ya*dh*urrukum kayduhum syay-an inna **al**l*aa*ha bim*aa* ya'maluuna mu*h*i

Jika kamu memperoleh kebaikan, (niscaya) mereka bersedih hati, tetapi jika kamu tertimpa bencana, mereka bergembira karenanya. Jika kamu bersabar dan bertakwa, tipu daya mereka tidak akan menyusahkan kamu sedikit pun. Sungguh, Allah Maha Meliputi segala a

3:121

# وَاِذْ غَدَوْتَ مِنْ اَهْلِكَ تُبَوِّئُ الْمُؤْمِنِيْنَ مَقَاعِدَ لِلْقِتَالِ ۗ وَاللّٰهُ سَمِيْعٌ عَلِيْمٌۙ

wa-i*dz* ghadawta min ahlika tubawwi-u **a**lmu/miniina maq*aa*'ida lilqit*aa*li wa**al**l*aa*hu samii'un 'aliim**un**

Dan (ingatlah), ketika engkau (Muhammad) berangkat pada pagi hari meninggalkan keluargamu untuk mengatur orang-orang beriman pada pos-pos pertempuran. Allah Maha Mendengar, Maha Mengetahui.

3:122

# اِذْ هَمَّتْ طَّۤاىِٕفَتٰنِ مِنْكُمْ اَنْ تَفْشَلَاۙ وَاللّٰهُ وَلِيُّهُمَا ۗ وَعَلَى اللّٰهِ فَلْيَتَوَكَّلِ الْمُؤْمِنُوْنَ

i*dz* hammat *thaa*-ifat*aa*ni minkum an tafsyal*aa* wa**al**l*aa*hu waliyyuhum*aa* wa'al*aa* **al**l*aa*hi falyatawakkali **a**lmu/minuun**a**

Ketika dua golongan dari pihak kamu ingin (mundur) karena takut, padahal Allah adalah penolong mereka. Karena itu, hendaklah kepada Allah saja orang-orang mukmin bertawakal.

3:123

# وَلَقَدْ نَصَرَكُمُ اللّٰهُ بِبَدْرٍ وَّاَنْتُمْ اَذِلَّةٌ ۚ فَاتَّقُوا اللّٰهَ لَعَلَّكُمْ تَشْكُرُوْنَ

walaqad na*sh*arakumu **al**l*aa*hu bibadrin wa-antum a*dz*illatun fa**i**ttaquu **al**l*aa*ha la'allakum tasykuruun**a**

Dan sungguh, Allah telah menolong kamu dalam perang Badar, padahal kamu dalam keadaan lemah. Karena itu bertakwalah kepada Allah, agar kamu mensyukuri-Nya.

3:124

# اِذْ تَقُوْلُ لِلْمُؤْمِنِيْنَ اَلَنْ يَّكْفِيَكُمْ اَنْ يُّمِدَّكُمْ رَبُّكُمْ بِثَلٰثَةِ اٰلَافٍ مِّنَ الْمَلٰۤىِٕكَةِ مُنْزَلِيْنَۗ

i*dz* taquulu lilmu/miniina alan yakfiyakum an yumiddakum rabbukum bitsal*aa*tsati *aa*l*aa*fin mina almal*aa*-ikati munzaliin**a**

(Ingatlah), ketika engkau (Muhammad) mengatakan kepada orang-orang beriman, “Apakah tidak cukup bagimu bahwa Allah membantu kamu dengan tiga ribu malaikat yang diturunkan (dari langit)?”

3:125

# بَلٰٓى ۙاِنْ تَصْبِرُوْا وَتَتَّقُوْا وَيَأْتُوْكُمْ مِّنْ فَوْرِهِمْ هٰذَا يُمْدِدْكُمْ رَبُّكُمْ بِخَمْسَةِ اٰلَافٍ مِّنَ الْمَلٰۤىِٕكَةِ مُسَوِّمِيْنَ

bal*aa* in ta*sh*biruu watattaquu waya/tuukum min fawrihim h*aadzaa* yumdidkum rabbukum bikhamsati *aa*l*aa*fin mina **a**lmal*aa*-ikati musawwimiin**a**

“Ya” (cukup). Jika kamu bersabar dan bertakwa ketika mereka datang menyerang kamu dengan tiba-tiba, niscaya Allah menolongmu dengan lima ribu malaikat yang memakai tanda.

3:126

# وَمَا جَعَلَهُ اللّٰهُ اِلَّا بُشْرٰى لَكُمْ وَلِتَطْمَىِٕنَّ قُلُوْبُكُمْ بِهٖ ۗ وَمَا النَّصْرُ اِلَّا مِنْ عِنْدِ اللّٰهِ الْعَزِيْزِ الْحَكِيْمِۙ

wam*aa* ja'alahu **al**l*aa*hu ill*aa* busyr*aa* lakum walita*th*ma-inna quluubukum bihi wam*aa* **al**nna*sh*ru ill*aa* min 'indi **al**l*aa*hi **a**l'aziizi

Dan Allah tidak menjadikannya (pemberian bala-bantuan itu) melainkan sebagai kabar gembira bagi (kemenangan)mu, dan agar hatimu tenang karenanya. Dan tidak ada kemenangan itu, selain dari Allah Yang Mahaperkasa, Mahabijaksana.

3:127

# لِيَقْطَعَ طَرَفًا مِّنَ الَّذِيْنَ كَفَرُوْٓا اَوْ يَكْبِتَهُمْ فَيَنْقَلِبُوْا خَاۤىِٕبِيْنَ

liyaq*th*a'a *th*arafan mina **al**la*dz*iina kafaruu aw yakbitahum fayanqalibuu kh*aa*-ibiin**a**

(Allah menolong kamu dalam perang Badar dan memberi bantuan) adalah untuk membinasakan segolongan orang kafir, atau untuk menjadikan mereka hina, sehingga mereka kembali tanpa memperoleh apa pun.

3:128

# لَيْسَ لَكَ مِنَ الْاَمْرِ شَيْءٌ اَوْ يَتُوْبَ عَلَيْهِمْ اَوْ يُعَذِّبَهُمْ فَاِنَّهُمْ ظٰلِمُوْنَ

laysa laka mina **a**l-amri syay-un aw yatuuba 'alayhim aw yu'a*dzdz*ibahum fa-innahum *zhaa*limuun**a**

Itu bukan menjadi urusanmu (Muhammad) apakah Allah menerima tobat mereka, atau mengazabnya, karena sesungguhnya mereka orang-orang zalim.

3:129

# وَلِلّٰهِ مَا فِى السَّمٰوٰتِ وَمَا فِى الْاَرْضِۗ يَغْفِرُ لِمَنْ يَّشَاۤءُ وَيُعَذِّبُ مَنْ يَّشَاۤءُ ۗ وَاللّٰهُ غَفُوْرٌ رَّحِيْمٌ ࣖ

walill*aa*hi m*aa* fii **al**ssam*aa*w*aa*ti wam*aa* fii **a**l-ar*dh*i yaghfiru liman yasy*aa*u wayu'a*dzdz*ibu man yasy*aa*u wa**al**l*aa*hu ghafuurun ra*h*iim

Dan milik Allah-lah apa yang ada di langit dan apa yang ada di bumi. Dia mengampuni siapa yang Dia kehendaki, dan mengazab siapa yang Dia kehendaki. Dan Allah Maha Pengampun, Maha Penyayang.

3:130

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْا لَا تَأْكُلُوا الرِّبٰوٓا اَضْعَافًا مُّضٰعَفَةً ۖوَّاتَّقُوا اللّٰهَ لَعَلَّكُمْ تُفْلِحُوْنَۚ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu l*aa* ta/kuluu **al**rrib*aa* a*dh*'*aa*fan mu*daa*'afatan wa**i**ttaquu **al**l*aa*ha la'allakum tufli*h*

*Wahai orang-orang yang beriman! Janganlah kamu memakan riba dengan berlipat ganda dan bertakwalah kepada Allah agar kamu beruntung.*









3:131

# وَاتَّقُوا النَّارَ الَّتِيْٓ اُعِدَّتْ لِلْكٰفِرِيْنَ ۚ

wa**i**ttaquu **al**nn*aa*ra **al**latii u'iddat lilk*aa*firiin**a**

Dan peliharalah dirimu dari api neraka, yang disediakan bagi orang kafir.

3:132

# وَاَطِيْعُوا اللّٰهَ وَالرَّسُوْلَ لَعَلَّكُمْ تُرْحَمُوْنَۚ

wa-a*th*ii'uu **al**l*aa*ha wa**al**rrasuula la'allakum tur*h*amuun**a**

Dan taatlah kepada Allah dan Rasul (Muhammad), agar kamu diberi rahmat.

3:133

# ۞ وَسَارِعُوْٓا اِلٰى مَغْفِرَةٍ مِّنْ رَّبِّكُمْ وَجَنَّةٍ عَرْضُهَا السَّمٰوٰتُ وَالْاَرْضُۙ اُعِدَّتْ لِلْمُتَّقِيْنَۙ

was*aa*ri'uu il*aa* maghfiratin min rabbikum wajannatin 'ar*dh*uh*aa* **al**ssam*aa*w*aa*tu wa**a**l-ar*dh*u u'iddat lilmuttaqiin**a**

Dan bersegeralah kamu mencari ampunan dari Tuhanmu dan mendapatkan surga yang luasnya seluas langit dan bumi yang disediakan bagi orang-orang yang bertakwa,

3:134

# الَّذِيْنَ يُنْفِقُوْنَ فِى السَّرَّۤاءِ وَالضَّرَّۤاءِ وَالْكَاظِمِيْنَ الْغَيْظَ وَالْعَافِيْنَ عَنِ النَّاسِۗ وَاللّٰهُ يُحِبُّ الْمُحْسِنِيْنَۚ

**al**la*dz*iina yunfiquuna fii **al**ssarr*aa*-i wa**al***dhdh*arr*aa*-i wa**a**lk*aats*imiina **a**lghay*zh*a wa**a**l'*aa*fiina 'ani

(yaitu) orang yang berinfak, baik di waktu lapang maupun sempit, dan orang-orang yang menahan amarahnya dan memaafkan (kesalahan) orang lain. Dan Allah mencintai orang yang berbuat kebaikan,

3:135

# وَالَّذِيْنَ اِذَا فَعَلُوْا فَاحِشَةً اَوْ ظَلَمُوْٓا اَنْفُسَهُمْ ذَكَرُوا اللّٰهَ فَاسْتَغْفَرُوْا لِذُنُوْبِهِمْۗ وَمَنْ يَّغْفِرُ الذُّنُوْبَ اِلَّا اللّٰهُ ۗ وَلَمْ يُصِرُّوْا عَلٰى مَا فَعَلُوْا وَهُمْ يَعْلَمُوْنَ

wa**a**lla*dz*iina i*dzaa* fa'aluu f*aah*isyatan aw *zh*alamuu anfusahum *dz*akaruu **al**l*aa*ha fa**i**staghfaruu li*dz*unuubihim waman yaghfiru **al***dzdz*unuu

dan (juga) orang-orang yang apabila mengerjakan perbuatan keji atau menzalimi diri sendiri, (segera) mengingat Allah, lalu memohon ampunan atas dosa-dosanya, dan siapa (lagi) yang dapat mengampuni dosa-dosa selain Allah? Dan mereka tidak meneruskan perbua

3:136

# اُولٰۤىِٕكَ جَزَاۤؤُهُمْ مَّغْفِرَةٌ مِّنْ رَّبِّهِمْ وَجَنّٰتٌ تَجْرِيْ مِنْ تَحْتِهَا الْاَنْهٰرُ خٰلِدِيْنَ فِيْهَا ۗ وَنِعْمَ اَجْرُ الْعٰمِلِيْنَۗ

ul*aa*-ika jaz*aa*uhum maghfiratun min rabbihim wajann*aa*tun tajrii min ta*h*tih*aa* **a**l-anh*aa*ru kh*aa*lidiina fiih*aa* wani'ma ajru **a**l'*aa*miliin**a**

Balasan bagi mereka ialah ampunan dari Tuhan mereka dan surga-surga yang mengalir di bawahnya sungai-sungai, mereka kekal di dalamnya. Dan (itulah) sebaik-baik pahala bagi orang-orang yang beramal.

3:137

# قَدْ خَلَتْ مِنْ قَبْلِكُمْ سُنَنٌۙ فَسِيْرُوْا فِى الْاَرْضِ فَانْظُرُوْا كَيْفَ كَانَ عَاقِبَةُ الْمُكَذِّبِيْنَ

qad khalat min qablikum sunanun fasiiruu fii **a**l-ar*dh*i fa**u**n*zh*uruu kayfa k*aa*na '*aa*qibatu **a**lmuka*dzdz*ibiin**a**

Sungguh, telah berlalu sebelum kamu sunnah-sunnah (Allah), karena itu berjalanlah kamu ke (segenap penjuru) bumi dan perhatikanlah bagai-mana kesudahan orang yang mendustakan (rasul-rasul).

3:138

# هٰذَا بَيَانٌ لِّلنَّاسِ وَهُدًى وَّمَوْعِظَةٌ لِّلْمُتَّقِيْنَ

h*aadzaa* bay*aa*nun li**l**nn*aa*si wahudan wamaw'i*zh*atun lilmuttaqiin**a**

Inilah (Al-Qur'an) suatu keterangan yang jelas untuk semua manusia, dan menjadi petunjuk serta pelajaran bagi orang-orang yang bertakwa.

3:139

# وَلَا تَهِنُوْا وَلَا تَحْزَنُوْا وَاَنْتُمُ الْاَعْلَوْنَ اِنْ كُنْتُمْ مُّؤْمِنِيْنَ

wal*aa* tahinuu wal*aa* ta*h*zanuu wa-antumu **a**l-a'lawna in kuntum mu/miniin**a**

Dan janganlah kamu (merasa) lemah, dan jangan (pula) bersedih hati, sebab kamu paling tinggi (derajatnya), jika kamu orang beriman.

3:140

# اِنْ يَّمْسَسْكُمْ قَرْحٌ فَقَدْ مَسَّ الْقَوْمَ قَرْحٌ مِّثْلُهٗ ۗوَتِلْكَ الْاَيَّامُ نُدَاوِلُهَا بَيْنَ النَّاسِۚ وَلِيَعْلَمَ اللّٰهُ الَّذِيْنَ اٰمَنُوْا وَيَتَّخِذَ مِنْكُمْ شُهَدَاۤءَ ۗوَاللّٰهُ لَا يُحِبُّ الظّٰلِمِيْنَۙ

in yamsaskum qar*h*un faqad massa **a**lqawma qar*h*un mitsluhu watilka **a**l-ayy*aa*mu nud*aa*wiluh*aa* bayna **al**nn*aa*si waliya'lama **al**l*aa*hu **al**

**Jika kamu (pada Perang Uhud) mendapat luka, maka mereka pun (pada Perang Badar) mendapat luka yang serupa. Dan masa (kejayaan dan kehancuran) itu, Kami pergilirkan di antara manusia (agar mereka mendapat pelajaran), dan agar Allah membedakan orang-orang y**









3:141

# وَلِيُمَحِّصَ اللّٰهُ الَّذِيْنَ اٰمَنُوْا وَيَمْحَقَ الْكٰفِرِيْنَ

waliyuma*hh*i*sh*a **al**l*aa*hu **al**la*dz*iina *aa*manuu wayam*h*aqa **a**lk*aa*firiin**a**

dan agar Allah membersihkan orang-orang yang beriman (dari dosa mereka) dan membinasakan orang-orang kafir.

3:142

# اَمْ حَسِبْتُمْ اَنْ تَدْخُلُوا الْجَنَّةَ وَلَمَّا يَعْلَمِ اللّٰهُ الَّذِيْنَ جَاهَدُوْا مِنْكُمْ وَيَعْلَمَ الصّٰبِرِيْنَ

am *h*asibtum an tadkhuluu **a**ljannata walamm*aa* ya'lami **al**l*aa*hu **al**la*dz*iina j*aa*haduu minkum waya'lama **al***shshaa*biriin**a**

Apakah kamu mengira bahwa kamu akan masuk surga, padahal belum nyata bagi Allah orang-orang yang berjihad di antara kamu, dan belum nyata orang-orang yang sabar.

3:143

# وَلَقَدْ كُنْتُمْ تَمَنَّوْنَ الْمَوْتَ مِنْ قَبْلِ اَنْ تَلْقَوْهُۖ فَقَدْ رَاَيْتُمُوْهُ وَاَنْتُمْ تَنْظُرُوْنَ ࣖ

walaqad kuntum tamannawna **a**lmawta min qabli an talqawhu faqad ra-aytumuuhu wa-antum tan*zh*uruun**a**

Dan kamu benar-benar mengharapkan mati (syahid) sebelum kamu menghadapinya; maka (sekarang) kamu sungguh, telah melihatnya dan kamu menyaksikannya.

3:144

# وَمَا مُحَمَّدٌ اِلَّا رَسُوْلٌۚ قَدْ خَلَتْ مِنْ قَبْلِهِ الرُّسُلُ ۗ اَفَا۟ىِٕنْ مَّاتَ اَوْ قُتِلَ انْقَلَبْتُمْ عَلٰٓى اَعْقَابِكُمْ ۗ وَمَنْ يَّنْقَلِبْ عَلٰى عَقِبَيْهِ فَلَنْ يَّضُرَّ اللّٰهَ شَيْـًٔا ۗوَسَيَجْزِى اللّٰهُ الشّٰكِرِيْنَ

wam*aa* mu*h*ammadun ill*aa* rasuulun qad khalat min qablihi **al**rrusulu afa-in m*aa*ta aw qutila inqalabtum 'al*aa* a'q*aa*bikum waman yanqalib 'al*aa* 'aqibayhi falan ya*dh*urra **al**l

Dan Muhammad hanyalah seorang Rasul; sebelumnya telah berlalu beberapa rasul. Apakah jika dia wafat atau dibunuh kamu berbalik ke belakang (murtad)? Barangsiapa berbalik ke belakang, maka ia tidak akan merugikan Allah sedikit pun. Allah akan memberi balas

3:145

# وَمَا كَانَ لِنَفْسٍ اَنْ تَمُوْتَ اِلَّا بِاِذْنِ اللّٰهِ كِتٰبًا مُّؤَجَّلًا ۗ وَمَنْ يُّرِدْ ثَوَابَ الدُّنْيَا نُؤْتِهٖ مِنْهَاۚ وَمَنْ يُّرِدْ ثَوَابَ الْاٰخِرَةِ نُؤْتِهٖ مِنْهَا ۗ وَسَنَجْزِى الشّٰكِرِيْنَ

wam*aa* k*aa*na linafsin an tamuuta ill*aa* bi-i*dz*ni **al**l*aa*hi kit*aa*ban mu-ajjalan waman yurid tsaw*aa*ba **al**dduny*aa* nu/tihi minh*aa* waman yurid tsaw*aa*ba **a**

Dan setiap yang bernyawa tidak akan mati kecuali dengan izin Allah, sebagai ketetapan yang telah ditentukan waktunya. Barangsiapa menghendaki pahala dunia, niscaya Kami berikan kepadanya pahala (dunia) itu, dan barangsiapa menghendaki pahala akhirat, Kami







3:146

# وَكَاَيِّنْ مِّنْ نَّبِيٍّ قَاتَلَۙ مَعَهٗ رِبِّيُّوْنَ كَثِيْرٌۚ فَمَا وَهَنُوْا لِمَآ اَصَابَهُمْ فِيْ سَبِيْلِ اللّٰهِ وَمَا ضَعُفُوْا وَمَا اسْتَكَانُوْا ۗ وَاللّٰهُ يُحِبُّ الصّٰبِرِيْنَ

waka-ayyin min nabiyyin q*aa*tala ma'ahu ribbiyyuuna katsiirun fam*aa* wahanuu lim*aa* a*shaa*bahum fii sabiili **al**l*aa*hi wam*aa* *dh*a'ufuu wam*aa* istak*aa*nuu wa**al**l*aa*

Dan betapa banyak nabi yang berperang didampingi sejumlah besar dari pengikut(nya) yang bertakwa. Mereka tidak (menjadi) lemah karena bencana yang menimpanya di jalan Allah, tidak patah semangat dan tidak (pula) menyerah (kepada musuh). Dan Allah mencinta

3:147

# وَمَا كَانَ قَوْلَهُمْ اِلَّآ اَنْ قَالُوْا رَبَّنَا اغْفِرْ لَنَا ذُنُوْبَنَا وَاِسْرَافَنَا فِيْٓ اَمْرِنَا وَثَبِّتْ اَقْدَامَنَا وَانْصُرْنَا عَلَى الْقَوْمِ الْكٰفِرِيْنَ

wam*aa* k*aa*na qawlahum ill*aa* an q*aa*luu rabban*aa* ighfir lan*aa* *dz*unuuban*aa* wa-isr*aa*fan*aa* fii amrin*aa* watsabbit aqd*aa*man*aa* wa**u**n*sh*urn*aa* 'al

Dan tidak lain ucapan mereka hanyalah doa, “Ya Tuhan kami, ampunilah dosa-dosa kami dan tindakan-tindakan kami yang berlebihan (dalam) urusan kami dan tetapkanlah pendirian kami, dan tolonglah kami terhadap orang-orang kafir.”

3:148

# فَاٰتٰىهُمُ اللّٰهُ ثَوَابَ الدُّنْيَا وَحُسْنَ ثَوَابِ الْاٰخِرَةِ ۗ وَاللّٰهُ يُحِبُّ الْمُحْسِنِيْنَ ࣖ

fa*aa*t*aa*humu **al**l*aa*hu tsaw*aa*ba **al**dduny*aa* wa*h*usna tsaw*aa*bi **a**l-*aa*khirati wa**al**l*aa*hu yu*h*ibbu **a**lmu*h*s

Maka Allah memberi mereka pahala di dunia dan pahala yang baik di akhirat. Dan Allah mencintai orang-orang yang berbuat kebaikan.

3:149

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْٓا اِنْ تُطِيْعُوا الَّذِيْنَ كَفَرُوْا يَرُدُّوْكُمْ عَلٰٓى اَعْقَابِكُمْ فَتَنْقَلِبُوْا خٰسِرِيْنَ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu in tu*th*ii'uu **al**la*dz*iina kafaruu yarudduukum 'al*aa* a'q*aa*bikum fatanqalibuu kh*aa*siriin**a**

Wahai orang-orang yang beriman! Jika kamu menaati orang-orang yang kafir, niscaya mereka akan mengembalikan kamu ke belakang (murtad), maka kamu akan kembali menjadi orang yang rugi.

3:150

# بَلِ اللّٰهُ مَوْلٰىكُمْ ۚ وَهُوَ خَيْرُ النّٰصِرِيْنَ

bali **al**l*aa*hu mawl*aa*kum wahuwa khayru **al**nn*aas*iriin**a**

Tetapi hanya Allah-lah pelindungmu, dan Dia penolong yang terbaik.

3:151

# سَنُلْقِيْ فِيْ قُلُوْبِ الَّذِيْنَ كَفَرُوا الرُّعْبَ بِمَٓا اَشْرَكُوْا بِاللّٰهِ مَا لَمْ يُنَزِّلْ بِهٖ سُلْطٰنًا ۚ وَمَأْوٰىهُمُ النَّارُ ۗ وَبِئْسَ مَثْوَى الظّٰلِمِيْنَ

sanulqii fii quluubi **al**la*dz*iina kafaruu **al**rru'ba bim*aa* asyrakuu bi**al**l*aa*hi m*aa* lam yunazzil bihi sul*thaa*nan wama/w*aa*humu **al**nn*aa*ru wa

Akan Kami masukkan rasa takut ke dalam hati orang-orang kafir, karena mereka mempersekutukan Allah dengan sesuatu yang Allah tidak menurunkan keterangan tentang itu. Dan tempat kembali mereka ialah neraka. Dan (itulah) seburuk-buruk tempat tinggal (ba

3:152

# وَلَقَدْ صَدَقَكُمُ اللّٰهُ وَعْدَهٗٓ اِذْ تَحُسُّوْنَهُمْ بِاِذْنِهٖ ۚ حَتّٰىٓ اِذَا فَشِلْتُمْ وَتَنَازَعْتُمْ فِى الْاَمْرِ وَعَصَيْتُمْ مِّنْۢ بَعْدِ مَآ اَرٰىكُمْ مَّا تُحِبُّوْنَ ۗ مِنْكُمْ مَّنْ يُّرِيْدُ الدُّنْيَا وَمِنْكُمْ مَّنْ يُّرِيْدُ الْا

walaqad *sh*adaqakumu **al**l*aa*hu wa'dahu i*dz* ta*h*ussuunahum bi-i*dz*nihi *h*att*aa* i*dzaa* fasyiltum watan*aa*za'tum fii **a**l-amri wa'a*sh*aytum min ba'di m*aa* ar

Dan sungguh, Allah telah memenuhi janji-Nya kepadamu, ketika kamu membunuh mereka dengan izin-Nya sampai pada saat kamu lemah dan berselisih dalam urusan itu dan mengabaikan perintah Rasul setelah Allah memperlihatkan kepadamu apa yang kamu sukai. Di anta

3:153

# ۞ اِذْ تُصْعِدُوْنَ وَلَا تَلْوٗنَ عَلٰٓى اَحَدٍ وَّالرَّسُوْلُ يَدْعُوْكُمْ فِيْٓ اُخْرٰىكُمْ فَاَثَابَكُمْ غَمًّا ۢبِغَمٍّ لِّكَيْلَا تَحْزَنُوْا عَلٰى مَا فَاتَكُمْ وَلَا مَآ اَصَابَكُمْ ۗ وَاللّٰهُ خَبِيْرٌ ۢبِمَا تَعْمَلُوْنَ

i*dz* tu*sh*'iduuna wal*aa* talwuuna 'al*aa* a*h*adin wa**al**rrasuulu yad'uukum fii ukhr*aa*kum fa-ats*aa*bakum ghamman bighammin likay l*aa* ta*h*zanuu 'al*aa* m*aa* f*aa*takum wal

(Ingatlah) ketika kamu lari dan tidak menoleh kepada siapa pun, sedang Rasul (Muhammad) yang berada di antara (kawan-kawan)mu yang lain memanggil kamu (kelompok yang lari), karena itu Allah menimpakan kepadamu kesedihan demi kesedihan, agar kamu tidak ber

3:154

# ثُمَّ اَنْزَلَ عَلَيْكُمْ مِّنْۢ بَعْدِ الْغَمِّ اَمَنَةً نُّعَاسًا يَّغْشٰى طَۤاىِٕفَةً مِّنْكُمْ ۙ وَطَۤاىِٕفَةٌ قَدْ اَهَمَّتْهُمْ اَنْفُسُهُمْ يَظُنُّوْنَ بِاللّٰهِ غَيْرَ الْحَقِّ ظَنَّ الْجَاهِلِيَّةِ ۗ يَقُوْلُوْنَ هَلْ لَّنَا مِنَ الْاَمْرِ مِنْ ش

tsumma anzala 'alaykum min ba'di **a**lghammi amanatan nu'*aa*san yaghsy*aa* *thaa*-ifatan minkum wa*thaa*-ifatun qad ahammat-hum anfusuhum ya*zh*unnuuna bi**al**l*aa*hi ghayra

Kemudian setelah kamu ditimpa kesedihan, Dia menurunkan rasa aman kepadamu (berupa) kantuk yang meliputi segolongan dari kamu, sedangkan segolongan lagi telah dicemaskan oleh diri mereka sendiri; mereka menyangka yang tidak benar terhadap Allah seperti sa







3:155

# اِنَّ الَّذِيْنَ تَوَلَّوْا مِنْكُمْ يَوْمَ الْتَقَى الْجَمْعٰنِۙ اِنَّمَا اسْتَزَلَّهُمُ الشَّيْطٰنُ بِبَعْضِ مَا كَسَبُوْا ۚ وَلَقَدْ عَفَا اللّٰهُ عَنْهُمْ ۗ اِنَّ اللّٰهَ غَفُوْرٌ حَلِيْمٌ ࣖ

inna **al**la*dz*iina tawallaw minkum yawma iltaq*aa* **a**ljam'*aa*ni innam*aa* istazallahumu **al**sysyay*thaa*nu biba'*dh*i m*aa* kasabuu walaqad 'af*aa* **al**

**Sesungguhnya orang-orang yang berpaling di antara kamu ketika terjadi pertemuan (pertempuran) antara dua pasukan itu, sesungguhnya mereka digelincirkan oleh setan, disebabkan sebagian kesalahan (dosa) yang telah mereka perbuat (pada masa lampau), tetapi A**









3:156

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْا لَا تَكُوْنُوْا كَالَّذِيْنَ كَفَرُوْا وَقَالُوْا لِاِخْوَانِهِمْ اِذَا ضَرَبُوْا فِى الْاَرْضِ اَوْ كَانُوْا غُزًّى لَّوْ كَانُوْا عِنْدَنَا مَا مَاتُوْا وَمَا قُتِلُوْاۚ لِيَجْعَلَ اللّٰهُ ذٰلِكَ حَسْرَةً فِيْ قُلُوْبِه

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu l*aa* takuunuu ka**a**lla*dz*iina kafaruu waq*aa*luu li-ikhw*aa*nihim i*dzaa* *dh*arabuu fii **a**l-ar*dh*i aw k*aa<*

Wahai orang-orang yang beriman! Janganlah kamu seperti orang-orang kafir yang mengatakan kepada saudara-saudaranya apabila mereka mengadakan perjalanan di bumi atau berperang, “Sekiranya mereka tetap bersama kita, tentulah mereka tidak mati dan tidak terb







3:157

# وَلَىِٕنْ قُتِلْتُمْ فِيْ سَبِيْلِ اللّٰهِ اَوْ مُتُّمْ لَمَغْفِرَةٌ مِّنَ اللّٰهِ وَرَحْمَةٌ خَيْرٌ مِّمَّا يَجْمَعُوْنَ

wala-in qutiltum fii sabiili **al**l*aa*hi aw muttum lamaghfiratun mina **al**l*aa*hi wara*h*matun khayrun mimm*aa* yajma'uun**a**

Dan sungguh, sekiranya kamu gugur di jalan Allah atau mati, sungguh, pastilah ampunan Allah dan rahmat-Nya lebih baik (bagimu) daripada apa (harta rampasan) yang mereka kumpulkan.

3:158

# وَلَىِٕنْ مُّتُّمْ اَوْ قُتِلْتُمْ لَاِلَى اللّٰهِ تُحْشَرُوْنَ

wala-in muttum aw qutiltum la-il*aa* **al**l*aa*hi tu*h*syaruun**a**

Dan sungguh, sekiranya kamu mati atau gugur, pastilah kepada Allah kamu dikumpulkan.

3:159

# فَبِمَا رَحْمَةٍ مِّنَ اللّٰهِ لِنْتَ لَهُمْ ۚ وَلَوْ كُنْتَ فَظًّا غَلِيْظَ الْقَلْبِ لَانْفَضُّوْا مِنْ حَوْلِكَ ۖ فَاعْفُ عَنْهُمْ وَاسْتَغْفِرْ لَهُمْ وَشَاوِرْهُمْ فِى الْاَمْرِۚ فَاِذَا عَزَمْتَ فَتَوَكَّلْ عَلَى اللّٰهِ ۗ اِنَّ اللّٰهَ يُحِبُّ الْم

fabim*aa* ra*h*matin mina **al**l*aa*hi linta lahum walaw kunta fa*zhzh*an ghalii*zh*a **a**lqalbi la**i**nfa*dhdh*uu min *h*awlika fa**u**'fu 'anhum wa**i**

**Maka berkat rahmat Allah engkau (Muhammad) berlaku lemah lembut terhadap mereka. Sekiranya engkau bersikap keras dan berhati kasar, tentulah mereka menjauhkan diri dari sekitarmu. Karena itu maafkanlah mereka dan mohonkanlah ampunan untuk mereka, dan berm**









3:160

# اِنْ يَّنْصُرْكُمُ اللّٰهُ فَلَا غَالِبَ لَكُمْ ۚ وَاِنْ يَّخْذُلْكُمْ فَمَنْ ذَا الَّذِيْ يَنْصُرُكُمْ مِّنْۢ بَعْدِهٖ ۗ وَعَلَى اللّٰهِ فَلْيَتَوَكَّلِ الْمُؤْمِنُوْنَ

in yan*sh*urkumu **al**l*aa*hu fal*aa* gh*aa*liba lakum wa-in yakh*dz*ulkum faman *dzaa* **al**la*dz*ii yan*sh*urukum min ba'dihi wa'al*aa* **al**l*aa*hi falyatawakka

Jika Allah menolong kamu, maka tidak ada yang dapat mengalahkanmu, tetapi jika Allah membiarkan kamu (tidak memberi pertolongan), maka siapa yang dapat menolongmu setelah itu? Karena itu, hendaklah kepada Allah saja orang-orang mukmin bertawakal.

3:161

# وَمَا كَانَ لِنَبِيٍّ اَنْ يَّغُلَّ ۗوَمَنْ يَّغْلُلْ يَأْتِ بِمَا غَلَّ يَوْمَ الْقِيٰمَةِ ۚ ثُمَّ تُوَفّٰى كُلُّ نَفْسٍ مَّا كَسَبَتْ وَهُمْ لَا يُظْلَمُوْنَ

wam*aa* k*aa*na linabiyyin an yaghulla waman yaghlul ya/ti bim*aa* ghalla yawma **a**lqiy*aa*mati tsumma tuwaff*aa* kullu nafsin m*aa* kasabat wahum l*aa* yu*zh*lamuun**a**v

Dan tidak mungkin seorang nabi berkhianat (dalam urusan harta rampasan perang). Barangsiapa berkhianat, niscaya pada hari Kiamat dia akan datang membawa apa yang dikhianatkannya itu. Kemudian setiap orang akan diberi balasan yang sempurna sesuai dengan ap

3:162

# اَفَمَنِ اتَّبَعَ رِضْوَانَ اللّٰهِ كَمَنْۢ بَاۤءَ بِسَخَطٍ مِّنَ اللّٰهِ وَمَأْوٰىهُ جَهَنَّمُ ۗ وَبِئْسَ الْمَصِيْرُ

afamani ittaba'a ri*dh*w*aa*na **al**l*aa*hi kaman b*aa*-a bisakha*th*in mina **al**l*aa*hi wama/w*aa*hu jahannamu wabi/sa **a**lma*sh*iir**u**

Maka adakah orang yang mengikuti keridaan Allah sama dengan orang yang kembali membawa kemurkaan dari Allah dan tempatnya di neraka Jahanam? Itulah seburuk-buruk tempat kembali.

3:163

# هُمْ دَرَجٰتٌ عِنْدَ اللّٰهِ ۗ وَاللّٰهُ بَصِيْرٌ ۢبِمَا يَعْمَلُوْنَ

hum daraj*aa*tun 'inda **al**l*aa*hi wa**al**l*aa*hu ba*sh*iirun bim*aa* ya'maluun**a**

(Kedudukan) mereka itu bertingkat-tingkat di sisi Allah, dan Allah Maha Melihat apa yang mereka kerjakan.

3:164

# لَقَدْ مَنَّ اللّٰهُ عَلَى الْمُؤْمِنِيْنَ اِذْ بَعَثَ فِيْهِمْ رَسُوْلًا مِّنْ اَنْفُسِهِمْ يَتْلُوْا عَلَيْهِمْ اٰيٰتِهٖ وَيُزَكِّيْهِمْ وَيُعَلِّمُهُمُ الْكِتٰبَ وَالْحِكْمَةَۚ وَاِنْ كَانُوْا مِنْ قَبْلُ لَفِيْ ضَلٰلٍ مُّبِيْنٍ

aqad manna **al**l*aa*hu 'al*aa* **a**lmu/miniina i*dz* ba'atsa fiihim rasuulan min anfusihim yatluu 'alayhim *aa*y*aa*tihi wayuzakkiihim wayu'allimuhumu **a**lkit*aa*ba wa**a**

**Sungguh, Allah telah memberi karunia kepada orang-orang beriman ketika (Allah) mengutus seorang Rasul (Muhammad) di tengah-tengah mereka dari kalangan mereka sendiri, yang membacakan kepada mereka ayat-ayat-Nya, menyucikan (jiwa) mereka, dan mengajarkan k**









3:165

# اَوَلَمَّآ اَصَابَتْكُمْ مُّصِيْبَةٌ قَدْ اَصَبْتُمْ مِّثْلَيْهَاۙ قُلْتُمْ اَنّٰى هٰذَا ۗ قُلْ هُوَ مِنْ عِنْدِ اَنْفُسِكُمْ ۗ اِنَّ اللّٰهَ عَلٰى كُلِّ شَيْءٍ قَدِيْرٌ

awa lamm*aa* a*shaa*batkum mu*sh*iibatun qad a*sh*abtum mitslayh*aa* qultum ann*aa* h*aadzaa* qul huwa min 'indi anfusikum inna **al**l*aa*ha 'al*aa* kulli syay-in qadiir**un**

Dan mengapa kamu (heran) ketika ditimpa musibah (kekalahan pada Perang Uhud), padahal kamu telah menimpakan musibah dua kali lipat (kepada musuh-musuhmu pada Perang Badar) kamu berkata, “Dari mana datangnya (kekalahan) ini?” Katakanlah, “Itu dari (kesalah

3:166

# وَمَآ اَصَابَكُمْ يَوْمَ الْتَقَى الْجَمْعٰنِ فَبِاِذْنِ اللّٰهِ وَلِيَعْلَمَ الْمُؤْمِنِيْنَۙ

wam*aa* a*shaa*bakum yawma iltaq*aa* **a**ljam'*aa*ni fabi-i*dz*ni **al**l*aa*hi waliya'lama **a**lmu/miniin**a**

Dan apa yang menimpa kamu ketika terjadi pertemuan (pertempuran) antara dua pasukan itu adalah dengan izin Allah, dan agar Allah menguji siapa orang (yang benar-benar) beriman.

3:167

# وَلِيَعْلَمَ الَّذِيْنَ نَافَقُوْا ۖوَقِيْلَ لَهُمْ تَعَالَوْا قَاتِلُوْا فِيْ سَبِيْلِ اللّٰهِ اَوِ ادْفَعُوْا ۗ قَالُوْا لَوْ نَعْلَمُ قِتَالًا لَّاتَّبَعْنٰكُمْ ۗ هُمْ لِلْكُفْرِ يَوْمَىِٕذٍ اَقْرَبُ مِنْهُمْ لِلْاِيْمَانِ ۚ يَقُوْلُوْنَ بِاَفْوَاهِهِم

waliya'lama **al**la*dz*iina n*aa*faquu waqiila lahum ta'*aa*law q*aa*tiluu fii sabiili **al**l*aa*hi awi idfa'uu q*aa*luu law na'lamu qit*aa*lan la**i**ttaba'n*aa*kum hum lilk

Dan untuk menguji orang-orang yang munafik, kepada mereka dikatakan, “Marilah berperang di jalan Allah atau pertahankanlah (dirimu).” Mereka berkata, “Sekiranya kami mengetahui (bagaimana cara) berperang, tentulah kami mengikuti kamu.” Mereka pada hari it

3:168

# اَلَّذِيْنَ قَالُوْا لِاِخْوَانِهِمْ وَقَعَدُوْا لَوْ اَطَاعُوْنَا مَا قُتِلُوْا ۗ قُلْ فَادْرَءُوْا عَنْ اَنْفُسِكُمُ الْمَوْتَ اِنْ كُنْتُمْ صٰدِقِيْنَ

**al**la*dz*iina q*aa*luu li-ikhw*aa*nihim waqa'aduu law a*thaa*'uun*aa* m*aa* qutiluu qul fa**i**drauu 'an anfusikumu **a**lmawta in kuntum *shaa*diqiin**a**

(Mereka itu adalah) orang-orang yang berkata kepada saudara-saudaranya dan mereka tidak turut pergi berperang, “Sekiranya mereka mengikuti kita, tentulah mereka tidak terbunuh.” Katakanlah, “Cegahlah kematian itu dari dirimu, jika kamu orang yang benar.”

3:169

# وَلَا تَحْسَبَنَّ الَّذِيْنَ قُتِلُوْا فِيْ سَبِيْلِ اللّٰهِ اَمْوَاتًا ۗ بَلْ اَحْيَاۤءٌ عِنْدَ رَبِّهِمْ يُرْزَقُوْنَۙ

wal*aa* ta*h*sabanna **al**la*dz*iina qutiluu fii sabiili **al**l*aa*hi amw*aa*tan bal a*h*y*aa*un 'inda rabbihim yurzaquun**a**

Dan jangan sekali-kali kamu mengira bahwa orang-orang yang gugur di jalan Allah itu mati; sebenarnya mereka itu hidup di sisi Tuhannya mendapat rezeki,

3:170

# فَرِحِيْنَ بِمَآ اٰتٰىهُمُ اللّٰهُ مِنْ فَضْلِهٖۙ وَيَسْتَبْشِرُوْنَ بِالَّذِيْنَ لَمْ يَلْحَقُوْا بِهِمْ مِّنْ خَلْفِهِمْ ۙ اَلَّا خَوْفٌ عَلَيْهِمْ وَلَا هُمْ يَحْزَنُوْنَۘ

fari*h*iina bim*aa* *aa*t*aa*humu **al**l*aa*hu min fa*dh*lihi wayastabsyiruuna bi**a**lla*dz*iina lam yal*h*aquu bihim min khalfihim **al**l*aa* khawfun 'alayhim wal*aa*

Mereka bergembira dengan karunia yang diberikan Allah kepadanya, dan bergirang hati terhadap orang yang masih tinggal di belakang yang belum menyusul mereka, bahwa tidak ada rasa takut pada mereka dan mereka tidak bersedih hati.







3:171

# ۞ يَسْتَبْشِرُوْنَ بِنِعْمَةٍ مِّنَ اللّٰهِ وَفَضْلٍۗ وَاَنَّ اللّٰهَ لَا يُضِيْعُ اَجْرَ الْمُؤْمِنِيْنَ ࣖ

yastabsyiruuna bini'matin mina **al**l*aa*hi wafa*dh*lin wa-anna **al**l*aa*ha l*aa* yu*dh*ii'u ajra **a**lmu/miniin**a**

Mereka bergirang hati dengan nikmat dan karunia dari Allah. Dan sungguh, Allah tidak menyia-nyiakan pahala orang-orang yang beriman,

3:172

# اَلَّذِيْنَ اسْتَجَابُوْا لِلّٰهِ وَالرَّسُوْلِ مِنْۢ بَعْدِ مَآ اَصَابَهُمُ الْقَرْحُ ۖ لِلَّذِيْنَ اَحْسَنُوْا مِنْهُمْ وَاتَّقَوْا اَجْرٌ عَظِيْمٌۚ

**al**la*dz*iina istaj*aa*buu lill*aa*hi wa**al**rrasuuli min ba'di m*aa* a*shaa*bahumu **a**lqar*h*u lilla*dz*iina a*h*sanuu minhum wa**i**ttaqaw ajrun 'a*zh*

*(yaitu) orang-orang yang menaati (perintah) Allah dan Rasul setelah mereka mendapat luka (dalam Perang Uhud). Orang-orang yang berbuat kebajikan dan bertakwa di antara mereka mendapat pahala yang besar.*









3:173

# اَلَّذِيْنَ قَالَ لَهُمُ النَّاسُ اِنَّ النَّاسَ قَدْ جَمَعُوْا لَكُمْ فَاخْشَوْهُمْ فَزَادَهُمْ اِيْمَانًاۖ وَّقَالُوْا حَسْبُنَا اللّٰهُ وَنِعْمَ الْوَكِيْلُ

**al**la*dz*iina q*aa*la lahumu **al**nn*aa*su inna **al**nn*aa*sa qad jama'uu lakum fa**i**khsyawhum faz*aa*dahum iim*aa*nan waq*aa*luu *h*asbun*aa*

(Yaitu) orang-orang (yang menaati Allah dan Rasul) yang ketika ada orang-orang mengatakan kepadanya, “Orang-orang (Quraisy) telah mengumpulkan pasukan untuk menyerang kamu, karena itu takutlah kepada mereka,” ternyata (ucapan) itu menambah (kuat) iman mer







3:174

# فَانْقَلَبُوْا بِنِعْمَةٍ مِّنَ اللّٰهِ وَفَضْلٍ لَّمْ يَمْسَسْهُمْ سُوْۤءٌۙ وَّاتَّبَعُوْا رِضْوَانَ اللّٰهِ ۗ وَاللّٰهُ ذُوْ فَضْلٍ عَظِيْمٍ

fa**i**nqalabuu bini'matin mina **al**l*aa*hi wafa*dh*lin lam yamsas-hum suu-un wa**i**ttaba'uu ri*dh*w*aa*na **al**l*aa*hi wa**al**l*aa*hu *dz*uu fa*dh<*

Maka mereka kembali dengan nikmat dan karunia (yang besar) dari Allah, mereka tidak ditimpa suatu bencana dan mereka mengikuti keridaan Allah. Allah mempunyai karunia yang besar.







3:175

# اِنَّمَا ذٰلِكُمُ الشَّيْطٰنُ يُخَوِّفُ اَوْلِيَاۤءَهٗۖ فَلَا تَخَافُوْهُمْ وَخَافُوْنِ اِنْ كُنْتُمْ مُّؤْمِنِيْنَ

innam*aa* *dzaa*likumu **al**sysyay*thaa*nu yukhawwifu awliy*aa*-ahu fal*aa* takh*aa*fuuhum wakh*aa*fuuni in kuntum mu/miniin

Sesungguhnya mereka hanyalah setan yang menakut-nakuti (kamu) dengan teman-teman setianya, karena itu janganlah kamu takut kepada mereka, tetapi takutlah kepada-Ku, jika kamu orang-orang beriman.

3:176

# وَلَا يَحْزُنْكَ الَّذِيْنَ يُسَارِعُوْنَ فِى الْكُفْرِۚ اِنَّهُمْ لَنْ يَّضُرُّوا اللّٰهَ شَيْـًٔا ۗ يُرِيْدُ اللّٰهُ اَلَّا يَجْعَلَ لَهُمْ حَظًّا فِى الْاٰخِرَةِ وَلَهُمْ عَذَابٌ عَظِيْمٌۚ

wal*aa* ya*h*zunka **al**la*dz*iina yus*aa*ri'uuna fii **a**lkufri innahum lan ya*dh*urruu **al**l*aa*ha syay-an yuriidu **al**l*aa*hu **al**l*aa* yaj

Dan janganlah engkau (Muhammad) dirisaukan oleh orang-orang yang dengan mudah kembali menjadi kafir; sesungguhnya sedikit pun mereka tidak merugikan Allah. Allah tidak akan memberi bagian (pahala) kepada mereka di akhirat, dan mereka akan mendapat azab ya

3:177

# اِنَّ الَّذِيْنَ اشْتَرَوُا الْكُفْرَ بِالْاِيْمَانِ لَنْ يَّضُرُّوا اللّٰهَ شَيْـًٔاۚ وَلَهُمْ عَذَابٌ اَلِيْمٌ

inna **al**la*dz*iina isytarawuu **a**lkufra bi**a**l-iim*aa*ni lan ya*dh*urruu **al**l*aa*ha syay-an walahum 'a*dzaa*bun **a**liim**un**

Sesungguhnya orang-orang yang membeli kekafiran dengan iman, sedikit pun tidak merugikan Allah; dan mereka akan mendapat azab yang pedih.

3:178

# وَلَا يَحْسَبَنَّ الَّذِيْنَ كَفَرُوْٓا اَنَّمَا نُمْلِيْ لَهُمْ خَيْرٌ لِّاَنْفُسِهِمْ ۗ اِنَّمَا نُمْلِيْ لَهُمْ لِيَزْدَادُوْٓا اِثْمًا ۚ وَلَهُمْ عَذَابٌ مُّهِيْنٌ

wal*aa* ya*h*sabanna **al**la*dz*iina kafaruu annam*aa* numlii lahum khayrun li-anfusihim innam*aa* numlii lahum liyazd*aa*duu itsman walahum 'a*dzaa*bun muhiin**un**

Dan jangan sekali-kali orang-orang kafir itu mengira bahwa tenggang waktu yang Kami berikan kepada mereka lebih baik baginya. Sesungguhnya tenggang waktu yang Kami berikan kepada mereka hanyalah agar dosa mereka semakin bertambah; dan mereka akan mendapat

3:179

# مَا كَانَ اللّٰهُ لِيَذَرَ الْمُؤْمِنِيْنَ عَلٰى مَآ اَنْتُمْ عَلَيْهِ حَتّٰى يَمِيْزَ الْخَبِيْثَ مِنَ الطَّيِّبِ ۗ وَمَا كَانَ اللّٰهُ لِيُطْلِعَكُمْ عَلَى الْغَيْبِ وَلٰكِنَّ اللّٰهَ يَجْتَبِيْ مِنْ رُّسُلِهٖ مَنْ يَّشَاۤءُ ۖ فَاٰمِنُوْا بِاللّٰهِ وَر

m*aa* k*aa*na **al**l*aa*hu liya*dz*ara **a**lmu/miniina 'al*aa* m*aa* antum 'alayhi *h*att*aa* yamiiza **a**lkhabiitsa mina **al***ththh*ayyibi wam*aa*

Allah tidak akan membiarkan orang-orang yang beriman sebagaimana dalam keadaan kamu sekarang ini, sehingga Dia membedakan yang buruk dari yang baik. Allah tidak akan memperlihatkan kepadamu hal-hal yang gaib, tetapi Allah memilih siapa yang Dia kehendaki

3:180

# وَلَا يَحْسَبَنَّ الَّذِيْنَ يَبْخَلُوْنَ بِمَآ اٰتٰىهُمُ اللّٰهُ مِنْ فَضْلِهٖ هُوَ خَيْرًا لَّهُمْ ۗ بَلْ هُوَ شَرٌّ لَّهُمْ ۗ سَيُطَوَّقُوْنَ مَا بَخِلُوْا بِهٖ يَوْمَ الْقِيٰمَةِ ۗ وَلِلّٰهِ مِيْرَاثُ السَّمٰوٰتِ وَالْاَرْضِۗ وَاللّٰهُ بِمَا تَعْمَلُ

wal*aa* ya*h*sabanna **al**la*dz*iina yabkhaluuna bim*aa* *aa*t*aa*hummu **al**l*aa*hu min fa*dh*lihi huwa khayran lahum bal huwa syarrun lahum sayu*th*awwaquuna m*aa* bakhiluu bi

Dan jangan sekali-kali orang-orang yang kikir dengan apa yang diberikan Allah kepada mereka dari karunia-Nya mengira bahwa (kikir) itu baik bagi mereka, padahal (kikir) itu buruk bagi mereka. Apa (harta) yang mereka kikirkan itu akan dikalungkan (di leher

3:181

# لَقَدْ سَمِعَ اللّٰهُ قَوْلَ الَّذِيْنَ قَالُوْٓا اِنَّ اللّٰهَ فَقِيْرٌ وَّنَحْنُ اَغْنِيَاۤءُ ۘ سَنَكْتُبُ مَا قَالُوْا وَقَتْلَهُمُ الْاَنْۢبِيَاۤءَ بِغَيْرِ حَقٍّۙ وَّنَقُوْلُ ذُوْقُوْا عَذَابَ الْحَرِيْقِ

laqad sami'a **al**l*aa*hu qawla **al**la*dz*iina q*aa*luu inna **al**l*aa*ha faqiirun wana*h*nu aghniy*aa*un sanaktubu m*aa* q*aa*luu waqatlahumu **a**l-anbiy*a*

Sungguh, Allah telah mendengar perkataan orang-orang (Yahudi) yang mengatakan, “Sesungguhnya Allah itu miskin dan kami kaya.” Kami akan mencatat perkataan mereka dan perbuatan mereka membunuh nabi-nabi tanpa hak (alasan yang benar), dan Kami akan mengatak







3:182

# ذٰلِكَ بِمَا قَدَّمَتْ اَيْدِيْكُمْ وَاَنَّ اللّٰهَ لَيْسَ بِظَلَّامٍ لِّلْعَبِيْدِۚ

*dzaa*lika bim*aa* qaddamat aydiikum wa-anna **al**l*aa*ha laysa bi*zh*all*aa*min lil'abiid**i**

Demikian itu disebabkan oleh perbuatan tanganmu sendiri, dan sesungguhnya Allah tidak menzalimi hamba-hamba-Nya.

3:183

# اَلَّذِيْنَ قَالُوْٓا اِنَّ اللّٰهَ عَهِدَ اِلَيْنَآ اَلَّا نُؤْمِنَ لِرَسُوْلٍ حَتّٰى يَأْتِيَنَا بِقُرْبَانٍ تَأْكُلُهُ النَّارُ ۗ قُلْ قَدْ جَاۤءَكُمْ رُسُلٌ مِّنْ قَبْلِيْ بِالْبَيِّنٰتِ وَبِالَّذِيْ قُلْتُمْ فَلِمَ قَتَلْتُمُوْهُمْ اِنْ كُنْتُمْ صٰد

**al**la*dz*iina q*aa*luu inna **al**l*aa*ha 'ahida ilayn*aa* **al**l*aa* nu/mina lirasuulin *h*att*aa* ya/tiyan*aa* biqurb*aa*nin ta/kuluhu **al**nn*aa*

(Yaitu) orang-orang (Yahudi) yang mengatakan, “Sesungguhnya Allah telah memerintahkan kepada kami, agar kami tidak beriman kepada seorang rasul, sebelum dia mendatangkan kepada kami kurban yang dimakan api.” Katakanlah (Muhammad), “Sungguh, beberapa orang

3:184

# فَاِنْ كَذَّبُوْكَ فَقَدْ كُذِّبَ رُسُلٌ مِّنْ قَبْلِكَ جَاۤءُوْ بِالْبَيِّنٰتِ وَالزُّبُرِ وَالْكِتٰبِ الْمُنِيْرِ

fa-in ka*dzdz*abuuka faqad ku*dzdz*iba rusulun min qablika j*aa*uu bi**a**lbayyin*aa*ti wa**al**zzuburi wa**a**lkit*aa*bi **a**lmuniir**i**

Maka jika mereka mendustakan engkau (Muhammad), maka (ketahuilah) rasul-rasul sebelum engkau pun telah didustakan (pula), mereka membawa mukjizat-mukjizat yang nyata, Zubur dan Kitab yang memberi penjelasan yang sempurna.

3:185

# كُلُّ نَفْسٍ ذَاۤىِٕقَةُ الْمَوْتِۗ وَاِنَّمَا تُوَفَّوْنَ اُجُوْرَكُمْ يَوْمَ الْقِيٰمَةِ ۗ فَمَنْ زُحْزِحَ عَنِ النَّارِ وَاُدْخِلَ الْجَنَّةَ فَقَدْ فَازَ ۗ وَمَا الْحَيٰوةُ الدُّنْيَآ اِلَّا مَتَاعُ الْغُرُوْرِ

kullu nafsin *dzaa*-iqatu **a**lmawti wa-innam*aa* tuwaffawna ujuurakum yawma **a**lqiy*aa*mati faman zu*h*zi*h*a 'ani **al**nn*aa*ri waudkhila **a**ljannata faqad f*aa*

*Setiap yang bernyawa akan merasakan mati. Dan hanya pada hari Kiamat sajalah diberikan dengan sempurna balasanmu. Barangsiapa dijauhkan dari neraka dan dimasukkan ke dalam surga, sungguh, dia memperoleh kemenangan. Kehidupan dunia hanyalah kesenangan yang*









3:186

# ۞ لَتُبْلَوُنَّ فِيْٓ اَمْوَالِكُمْ وَاَنْفُسِكُمْۗ وَلَتَسْمَعُنَّ مِنَ الَّذِيْنَ اُوْتُوا الْكِتٰبَ مِنْ قَبْلِكُمْ وَمِنَ الَّذِيْنَ اَشْرَكُوْٓا اَذًى كَثِيْرًا ۗ وَاِنْ تَصْبِرُوْا وَتَتَّقُوْا فَاِنَّ ذٰلِكَ مِنْ عَزْمِ الْاُمُوْرِ

latublawunna fii amw*aa*likum wa-anfusikum walatasma'unna mina **al**la*dz*iina uutuu **a**lkit*aa*ba min qablikum wamina **al**la*dz*iina asyrakuu a*dz*an katsiiran wa-in ta*sh*biruu wa

Kamu pasti akan diuji dengan hartamu dan dirimu. Dan pasti kamu akan mendengar banyak hal yang sangat menyakitkan hati dari orang-orang yang diberi Kitab sebelum kamu dan dari orang-orang musyrik. Jika kamu bersabar dan bertakwa, maka sesungguhnya yang de

3:187

# وَاِذْ اَخَذَ اللّٰهُ مِيْثَاقَ الَّذِيْنَ اُوْتُوا الْكِتٰبَ لَتُبَيِّنُنَّهٗ لِلنَّاسِ وَلَا تَكْتُمُوْنَهٗۖ فَنَبَذُوْهُ وَرَاۤءَ ظُهُوْرِهِمْ وَاشْتَرَوْا بِهٖ ثَمَنًا قَلِيْلًا ۗ فَبِئْسَ مَا يَشْتَرُوْنَ

wa-i*dz* akha*dz*a **al**l*aa*hu miits*aa*qa **al**la*dz*iina uutuu **a**lkit*aa*ba latubayyinunnahu li**l**nn*aa*si wal*aa* taktumuunahu fanaba*dz*uuhu war

Dan (ingatlah), ketika Allah mengambil janji dari orang-orang yang telah diberi Kitab (yaitu), “Hendaklah kamu benar-benar menerangkannya (isi Kitab itu) kepada manusia, dan janganlah kamu menyembunyikannya,” lalu mereka melemparkan (janji itu) ke belakan

3:188

# لَا تَحْسَبَنَّ الَّذِيْنَ يَفْرَحُوْنَ بِمَآ اَتَوْا وَّيُحِبُّوْنَ اَنْ يُّحْمَدُوْا بِمَا لَمْ يَفْعَلُوْا فَلَا تَحْسَبَنَّهُمْ بِمَفَازَةٍ مِّنَ الْعَذَابِۚ وَلَهُمْ عَذَابٌ اَلِيْمٌ

l*aa* ta*h*sabanna **al**la*dz*iina yafra*h*uuna bim*aa* ataw wayu*h*ibbuuna an yu*h*maduu bim*aa* lam yaf'aluu fal*aa* ta*h*sabannahum bimaf*aa*zatin mina **a**l'a*dzaa*

*Jangan sekali-kali kamu mengira bahwa orang yang gembira dengan apa yang telah mereka kerjakan dan mereka suka dipuji atas perbuatan yang tidak mereka lakukan, jangan sekali-kali kamu mengira bahwa mereka akan lolos dari azab. Mereka akan mendapat azab ya*









3:189

# وَلِلّٰهِ مُلْكُ السَّمٰوٰتِ وَالْاَرْضِۗ وَاللّٰهُ عَلٰى كُلِّ شَيْءٍ قَدِيْرٌ ࣖ

walill*aa*hi mulku **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i wa**al**l*aa*hu 'al*aa* kulli syay-in qadiir**un**

Dan milik Allah-lah kerajaan langit dan bumi; dan Allah Mahakuasa atas segala sesuatu.

3:190

# اِنَّ فِيْ خَلْقِ السَّمٰوٰتِ وَالْاَرْضِ وَاخْتِلَافِ الَّيْلِ وَالنَّهَارِ لَاٰيٰتٍ لِّاُولِى الْاَلْبَابِۙ

inna fii khalqi **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i wa**i**khtil*aa*fi **al**layli wa**al**nnah*aa*ri la*aa*y*aa*tin li-ulii **a**l-alb*a*

Sesungguhnya dalam penciptaan langit dan bumi, dan pergantian malam dan siang terdapat tanda-tanda (kebesaran Allah) bagi orang yang berakal,







3:191

# الَّذِيْنَ يَذْكُرُوْنَ اللّٰهَ قِيَامًا وَّقُعُوْدًا وَّعَلٰى جُنُوْبِهِمْ وَيَتَفَكَّرُوْنَ فِيْ خَلْقِ السَّمٰوٰتِ وَالْاَرْضِۚ رَبَّنَا مَا خَلَقْتَ هٰذَا بَاطِلًاۚ سُبْحٰنَكَ فَقِنَا عَذَابَ النَّارِ

**al**la*dz*iina ya*dz*kuruuna **al**l*aa*ha qiy*aa*man waqu'uudan wa'al*aa* junuubihim wayatafakkaruuna fii khalqi **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i rabban

(yaitu) orang-orang yang mengingat Allah sambil berdiri, duduk atau dalam keadaan berbaring, dan mereka memikirkan tentang penciptaan langit dan bumi (seraya berkata), “Ya Tuhan kami, tidaklah Engkau menciptakan semua ini sia-sia; Mahasuci Engkau, lindung

3:192

# رَبَّنَآ اِنَّكَ مَنْ تُدْخِلِ النَّارَ فَقَدْ اَخْزَيْتَهٗ ۗ وَمَا لِلظّٰلِمِيْنَ مِنْ اَنْصَارٍ

rabban*aa* innaka man tudkhili **al**nn*aa*ra faqad akhzaytahu wam*aa* li**l***zhzhaa*limiina min an*shaa*r**in**

Ya Tuhan kami, sesungguhnya orang yang Engkau masukkan ke dalam neraka, maka sungguh, Engkau telah menghinakannya, dan tidak ada seorang penolong pun bagi orang yang zalim.

3:193

# رَبَّنَآ اِنَّنَا سَمِعْنَا مُنَادِيًا يُّنَادِيْ لِلْاِيْمَانِ اَنْ اٰمِنُوْا بِرَبِّكُمْ فَاٰمَنَّا ۖرَبَّنَا فَاغْفِرْ لَنَا ذُنُوْبَنَا وَكَفِّرْ عَنَّا سَيِّاٰتِنَا وَتَوَفَّنَا مَعَ الْاَبْرَارِۚ

rabban*aa* innan*aa* sami'n*aa* mun*aa*diyan yun*aa*dii lil-iim*aa*ni an *aa*minuu birabbikum fa*aa*mann*aa* rabban*aa* fa**i**ghfir lan*aa* *dz*unuuban*aa* wakaffir 'ann*aa*

*Ya Tuhan kami, sesungguhnya kami mendengar orang yang menyeru kepada iman, (yaitu), “Berimanlah kamu kepada Tuhanmu,” maka kami pun beriman. Ya Tuhan kami, ampunilah dosa-dosa kami dan hapuskanlah kesalahan-kesalahan kami, dan matikanlah kami beserta oran*









3:194

# رَبَّنَا وَاٰتِنَا مَا وَعَدْتَّنَا عَلٰى رُسُلِكَ وَلَا تُخْزِنَا يَوْمَ الْقِيٰمَةِ ۗ اِنَّكَ لَا تُخْلِفُ الْمِيْعَادَ

rabban*aa* wa*aa*tin*aa* m*aa* wa'adtan*aa* 'al*aa* rusulika wal*aa* tukhzin*aa* yawma **a**lqiy*aa*mati innaka l*aa* tukhlifu **a**lmii'*aa*d**a**

Ya Tuhan kami, berilah kami apa yang telah Engkau janjikan kepada kami melalui rasul-rasul-Mu. Dan janganlah Engkau hinakan kami pada hari Kiamat. Sungguh, Engkau tidak pernah mengingkari janji.”

3:195

# فَاسْتَجَابَ لَهُمْ رَبُّهُمْ اَنِّيْ لَآ اُضِيْعُ عَمَلَ عَامِلٍ مِّنْكُمْ مِّنْ ذَكَرٍ اَوْ اُنْثٰى ۚ بَعْضُكُمْ مِّنْۢ بَعْضٍ ۚ فَالَّذِيْنَ هَاجَرُوْا وَاُخْرِجُوْا مِنْ دِيَارِهِمْ وَاُوْذُوْا فِيْ سَبِيْلِيْ وَقٰتَلُوْا وَقُتِلُوْا لَاُكَفِّرَنَّ ع

fa**i**staj*aa*ba lahum rabbuhum annii l*aa* u*dh*ii'u 'amala '*aa*milin minkum min *dz*akarin aw unts*aa* ba'*dh*ukum min ba'*dh*in fa**a**lla*dz*iina h*aa*jaruu waukhrijuu min d

Maka Tuhan mereka memperkenankan permohonannya (dengan berfirman), “Sesungguhnya Aku tidak menyia-nyiakan amal orang yang beramal di antara kamu, baik laki-laki maupun perempuan, (karena) sebagian kamu adalah (keturunan) dari sebagian yang lain. Maka oran

3:196

# لَا يَغُرَّنَّكَ تَقَلُّبُ الَّذِيْنَ كَفَرُوْا فِى الْبِلَادِۗ

l*aa* yaghurrannaka taqallubu **al**la*dz*iina kafaruu fii **a**lbil*aa*d**i**

Jangan sekali-kali kamu teperdaya oleh kegiatan orang-orang kafir (yang bergerak) di seluruh negeri.

3:197

# مَتَاعٌ قَلِيْلٌ ۗ ثُمَّ مَأْوٰىهُمْ جَهَنَّمُ ۗوَبِئْسَ الْمِهَادُ

mat*aa*'un qaliilun tsumma ma/w*aa*hum jahannamu wabi/sa **a**lmih*aa*d**u**

Itu hanyalah kesenangan sementara, kemudian tempat kembali mereka ialah neraka Jahanam. (Jahanam) itu seburuk-buruk tempat tinggal.

3:198

# لٰكِنِ الَّذِيْنَ اتَّقَوْا رَبَّهُمْ لَهُمْ جَنّٰتٌ تَجْرِيْ مِنْ تَحْتِهَا الْاَنْهٰرُ خٰلِدِيْنَ فِيْهَا نُزُلًا مِّنْ عِنْدِ اللّٰهِ ۗ وَمَا عِنْدَ اللّٰهِ خَيْرٌ لِّلْاَبْرَارِ

l*aa*kini **al**la*dz*iina ittaqaw rabbahum lahum jann*aa*tun tajrii min ta*h*tih*aa* **a**l-anh*aa*ru kh*aa*lidiina fiih*aa* nuzulan min 'indi **al**l*aa*hi wam*aa*

Tetapi orang-orang yang bertakwa kepada Tuhannya, mereka akan mendapat surga-surga yang mengalir di bawahnya sungai-sungai, mereka kekal di dalamnya sebagai karunia dari Allah. Dan apa yang di sisi Allah lebih baik bagi orang-orang yang berbakti.

3:199

# وَاِنَّ مِنْ اَهْلِ الْكِتٰبِ لَمَنْ يُّؤْمِنُ بِاللّٰهِ وَمَآ اُنْزِلَ اِلَيْكُمْ وَمَآ اُنْزِلَ اِلَيْهِمْ خٰشِعِيْنَ لِلّٰهِ ۙ لَا يَشْتَرُوْنَ بِاٰيٰتِ اللّٰهِ ثَمَنًا قَلِيْلًا ۗ اُولٰۤىِٕكَ لَهُمْ اَجْرُهُمْ عِنْدَ رَبِّهِمْ ۗ اِنَّ اللّٰهَ سَرِيْ

wa-inna min ahli **a**lkit*aa*bi laman yu/minu bi**al**l*aa*hi wam*aa* unzila ilaykum wam*aa* unzila ilayhim kh*aa*syi'iina lill*aa*hi l*aa* yasytaruuna bi-*aa*y*aa*ti **al**

**Dan sesungguhnya di antara Ahli Kitab ada yang beriman kepada Allah, dan kepada apa yang diturunkan kepada kamu, dan yang diturunkan kepada mereka, karena mereka berendah hati kepada Allah, dan mereka tidak memperjualbelikan ayat-ayat Allah dengan harga m**









3:200

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوا اصْبِرُوْا وَصَابِرُوْا وَرَابِطُوْاۗ وَاتَّقُوا اللّٰهَ لَعَلَّكُمْ تُفْلِحُوْنَ ࣖ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu i*sh*biruu wa*shaa*biruu war*aa*bi*th*uu wa**i**ttaquu **al**l*aa*ha la'allakum tufli*h*uun**a**

Wahai orang-orang yang beriman! Bersabarlah kamu dan kuatkanlah kesabaranmu dan tetaplah bersiap-siaga (di perbatasan negerimu) dan bertakwalah kepada Allah agar kamu beruntung.

<!--EndFragment-->