---
title: (102) At-Takasur - التكاثر
date: 2021-10-27T04:15:53.733Z
ayat: 102
description: "Jumlah Ayat: 8 / Arti: Bermegah-Megahan"
---
<!--StartFragment-->

102:1

# اَلْهٰىكُمُ التَّكَاثُرُۙ

alh*aa*kumu **al**ttak*aa*tsur**u**

Bermegah-megahan telah melalaikan kamu,

102:2

# حَتّٰى زُرْتُمُ الْمَقَابِرَۗ

*h*att*aa* zurtumu **a**lmaq*aa*bir**a**

sampai kamu masuk ke dalam kubur.

102:3

# كَلَّا سَوْفَ تَعْلَمُوْنَۙ

kall*aa* sawfa ta'lamuun**a**

Sekali-kali tidak! Kelak kamu akan mengetahui (akibat perbuatanmu itu),

102:4

# ثُمَّ كَلَّا سَوْفَ تَعْلَمُوْنَ

tsumma kall*aa* sawfa ta'lamuun**a**

kemudian sekali-kali tidak! Kelak kamu akan mengetahui.

102:5

# كَلَّا لَوْ تَعْلَمُوْنَ عِلْمَ الْيَقِيْنِۗ

kall*aa* law ta'lamuuna 'ilma **a**lyaqiin**i**

Sekali-kali tidak! Sekiranya kamu mengetahui dengan pasti,

102:6

# لَتَرَوُنَّ الْجَحِيْمَۙ

latarawunna **a**lja*h*iim**a**

niscaya kamu benar-benar akan melihat neraka Jahim,

102:7

# ثُمَّ لَتَرَوُنَّهَا عَيْنَ الْيَقِيْنِۙ

tsumma latarawunnah*aa* 'ayna **a**lyaqiin**i**

kemudian kamu benar-benar akan melihatnya dengan mata kepala sendiri,

102:8

# ثُمَّ لَتُسْـَٔلُنَّ يَوْمَىِٕذٍ عَنِ النَّعِيْمِ ࣖ

tsumma latus-alunna yawma-i*dz*in 'ani **al**nna'iim**i**

kemudian kamu benar-benar akan ditanya pada hari itu tentang kenikmatan (yang megah di dunia itu).

<!--EndFragment-->