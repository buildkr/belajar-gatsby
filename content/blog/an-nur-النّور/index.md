---
title: (24) An-Nur - النّور
date: 2021-10-27T03:51:15.928Z
ayat: 24
description: "Jumlah Ayat: 64 / Arti: Cahaya"
---
<!--StartFragment-->

24:1

# سُوْرَةٌ اَنْزَلْنٰهَا وَفَرَضْنٰهَا وَاَنْزَلْنَا فِيْهَآ اٰيٰتٍۢ بَيِّنٰتٍ لَّعَلَّكُمْ تَذَكَّرُوْنَ

suuratun anzaln*aa*h*aa* wafara*dh*n*aa*h*aa* wa-anzaln*aa* fiih*aa* *aa*y*aa*tin bayyin*aa*tin la'allakum ta*dz*akkaruun**a**

(Inilah) suatu surah yang Kami turunkan dan Kami wajibkan (menjalankan hukum-hukum)nya, dan Kami turunkan di dalamnya tanda-tanda (kebesaran Allah) yang jelas, agar kamu ingat.

24:2

# اَلزَّانِيَةُ وَالزَّانِيْ فَاجْلِدُوْا كُلَّ وَاحِدٍ مِّنْهُمَا مِائَةَ جَلْدَةٍ ۖوَّلَا تَأْخُذْكُمْ بِهِمَا رَأْفَةٌ فِيْ دِيْنِ اللّٰهِ اِنْ كُنْتُمْ تُؤْمِنُوْنَ بِاللّٰهِ وَالْيَوْمِ الْاٰخِرِۚ وَلْيَشْهَدْ عَذَابَهُمَا طَاۤىِٕفَةٌ مِّنَ الْمُؤْمِنِ

a**l**zz*aa*niyatu wa**al**zz*aa*nii fa**i**jliduu kulla w*aah*idin minhum*aa* mi-ata jaldatin wal*aa* ta/khu*dz*kum bihim*aa* ra/fatun fii diini **al**l*aa*hi in

Pezina perempuan dan pezina laki-laki, deralah masing-masing dari keduanya seratus kali, dan janganlah rasa belas kasihan kepada keduanya mencegah kamu untuk (menjalankan) agama (hukum) Allah, jika kamu beriman kepada Allah dan hari kemudian; dan hendakla

24:3

# اَلزَّانِيْ لَا يَنْكِحُ اِلَّا زَانِيَةً اَوْ مُشْرِكَةً ۖوَّالزَّانِيَةُ لَا يَنْكِحُهَآ اِلَّا زَانٍ اَوْ مُشْرِكٌۚ وَحُرِّمَ ذٰلِكَ عَلَى الْمُؤْمِنِيْنَ

a**l**zz*aa*nii l*aa* yanki*h*u ill*aa* z*aa*niyatan aw musyrikatan wa**al**zz*aa*niyatu l*aa* yanki*h*uh*aa* ill*aa* z*aa*nin aw musyrikun wa*h*urrima *dzaa*lika '

Pezina laki-laki tidak boleh menikah kecuali dengan pezina perempuan, atau dengan perempuan musyrik; dan pezina perempuan tidak boleh menikah kecuali dengan pezina laki-laki atau dengan laki-laki musyrik; dan yang demikian itu diharamkan bagi orang-orang

24:4

# وَالَّذِيْنَ يَرْمُوْنَ الْمُحْصَنٰتِ ثُمَّ لَمْ يَأْتُوْا بِاَرْبَعَةِ شُهَدَاۤءَ فَاجْلِدُوْهُمْ ثَمٰنِيْنَ جَلْدَةً وَّلَا تَقْبَلُوْا لَهُمْ شَهَادَةً اَبَدًاۚ وَاُولٰۤىِٕكَ هُمُ الْفٰسِقُوْنَ ۙ

wa**a**lla*dz*iina yarmuuna **a**lmu*hs*an*aa*ti tsumma lam ya/tuu bi-arba'ati syuhad*aa*-a fa**i**jliduuhum tsam*aa*niina jaldatan wal*aa* taqbaluu lahum syah*aa*datan abadan waul<

Dan orang-orang yang menuduh perempuan-perempuan yang baik (berzina) dan mereka tidak mendatangkan empat orang saksi, maka deralah mereka delapan puluh kali, dan janganlah kamu terima kesaksian mereka untuk selama-lamanya. Mereka itulah orang-orang yang f

24:5

# اِلَّا الَّذِيْنَ تَابُوْا مِنْۢ بَعْدِ ذٰلِكَ وَاَصْلَحُوْاۚ فَاِنَّ اللّٰهَ غَفُوْرٌ رَّحِيْمٌ

ill*aa* **al**la*dz*iina t*aa*buu min ba'di *dzaa*lika wa-a*sh*la*h*uu fa-inna **al**l*aa*ha ghafuurun ra*h*iim**un**

kecuali mereka yang bertobat setelah itu dan memperbaiki (dirinya), maka sungguh, Allah Maha Pengampun, Maha Penyayang.

24:6

# وَالَّذِيْنَ يَرْمُوْنَ اَزْوَاجَهُمْ وَلَمْ يَكُنْ لَّهُمْ شُهَدَاۤءُ اِلَّآ اَنْفُسُهُمْ فَشَهَادَةُ اَحَدِهِمْ اَرْبَعُ شَهٰدٰتٍۢ بِاللّٰهِ ۙاِنَّهٗ لَمِنَ الصّٰدِقِيْنَ

wa**a**lla*dz*iina yarmuuna azw*aa*jahum walam yakun lahum syuhad*aa*u ill*aa* anfusuhum fasyah*aa*datu a*h*adihim arba'u syah*aa*d*aa*tin bi**al**l*aa*hi innahu lamina **al**

**Dan orang-orang yang menuduh istrinya (berzina), padahal mereka tidak mempunyai saksi-saksi selain diri mereka sendiri, maka kesaksian masing-masing orang itu ialah empat kali bersumpah dengan (nama) Allah, bahwa sesungguhnya dia termasuk orang yang berka**

24:7

# وَالْخَامِسَةُ اَنَّ لَعْنَتَ اللّٰهِ عَلَيْهِ اِنْ كَانَ مِنَ الْكٰذِبِيْنَ

wa**a**lkh*aa*misatu anna la'nata **al**l*aa*hi 'alayhi in k*aa*na mina **a**lk*aadz*ibiin**a**

Dan (sumpah) yang kelima bahwa laknat Allah akan menimpanya, jika dia termasuk orang yang berdusta.

24:8

# وَيَدْرَؤُا عَنْهَا الْعَذَابَ اَنْ تَشْهَدَ اَرْبَعَ شَهٰدٰتٍۢ بِاللّٰهِ اِنَّهٗ لَمِنَ الْكٰذِبِيْنَ ۙ

wayadrau 'anh*aa* **a**l'a*dzaa*ba an tasyhada arba'a syah*aa*d*aa*tin bi**al**l*aa*hi innahu lamina **a**lk*aadz*ibiin**a**

Dan istri itu terhindar dari hukuman apabila dia bersumpah empat kali atas (nama) Allah bahwa dia (suaminya) benar-benar termasuk orang-orang yang berdusta,

24:9

# وَالْخَامِسَةَ اَنَّ غَضَبَ اللّٰهِ عَلَيْهَآ اِنْ كَانَ مِنَ الصّٰدِقِيْنَ

wa**a**lkh*aa*misata anna gha*dh*aba **al**l*aa*hi 'alayh*aa* in k*aa*na mina **al***shshaa*diqiin**a**

dan (sumpah) yang kelima bahwa kemurkaan Allah akan menimpanya (istri), jika dia (suaminya) itu termasuk orang yang berkata benar.

24:10

# وَلَوْلَا فَضْلُ اللّٰهِ عَلَيْكُمْ وَرَحْمَتُهٗ وَاَنَّ اللّٰهَ تَوَّابٌ حَكِيْمٌ ࣖ

walawl*aa* fa*dh*lu **al**l*aa*hi 'alaykum wara*h*matuhu wa-anna **al**l*aa*ha taww*aa*bun *h*akiim**un**

Dan seandainya bukan karena karunia Allah dan rahmat-Nya kepadamu (niscaya kamu akan menemui kesulitan). Dan sesungguhnya Allah Maha Penerima Tobat, Mahabijaksana.

24:11

# اِنَّ الَّذِيْنَ جَاۤءُوْ بِالْاِفْكِ عُصْبَةٌ مِّنْكُمْۗ لَا تَحْسَبُوْهُ شَرًّا لَّكُمْۗ بَلْ هُوَ خَيْرٌ لَّكُمْۗ لِكُلِّ امْرِئٍ مِّنْهُمْ مَّا اكْتَسَبَ مِنَ الْاِثْمِۚ وَالَّذِيْ تَوَلّٰى كِبْرَهٗ مِنْهُمْ لَهٗ عَذَابٌ عَظِيْمٌ

inna **al**la*dz*iina j*aa*uu bi**a**l-ifki 'u*sh*batun minkum l*aa* ta*h*sabuuhu syarran lakum bal huwa khayrun lakum likulli imri-in minhum m*aa* iktasaba mina **a**l-itsmi wa**a**

Sesungguhnya orang-orang yang membawa berita bohong itu adalah dari golongan kamu (juga). Janganlah kamu mengira berita itu buruk bagi kamu bahkan itu baik bagi kamu. Setiap orang dari mereka akan mendapat balasan dari dosa yang diperbuatnya. Dan barangsi

24:12

# لَوْلَآ اِذْ سَمِعْتُمُوْهُ ظَنَّ الْمُؤْمِنُوْنَ وَالْمُؤْمِنٰتُ بِاَنْفُسِهِمْ خَيْرًاۙ وَّقَالُوْا هٰذَآ اِفْكٌ مُّبِيْنٌ

lawl*aa* i*dz* sami'tumuuhu *zh*anna **a**lmu/minuuna wa**a**lmu/min*aa*tu bi-anfusihim khayran waq*aa*luu h*aadzaa* ifkun mubiin**un**

Mengapa orang-orang mukmin dan mukminat tidak berbaik sangka terhadap diri mereka sendiri, ketika kamu mendengar berita bohong itu dan berkata, “Ini adalah (suatu berita) bohong yang nyata.”

24:13

# لَوْلَا جَاۤءُوْ عَلَيْهِ بِاَرْبَعَةِ شُهَدَاۤءَۚ فَاِذْ لَمْ يَأْتُوْا بِالشُّهَدَاۤءِ فَاُولٰۤىِٕكَ عِنْدَ اللّٰهِ هُمُ الْكٰذِبُوْنَ

lawl*aa* j*aa*uu 'alayhi bi-arba'ati syuhad*aa*-a fa-i*dz* lam ya/tuu bi**al**sysyuhad*aa*-i faul*aa*-ika 'inda **al**l*aa*hi humu **a**lk*aadz*ibuun**a**

Mengapa mereka (yang menuduh itu) tidak datang membawa empat saksi? Oleh karena mereka tidak membawa saksi-saksi, maka mereka itu dalam pandangan Allah adalah orang-orang yang berdusta.

24:14

# وَلَوْلَا فَضْلُ اللّٰهِ عَلَيْكُمْ وَرَحْمَتُهٗ فِى الدُّنْيَا وَالْاٰخِرَةِ لَمَسَّكُمْ فِيْ مَآ اَفَضْتُمْ فِيْهِ عَذَابٌ عَظِيْمٌ

walawl*aa* fa*dh*lu **al**l*aa*hi 'alaykum wara*h*matuhu fii **al**dduny*aa* wa**a**l-*aa*khirati lamassakum fii m*aa* afa*dh*tum fiihi 'a*dzaa*bun 'a*zh*iim**u**

Dan seandainya bukan karena karunia Allah dan rahmat-Nya kepadamu di dunia dan di akhirat, niscaya kamu ditimpa azab yang besar, disebabkan oleh pembicaraan kamu tentang hal itu (berita bohong itu).

24:15

# اِذْ تَلَقَّوْنَهٗ بِاَلْسِنَتِكُمْ وَتَقُوْلُوْنَ بِاَفْوَاهِكُمْ مَّا لَيْسَ لَكُمْ بِهٖ عِلْمٌ وَّتَحْسَبُوْنَهٗ هَيِّنًاۙ وَّهُوَ عِنْدَ اللّٰهِ عَظِيْمٌ ۚ

i*dz* talaqqawnahu bi-alsinatikum wataquuluuna bi-afw*aa*hikum m*aa* laysa lakum bihi 'ilmun wata*h*sabuunahu hayyinan wahuwa 'inda **al**l*aa*hi 'a*zh*iim**un**

(Ingatlah) ketika kamu menerima (berita bohong) itu dari mulut ke mulut dan kamu katakan dengan mulutmu apa yang tidak kamu ketahui sedikit pun, dan kamu menganggapnya remeh, padahal dalam pandangan Allah itu soal besar.

24:16

# وَلَوْلَآ اِذْ سَمِعْتُمُوْهُ قُلْتُمْ مَّا يَكُوْنُ لَنَآ اَنْ نَّتَكَلَّمَ بِهٰذَاۖ سُبْحٰنَكَ هٰذَا بُهْتَانٌ عَظِيْمٌ

walawl*aa* i*dz* sami'tumuuhu qultum m*aa* yakuunu lan*aa* an natakallama bih*aadzaa* sub*haa*naka h*aadzaa* buht*aa*nun 'a*zh*iim**un**

Dan mengapa kamu tidak berkata ketika mendengarnya, “Tidak pantas bagi kita membicarakan ini. Mahasuci Engkau, ini adalah kebohongan yang besar.”

24:17

# يَعِظُكُمُ اللّٰهُ اَنْ تَعُوْدُوْا لِمِثْلِهٖٓ اَبَدًا اِنْ كُنْتُمْ مُّؤْمِنِيْنَ ۚ

ya'i*zh*ukumu **al**l*aa*hu an ta'uuduu limitslihi abadan in kuntum mu/miniin**a**

Allah memperingatkan kamu agar (jangan) kembali mengulangi seperti itu selama-lamanya, jika kamu orang beriman,

24:18

# وَيُبَيِّنُ اللّٰهُ لَكُمُ الْاٰيٰتِۗ وَاللّٰهُ عَلِيْمٌ حَكِيْمٌ

wayubayyinu **al**l*aa*hu lakumu **a**l-*aa*y*aa*ti wa**al**l*aa*hu 'aliimun *h*akiim**un**

dan Allah menjelaskan ayat-ayat(-Nya) kepada kamu. Dan Allah Maha Mengetahui, Mahabijaksana.

24:19

# اِنَّ الَّذِيْنَ يُحِبُّوْنَ اَنْ تَشِيْعَ الْفَاحِشَةُ فِى الَّذِيْنَ اٰمَنُوْا لَهُمْ عَذَابٌ اَلِيْمٌۙ فِى الدُّنْيَا وَالْاٰخِرَةِۗ وَاللّٰهُ يَعْلَمُ وَاَنْتُمْ لَا تَعْلَمُوْنَ

inna **al**la*dz*iina yu*h*ibbuuna an tasyii'a **a**lf*aah*isyatu fii **al**la*dz*iina *aa*manuu lahum 'a*dzaa*bun **a**liimun fii **al**dduny*aa* wa

Sesungguhnya orang-orang yang ingin agar perbuatan yang sangat keji itu (berita bohong) tersiar di kalangan orang-orang yang beriman, mereka mendapat azab yang pedih di dunia dan di akhirat. Dan Allah mengetahui, sedang kamu tidak mengetahui.

24:20

# وَلَوْلَا فَضْلُ اللّٰهِ عَلَيْكُمْ وَرَحْمَتُهٗ وَاَنَّ اللّٰهَ رَءُوْفٌ رَّحِيْمٌ ࣖ

walawl*aa* fa*dh*lu **al**l*aa*hi 'alaykum wara*h*matuhu wa-anna **al**l*aa*ha rauufun ra*h*iim**un**

Dan kalau bukan karena karunia Allah dan rahmat-Nya kepadamu (niscaya kamu akan ditimpa azab yang besar). Sungguh, Allah Maha Penyantun, Ma-ha Penyayang.

24:21

# ۞ يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْا لَا تَتَّبِعُوْا خُطُوٰتِ الشَّيْطٰنِۗ وَمَنْ يَّتَّبِعْ خُطُوٰتِ الشَّيْطٰنِ فَاِنَّهٗ يَأْمُرُ بِالْفَحْشَاۤءِ وَالْمُنْكَرِۗ وَلَوْلَا فَضْلُ اللّٰهِ عَلَيْكُمْ وَرَحْمَتُهٗ مَا زَكٰى مِنْكُمْ مِّنْ اَحَدٍ اَبَدًاۙ وّ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu l*aa* tattabi'uu khu*th*uw*aa*ti **al**sysyay*thaa*ni waman yattabi' khu*th*uw*aa*ti **al**sysyay*thaa*n

Wahai orang-orang yang beriman! Janganlah kamu mengikuti langkah-langkah setan. Barangsiapa mengikuti langkah-langkah setan, maka sesungguhnya dia (setan) menyuruh mengerjakan perbuatan yang keji dan mungkar. Kalau bukan karena karunia Allah dan rahmat-Ny

24:22

# وَلَا يَأْتَلِ اُولُو الْفَضْلِ مِنْكُمْ وَالسَّعَةِ اَنْ يُّؤْتُوْٓا اُولِى الْقُرْبٰى وَالْمَسٰكِيْنَ وَالْمُهٰجِرِيْنَ فِيْ سَبِيْلِ اللّٰهِ ۖوَلْيَعْفُوْا وَلْيَصْفَحُوْاۗ اَلَا تُحِبُّوْنَ اَنْ يَّغْفِرَ اللّٰهُ لَكُمْ ۗوَاللّٰهُ غَفُوْرٌ رَّحِيْمٌ

wal*aa* ya/tali uluu **a**lfa*dh*li minkum wa**al**ssa'ati an yu/tuu ulii **a**lqurb*aa* wa**a**lmas*aa*kiina wa**a**lmuh*aa*jiriina fii sabiili **al**l

Dan janganlah orang-orang yang mempunyai kelebihan dan kelapangan di antara kamu bersumpah bahwa mereka (tidak) akan memberi (bantuan) kepada kerabat(nya), orang-orang miskin dan orang-orang yang berhijrah di jalan Allah, dan hendaklah mereka memaafkan da

24:23

# اِنَّ الَّذِيْنَ يَرْمُوْنَ الْمُحْصَنٰتِ الْغٰفِلٰتِ الْمُؤْمِنٰتِ لُعِنُوْا فِى الدُّنْيَا وَالْاٰخِرَةِۖ وَلَهُمْ عَذَابٌ عَظِيْمٌ ۙ

inna **al**la*dz*iina yarmuuna **a**lmu*hs*an*aa*ti **a**lgh*aa*fil*aa*ti **a**lmu/min*aa*ti lu'inuu fii **al**dduny*aa* wa**a**l-*aa*khi

Sungguh, orang-orang yang menuduh perempuan-perempuan baik, yang lengah dan beriman (dengan tuduhan berzina), mereka dilaknat di dunia dan di akhirat, dan mereka akan mendapat azab yang besar,

24:24

# يَّوْمَ تَشْهَدُ عَلَيْهِمْ اَلْسِنَتُهُمْ وَاَيْدِيْهِمْ وَاَرْجُلُهُمْ بِمَا كَانُوْا يَعْمَلُوْنَ

yawma tasyhadu 'alayhim **a**lsinatuhum wa-aydiihim wa-arjuluhum bim*aa* k*aa*nuu ya'maluun**a**

pada hari, (ketika) lidah, tangan dan kaki mereka menjadi saksi atas mereka terhadap apa yang dahulu mereka kerjakan.

24:25

# يَوْمَىِٕذٍ يُّوَفِّيْهِمُ اللّٰهُ دِيْنَهُمُ الْحَقَّ وَيَعْلَمُوْنَ اَنَّ اللّٰهَ هُوَ الْحَقُّ الْمُبِيْنُ

yawma-i*dz*in yuwaffiihimu **al**l*aa*hu diinahumu **a**l*h*aqqa waya'lamuuna anna **al**l*aa*ha huwa **a**l*h*aqqu **a**lmubiin**u**

Pada hari itu Allah menyempurnakan balasan yang sebenarnya bagi mereka, dan mereka tahu bahwa Allah Maha-benar, Maha Menjelaskan.

24:26

# اَلْخَبِيْثٰتُ لِلْخَبِيْثِيْنَ وَالْخَبِيْثُوْنَ لِلْخَبِيْثٰتِۚ وَالطَّيِّبٰتُ لِلطَّيِّبِيْنَ وَالطَّيِّبُوْنَ لِلطَّيِّبٰتِۚ اُولٰۤىِٕكَ مُبَرَّءُوْنَ مِمَّا يَقُوْلُوْنَۗ لَهُمْ مَّغْفِرَةٌ وَّرِزْقٌ كَرِيْمٌ ࣖ

alkhabiits*aa*tu lilkhabiitsiina wa**a**lkhabiitsuuna lilkhabiits*aa*ti wa**al***ththh*ayyib*aa*tu li**l***ththh*ayyibiina wa**al***ththh*ayyibuuna li**l***th*

Perempuan-perempuan yang keji untuk laki-laki yang keji, dan laki-laki yang keji untuk perempuan-perempuan yang keji (pula), sedangkan perempuan-perempuan yang baik untuk laki-laki yang baik dan laki-laki yang baik untuk perempuan-perempuan yang baik (pul

24:27

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْا لَا تَدْخُلُوْا بُيُوْتًا غَيْرَ بُيُوْتِكُمْ حَتّٰى تَسْتَأْنِسُوْا وَتُسَلِّمُوْا عَلٰٓى اَهْلِهَاۗ ذٰلِكُمْ خَيْرٌ لَّكُمْ لَعَلَّكُمْ تَذَكَّرُوْنَ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu l*aa* tadkhuluu buyuutan ghayra buyuutikum *h*att*aa* tasta/nisuu watusallimuu 'al*aa* ahlih*aa* *dzaa*likum khayrun lakum la'allakum ta*dz*akka

Wahai orang-orang yang beriman! Janganlah kamu memasuki rumah yang bukan rumahmu sebelum meminta izin dan memberi salam kepada penghuninya. Yang demikian itu lebih baik bagimu, agar kamu (selalu) ingat.

24:28

# فَاِنْ لَّمْ تَجِدُوْا فِيْهَآ اَحَدًا فَلَا تَدْخُلُوْهَا حَتّٰى يُؤْذَنَ لَكُمْ وَاِنْ قِيْلَ لَكُمُ ارْجِعُوْا فَارْجِعُوْا هُوَ اَزْكٰى لَكُمْ ۗوَاللّٰهُ بِمَا تَعْمَلُوْنَ عَلِيْمٌ

fa-in lam tajiduu fiih*aa* a*h*adan fal*aa* tadkhuluuh*aa* *h*att*aa* yu/*dz*ana lakum wa-in qiila lakumu irji'uu fa**i**rji'uu huwa azk*aa* lakum wa**al**l*aa*hu bim*aa* ta'maluu

Dan jika kamu tidak menemui seorang pun di dalamnya, maka janganlah kamu masuk sebelum kamu mendapat izin. Dan jika dikatakan kepadamu, “Kembalilah!” Maka (hendaklah) kamu kembali. Itu lebih suci bagimu, dan Allah Maha Mengetahui apa yang kamu kerjakan.

24:29

# لَيْسَ عَلَيْكُمْ جُنَاحٌ اَنْ تَدْخُلُوْا بُيُوْتًا غَيْرَ مَسْكُوْنَةٍ فِيْهَا مَتَاعٌ لَّكُمْۗ وَاللّٰهُ يَعْلَمُ مَا تُبْدُوْنَ وَمَا تَكْتُمُوْنَ

laysa 'alaykum jun*aah*un an tadkhuluu buyuutan ghayra maskuunatin fiih*aa* mat*aa*'un lakum wa**al**l*aa*hu ya'lamu m*aa* tubduuna wam*aa* taktumuun**a**

Tidak ada dosa atasmu memasuki rumah yang tidak dihuni, yang di dalamnya ada kepentingan kamu; Allah mengetahui apa yang kamu nyatakan dan apa yang kamu sembunyikan.

24:30

# قُلْ لِّلْمُؤْمِنِيْنَ يَغُضُّوْا مِنْ اَبْصَارِهِمْ وَيَحْفَظُوْا فُرُوْجَهُمْۗ ذٰلِكَ اَزْكٰى لَهُمْۗ اِنَّ اللّٰهَ خَبِيْرٌۢ بِمَا يَصْنَعُوْنَ

qul lilmu/miniina yaghu*dhdh*uu min ab*shaa*rihum waya*h*fa*zh*uu furuujahum *dzaa*lika azk*aa* lahum inna **al**l*aa*ha khabiirun bim*aa* ya*sh*na'uun**a**

Katakanlah kepada laki-laki yang beriman, agar mereka menjaga pandangannya, dan memelihara kemaluannya; yang demikian itu, lebih suci bagi mereka. Sungguh, Allah Maha Mengetahui apa yang mereka perbuat.

24:31

# وَقُلْ لِّلْمُؤْمِنٰتِ يَغْضُضْنَ مِنْ اَبْصَارِهِنَّ وَيَحْفَظْنَ فُرُوْجَهُنَّ وَلَا يُبْدِيْنَ زِيْنَتَهُنَّ اِلَّا مَا ظَهَرَ مِنْهَا وَلْيَضْرِبْنَ بِخُمُرِهِنَّ عَلٰى جُيُوْبِهِنَّۖ وَلَا يُبْدِيْنَ زِيْنَتَهُنَّ اِلَّا لِبُعُوْلَتِهِنَّ اَوْ اٰبَاۤ

waqul lilmu/min*aa*ti yagh*dh*u*dh*na min ab*shaa*rihinna waya*h*fa*zh*na furuujahunna wal*aa* yubdiina ziinatahunna ill*aa* m*aa* *zh*ahara minh*aa* walya*dh*ribna bikhumurihinna 'al*aa* ju

Dan katakanlah kepada para perempuan yang beriman, agar mereka menjaga pandangannya, dan memelihara kemaluannya, dan janganlah menampakkan perhiasannya (auratnya), kecuali yang (biasa) terlihat. Dan hendaklah mereka menutupkan kain kerudung ke dadanya, da

24:32

# وَاَنْكِحُوا الْاَيَامٰى مِنْكُمْ وَالصّٰلِحِيْنَ مِنْ عِبَادِكُمْ وَاِمَاۤىِٕكُمْۗ اِنْ يَّكُوْنُوْا فُقَرَاۤءَ يُغْنِهِمُ اللّٰهُ مِنْ فَضْلِهٖۗ وَاللّٰهُ وَاسِعٌ عَلِيْمٌ

wa-anki*h*uu **a**l-ay*aa*m*aa* minkum wa**al***shshaa*li*h*iina min 'ib*aa*dikum wa-im*aa*\-ikum in yakuunuu fuqar*aa*\-a yughnihimu **al**l*aa*hu min fa*dh*lihi wa

Dan nikahkanlah orang-orang yang masih membujang di antara kamu, dan juga orang-orang yang layak (menikah) dari hamba-hamba sahayamu yang laki-laki dan perempuan. Jika mereka miskin, Allah akan memberi kemampuan kepada mereka dengan karunia-Nya. Dan Allah

24:33

# وَلْيَسْتَعْفِفِ الَّذِيْنَ لَا يَجِدُوْنَ نِكَاحًا حَتّٰى يُغْنِيَهُمُ اللّٰهُ مِنْ فَضْلِهٖ ۗوَالَّذِيْنَ يَبْتَغُوْنَ الْكِتٰبَ مِمَّا مَلَكَتْ اَيْمَانُكُمْ فَكَاتِبُوْهُمْ اِنْ عَلِمْتُمْ فِيْهِمْ خَيْرًا وَّاٰتُوْهُمْ مِّنْ مَّالِ اللّٰهِ الَّذِيْٓ

walyasta'fifi **al**la*dz*iina l*aa* yajiduuna nik*aah*an *h*att*aa* yughniyahumu **al**l*aa*hu min fa*dh*lihi wa**a**lla*dz*iina yabtaghuuna **a**lkit*aa*ba

Dan orang-orang yang tidak mampu menikah hendaklah menjaga kesucian (diri)nya, sampai Allah memberi kemampuan kepada mereka dengan karunia-Nya. Dan jika hamba sahaya yang kamu miliki menginginkan perjanjian (kebebasan), hendaklah kamu buat perjanjian kepa

24:34

# وَلَقَدْ اَنْزَلْنَآ اِلَيْكُمْ اٰيٰتٍ مُّبَيِّنٰتٍ وَّمَثَلًا مِّنَ الَّذِيْنَ خَلَوْا مِنْ قَبْلِكُمْ وَمَوْعِظَةً لِّلْمُتَّقِيْنَ ࣖ

walaqad anzaln*aa* ilaykum *aa*y*aa*tin mubayyin*aa*tin wamatsalan mina **al**la*dz*iina khalaw min qablikum wamaw'i*zh*atan lilmuttaqiin**a**

Dan sungguh, Kami telah menurunkan kepada kamu ayat-ayat yang memberi penjelasan, dan contoh-contoh dari orang-orang yang terdahulu sebelum kamu dan sebagai pelajaran bagi orang-orang yang bertakwa.

24:35

# ۞ اَللّٰهُ نُوْرُ السَّمٰوٰتِ وَالْاَرْضِۗ مَثَلُ نُوْرِهٖ كَمِشْكٰوةٍ فِيْهَا مِصْبَاحٌۗ اَلْمِصْبَاحُ فِيْ زُجَاجَةٍۗ اَلزُّجَاجَةُ كَاَنَّهَا كَوْكَبٌ دُرِّيٌّ يُّوْقَدُ مِنْ شَجَرَةٍ مُّبٰرَكَةٍ زَيْتُوْنَةٍ لَّا شَرْقِيَّةٍ وَّلَا غَرْبِيَّةٍۙ يَّك

**al**l*aa*hu nuuru **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i matsalu nuurihi kamisyk*aa*tin fiih*aa* mi*sh*b*aah*un **a**lmi*sh*b*aah*u fii zuj*aa*jat

Allah (pemberi) cahaya (kepada) langit dan bumi. Perumpamaan cahaya-Nya, seperti sebuah lubang yang tidak tembus, yang di dalamnya ada pelita besar. Pelita itu di dalam tabung kaca (dan) tabung kaca itu bagaikan bintang yang berkilauan, yang dinyalakan de

24:36

# فِيْ بُيُوْتٍ اَذِنَ اللّٰهُ اَنْ تُرْفَعَ وَيُذْكَرَ فِيْهَا اسْمُهٗۙ يُسَبِّحُ لَهٗ فِيْهَا بِالْغُدُوِّ وَالْاٰصَالِ ۙ

fii buyuutin a*dz*ina **al**l*aa*hu an turfa'a wayu*dz*kara fiih*aa* ismuhu yusabbi*h*u lahu fiih*aa* bi**a**lghuduwwi wa**a**l-*aasaa*l**i**

(Cahaya itu) di rumah-rumah yang di sana telah diperintahkan Allah untuk memuliakan dan menyebut nama-Nya, di sana bertasbih (menyucikan) nama-Nya pada waktu pagi dan petang,

24:37

# رِجَالٌ لَّا تُلْهِيْهِمْ تِجَارَةٌ وَّلَا بَيْعٌ عَنْ ذِكْرِ اللّٰهِ وَاِقَامِ الصَّلٰوةِ وَاِيْتَاۤءِ الزَّكٰوةِ ۙيَخَافُوْنَ يَوْمًا تَتَقَلَّبُ فِيْهِ الْقُلُوْبُ وَالْاَبْصَارُ ۙ

rij*aa*lun l*aa* tulhiihim tij*aa*ratun wal*aa* bay'un 'an *dz*ikri **al**l*aa*hi wa-iq*aa*mi **al***shsh*al*aa*ti wa-iit*aa*\-i **al**zzak*aa*ti yakh*aa*fuu

orang yang tidak dilalaikan oleh perdagangan dan jual beli dari mengingat Allah, melaksanakan salat, dan menunaikan zakat. Mereka takut kepada hari ketika hati dan penglihatan menjadi guncang (hari Kiamat),

24:38

# لِيَجْزِيَهُمُ اللّٰهُ اَحْسَنَ مَا عَمِلُوْا وَيَزِيْدَهُمْ مِّنْ فَضْلِهٖۗ وَاللّٰهُ يَرْزُقُ مَنْ يَّشَاۤءُ بِغَيْرِ حِسَابٍ

liyajziyahumu **al**l*aa*hu a*h*sana m*aa* 'amiluu wayaziidahum min fa*dh*lihi wa**al**l*aa*hu yarzuqu man yasy*aa*u bighayri *h*is*aa*b**in**

(mereka melakukan itu) agar Allah memberi balasan kepada mereka dengan yang lebih baik daripada apa yang telah mereka kerjakan, dan agar Dia menambah karunia-Nya kepada mereka. Dan Allah memberi rezeki kepada siapa saja yang Dia kehendaki tanpa batas.

24:39

# وَالَّذِيْنَ كَفَرُوْٓا اَعْمَالُهُمْ كَسَرَابٍۢ بِقِيْعَةٍ يَّحْسَبُهُ الظَّمْاٰنُ مَاۤءًۗ حَتّٰٓى اِذَا جَاۤءَهٗ لَمْ يَجِدْهُ شَيْـًٔا وَّوَجَدَ اللّٰهَ عِنْدَهٗ فَوَفّٰىهُ حِسَابَهٗ ۗ وَاللّٰهُ سَرِيْعُ الْحِسَابِ ۙ

wa**a**lla*dz*iina kafaruu a'm*aa*luhum kasar*aa*bin biqii'atin ya*h*sabuhu **al***zhzh*am-*aa*nu m*aa*\-an* h*att*aa *i*dzaa *j*aa*-ahu lam yajidhu syay-an wawajada **a**

Dan orang-orang yang kafir, amal perbuatan mereka seperti fatamorgana di tanah yang datar, yang disangka air oleh orang-orang yang dahaga, tetapi apabila (air) itu didatangi tidak ada apa pun. Dan didapatinya (ketetapan) Allah baginya. Lalu Allah memberik

24:40

# اَوْ كَظُلُمٰتٍ فِيْ بَحْرٍ لُّجِّيٍّ يَّغْشٰىهُ مَوْجٌ مِّنْ فَوْقِهٖ مَوْجٌ مِّنْ فَوْقِهٖ سَحَابٌۗ ظُلُمٰتٌۢ بَعْضُهَا فَوْقَ بَعْضٍۗ اِذَآ اَخْرَجَ يَدَهٗ لَمْ يَكَدْ يَرٰىهَاۗ وَمَنْ لَّمْ يَجْعَلِ اللّٰهُ لَهٗ نُوْرًا فَمَا لَهٗ مِنْ نُّوْرٍ ࣖ

aw ka*zh*ulum*aa*tin fii ba*h*rin lujjiyyin yaghsy*aa*hu mawjun min fawqihi mawjun min fawqihi sa*haa*bun *zh*ulum*aa*tun ba'*dh*uh*aa* fawqa ba'*dh*in i*dzaa* akhraja yadahu lam yakad yar*aa*h

Atau (keadaan orang-orang kafir) seperti gelap gulita di lautan yang dalam, yang diliputi oleh gelombang demi gelombang, di atasnya ada (lagi) awan gelap. Itulah gelap gulita yang berlapis-lapis. Apabila dia mengeluarkan tangannya hampir tidak dapat melih

24:41

# اَلَمْ تَرَ اَنَّ اللّٰهَ يُسَبِّحُ لَهٗ مَنْ فِى السَّمٰوٰتِ وَالْاَرْضِ وَالطَّيْرُ صٰۤفّٰتٍۗ كُلٌّ قَدْ عَلِمَ صَلَاتَهٗ وَتَسْبِيْحَهٗۗ وَاللّٰهُ عَلِيْمٌۢ بِمَا يَفْعَلُوْنَ

alam tara anna **al**l*aa*ha yusabbi*h*u lahu man fii **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i wa**al***ththh*ayru* shaa*ff*aa*tin kullun qad 'alima* sh*al*aa*

Tidakkah engkau (Muhammad) tahu bahwa kepada Allah-lah bertasbih apa yang di langit dan di bumi, dan juga burung yang mengembangkan sayapnya. Masing-masing sungguh, telah mengetahui (cara) berdoa dan bertasbih. Allah Maha Mengetahui apa yang mereka kerjak

24:42

# وَلِلّٰهِ مُلْكُ السَّمٰوٰتِ وَالْاَرْضِۚ وَاِلَى اللّٰهِ الْمَصِيْرُ

walill*aa*hi mulku **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i wa-il*aa* **al**l*aa*hi **a**lma*sh*iir**u**

Dan milik Allah-lah kerajaan langit dan bumi dan hanya kepada Allah-lah kembali (seluruh makhluk).

24:43

# اَلَمْ تَرَ اَنَّ اللّٰهَ يُزْجِيْ سَحَابًا ثُمَّ يُؤَلِّفُ بَيْنَهٗ ثُمَّ يَجْعَلُهٗ رُكَامًا فَتَرَى الْوَدْقَ يَخْرُجُ مِنْ خِلٰلِهٖۚ وَيُنَزِّلُ مِنَ السَّمَاۤءِ مِنْ جِبَالٍ فِيْهَا مِنْۢ بَرَدٍ فَيُصِيْبُ بِهٖ مَنْ يَّشَاۤءُ وَيَصْرِفُهٗ عَنْ مَّنْ

alam tara anna **al**l*aa*ha yuzjii sa*haa*ban tsumma yu-allifu baynahu tsumma yaj'aluhu ruk*aa*man fatar*aa* **a**lwadqa yakhruju min khil*aa*lihi wayunazzilu mina **al**ssam*aa*-i min

Tidakkah engkau melihat bahwa Allah menjadikan awan bergerak perlahan, kemudian mengumpulkannya, lalu Dia menjadikannya bertumpuk-tumpuk, lalu engkau lihat hujan keluar dari celah-celahnya dan Dia (juga) menurunkan (butiran-butiran) es dari langit, (yaitu

24:44

# يُقَلِّبُ اللّٰهُ الَّيْلَ وَالنَّهَارَۗ اِنَّ فِيْ ذٰلِكَ لَعِبْرَةً لِّاُولِى الْاَبْصَارِ

yuqallibu **al**l*aa*hu **al**layla wa**al**nnah*aa*ra inna fii *dzaa*lika la'ibratan li-ulii **a**l-ab*shaa*r**i**

Allah mempergantikan malam dan siang. Sesungguhnya pada yang demikian itu, pasti terdapat pelajaran bagi orang-orang yang mempunyai penglihatan (yang tajam).

24:45

# وَاللّٰهُ خَلَقَ كُلَّ دَاۤبَّةٍ مِّنْ مَّاۤءٍۚ فَمِنْهُمْ مَّنْ يَّمْشِيْ عَلٰى بَطْنِهٖۚ وَمِنْهُمْ مَّنْ يَّمْشِيْ عَلٰى رِجْلَيْنِۚ وَمِنْهُمْ مَّنْ يَّمْشِيْ عَلٰٓى اَرْبَعٍۗ يَخْلُقُ اللّٰهُ مَا يَشَاۤءُۗ اِنَّ اللّٰهَ عَلٰى كُلِّ شَيْءٍ قَدِيْرٌ

wa**al**l*aa*hu khalaqa kulla d*aa*bbatin min m*aa*-in faminhum man yamsyii 'al*aa* ba*th*nihi waminhum man yamsyii 'al*aa* rijlayni waminhum man yamsyii 'al*aa* arba'in yakhluqu **al**l*aa*

*Dan Allah menciptakan semua jenis hewan dari air, maka sebagian ada yang berjalan di atas perutnya dan sebagian berjalan dengan dua kaki, sedang sebagian (yang lain) berjalan dengan empat kaki. Allah menciptakan apa yang Dia kehendaki. Sungguh, Allah Maha*

24:46

# لَقَدْ اَنْزَلْنَآ اٰيٰتٍ مُّبَيِّنٰتٍۗ وَاللّٰهُ يَهْدِيْ مَنْ يَّشَاۤءُ اِلٰى صِرَاطٍ مُّسْتَقِيْمٍ

laqad anzaln*aa* *aa*y*aa*tin mubayyin*aa*tin wa**al**l*aa*hu yahdii man yasy*aa*u il*aa* *sh*ir*aath*in mustaqiim**in**

Sungguh, Kami telah menurunkan ayat-ayat yang memberi penjelasan. Dan Allah memberi petunjuk siapa yang Dia kehendaki ke jalan yang lurus.

24:47

# وَيَقُوْلُوْنَ اٰمَنَّا بِاللّٰهِ وَبِالرَّسُوْلِ وَاَطَعْنَا ثُمَّ يَتَوَلّٰى فَرِيْقٌ مِّنْهُمْ مِّنْۢ بَعْدِ ذٰلِكَۗ وَمَآ اُولٰۤىِٕكَ بِالْمُؤْمِنِيْنَ

wayaquuluuna *aa*mann*aa* bi**al**l*aa*hi wabi**al**rrasuuli wa-a*th*a'n*aa* tsumma yatawall*aa* fariiqun minhum min ba'di *dzaa*lika wam*aa* ul*aa*-ika bi**a**lmu/miniin<

Dan mereka (orang-orang munafik) berkata, “Kami telah beriman kepada Allah dan Rasul (Muhammad), dan kami menaati (keduanya).” Kemudian sebagian dari mereka berpaling setelah itu. Mereka itu bukanlah orang-orang beriman.

24:48

# وَاِذَا دُعُوْٓا اِلَى اللّٰهِ وَرَسُوْلِهٖ لِيَحْكُمَ بَيْنَهُمْ اِذَا فَرِيْقٌ مِّنْهُمْ مُّعْرِضُوْنَ

wa-i*dzaa* du'uu il*aa* **al**l*aa*hi warasuulihi liya*h*kuma baynahum i*dzaa* fariiqun minhum mu'ri*dh*uun**a**

Dan apabila mereka diajak kepada Allah dan Rasul-Nya, agar (Rasul) memutuskan perkara di antara mereka, tiba-tiba sebagian dari mereka menolak (untuk datang).

24:49

# وَاِنْ يَّكُنْ لَّهُمُ الْحَقُّ يَأْتُوْٓا اِلَيْهِ مُذْعِنِيْنَ

wa-in yakun lahumu **a**l*h*aqqu ya/tuu ilayhi mu*dz*'iniin**a**

Tetapi, jika kebenaran di pihak mereka, mereka datang kepadanya (Rasul) dengan patuh.

24:50

# اَفِيْ قُلُوْبِهِمْ مَّرَضٌ اَمِ ارْتَابُوْٓا اَمْ يَخَافُوْنَ اَنْ يَّحِيْفَ اللّٰهُ عَلَيْهِمْ وَرَسُوْلُهٗ ۗبَلْ اُولٰۤىِٕكَ هُمُ الظّٰلِمُوْنَ ࣖ

afii quluubihim mara*dh*un ami irt*aa*buu am yakh*aa*fuuna an ya*h*iifa **al**l*aa*hu 'alayhim warasuuluhu bal ul*aa*-ika humu **al***zhzhaa*limuun**a**

Apakah (ketidakhadiran mereka karena) dalam hati mereka ada penyakit, atau (karena) mereka ragu-ragu ataukah (karena) takut kalau-kalau Allah dan Rasul-Nya berlaku zalim kepada mereka? Sebenarnya, mereka itulah orang-orang yang zalim.

24:51

# اِنَّمَا كَانَ قَوْلَ الْمُؤْمِنِيْنَ اِذَا دُعُوْٓا اِلَى اللّٰهِ وَرَسُوْلِهٖ لِيَحْكُمَ بَيْنَهُمْ اَنْ يَّقُوْلُوْا سَمِعْنَا وَاَطَعْنَاۗ وَاُولٰۤىِٕكَ هُمُ الْمُفْلِحُوْنَ

innam*aa* k*aa*na qawla **a**lmu/miniina i*dzaa* du'uu il*aa* **al**l*aa*hi warasuulihi liya*h*kuma baynahum an yaquuluu sami'n*aa* wa-a*th*a'n*aa* waul*aa*-ika humu **a**

**Hanya ucapan orang-orang mukmin, yang apabila mereka diajak kepada Allah dan Rasul-Nya agar Rasul memutuskan (perkara) di antara mereka, mereka berkata, “Kami mendengar, dan kami taat.” Dan mereka itulah orang-orang yang beruntung.**

24:52

# وَمَنْ يُّطِعِ اللّٰهَ وَرَسُوْلَهٗ وَيَخْشَ اللّٰهَ وَيَتَّقْهِ فَاُولٰۤىِٕكَ هُمُ الْفَاۤىِٕزُوْنَ

waman yu*th*i'i **al**l*aa*ha warasuulahu wayakhsya **al**l*aa*ha wayattaqhi faul*aa*-ika humu **a**lf*aa*-izuun**a**

Dan barangsiapa taat kepada Allah dan Rasul-Nya serta takut kepada Allah dan bertakwa kepada-Nya, mereka itulah orang-orang yang mendapat kemenangan.

24:53

# ۞ وَاَقْسَمُوْا بِاللّٰهِ جَهْدَ اَيْمَانِهِمْ لَىِٕنْ اَمَرْتَهُمْ لَيَخْرُجُنَّۗ قُلْ لَّا تُقْسِمُوْاۚ طَاعَةٌ مَّعْرُوْفَةٌ ۗاِنَّ اللّٰهَ خَبِيْرٌۢ بِمَا تَعْمَلُوْنَ

wa-aqsamuu bi**al**l*aa*hi jahda aym*aa*nihim la-in amartahum layakhrujunna qul l*aa* tuqsimuu *thaa*'atun ma'ruufatun inna **al**l*aa*ha khabiirun bim*aa* ta'maluun**a**

Dan mereka bersumpah dengan (nama) Allah dengan sumpah sungguh-sungguh, bahwa jika engkau suruh mereka berperang, pastilah mereka akan pergi. Katakanlah (Muhammad), “Janganlah kamu bersumpah, (karena yang diminta) adalah ketaatan yang baik. Sungguh, Allah

24:54

# قُلْ اَطِيْعُوا اللّٰهَ وَاَطِيْعُوا الرَّسُوْلَۚ فَاِنْ تَوَلَّوْا فَاِنَّمَا عَلَيْهِ مَا حُمِّلَ وَعَلَيْكُمْ مَّا حُمِّلْتُمْۗ وَاِنْ تُطِيْعُوْهُ تَهْتَدُوْاۗ وَمَا عَلَى الرَّسُوْلِ اِلَّا الْبَلٰغُ الْمُبِيْنُ

qul a*th*ii'uu **al**l*aa*ha wa-a*th*ii'uu **al**rrasuula fa-in tawallaw fa-innam*aa* 'alayhi m*aa* *h*ummila wa'alaykum m*aa* *h*ummiltum wa-in tu*th*ii'uuhu tahtaduu wam*aa* 'al

Katakanlah, “Taatlah kepada Allah dan taatlah kepada Rasul; jika kamu berpaling, maka sesungguhnya kewajiban Rasul (Muhammad) itu hanyalah apa yang dibebankan kepadanya, dan kewajiban kamu hanyalah apa yang dibebankan kepadamu. Jika kamu taat kepadanya, n

24:55

# وَعَدَ اللّٰهُ الَّذِيْنَ اٰمَنُوْا مِنْكُمْ وَعَمِلُوا الصّٰلِحٰتِ لَيَسْتَخْلِفَنَّهُمْ فِى الْاَرْضِ كَمَا اسْتَخْلَفَ الَّذِيْنَ مِنْ قَبْلِهِمْۖ وَلَيُمَكِّنَنَّ لَهُمْ دِيْنَهُمُ الَّذِى ارْتَضٰى لَهُمْ وَلَيُبَدِّلَنَّهُمْ مِّنْۢ بَعْدِ خَوْفِهِمْ

wa'ada **al**l*aa*hu **al**la*dz*iina *aa*manuu minkum wa'amiluu **al***shshaa*li*haa*ti layastakhlifannahum fii **a**l-ar*dh*i kam*aa* istakhlafa **al**la

Allah telah menjanjikan kepada orang-orang di antara kamu yang beriman dan yang mengerjakan kebajikan, bahwa Dia sungguh, akan menjadikan mereka berkuasa di bumi, sebagaimana Dia telah menjadikan orang-orang sebelum mereka berkuasa, dan sungguh, Dia akan

24:56

# وَاَقِيْمُوا الصَّلٰوةَ وَاٰتُوا الزَّكٰوةَ وَاَطِيْعُوا الرَّسُوْلَ لَعَلَّكُمْ تُرْحَمُوْنَ

wa-aqiimuu **al***shsh*al*aa*ta wa*aa*tuu **al**zzak*aa*ta wa-a*th*ii'uu **al**rrasuula la'allakum tur*h*amuun**a**

Dan laksanakanlah salat, tunaikanlah zakat, dan taatlah kepada Rasul (Muhammad), agar kamu diberi rahmat.

24:57

# لَا تَحْسَبَنَّ الَّذِيْنَ كَفَرُوْا مُعْجِزِيْنَ فِى الْاَرْضِۚ وَمَأْوٰىهُمُ النَّارُۗ وَلَبِئْسَ الْمَصِيْرُ ࣖ

l*aa* ta*h*sabanna **al**la*dz*iina kafaruu mu'jiziina fii **a**l-ar*dh*i wama/w*aa*humu **al**nn*aa*ru walabi/sa **a**lma*sh*iir**u**

Janganlah engkau mengira bahwa orang-orang yang kafir itu dapat luput dari siksaan Allah di bumi; sedang tempat kembali mereka (di akhirat) adalah neraka. Dan itulah seburuk-buruk tempat kembali.

24:58

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْا لِيَسْتَأْذِنْكُمُ الَّذِيْنَ مَلَكَتْ اَيْمَانُكُمْ وَالَّذِيْنَ لَمْ يَبْلُغُوا الْحُلُمَ مِنْكُمْ ثَلٰثَ مَرّٰتٍۗ مِنْ قَبْلِ صَلٰوةِ الْفَجْرِ وَحِيْنَ تَضَعُوْنَ ثِيَابَكُمْ مِّنَ الظَّهِيْرَةِ وَمِنْۢ بَعْدِ صَلٰوةِ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu liyasta/*dz*inkumu **al**la*dz*iina malakat aym*aa*nukum wa**a**lla*dz*iina lam yablughuu **a**l*h*uluma minkum tsa

Wahai orang-orang yang beriman! Hendaklah hamba sahaya (laki-laki dan perempuan) yang kamu miliki, dan orang-orang yang belum balig (dewasa) di antara kamu, meminta izin kepada kamu pada tiga kali (kesempatan) yaitu, sebelum salat Subuh, ketika kamu menan

24:59

# وَاِذَا بَلَغَ الْاَطْفَالُ مِنْكُمُ الْحُلُمَ فَلْيَسْتَأْذِنُوْا كَمَا اسْتَأْذَنَ الَّذِيْنَ مِنْ قَبْلِهِمْۗ كَذٰلِكَ يُبَيِّنُ اللّٰهُ لَكُمْ اٰيٰتِهٖۗ وَاللّٰهُ عَلِيْمٌ حَكِيْمٌ

wa-i*dzaa* balagha **a**l-a*th*f*aa*lu minkumu **a**l*h*uluma falyasta/*dz*inuu kam*aa* ista/*dz*ana **al**la*dz*iina min qablihim ka*dzaa*lika yubayyinu **al**

**Dan apabila anak-anakmu telah sampai umur dewasa, maka hendaklah mereka (juga) meminta izin, seperti orang-orang yang lebih dewasa meminta izin. Demikianlah Allah menjelaskan ayat-ayat-Nya kepadamu. Allah Maha Mengetahui, Mahabijaksana.**

24:60

# وَالْقَوَاعِدُ مِنَ النِّسَاۤءِ الّٰتِيْ لَا يَرْجُوْنَ نِكَاحًا فَلَيْسَ عَلَيْهِنَّ جُنَاحٌ اَنْ يَّضَعْنَ ثِيَابَهُنَّ غَيْرَ مُتَبَرِّجٰتٍۢ بِزِيْنَةٍۗ وَاَنْ يَّسْتَعْفِفْنَ خَيْرٌ لَّهُنَّۗ وَاللّٰهُ سَمِيْعٌ عَلِيْمٌ

wa**a**lqaw*aa*'idu mina **al**nnis*aa*-i **al**l*aa*tii l*aa* yarjuuna nik*aah*an falaysa 'alayhinna jun*aah*un an ya*dh*a'na tsiy*aa*bahunna ghayra mutabarrij*aa*tin biz

Dan para perempuan tua yang telah berhenti (dari haid dan mengandung) yang tidak ingin menikah (lagi), maka tidak ada dosa menanggalkan pakaian (luar) mereka dengan tidak (bermaksud) menampakkan perhiasan; tetapi memelihara kehormatan adalah lebih baik ba

24:61

# لَيْسَ عَلَى الْاَعْمٰى حَرَجٌ وَّلَا عَلَى الْاَعْرَجِ حَرَجٌ وَّلَا عَلَى الْمَرِيْضِ حَرَجٌ وَّلَا عَلٰٓى اَنْفُسِكُمْ اَنْ تَأْكُلُوْا مِنْۢ بُيُوْتِكُمْ اَوْ بُيُوْتِ اٰبَاۤىِٕكُمْ اَوْ بُيُوْتِ اُمَّهٰتِكُمْ اَوْ بُيُوْتِ اِخْوَانِكُمْ اَوْ بُيُوْتِ

laysa 'al*aa* **a**l-a'm*aa* *h*arajun wal*aa* 'al*aa* **a**l-a'raji *h*arajun wal*aa* 'al*aa* **a**lmarii*dh*i *h*arajun wal*aa* 'al*aa* anfusikum an ta/ku

Tidak ada halangan bagi orang buta, tidak (pula) bagi orang pincang, tidak (pula) bagi orang sakit, dan tidak (pula) bagi dirimu, makan (bersama-sama mereka) di rumah kamu atau di rumah bapak-bapakmu, di rumah ibu-ibumu, di rumah saudara-saudaramu yang la

24:62

# اِنَّمَا الْمُؤْمِنُوْنَ الَّذِيْنَ اٰمَنُوْا بِاللّٰهِ وَرَسُوْلِهٖ وَاِذَا كَانُوْا مَعَهٗ عَلٰٓى اَمْرٍ جَامِعٍ لَّمْ يَذْهَبُوْا حَتّٰى يَسْتَأْذِنُوْهُۗ اِنَّ الَّذِيْنَ يَسْتَأْذِنُوْنَكَ اُولٰۤىِٕكَ الَّذِيْنَ يُؤْمِنُوْنَ بِاللّٰهِ وَرَسُوْلِهٖۚ ف

innam*aa* **a**lmu/minuuna **al**la*dz*iina *aa*manuu bi**al**l*aa*hi warasuulihi wa-i*dzaa* k*aa*nuu ma'ahu 'al*aa* amrin j*aa*mi'in lam ya*dz*habuu *h*att*aa*

(Yang disebut) orang mukmin hanyalah orang yang beriman kepada Allah dan Rasul-Nya (Muhammad), dan apabila mereka berada bersama-sama dengan dia (Muhammad) dalam suatu urusan bersama, mereka tidak meninggalkan (Rasulullah) sebelum meminta izin kepadanya.

24:63

# لَا تَجْعَلُوْا دُعَاۤءَ الرَّسُوْلِ بَيْنَكُمْ كَدُعَاۤءِ بَعْضِكُمْ بَعْضًاۗ قَدْ يَعْلَمُ اللّٰهُ الَّذِيْنَ يَتَسَلَّلُوْنَ مِنْكُمْ لِوَاذًاۚ فَلْيَحْذَرِ الَّذِيْنَ يُخَالِفُوْنَ عَنْ اَمْرِهٖٓ اَنْ تُصِيْبَهُمْ فِتْنَةٌ اَوْ يُصِيْبَهُمْ عَذَابٌ اَ

l*aa* taj'aluu du'*aa*-a **al**rrasuuli baynakum kadu'*aa*-i ba'*dh*ikum ba'*dh*an qad ya'lamu **al**l*aa*hu **al**la*dz*iina yatasallaluuna minkum liw*aadz*an falya*hts*a

Janganlah kamu jadikan panggilan Rasul (Muhammad) di antara kamu seperti panggilan sebagian kamu kepada sebagian (yang lain). Sungguh, Allah mengetahui orang-orang yang keluar (secara) sembunyi-sembunyi di antara kamu dengan berlindung (kepada kawannya),

24:64

# اَلَآ اِنَّ لِلّٰهِ مَا فِى السَّمٰوٰتِ وَالْاَرْضِۗ قَدْ يَعْلَمُ مَآ اَنْتُمْ عَلَيْهِۗ وَيَوْمَ يُرْجَعُوْنَ اِلَيْهِ فَيُنَبِّئُهُمْ بِمَا عَمِلُوْاۗ وَاللّٰهُ بِكُلِّ شَيْءٍ عَلِيْمٌ ࣖ

al*aa* inna lill*aa*hi m*aa* fii **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i qad ya'lamu m*aa* antum 'alayhi wayawma yurja'uuna ilayhi fayunabbi-uhum bim*aa* 'amiluu wa**al**l*aa<*

Ketahuilah, sesungguhnya milik Allah-lah apa yang di langit dan di bumi. Dia mengetahui keadaan kamu sekarang. Dan (mengetahui pula) hari (ketika mereka) dikembalikan kepada-Nya, lalu diterangkan-Nya kepada mereka apa yang telah mereka kerjakan. Dan Allah maha mengetahui segala sesuatu.

<!--EndFragment-->