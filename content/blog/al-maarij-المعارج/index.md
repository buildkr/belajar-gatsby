---
title: (70) Al-Ma'arij - المعارج
date: 2021-10-27T04:06:07.545Z
ayat: 70
description: "Jumlah Ayat: 44 / Arti: Tempat Naik"
---
<!--StartFragment-->

70:1

# سَاَلَ سَاۤىِٕلٌۢ بِعَذَابٍ وَّاقِعٍۙ

sa-ala s*aa*-ilun bi'a*dzaa*bin w*aa*qi'**in**

Seseorang bertanya tentang azab yang pasti terjadi,

70:2

# لِّلْكٰفِرِيْنَ لَيْسَ لَهٗ دَافِعٌۙ

lilk*aa*firiina laysa lahu d*aa*fi'**un**

bagi orang-orang kafir, yang tidak seorang pun dapat menolaknya,

70:3

# مِّنَ اللّٰهِ ذِى الْمَعَارِجِۗ

mina **al**l*aa*hi *dz*ii **a**lma'*aa*rij**i**

(Azab) dari Allah, yang memiliki tempat-tempat naik.

70:4

# تَعْرُجُ الْمَلٰۤىِٕكَةُ وَالرُّوْحُ اِلَيْهِ فِيْ يَوْمٍ كَانَ مِقْدَارُهٗ خَمْسِيْنَ اَلْفَ سَنَةٍۚ

ta'ruju **a**lmal*aa*-ikatu wa**al**rruu*h*u ilayhi fii yawmin k*aa*na miqd*aa*ruhu khamsiina **a**lfa sana**tin**

Para malaikat dan Jibril naik (menghadap) kepada Tuhan, dalam sehari setara dengan lima puluh ribu tahun.

70:5

# فَاصْبِرْ صَبْرًا جَمِيْلًا

fa**i***sh*bir *sh*abran jamiil*aa***n**

Maka bersabarlah engkau (Muhammad) dengan kesabaran yang baik.

70:6

# اِنَّهُمْ يَرَوْنَهٗ بَعِيْدًاۙ

innahum yarawnahu ba'iid*aa***n**

Mereka memandang (azab) itu jauh (mustahil).

70:7

# وَّنَرٰىهُ قَرِيْبًاۗ

wanar*aa*hu qariib*aa***n**

Sedangkan Kami memandangnya dekat (pasti terjadi).

70:8

# يَوْمَ تَكُوْنُ السَّمَاۤءُ كَالْمُهْلِۙ

yawma takuunu **al**ssam*aa*u ka**a**lmuhl**i**

(Ingatlah) pada hari ketika langit men-jadi bagaikan cairan tembaga,

70:9

# وَتَكُوْنُ الْجِبَالُ كَالْعِهْنِۙ

watakuunu **a**ljib*aa*lu ka**a**l'ihn**i**

dan gunung-gunung bagaikan bulu (yang beterbangan),

70:10

# وَلَا يَسْـَٔلُ حَمِيْمٌ حَمِيْمًاۚ

wal*aa* yas-alu *h*amiimun *h*amiim*aa***n**

dan tidak ada seorang teman karib pun menanyakan temannya,

70:11

# يُبَصَّرُوْنَهُمْۗ يَوَدُّ الْمُجْرِمُ لَوْ يَفْتَدِيْ مِنْ عَذَابِ يَوْمِىِٕذٍۢ بِبَنِيْهِۙ

yuba*shsh*aruunahum yawaddu **a**lmujrimu law yaftadii min 'a*dzaa*bi yawmi-i*dz*in bibaniih**i**

sedang mereka saling melihat. Pada hari itu, orang yang berdosa ingin sekiranya dia dapat menebus (dirinya) dari azab dengan anak-anaknya,

70:12

# وَصَاحِبَتِهٖ وَاَخِيْهِۙ

wa*shaah*ibatihi wa-akhiih**i**

dan istrinya dan saudaranya,

70:13

# وَفَصِيْلَتِهِ الَّتِيْ تُـْٔوِيْهِۙ

wafa*sh*iilatihi **al**latii tu/wiih**i**

dan keluarga yang melindunginya (di dunia),

70:14

# وَمَنْ فِى الْاَرْضِ جَمِيْعًاۙ ثُمَّ يُنْجِيْهِۙ

waman fii **a**l-ar*dh*i jamii'an tsumma yunjiih**i**

dan orang-orang di bumi seluruhnya, kemudian mengharapkan (tebusan) itu dapat menyelamatkannya.

70:15

# كَلَّاۗ اِنَّهَا لَظٰىۙ

kall*aa* innah*aa* la*zhaa*

Sama sekali tidak! Sungguh, neraka itu api yang bergejolak,

70:16

# نَزَّاعَةً لِّلشَّوٰىۚ

nazz*aa*'atan li**l**sysyaw*aa*

yang mengelupaskan kulit kepala.

70:17

# تَدْعُوْا مَنْ اَدْبَرَ وَتَوَلّٰىۙ

tad'uu man adbara watawall*aa*

Yang memanggil orang yang membelakangi dan yang berpaling (dari agama),

70:18

# وَجَمَعَ فَاَوْعٰى

wajama'a fa-aw'*aa*

dan orang yang mengumpulkan (harta benda) lalu menyimpannya.

70:19

# ۞ اِنَّ الْاِنْسَانَ خُلِقَ هَلُوْعًاۙ

inna **a**l-ins*aa*na khuliqa haluu'*aa***n**

Sungguh, manusia diciptakan bersifat suka mengeluh.

70:20

# اِذَا مَسَّهُ الشَّرُّ جَزُوْعًاۙ

i*dzaa* massahu **al**sysyarru jazuu'*aa***n**

Apabila dia ditimpa kesusahan dia berkeluh kesah,

70:21

# وَّاِذَا مَسَّهُ الْخَيْرُ مَنُوْعًاۙ

wa-i*dzaa* massahu **a**lkhayru manuu'*aa***n**

dan apabila mendapat kebaikan (harta) dia jadi kikir,

70:22

# اِلَّا الْمُصَلِّيْنَۙ

ill*aa* **a**lmu*sh*alliin**a**

kecuali orang-orang yang melaksanakan salat,

70:23

# الَّذِيْنَ هُمْ عَلٰى صَلَاتِهِمْ دَاۤىِٕمُوْنَۖ

ill*aa* **a**lmu*sh*alliin**a**

mereka yang tetap setia melaksanakan salatnya,

70:24

# وَالَّذِيْنَ فِيْٓ اَمْوَالِهِمْ حَقٌّ مَّعْلُوْمٌۖ

wa**a**lla*dz*iina fii amw*aa*lihim *h*aqqun ma'luum**un**

dan orang-orang yang dalam hartanya disiapkan bagian tertentu,

70:25

# لِّلسَّاۤىِٕلِ وَالْمَحْرُوْمِۖ

li**l**ss*aa*-ili wa**a**lma*h*ruum**i**

bagi orang (miskin) yang meminta dan yang tidak meminta,

70:26

# وَالَّذِيْنَ يُصَدِّقُوْنَ بِيَوْمِ الدِّيْنِۖ

wa**a**lla*dz*iina yu*sh*addiquuna biyawmi **al**ddiin**i**

dan orang-orang yang mempercayai hari pembalasan,

70:27

# وَالَّذِيْنَ هُمْ مِّنْ عَذَابِ رَبِّهِمْ مُّشْفِقُوْنَۚ

wa**a**lla*dz*iina hum min 'a*dzaa*bi rabbihim musyfiquun**a**

dan orang-orang yang takut terhadap azab Tuhannya,

70:28

# اِنَّ عَذَابَ رَبِّهِمْ غَيْرُ مَأْمُوْنٍۖ

inna 'a*dzaa*ba rabbihim ghayru ma/muun**in**

sesungguhnya terhadap azab Tuhan mereka, tidak ada seseorang yang merasa aman (dari kedatangannya),

70:29

# وَّالَّذِيْنَ هُمْ لِفُرُوْجِهِمْ حٰفِظُوْنَۙ

wa**a**lla*dz*iina hum lifuruujihim *haa*fi*zh*uun**a**

dan orang-orang yang memelihara kemaluannya,

70:30

# اِلَّا عَلٰٓى اَزْوَاجِهِمْ اَوْ مَا مَلَكَتْ اَيْمَانُهُمْ فَاِنَّهُمْ غَيْرُ مَلُوْمِيْنَۚ

ill*aa* 'al*aa* azw*aa*jihim aw m*aa* malakat aym*aa*nuhum fa-innahum ghayru maluumiin**a**

kecuali terhadap istri-istri mereka atau hamba sahaya yang mereka miliki maka sesungguhnya mereka tidak tercela.

70:31

# فَمَنِ ابْتَغٰى وَرَاۤءَ ذٰلِكَ فَاُولٰۤىِٕكَ هُمُ الْعٰدُوْنَۚ

famani ibtagh*aa* war*aa*-a *dzaa*lika faul*aa*-ika humu **a**l'*aa*duun**a**

Maka barangsiapa mencari di luar itu (seperti zina, homoseks dan lesbian), mereka itulah orang-orang yang melampaui batas.

70:32

# وَالَّذِيْنَ هُمْ لِاَمٰنٰتِهِمْ وَعَهْدِهِمْ رَاعُوْنَۖ

wa**a**lla*dz*iina hum li-am*aa*n*aa*tihim wa'ahdihim r*aa*'uun**a**

Dan orang-orang yang memelihara amanat dan janjinya,

70:33

# وَالَّذِيْنَ هُمْ بِشَهٰدٰتِهِمْ قَاۤىِٕمُوْنَۖ

wa**a**lla*dz*iina hum bisyah*aa*d*aa*tihim q*aa*-imuun**a**

dan orang-orang yang berpegang teguh pada kesaksiannya,

70:34

# وَالَّذِيْنَ هُمْ عَلٰى صَلَاتِهِمْ يُحَافِظُوْنَۖ

wa**a**lla*dz*iina hum 'al*aa* *sh*al*aa*tihim yu*haa*fi*zh*uun**a**

dan orang-orang yang memelihara salatnya.

70:35

# اُولٰۤىِٕكَ فِيْ جَنّٰتٍ مُّكْرَمُوْنَ ۗ ࣖ

ul*aa*-ika fii jann*aa*tin mukramuun**a**

Mereka itu dimuliakan di dalam surga.

70:36

# فَمَالِ الَّذِيْنَ كَفَرُوْا قِبَلَكَ مُهْطِعِيْنَۙ

fam*aa*li **al**la*dz*iina kafaruu qibalaka muh*th*i'iin**a**

Maka mengapa orang-orang kafir itu datang bergegas ke hadapanmu (Muhammad),

70:37

# عَنِ الْيَمِيْنِ وَعَنِ الشِّمَالِ عِزِيْنَ

'ani **a**lyamiini wa'ani **al**sysyim*aa*li 'iziin**a**

dari kanan dan dari kiri dengan berkelompok-kelompok?

70:38

# اَيَطْمَعُ كُلُّ امْرِئٍ مِّنْهُمْ اَنْ يُّدْخَلَ جَنَّةَ نَعِيْمٍۙ

aya*th*ma'u kullu imri-in minhum an yudkhala jannata na'iim**in**

Apakah setiap orang dari orang-orang kafir itu ingin masuk surga yang penuh kenikmatan?

70:39

# كَلَّاۗ اِنَّا خَلَقْنٰهُمْ مِّمَّا يَعْلَمُوْنَ

kall*aa* inn*aa* khalaqn*aa*hum mimm*aa* ya'lamuun**a**

Tidak mungkin! Sesungguhnya Kami menciptakan mereka dari apa yang mereka ketahui.

70:40

# فَلَآ اُقْسِمُ بِرَبِّ الْمَشَارِقِ وَالْمَغٰرِبِ اِنَّا لَقٰدِرُوْنَۙ

fal*aa* uqsimu birabbi **a**lmasy*aa*riqi wa**a**lmagh*aa*ribi inn*aa* laq*aa*diruun**a**

Maka Aku bersumpah demi Tuhan yang mengatur tempat-tempat terbit dan terbenamnya (matahari, bulan dan bintang), sungguh, Kami pasti mampu,

70:41

# عَلٰٓى اَنْ نُّبَدِّلَ خَيْرًا مِّنْهُمْۙ وَمَا نَحْنُ بِمَسْبُوْقِيْنَ

'al*aa* an nubaddila khayran minhum wam*aa* na*h*nu bimasbuuqiin**a**

untuk mengganti (mereka) dengan kaum yang lebih baik dari mereka, dan Kami tidak dapat dikalahkan.

70:42

# فَذَرْهُمْ يَخُوْضُوْا وَيَلْعَبُوْا حَتّٰى يُلٰقُوْا يَوْمَهُمُ الَّذِيْ يُوْعَدُوْنَۙ

fa*dz*arhum yakhuu*dh*uu wayal'abuu *h*att*aa* yul*aa*quu yawmahumu **al**la*dz*ii yuu'aduun**a**

Maka biarkanlah mereka tenggelam dan bermain-main (dalam kesesatan) sampai mereka menjumpai hari yang diancamkan kepada mereka,

70:43

# يَوْمَ يَخْرُجُوْنَ مِنَ الْاَجْدَاثِ سِرَاعًا كَاَنَّهُمْ اِلٰى نُصُبٍ يُّوْفِضُوْنَۙ

yawma yakhrujuuna mina **a**l-ajd*aa*tsi sir*aa*'an ka-annahum il*aa* nu*sh*ubin yuufi*dh*uun**a**

(yaitu) pada hari ketika mereka keluar dari kubur dengan cepat seakan-akan mereka pergi dengan segera kepada berhala-berhala (sewaktu di dunia),

70:44

# خَاشِعَةً اَبْصَارُهُمْ تَرْهَقُهُمْ ذِلَّةٌ ۗذٰلِكَ الْيَوْمُ الَّذِيْ كَانُوْا يُوْعَدُوْنَ ࣖ

kh*aa*syi'atan ab*shaa*ruhum tarhaquhum *dz*illatun *dzaa*lika **a**lyawmu **al**la*dz*ii k*aa*nuu yuu'aduun**a**

pandangan mereka tertunduk ke bawah diliputi kehinaan. Itulah hari yang diancamkan kepada mereka.

<!--EndFragment-->