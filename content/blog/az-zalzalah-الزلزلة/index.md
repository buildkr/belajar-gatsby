---
title: (99) Az-Zalzalah - الزلزلة
date: 2021-10-27T04:33:01.087Z
ayat: 99
description: "Jumlah Ayat: 8 / Arti: Guncangan"
---
<!--StartFragment-->

99:1

# اِذَا زُلْزِلَتِ الْاَرْضُ زِلْزَالَهَاۙ

i*dzaa* zulzilati **a**l-ar*dh*u zilz*aa*lah*aa*

Apabila bumi diguncangkan dengan guncangan yang dahsyat,

99:2

# وَاَخْرَجَتِ الْاَرْضُ اَثْقَالَهَاۙ

wa-akhrajati **a**l-ar*dh*u atsq*aa*lah*aa*

dan bumi telah mengeluarkan beban-beban berat (yang dikandung)nya,

99:3

# وَقَالَ الْاِنْسَانُ مَا لَهَاۚ

waq*aa*la **a**l-ins*aa*nu m*aa* lah*aa*

Dan manusia bertanya, “Apa yang terjadi pada bumi ini?”

99:4

# يَوْمَىِٕذٍ تُحَدِّثُ اَخْبَارَهَاۙ

yawma-i*dz*in tu*h*additsu akhb*aa*rah*aa*

Pada hari itu bumi menyampaikan beritanya,

99:5

# بِاَنَّ رَبَّكَ اَوْحٰى لَهَاۗ

bi-anna rabbaka aw*haa* lah*aa*

karena sesungguhnya Tuhanmu telah memerintahkan (yang sedemikian itu) padanya.

99:6

# يَوْمَىِٕذٍ يَّصْدُرُ النَّاسُ اَشْتَاتًا ەۙ لِّيُرَوْا اَعْمَالَهُمْۗ

yawma-i*dz*in ya*sh*duru **al**nn*aa*su asyt*aa*tan liyuraw a'm*aa*lahum

Pada hari itu manusia keluar dari kuburnya dalam keadaan berkelompok-kelompok, untuk diperlihatkan kepada mereka (balasan) semua perbuatannya.

99:7

# فَمَنْ يَّعْمَلْ مِثْقَالَ ذَرَّةٍ خَيْرًا يَّرَهٗۚ

faman ya'mal mitsq*aa*la *dz*arratin khayran yarah**u**

Maka barangsiapa mengerjakan kebaikan seberat zarrah, niscaya dia akan melihat (balasan)nya,

99:8

# وَمَنْ يَّعْمَلْ مِثْقَالَ ذَرَّةٍ شَرًّا يَّرَهٗ ࣖ

waman ya'mal mitsq*aa*la *dz*arratin syarran yarah**u**

dan barangsiapa mengerjakan kejahatan seberat zarrah, niscaya dia akan melihat (balasan)nya.

<!--EndFragment-->