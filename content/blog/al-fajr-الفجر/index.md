---
title: (89) Al-Fajr - الفجر
date: 2021-10-27T04:26:15.591Z
ayat: 89
description: "Jumlah Ayat: 30 / Arti: Fajar"
---
<!--StartFragment-->

89:1

# وَالْفَجْرِۙ

wa**a**lfajr**i**

Demi fajar,

89:2

# وَلَيَالٍ عَشْرٍۙ

walay*aa*lin 'asyr**in**

demi malam yang sepuluh,

89:3

# وَّالشَّفْعِ وَالْوَتْرِۙ

wa**al**sysyaf'i wa**a**lwatr**i**

demi yang genap dan yang ganjil,

89:4

# وَالَّيْلِ اِذَا يَسْرِۚ

wa**a**llayli i*dzaa* yasr**i**

demi malam apabila berlalu.

89:5

# هَلْ فِيْ ذٰلِكَ قَسَمٌ لِّذِيْ حِجْرٍۗ

hal fii *dzaa*lika qasamun li*dz*ii *h*ijr**in**

Adakah pada yang demikian itu terdapat sumpah (yang dapat diterima) bagi orang-orang yang berakal?

89:6

# اَلَمْ تَرَ كَيْفَ فَعَلَ رَبُّكَ بِعَادٍۖ

alam tara kayfa fa'ala rabbuka bi'*aa*d**in**

Tidakkah engkau (Muhammad) memperhatikan bagaimana Tuhanmu berbuat terhadap (kaum) ‘Ad?

89:7

# اِرَمَ ذَاتِ الْعِمَادِۖ

irama *dzaa*ti **a**l'im*aa*d**i**

(yaitu) penduduk Iram (ibukota kaum ‘Ad) yang mempunyai bangunan-bangunan yang tinggi,

89:8

# الَّتِيْ لَمْ يُخْلَقْ مِثْلُهَا فِى الْبِلَادِۖ

**al**latii lam yukhlaq mitsluh*aa* fii **a**lbil*aa*d**i**

yang belum pernah dibangun (suatu kota) seperti itu di negeri-negeri lain,

89:9

# وَثَمُوْدَ الَّذِيْنَ جَابُوا الصَّخْرَ بِالْوَادِۖ

watsamuuda **al**la*dz*iina j*aa*buu **al***shsh*akhra bi**a**lw*aa*d**i**

dan (terhadap) kaum samud yang memotong batu-batu besar di lembah,

89:10

# وَفِرْعَوْنَ ذِى الْاَوْتَادِۖ

wafir'awna *dz*ii **a**l-awt*aa*d**i**

dan (terhadap) Fir‘aun yang mempunyai pasak-pasak (bangunan yang besar),

89:11

# الَّذِيْنَ طَغَوْا فِى الْبِلَادِۖ

**al**la*dz*iina *th*aghaw fii **a**lbil*aa*d**i**

yang berbuat sewenang-wenang dalam negeri,

89:12

# فَاَكْثَرُوْا فِيْهَا الْفَسَادَۖ

fa-aktsaruu fiih*aa* **a**lfas*aa*d**a**

lalu mereka banyak berbuat kerusakan dalam negeri itu,

89:13

# فَصَبَّ عَلَيْهِمْ رَبُّكَ سَوْطَ عَذَابٍۖ

fa*sh*abba 'alayhim rabbuka saw*th*a 'a*dzaa*b**in**

karena itu Tuhanmu menimpakan cemeti azab kepada mereka,

89:14

# اِنَّ رَبَّكَ لَبِالْمِرْصَادِۗ

inna rabbaka labi**a**lmir*shaa*d**i**

sungguh, Tuhanmu benar-benar mengawasi.

89:15

# فَاَمَّا الْاِنْسَانُ اِذَا مَا ابْتَلٰىهُ رَبُّهٗ فَاَكْرَمَهٗ وَنَعَّمَهٗۙ فَيَقُوْلُ رَبِّيْٓ اَكْرَمَنِۗ

fa-amm*aa* **a**l-ins*aa*nu i*dzaa* m*aa* ibtal*aa*hu rabbuhu fa-akramahu wana''amahu fayaquulu rabbii akraman**i**

Maka adapun manusia, apabila Tuhan mengujinya lalu memuliakannya dan memberinya kesenangan, maka dia berkata, “Tuhanku telah memuliakanku.”

89:16

# وَاَمَّآ اِذَا مَا ابْتَلٰىهُ فَقَدَرَ عَلَيْهِ رِزْقَهٗ ەۙ فَيَقُوْلُ رَبِّيْٓ اَهَانَنِۚ

wa-amm*aa* i*dzaa* m*aa* ibtal*aa*hu faqadara 'alayhi rizqahu fayaquulu rabbii ah*aa*nan**i**

Namun apabila Tuhan mengujinya lalu membatasi rezekinya, maka dia berkata, “Tuhanku telah menghinaku.”

89:17

# كَلَّا بَلْ لَّا تُكْرِمُوْنَ الْيَتِيْمَۙ

kall*aa* bal l*aa* tukrimuuna **a**lyatiim**a**

Sekali-kali tidak! Bahkan kamu tidak memuliakan anak yatim,

89:18

# وَلَا تَحٰۤضُّوْنَ عَلٰى طَعَامِ الْمِسْكِيْنِۙ

wal*aa* ta*hadd*uuna 'al*aa* *th*a'*aa*mi **a**lmiskiin**i**

dan kamu tidak saling mengajak memberi makan orang miskin,

89:19

# وَتَأْكُلُوْنَ التُّرَاثَ اَكْلًا لَّمًّاۙ

wata/kuluuna **al**ttur*aa*tsa aklan lamm*aa***n**

sedangkan kamu memakan harta warisan dengan cara mencampurbaurkan (yang halal dan yang haram),

89:20

# وَّتُحِبُّوْنَ الْمَالَ حُبًّا جَمًّاۗ

watu*h*ibbuuna **a**lm*aa*la *h*ubban jamm*aa***n**

dan kamu mencintai harta dengan kecintaan yang berlebihan.

89:21

# كَلَّآ اِذَا دُكَّتِ الْاَرْضُ دَكًّا دَكًّاۙ

kall*aa* i*dzaa* dukkati **a**l-ar*dh*u dakkan dakk*aa***n**

Sekali-kali tidak! Apabila bumi diguncangkan berturut-turut (berbenturan),

89:22

# وَّجَآءَ رَبُّكَ وَالْمَلَكُ صَفًّا صَفًّاۚ

waj*aa*-a rabbuka wa**a**lmalaku *sh*affan *sh*aff*aa***n**

dan datanglah Tuhanmu; dan malaikat berbaris-baris,

89:23

# وَجِايْۤءَ يَوْمَىِٕذٍۢ بِجَهَنَّمَۙ يَوْمَىِٕذٍ يَّتَذَكَّرُ الْاِنْسَانُ وَاَنّٰى لَهُ الذِّكْرٰىۗ

wajii-a yawma-i*dz*in bijahannama yawma-i*dz*in yata*dz*akkaru **a**l-ins*aa*nu wa-ann*aa* lahu **al***dzdz*ikr*aa*

dan pada hari itu diperlihatkan neraka Jahanam; pada hari itu sadarlah manusia, tetapi tidak berguna lagi baginya kesadaran itu.

89:24

# يَقُوْلُ يٰلَيْتَنِيْ قَدَّمْتُ لِحَيَاتِيْۚ

yaquulu y*aa* laytanii qaddamtu li*h*ay*aa*tii

Dia berkata, “Alangkah baiknya sekiranya dahulu aku mengerjakan (kebajikan) untuk hidupku ini.”

89:25

# فَيَوْمَىِٕذٍ لَّا يُعَذِّبُ عَذَابَهٗٓ اَحَدٌ ۙ

fayawma-i*dz*in l*aa* yu'a*dzdz*ibu 'a*dzaa*bahu a*h*ad**un**

Maka pada hari itu tidak ada seorang pun yang mengazab seperti azab-Nya (yang adil),

89:26

# وَّلَا يُوْثِقُ وَثَاقَهٗٓ اَحَدٌ ۗ

wal*aa* yuutsiqu wats*aa*qahu a*h*ad**un**

dan tidak ada seorang pun yang mengikat seperti ikatan-Nya.

89:27

# يٰٓاَيَّتُهَا النَّفْسُ الْمُطْمَىِٕنَّةُۙ

y*aa* ayyatuh*aa* **al**nnafsu **a**lmu*th*ma-inna**tu**

Wahai jiwa yang tenang!

89:28

# ارْجِعِيْٓ اِلٰى رَبِّكِ رَاضِيَةً مَّرْضِيَّةً ۚ

irji'ii il*aa* rabbiki r*aad*iyatan mar*dh*iyya**tan**

Kembalilah kepada Tuhanmu dengan hati yang rida dan diridai-Nya.

89:29

# فَادْخُلِيْ فِيْ عِبٰدِيْۙ

fa**u**dkhulii fii 'ib*aa*dii

Maka masuklah ke dalam golongan hamba-hamba-Ku,

89:30

# وَادْخُلِيْ جَنَّتِيْ ࣖࣖ

wa**u**dkhulii jannatii

dan masuklah ke dalam surga-Ku.

<!--EndFragment-->