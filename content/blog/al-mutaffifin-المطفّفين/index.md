---
title: (83) Al-Mutaffifin - المطفّفين
date: 2021-10-27T04:17:18.967Z
ayat: 83
description: "Jumlah Ayat: 36 / Arti: Orang-Orang Curang"
---
<!--StartFragment-->

83:1

# وَيْلٌ لِّلْمُطَفِّفِيْنَۙ

waylun lilmu*th*affifiin**a**

Celakalah bagi orang-orang yang curang (dalam menakar dan menimbang)!

83:2

# الَّذِيْنَ اِذَا اكْتَالُوْا عَلَى النَّاسِ يَسْتَوْفُوْنَۖ

**al**la*dz*iina i*dzaa* ikt*aa*luu 'al*aa* **al**nn*aa*si yastawfuun**a**

(Yaitu) orang-orang yang apabila menerima takaran dari orang lain mereka minta dicukupkan,

83:3

# وَاِذَا كَالُوْهُمْ اَوْ وَّزَنُوْهُمْ يُخْسِرُوْنَۗ

wa-i*dzaa* k*aa*luuhum aw wazanuuhum yukhsiruun**a**

dan apabila mereka menakar atau menimbang (untuk orang lain), mereka mengurangi.

83:4

# اَلَا يَظُنُّ اُولٰۤىِٕكَ اَنَّهُمْ مَّبْعُوْثُوْنَۙ

al*aa* ya*zh*unnu ul*aa*-ika annahum mab'uutsuun**a**

Tidakkah mereka itu mengira, bahwa sesungguhnya mereka akan dibangkitkan,

83:5

# لِيَوْمٍ عَظِيْمٍۙ

liyawmin 'a*zh*iim**in**

pada suatu hari yang besar,

83:6

# يَّوْمَ يَقُوْمُ النَّاسُ لِرَبِّ الْعٰلَمِيْنَۗ

yawma yaquumu **al**nn*aa*su lirabbi **a**l'*aa*lamiin**a**

(yaitu) pada hari (ketika) semua orang bangkit menghadap Tuhan seluruh alam.

83:7

# كَلَّآ اِنَّ كِتٰبَ الْفُجَّارِ لَفِيْ سِجِّيْنٍۗ

kall*aa* inna kit*aa*ba **a**lfujj*aa*ri lafii sijjiin**in**

Sekali-kali jangan begitu! Sesungguhnya catatan orang yang durhaka benar-benar tersimpan dalam Sijjin.

83:8

# وَمَآ اَدْرٰىكَ مَا سِجِّيْنٌۗ

wam*aa* adr*aa*ka m*aa* sijjiin**un**

Dan tahukah engkau apakah Sijjin itu?

83:9

# كِتٰبٌ مَّرْقُوْمٌۗ

kit*aa*bun marquum**un**

(Yaitu) Kitab yang berisi catatan (amal).

83:10

# وَيْلٌ يَّوْمَىِٕذٍ لِّلْمُكَذِّبِيْنَۙ

waylun yawma-i*dz*in lilmuka*dzdz*ibiin**a**

Celakalah pada hari itu, bagi orang-orang yang mendustakan!

83:11

# الَّذِيْنَ يُكَذِّبُوْنَ بِيَوْمِ الدِّيْنِۗ

**al**la*dz*iina yuka*dzdz*ibuuna biyawmi **al**ddiin**i**

(yaitu) orang-orang yang mendustakannya (hari pembalasan).

83:12

# وَمَا يُكَذِّبُ بِهٖٓ اِلَّا كُلُّ مُعْتَدٍ اَثِيْمٍۙ

**al**la*dz*iina yuka*dzdz*ibuuna biyawmi **al**ddiin**i**

Dan tidak ada yang mendustakannya (hari pembalasan) kecuali setiap orang yang melampaui batas dan berdosa,

83:13

# اِذَا تُتْلٰى عَلَيْهِ اٰيٰتُنَا قَالَ اَسَاطِيْرُ الْاَوَّلِيْنَۗ

i*dzaa* tutl*aa* 'alayhi *aa*y*aa*tun*aa* q*aa*la as*aath*iiru **a**l-awwaliin**a**

yang apabila dibacakan kepadanya ayat-ayat Kami, dia berkata, “Itu adalah dongeng orang-orang dahulu.”

83:14

# كَلَّا بَلْ ۜرَانَ عَلٰى قُلُوْبِهِمْ مَّا كَانُوْا يَكْسِبُوْنَ

kall*aa* bal r*aa*na 'al*aa* quluubihim m*aa* k*aa*nuu yaksibuun**a**

Sekali-kali tidak! Bahkan apa yang mereka kerjakan itu telah menutupi hati mereka.

83:15

# كَلَّآ اِنَّهُمْ عَنْ رَّبِّهِمْ يَوْمَىِٕذٍ لَّمَحْجُوْبُوْنَۗ

kall*aa* innahum 'an rabbihim yawma-i*dz*in lama*h*juubuun**a**

Sekali-kali tidak! Sesungguhnya mereka pada hari itu benar-benar terhalang dari (melihat) Tuhannya.

83:16

# ثُمَّ اِنَّهُمْ لَصَالُوا الْجَحِيْمِۗ

tsumma innahum la*shaa*luu **a**lja*h*iim**i**

Kemudian, sesungguhnya mereka benar-benar masuk neraka.

83:17

# ثُمَّ يُقَالُ هٰذَا الَّذِيْ كُنْتُمْ بِهٖ تُكَذِّبُوْنَۗ

tsumma yuq*aa*lu h*aadzaa* **al**la*dz*ii kuntum bihi tuka*dzdz*ibuun**a**

Kemudian, dikatakan (kepada mereka), “Inilah (azab) yang dahulu kamu dustakan.”

83:18

# كَلَّآ اِنَّ كِتٰبَ الْاَبْرَارِ لَفِيْ عِلِّيِّيْنَۗ

kall*aa* inna kit*aa*ba **a**l-abr*aa*ri lafii 'illiyyiin**a**

Sekali-kali tidak! Sesungguhnya catatan orang-orang yang berbakti benar-benar tersimpan dalam ’Illiyyin.

83:19

# وَمَآ اَدْرٰىكَ مَا عِلِّيُّوْنَۗ

wam*aa* adr*aa*ka m*aa* 'illiyyuun**a**

Dan tahukah engkau apakah ’Illiyyin itu?

83:20

# كِتٰبٌ مَّرْقُوْمٌۙ

kit*aa*bun marquum**un**

(Yaitu) Kitab yang berisi catatan (amal),

83:21

# يَّشْهَدُهُ الْمُقَرَّبُوْنَۗ

yasyhaduhu **a**lmuqarrabuun**a**

yang disaksikan oleh (malaikat-malaikat) yang didekatkan (kepada Allah).

83:22

# اِنَّ الْاَبْرَارَ لَفِيْ نَعِيْمٍۙ

inna **a**l-abr*aa*ra lafii na'iim**in**

Sesungguhnya orang-orang yang berbakti benar-benar berada dalam (surga yang penuh) kenikmatan,

83:23

# عَلَى الْاَرَاۤىِٕكِ يَنْظُرُوْنَۙ

'al*aa* **a**l-ar*aa*-iki yan*zh*uruun**a**

mereka (duduk) di atas dipan-dipan melepas pandangan.

83:24

# تَعْرِفُ فِيْ وُجُوْهِهِمْ نَضْرَةَ النَّعِيْمِۚ

ta'rifu fii wujuuhihim na*dh*rata **al**nna'iim**i**

Kamu dapat mengetahui dari wajah mereka kesenangan hidup yang penuh kenikmatan.

83:25

# يُسْقَوْنَ مِنْ رَّحِيْقٍ مَّخْتُوْمٍۙ

yusqawna min ra*h*iiqin makhtuum**in**

Mereka diberi minum dari khamar murni (tidak memabukkan) yang (tempatnya) masih dilak (disegel),

83:26

# خِتٰمُهٗ مِسْكٌ ۗوَفِيْ ذٰلِكَ فَلْيَتَنَافَسِ الْمُتَنَافِسُوْنَۗ

khit*aa*muhu miskun wafii *dzaa*lika falyatan*aa*fasi **a**lmutan*aa*fisuun**a**

laknya dari kasturi. Dan untuk yang demikian itu hendaknya orang berlomba-lomba.

83:27

# وَمِزَاجُهٗ مِنْ تَسْنِيْمٍۙ

wamiz*aa*juhu min tasniim**in**

Dan campurannya dari tasnim,

83:28

# عَيْنًا يَّشْرَبُ بِهَا الْمُقَرَّبُوْنَۗ

'aynan yasyrabu bih*aa* **a**lmuqarrabuun**a**

(yaitu) mata air yang diminum oleh mereka yang dekat (kepada Allah).

83:29

# اِنَّ الَّذِيْنَ اَجْرَمُوْا كَانُوْا مِنَ الَّذِيْنَ اٰمَنُوْا يَضْحَكُوْنَۖ

inna **al**la*dz*iina ajramuu k*aa*nuu mina **al**la*dz*iina *aa*manuu ya*dh*akuun**a**

Sesungguhnya orang-orang yang berdosa adalah mereka yang dahulu menertawakan orang-orang yang beriman.

83:30

# وَاِذَا مَرُّوْا بِهِمْ يَتَغَامَزُوْنَۖ

wa-i*dzaa* marruu bihim yatagh*aa*mazuun**a**

Dan apabila mereka (orang-orang yang beriman) melintas di hadapan mereka, mereka saling mengedip-ngedipkan matanya,

83:31

# وَاِذَا انْقَلَبُوْٓا اِلٰٓى اَهْلِهِمُ انْقَلَبُوْا فَكِهِيْنَۖ

wa-i*dzaa* inqalabuu il*aa* ahlihimu inqalabuu fakihiin**a**

dan apabila kembali kepada kaumnya, mereka kembali dengan gembira ria.

83:32

# وَاِذَا رَاَوْهُمْ قَالُوْٓا اِنَّ هٰٓؤُلَاۤءِ لَضَاۤلُّوْنَۙ

wa-i*dzaa* ra-awhum q*aa*luu inna h*aa*ul*aa*-i la*daa*lluun**a**

Dan apabila mereka melihat (orang-orang mukmin), mereka mengatakan, “Sesungguhnya mereka benar-benar orang-orang sesat,”

83:33

# وَمَآ اُرْسِلُوْا عَلَيْهِمْ حٰفِظِيْنَۗ

wam*aa* ursiluu 'alayhim *haa*fi*zh*iin**a**

padahal (orang-orang yang berdosa itu), mereka tidak diutus sebagai penjaga (orang-orang mukmin).

83:34

# فَالْيَوْمَ الَّذِيْنَ اٰمَنُوْا مِنَ الْكُفَّارِ يَضْحَكُوْنَۙ

fa**a**lyawma **al**la*dz*iina *aa*manuu mina **a**lkuff*aa*ri ya*dh*akuun**a**

Maka pada hari ini, orang-orang yang beriman yang menertawakan orang-orang kafir,

83:35

# عَلَى الْاَرَاۤىِٕكِ يَنْظُرُوْنَۗ

'al*aa* **a**l-ar*aa*-iki yan*zh*uruun**a**

mereka (duduk) di atas dipan-dipan melepas pandangan.

83:36

# هَلْ ثُوِّبَ الْكُفَّارُ مَا كَانُوْا يَفْعَلُوْنَ ࣖ

hal tsuwwiba **a**lkuff*aa*ru m*aa* k*aa*nuu yaf'aluun**a**

Apakah orang-orang kafir itu diberi balasan (hukuman) terhadap apa yang telah mereka perbuat?

<!--EndFragment-->