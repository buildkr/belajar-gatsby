---
title: (35) Fatir - فاطر
date: 2021-10-27T03:56:27.212Z
ayat: 35
description: "Jumlah Ayat: 45 / Arti: Maha Pencipta"
---
<!--StartFragment-->

35:1

# اَلْحَمْدُ لِلّٰهِ فَاطِرِ السَّمٰوٰتِ وَالْاَرْضِ جَاعِلِ الْمَلٰۤىِٕكَةِ رُسُلًاۙ اُولِيْٓ اَجْنِحَةٍ مَّثْنٰى وَثُلٰثَ وَرُبٰعَۗ يَزِيْدُ فِى الْخَلْقِ مَا يَشَاۤءُۗ اِنَّ اللّٰهَ عَلٰى كُلِّ شَيْءٍ قَدِيْرٌ

al*h*amdu lill*aa*hi f*aath*iri **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i j*aa*'ili **a**lmal*aa*-ikati rusulan ulii ajni*h*atin matsn*aa* watsul*aa*tsa warub*aa<*

Segala puji bagi Allah Pencipta langit dan bumi, yang menjadikan malaikat sebagai utusan-utusan (untuk mengurus berbagai macam urusan) yang mempunyai sayap, masing-masing (ada yang) dua, tiga dan empat. Allah menambahkan pada ciptaan-Nya apa yang Dia kehe

35:2

# مَا يَفْتَحِ اللّٰهُ لِلنَّاسِ مِنْ رَّحْمَةٍ فَلَا مُمْسِكَ لَهَا ۚوَمَا يُمْسِكْۙ فَلَا مُرْسِلَ لَهٗ مِنْۢ بَعْدِهٖۗ وَهُوَ الْعَزِيْزُ الْحَكِيْمُ

m*aa* yafta*h*i **al**l*aa*hu li**l**nn*aa*si min ra*h*matin fal*aa* mumsika lah*aa* wam*aa* yumsik fal*aa* mursila lahu min ba'dihi wahuwa **a**l'aziizu **a**

**Apa saja di antara rahmat Allah yang dianugerahkan kepada manusia, maka tidak ada yang dapat menahannya; dan apa saja yang ditahan-Nya maka tidak ada yang sanggup untuk melepaskannya setelah itu. Dan Dialah Yang Mahaperkasa, Mahabijaksana.**

35:3

# يٰٓاَيُّهَا النَّاسُ اذْكُرُوْا نِعْمَتَ اللّٰهِ عَلَيْكُمْۗ هَلْ مِنْ خَالِقٍ غَيْرُ اللّٰهِ يَرْزُقُكُمْ مِّنَ السَّمَاۤءِ وَالْاَرْضِۗ لَآ اِلٰهَ اِلَّا هُوَۖ فَاَنّٰى تُؤْفَكُوْنَ

y*aa* ayyuh*aa* **al**nn*aa*su u*dz*kuruu ni'mata **al**l*aa*hi 'alaykum hal min kh*aa*liqin ghayru **al**l*aa*hi yarzuqukum mina **al**ssam*aa*-i wa**a**

**Wahai manusia! Ingatlah akan nikmat Allah kepadamu. Adakah pencipta selain Allah yang dapat memberikan rezeki kepadamu dari langit dan bumi? Tidak ada tuhan selain Dia; maka mengapa kamu berpaling (dari ketauhidan)?**

35:4

# وَاِنْ يُّكَذِّبُوْكَ فَقَدْ كُذِّبَتْ رُسُلٌ مِّنْ قَبْلِكَۗ وَاِلَى اللّٰهِ تُرْجَعُ الْاُمُوْرُ

wa-in yuka*dzdz*ibuuka faqad kut*hts*ibat rusulun min qablika wa-il*aa* **al**l*aa*hi turja'u **a**l-umuur**u**

Dan jika mereka mendustakan engkau (setelah engkau beri peringatan), maka sungguh, rasul-rasul sebelum engkau telah didustakan pula. Dan hanya kepada Allah segala urusan dikembalikan.

35:5

# يٰٓاَيُّهَا النَّاسُ اِنَّ وَعْدَ اللّٰهِ حَقٌّ فَلَا تَغُرَّنَّكُمُ الْحَيٰوةُ الدُّنْيَاۗ وَلَا يَغُرَّنَّكُمْ بِاللّٰهِ الْغَرُوْرُ

y*aa* ayyuh*aa* **al**nn*aa*su inna wa'da **al**l*aa*hi *h*aqqun fal*aa* taghurrannakumu **a**l*h*ay*aa*tu **al**dduny*aa* wal*aa* yaghurrannakum bi

Wahai manusia! Sungguh, janji Allah itu benar, maka janganlah kehidupan dunia memperdayakan kamu dan janganlah (setan) yang pandai menipu, memperdayakan kamu tentang Allah.

35:6

# اِنَّ الشَّيْطٰنَ لَكُمْ عَدُوٌّ فَاتَّخِذُوْهُ عَدُوًّاۗ اِنَّمَا يَدْعُوْا حِزْبَهٗ لِيَكُوْنُوْا مِنْ اَصْحٰبِ السَّعِيْرِۗ

inna **al**sysyay*thaa*na lakum 'aduwwun fa**i**ttakhi*dz*uuhu 'aduwwan innam*aa* yad'uu *h*izbahu liyakuunuu min a*sh*-*haa*bi **al**ssa'iir**i**

Sungguh, setan itu musuh bagimu, maka perlakukanlah ia sebagai musuh, karena sesungguhnya setan itu hanya mengajak golongannya agar mereka menjadi penghuni neraka yang menyala-nyala.

35:7

# اَلَّذِيْنَ كَفَرُوْا لَهُمْ عَذَابٌ شَدِيْدٌ ەۗ وَالَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِ لَهُمْ مَّغْفِرَةٌ وَّاَجْرٌ كَبِيْرٌ ࣖ

**al**la*dz*iina kafaruu lahum 'a*dzaa*bun syadiidun wa**a**lla*dz*iina *aa*manuu wa'amiluu **al***shshaa*li*haa*ti lahum maghfiratun wa-ajrun kabiir**un**

Orang-orang yang kafir, mereka akan mendapat azab yang sangat keras. Dan orang-orang yang beriman dan mengerjakan kebajikan, mereka memperoleh ampunan dan pahala yang besar.

35:8

# اَفَمَنْ زُيِّنَ لَهٗ سُوْۤءُ عَمَلِهٖ فَرَاٰهُ حَسَنًاۗ فَاِنَّ اللّٰهَ يُضِلُّ مَنْ يَّشَاۤءُ وَيَهْدِيْ مَنْ يَّشَاۤءُۖ فَلَا تَذْهَبْ نَفْسُكَ عَلَيْهِمْ حَسَرٰتٍۗ اِنَّ اللّٰهَ عَلِيْمٌ ۢبِمَا يَصْنَعُوْنَ

afaman zuyyina lahu suu-u 'amalihi fara*aa*hu *h*asanan fa-inna **al**l*aa*ha yu*dh*illu man yasy*aa*u wayahdii man yasy*aa*u fal*aa* ta*dz*hab nafsuka 'alayhim *h*asar*aa*tin inna **al<**

Maka apakah pantas orang yang dijadikan terasa indah perbuatan buruknya, lalu menganggap baik perbuatannya itu? Sesungguhnya Allah menyesatkan siapa yang Dia kehendaki dan memberi petunjuk kepada siapa yang Dia kehendaki. Maka jangan engkau (Muhammad) bia

35:9

# وَاللّٰهُ الَّذِيْٓ اَرْسَلَ الرِّيٰحَ فَتُثِيْرُ سَحَابًا فَسُقْنٰهُ اِلٰى بَلَدٍ مَّيِّتٍ فَاَحْيَيْنَا بِهِ الْاَرْضَ بَعْدَ مَوْتِهَاۗ كَذٰلِكَ النُّشُوْرُ

wa**al**l*aa*hu **al**la*dz*ii arsala **al**rriy*aah*a fatutsiiru sa*haa*ban fasuqn*aa*hu il*aa* baladin mayyitin fa-a*h*yayn*aa* bihi **a**l-ar*dh*a ba'da ma

Dan Allah-lah yang mengirimkan angin; lalu (angin itu) menggerakkan awan, maka Kami arahkan awan itu ke suatu negeri yang mati (tandus) lalu dengan hujan itu Kami hidupkan bumi setelah mati (kering). Seperti itulah kebangkitan itu.

35:10

# مَنْ كَانَ يُرِيْدُ الْعِزَّةَ فَلِلّٰهِ الْعِزَّةُ جَمِيْعًاۗ اِلَيْهِ يَصْعَدُ الْكَلِمُ الطَّيِّبُ وَالْعَمَلُ الصَّالِحُ يَرْفَعُهٗ ۗوَالَّذِيْنَ يَمْكُرُوْنَ السَّيِّاٰتِ لَهُمْ عَذَابٌ شَدِيْدٌ ۗوَمَكْرُ اُولٰۤىِٕكَ هُوَ يَبُوْرُ

man k*aa*na yuriidu **a**l'izzata falill*aa*hi **a**l'izzatu jamii'an ilayhi ya*sh*'adu **a**lkalimu **al***ththh*ayyibu wa**a**l'amalu **al***shshaa*li

Barangsiapa menghendaki kemuliaan, maka (ketahuilah) kemuliaan itu semuanya milik Allah. Kepada-Nyalah akan naik perkataan-perkataan yang baik, dan amal kebajikan Dia akan mengangkatnya. Adapun orang-orang yang merencanakan kejahatan mereka akan mendapat

35:11

# وَاللّٰهُ خَلَقَكُمْ مِّنْ تُرَابٍ ثُمَّ مِنْ نُّطْفَةٍ ثُمَّ جَعَلَكُمْ اَزْوَاجًاۗ وَمَا تَحْمِلُ مِنْ اُنْثٰى وَلَا تَضَعُ اِلَّا بِعِلْمِهٖۗ وَمَا يُعَمَّرُ مِنْ مُّعَمَّرٍ وَّلَا يُنْقَصُ مِنْ عُمُرِهٖٓ اِلَّا فِيْ كِتٰبٍۗ اِنَّ ذٰلِكَ عَلَى اللّٰهِ

wa**al**l*aa*hu khalaqakum min tur*aa*bin tsumma min nu*th*fatin tsumma ja'alakum azw*aa*jan wam*aa* ta*h*milu min unts*aa* wal*aa* ta*dh*a'u ill*aa* bi'ilmihi wam*aa* yu'ammaru min mu'am

Dan Allah menciptakan kamu dari tanah kemudian dari air mani, kemudian Dia menjadikan kamu berpasangan (laki-laki dan perempuan). Tidak ada seorang perempuan pun yang mengandung dan melahirkan, melainkan dengan sepengetahuan-Nya. Dan tidak dipanjangkan um

35:12

# وَمَا يَسْتَوِى الْبَحْرٰنِۖ هٰذَا عَذْبٌ فُرَاتٌ سَاۤىِٕغٌ شَرَابُهٗ وَهٰذَا مِلْحٌ اُجَاجٌۗ وَمِنْ كُلٍّ تَأْكُلُوْنَ لَحْمًا طَرِيًّا وَّتَسْتَخْرِجُوْنَ حِلْيَةً تَلْبَسُوْنَهَا ۚوَتَرَى الْفُلْكَ فِيْهِ مَوَاخِرَ لِتَبْتَغُوْا مِنْ فَضْلِهٖ وَلَعَلَّ

wam*aa* yastawii **a**lba*h*r*aa*ni h*aadzaa* 'a*dz*bun fur*aa*tun s*aa*-ighun syar*aa*buhu wah*aadzaa* mil*h*un uj*aa*jun wamin kullin ta/kuluuna la*h*man *th*ariyyan watastakh

Dan tidak sama (antara) dua lautan; yang ini tawar, segar, sedap diminum dan yang lain asin lagi pahit. Dan dari (masing-masing lautan) itu kamu dapat memakan daging yang segar dan kamu dapat mengeluarkan perhiasan yang kamu pakai, dan di sana kamu meliha

35:13

# يُوْلِجُ الَّيْلَ فِى النَّهَارِ وَيُوْلِجُ النَّهَارَ فِى الَّيْلِۚ وَسَخَّرَ الشَّمْسَ وَالْقَمَرَ كُلٌّ يَّجْرِيْ لِاَجَلٍ مُّسَمًّىۗ ذٰلِكُمُ اللّٰهُ رَبُّكُمْ لَهُ الْمُلْكُۗ وَالَّذِيْنَ تَدْعُوْنَ مِنْ دُوْنِهٖ مَا يَمْلِكُوْنَ مِنْ قِطْمِيْرٍۗ

yuuliju **al**layla fii **al**nnah*aa*ri wayuuliju **al**nnah*aa*ra fii **al**layli wasakhkhara **al**sysyamsa wa**a**lqamara kullun yajrii li-ajalin musamman *dzaa<*

Dia memasukkan malam ke dalam siang dan memasukkan siang ke dalam malam dan menundukkan matahari dan bulan, masing-masing beredar menurut waktu yang ditentukan. Yang (berbuat) demikian itulah Allah Tuhanmu, milik-Nyalah segala kerajaan. Dan orang-orang ya

35:14

# اِنْ تَدْعُوْهُمْ لَا يَسْمَعُوْا دُعَاۤءَكُمْۚ وَلَوْ سَمِعُوْا مَا اسْتَجَابُوْا لَكُمْۗ وَيَوْمَ الْقِيٰمَةِ يَكْفُرُوْنَ بِشِرْكِكُمْۗ وَلَا يُنَبِّئُكَ مِثْلُ خَبِيْرٍ ࣖ

in tad'uuhum l*aa* yasma'uu du'*aa*-akum walaw sami'uu m*aa* istaj*aa*buu lakum wayawma **a**lqiy*aa*mati yakfuruuna bisyirkikum wal*aa* yunabbi-uka mitslu khabiir**in**

Jika kamu menyeru mereka, mereka tidak mendengar seruanmu, dan sekiranya mereka mendengar, mereka juga tidak memperkenankan permintaanmu. Dan pada hari Kiamat mereka akan mengingkari kemusyrikanmu dan tidak ada yang dapat memberikan keterangan kepadamu se

35:15

# ۞ يٰٓاَيُّهَا النَّاسُ اَنْتُمُ الْفُقَرَاۤءُ اِلَى اللّٰهِ ۚوَاللّٰهُ هُوَ الْغَنِيُّ الْحَمِيْدُ

y*aa* ayyuh*aa* **al**nn*aa*su antumu **a**lfuqar*aa*u il*aa* **al**l*aa*hi wa**al**l*aa*hu huwa **a**lghaniyyu **a**l*h*amiid**u**

**Wahai manusia! Kamulah yang memerlukan Allah; dan Allah Dialah Yang Mahakaya (tidak memerlukan sesuatu), Maha Terpuji.**

35:16

# اِنْ يَّشَأْ يُذْهِبْكُمْ وَيَأْتِ بِخَلْقٍ جَدِيْدٍۚ

in yasya/ yu*dz*hibkum waya/ti bikhalqin jadiid**in**

Jika Dia menghendaki, niscaya Dia membinasakan kamu dan mendatangkan makhluk yang baru (untuk menggantikan kamu).

35:17

# وَمَا ذٰلِكَ عَلَى اللّٰهِ بِعَزِيْزٍ

wam*aa* *dzaa*lika 'al*aa* **al**l*aa*hi bi'aziiz**in**

Dan yang demikian itu tidak sulit bagi Allah.

35:18

# وَلَا تَزِرُ وَازِرَةٌ وِّزْرَ اُخْرٰى ۗوَاِنْ تَدْعُ مُثْقَلَةٌ اِلٰى حِمْلِهَا لَا يُحْمَلْ مِنْهُ شَيْءٌ وَّلَوْ كَانَ ذَا قُرْبٰىۗ اِنَّمَا تُنْذِرُ الَّذِيْنَ يَخْشَوْنَ رَبَّهُمْ بِالْغَيْبِ وَاَقَامُوا الصَّلٰوةَ ۗوَمَنْ تَزَكّٰى فَاِنَّمَا يَتَزَك

wal*aa* taziru w*aa*ziratun wizra ukhr*aa* wa-in tad'u mutsqalatun il*aa* *h*imlih*aa* l*aa* yu*h*mal minhu syay-un walaw k*aa*na *dzaa* qurb*aa* innam*aa* tun*dz*iru **al**la

Dan orang yang berdosa tidak akan memikul dosa orang lain. Dan jika seseorang yang dibebani berat dosanya memanggil (orang lain) untuk memikul bebannya itu tidak akan dipikulkan sedikit pun, meskipun (yang dipanggilnya itu) kaum kerabatnya. Sesungguhnya y

35:19

# وَمَا يَسْتَوِى الْاَعْمٰى وَالْبَصِيْرُ ۙ

wam*aa* yastawii **a**l-a'm*aa* wa**a**lba*sh*iir**u**

Dan tidaklah sama orang yang buta dengan orang yang melihat,

35:20

# وَلَا الظُّلُمٰتُ وَلَا النُّوْرُۙ

wal*aa* **al***zhzh*ulum*aa*tu wal*aa* **al**nnuur**u**

dan tidak (pula) sama gelap gulita dengan cahaya,

35:21

# وَلَا الظِّلُّ وَلَا الْحَرُوْرُۚ

wal*aa* **al***zhzh*illu wal*aa* **a**l*h*aruur**u**

dan tidak (pula) sama yang teduh dengan yang panas,

35:22

# وَمَا يَسْتَوِى الْاَحْيَاۤءُ وَلَا الْاَمْوَاتُۗ اِنَّ اللّٰهَ يُسْمِعُ مَنْ يَّشَاۤءُ ۚوَمَآ اَنْتَ بِمُسْمِعٍ مَّنْ فِى الْقُبُوْرِ

wam*aa* yastawii **a**l-a*h*y*aa*u wal*aa* **a**l-amw*aa*tu inna **al**l*aa*ha yusmi'u man yasy*aa*u wam*aa* anta bimusmi'in man fii **a**lqubuur**i**

dan tidak (pula) sama orang yang hi-dup dengan orang yang mati. Sungguh, Allah memberikan pendengaran kepada siapa yang Dia kehendaki dan engkau (Muhammad) tidak akan sanggup menjadikan orang yang di dalam kubur dapat mendengar.

35:23

# اِنْ اَنْتَ اِلَّا نَذِيْرٌ

in anta ill*aa* na*dz*iir**un**

Engkau tidak lain hanyalah seorang pemberi peringatan.

35:24

# اِنَّآ اَرْسَلْنٰكَ بِالْحَقِّ بَشِيْرًا وَّنَذِيْرًا ۗوَاِنْ مِّنْ اُمَّةٍ اِلَّا خَلَا فِيْهَا نَذِيْرٌ

inn*aa* arsaln*aa*ka bi**a**l*h*aqqi basyiiran wana*dz*iiran wa-in min ummatin ill*aa* khal*aa* fiih*aa* na*dz*iir**un**

Sungguh, Kami mengutus engkau dengan membawa kebenaran sebagai pembawa berita gembira dan sebagai pemberi peringatan. Dan tidak ada satu pun umat melainkan di sana telah datang seorang pemberi peringatan.

35:25

# وَاِنْ يُّكَذِّبُوْكَ فَقَدْ كَذَّبَ الَّذِيْنَ مِنْ قَبْلِهِمْ ۚجَاۤءَتْهُمْ رُسُلُهُمْ بِالْبَيِّنٰتِ وَبِالزُّبُرِ وَبِالْكِتٰبِ الْمُنِيْرِ

wa-in yuka*dzdz*ibuuka faqad ka*dzdz*aba **al**la*dz*iina min qablihim j*aa*-at-hum rusuluhum bi**a**lbayyin*aa*ti wabi**al**zzuburi wabi**a**lkit*aa*bi **a**lm

Dan jika mereka mendustakanmu, maka sungguh, orang-orang yang sebelum mereka pun telah mendustakan (rasul-rasul); ketika rasul-rasulnya datang dengan membawa keterangan yang nyata (mukjizat), zubur, dan kitab yang memberi penjelasan yang sempurna.

35:26

# ثُمَّ اَخَذْتُ الَّذِيْنَ كَفَرُوْا فَكَيْفَ كَانَ نَكِيْرِ ࣖ

tsumma akha*dz*tu **al**la*dz*iina kafaruu fakayfa k*aa*na nakiir**i**

Kemudian Aku azab orang-orang yang kafir; maka (lihatlah) bagaimana akibat kemurkaan-Ku.

35:27

# اَلَمْ تَرَ اَنَّ اللّٰهَ اَنْزَلَ مِنَ السَّمَاۤءِ مَاۤءًۚ فَاَخْرَجْنَا بِهٖ ثَمَرٰتٍ مُّخْتَلِفًا اَلْوَانُهَا ۗوَمِنَ الْجِبَالِ جُدَدٌ ۢبِيْضٌ وَّحُمْرٌ مُّخْتَلِفٌ اَلْوَانُهَا وَغَرَابِيْبُ سُوْدٌ

alam tara anna **al**l*aa*ha anzala mina **al**ssam*aa*-i m*aa*-an fa-akhrajn*aa* bihi tsamar*aa*tin mukhtalifan **a**lw*aa*nuh*aa* wamina **a**ljib*aa*li judadun

Tidakkah engkau melihat bahwa Allah menurunkan air dari langit lalu dengan air itu Kami hasilkan buah-buahan yang beraneka macam jenisnya. Dan di antara gunung-gunung itu ada garis-garis putih dan merah yang beraneka macam warnanya dan ada (pula) yang hit

35:28

# وَمِنَ النَّاسِ وَالدَّوَاۤبِّ وَالْاَنْعَامِ مُخْتَلِفٌ اَلْوَانُهٗ كَذٰلِكَۗ اِنَّمَا يَخْشَى اللّٰهَ مِنْ عِبَادِهِ الْعُلَمٰۤؤُاۗ اِنَّ اللّٰهَ عَزِيْزٌ غَفُوْرٌ

wamina **al**nn*aa*si wa**al**ddaw*aa*bbi wa**a**l-an'*aa*mi mukhtalifun **a**lw*aa*nuhu ka*dzaa*lika innam*aa* yakhsy*aa* **al**l*aa*ha min 'ib*aa<*

Dan demikian (pula) di antara manusia, makhluk bergerak yang bernyawa dan hewan-hewan ternak ada yang bermacam-macam warnanya (dan jenisnya). Di antara hamba-hamba Allah yang takut kepada-Nya, hanyalah para ulama. Sungguh, Allah Mahaperkasa, Maha Pengampu

35:29

# اِنَّ الَّذِيْنَ يَتْلُوْنَ كِتٰبَ اللّٰهِ وَاَقَامُوا الصَّلٰوةَ وَاَنْفَقُوْا مِمَّا رَزَقْنٰهُمْ سِرًّا وَّعَلَانِيَةً يَّرْجُوْنَ تِجَارَةً لَّنْ تَبُوْرَۙ

inna **al**la*dz*iina yatluuna kit*aa*ba **al**l*aa*hi wa-aq*aa*muu **al***shsh*al*aa*ta wa-anfaquu mimm*aa *razaqn*aa*hum sirran wa'al*aa*niyatan yarjuuna tij*aa*rat

Sesungguhnya orang-orang yang selalu membaca Kitab Allah (Al-Qur'an) dan melaksanakan salat dan menginfakkan sebagian rezeki yang Kami anugerahkan kepadanya dengan diam-diam dan terang-terangan, mereka itu mengharapkan perdagangan yang tidak akan rugi,

35:30

# لِيُوَفِّيَهُمْ اُجُوْرَهُمْ وَيَزِيْدَهُمْ مِّنْ فَضْلِهٖۗ اِنَّهٗ غَفُوْرٌ شَكُوْرٌ

liyuwaffiyahum ujuurahum wayaziidahum min fa*dh*lihi innahu ghafuurun syakuur**un**

agar Allah menyempurnakan pahalanya kepada mereka dan menambah karunia-Nya. Sungguh, Allah Maha Pengampun, Maha Mensyukuri.

35:31

# وَالَّذِيْٓ اَوْحَيْنَآ اِلَيْكَ مِنَ الْكِتٰبِ هُوَ الْحَقُّ مُصَدِّقًا لِّمَا بَيْنَ يَدَيْهِۗ اِنَّ اللّٰهَ بِعِبَادِهٖ لَخَبِيْرٌۢ بَصِيْرٌ

wa**a**lla*dz*ii aw*h*ayn*aa* ilayka mina **a**lkit*aa*bi huwa **a**l*h*aqqu mu*sh*addiqan lim*aa* bayna yadayhi inna **al**l*aa*ha bi'ib*aa*dihi lakhabiirun

Dan apa yang telah Kami wahyukan kepadamu (Muhammad) yaitu Kitab (Al-Qur'an) itulah yang benar, membenarkan kitab-kitab yang sebelumnya. Sungguh, Allah benar-benar Maha Mengetahui, Maha Melihat (keadaan) hamba-hamba-Nya.

35:32

# ثُمَّ اَوْرَثْنَا الْكِتٰبَ الَّذِيْنَ اصْطَفَيْنَا مِنْ عِبَادِنَاۚ فَمِنْهُمْ ظَالِمٌ لِّنَفْسِهٖ ۚوَمِنْهُمْ مُّقْتَصِدٌ ۚوَمِنْهُمْ سَابِقٌۢ بِالْخَيْرٰتِ بِاِذْنِ اللّٰهِ ۗذٰلِكَ هُوَ الْفَضْلُ الْكَبِيْرُۗ

tsumma awratsn*aa* **a**lkit*aa*ba **al**la*dz*iina i*sth*afayn*aa* min 'ib*aa*din*aa* faminhum *zhaa*limun linafsihi waminhum muqta*sh*idun waminhum s*aa*biqun bi**a**

**Kemudian Kitab itu Kami wariskan kepada orang-orang yang Kami pilih di antara hamba-hamba Kami, lalu di antara mereka ada yang menzalimi diri sendiri, ada yang pertengahan dan ada (pula) yang lebih dahulu berbuat kebaikan dengan izin Allah. Yang demikian**

35:33

# جَنّٰتُ عَدْنٍ يَّدْخُلُوْنَهَا يُحَلَّوْنَ فِيْهَا مِنْ اَسَاوِرَ مِنْ ذَهَبٍ وَّلُؤْلُؤًا ۚوَلِبَاسُهُمْ فِيْهَا حَرِيْرٌ

jann*aa*tu 'adnin yadkhuluunah*aa* yu*h*allawna fiih*aa* min as*aa*wira min *dz*ahabin walu/lu-an walib*aa*suhum fiih*aa* *h*ariir**un**

(Mereka akan mendapat) surga ‘Adn, mereka masuk ke dalamnya, di dalamnya mereka diberi perhiasan gelang-gelang dari emas dan mutiara, dan pakaian mereka di dalamnya adalah sutera.

35:34

# وَقَالُوا الْحَمْدُ لِلّٰهِ الَّذِيْٓ اَذْهَبَ عَنَّا الْحَزَنَۗ اِنَّ رَبَّنَا لَغَفُوْرٌ شَكُوْرٌۙ

waq*aa*luu **a**l*h*amdu lill*aa*hi **al**la*dz*ii a*dz*haba 'ann*aa* **a**l*h*azana inna rabban*aa* laghafuurun syakuur**un**

Dan mereka berkata, “Segala puji bagi Allah yang telah menghilangkan kesedihan dari kami. Sungguh, Tuhan kami benar-benar Maha Pengampun, Maha Mensyukuri,

35:35

# ۨالَّذِيْٓ اَحَلَّنَا دَارَ الْمُقَامَةِ مِنْ فَضْلِهٖۚ لَا يَمَسُّنَا فِيْهَا نَصَبٌ وَّلَا يَمَسُّنَا فِيْهَا لُغُوْبٌ

**al**la*dz*ii a*h*allan*aa* d*aa*ra **a**lmuq*aa*mati min fa*dh*lihi l*aa* yamassun*aa* fiih*aa* na*sh*abun wal*aa* yamassun*aa* fiih*aa* lughuub**un**

**yang dengan karunia-Nya menempatkan kami dalam tempat yang kekal (surga); di dalamnya kami tidak merasa lelah dan tidak pula merasa lesu.”**

35:36

# وَالَّذِيْنَ كَفَرُوْا لَهُمْ نَارُ جَهَنَّمَۚ لَا يُقْضٰى عَلَيْهِمْ فَيَمُوْتُوْا وَلَا يُخَفَّفُ عَنْهُمْ مِّنْ عَذَابِهَاۗ كَذٰلِكَ نَجْزِيْ كُلَّ كَفُوْرٍ ۚ

wa**a**lla*dz*iina kafaruu lahum n*aa*ru jahannama l*aa* yuq*daa* 'alayhim fayamuutuu wal*aa* yukhaffafu 'anhum min 'a*dzaa*bih*aa* ka*dzaa*lika najzii kulla kafuur**in**

Dan orang-orang yang kafir, bagi mereka neraka Jahanam. Mereka tidak dibinasakan hingga mereka mati, dan tidak diringankan dari mereka azabnya. Demikianlah Kami membalas setiap orang yang sangat kafir.

35:37

# وَهُمْ يَصْطَرِخُوْنَ فِيْهَاۚ رَبَّنَآ اَخْرِجْنَا نَعْمَلْ صَالِحًا غَيْرَ الَّذِيْ كُنَّا نَعْمَلُۗ اَوَلَمْ نُعَمِّرْكُمْ مَّا يَتَذَكَّرُ فِيْهِ مَنْ تَذَكَّرَ وَجَاۤءَكُمُ النَّذِيْرُۗ فَذُوْقُوْا فَمَا لِلظّٰلِمِيْنَ مِنْ نَّصِيْرٍ

wahum ya*sth*arikhuuna fiih*aa* rabban*aa* akhrijn*aa* na'mal *shaa*li*h*an ghayra **al**la*dz*ii kunn*aa* na'malu awa lam nu'ammirkum m*aa* yata*dz*akkaru fiihi man ta*dz*akkara waj*aa*

Dan mereka berteriak di dalam neraka itu, “Ya Tuhan kami, keluarkanlah kami (dari neraka), niscaya kami akan mengerjakan kebajikan, yang berlainan dengan yang telah kami kerjakan dahulu.” (Dikatakan kepada mereka), “Bukankah Kami telah memanjangkan umurmu

35:38

# اِنَّ اللّٰهَ عَالِمُ غَيْبِ السَّمٰوٰتِ وَالْاَرْضِۗ اِنَّهٗ عَلِيْمٌ ۢبِذَاتِ الصُّدُوْرِ

inna **al**l*aa*ha '*aa*limu ghaybi **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i innahu 'aliimun bi*dzaa*ti **al***shsh*uduur**i**

Sungguh, Allah mengetahui yang gaib (tersembunyi) di langit dan di bumi. Sungguh, Dia Maha Mengetahui segala isi hati.

35:39

# هُوَ الَّذِيْ جَعَلَكُمْ خَلٰۤىِٕفَ فِى الْاَرْضِۗ فَمَنْ كَفَرَ فَعَلَيْهِ كُفْرُهٗۗ وَلَا يَزِيْدُ الْكٰفِرِيْنَ كُفْرُهُمْ عِنْدَ رَبِّهِمْ اِلَّا مَقْتًا ۚوَلَا يَزِيْدُ الْكٰفِرِيْنَ كُفْرُهُمْ اِلَّا خَسَارًا

huwa **al**la*dz*ii ja'alakum khal*aa*-ifa fii **a**l-ar*dh*i faman kafara fa'alayhi kufruhu wal*aa* yaziidu **a**lk*aa*firiina kufruhum 'inda rabbihim ill*aa* maqtan wal*aa* yaziid

Dialah yang menjadikan kamu sebagai khalifah-khalifah di bumi. Barangsiapa kafir, maka (akibat) kekafirannya akan menimpa dirinya sendiri. Dan kekafiran orang-orang kafir itu hanya akan menambah kemurkaan di sisi Tuhan mereka. Dan kekafiran orang-orang ka

35:40

# قُلْ اَرَاَيْتُمْ شُرَكَاۤءَكُمُ الَّذِيْنَ تَدْعُوْنَ مِنْ دُوْنِ اللّٰهِ ۗاَرُوْنِيْ مَاذَا خَلَقُوْا مِنَ الْاَرْضِ اَمْ لَهُمْ شِرْكٌ فِى السَّمٰوٰتِۚ اَمْ اٰتَيْنٰهُمْ كِتٰبًا فَهُمْ عَلٰى بَيِّنَتٍ مِّنْهُۚ بَلْ اِنْ يَّعِدُ الظّٰلِمُوْنَ بَعْضُهُمْ

qul ara-aytum syurak*aa*-akumu **al**la*dz*iina tad'uuna min duuni **al**l*aa*hi aruunii m*aatsaa* khalaquu mina **a**l-ar*dh*i am lahum syirkun fii **al**ssam*aa*w*aa*

Katakanlah, “Terangkanlah olehmu tentang sekutu-sekutumu yang kamu seru selain Allah.” Perlihatkanlah kepada-Ku (bagian) manakah dari bumi ini yang telah mereka ciptakan; ataukah mereka mempunyai peran serta dalam (penciptaan) langit; atau adakah Kami mem

35:41

# ۞ اِنَّ اللّٰهَ يُمْسِكُ السَّمٰوٰتِ وَالْاَرْضَ اَنْ تَزُوْلَا ەۚ وَلَىِٕنْ زَالَتَآ اِنْ اَمْسَكَهُمَا مِنْ اَحَدٍ مِّنْۢ بَعْدِهٖ ۗاِنَّهٗ كَانَ حَلِيْمًا غَفُوْرًا

inna **al**l*aa*ha yumsiku **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*a an tazuul*aa* wala-in z*aa*lat*aa* in amsakahum*aa* min a*h*adin min ba'dihi innahu k*aa*na *h*

Sungguh, Allah yang menahan langit dan bumi agar tidak lenyap; dan jika keduanya akan lenyap tidak ada seorang pun yang mampu menahannya selain Allah. Sungguh, Dia Maha Penyantun, Maha Pengampun.

35:42

# وَاَقْسَمُوْا بِاللّٰهِ جَهْدَ اَيْمَانِهِمْ لَىِٕنْ جَاۤءَهُمْ نَذِيْرٌ لَّيَكُوْنُنَّ اَهْدٰى مِنْ اِحْدَى الْاُمَمِۚ فَلَمَّا جَاۤءَهُمْ نَذِيْرٌ مَّا زَادَهُمْ اِلَّا نُفُوْرًاۙ

wa-aqsamuu bi**al**l*aa*hi jahda aym*aa*nihim la-in j*aa*-ahum na*dz*iirun layakuununna ahd*aa* min i*h*d*aa* **a**l-umami falamm*aa* j*aa*-ahum na*dz*iirun m*aa* z*aa*d

Dan mereka bersumpah dengan nama Allah dengan sungguh-sungguh bahwa jika datang kepada mereka seorang pemberi peringatan, niscaya mereka akan lebih mendapat petunjuk dari salah satu umat-umat (yang lain). Tetapi ketika pemberi peringatan datang kepada mer

35:43

# ۨاسْتِكْبَارًا فِى الْاَرْضِ وَمَكْرَ السَّيِّئِۗ وَلَا يَحِيْقُ الْمَكْرُ السَّيِّئُ اِلَّا بِاَهْلِهٖ ۗفَهَلْ يَنْظُرُوْنَ اِلَّا سُنَّتَ الْاَوَّلِيْنَۚ فَلَنْ تَجِدَ لِسُنَّتِ اللّٰهِ تَبْدِيْلًا ەۚ وَلَنْ تَجِدَ لِسُنَّتِ اللّٰهِ تَحْوِيْلًا

istikb*aa*ran fii **a**l-ar*dh*i wamakra **al**ssayyi-i wal*aa* ya*h*iiqu **a**lmakru **al**ssayyi-u ill*aa* bi-ahlihi fahal yan*zh*uruuna ill*aa* sunnata **a**

**karena kesombongan (mereka) di bumi dan karena rencana (mereka) yang jahat. Rencana yang jahat itu hanya akan menimpa orang yang merencanakannya sendiri. Mereka hanyalah menunggu (berlakunya) ketentuan kepada orang-orang yang terdahulu. Maka kamu tidak ak**

35:44

# اَوَلَمْ يَسِيْرُوْا فِى الْاَرْضِ فَيَنْظُرُوْا كَيْفَ كَانَ عَاقِبَةُ الَّذِيْنَ مِنْ قَبْلِهِمْ وَكَانُوْٓا اَشَدَّ مِنْهُمْ قُوَّةً ۗوَمَا كَانَ اللّٰهُ لِيُعْجِزَهٗ مِنْ شَيْءٍ فِى السَّمٰوٰتِ وَلَا فِى الْاَرْضِۗ اِنَّهٗ كَانَ عَلِيْمًا قَدِيْرًا

awa lam yasiiruu fii **a**l-ar*dh*i fayan*zh*uruu kayfa k*aa*na '*aa*qibatu **al**la*dz*iina min qablihim wak*aa*nuu asyadda minhum quwwatan wam*aa* k*aa*na **al**l*aa*hu

Dan tidakkah mereka bepergian di bumi lalu melihat bagaimana kesudahan orang-orang sebelum mereka (yang mendustakan rasul), padahal orang-orang itu lebih besar kekuatannya dari mereka? Dan tidak ada sesuatu pun yang dapat melemahkan Allah baik di langit m

35:45

# وَلَوْ يُؤَاخِذُ اللّٰهُ النَّاسَ بِمَا كَسَبُوْا مَا تَرَكَ عَلٰى ظَهْرِهَا مِنْ دَاۤبَّةٍ وَّلٰكِنْ يُّؤَخِّرُهُمْ اِلٰٓى اَجَلٍ مُّسَمًّىۚ فَاِذَا جَاۤءَ اَجَلُهُمْ فَاِنَّ اللّٰهَ كَانَ بِعِبَادِهٖ بَصِيْرًا ࣖ

walaw yu-*aa*khi*dz*u **al**l*aa*hu **al**nn*aa*sa bim*aa* kasabuu m*aa* taraka 'al*aa* *zh*ahrih*aa* min d*aa*bbatin wal*aa*kin yu-akhkhiruhum il*aa* ajalin musamman fa

Dan kalau sekiranya Allah menyiksa manusia disebabkan usahanya, niscaya Dia tidak akan meninggalkan di atas permukaan bumi suatu mahluk yang melatapun akan tetapi Allah menangguhkan (penyiksaan) mereka, sampai waktu yang tertentu; maka apabila datang ajal mereka, maka sesungguhnya Allah adalah Maha Melihat (keadaan) hamba-hamba-Nya.

<!--EndFragment-->