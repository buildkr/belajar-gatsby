---
title: (97) Al-Qadr - القدر
date: 2021-10-27T04:32:19.222Z
ayat: 97
description: "Jumlah Ayat: 5 / Arti: Kemuliaan"
---
<!--StartFragment-->

97:1

# اِنَّآ اَنْزَلْنٰهُ فِيْ لَيْلَةِ الْقَدْرِ

inn*aa* anzaln*aa*hu fii laylati **a**lqadr**i**

Sesungguhnya Kami telah menurunkannya (Al-Qur'an) pada malam qadar.

97:2

# وَمَآ اَدْرٰىكَ مَا لَيْلَةُ الْقَدْرِۗ

wam*aa* adr*aa*ka m*aa* laylatu **a**lqadr**i**

Dan tahukah kamu apakah malam kemuliaan itu?

97:3

# لَيْلَةُ الْقَدْرِ ەۙ خَيْرٌ مِّنْ اَلْفِ شَهْرٍۗ

laylatu **a**lqadri khayrun min **a**lfi syahr**in**

Malam kemuliaan itu lebih baik daripada seribu bulan.

97:4

# تَنَزَّلُ الْمَلٰۤىِٕكَةُ وَالرُّوْحُ فِيْهَا بِاِذْنِ رَبِّهِمْۚ مِنْ كُلِّ اَمْرٍۛ

tanazzalu **a**lmal*aa*-ikatu wa**al**rruu*h*u fiih*aa* bi-i*dz*ni rabbihim min kulli amr**in**

Pada malam itu turun para malaikat dan Ruh (Jibril) dengan izin Tuhannya untuk mengatur semua urusan.

97:5

# سَلٰمٌ ۛهِيَ حَتّٰى مَطْلَعِ الْفَجْرِ ࣖ

sal*aa*mun hiya *h*att*aa* ma*th*la'i **a**lfajr**i**

Sejahteralah (malam itu) sampai terbit fajar.

<!--EndFragment-->