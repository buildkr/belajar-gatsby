---
title: (93) Ad-Duha - الضحى
date: 2021-10-27T04:29:51.956Z
ayat: 93
description: "Jumlah Ayat: 11 / Arti: Duha"
---
<!--StartFragment-->

93:1

# وَالضُّحٰىۙ

wa**al***dhdh*u*haa*

Demi waktu duha (ketika matahari naik sepenggalah),

93:2

# وَالَّيْلِ اِذَا سَجٰىۙ

wa**a**llayli i*dzaa* saj*aa*

dan demi malam apabila telah sunyi,

93:3

# مَا وَدَّعَكَ رَبُّكَ وَمَا قَلٰىۗ

m*aa* wadda'aka rabbuka wam*aa* qal*aa*

Tuhanmu tidak meninggalkan engkau (Muhammad) dan tidak (pula) membencimu,

93:4

# وَلَلْاٰخِرَةُ خَيْرٌ لَّكَ مِنَ الْاُوْلٰىۗ

walal-*aa*khiratu khayrun laka mina **a**l-uul*aa*

dan sungguh, yang kemudian itu lebih baik bagimu dari yang permulaan.

93:5

# وَلَسَوْفَ يُعْطِيْكَ رَبُّكَ فَتَرْضٰىۗ

walasawfa yu'*th*iika rabbuka fatar*daa*

Dan sungguh, kelak Tuhanmu pasti memberikan karunia-Nya kepadamu, sehingga engkau menjadi puas.

93:6

# اَلَمْ يَجِدْكَ يَتِيْمًا فَاٰوٰىۖ

alam yajidka yatiiman fa*aa*w*aa*

Bukankah Dia mendapatimu sebagai seorang yatim, lalu Dia melindungi(mu),

93:7

# وَوَجَدَكَ ضَاۤلًّا فَهَدٰىۖ

wawajadaka *daa*llan fahad*aa*

dan Dia mendapatimu sebagai seorang yang bingung, lalu Dia memberikan petunjuk,

93:8

# وَوَجَدَكَ عَاۤىِٕلًا فَاَغْنٰىۗ

wawajadaka '*aa*-ilan fa-aghn*aa*

dan Dia mendapatimu sebagai seorang yang kekurangan, lalu Dia memberikan kecukupan.

93:9

# فَاَمَّا الْيَتِيْمَ فَلَا تَقْهَرْۗ

fa-amm*aa* **a**lyatiima fal*aa* taqhar

Maka terhadap anak yatim janganlah engkau berlaku sewenang-wenang.

93:10

# وَاَمَّا السَّاۤىِٕلَ فَلَا تَنْهَرْ

wa-amm*aa* **al**ss*aa*-ila fal*aa* tanhar

Dan terhadap orang yang meminta-minta janganlah engkau menghardik(nya).

93:11

# وَاَمَّا بِنِعْمَةِ رَبِّكَ فَحَدِّثْ ࣖ

wa-amm*aa* bini'mati rabbika fa*h*addits

Dan terhadap nikmat Tuhanmu hendaklah engkau nyatakan (dengan bersyukur).

<!--EndFragment-->