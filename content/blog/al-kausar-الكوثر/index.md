---
title: (108) Al-Kausar - الكوثر
date: 2021-10-27T04:17:54.514Z
ayat: 108
description: "Jumlah Ayat: 3 / Arti: Pemberian Yang Banyak"
---
<!--StartFragment-->

108:1

# اِنَّآ اَعْطَيْنٰكَ الْكَوْثَرَۗ

inn*aa* a'*th*ayn*aa*ka **a**lkawtsar**a**

Sungguh, Kami telah memberimu (Muhammad) nikmat yang banyak.

108:2

# فَصَلِّ لِرَبِّكَ وَانْحَرْۗ

fa*sh*alli lirabbika wa**i**n*h*ar

Maka laksanakanlah salat karena Tuhanmu, dan berkurbanlah (sebagai ibadah dan mendekatkan diri kepada Allah).

108:3

# اِنَّ شَانِئَكَ هُوَ الْاَبْتَرُ ࣖ

inna sy*aa*ni-aka huwa **a**l-abtar**u**

Sungguh, orang-orang yang membencimu dialah yang terputus (dari rahmat Allah).

<!--EndFragment-->