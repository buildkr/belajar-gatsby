---
title: (41) Fussilat - فصّلت
date: 2021-10-27T03:49:37.743Z
ayat: 41
description: "Jumlah Ayat: 54 / Arti: Yang Dijelaskan"
---
<!--StartFragment-->

41:1

# حٰمۤ ۚ

*haa*-miim

Ha Mim

41:2

# تَنْزِيْلٌ مِّنَ الرَّحْمٰنِ الرَّحِيْمِ ۚ

tanziilun mina **al**rra*h*m*aa*ni **al**rra*h*iim**i**

(Al-Qur'an ini) diturunkan dari Tuhan Yang Maha Pengasih, Maha Penyayang.

41:3

# كِتٰبٌ فُصِّلَتْ اٰيٰتُهٗ قُرْاٰنًا عَرَبِيًّا لِّقَوْمٍ يَّعْلَمُوْنَۙ

kit*aa*bun fu*shsh*ilat *aa*y*aa*tuhu qur-*aa*nan 'arabiyyan liqawmin ya'lamuun**a**

Kitab yang ayat-ayatnya dijelaskan, bacaan dalam bahasa Arab, untuk kaum yang mengetahui,

41:4

# بَشِيْرًا وَّنَذِيْرًاۚ فَاَعْرَضَ اَكْثَرُهُمْ فَهُمْ لَا يَسْمَعُوْنَ

basyiiran wana*dz*iiran fa-a'ra*dh*a aktsaruhum fahum l*aa* yasma'uun**a**

yang membawa berita gembira dan peringatan, tetapi kebanyakan mereka berpaling (darinya) serta tidak mendengarkan.

41:5

# وَقَالُوْا قُلُوْبُنَا فِيْٓ اَكِنَّةٍ مِّمَّا تَدْعُوْنَآ اِلَيْهِ وَفِيْٓ اٰذَانِنَا وَقْرٌ وَّمِنْۢ بَيْنِنَا وَبَيْنِكَ حِجَابٌ فَاعْمَلْ اِنَّنَا عٰمِلُوْنَ

waq*aa*luu quluubun*aa* fii akinnatin mimm*aa* tad'uun*aa* ilayhi wafii *aatsaa*nin*aa* waqrun wamin baynin*aa* wabaynika *h*ij*aa*bun fa**i**'mal innan*aa* '*aa*miluun**a**

Dan mereka berkata, “Hati kami sudah tertutup dari apa yang engkau seru kami kepadanya dan telinga kami sudah tersumbat, dan di antara kami dan engkau ada dinding, karena itu lakukanlah (sesuai kehendakmu), sesungguhnya kami akan melakukan (sesuai kehenda

41:6

# قُلْ اِنَّمَآ اَنَا۟ بَشَرٌ مِّثْلُكُمْ يُوْحٰىٓ اِلَيَّ اَنَّمَآ اِلٰهُكُمْ اِلٰهٌ وَّاحِدٌ فَاسْتَقِيْمُوْٓا اِلَيْهِ وَاسْتَغْفِرُوْهُ ۗوَوَيْلٌ لِّلْمُشْرِكِيْنَۙ

qul innam*aa* an*aa* basyarun mitslukum yuu*haa* ilayya annam*aa* il*aa*hukum il*aa*hun w*aah*idun fa**i**staqiimuu ilayhi wa**i**staghfiruuhu wawaylun lilmusyrikiin**a**

Katakanlah (Muhammad), “Aku ini hanyalah seorang manusia seperti kamu, yang diwahyukan kepadaku bahwa Tuhan kamu adalah Tuhan Yang Maha Esa, karena itu tetaplah kamu (beribadah) kepada-Nya dan mohonlah ampunan kepada-Nya. Dan celakalah bagi orang-orang ya

41:7

# الَّذِيْنَ لَا يُؤْتُوْنَ الزَّكٰوةَ وَهُمْ بِالْاٰخِرَةِ هُمْ كٰفِرُوْنَ

**al**la*dz*iina l*aa* yu/tuuna **al**zzak*aa*ta wahum bi**a**l-*aa*khirati hum k*aa*firuun**a**

(yaitu) orang-orang yang tidak menunaikan zakat dan mereka ingkar terhadap kehidupan akhirat.

41:8

# اِنَّ الَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِ لَهُمْ اَجْرٌ غَيْرُ مَمْنُوْنٍ ࣖ

inna **al**la*dz*iina *aa*manuu wa'amiluu **al***shshaa*li*haa*ti lahum ajrun ghayru mamnuun**in**

Sesungguhnya orang-orang yang beriman dan mengerjakan kebajikan, mereka mendapat pahala yang tidak ada putus-putusnya.”

41:9

# ۞ قُلْ اَىِٕنَّكُمْ لَتَكْفُرُوْنَ بِالَّذِيْ خَلَقَ الْاَرْضَ فِيْ يَوْمَيْنِ وَتَجْعَلُوْنَ لَهٗٓ اَنْدَادًا ۗذٰلِكَ رَبُّ الْعٰلَمِيْنَ ۚ

qul a-innakum latakfuruuna bi**a**lla*dz*ii khalaqa **a**l-ar*dh*a fii yawmayni wataj'aluuna lahu and*aa*dan *dzaa*lika rabbu **a**l'*aa*lamiin**a**

Katakanlah, “Pantaskah kamu ingkar kepada Tuhan yang menciptakan bumi dalam dua masa dan kamu adakan pula sekutu-sekutu bagi-Nya? Itulah Tuhan seluruh alam.”

41:10

# وَجَعَلَ فِيْهَا رَوَاسِيَ مِنْ فَوْقِهَا وَبٰرَكَ فِيْهَا وَقَدَّرَ فِيْهَآ اَقْوَاتَهَا فِيْٓ اَرْبَعَةِ اَيَّامٍۗ سَوَاۤءً لِّلسَّاۤىِٕلِيْنَ

waja'ala fiih*aa* raw*aa*siya min fawqih*aa* wab*aa*raka fiih*aa* waqaddara fiih*aa* aqw*aa*tah*aa* fii arba'ati ayy*aa*min saw*aa*-an li**l**ss*aa*-iliin**a**

Dan Dia ciptakan padanya gunung-gunung yang kokoh di atasnya. Dan kemudian Dia berkahi, dan Dia tentukan makanan-makanan (bagi penghuni)nya dalam empat masa, memadai untuk (memenuhi kebutuhan) mereka yang memerlukannya.

41:11

# ثُمَّ اسْتَوٰىٓ اِلَى السَّمَاۤءِ وَهِيَ دُخَانٌ فَقَالَ لَهَا وَلِلْاَرْضِ ائْتِيَا طَوْعًا اَوْ كَرْهًاۗ قَالَتَآ اَتَيْنَا طَاۤىِٕعِيْنَ

tsumma istaw*aa* il*aa* **al**ssam*aa*-i wahiya dukh*aa*nun faq*aa*la lah*aa* walil-ar*dh*i i/tiy*aa* *th*aw'an aw karhan q*aa*lat*aa* atayn*aa* *thaa*-i'iin**a**

**Kemudian Dia menuju ke langit dan (langit) itu masih berupa asap, lalu Dia berfirman kepadanya dan kepada bumi, “Datanglah kamu berdua menurut perintah-Ku dengan patuh atau terpaksa.” Keduanya menjawab, “Kami datang dengan patuh.”**









41:12

# فَقَضٰىهُنَّ سَبْعَ سَمٰوٰتٍ فِيْ يَوْمَيْنِ وَاَوْحٰى فِيْ كُلِّ سَمَاۤءٍ اَمْرَهَا ۗوَزَيَّنَّا السَّمَاۤءَ الدُّنْيَا بِمَصَابِيْحَۖ وَحِفْظًا ۗذٰلِكَ تَقْدِيْرُ الْعَزِيْزِ الْعَلِيْمِ

faqa*daa*hunna sab'a sam*aa*w*aa*tin fii yawmayni wa-aw*haa* fii kulli sam*aa*-in amrah*aa* wazayyann*aa* **al**ssam*aa*-a **al**dduny*aa* bima*shaa*bii*h*a wa*h*if*zh*

Lalu diciptakan-Nya tujuh langit dalam dua masa dan pada setiap langit Dia mewahyukan urusan masing-masing. Kemudian langit yang dekat (dengan bumi), Kami hiasi dengan bintang-bintang, dan (Kami ciptakan itu) untuk memelihara. Demikianlah ketentuan (Allah







41:13

# فَاِنْ اَعْرَضُوْا فَقُلْ اَنْذَرْتُكُمْ صٰعِقَةً مِّثْلَ صٰعِقَةِ عَادٍ وَّثَمُوْدَ ۗ

fa-in a'ra*dh*uu fuqul an*dz*artukum *shaa*'iqatan mitsla *shaa*'iqati '*aa*din watsamuud**a**

Jika mereka berpaling maka katakanlah, “Aku telah memperingatkan kamu akan (bencana) petir seperti petir yang menimpa kaum ’Ad dan kaum Samud.”

41:14

# اِذْ جَاۤءَتْهُمُ الرُّسُلُ مِنْۢ بَيْنِ اَيْدِيْهِمْ وَمِنْ خَلْفِهِمْ اَلَّا تَعْبُدُوْٓا اِلَّا اللّٰهَ ۗقَالُوْا لَوْ شَاۤءَ رَبُّنَا لَاَنْزَلَ مَلٰۤىِٕكَةً فَاِنَّا بِمَآ اُرْسِلْتُمْ بِهٖ كٰفِرُوْنَ

i*dz* j*aa*-at-humu **al**rrusulu min bayni aydiihim wamin khalfihim **al**l*aa* ta'buduu ill*aa* **al**l*aa*ha q*aa*luu law sy*aa*-a rabbun*aa* la-anzala mal*aa*-ikatan f

Ketika para rasul datang kepada mereka dari depan dan dari belakang mereka (dengan menyerukan), “Janganlah kamu menyembah selain Allah.” Mereka menjawab, “Kalau Tuhan kami menghendaki tentu Dia menurunkan malaikat-malaikat-Nya, maka sesungguhnya kami meng

41:15

# فَاَمَّا عَادٌ فَاسْتَكْبَرُوْا فِى الْاَرْضِ بِغَيْرِ الْحَقِّ وَقَالُوْا مَنْ اَشَدُّ مِنَّا قُوَّةً ۗ اَوَلَمْ يَرَوْا اَنَّ اللّٰهَ الَّذِيْ خَلَقَهُمْ هُوَ اَشَدُّ مِنْهُمْ قُوَّةً ۗ وَكَانُوْا بِاٰيٰتِنَا يَجْحَدُوْنَ

fa-amm*aa* '*aa*dun fa**i**stakbaruu fii **a**l-ar*dh*i bighayri **a**l*h*aqqi waq*aa*luu man asyaddu minn*aa* quwwatan awa lam yaraw anna **al**l*aa*ha **al**

**Maka adapun kaum ‘Ad, mereka menyombongkan diri di bumi tanpa (mengindahkan) kebenaran dan mereka berkata, “Siapakah yang lebih hebat kekuatannya dari kami?” Tidakkah mereka memperhatikan bahwa sesungguhnya Allah yang menciptakan mereka. Dia lebih hebat k**









41:16

# فَاَرْسَلْنَا عَلَيْهِمْ رِيْحًا صَرْصَرًا فِيْٓ اَيَّامٍ نَّحِسَاتٍ لِّنُذِيْقَهُمْ عَذَابَ الْخِزْيِ فِى الْحَيٰوةِ الدُّنْيَا ۗوَلَعَذَابُ الْاٰخِرَةِ اَخْزٰى وَهُمْ لَا يُنْصَرُوْنَ

fa-arsaln*aa* 'alayhim rii*h*an *sh*ar*sh*aran fii ayy*aa*min na*h*is*aa*tin linu*dz*iiqahum 'a*dzaa*ba **a**lkhizyi fii **a**l*h*ay*aa*ti **al**dduny*aa* w

Maka Kami tiupkan angin yang sangat bergemuruh kepada mereka dalam beberapa hari yang nahas, karena Kami ingin agar mereka itu merasakan siksaan yang menghinakan dalam kehidupan di dunia. Sedangkan azab akhirat pasti lebih menghinakan dan mereka tidak dib

41:17

# وَاَمَّا ثَمُوْدُ فَهَدَيْنٰهُمْ فَاسْتَحَبُّوا الْعَمٰى عَلَى الْهُدٰى فَاَخَذَتْهُمْ صٰعِقَةُ الْعَذَابِ الْهُوْنِ بِمَا كَانُوْا يَكْسِبُوْنَ ۚ

wa-amm*aa* tsamuudu fahadayn*aa*hum fa**i**sta*h*abbuu **a**l'am*aa* 'al*aa* **a**lhud*aa* fa-akha*dz*at-hum *shaa*'iqatu **a**l'a*dzaa*bi **a**l

Dan adapun kaum Samud, mereka telah Kami beri petunjuk tetapi mereka lebih menyukai kebutaan (kesesatan) daripada petunjuk itu, maka mereka disambar petir sebagai azab yang menghinakan disebabkan apa yang telah mereka kerjakan.

41:18

# وَنَجَّيْنَا الَّذِيْنَ اٰمَنُوْا وَكَانُوْا يَتَّقُوْنَ ࣖ

wanajjayn*aa* **al**la*dz*iina *aa*manuu wak*aa*nuu yattaquun**a**

Dan Kami selamatkan orang-orang yang beriman karena mereka adalah orang-orang yang bertakwa.

41:19

# وَيَوْمَ يُحْشَرُ اَعْدَاۤءُ اللّٰهِ اِلَى النَّارِ فَهُمْ يُوْزَعُوْنَ

wayawma yu*h*syaru a'd*aa*u **al**l*aa*hi il*aa* **al**nn*aa*ri fahum yuuza'uun**a**

Dan (ingatlah) pada hari (ketika) musuh-musuh Allah digiring ke neraka lalu mereka dipisah-pisahkan.

41:20

# حَتّٰىٓ اِذَا مَا جَاۤءُوْهَا شَهِدَ عَلَيْهِمْ سَمْعُهُمْ وَاَبْصَارُهُمْ وَجُلُوْدُهُمْ بِمَا كَانُوْا يَعْمَلُوْنَ

*h*att*aa* i*dzaa* m*aa* j*aa*uuh*aa* syahida 'alayhim sam'uhum wa-ab*shaa*ruhum wajuluuduhum bim*aa* k*aa*nuu ya'maluun**a**

Sehingga apabila mereka sampai ke neraka, pendengaran, penglihatan dan kulit mereka menjadi saksi terhadap apa yang telah mereka lakukan.

41:21

# وَقَالُوْا لِجُلُوْدِهِمْ لِمَ شَهِدْتُّمْ عَلَيْنَا ۗقَالُوْٓا اَنْطَقَنَا اللّٰهُ الَّذِيْٓ اَنْطَقَ كُلَّ شَيْءٍ وَّهُوَ خَلَقَكُمْ اَوَّلَ مَرَّةٍۙ وَّاِلَيْهِ تُرْجَعُوْنَ

waq*aa*luu lijuluudihim lima syahidtum 'alayn*aa* q*aa*luu an*th*aqan*aa* **al**l*aa*hu **al**la*dz*ii an*th*aqa kulla syay-in wahuwa khalaqakum awwala marratin wa-ilayhi turja'uun**a**

Dan mereka berkata kepada kulit mereka, “Mengapa kamu menjadi saksi terhadap kami?” (Kulit) mereka men-jawab, “Yang menjadikan kami dapat berbicara adalah Allah, yang (juga) menjadikan segala sesuatu dapat berbicara, dan Dialah yang menciptakan kamu yang







41:22

# وَمَا كُنْتُمْ تَسْتَتِرُوْنَ اَنْ يَّشْهَدَ عَلَيْكُمْ سَمْعُكُمْ وَلَآ اَبْصَارُكُمْ وَلَا جُلُوْدُكُمْ وَلٰكِنْ ظَنَنْتُمْ اَنَّ اللّٰهَ لَا يَعْلَمُ كَثِيْرًا مِّمَّا تَعْمَلُوْنَ

wam*aa* kuntum tastatiruuna an yasyhada 'alaykum sam'ukum wal*aa* ab*shaa*rukum wal*aa* juluudukum wal*aa*kin *zh*anantum anna **al**l*aa*ha l*aa* ya'lamu katsiiran mimm*aa* ta'maluun**a**

**Dan kamu tidak dapat bersembunyi dari kesaksian pendengaran, penglihatan dan kulitmu terhadapmu ) bahkan kamu mengira Allah tidak mengetahui banyak tentang apa yang kamu lakukan.**









41:23

# وَذٰلِكُمْ ظَنُّكُمُ الَّذِيْ ظَنَنْتُمْ بِرَبِّكُمْ اَرْدٰىكُمْ فَاَصْبَحْتُمْ مِّنَ الْخٰسِرِيْنَ

wa*dzaa*likum *zh*annukumu **al**la*dz*ii *zh*anantum birabbikum ard*aa*kum fa-a*sh*ba*h*tum mina **a**lkh*aa*siriin**a**

Dan itulah dugaanmu yang telah kamu sangkakan terhadap Tuhanmu (dugaan itu) telah membinasakan kamu, sehingga jadilah kamu termasuk orang yang rugi.

41:24

# فَاِنْ يَّصْبِرُوْا فَالنَّارُ مَثْوًى لَّهُمْ ۚوَاِنْ يَّسْتَعْتِبُوْا فَمَا هُمْ مِّنَ الْمُعْتَبِيْنَ

fa-in ya*sh*biruu fa**al**nn*aa*ru matswan lahum wa-in yasta'tibuu fam*aa* hum mina **a**lmu'tabiin**a**

Meskipun mereka bersabar (atas azab neraka) maka nerakalah tempat tinggal mereka dan jika mereka minta belas kasihan, maka mereka itu tidak termasuk orang yang pantas dikasihani.

41:25

# ۞ وَقَيَّضْنَا لَهُمْ قُرَنَاۤءَ فَزَيَّنُوْا لَهُمْ مَّا بَيْنَ اَيْدِيْهِمْ وَمَا خَلْفَهُمْ وَحَقَّ عَلَيْهِمُ الْقَوْلُ فِيْٓ اُمَمٍ قَدْ خَلَتْ مِنْ قَبْلِهِمْ مِّنَ الْجِنِّ وَالْاِنْسِۚ اِنَّهُمْ كَانُوْا خٰسِرِيْنَ ࣖ

waqayya*dh*n*aa* lahum quran*aa*-a fazayyanuu lahum m*aa* bayna aydiihim wam*aa* khalfahum wa*h*aqqa 'alayhimu **a**lqawlu fii umamin qad khalat min qablihim mina **a**ljinni wa**a**l-in

Dan Kami tetapkan bagi mereka teman-teman (setan) yang memuji-muji apa saja yang ada di hadapan dan di belakang mereka dan tetaplah atas mereka putusan azab bersama umat-umat yang terdahulu sebelum mereka dari (golongan) jin dan manusia. Sungguh, mereka a

41:26

# وَقَالَ الَّذِيْنَ كَفَرُوْا لَا تَسْمَعُوْا لِهٰذَا الْقُرْاٰنِ وَالْغَوْا فِيْهِ لَعَلَّكُمْ تَغْلِبُوْنَ

waq*aa*la **al**la*dz*iina kafaruu l*aa* tasma'uu lih*aadzaa* **a**lqur-*aa*ni wa**i**lghaw fiihi la'allakum taghlibuun**a**

Dan orang-orang yang kafir berkata, “Janganlah kamu mendengarkan (bacaan) Al-Qur'an ini dan buatlah kegaduhan terhadapnya, agar kamu dapat mengalahkan (mereka).”

41:27

# فَلَنُذِيْقَنَّ الَّذِيْنَ كَفَرُوْا عَذَابًا شَدِيْدًاۙ وَّلَنَجْزِيَنَّهُمْ اَسْوَاَ الَّذِيْ كَانُوْا يَعْمَلُوْنَ

falanu*dz*iiqanna **al**la*dz*iina kafaruu 'a*dzaa*ban syadiidan walanajziyannahum aswa-a **al**la*dz*ii k*aa*nuu ya'maluun**a**

Maka sungguh, akan Kami timpakan azab yang keras kepada orang-orang yang kafir itu dan sungguh, akan Kami beri balasan mereka dengan seburuk-buruk balasan terhadap apa yang telah mereka kerjakan.

41:28

# ذٰلِكَ جَزَاۤءُ اَعْدَاۤءِ اللّٰهِ النَّارُ لَهُمْ فِيْهَا دَارُ الْخُلْدِ ۗجَزَاۤءً ۢبِمَا كَانُوْا بِاٰيٰتِنَا يَجْحَدُوْنَ

*dzaa*lika jaz*aa*u a'd*aa*-i **al**l*aa*hi **al**nn*aa*ru lahum fiih*aa* d*aa*ru **a**lkhuldi jaz*aa*-an bim*aa* k*aa*nuu bi-*aa*y*aa*tin*aa* yaj*h<*

Demikianlah balasan (terhadap) musuh-musuh Allah (yaitu) neraka; mereka mendapat tempat tinggal yang kekal di dalamnya sebagai balasan atas keingkaran mereka terhadap ayat-ayat Kami.







41:29

# وَقَالَ الَّذِيْنَ كَفَرُوْا رَبَّنَآ اَرِنَا الَّذَيْنِ اَضَلّٰنَا مِنَ الْجِنِّ وَالْاِنْسِ نَجْعَلْهُمَا تَحْتَ اَقْدَامِنَا لِيَكُوْنَا مِنَ الْاَسْفَلِيْنَ

waq*aa*la **al**la*dz*iina kafaruu rabban*aa* arin*aa* **al**la*dz*ayni a*dh*all*aa*n*aa* mina **a**ljinni wa**a**l-insi naj'alhum*aa* ta*h*ta aqd*aa*

*Dan orang-orang yang kafir berkata, “Ya Tuhan kami, perlihatkanlah kepada kami dua golongan yang telah menyesatkan kami yaitu (golongan) jin dan manusia, agar kami letakkan keduanya di bawah telapak kaki kami agar kedua golongan itu menjadi yang paling ba*









41:30

# اِنَّ الَّذِيْنَ قَالُوْا رَبُّنَا اللّٰهُ ثُمَّ اسْتَقَامُوْا تَتَنَزَّلُ عَلَيْهِمُ الْمَلٰۤىِٕكَةُ اَلَّا تَخَافُوْا وَلَا تَحْزَنُوْا وَاَبْشِرُوْا بِالْجَنَّةِ الَّتِيْ كُنْتُمْ تُوْعَدُوْنَ

inna **al**la*dz*iina q*aa*luu rabbun*aa* **al**l*aa*hu tsumma istaq*aa*muu tatanazzalu 'alayhimu **a**lmal*aa*-ikatu **al**l*aa* takh*aa*fuu wal*aa* ta*h*

*Sesungguhnya orang-orang yang berkata, “Tuhan kami adalah Allah” kemudian mereka meneguhkan pendirian mereka, maka malaikat-malaikat akan turun kepada mereka (dengan berkata), “Janganlah kamu merasa takut dan janganlah kamu bersedih hati; dan bergembirala*









41:31

# نَحْنُ اَوْلِيَاۤؤُكُمْ فِى الْحَيٰوةِ الدُّنْيَا وَفِى الْاٰخِرَةِ ۚوَلَكُمْ فِيْهَا مَا تَشْتَهِيْٓ اَنْفُسُكُمْ وَلَكُمْ فِيْهَا مَا تَدَّعُوْنَ ۗ

na*h*nu awliy*aa*ukum fii **a**l*h*ay*aa*ti **al**dduny*aa* wafii **a**l-*aa*khirati walakum fiih*aa* m*aa* tasytahii anfusukum walakum fiih*aa* m*aa* tadda'uun

Kamilah pelindung-pelindungmu dalam kehidupan dunia dan akhirat; di dalamnya (surga) kamu memperoleh apa yang kamu inginkan dan memperoleh apa yang kamu minta.

41:32

# نُزُلًا مِّنْ غَفُوْرٍ رَّحِيْمٍ ࣖ

nuzulan min ghafuurin ra*h*iim**in**

Sebagai penghormatan (bagimu) dari (Allah) Yang Maha Pengampun, Maha Penyayang.

41:33

# وَمَنْ اَحْسَنُ قَوْلًا مِّمَّنْ دَعَآ اِلَى اللّٰهِ وَعَمِلَ صَالِحًا وَّقَالَ اِنَّنِيْ مِنَ الْمُسْلِمِيْنَ

waman a*h*sanu qawlan mimman da'*aa* il*aa* **al**l*aa*hi wa'amila *shaa*li*h*an waq*aa*la innanii mina **a**lmusl

Dan siapakah yang lebih baik perkataannya daripada orang yang menyeru kepada Allah dan mengerjakan kebajikan dan berkata, “Sungguh, aku termasuk orang-orang muslim (yang berserah diri)?”

41:34

# وَلَا تَسْتَوِى الْحَسَنَةُ وَلَا السَّيِّئَةُ ۗاِدْفَعْ بِالَّتِيْ هِيَ اَحْسَنُ فَاِذَا الَّذِيْ بَيْنَكَ وَبَيْنَهٗ عَدَاوَةٌ كَاَنَّهٗ وَلِيٌّ حَمِيْمٌ

wal*aa* tastawii **a**l*h*asanatu wal*aa* **al**ssayyi-atu idfa' bi**a**llatii hiya a*h*sanu fa-i*dzaa* **al**la*dz*ii baynaka wabaynahu 'ad*aa*watun ka-annahu waliyyun

Dan tidaklah sama kebaikan dengan kejahatan. Tolaklah (kejahatan itu) dengan cara yang lebih baik, sehingga orang yang ada rasa permusuhan an-tara kamu dan dia akan seperti teman yang setia.

41:35

# وَمَا يُلَقّٰىهَآ اِلَّا الَّذِيْنَ صَبَرُوْاۚ وَمَا يُلَقّٰىهَآ اِلَّا ذُوْ حَظٍّ عَظِيْمٍ

wam*aa* yulaqq*aa*h*aa* ill*aa* **al**la*dz*iina *sh*abaruu wam*aa* yulaqq*aa*h*aa* ill*aa* *dz*uu *h*a*zhzh*in 'a*zh*iim**in**

Dan (sifat-sifat yang baik itu) tidak akan dianugerahkan kecuali kepada orang-orang yang sabar dan tidak dianugerahkan kecuali kepada orang-orang yang mempunyai keberuntungan yang besar.

41:36

# وَاِمَّا يَنْزَغَنَّكَ مِنَ الشَّيْطٰنِ نَزْغٌ فَاسْتَعِذْ بِاللّٰهِ ۗاِنَّهٗ هُوَ السَّمِيْعُ الْعَلِيْمُ

wa-imm*aa* yanzaghannaka mina **al**sysyay*thaa*ni nazghun fa**i**sta'i*dz* bi**al**l*aa*hi innahu huwa **al**ssamii'u **a**l'aliim

Dan jika setan mengganggumu dengan suatu godaan, maka mohonlah perlindungan kepada Allah. Sungguh, Dialah Yang Maha Mendengar, Maha Mengetahui.

41:37

# وَمِنْ اٰيٰتِهِ الَّيْلُ وَالنَّهَارُ وَالشَّمْسُ وَالْقَمَرُۗ لَا تَسْجُدُوْا لِلشَّمْسِ وَلَا لِلْقَمَرِ وَاسْجُدُوْا لِلّٰهِ الَّذِيْ خَلَقَهُنَّ اِنْ كُنْتُمْ اِيَّاهُ تَعْبُدُوْنَ

wamin *aa*y*aa*tihi **al**laylu wa**al**nnah*aa*ru wa**al**sysyamsu wa**a**lqamaru l*aa* tasjuduu li**l**sysyamsi wal*aa* lilqamari wa**u**sjuduu lill

Dan sebagian dari tanda-tanda kebesaran-Nya ialah malam, siang, mata-hari dan bulan. Janganlah bersujud kepada matahari dan jangan (pula) kepada bulan, tetapi bersujudlah kepada Allah yang menciptakannya, jika kamu hanya menyembah kepada-Nya.

41:38

# فَاِنِ اسْتَكْبَرُوْا فَالَّذِيْنَ عِنْدَ رَبِّكَ يُسَبِّحُوْنَ لَهٗ بِالَّيْلِ وَالنَّهَارِ وَهُمْ لَا يَسْـَٔمُوْنَ ۩

fa-ini istakbaruu fa**a**lla*dz*iina 'inda rabbika yusabbi*h*uuna lahu bi**a**llayli wa**al**nnah*aa*ri wahum l*aa* yas-amuun**a**

Jika mereka menyombongkan diri, maka mereka (malaikat) yang di sisi Tuhanmu bertasbih kepada-Nya pada malam dan siang hari, sedang mereka tidak pernah jemu.

41:39

# وَمِنْ اٰيٰتِهٖٓ اَنَّكَ تَرَى الْاَرْضَ خَاشِعَةً فَاِذَآ اَنْزَلْنَا عَلَيْهَا الْمَاۤءَ اهْتَزَّتْ وَرَبَتْۗ اِنَّ الَّذِيْٓ اَحْيَاهَا لَمُحْيِ الْمَوْتٰى ۗاِنَّهٗ عَلٰى كُلِّ شَيْءٍ قَدِيْرٌ

wamin *aa*y*aa*tihi annaka tar*aa* **a**l-ar*dh*a kh*aa*syi'atan fa-i*dzaa* anzaln*aa* 'alayh*aa* **a**lm*aa*-a ihtazzat warabat inna **al**la*dz*ii a*h*y*aa*

*Dan sebagian dari tanda-tanda (kebesaran)-Nya, engkau melihat bumi itu kering dan tandus, tetapi apabila Kami turunkan hujan di atasnya, niscaya ia bergerak dan subur. Sesungguhnya (Allah) yang menghidupkannya pasti dapat menghidupkan yang mati; sesungguh*









41:40

# اِنَّ الَّذِيْنَ يُلْحِدُوْنَ فِيْٓ اٰيٰتِنَا لَا يَخْفَوْنَ عَلَيْنَاۗ اَفَمَنْ يُّلْقٰى فِى النَّارِ خَيْرٌ اَمَّنْ يَّأْتِيْٓ اٰمِنًا يَّوْمَ الْقِيٰمَةِ ۗاِعْمَلُوْا مَا شِئْتُمْ ۙاِنَّهٗ بِمَا تَعْمَلُوْنَ بَصِيْرٌ

inna **al**la*dz*iina yul*h*iduuna fii *aa*y*aa*tin*aa* l*aa* yakhfawna 'alayn*aa* afaman yulq*aa* fii **al**nn*aa*ri khayrun amman ya/tii *aa*minan yawma **a**lqiy

Sesungguhnya orang-orang yang mengingkari tanda-tanda (kebesaran) Kami, mereka tidak tersembunyi dari Kami. Apakah orang-orang yang dilemparkan ke dalam neraka yang lebih baik ataukah mereka yang datang dengan aman sentosa pada hari Kiamat? Lakukanlah apa

41:41

# اِنَّ الَّذِيْنَ كَفَرُوْا بِالذِّكْرِ لَمَّا جَاۤءَهُمْ ۗوَاِنَّهٗ لَكِتٰبٌ عَزِيْزٌ ۙ

inna **al**la*dz*iina kafaruu bi**al***dzdz*ikri lamm*aa* j*aa*-ahum wa-innahu lakit*aa*bun 'aziiz**un**

Sesungguhnya orang-orang yang mengingkari Al-Qur'an ketika (Al-Qur'an) itu disampaikan kepada mereka (mereka itu pasti akan celaka), dan sesungguhnya (Al-Qur'an) itu adalah Kitab yang mulia,

41:42

# لَّا يَأْتِيْهِ الْبَاطِلُ مِنْۢ بَيْنِ يَدَيْهِ وَلَا مِنْ خَلْفِهٖ ۗتَنْزِيْلٌ مِّنْ حَكِيْمٍ حَمِيْدٍ

l*aa* ya/tiihi **a**lb*aath*ilu min bayni yadayhi wal*aa* min khalfihi tanziilun min *h*akiimin *h*amiid**in**

yang) tidak akan didatangi oleh kebatilan baik dari depan maupun dari belakang (pada masa lalu dan yang akan datang), yang diturunkan dari Tuhan Yang Mahabijaksana, Maha Terpuji.

41:43

# مَا يُقَالُ لَكَ اِلَّا مَا قَدْ قِيْلَ لِلرُّسُلِ مِنْ قَبْلِكَ ۗاِنَّ رَبَّكَ لَذُوْ مَغْفِرَةٍ وَّذُوْ عِقَابٍ اَلِيْمٍ

m*aa* yuq*aa*lu laka ill*aa* m*aa* qad qiila li**l**rrusuli min qablika inna rabbaka la*dz*uu maghfiratin wa*dz*uu 'iq*aa*bin **a**liim**in**

Apa yang dikatakan (oleh orang-orang kafir) kepadamu tidak lain adalah apa yang telah dikatakan kepada rasul-rasul sebelummu. Sungguh, Tuhanmu mempunyai ampunan dan azab yang pedih.

41:44

# وَلَوْ جَعَلْنٰهُ قُرْاٰنًا اَعْجَمِيًّا لَّقَالُوْا لَوْلَا فُصِّلَتْ اٰيٰتُهٗ ۗ ءَاَ۬عْجَمِيٌّ وَّعَرَبِيٌّ ۗ قُلْ هُوَ لِلَّذِيْنَ اٰمَنُوْا هُدًى وَّشِفَاۤءٌ ۗوَالَّذِيْنَ لَا يُؤْمِنُوْنَ فِيْٓ اٰذَانِهِمْ وَقْرٌ وَّهُوَ عَلَيْهِمْ عَمًىۗ اُولٰۤىِٕكَ

walaw ja'aln*aa*hu qur-*aa*nan a'jamiyyan laq*aa*luu lawl*aa* fu*shsh*ilat *aa*y*aa*tuhu a-a'jamiyyun wa'arabiyyun qul huwa lilla*dz*iina *aa*manuu hudan wasyif*aa*un wa**a**lla*dz*iina l

Dan sekiranya Al-Qur'an Kami jadikan sebagai bacaan dalam bahasa selain bahasa Arab niscaya mereka mengatakan, “Mengapa tidak dijelaskan ayat-ayatnya?” Apakah patut (Al-Qur'an) dalam bahasa selain bahasa Arab sedang (rasul), orang Arab? Katakanlah, “Al-Qu

41:45

# وَلَقَدْ اٰتَيْنَا مُوْسَى الْكِتٰبَ فَاخْتُلِفَ فِيْهِ ۗوَلَوْلَا كَلِمَةٌ سَبَقَتْ مِنْ رَّبِّكَ لَقُضِيَ بَيْنَهُمْ ۗوَاِنَّهُمْ لَفِيْ شَكٍّ مِّنْهُ مُرِيْبٍ

walaqad *aa*tayn*aa* muus*aa* **a**lkit*aa*ba fa**i**khtulifa fiihi walawl*aa* kalimatun sabaqat min rabbika laqu*dh*iya baynahum wa-innahum lafii syakkin minhu muriib**in**

Dan sungguh, telah Kami berikan kepada Musa Kitab (Taurat) lalu diperselisihkan. Sekiranya tidak ada keputusan yang terdahulu dari Tuhanmu, orang-orang kafir itu pasti sudah dibinasakan. Dan sesungguhnya mereka benar-benar dalam keraguan yang mendalam ter

41:46

# مَنْ عَمِلَ صَالِحًا فَلِنَفْسِهٖ ۙوَمَنْ اَسَاۤءَ فَعَلَيْهَا ۗوَمَا رَبُّكَ بِظَلَّامٍ لِّلْعَبِيْدِ ۔

man 'amila *shaa*li*h*an falinafsihi waman as*aa*-a fa'alayh*aa* wam*aa* rabbuka bi*zh*all*aa*min lil'abiid**i**

Barangsiapa mengerjakan kebajikan maka (pahalanya) untuk dirinya sendiri dan barangsiapa berbuat jahat maka (dosanya) menjadi tanggungan dirinya sendiri. Dan Tuhanmu sama sekali tidak menzalimi hamba-hamba(-Nya).

41:47

# ۞ اِلَيْهِ يُرَدُّ عِلْمُ السَّاعَةِ ۗوَمَا تَخْرُجُ مِنْ ثَمَرٰتٍ ِمّنْ اَكْمَامِهَا وَمَا تَحْمِلُ مِنْ اُنْثٰى وَلَا تَضَعُ اِلَّا بِعِلْمِهٖ ۗوَيَوْمَ يُنَادِيْهِمْ اَيْنَ شُرَكَاۤءِيْۙ قَالُوْٓا اٰذَنّٰكَ مَا مِنَّا مِنْ شَهِيْدٍ ۚ

ilayhi yuraddu 'ilmu **al**ss*aa*'ati wam*aa* takhruju min tsamar*aa*tin min akm*aa*mih*aa* wam*aa* ta*h*milu min unts*aa* wal*aa* ta*dh*a'u ill*aa* bi'ilmihi wayawma yun*aa*diihim a

Kepada-Nyalah ilmu tentang hari Kiamat itu dikembalikan. Tidak ada buah-buahan yang keluar dari kelopaknya dan tidak seorang perempuan pun yang mengandung dan yang melahirkan, melainkan semuanya dengan sepengetahuan-Nya. Pada hari ketika Dia (Allah) menye

41:48

# وَضَلَّ عَنْهُمْ مَّا كَانُوْا يَدْعُوْنَ مِنْ قَبْلُ وَظَنُّوْا مَا لَهُمْ مِّنْ مَّحِيْصٍ

wa*dh*alla 'anhum m*aa* k*aa*nuu yad'uuna min qablu wa*zh*annuu m*aa* lahum min ma*h*ii*sh***in**

Dan lenyaplah dari mereka apa yang dahulu selalu mereka sembah, dan mereka pun tahu bahwa tidak ada jalan keluar (dari azab Allah) bagi mereka.

41:49

# لَا يَسْـَٔمُ الْاِنْسَانُ مِنْ دُعَاۤءِ الْخَيْرِۖ وَاِنْ مَّسَّهُ الشَّرُّ فَيَـُٔوْسٌ قَنُوْطٌ

l*aa* yas-amu **a**l-ins*aa*nu min du'*aa*-i **a**lkhayri wa-in massahu **al**sysyarru fayauusun qanuu*th***u**

Manusia tidak jemu memohon kebaikan, dan jika ditimpa malapetaka, mereka berputus asa dan hilang harapannya.

41:50

# وَلَىِٕنْ اَذَقْنٰهُ رَحْمَةً مِّنَّا مِنْۢ بَعْدِ ضَرَّاۤءَ مَسَّتْهُ لَيَقُوْلَنَّ هٰذَا لِيْۙ وَمَآ اَظُنُّ السَّاعَةَ قَاۤىِٕمَةًۙ وَّلَىِٕنْ رُّجِعْتُ اِلٰى رَبِّيْٓ اِنَّ لِيْ عِنْدَهٗ لَلْحُسْنٰىۚ فَلَنُنَبِّئَنَّ الَّذِيْنَ كَفَرُوْا بِمَا عَمِلُ

wala-in a*dz*aqn*aa*hu ra*h*matan minn*aa* min ba'di *dh*arr*aa*-a massat-hu layaquulanna h*aadzaa* lii wam*aa* a*zh*unnu **al**ss*aa*'ata q*aa*-imatan wala-in ruji'tu il*aa* rabbii

Dan jika Kami berikan kepadanya suatu rahmat dari Kami setelah ditimpa kesusahan, pastilah dia berkata, “Ini adalah hakku, dan aku tidak yakin bahwa hari Kiamat itu akan terjadi. Dan jika aku dikembalikan kepada Tuhanku, sesungguhnya aku akan memperoleh k

41:51

# وَاِذَآ اَنْعَمْنَا عَلَى الْاِنْسَانِ اَعْرَضَ وَنَاٰ بِجَانِبِهٖۚ وَاِذَا مَسَّهُ الشَّرُّ فَذُوْ دُعَاۤءٍ عَرِيْضٍ

wa-i*dzaa* an'amn*aa* 'al*aa* **a**l-ins*aa*ni a'ra*dh*a wana*aa* bij*aa*nibihi wa-i*dzaa* massahu **al**sysyarru fa*dz*uu du'*aa*-in 'arii*dh***in**

Dan apabila Kami berikan nikmat kepada manusia, dia berpaling dan menjauhkan diri (dengan sombong); tetapi apabila ditimpa malapetaka maka dia banyak berdoa.

41:52

# قُلْ اَرَءَيْتُمْ اِنْ كَانَ مِنْ عِنْدِ اللّٰهِ ثُمَّ كَفَرْتُمْ بِهٖ مَنْ اَضَلُّ مِمَّنْ هُوَ فِيْ شِقَاقٍۢ بَعِيْدٍ

qul ara-aytum in k*aa*na min 'indi **al**l*aa*hi tsumma kafartum bihi man a*dh*allu mimman huwa fii syiq*aa*qin ba'iid**in**

Katakanlah, “Bagaimana pendapatmu jika (Al-Qur'an) itu datang dari sisi Allah, kemudian kamu mengingkarinya. Siapakah yang lebih sesat daripada orang yang selalu berada dalam penyimpangan yang jauh (dari kebenaran)?”

41:53

# سَنُرِيْهِمْ اٰيٰتِنَا فِى الْاٰفَاقِ وَفِيْٓ اَنْفُسِهِمْ حَتّٰى يَتَبَيَّنَ لَهُمْ اَنَّهُ الْحَقُّۗ اَوَلَمْ يَكْفِ بِرَبِّكَ اَنَّهٗ عَلٰى كُلِّ شَيْءٍ شَهِيْدٌ

sanuriihim *aa*y*aa*tin*aa* fii **a**l-*aa*f*aa*qi wafii anfusihim *h*att*aa* yatabayyana lahum annahu **a**l*h*aqqu awa lam yakfi birabbika annahu 'al*aa* kulli syay-in syahiid

Kami akan memperlihatkan kepada mereka tanda-tanda (kebesaran) Kami di segenap penjuru dan pada diri mereka sendiri, sehingga jelaslah bagi mereka bahwa Al-Qur'an itu adalah benar. Tidak cukupkah (bagi kamu) bahwa Tuhanmu menjadi saksi atas segala sesuatu

41:54

# اَلَآ اِنَّهُمْ فِيْ مِرْيَةٍ مِّنْ لِّقَاۤءِ رَبِّهِمْ ۗ اَلَآ اِنَّهٗ بِكُلِّ شَيْءٍ مُّحِيْطٌ ࣖ

al*aa* innahum fii miryatin min liq*aa*-i rabbihim **a**l*aa* innahu bikulli syay-in mu*h*ii*th***un**

Ingatlah, sesungguhnya mereka dalam keraguan tentang pertemuan dengan Tuhan mereka. Ingatlah, sesungguhnya Dia Maha Meliputi segala sesuatu.

<!--EndFragment-->