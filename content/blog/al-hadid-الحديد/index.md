---
title: (57) Al-Hadid - الحديد
date: 2021-10-27T04:13:54.362Z
ayat: 57
description: "Jumlah Ayat: 29 / Arti: Besi"
---
<!--StartFragment-->

57:1

# سَبَّحَ لِلّٰهِ مَا فِى السَّمٰوٰتِ وَالْاَرْضِۚ وَهُوَ الْعَزِيْزُ الْحَكِيْمُ

sabba*h*a lill*aa*hi m*aa* fii **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i wahuwa **a**l'aziizu **a**l*h*akiim**u**

Apa yang di langit dan di bumi bertasbih kepada Allah. Dialah Yang Mahaperkasa, Mahabijaksana.

57:2

# لَهٗ مُلْكُ السَّمٰوٰتِ وَالْاَرْضِۚ يُحْيٖ وَيُمِيْتُۚ وَهُوَ عَلٰى كُلِّ شَيْءٍ قَدِيْرٌ

lahu mulku **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i yu*h*yii wayumiitu wahuwa 'al*aa* kulli syay-in qadiir**un**

Milik-Nyalah kerajaan langit dan bumi, Dia menghidupkan dan mematikan, dan Dia Mahakuasa atas segala sesuatu.

57:3

# هُوَ الْاَوَّلُ وَالْاٰخِرُ وَالظَّاهِرُ وَالْبَاطِنُۚ وَهُوَ بِكُلِّ شَيْءٍ عَلِيْمٌ

huwa **a**l-awwalu wa**a**l-*aa*khiru wa**al***zhzhaa*hiru wa**a**lb*aath*inu wahuwa bikulli syay-in 'aliim**un**

Dialah Yang Awal, Yang Akhir, Yang Zahir dan Yang Batin; dan Dia Maha Mengetahui segala sesuatu.

57:4

# هُوَ الَّذِيْ خَلَقَ السَّمٰوٰتِ وَالْاَرْضَ فِيْ سِتَّةِ اَيَّامٍ ثُمَّ اسْتَوٰى عَلَى الْعَرْشِۚ يَعْلَمُ مَا يَلِجُ فِى الْاَرْضِ وَمَا يَخْرُجُ مِنْهَا وَمَا يَنْزِلُ مِنَ السَّمَاۤءِ وَمَا يَعْرُجُ فِيْهَاۗ وَهُوَ مَعَكُمْ اَيْنَ مَا كُنْتُمْۗ وَاللّ

huwa **al**la*dz*ii khalaqa **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*a fii sittati ayy*aa*min tsumma istaw*aa* 'al*aa* **a**l'arsyi ya'lamu m*aa* yaliju fii **a<**

Dialah yang menciptakan langit dan bumi dalam enam masa; kemudian Dia bersemayam di atas ‘Arsy. Dia mengetahui apa yang masuk ke dalam bumi dan apa yang keluar dari dalamnya, apa yang turun dari langit dan apa yang naik ke sana. Dan Dia bersama kamu di ma

57:5

# لَهٗ مُلْكُ السَّمٰوٰتِ وَالْاَرْضِۗ وَاِلَى اللّٰهِ تُرْجَعُ الْاُمُوْرُ

lahu mulku **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i wa-il*aa* **al**l*aa*hi turja'u **a**l-umuur**u**

Milik-Nyalah kerajaan langit dan bumi. Dan hanya kepada Allah segala urusan dikembalikan.

57:6

# يُوْلِجُ الَّيْلَ فِى النَّهَارِ وَيُوْلِجُ النَّهَارَ فِى الَّيْلِۗ وَهُوَ عَلِيْمٌ ۢبِذَاتِ الصُّدُوْرِ

yuuliju **al**layla fii **al**nnah*aa*ri wayuuliju **al**nnah*aa*ra fii **al**layi wahuwa 'aliimun bi*dzaa*ti **al***shsh*uduur**i**

Dia memasukkan malam ke dalam siang dan memasukkan siang ke dalam malam. Dan Dia Maha Mengetahui segala isi hati.

57:7

# اٰمِنُوْا بِاللّٰهِ وَرَسُوْلِهٖ وَاَنْفِقُوْا مِمَّا جَعَلَكُمْ مُّسْتَخْلَفِيْنَ فِيْهِۗ فَالَّذِيْنَ اٰمَنُوْا مِنْكُمْ وَاَنْفَقُوْا لَهُمْ اَجْرٌ كَبِيْرٌ

*aa*minuu bi**al**l*aa*hi warasuulihi wa-anfiquu mimm*aa* ja'alakum mustakhlafiina fiihi fa**a**lla*dz*iina *aa*manuu minkum wa-anfaquu lahum ajrun kabiir**un**

Berimanlah kamu kepada Allah dan Rasul-Nya dan infakkanlah (di jalan Allah) sebagian dari harta yang Dia telah menjadikan kamu sebagai penguasanya (amanah). Maka orang-orang yang beriman di antara kamu dan menginfakkan (hartanya di jalan Allah) memperoleh

57:8

# وَمَا لَكُمْ لَا تُؤْمِنُوْنَ بِاللّٰهِ ۚوَالرَّسُوْلُ يَدْعُوْكُمْ لِتُؤْمِنُوْا بِرَبِّكُمْ وَقَدْ اَخَذَ مِيْثَاقَكُمْ اِنْ كُنْتُمْ مُّؤْمِنِيْنَ

wam*aa* lakum l*aa* tu/minuuna bi**al**l*aa*hi wa**al**rrasuulu yad'uukum litu/minuu birabbikum waqad akha*dz*a miits*aa*qakum in kuntum mu/miniin**a**

Dan mengapa kamu tidak beriman kepada Allah, padahal Rasul mengajak kamu beriman kepada Tuhanmu? Dan Dia telah mengambil janji (setia)mu, jika kamu orang-orang mukmin.

57:9

# هُوَ الَّذِيْ يُنَزِّلُ عَلٰى عَبْدِهٖٓ اٰيٰتٍۢ بَيِّنٰتٍ لِّيُخْرِجَكُمْ مِّنَ الظُّلُمٰتِ اِلَى النُّوْرِۗ وَاِنَّ اللّٰهَ بِكُمْ لَرَءُوْفٌ رَّحِيْمٌ

huwa **al**la*dz*ii yunazzilu 'al*aa* 'abdihi *aa*y*aa*tin bayyin*aa*tin liyukhrijakum mina **al***zhzh*ulum*aa*ti il*aa* **al**nnuuri wa-inna **al**l*aa*ha b

Dialah yang menurunkan ayat-ayat yang terang (Al-Qur'an) kepada hamba-Nya (Muhammad) untuk mengeluarkan kamu dari kegelapan kepada cahaya. Dan sungguh, terhadap kamu Allah Maha Penyantun, Maha Penyayang.

57:10

# وَمَا لَكُمْ اَلَّا تُنْفِقُوْا فِيْ سَبِيْلِ اللّٰهِ وَلِلّٰهِ مِيْرَاثُ السَّمٰوٰتِ وَالْاَرْضِۗ لَا يَسْتَوِيْ مِنْكُمْ مَّنْ اَنْفَقَ مِنْ قَبْلِ الْفَتْحِ وَقَاتَلَۗ اُولٰۤىِٕكَ اَعْظَمُ دَرَجَةً مِّنَ الَّذِيْنَ اَنْفَقُوْا مِنْۢ بَعْدُ وَقَاتَلُو

wam*aa* lakum **al**l*aa* tunfiquu fii sabiili **al**l*aa*hi walill*aa*hi miir*aa*tsu **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i l*aa* yastawii minkum man anfaqa

Dan mengapa kamu tidak menginfakkan hartamu di jalan Allah, padahal milik Allah semua pusaka langit dan bumi? Tidak sama orang yang menginfakkan (hartanya di jalan Allah) di antara kamu dan berperang sebelum penaklukan (Mekah). Mereka lebih tinggi derajat

57:11

# مَنْ ذَا الَّذِيْ يُقْرِضُ اللّٰهَ قَرْضًا حَسَنًا فَيُضٰعِفَهٗ لَهٗ وَلَهٗٓ اَجْرٌ كَرِيْمٌ

man *dzaa* **al**la*dz*ii yuqri*dh*u **al**l*aa*ha qar*dh*an *h*asanan fayu*daa*'ifahu lahu walahu ajrun kariim**un**

Barangsiapa meminjamkan kepada Allah dengan pinjaman yang baik, maka Allah akan mengembalikannya berlipat ganda untuknya, dan baginya pahala yang mulia,

57:12

# يَوْمَ تَرَى الْمُؤْمِنِيْنَ وَالْمُؤْمِنٰتِ يَسْعٰى نُوْرُهُمْ بَيْنَ اَيْدِيْهِمْ وَبِاَيْمَانِهِمْ بُشْرٰىكُمُ الْيَوْمَ جَنّٰتٌ تَجْرِيْ مِنْ تَحْتِهَا الْاَنْهٰرُ خٰلِدِيْنَ فِيْهَاۗ ذٰلِكَ هُوَ الْفَوْزُ الْعَظِيْمُۚ

yawma tar*aa* **a**lmu/miniina wa**a**lmu/min*aa*ti yas'*aa* nuuruhum bayna aydiihim wabi-aym*aa*nihim busyr*aa*kumu **a**lyawma jann*aa*tun tajrii min ta*h*tih*aa* **a**

**pada hari engkau akan melihat orang-orang yang beriman laki-laki dan perempuan, betapa cahaya mereka bersinar di depan dan di samping kanan mereka, (dikatakan kepada mereka), “Pada hari ini ada berita gembira untukmu, (yaitu) surga-surga yang mengalir di**

57:13

# يَوْمَ يَقُوْلُ الْمُنٰفِقُوْنَ وَالْمُنٰفِقٰتُ لِلَّذِيْنَ اٰمَنُوا انْظُرُوْنَا نَقْتَبِسْ مِنْ نُّوْرِكُمْۚ قِيْلَ ارْجِعُوْا وَرَاۤءَكُمْ فَالْتَمِسُوْا نُوْرًاۗ فَضُرِبَ بَيْنَهُمْ بِسُوْرٍ لَّهٗ بَابٌۗ بَاطِنُهٗ فِيْهِ الرَّحْمَةُ وَظَاهِرُهٗ مِنْ ق

yawma yaquulu **a**lmun*aa*fiquuna wa**a**lmun*aa*fiq*aa*tu lilla*dz*iina *aa*manuu un*zh*uruun*aa* naqtabis min nuurikum qiila irji'uu war*aa*-akum fa**i**ltamisuu nuuran fa

Pada hari orang-orang munafik laki-laki dan perempuan berkata kepada orang-orang yang beriman, “Tunggulah kami! Kami ingin mengambil cahayamu.” (Kepada mereka) dikatakan, ”Kembalilah kamu ke belakang dan carilah sendiri cahaya (untukmu).” Lalu di antara

57:14

# يُنَادُوْنَهُمْ اَلَمْ نَكُنْ مَّعَكُمْۗ قَالُوْا بَلٰى وَلٰكِنَّكُمْ فَتَنْتُمْ اَنْفُسَكُمْ وَتَرَبَّصْتُمْ وَارْتَبْتُمْ وَغَرَّتْكُمُ الْاَمَانِيُّ حَتّٰى جَاۤءَ اَمْرُ اللّٰهِ وَغَرَّكُمْ بِاللّٰهِ الْغَرُوْرُ

yun*aa*duunahum alam nakun ma'akum q*aa*luu bal*aa* wal*aa*kinnakum fatantum anfusakum watarabba*sh*tum wa**i**rtabtum wagharratkumu al-am*aa*niyyu *h*att*aa* j*aa*-a amru **al**l*aa*

Orang-orang munafik memanggil orang-orang mukmin, “Bukankah kami dahulu bersama kamu?” Mereka menjawab, “Benar, tetapi kamu mencelakakan dirimu sendiri, dan hanya menunggu, meragukan (janji Allah) dan ditipu oleh angan-angan kosong sampai datang ketetapan

57:15

# فَالْيَوْمَ لَا يُؤْخَذُ مِنْكُمْ فِدْيَةٌ وَّلَا مِنَ الَّذِيْنَ كَفَرُوْاۗ مَأْوٰىكُمُ النَّارُۗ هِيَ مَوْلٰىكُمْۗ وَبِئْسَ الْمَصِيْرُ

fa**a**lyawma l*aa* yu/kha*dz*u minkum fidyatun wal*aa* mina **al**la*dz*iina kafaruu ma/w*aa*kumu **al**nn*aa*ru hiya mawl*aa*kum wabi/sa **a**lma*sh*iir**u**

Maka pada hari ini tidak akan diterima tebusan dari kamu maupun dari orang-orang kafir. Tempat kamu di neraka. Itulah tempat berlindungmu, dan itulah seburuk-buruk tempat kembali.”

57:16

# اَلَمْ يَأْنِ لِلَّذِيْنَ اٰمَنُوْٓا اَنْ تَخْشَعَ قُلُوْبُهُمْ لِذِكْرِ اللّٰهِ وَمَا نَزَلَ مِنَ الْحَقِّۙ وَلَا يَكُوْنُوْا كَالَّذِيْنَ اُوْتُوا الْكِتٰبَ مِنْ قَبْلُ فَطَالَ عَلَيْهِمُ الْاَمَدُ فَقَسَتْ قُلُوْبُهُمْۗ وَكَثِيْرٌ مِّنْهُمْ فٰسِقُوْنَ

alam ya/ni lilla*dz*iina *aa*manuu an takhsya'a quluubuhum li*dz*ikri **al**l*aa*hi wam*aa* nazala mina **a**l*h*aqqi wal*aa* yakuunuu ka**a**lla*dz*iina uutuu **a**

**Belum tibakah waktunya bagi orang-orang yang beriman, untuk secara khusyuk mengingat Allah dan mematuhi kebenaran yang telah diwahyukan (kepada mereka), dan janganlah mereka (berlaku) seperti orang-orang yang telah menerima kitab sebelum itu, kemudian mer**

57:17

# اِعْلَمُوْٓا اَنَّ اللّٰهَ يُحْيِ الْاَرْضَ بَعْدَ مَوْتِهَاۗ قَدْ بَيَّنَّا لَكُمُ الْاٰيٰتِ لَعَلَّكُمْ تَعْقِلُوْنَ

i'lamuu anna **al**l*aa*ha yu*h*yii **a**l-ar*dh*a ba'da mawtih*aa* qad bayyann*aa* lakumu **a**l-*aa*y*aa*ti la'allakum ta'qiluun**a**

Ketahuilah bahwa Allah yang menghidupkan bumi setelah matinya (kering). Sungguh, telah Kami jelaskan kepadamu tanda-tanda (kebesaran Kami) agar kamu mengerti.

57:18

# اِنَّ الْمُصَّدِّقِيْنَ وَالْمُصَّدِّقٰتِ وَاَقْرَضُوا اللّٰهَ قَرْضًا حَسَنًا يُّضٰعَفُ لَهُمْ وَلَهُمْ اَجْرٌ كَرِيْمٌ

inna **a**lmu*shsh*addiqiina wa**a**lmu*shsh*addiq*aa*ti wa-aqra*dh*uu **al**l*aa*ha qar*dh*an *h*asanan yu*daa*'afu lahum walahum ajrun kariim**un**

Sesungguhnya orang-orang yang bersedekah baik laki-laki maupun perempuan dan meminjamkan kepada Allah dengan pinjaman yang baik, akan dilipatgandakan (balasannya) bagi mereka; dan mereka akan mendapat pahala yang mulia.

57:19

# وَالَّذِيْنَ اٰمَنُوْا بِاللّٰهِ وَرُسُلِهٖٓ اُولٰۤىِٕكَ هُمُ الصِّدِّيْقُوْنَ ۖوَالشُّهَدَاۤءُ عِنْدَ رَبِّهِمْۗ لَهُمْ اَجْرُهُمْ وَنُوْرُهُمْۗ وَالَّذِيْنَ كَفَرُوْا وَكَذَّبُوْا بِاٰيٰتِنَآ اُولٰۤىِٕكَ اَصْحٰبُ الْجَحِيْمِ ࣖ

wa**a**lla*dz*iina *aa*manuu bi**al**l*aa*hi warusulihi ul*aa*-ika humu **al***shsh*iddiiquuna wa**al**sysyuhad*aa*u 'inda rabbihim lahum ajruhum wanuuruhum wa**a**

**Dan orang-orang yang beriman kepada Allah dan rasul-rasul-Nya, mereka itu orang-orang yang tulus hati (pencinta kebenaran) dan saksi-saksi di sisi Tuhan mereka. Mereka berhak mendapat pahala dan cahaya. Tetapi orang-orang yang kafir dan mendustakan ayat-a**

57:20

# اِعْلَمُوْٓا اَنَّمَا الْحَيٰوةُ الدُّنْيَا لَعِبٌ وَّلَهْوٌ وَّزِيْنَةٌ وَّتَفَاخُرٌۢ بَيْنَكُمْ وَتَكَاثُرٌ فِى الْاَمْوَالِ وَالْاَوْلَادِۗ كَمَثَلِ غَيْثٍ اَعْجَبَ الْكُفَّارَ نَبَاتُهٗ ثُمَّ يَهِيْجُ فَتَرٰىهُ مُصْفَرًّا ثُمَّ يَكُوْنُ حُطَامًاۗ وَف

i'lamuu annam*aa* **a**l*h*ay*aa*tu **al**dduny*aa* la'ibun walahwun waziinatun wataf*aa*khurun baynakum watak*aa*tsurun fii **a**l-amw*aa*li wa**a**l-awl*aa*di ka

Ketahuilah, sesungguhnya kehidupan dunia itu hanyalah permainan dan sendagurauan, perhiasan dan saling berbangga di antara kamu serta berlomba dalam kekayaan dan anak keturunan, seperti hujan yang tanam-tanamannya mengagumkan para petani; kemudian (tanama

57:21

# سَابِقُوْٓا اِلٰى مَغْفِرَةٍ مِّنْ رَّبِّكُمْ وَجَنَّةٍ عَرْضُهَا كَعَرْضِ السَّمَاۤءِ وَالْاَرْضِۙ اُعِدَّتْ لِلَّذِيْنَ اٰمَنُوْا بِاللّٰهِ وَرُسُلِهٖۗ ذٰلِكَ فَضْلُ اللّٰهِ يُؤْتِيْهِ مَنْ يَّشَاۤءُ ۚوَاللّٰهُ ذُو الْفَضْلِ الْعَظِيْمِ

s*aa*biquu il*aa* maghfiratin min rabbikum wajannatin 'ar*dh*uh*aa* ka'ar*dh*i **al**ssam*aa*-i wa**a**l-ar*dh*i u'iddat lilla*dz*iina *aa*manuu bi**al**l*aa*hi warus

Berlomba-lombalah kamu untuk mendapatkan ampunan dari Tuhanmu dan surga yang luasnya seluas langit dan bumi, yang disediakan bagi orang-orang yang beriman kepada Allah dan rasul-rasul-Nya. Itulah karunia Allah, yang diberikan kepada siapa yang Dia kehenda

57:22

# مَآ اَصَابَ مِنْ مُّصِيْبَةٍ فِى الْاَرْضِ وَلَا فِيْٓ اَنْفُسِكُمْ اِلَّا فِيْ كِتٰبٍ مِّنْ قَبْلِ اَنْ نَّبْرَاَهَا ۗاِنَّ ذٰلِكَ عَلَى اللّٰهِ يَسِيْرٌۖ

m*aa* a*shaa*ba min mu*sh*iibatin fii **a**l-ar*dh*i wal*aa* fii anfusikum ill*aa* fii kit*aa*bin min qabli an nabra-ah*aa* inna *dzaa*lika 'al*aa* **al**l*aa*hi yasiir

Setiap bencana yang menimpa di bumi dan yang menimpa dirimu sendiri, semuanya telah tertulis dalam Kitab (Lauh Mahfuzh) sebelum Kami mewujudkannya. Sungguh, yang demikian itu mudah bagi Allah.

57:23

# لِّكَيْلَا تَأْسَوْا عَلٰى مَا فَاتَكُمْ وَلَا تَفْرَحُوْا بِمَآ اٰتٰىكُمْ ۗوَاللّٰهُ لَا يُحِبُّ كُلَّ مُخْتَالٍ فَخُوْرٍۙ

likayl*aa* ta/saw 'al*aa* m*aa* f*aa*takum wal*aa* tafra*h*uu bim*aa* *aa*t*aa*kum wa**al**l*aa*hu l*aa* yu*h*ibbu kulla mukht*aa*lin fakhuur**in**

Agar kamu tidak bersedih hati terhadap apa yang luput dari kamu, dan jangan pula terlalu gembira terhadap apa yang diberikan-Nya kepadamu. Dan Allah tidak menyukai setiap orang yang sombong dan membanggakan diri,

57:24

# ۨالَّذِيْنَ يَبْخَلُوْنَ وَيَأْمُرُوْنَ النَّاسَ بِالْبُخْلِ ۗوَمَنْ يَّتَوَلَّ فَاِنَّ اللّٰهَ هُوَ الْغَنِيُّ الْحَمِيْدُ

**al**la*dz*iina yabkhaluuna waya/muruuna **al**nn*aa*sa bi**a**lbukhli waman yatawalla fa-inna **al**l*aa*ha huwa **a**lghaniyyu **a**l*h*amiid**u**

**yaitu orang-orang yang kikir dan menyuruh orang lain berbuat kikir. Barangsiapa berpaling (dari perintah-perintah Allah), maka sesungguhnya Allah, Dia Mahakaya, Maha Terpuji.**

57:25

# لَقَدْ اَرْسَلْنَا رُسُلَنَا بِالْبَيِّنٰتِ وَاَنْزَلْنَا مَعَهُمُ الْكِتٰبَ وَالْمِيْزَانَ لِيَقُوْمَ النَّاسُ بِالْقِسْطِۚ وَاَنْزَلْنَا الْحَدِيْدَ فِيْهِ بَأْسٌ شَدِيْدٌ وَّمَنَافِعُ لِلنَّاسِ وَلِيَعْلَمَ اللّٰهُ مَنْ يَّنْصُرُهٗ وَرُسُلَهٗ بِالْغَيْ

laqad arsaln*aa* rusulan*aa* bi**a**lbayyin*aa*ti wa-anzaln*aa* ma'ahumu **a**lkit*aa*ba wa**a**lmiiz*aa*na liyaquuma **al**nn*aa*su bi**a**lqis*th*i

Sungguh, Kami telah mengutus rasul-rasul Kami dengan bukti-bukti yang nyata dan kami turunkan bersama mereka kitab dan neraca (keadilan) agar manusia dapat berlaku adil. Dan Kami menciptakan besi yang mempunyai kekuatan, hebat dan banyak manfaat bagi manu

57:26

# وَلَقَدْ اَرْسَلْنَا نُوْحًا وَّاِبْرٰهِيْمَ وَجَعَلْنَا فِيْ ذُرِّيَّتِهِمَا النُّبُوَّةَ وَالْكِتٰبَ فَمِنْهُمْ مُّهْتَدٍۚ وَكَثِيْرٌ مِّنْهُمْ فٰسِقُوْنَ

walaqad arsaln*aa* nuu*h*an wa-ibr*aa*hiima waja'aln*aa* fii *dz*urriyyatihim*aa* **al**nnubuwwata wa**a**lkit*aa*ba faminhum muhtadin wakatsiirun minhum f*aa*siquun**a**

Dan sungguh, Kami telah mengutus Nuh dan Ibrahim dan Kami berikan kenabian dan kitab (wahyu) kepada keturunan keduanya, di antara mereka ada yang menerima petunjuk dan banyak di antara mereka yang fasik.

57:27

# ثُمَّ قَفَّيْنَا عَلٰٓى اٰثَارِهِمْ بِرُسُلِنَا وَقَفَّيْنَا بِعِيْسَى ابْنِ مَرْيَمَ وَاٰتَيْنٰهُ الْاِنْجِيْلَ ەۙ وَجَعَلْنَا فِيْ قُلُوْبِ الَّذِيْنَ اتَّبَعُوْهُ رَأْفَةً وَّرَحْمَةً ۗوَرَهْبَانِيَّةَ ِۨابْتَدَعُوْهَا مَا كَتَبْنٰهَا عَلَيْهِمْ اِلَّ

tsumma qaffayn*aa* 'al*aa* *aa*ts*aa*rihim birusulin*aa* waqaffayn*aa* bi'iis*aa* ibni maryama wa*aa*tayn*aa*hu **a**l-injiila waja'aln*aa* fii quluubi **al**la*dz*iina ittab

Kemudian Kami susulkan rasul-rasul Kami mengikuti jejak mereka dan Kami susulkan (pula) Isa putra Maryam; Dan Kami berikan Injil kepadanya dan Kami jadikan rasa santun dan kasih sayang dalam hati orang-orang yang mengikutinya. Mereka mengada-adakan rahban

57:28

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوا اتَّقُوا اللّٰهَ وَاٰمِنُوْا بِرَسُوْلِهٖ يُؤْتِكُمْ كِفْلَيْنِ مِنْ رَّحْمَتِهٖ وَيَجْعَلْ لَّكُمْ نُوْرًا تَمْشُوْنَ بِهٖ وَيَغْفِرْ لَكُمْۗ وَاللّٰهُ غَفُوْرٌ رَّحِيْمٌۙ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu ittaquu **al**l*aa*ha wa*aa*minuu birasuulihi yu/tikum kiflayni min ra*h*matihi wayaj'al lakum nuuran tamsyuuna bihi wayaghfir lakum wa**al**

**Wahai orang-orang yang beriman! Bertakwalah kepada Allah dan berimanlah kepada Rasul-Nya (Muhammad), niscaya Allah memberikan rahmat-Nya kepadamu dua bagian, dan menjadikan cahaya untukmu yang dengan cahaya itu kamu dapat berjalan serta Dia mengampuni kam**

57:29

# لِّئَلَّا يَعْلَمَ اَهْلُ الْكِتٰبِ اَلَّا يَقْدِرُوْنَ عَلٰى شَيْءٍ مِّنْ فَضْلِ اللّٰهِ وَاَنَّ الْفَضْلَ بِيَدِ اللّٰهِ يُؤْتِيْهِ مَنْ يَّشَاۤءُ ۗوَاللّٰهُ ذُو الْفَضْلِ الْعَظِيْمِ ࣖ ۔

li\`allā ya’lama ahlul-kitābi allā yaqdirụna ‘alā syai\`im min faḍlillāhi wa annal-faḍla biyadillāhi yu\`tīhi may yasyā\`, wallāhu żul-faḍlil-‘aẓīm 29. (Kami terangkan yang demikian itu) supaya ahli Kitab mengetahui bahwa mereka tiada mendapat sedikitpun akan karunia Allah (jika mereka tidak beriman kepada Muhammad), dan bahwasanya karunia itu adalah di tangan Allah. Dia berikan karunia itu kepada siapa yang dikehendaki-Nya. Dan Allah mempunyai karunia yang besar.

<!--EndFragment-->