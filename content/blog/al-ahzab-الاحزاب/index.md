---
title: (33) Al-Ahzab - الاحزاب
date: 2021-10-27T03:55:49.280Z
ayat: 33
description: "Jumlah Ayat: 73 / Arti: Golongan Yang Bersekutu"
---
<!--StartFragment-->

33:1

# يٰٓاَيُّهَا النَّبِيُّ اتَّقِ اللّٰهَ وَلَا تُطِعِ الْكٰفِرِيْنَ وَالْمُنٰفِقِيْنَ ۗاِنَّ اللّٰهَ كَانَ عَلِيْمًا حَكِيْمًاۙ

y*aa* ayyuh*aa* **al**nnabiyyu ittaqi **al**l*aa*ha wal*aa* tu*th*i'i **a**lk*aa*firiina wa**a**lmun*aa*fiqiina inna **al**l*aa*ha k*aa*na 'aliim

Wahai Nabi! Bertakwalah kepada Allah dan janganlah engkau menuruti (keinginan) orang-orang kafir dan orang-orang munafik. Sesungguhnya Allah Maha Mengetahui, Mahabijaksana,

33:2

# وَّاتَّبِعْ مَا يُوْحٰىٓ اِلَيْكَ مِنْ رَّبِّكَ ۗاِنَّ اللّٰهَ كَانَ بِمَا تَعْمَلُوْنَ خَبِيْرًاۙ

wa**i**ttabi' m*aa* yuu*haa* ilayka min rabbika inna **al**l*aa*ha k*aa*na bim*aa* ta'maluuna khabiir*aa***n**

dan ikutilah apa yang diwahyukan Tuhanmu kepadamu. Sungguh, Allah Mahateliti terhadap apa yang kamu kerjakan,

33:3

# وَّتَوَكَّلْ عَلَى اللّٰهِ ۗوَكَفٰى بِاللّٰهِ وَكِيْلًا

watawakkal 'al*aa* **al**l*aa*hi wakaf*aa* bi**al**l*aa*hi wakiil*aa***n**

dan bertawakallah kepada Allah. Dan cukuplah Allah sebagai pemelihara.

33:4

# مَا جَعَلَ اللّٰهُ لِرَجُلٍ مِّنْ قَلْبَيْنِ فِيْ جَوْفِهٖ ۚوَمَا جَعَلَ اَزْوَاجَكُمُ الّٰـِٕۤيْ تُظٰهِرُوْنَ مِنْهُنَّ اُمَّهٰتِكُمْ ۚوَمَا جَعَلَ اَدْعِيَاۤءَكُمْ اَبْنَاۤءَكُمْۗ ذٰلِكُمْ قَوْلُكُمْ بِاَفْوَاهِكُمْ ۗوَاللّٰهُ يَقُوْلُ الْحَقَّ وَهُوَ ي

m*aa* ja'ala **al**l*aa*hu lirajulin min qalbayni fii jawfihi wam*aa* ja'ala azw*aa*jakumu **al**l*aa*-ii tu*zhaa*hiruuna minhunna ummah*aa*tikum wam*aa* ja'ala ad'iy*aa*-akum abn*aa*

Allah tidak menjadikan bagi seseorang dua hati dalam rongganya; dan Dia tidak menjadikan istri-istrimu yang kamu zihar itu sebagai ibumu, dan Dia tidak menjadikan anak angkatmu sebagai anak kandungmu (sendiri). Yang demikian itu hanyalah perkataan di mulu







33:5

# اُدْعُوْهُمْ لِاٰبَاۤىِٕهِمْ هُوَ اَقْسَطُ عِنْدَ اللّٰهِ ۚ فَاِنْ لَّمْ تَعْلَمُوْٓا اٰبَاۤءَهُمْ فَاِخْوَانُكُمْ فِى الدِّيْنِ وَمَوَالِيْكُمْ ۗوَلَيْسَ عَلَيْكُمْ جُنَاحٌ فِيْمَآ اَخْطَأْتُمْ بِهٖ وَلٰكِنْ مَّا تَعَمَّدَتْ قُلُوْبُكُمْ ۗوَكَانَ اللّٰه

ud'uuhum li-*aa*b*aa*-ihim huwa aqsa*th*u 'inda **al**l*aa*hi fa-in lam ta'lamuu *aa*b*aa*-ahum fa-ikhw*aa*nukum fii **al**ddiini wamaw*aa*liikum walaysa 'alaykum jun*aah*un fiim*aa<*

Panggillah mereka (anak angkat itu) dengan (memakai) nama bapak-bapak mereka; itulah yang adil di sisi Allah, dan jika kamu tidak mengetahui bapak mereka, maka (panggillah mereka sebagai) saudara-saudaramu seagama dan maula-maulamu. Dan tidak ada dosa ata







33:6

# اَلنَّبِيُّ اَوْلٰى بِالْمُؤْمِنِيْنَ مِنْ اَنْفُسِهِمْ وَاَزْوَاجُهٗٓ اُمَّهٰتُهُمْ ۗوَاُولُوا الْاَرْحَامِ بَعْضُهُمْ اَوْلٰى بِبَعْضٍ فِيْ كِتٰبِ اللّٰهِ مِنَ الْمُؤْمِنِيْنَ وَالْمُهٰجِرِيْنَ اِلَّآ اَنْ تَفْعَلُوْٓا اِلٰٓى اَوْلِيَاۤىِٕكُمْ مَّعْرُو

a**l**nnabiyyu awl*aa* bi**a**lmu/miniina min anfusihim wa-azw*aa*juhu ummah*aa*tuhum wauluu **a**l-ar*haa*mi ba'*dh*uhum awl*aa* biba'*dh*in fii kit*aa*bi **al**l

Nabi itu lebih utama bagi orang-orang mukmin dibandingkan diri mereka sendiri dan istri-istrinya adalah ibu-ibu mereka. Orang-orang yang mempunyai hubungan darah satu sama lain lebih berhak (waris-mewarisi) di dalam Kitab Allah daripada orang-orang mukmin

33:7

# وَاِذْ اَخَذْنَا مِنَ النَّبِيّٖنَ مِيْثَاقَهُمْ وَمِنْكَ وَمِنْ نُّوْحٍ وَّاِبْرٰهِيْمَ وَمُوْسٰى وَعِيْسَى ابْنِ مَرْيَمَ ۖوَاَخَذْنَا مِنْهُمْ مِّيْثَاقًا غَلِيْظًاۙ

wa-i*dz* akha*dz*n*aa* mina **al**nnabiyyiina miits*aa*qahum waminka wamin nuu*h*in wa-ibr*aa*hiima wamuus*aa* wa'iis*aa* ibni maryama wa-akha*dz*n*aa* minhum miits*aa*qan ghalii*zhaa*

*Dan (ingatlah) ketika Kami mengambil perjanjian dari para nabi dan dari engkau (sendiri), dari Nuh, Ibrahim, Musa dan Isa putra Maryam, dan Kami telah mengambil dari mereka perjanjian yang teguh,*









33:8

# لِّيَسْـَٔلَ الصّٰدِقِيْنَ عَنْ صِدْقِهِمْ ۚوَاَعَدَّ لِلْكٰفِرِيْنَ عَذَابًا اَلِيْمًا ࣖ

liyas-ala **al***shshaa*diqiina 'an *sh*idqihim wa-a'adda lilk*aa*firiina 'a*dzaa*ban **a**liim*aa***n**

agar Dia menanyakan kepada orang-orang yang benar tentang kebenaran mereka. Dia menyediakan azab yang pedih bagi orang-orang kafir.

33:9

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوا اذْكُرُوْا نِعْمَةَ اللّٰهِ عَلَيْكُمْ اِذْ جَاۤءَتْكُمْ جُنُوْدٌ فَاَرْسَلْنَا عَلَيْهِمْ رِيْحًا وَّجُنُوْدًا لَّمْ تَرَوْهَا ۗوَكَانَ اللّٰهُ بِمَا تَعْمَلُوْنَ بَصِيْرًاۚ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu u*dz*kuruu ni'mata **al**l*aa*hi 'alaykum i*dz* j*aa*-atkum junuudun fa-arsaln*aa* 'alayhim rii*h*an wajunuudan lam tarawh*aa* wak<

Wahai orang-orang yang beriman! Ingatlah akan nikmat Allah (yang telah dikaruniakan) kepadamu ketika bala tentara datang kepadamu, lalu Kami kirimkan kepada mereka angin topan dan bala tentara yang tidak dapat terlihat olehmu. Allah Maha Melihat apa yang

33:10

# اِذْ جَاۤءُوْكُمْ مِّنْ فَوْقِكُمْ وَمِنْ اَسْفَلَ مِنْكُمْ وَاِذْ زَاغَتِ الْاَبْصَارُ وَبَلَغَتِ الْقُلُوْبُ الْحَنَاجِرَ وَتَظُنُّوْنَ بِاللّٰهِ الظُّنُوْنَا۠ ۗ

i*dz* j*aa*uukum min fawqikum wamin asfala minkum wa-i*dz* z*aa*ghati **a**l-ab*shaa*ru wabalaghati **a**lquluubu **a**l*h*an*aa*jira wata*zh*unnuuna bi**al**l*a*

(Yaitu) ketika mereka datang kepadamu dari atas dan dari bawahmu, dan ketika penglihatan(mu) terpana dan hatimu menyesak sampai ke tenggorokan dan kamu berprasangka yang bukan-bukan terhadap Allah.







33:11

# هُنَالِكَ ابْتُلِيَ الْمُؤْمِنُوْنَ وَزُلْزِلُوْا زِلْزَالًا شَدِيْدًا

hun*aa*lika ibtuliya **a**lmu-minuuna wazulziluu zilz*aa*lan syadiid*aa***n**

Di situlah diuji orang-orang mukmin dan digoncangkan (hatinya) dengan goncangan yang dahsyat.

33:12

# وَاِذْ يَقُوْلُ الْمُنٰفِقُوْنَ وَالَّذِيْنَ فِيْ قُلُوْبِهِمْ مَّرَضٌ مَّا وَعَدَنَا اللّٰهُ وَرَسُوْلُهٗٓ اِلَّا غُرُوْرًا

wa-i*dz* yaquulu **a**lmun*aa*fiquuna wa**a**lla*dz*iina fii quluubihim mara*dh*un m*aa* wa'adan*aa* **al**l*aa*hu warasuuluhu ill*aa* ghuruur*aa***n**

Dan (ingatlah) ketika orang-orang munafik dan orang-orang yang hatinya berpenyakit berkata, “Yang dijanjikan Allah dan Rasul-Nya kepada kami hanya tipu daya belaka.”

33:13

# وَاِذْ قَالَتْ طَّاۤىِٕفَةٌ مِّنْهُمْ يٰٓاَهْلَ يَثْرِبَ لَا مُقَامَ لَكُمْ فَارْجِعُوْا ۚوَيَسْتَأْذِنُ فَرِيْقٌ مِّنْهُمُ النَّبِيَّ يَقُوْلُوْنَ اِنَّ بُيُوْتَنَا عَوْرَةٌ ۗوَمَا هِيَ بِعَوْرَةٍ ۗاِنْ يُّرِيْدُوْنَ اِلَّا فِرَارًا

wa-i*dz* q*aa*lat *thaa*-ifatun minhum y*aa* ahla yatsriba l*aa* muq*aa*ma lakum fa**i**rji'uu wayasta/*dz*inu fariiqun minhumu **al**nnabiyya yaquuluuna inna buyuutan*aa* 'awratun w

Dan (ingatlah) ketika segolongan di antara mereka berkata, “Wahai penduduk Yasrib (Madinah)! Tidak ada tempat bagimu, maka kembalilah kamu.” Dan sebagian dari mereka meminta izin kepada Nabi (untuk kembali pulang) dengan berkata, “Sesungguhnya rumah-rumah

33:14

# وَلَوْ دُخِلَتْ عَلَيْهِمْ مِّنْ اَقْطَارِهَا ثُمَّ سُـِٕلُوا الْفِتْنَةَ لَاٰتَوْهَا وَمَا تَلَبَّثُوْا بِهَآ اِلَّا يَسِيْرًا

walaw dukhilat 'alayhim min aq*thaa*rih*aa* tsumma su-iluu **a**lfitnata la*aa*tawh*aa* wam*aa* talabbatsuu bih*aa* ill*aa* yasiir*aa***n**

Dan kalau (Yasrib) diserang dari segala penjuru, dan mereka diminta agar membuat kekacauan, niscaya mereka mengerjakannya; dan hanya sebentar saja mereka menunggu.

33:15

# وَلَقَدْ كَانُوْا عَاهَدُوا اللّٰهَ مِنْ قَبْلُ لَا يُوَلُّوْنَ الْاَدْبَارَ ۗوَكَانَ عَهْدُ اللّٰهِ مَسْـُٔوْلًا

walaqad k*aa*nuu '*aa*haduu **al**l*aa*ha min qablu l*aa* yuwalluuna **a**l-adb*aa*ra wak*aa*na 'ahdu **al**l*aa*hi mas-uul*aa***n**

Dan sungguh, mereka sebelum itu telah berjanji kepada Allah, tidak akan berbalik ke belakang (mundur). Dan perjanjian dengan Allah akan diminta pertanggungjawabannya.

33:16

# قُلْ لَّنْ يَّنْفَعَكُمُ الْفِرَارُ اِنْ فَرَرْتُمْ مِّنَ الْمَوْتِ اَوِ الْقَتْلِ وَاِذًا لَّا تُمَتَّعُوْنَ اِلَّا قَلِيْلًا

qul lan yanfa'akumu **a**lfir*aa*ru in farartum mina **a**lmawti awi **a**lqatli wa-i*dz*an l*aa* tumatta'uuna ill*aa* qaliil*aa***n**

Katakanlah (Muhammad), “Lari tidaklah berguna bagimu, jika kamu melarikan diri dari kematian atau pembunuhan, dan jika demikian (kamu terhindar dari kematian) kamu hanya akan mengecap kesenangan sebentar saja.”

33:17

# قُلْ مَنْ ذَا الَّذِيْ يَعْصِمُكُمْ مِّنَ اللّٰهِ اِنْ اَرَادَ بِكُمْ سُوْۤءًا اَوْ اَرَادَ بِكُمْ رَحْمَةً ۗوَلَا يَجِدُوْنَ لَهُمْ مِّنْ دُوْنِ اللّٰهِ وَلِيًّا وَّلَا نَصِيْرًا

qul man *dzaa* **al**la*dz*ii ya'*sh*imukum mina **al**l*aa*hi in ar*aa*da bikum suu-an aw ar*aa*da bikum ra*h*matan wal*aa* yajiduuna lahum min duuni **al**l*aa*hi waliyy

Katakanlah, “Siapakah yang dapat melindungi kamu dari (ketentuan) Allah jika Dia menghendaki bencana atasmu atau menghendaki rahmat untuk dirimu?” Mereka itu tidak akan mendapatkan pelindung dan penolong selain Allah.

33:18

# ۞ قَدْ يَعْلَمُ اللّٰهُ الْمُعَوِّقِيْنَ مِنْكُمْ وَالْقَاۤىِٕلِيْنَ لِاِخْوَانِهِمْ هَلُمَّ اِلَيْنَا ۚوَلَا يَأْتُوْنَ الْبَأْسَ اِلَّا قَلِيْلًاۙ

qad ya'lamu **al**l*aa*hu **a**lmu'awwiqiina minkum wa**a**lq*aa*-iliina li-ikhw*aa*nihim halumma ilayn*aa* wal*aa* ya/tuuna **a**lba/sa ill*aa* qaliil*aa***n**

**Sungguh, Allah mengetahui orang-orang yang menghalang-halangi di antara kamu dan orang yang berkata kepada saudara-saudaranya, “Marilah bersama kami.” Tetapi mereka datang berperang hanya sebentar,**









33:19

# اَشِحَّةً عَلَيْكُمْ ۖ فَاِذَا جَاۤءَ الْخَوْفُ رَاَيْتَهُمْ يَنْظُرُوْنَ اِلَيْكَ تَدُوْرُ اَعْيُنُهُمْ كَالَّذِيْ يُغْشٰى عَلَيْهِ مِنَ الْمَوْتِۚ فَاِذَا ذَهَبَ الْخَوْفُ سَلَقُوْكُمْ بِاَلْسِنَةٍ حِدَادٍ اَشِحَّةً عَلَى الْخَيْرِۗ اُولٰۤىِٕكَ لَمْ يُؤ

asyi*hh*atan 'alaykum fa-i*dzaa* j*aa*-a **a**lkhawfu ra-aytahum yan*zh*uruuna ilayka taduuru a'yunuhum ka**a**lla*dz*ii yughsy*aa* 'alayhi mina **a**lmawti fa-i*dzaa* *dz*ahab

mereka kikir terhadapmu. Apabila datang ketakutan (bahaya), kamu lihat mereka itu memandang kepadamu dengan mata yang terbalik-balik seperti orang yang pingsan karena akan mati, dan apabila ketakutan telah hilang, mereka mencaci kamu dengan lidah yang taj

33:20

# يَحْسَبُوْنَ الْاَحْزَابَ لَمْ يَذْهَبُوْا ۚوَاِنْ يَّأْتِ الْاَحْزَابُ يَوَدُّوْا لَوْ اَنَّهُمْ بَادُوْنَ فِى الْاَعْرَابِ يَسْاَلُوْنَ عَنْ اَنْۢبَاۤىِٕكُمْ ۖوَلَوْ كَانُوْا فِيْكُمْ مَّا قٰتَلُوْٓا اِلَّا قَلِيْلًا ࣖ

ya*h*sabuuna **a**l-a*h*z*aa*ba lam ya*dz*habuu wa-in ya/ti **a**l-a*h*z*aa*bu yawadduu law annahum b*aa*duuna fii **a**l-a'r*aa*bi yas-aluuna 'an anb*aa*-ikum walaw k*a*

Mereka mengira (bahwa) golongan-golongan (yang bersekutu) itu belum pergi, dan jika golongan-golongan (yang bersekutu) itu datang kembali, niscaya mereka ingin berada di dusun-dusun bersama-sama orang Arab Badui, sambil menanyakan berita tentang kamu. Dan







33:21

# لَقَدْ كَانَ لَكُمْ فِيْ رَسُوْلِ اللّٰهِ اُسْوَةٌ حَسَنَةٌ لِّمَنْ كَانَ يَرْجُوا اللّٰهَ وَالْيَوْمَ الْاٰخِرَ وَذَكَرَ اللّٰهَ كَثِيْرًاۗ

laqad k*aa*na lakum fii rasuuli **al**l*aa*hi uswatun *h*asanatun liman k*aa*na yarjuu **al**l*aa*ha wa**a**lyawma **a**l-*aa*khira wa*dz*akara **al**l*a*

Sungguh, telah ada pada (diri) Rasulullah itu suri teladan yang baik bagimu (yaitu) bagi orang yang mengharap (rahmat) Allah dan (kedatangan) hari Kiamat dan yang banyak mengingat Allah.







33:22

# وَلَمَّا رَاَ الْمُؤْمِنُوْنَ الْاَحْزَابَۙ قَالُوْا هٰذَا مَا وَعَدَنَا اللّٰهُ وَرَسُوْلُهٗ وَصَدَقَ اللّٰهُ وَرَسُوْلُهٗ ۖوَمَا زَادَهُمْ اِلَّآ اِيْمَانًا وَّتَسْلِيْمًاۗ

walamm*aa* ra*aa* **a**lmu/minuuna **a**l-a*h*z*aa*ba q*aa*luu h*aadzaa* m*aa* wa'adan*aa* **al**l*aa*hu warasuuluhu wa*sh*adaqa **al**l*aa*hu waras

Dan ketika orang-orang mukmin melihat golongan-golongan (yang bersekutu) itu, mereka berkata, “Inilah yang dijanjikan Allah dan Rasul-Nya kepada kita.” Dan benarlah Allah dan Rasul-Nya. Dan yang demikian itu menambah keimanan dan keislaman mereka.

33:23

# مِنَ الْمُؤْمِنِيْنَ رِجَالٌ صَدَقُوْا مَا عَاهَدُوا اللّٰهَ عَلَيْهِ ۚ فَمِنْهُمْ مَّنْ قَضٰى نَحْبَهٗۙ وَمِنْهُمْ مَّنْ يَّنْتَظِرُ ۖوَمَا بَدَّلُوْا تَبْدِيْلًاۙ

mina **a**lmu/miniina rij*aa*lun *sh*adaquu m*aa* '*aa*haduu **al**l*aa*ha 'alayhi faminhum man qa*daa* na*h*bahu waminhum man yanta*zh*iru wam*aa* baddaluu tabdiil*aa***n**

**Di antara orang-orang mukmin itu ada orang-orang yang menepati apa yang telah mereka janjikan kepada Allah. Dan di antara mereka ada yang gugur, dan di antara mereka ada (pula) yang menunggu-nunggu dan mereka sedikit pun tidak mengubah (janjinya),**









33:24

# لِيَجْزِيَ اللّٰهُ الصّٰدِقِيْنَ بِصِدْقِهِمْ وَيُعَذِّبَ الْمُنٰفِقِيْنَ اِنْ شَاۤءَ اَوْ يَتُوْبَ عَلَيْهِمْ ۗاِنَّ اللّٰهَ كَانَ غَفُوْرًا رَّحِيْمًاۚ

liyajziya **al**l*aa*hu **al***shshaa*diqiina bi*sh*idqihim wayu'a*dzdz*iba **a**lmun*aa*fiqiina in sy*aa*-a aw yatuuba 'alayhim inna **al**l*aa*ha k*aa*na ghafuur

agar Allah memberikan balasan kepada orang-orang yang benar itu karena kebenarannya, dan mengazab orang munafik jika Dia kehendaki, atau menerima tobat mereka. Sungguh, Allah Maha Pengampun, Maha Penyayang.

33:25

# وَرَدَّ اللّٰهُ الَّذِيْنَ كَفَرُوْا بِغَيْظِهِمْ لَمْ يَنَالُوْا خَيْرًا ۗوَكَفَى اللّٰهُ الْمُؤْمِنِيْنَ الْقِتَالَ ۗوَكَانَ اللّٰهُ قَوِيًّا عَزِيْزًاۚ

waradda **al**l*aa*hu **al**la*dz*iina kafaruu bighay*zh*ihim lam yan*aa*luu khayran wakaf*aa* **al**l*aa*hu **a**lmu/miniina **a**lqit*aa*la wak*aa*n

Dan Allah menghalau orang-orang kafir itu yang keadaan mereka penuh kejengkelan, karena mereka (juga) tidak memperoleh keuntungan apa pun. Cukuplah Allah (yang menolong) menghindarkan orang-orang mukmin dalam peperangan. Dan Allah Mahakuat, Mahaperkasa.

33:26

# وَاَنْزَلَ الَّذِيْنَ ظَاهَرُوْهُمْ مِّنْ اَهْلِ الْكِتٰبِ مِنْ صَيَاصِيْهِمْ وَقَذَفَ فِيْ قُلُوْبِهِمُ الرُّعْبَ فَرِيْقًا تَقْتُلُوْنَ وَتَأْسِرُوْنَ فَرِيْقًاۚ

wa-anzala **al**la*dz*iina *zhaa*haruuhum min ahli **a**lkit*aa*bi min *sh*ay*aas*iihim waqa*dz*afa fii quluubihimu **al**rru'ba fariiqan taqtuluuna wata/siruuna fariiq*aa***n**

Dan Dia menurunkan orang-orang Ahli Kitab (Bani Quraizah) yang membantu mereka (golongan-golongan yang bersekutu) dari benteng-benteng mereka, dan Dia memasukkan rasa takut ke dalam hati mereka. Sebagian mereka kamu bunuh dan sebagian yang lain kamu tawan







33:27

# وَاَوْرَثَكُمْ اَرْضَهُمْ وَدِيَارَهُمْ وَاَمْوَالَهُمْ وَاَرْضًا لَّمْ تَطَـُٔوْهَا ۗوَكَانَ اللّٰهُ عَلٰى كُلِّ شَيْءٍ قَدِيْرًا ࣖ

wa-awratsakum ar*dh*ahum wadiy*aa*rahum wa-amw*aa*lahum wa-ar*dh*an lam ta*th*auuh*aa* wak*aa*na **al**l*aa*hu 'al*aa* kulli syay-in qadiir*aa***n**

Dan Dia mewariskan kepadamu tanah-tanah, rumah-rumah dan harta benda mereka, dan (begitu pula) tanah yang belum kamu injak. Dan Allah Mahakuasa terhadap segala sesuatu.

33:28

# يٰٓاَيُّهَا النَّبِيُّ قُلْ لِّاَزْوَاجِكَ اِنْ كُنْتُنَّ تُرِدْنَ الْحَيٰوةَ الدُّنْيَا وَزِيْنَتَهَا فَتَعَالَيْنَ اُمَتِّعْكُنَّ وَاُسَرِّحْكُنَّ سَرَاحًا جَمِيْلًا

y*aa* ayyuh*aa* **al**nnabiyyu qul li-azw*aa*jika in kuntunna turidna **a**l*h*ay*aa*ta **al**dduny*aa* waziinatah*aa* fata'*aa*layna umatti'kunna wausarri*h*kunna sar*a*

Wahai Nabi! Katakanlah kepada istri-istrimu, “Jika kamu menginginkan kehidupan di dunia dan perhiasannya, maka kemarilah agar kuberikan kepadamu mut‘ah dan aku ceraikan kamu dengan cara yang baik.”







33:29

# وَاِنْ كُنْتُنَّ تُرِدْنَ اللّٰهَ وَرَسُوْلَهٗ وَالدَّارَ الْاٰخِرَةَ فَاِنَّ اللّٰهَ اَعَدَّ لِلْمُحْسِنٰتِ مِنْكُنَّ اَجْرًا عَظِيْمًا

wa-in kuntunna turidna **al**l*aa*ha warasuulahu wa**al**dd*aa*ra **a**l-*aa*khirata fa-inna **al**l*aa*ha a'adda lilmu*h*sin*aa*ti minkunna ajran 'a*zh*iim*aa*

Dan jika kamu menginginkan Allah dan Rasul-Nya dan negeri akhirat, maka sesungguhnya Allah menyediakan pahala yang besar bagi siapa yang berbuat baik di antara kamu.

33:30

# يٰنِسَاۤءَ النَّبِيِّ مَنْ يَّأْتِ مِنْكُنَّ بِفَاحِشَةٍ مُّبَيِّنَةٍ يُّضٰعَفْ لَهَا الْعَذَابُ ضِعْفَيْنِۗ وَكَانَ ذٰلِكَ عَلَى اللّٰهِ يَسِيْرًا ۔

y*aa* nis*aa*-a **al**nnabiyyi man ya/ti minkunna bif*aah*isyatin mubayyinatin yu*daa*'af lah*aa* **a**l'a*dzaa*bu *dh*i'fayni wak*aa*na *dzaa*lika 'al*aa* **al**l

Wahai istri-istri Nabi! Barangsiapa di antara kamu yang mengerjakan perbuatan keji yang nyata, niscaya azabnya akan dilipatgandakan dua kali lipat kepadanya. Dan yang demikian itu, mudah bagi Allah.







33:31

# ۞ وَمَنْ يَّقْنُتْ مِنْكُنَّ لِلّٰهِ وَرَسُوْلِهٖ وَتَعْمَلْ صَالِحًا نُّؤْتِهَآ اَجْرَهَا مَرَّتَيْنِۙ وَاَعْتَدْنَا لَهَا رِزْقًا كَرِيْمًا

waman yaqnut minkunna lill*aa*hi warasuulihi wata'mal *shaa*li*h*an nu/tih*aa* ajrah*aa* marratayni wa-a'tadn*aa* lah*aa* rizqan kariim*aa***n**

Dan barangsiapa di antara kamu (istri-istri Nabi) tetap taat kepada Allah dan Rasul-Nya dan mengerjakan kebajikan, niscaya Kami berikan pahala kepadanya dua kali lipat dan Kami sediakan rezeki yang mulia baginya.

33:32

# يٰنِسَاۤءَ النَّبِيِّ لَسْتُنَّ كَاَحَدٍ مِّنَ النِّسَاۤءِ اِنِ اتَّقَيْتُنَّ فَلَا تَخْضَعْنَ بِالْقَوْلِ فَيَطْمَعَ الَّذِيْ فِيْ قَلْبِهٖ مَرَضٌ وَّقُلْنَ قَوْلًا مَّعْرُوْفًاۚ

y*aa* nis*aa*-a **al**nnabiyyi lastunna ka-a*h*adin mina **al**nnis*aa*-i ini ittaqaytunna fal*aa* takh*dh*a'na bi**a**lqawli faya*th*ma'a **al**la*dz*ii fii qalbi

Wahai istri-istri Nabi! Kamu tidak seperti perempuan-perempuan yang lain, jika kamu bertakwa. Maka janganlah kamu tunduk (melemah lembutkan suara) dalam berbicara sehingga bangkit nafsu orang yang ada penyakit dalam hatinya, dan ucapkanlah perkataan yang

33:33

# وَقَرْنَ فِيْ بُيُوْتِكُنَّ وَلَا تَبَرَّجْنَ تَبَرُّجَ الْجَاهِلِيَّةِ الْاُوْلٰى وَاَقِمْنَ الصَّلٰوةَ وَاٰتِيْنَ الزَّكٰوةَ وَاَطِعْنَ اللّٰهَ وَرَسُوْلَهٗ ۗاِنَّمَا يُرِيْدُ اللّٰهُ لِيُذْهِبَ عَنْكُمُ الرِّجْسَ اَهْلَ الْبَيْتِ وَيُطَهِّرَكُمْ تَطْهِ

waqarna fii buyuutikunna wal*aa* tabarrajna tabarruja **a**lj*aa*hiliyyati **a**l-uul*aa* wa-aqimna **al***shsh*al*aa*ta wa*aa*tiina **al**zzak*aa*ta wa-a*th*i'na

Dan hendaklah kamu tetap di rumahmu dan janganlah kamu berhias dan (bertingkah laku) seperti orang-orang jahiliah dahulu, dan laksanakanlah salat, tunaikanlah zakat dan taatilah Allah dan Rasul-Nya. Sesungguhnya Allah bermaksud hendak menghilangkan dosa d

33:34

# وَاذْكُرْنَ مَا يُتْلٰى فِيْ بُيُوْتِكُنَّ مِنْ اٰيٰتِ اللّٰهِ وَالْحِكْمَةِۗ اِنَّ اللّٰهَ كَانَ لَطِيْفًا خَبِيْرًا ࣖ

wa**u***dz*kurna m*aa* yutl*aa* fii buyuutikunna min *aa*y*aa*ti **al**l*aa*hi wa**a**l*h*ikmati inna **al**l*aa*ha k*aa*na la*th*iifan khabiir*aa*<

Dan ingatlah apa yang dibacakan di rumahmu dari ayat-ayat Allah dan hikmah (sunnah Nabimu). Sungguh, Allah Mahalembut, Maha Mengetahui.

33:35

# اِنَّ الْمُسْلِمِيْنَ وَالْمُسْلِمٰتِ وَالْمُؤْمِنِيْنَ وَالْمُؤْمِنٰتِ وَالْقٰنِتِيْنَ وَالْقٰنِتٰتِ وَالصّٰدِقِيْنَ وَالصّٰدِقٰتِ وَالصّٰبِرِيْنَ وَالصّٰبِرٰتِ وَالْخٰشِعِيْنَ وَالْخٰشِعٰتِ وَالْمُتَصَدِّقِيْنَ وَالْمُتَصَدِّقٰتِ وَالصَّاۤىِٕمِيْنَ وَال

inna **a**lmuslimiina wa**a**lmuslim*aa*ti wa**a**lmu/miniina wa**a**lmu/min*aa*ti wa**a**lq*aa*nitiina wa**a**lq*aa*nit*aa*ti wa**al***s*

Sungguh, laki-laki dan perempuan muslim, laki-laki dan perempuan mukmin, laki-laki dan perempuan yang tetap dalam ketaatannya, laki-laki dan perempuan yang benar, laki-laki dan perempuan yang sabar, laki-laki dan perempuan yang khusyuk, laki-laki dan pere







33:36

# وَمَا كَانَ لِمُؤْمِنٍ وَّلَا مُؤْمِنَةٍ اِذَا قَضَى اللّٰهُ وَرَسُوْلُهٗٓ اَمْرًا اَنْ يَّكُوْنَ لَهُمُ الْخِيَرَةُ مِنْ اَمْرِهِمْ ۗوَمَنْ يَّعْصِ اللّٰهَ وَرَسُوْلَهٗ فَقَدْ ضَلَّ ضَلٰلًا مُّبِيْنًاۗ

wam*aa* k*aa*na limu/minin wal*aa* mu/minatin i*dzaa* qa*daa* **al**l*aa*hu warasuuluhu amran an yakuuna lahumu **a**lkhiyaratu min amrihim waman ya'*sh*i **al**l*aa*ha warasuu

Dan tidaklah pantas bagi laki-laki yang mukmin dan perempuan yang mukmin, apabila Allah dan Rasul-Nya telah menetapkan suatu ketetapan, akan ada pilihan (yang lain) bagi mereka tentang urusan mereka. Dan barangsiapa mendurhakai Allah dan Rasul-Nya, maka s

33:37

# وَاِذْ تَقُوْلُ لِلَّذِيْٓ اَنْعَمَ اللّٰهُ عَلَيْهِ وَاَنْعَمْتَ عَلَيْهِ اَمْسِكْ عَلَيْكَ زَوْجَكَ وَاتَّقِ اللّٰهَ وَتُخْفِيْ فِيْ نَفْسِكَ مَا اللّٰهُ مُبْدِيْهِ وَتَخْشَى النَّاسَۚ وَاللّٰهُ اَحَقُّ اَنْ تَخْشٰىهُ ۗ فَلَمَّا قَضٰى زَيْدٌ مِّنْهَا وَ

wa-i*dz* taquulu lilla*dz*ii an'ama **al**l*aa*hu 'alayhi wa-an'amta 'alayhi amsik 'alayka zawjaka wa**i**ttaqi **al**l*aa*ha watukhfii fii nafsika m*aa* **al**l*aa*hu mubdi

Dan (ingatlah), ketika engkau (Muhammad) berkata kepada orang yang telah diberi nikmat oleh Allah dan engkau (juga) telah memberi nikmat kepadanya, “Pertahankanlah terus istrimu dan bertakwalah kepada Allah,” sedang engkau menyembunyikan di dalam hatimu a

33:38

# مَا كَانَ عَلَى النَّبِيِّ مِنْ حَرَجٍ فِيْمَا فَرَضَ اللّٰهُ لَهٗ ۗسُنَّةَ اللّٰهِ فِى الَّذِيْنَ خَلَوْا مِنْ قَبْلُ ۗوَكَانَ اَمْرُ اللّٰهِ قَدَرًا مَّقْدُوْرًاۙ

m*aa* k*aa*na 'al*aa* **al**nnabiyyi min *h*arajin fiim*aa* fara*dh*a **al**l*aa*hu lahu sunnata **al**l*aa*hi fii **al**la*dz*iina khalaw min qablu wak*aa<*

Tidak ada keberatan apa pun pada Nabi tentang apa yang telah ditetapkan Allah baginya. (Allah telah menetapkan yang demikian) sebagai sunnah Allah pada nabi-nabi yang telah terdahulu. Dan ketetapan Allah itu suatu ketetapan yang pasti berlaku,







33:39

# ۨالَّذِيْنَ يُبَلِّغُوْنَ رِسٰلٰتِ اللّٰهِ وَيَخْشَوْنَهٗ وَلَا يَخْشَوْنَ اَحَدًا اِلَّا اللّٰهَ ۗوَكَفٰى بِاللّٰهِ حَسِيْبًا

**al**la*dz*iina yuballighuuna ris*aa*l*aa*ti **al**l*aa*hi wayakhsyawnahu wal*aa* yakhsyawna a*h*adan ill*aa* **al**l*aa*ha wakaf*aa* bi**al**l*aa*hi

(yaitu) orang-orang yang menyampaikan risalah-risalah Allah, mereka takut kepada-Nya dan tidak merasa takut kepada siapa pun selain kepada Allah. Dan cukuplah Allah sebagai pembuat perhitungan.

33:40

# مَا كَانَ مُحَمَّدٌ اَبَآ اَحَدٍ مِّنْ رِّجَالِكُمْ وَلٰكِنْ رَّسُوْلَ اللّٰهِ وَخَاتَمَ النَّبِيّٖنَۗ وَكَانَ اللّٰهُ بِكُلِّ شَيْءٍ عَلِيْمًا ࣖ

m*aa* k*aa*na mu*h*ammadun ab*aa* a*h*adin min rij*aa*likum wal*aa*kin rasuula **al**l*aa*hi wakh*aa*tama **al**nnabiyyiina wak*aa*na **al**l*aa*hu bikulli syay-

Muhammad itu bukanlah bapak dari seseorang di antara kamu, tetapi dia adalah utusan Allah dan penutup para nabi. Dan Allah Maha Mengetahui segala sesuatu.

33:41

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوا اذْكُرُوا اللّٰهَ ذِكْرًا كَثِيْرًاۙ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu u*dz*kuruu **al**l*aa*ha *dz*ikran katsiir*aa***n**

Wahai orang-orang yang beriman! Ingatlah kepada Allah, dengan mengingat (nama-Nya) sebanyak-banyaknya,

33:42

# وَّسَبِّحُوْهُ بُكْرَةً وَّاَصِيْلًا

wasabbi*h*uuhu bukratan wa-a*sh*iil*aa***n**

dan bertasbihlah kepada-Nya pada waktu pagi dan petang.

33:43

# هُوَ الَّذِيْ يُصَلِّيْ عَلَيْكُمْ وَمَلٰۤىِٕكَتُهٗ لِيُخْرِجَكُمْ مِّنَ الظُّلُمٰتِ اِلَى النُّوْرِۗ وَكَانَ بِالْمُؤْمِنِيْنَ رَحِيْمًا

huwa **al**la*dz*ii yu*sh*allii 'alaykum wamal*aa*-ikatuhu liyukhrijakum mina **al***zhzh*ulum*aa*ti il*aa* **al**nnuuri wak*aa*na bi**a**lmu/miniina ra*h*iim*aa*

Dialah yang memberi rahmat kepadamu dan para malaikat-Nya (memohonkan ampunan untukmu), agar Dia mengeluarkan kamu dari kegelapan kepada cahaya (yang terang). Dan Dia Maha Penyayang kepada orang-orang yang beriman.







33:44

# تَحِيَّتُهُمْ يَوْمَ يَلْقَوْنَهٗ سَلٰمٌ ۚوَاَعَدَّ لَهُمْ اَجْرًا كَرِيْمًا

ta*h*iyyatuhum yawma yalqawnahu sal*aa*mun wa-a'adda lahum ajran kariim*aa***n**

Penghormatan mereka (orang-orang mukmin itu) ketika mereka menemui-Nya ialah, “Salam,” dan Dia menyediakan pahala yang mulia bagi mereka.

33:45

# يٰٓاَيُّهَا النَّبِيُّ اِنَّآ اَرْسَلْنٰكَ شَاهِدًا وَّمُبَشِّرًا وَّنَذِيْرًاۙ

y*aa* ayyuh*aa* **al**nnabiyyu inn*aa* arsaln*aa*ka sy*aa*hidan wamubasysyiran wana*dz*iir*aa***n**

Wahai Nabi! Sesungguhnya Kami mengutusmu untuk menjadi saksi, pembawa kabar gembira dan pemberi peringatan,

33:46

# وَّدَاعِيًا اِلَى اللّٰهِ بِاِذْنِهٖ وَسِرَاجًا مُّنِيْرًا

wad*aa*'iyan il*aa* **al**l*aa*hi bi-i*dz*nihi wasir*aa*jan muniir*aa***n**

dan untuk menjadi penyeru kepada (agama) Allah dengan izin-Nya dan sebagai cahaya yang menerangi.

33:47

# وَبَشِّرِ الْمُؤْمِنِيْنَ بِاَنَّ لَهُمْ مِّنَ اللّٰهِ فَضْلًا كَبِيْرًا

wabasysyiri **a**lmu/miniina bi-anna lahum mina **al**l*aa*hi fa*dh*lan kabiir*aa***n**

Dan sampaikanlah kabar gembira kepada orang-orang mukmin bahwa sesungguhnya bagi mereka karunia yang besar dari Allah.

33:48

# وَلَا تُطِعِ الْكٰفِرِيْنَ وَالْمُنٰفِقِيْنَ وَدَعْ اَذٰىهُمْ وَتَوَكَّلْ عَلَى اللّٰهِ ۗوَكَفٰى بِاللّٰهِ وَكِيْلًا

wal*aa* tu*th*i'i **a**lk*aa*firiina wa**a**lmun*aa*fiqiina wada' a*dzaa*hum watawakkal 'al*aa* **al**l*aa*hi wakaf*aa* bi**al**l*aa*hi wakiil*aa*

Dan janganlah engkau (Muhammad) menuruti orang-orang kafir dan orang-orang munafik itu, janganlah engkau hiraukan gangguan mereka dan bertawakallah kepada Allah. Dan cukuplah Allah sebagai pelindung.

33:49

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْٓا اِذَا نَكَحْتُمُ الْمُؤْمِنٰتِ ثُمَّ طَلَّقْتُمُوْهُنَّ مِنْ قَبْلِ اَنْ تَمَسُّوْهُنَّ فَمَا لَكُمْ عَلَيْهِنَّ مِنْ عِدَّةٍ تَعْتَدُّوْنَهَاۚ فَمَتِّعُوْهُنَّ وَسَرِّحُوْهُنَّ سَرَاحًا جَمِيْلًا

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu i*dzaa* naka*h*tumu **a**lmu/min*aa*ti tsumma *th*allaqtumuuhunna min qabli an tamassuuhunna fam*aa* lakum 'alayhinna min 'iddatin ta'tadduun

Wahai orang-orang yang beriman! Apabila kamu menikahi perempuan-perempuan mukmin, kemudian kamu ceraikan mereka sebelum kamu mencampurinya maka tidak ada masa idah atas mereka yang perlu kamu perhitungkan. Namun berilah mereka mut’ah dan lepaskanlah merek

33:50

# يٰٓاَيُّهَا النَّبِيُّ اِنَّآ اَحْلَلْنَا لَكَ اَزْوَاجَكَ الّٰتِيْٓ اٰتَيْتَ اُجُوْرَهُنَّ وَمَا مَلَكَتْ يَمِيْنُكَ مِمَّآ اَفَاۤءَ اللّٰهُ عَلَيْكَ وَبَنٰتِ عَمِّكَ وَبَنٰتِ عَمّٰتِكَ وَبَنٰتِ خَالِكَ وَبَنٰتِ خٰلٰتِكَ الّٰتِيْ هَاجَرْنَ مَعَكَۗ وَام

y*aa* ayyuh*aa* **al**nnabiyyu inn*aa* a*h*laln*aa* laka azw*aa*jaka **al**l*aa*tii *aa*tayta ujuurahunna wam*aa* malakat yamiinuka mimm*aa* af*aa*-a **al**l*a*

Wahai Nabi! Sesungguhnya Kami telah menghalalkan bagimu istri-istrimu yang telah engkau berikan maskawinnya dan hamba sahaya yang engkau miliki, termasuk apa yang engkau peroleh dalam peperangan yang dikaruniakan Allah untukmu, dan (demikian pula) anak-an







33:51

# ۞ تُرْجِيْ مَنْ تَشَاۤءُ مِنْهُنَّ وَتُـْٔوِيْٓ اِلَيْكَ مَنْ تَشَاۤءُۗ وَمَنِ ابْتَغَيْتَ مِمَّنْ عَزَلْتَ فَلَا جُنَاحَ عَلَيْكَۗ ذٰلِكَ اَدْنٰٓى اَنْ تَقَرَّ اَعْيُنُهُنَّ وَلَا يَحْزَنَّ وَيَرْضَيْنَ بِمَآ اٰتَيْتَهُنَّ كُلُّهُنَّۗ وَاللّٰهُ يَعْلَمُ

turjii man tasy*aa*u minhunna watu/wii ilayka man tasy*aa*u wamani ibtaghayta mimman 'azalta fal*aa* jun*aah*a 'alayka *dzaa*lika adn*aa* an taqarra a'yunuhunna wal*aa* ya*h*zanna wayar*dh*ayna bim*aa*

Engkau boleh menangguhkan (menggauli) siapa yang engkau kehendaki di antara mereka (para istrimu) dan (boleh pula) menggauli siapa (di antara mereka) yang engkau kehendaki. Dan siapa yang engkau ingini untuk menggaulinya kembali dari istri-istrimu yang te







33:52

# لَا يَحِلُّ لَكَ النِّسَاۤءُ مِنْۢ بَعْدُ وَلَآ اَنْ تَبَدَّلَ بِهِنَّ مِنْ اَزْوَاجٍ وَّلَوْ اَعْجَبَكَ حُسْنُهُنَّ اِلَّا مَا مَلَكَتْ يَمِيْنُكَۗ وَكَانَ اللّٰهُ عَلٰى كُلِّ شَيْءٍ رَّقِيْبًا ࣖ

turjii man tasy*aa*u minhunna watu/wii ilayka man tasy*aa*u wamani ibtaghayta mimman 'azalta fal*aa* jun*aah*a 'alayka *dzaa*lika adn*aa* an taqarra a'yunuhunna wal*aa* ya*h*zanna wayar*dh*ayna bim*aa*

Tidak halal bagimu (Muhammad) menikahi perempuan-perempuan (lain) setelah itu, dan tidak boleh (pula) mengganti mereka dengan istri-istri (yang lain), meskipun kecantikannya menarik hatimu kecuali perempuan-perempuan (hamba sahaya) yang engkau miliki. Dan







33:53

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْا لَا تَدْخُلُوْا بُيُوْتَ النَّبِيِّ اِلَّآ اَنْ يُّؤْذَنَ لَكُمْ اِلٰى طَعَامٍ غَيْرَ نٰظِرِيْنَ اِنٰىهُ وَلٰكِنْ اِذَا دُعِيْتُمْ فَادْخُلُوْا فَاِذَا طَعِمْتُمْ فَانْتَشِرُوْا وَلَا مُسْتَأْنِسِيْنَ لِحَدِيْثٍۗ اِنَّ ذٰ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu l*aa* tadkhuluu buyuuta **al**nnabiyyi ill*aa* an yu/*dz*ana lakum il*aa* *th*a'*aa*min ghayra n*aats*iriina in*aa*hu wal*aa*

Wahai orang-orang yang beriman! Janganlah kamu memasuki rumah-rumah Nabi kecuali jika kamu diizinkan untuk makan tanpa menunggu waktu masak (makanannya), tetapi jika kamu dipanggil maka masuklah dan apabila kamu selesai makan, keluarlah kamu tanpa memperp







33:54

# اِنْ تُبْدُوْا شَيْـًٔا اَوْ تُخْفُوْهُ فَاِنَّ اللّٰهَ كَانَ بِكُلِّ شَيْءٍ عَلِيْمًا

in tubduu syay-an aw tukhfuuhu fa-inna **al**l*aa*ha k*aa*na bikulli syay-in 'aliim*aa***n**

Jika kamu menyatakan sesuatu atau menyembunyikannya, maka sesungguhnya Allah Maha Mengetahui segala sesuatu.

33:55

# لَا جُنَاحَ عَلَيْهِنَّ فِيْٓ اٰبَاۤىِٕهِنَّ وَلَآ اَبْنَاۤىِٕهِنَّ وَلَآ اِخْوَانِهِنَّ وَلَآ اَبْنَاۤءِ اِخْوَانِهِنَّ وَلَآ اَبْنَاۤءِ اَخَوٰتِهِنَّ وَلَا نِسَاۤىِٕهِنَّ وَلَا مَا مَلَكَتْ اَيْمَانُهُنَّۚ وَاتَّقِيْنَ اللّٰهَ ۗاِنَّ اللّٰهَ كَانَ ع

l*aa* jun*aah*a 'alayhinna fii *aa*b*aa*-ihinna wal*aa* abn*aa*-ihinna wal*aa* ikhw*aa*nihinna wal*aa* abn*aa*-i ikhw*aa*nihinna wal*aa* abn*aa*-i akhaw*aa*tihinna wal*aa* nis*aa*

Tidak ada dosa atas istri-istri Nabi (untuk berjumpa tanpa tabir) dengan bapak-bapak mereka, anak laki-laki mereka, saudara laki-laki mereka, anak laki-laki dari saudara laki-laki mereka, anak laki-laki dari saudara perempuan mereka, perempuan-perempuan m







33:56

# اِنَّ اللّٰهَ وَمَلٰۤىِٕكَتَهٗ يُصَلُّوْنَ عَلَى النَّبِيِّۗ يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْا صَلُّوْا عَلَيْهِ وَسَلِّمُوْا تَسْلِيْمًا

inna **al**l*aa*ha wamal*aa*-ikatahu yu*sh*alluuna 'al*aa* **al**nnabiyyi y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu *sh*alluu 'alayhi wasallimuu tasliim*aa***n**

Sesungguhnya Allah dan para malaikat-Nya bersalawat untuk Nabi. Wahai orang-orang yang beriman! Bersalawatlah kamu untuk Nabi dan ucapkanlah salam dengan penuh penghormatan kepadanya.







33:57

# اِنَّ الَّذِيْنَ يُؤْذُوْنَ اللّٰهَ وَرَسُوْلَهٗ لَعَنَهُمُ اللّٰهُ فِى الدُّنْيَا وَالْاٰخِرَةِ وَاَعَدَّ لَهُمْ عَذَابًا مُّهِيْنًا

inna **al**la*dz*iina yu/*dz*uuna **al**l*aa*ha warasuulahu la'anahumu **al**l*aa*hu fii **al**dduny*aa* wa**a**l-*aa*khirati wa-a'adda lahum 'a*dzaa*ban m

Sesungguhnya (terhadap) orang-orang yang menyakiti Allah dan Rasul-Nya, Allah akan melaknatnya di dunia dan di akhirat, dan menyediakan azab yang menghinakan bagi mereka.

33:58

# وَالَّذِيْنَ يُؤْذُوْنَ الْمُؤْمِنِيْنَ وَالْمُؤْمِنٰتِ بِغَيْرِ مَا اكْتَسَبُوْا فَقَدِ احْتَمَلُوْا بُهْتَانًا وَّاِثْمًا مُّبِيْنًا ࣖ

wa**a**lla*dz*iina yu/*dz*uuna **a**lmu/miniina wa**a**lmu/min*aa*ti bighayri m*aa* iktasabuu faqadi i*h*tamaluu buht*aa*nan wa-itsman mubiin*aa***n**

Dan orang-orang yang menyakiti orang-orang mukmin laki-laki dan perempuan, tanpa ada kesalahan yang mereka perbuat, maka sungguh, mereka telah memikul kebohongan dan dosa yang nyata.

33:59

# يٰٓاَيُّهَا النَّبِيُّ قُلْ لِّاَزْوَاجِكَ وَبَنٰتِكَ وَنِسَاۤءِ الْمُؤْمِنِيْنَ يُدْنِيْنَ عَلَيْهِنَّ مِنْ جَلَابِيْبِهِنَّۗ ذٰلِكَ اَدْنٰىٓ اَنْ يُّعْرَفْنَ فَلَا يُؤْذَيْنَۗ وَكَانَ اللّٰهُ غَفُوْرًا رَّحِيْمًا

y*aa* ayyuh*aa* **al**nnabiyyu qul li-azw*aa*jika waban*aa*tika wanis*aa*-i **a**lmu/miniina yudniina 'alayhinna min jal*aa*biibihinna *dzaa*lika adn*aa* an yu'rafna fal*aa* yu/*dz*

*Wahai Nabi! Katakanlah kepada istri-istrimu, anak-anak perempuanmu dan istri-istri orang mukmin, “Hendaklah mereka menutupkan jilbabnya ke seluruh tubuh mereka.” Yang demikian itu agar mereka lebih mudah untuk dikenali, sehingga mereka tidak diganggu. Dan*









33:60

# ۞ لَىِٕنْ لَّمْ يَنْتَهِ الْمُنٰفِقُوْنَ وَالَّذِيْنَ فِيْ قُلُوْبِهِمْ مَّرَضٌ وَّالْمُرْجِفُوْنَ فِى الْمَدِيْنَةِ لَنُغْرِيَنَّكَ بِهِمْ ثُمَّ لَا يُجَاوِرُوْنَكَ فِيْهَآ اِلَّا قَلِيْلًا

la-in lam yantahi **a**lmun*aa*fiquuna wa**a**lla*dz*iina fii quluubihim mara*dh*un wa**a**lmurjifuuna fii **a**lmadiinati lanughriyannaka bihim tsumma l*aa* yuj*aa*wiruunaka fii

Sungguh, jika orang-orang munafik, orang-orang yang berpenyakit dalam hatinya dan orang-orang yang menyebarkan kabar bohong di Madinah tidak berhenti (dari menyakitimu), niscaya Kami perintahkan engkau (untuk memerangi) mereka, kemudian mereka tidak lagi

33:61

# مَلْعُوْنِيْنَۖ اَيْنَمَا ثُقِفُوْٓا اُخِذُوْا وَقُتِّلُوْا تَقْتِيْلًا

mal'uuniina ayna m*aa* tsuqifuu ukhi*dz*uu waquttiluu taqtiil*aa***n**

dalam keadaan terlaknat. Di mana saja mereka dijumpai, mereka akan ditangkap dan dibunuh tanpa ampun.

33:62

# سُنَّةَ اللّٰهِ فِى الَّذِيْنَ خَلَوْا مِنْ قَبْلُ ۚوَلَنْ تَجِدَ لِسُنَّةِ اللّٰهِ تَبْدِيْلًا

sunnata **al**l*aa*hi fii **al**la*dz*iina khalaw min qablu walan tajida lisunnati **al**l*aa*hi tabdiil*aa***n**

Sebagai sunnah Allah yang (berlaku juga) bagi orang-orang yang telah terdahulu sebelum(mu), dan engkau tidak akan mendapati perubahan pada sunnah Allah.

33:63

# يَسْـَٔلُكَ النَّاسُ عَنِ السَّاعَةِۗ قُلْ اِنَّمَا عِلْمُهَا عِنْدَ اللّٰهِ ۗوَمَا يُدْرِيْكَ لَعَلَّ السَّاعَةَ تَكُوْنُ قَرِيْبًا

yas-aluka **al**nn*aa*su 'ani **al**ss*aa*'ati qul innam*aa* 'ilmuh*aa* 'inda **al**l*aa*hi wam*aa* yudriika la'alla **al**ss*aa*'ata takuunu qariib*aa***n**

**Manusia bertanya kepadamu (Muhammad) tentang hari Kiamat. Katakanlah, “Ilmu tentang hari Kiamat itu hanya di sisi Allah.” Dan tahukah engkau, boleh jadi hari Kiamat itu sudah dekat waktunya.**









33:64

# اِنَّ اللّٰهَ لَعَنَ الْكٰفِرِيْنَ وَاَعَدَّ لَهُمْ سَعِيْرًاۙ

inna **al**l*aa*ha la'ana **a**lk*aa*firiina wa-a'adda lahum sa'iir*aa***n**

Sungguh, Allah melaknat orang-orang kafir dan menyediakan bagi mereka api yang menyala-nyala (neraka),

33:65

# خٰلِدِيْنَ فِيْهَآ اَبَدًاۚ لَا يَجِدُوْنَ وَلِيًّا وَّلَا نَصِيْرًا ۚ

kh*aa*lidiina fiih*aa* abadan l*aa* yajiduuna waliyyan wal*aa* na*sh*iir*aa***n**

mereka kekal di dalamnya selama-lamanya; mereka tidak akan mendapatkan pelindung dan penolong.

33:66

# يَوْمَ تُقَلَّبُ وُجُوْهُهُمْ فِى النَّارِ يَقُوْلُوْنَ يٰلَيْتَنَآ اَطَعْنَا اللّٰهَ وَاَطَعْنَا الرَّسُوْلَا۠

yawma tuqallabu wujuuhuhum fii **al**nn*aa*ri yaquuluuna y*aa* laytan*aa* a*th*a'n*aa* **al**l*aa*ha wa-a*th*a'n*aa* **al**rrasuul*aa*

Pada hari (ketika) wajah mereka dibolak-balikkan dalam neraka, mereka berkata, “Wahai, kiranya dahulu kami taat kepada Allah dan taat (pula) kepada Rasul.”

33:67

# وَقَالُوْا رَبَّنَآ اِنَّآ اَطَعْنَا سَادَتَنَا وَكُبَرَاۤءَنَا فَاَضَلُّوْنَا السَّبِيْلَا۠

waq*aa*luu rabban*aa* inn*aa* a*th*a'n*aa* s*aa*datan*aa* wakubar*aa*-an*aa* fa-a*dh*alluun*aa* **al**ssabiil*aa*

Dan mereka berkata, “Ya Tuhan kami, sesungguhnya kami telah menaati para pemimpin dan para pembesar kami, lalu mereka menyesatkan kami dari jalan (yang benar).

33:68

# رَبَّنَآ اٰتِهِمْ ضِعْفَيْنِ مِنَ الْعَذَابِ وَالْعَنْهُمْ لَعْنًا كَبِيْرًا ࣖ

rabban*aa* *aa*tihim *dh*i'fayni mina **a**l'a*dzaa*bi wa**i**l'anhum la'nan kabiir*aa***n**

Ya Tuhan kami, timpakanlah kepada mereka azab dua kali lipat dan laknatlah mereka dengan laknat yang besar.”

33:69

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْا لَا تَكُوْنُوْا كَالَّذِيْنَ اٰذَوْا مُوْسٰى فَبَرَّاَهُ اللّٰهُ مِمَّا قَالُوْا ۗوَكَانَ عِنْدَ اللّٰهِ وَجِيْهًا ۗ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu l*aa* takuunuu ka**a**lla*dz*iina *aadz*aw muus*aa* fabarra-ahu **al**l*aa*hu mimm*aa* q*aa*luu wak*aa*na 'inda <

Wahai orang-orang yang beriman! Janganlah kamu seperti orang-orang yang menyakiti Musa, maka Allah membersihkannya dari tuduhan-tuduhan yang mereka lontarkan. Dan dia seorang yang mempunyai kedudukan terhormat di sisi Allah.

33:70

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوا اتَّقُوا اللّٰهَ وَقُوْلُوْا قَوْلًا سَدِيْدًاۙ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu ittaquu **al**l*aa*ha waquuluu qawlan sadiid*aa***n**

Wahai orang-orang yang beriman! Bertakwalah kamu kepada Allah dan ucapkanlah perkataan yang benar,

33:71

# يُّصْلِحْ لَكُمْ اَعْمَالَكُمْ وَيَغْفِرْ لَكُمْ ذُنُوْبَكُمْۗ وَمَنْ يُّطِعِ اللّٰهَ وَرَسُوْلَهٗ فَقَدْ فَازَ فَوْزًا عَظِيْمًا

yu*sh*li*h* lakum a'm*aa*lakum wayaghfir lakum *dz*unuubakum waman yu*th*i'i **al**l*aa*ha warasuulahu faqad f*aa*za fawzan 'a*zh*iim*aa***n**

niscaya Allah akan memperbaiki amal-amalmu dan mengampuni dosa-dosamu. Dan barangsiapa menaati Allah dan Rasul-Nya, maka sungguh, dia menang dengan kemenangan yang agung.

33:72

# اِنَّا عَرَضْنَا الْاَمَانَةَ عَلَى السَّمٰوٰتِ وَالْاَرْضِ وَالْجِبَالِ فَاَبَيْنَ اَنْ يَّحْمِلْنَهَا وَاَشْفَقْنَ مِنْهَا وَحَمَلَهَا الْاِنْسَانُۗ اِنَّهٗ كَانَ ظَلُوْمًا جَهُوْلًاۙ

inn*aa* 'ara*dh*n*aa* **a**l-am*aa*nata 'al*aa* **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i wa**a**ljib*aa*li fa-abayna an ya*h*milnah*aa* wa-asyfaqna mi

Sesungguhnya Kami telah menawarkan amanat kepada langit, bumi dan gunung-gunung; tetapi semuanya enggan untuk memikul amanat itu dan mereka khawatir tidak akan melaksanakannya (berat), lalu dipikullah amanat itu oleh manusia. Sungguh, manusia itu sangat z

33:73

# لِّيُعَذِّبَ اللّٰهُ الْمُنٰفِقِيْنَ وَالْمُنٰفِقَتِ وَالْمُشْرِكِيْنَ وَالْمُشْرِكٰتِ وَيَتُوْبَ اللّٰهُ عَلَى الْمُؤْمِنِيْنَ وَالْمُؤْمِنٰتِۗ وَكَانَ اللّٰهُ غَفُوْرًا رَّحِيْمًا ࣖ

liyu'a*dzdz*iba **al**l*aa*hu **a**lmun*aa*fiqiina wa**a**lmun*aa*fiq*aa*ti wa**a**lmusyrikiina wa**a**lmusyrik*aa*ti wayatuuba **al**l*aa*hu

sehingga Allah akan mengazab orang-orang munafik laki-laki dan perempuan, orang-orang musyrik, laki-laki dan perempuan; dan Allah akan menerima tobat orang-orang mukmin laki-laki dan perempuan. Dan Allah Maha Pengampun, Maha Penyayang.

<!--EndFragment-->