---
title: (29) Al-'Ankabut - العنكبوت
date: 2021-10-27T03:54:33.123Z
ayat: 29
description: "Jumlah Ayat: 69 / Arti: Laba-Laba"
---
<!--StartFragment-->

29:1

# الۤمّۤ ۗ

alif-l*aa*m-miim

Alif Lam Mim.

29:2

# اَحَسِبَ النَّاسُ اَنْ يُّتْرَكُوْٓا اَنْ يَّقُوْلُوْٓا اٰمَنَّا وَهُمْ لَا يُفْتَنُوْنَ

a*h*asiba **al**nn*aa*su an yutrakuu an yaquuluu *aa*mann*aa* wahum l*aa* yuftanuun**a**

Apakah manusia mengira bahwa mereka akan dibiarkan hanya dengan mengatakan, “Kami telah beriman,” dan mereka tidak diuji?

29:3

# وَلَقَدْ فَتَنَّا الَّذِيْنَ مِنْ قَبْلِهِمْ فَلَيَعْلَمَنَّ اللّٰهُ الَّذِيْنَ صَدَقُوْا وَلَيَعْلَمَنَّ الْكٰذِبِيْنَ

walaqad fatann*aa* **al**la*dz*iina min qablihim falaya'lamanna **al**l*aa*hu **al**la*dz*iina *sh*adaquu walaya'lamanna **a**lk*aadz*ibiin**a**

Dan sungguh, Kami telah menguji orang-orang sebelum mereka, maka Allah pasti mengetahui orang-orang yang benar dan pasti mengetahui orang-orang yang dusta.

29:4

# اَمْ حَسِبَ الَّذِيْنَ يَعْمَلُوْنَ السَّيِّاٰتِ اَنْ يَّسْبِقُوْنَا ۗسَاۤءَ مَا يَحْكُمُوْنَ

am *h*asiba **al**la*dz*iina ya'maluuna **al**ssayyi-*aa*ti an yasbiquun*aa* s*aa*-a m*aa* ya*h*kumuun**a**

Ataukah orang-orang yang mengerjakan kejahatan itu mengira bahwa mereka akan luput dari (azab) Kami? Sangatlah buruk apa yang mereka tetapkan itu!

29:5

# مَنْ كَانَ يَرْجُوْا لِقَاۤءَ اللّٰهِ فَاِنَّ اَجَلَ اللّٰهِ لَاٰتٍ ۗوَهُوَ السَّمِيْعُ الْعَلِيْمُ

man k*aa*na yarjuu liq*aa*-a **al**l*aa*hi fa-inna ajala **al**l*aa*hi la*aa*tin wahuwa **al**ssamii'u **a**l'aliim**u**

Barangsiapa mengharap pertemuan dengan Allah, maka sesungguhnya waktu (yang dijanjikan) Allah pasti datang. Dan Dia Yang Maha Mendengar, Maha Mengetahui.

29:6

# وَمَنْ جَاهَدَ فَاِنَّمَا يُجَاهِدُ لِنَفْسِهٖ ۗاِنَّ اللّٰهَ لَغَنِيٌّ عَنِ الْعٰلَمِيْنَ

waman j*aa*hada fa-innam*aa* yuj*aa*hidu linafsihi inna **al**l*aa*ha laghaniyyun 'ani **a**l'*aa*lamiin**a**

Dan barangsiapa berjihad, maka sesungguhnya jihadnya itu untuk dirinya sendiri. Sungguh, Allah Mahakaya (tidak memerlukan sesuatu) dari seluruh alam.

29:7

# وَالَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِ لَنُكَفِّرَنَّ عَنْهُمْ سَيِّاٰتِهِمْ وَلَنَجْزِيَنَّهُمْ اَحْسَنَ الَّذِيْ كَانُوْا يَعْمَلُوْنَ

wa**a**lla*dz*iina *aa*manuu wa'amiluu **al***shshaa*li*haa*ti lanukaffiranna 'anhum sayyi-*aa*tihim walanajziyannahum a*h*sana **al**la*dz*ii k*aa*nuu ya'maluun**a**

**Dan orang-orang yang beriman dan mengerjakan kebajikan, pasti akan Kami hapus kesalahan-kesalahannya dan mereka pasti akan Kami beri balasan yang lebih baik dari apa yang mereka kerjakan.**









29:8

# وَوَصَّيْنَا الْاِنْسَانَ بِوَالِدَيْهِ حُسْنًا ۗوَاِنْ جَاهَدٰكَ لِتُشْرِكَ بِيْ مَا لَيْسَ لَكَ بِهٖ عِلْمٌ فَلَا تُطِعْهُمَا ۗاِلَيَّ مَرْجِعُكُمْ فَاُنَبِّئُكُمْ بِمَا كُنْتُمْ تَعْمَلُوْنَ

wawa*shsh*ayn*aa* **a**l-ins*aa*na biw*aa*lidayhi *h*usnan wa-in j*aa*had*aa*ka litusyrika bii m*aa* laysa laka bihi 'ilmun fal*aa* tu*th*i'hum*aa* ilayya marji'ukum fa-unabbi-ukum bim

Dan Kami wajibkan kepada manusia agar (berbuat) kebaikan kepada kedua orang tuanya. Dan jika keduanya memaksamu untuk mempersekutukan Aku dengan sesuatu yang engkau tidak mempunyai ilmu tentang itu, maka janganlah engkau patuhi keduanya. Hanya kepada-Ku t







29:9

# وَالَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِ لَنُدْخِلَنَّهُمْ فِى الصّٰلِحِيْنَ

wa**a**lla*dz*iina *aa*manuu wa'amiluu **al***shshaa*li*haa*ti lanudkhilannahum fii **al***shshaa*li*h*iin**a**

Dan orang-orang yang beriman dan mengerjakan kebajikan mereka pasti akan Kami masukkan mereka ke dalam (golongan) orang yang saleh.

29:10

# وَمِنَ النَّاسِ مَنْ يَّقُوْلُ اٰمَنَّا بِاللّٰهِ فَاِذَآ اُوْذِيَ فِى اللّٰهِ جَعَلَ فِتْنَةَ النَّاسِ كَعَذَابِ اللّٰهِ ۗوَلَىِٕنْ جَاۤءَ نَصْرٌ مِّنْ رَّبِّكَ لَيَقُوْلُنَّ اِنَّا كُنَّا مَعَكُمْۗ اَوَلَيْسَ اللّٰهُ بِاَعْلَمَ بِمَا فِيْ صُدُوْرِ الْع

wamina **al**nn*aa*si man yaquulu *aa*mann*aa* bi**al**l*aa*hi fa-i*dzaa* uu*dz*iya fii **al**l*aa*hi ja'ala fitnata **al**nn*aa*si ka'a*dzaa*bi **al**

**Dan di antara manusia ada sebagian yang berkata, “Kami beriman kepada Allah,” tetapi apabila dia disakiti (karena dia beriman) kepada Allah, dia menganggap cobaan manusia itu sebagai siksaan Allah. Dan jika datang pertolongan dari Tuhanmu, niscaya mereka**









29:11

# وَلَيَعْلَمَنَّ اللّٰهُ الَّذِيْنَ اٰمَنُوْا وَلَيَعْلَمَنَّ الْمُنٰفِقِيْنَ

walaya'lamanna **al**l*aa*hu **al**la*dz*iina *aa*manuu walaya'lamanna **a**lmun*aa*fiqiin**a**

Dan Allah pasti mengetahui orang-orang yang beriman dan Dia pasti mengetahui orang-orang yang munafik.

29:12

# وَقَالَ الَّذِيْنَ كَفَرُوْا لِلَّذِيْنَ اٰمَنُوا اتَّبِعُوْا سَبِيْلَنَا وَلْنَحْمِلْ خَطٰيٰكُمْۗ وَمَا هُمْ بِحَامِلِيْنَ مِنْ خَطٰيٰهُمْ مِّنْ شَيْءٍۗ اِنَّهُمْ لَكٰذِبُوْنَ

waq*aa*la **al**la*dz*iina kafaruu lilla*dz*iina *aa*manuu ittabi'uu sabiilan*aa* walna*h*mil kha*thaa*y*aa*kum wam*aa* hum bi*haa*miliina min kha*thaa*y*aa*hum min sya

Dan orang-orang yang kafir berkata kepada orang-orang yang beriman, “Ikutilah jalan kami, dan kami akan memikul dosa-dosamu,” padahal mereka sedikit pun tidak (sanggup) memikul dosa-dosa mereka sendiri. Sesungguhnya mereka benar-benar pendusta.

29:13

# وَلَيَحْمِلُنَّ اَثْقَالَهُمْ وَاَثْقَالًا مَّعَ اَثْقَالِهِمْ وَلَيُسْـَٔلُنَّ يَوْمَ الْقِيٰمَةِ عَمَّا كَانُوْا يَفْتَرُوْنَ ࣖ

walaya*h*milunna atsq*aa*lahum wa-atsq*aa*lan ma'a atsq*aa*lihim walayus-alunna yawma **a**lqiy*aa*mati 'amm*aa* k*aa*nuu yaftaruun**a**

Dan mereka benar-benar akan memikul dosa-dosa mereka sendiri, dan dosa-dosa yang lain bersama dosa mereka, dan pada hari Kiamat mereka pasti akan ditanya tentang kebohongan yang selalu mereka ada-adakan.

29:14

# وَلَقَدْ اَرْسَلْنَا نُوْحًا اِلٰى قَوْمِهٖ فَلَبِثَ فِيْهِمْ اَلْفَ سَنَةٍ اِلَّا خَمْسِيْنَ عَامًا ۗفَاَخَذَهُمُ الطُّوْفَانُ وَهُمْ ظٰلِمُوْنَ

walaqad arsaln*aa* nuu*h*an il*aa* qawmihi falabitsa fiihim **a**lfa sanatin ill*aa* khamsiina '*aa*man fa-akha*dz*ahumu **al***ththh*uuf*aa*nu wahum *zhaa*limuun**a**

Dan sungguh, Kami telah mengutus Nuh kepada kaumnya, maka dia tinggal bersama mereka selama seribu tahun kurang lima puluh tahun. Kemudian mereka dilanda banjir besar, sedangkan mereka adalah orang-orang yang zalim.

29:15

# فَاَنْجَيْنٰهُ وَاَصْحٰبَ السَّفِيْنَةِ وَجَعَلْنٰهَآ اٰيَةً لِّلْعٰلَمِيْنَ

fa-anjayn*aa*hu wa-a*sh*-*haa*ba **al**ssafiinati waja'aln*aa*h*aa* *aa*yatan lil'*aa*lamiin**a**

Maka Kami selamatkan Nuh dan orang-orang yang berada di kapal itu, dan Kami jadikan (peristiwa) itu sebagai pelajaran bagi semua manusia.

29:16

# وَاِبْرٰهِيْمَ اِذْ قَالَ لِقَوْمِهِ اعْبُدُوا اللّٰهَ وَاتَّقُوْهُ ۗذٰلِكُمْ خَيْرٌ لَّكُمْ اِنْ كُنْتُمْ تَعْلَمُوْنَ

wa-ibr*aa*hiima i*dz* q*aa*la liqawmihi u'buduu **al**l*aa*ha wa**i**ttaquuhu *dzaa*likum khayrun lakum in kuntum ta'lamuun**a**

Dan (ingatlah) Ibrahim, ketika dia berkata kepada kaumnya, “Sembahlah Allah dan bertakwalah kepada-Nya. Yang demikian itu lebih baik bagimu, jika kamu mengetahui.

29:17

# اِنَّمَا تَعْبُدُوْنَ مِنْ دُوْنِ اللّٰهِ اَوْثَانًا وَّتَخْلُقُوْنَ اِفْكًا ۗاِنَّ الَّذِيْنَ تَعْبُدُوْنَ مِنْ دُوْنِ اللّٰهِ لَا يَمْلِكُوْنَ لَكُمْ رِزْقًا فَابْتَغُوْا عِنْدَ اللّٰهِ الرِّزْقَ وَاعْبُدُوْهُ وَاشْكُرُوْا لَهٗ ۗاِلَيْهِ تُرْجَعُوْنَ

innam*aa* ta'buduuna min duuni **al**l*aa*hi awts*aa*nan watakhluquuna ifkan inna **al**la*dz*iina ta'buduuna min duuni **al**l*aa*hi l*aa* yamlikuuna lakum rizqan fa**i**bt

Sesungguhnya yang kamu sembah selain Allah hanyalah berhala-berhala, dan kamu membuat kebohongan. Sesungguhnya apa yang kamu sembah selain Allah itu tidak mampu memberikan rezeki kepadamu; maka mintalah rezeki dari Allah, dan sembahlah Dia dan bersyukurla

29:18

# وَاِنْ تُكَذِّبُوْا فَقَدْ كَذَّبَ اُمَمٌ مِّنْ قَبْلِكُمْ ۗوَمَا عَلَى الرَّسُوْلِ اِلَّا الْبَلٰغُ الْمُبِيْنُ

wa-in tuka*dzdz*ibuu faqad ka*dzdz*aba umamun min qablikum wam*aa* 'al*aa* **al**rrasuuli ill*aa* **a**lbal*aa*ghu **a**lmubiin**u**

Dan jika kamu (orang kafir) mendustakan, maka sungguh, umat sebelum kamu juga telah mendustakan (para rasul). Dan kewajiban rasul itu hanyalah menyampaikan (agama Allah) dengan jelas.”

29:19

# اَوَلَمْ يَرَوْا كَيْفَ يُبْدِئُ اللّٰهُ الْخَلْقَ ثُمَّ يُعِيْدُهٗ ۗاِنَّ ذٰلِكَ عَلَى اللّٰهِ يَسِيْرٌ

awa lam yaraw kayfa yubdi-u **al**l*aa*hu **a**lkhalqa tsumma yu'iiduhu inna *dzaa*lika 'al*aa* **al**l*aa*hi yasiir**un**

Dan apakah mereka tidak memperhatikan bagaimana Allah memulai penciptaan (makhluk), kemudian Dia mengulanginya (kembali). Sungguh, yang demikian itu mudah bagi Allah.

29:20

# قُلْ سِيْرُوْا فِى الْاَرْضِ فَانْظُرُوْا كَيْفَ بَدَاَ الْخَلْقَ ثُمَّ اللّٰهُ يُنْشِئُ النَّشْاَةَ الْاٰخِرَةَ ۗاِنَّ اللّٰهَ عَلٰى كُلِّ شَيْءٍ قَدِيْرٌ ۚ

qul siiruu fii **a**l-ar*dh*i fa**u**n*zh*uruu kayfa bada-a **a**lkhalqa tsumma **al**l*aa*hu yunsyi-u **al**nnasy-ata **a**l-*aa*khirata inna **al**

**Katakanlah, “Berjalanlah di bumi, maka perhatikanlah bagaimana (Allah) memulai penciptaan (makhluk), kemudian Allah menjadikan kejadian yang akhir. Sungguh, Allah Mahakuasa atas segala sesuatu.**









29:21

# يُعَذِّبُ مَنْ يَّشَاۤءُ وَيَرْحَمُ مَنْ يَّشَاۤءُ ۚوَاِلَيْهِ تُقْلَبُوْنَ

yu'a*dzdz*ibu man yasy*aa*u wayar*h*amu man yasy*aa*u wa-ilayhi tuqlabuun**a**

Dia (Allah) mengazab siapa yang Dia kehendaki dan memberi rahmat kepada siapa yang Dia kehendaki, dan hanya kepada-Nya kamu akan dikembalikan.

29:22

# وَمَآ اَنْتُمْ بِمُعْجِزِيْنَ فِى الْاَرْضِ وَلَا فِى السَّمَاۤءِ ۖوَمَا لَكُمْ مِّنْ دُوْنِ اللّٰهِ مِنْ وَّلِيٍّ وَّلَا نَصِيْرٍ ࣖ

wam*aa* antum bimu'jiziina fii **a**l-ar*dh*i wal*aa* fii **al**ssam*aa*-i wam*aa* lakum min duuni **al**l*aa*hi min waliyyin wal*aa* na*sh*iir**in**

Dan kamu sama sekali tidak dapat melepaskan diri (dari azab Allah) baik di bumi maupun di langit, dan tidak ada pelindung dan penolong bagimu selain Allah.

29:23

# وَالَّذِيْنَ كَفَرُوْا بِاٰيٰتِ اللّٰهِ وَلِقَاۤىِٕهٖٓ اُولٰۤىِٕكَ يَىِٕسُوْا مِنْ رَّحْمَتِيْ وَاُولٰۤىِٕكَ لَهُمْ عَذَابٌ اَلِيْمٌ

wa**a**lla*dz*iina kafaruu bi-*aa*y*aa*ti **al**l*aa*hi waliq*aa*-ihi ul*aa*-ika ya-isuu min ra*h*matii waul*aa*-ika lahum 'a*dzaa*bun **a**liim**un**

Dan orang-orang yang mengingkari ayat-ayat Allah dan pertemuan dengan-Nya, mereka berputus asa dari rahmat-Ku, dan mereka itu akan mendapat azab yang pedih.

29:24

# فَمَا كَانَ جَوَابَ قَوْمِهٖٓ اِلَّآ اَنْ قَالُوا اقْتُلُوْهُ اَوْ حَرِّقُوْهُ فَاَنْجٰىهُ اللّٰهُ مِنَ النَّارِۗ اِنَّ فِيْ ذٰلِكَ لَاٰيٰتٍ لِّقَوْمٍ يُّؤْمِنُوْنَ

fam*aa* k*aa*na jaw*aa*ba qawmihi ill*aa* an q*aa*luu uqtuluuhu aw *h*arriquuhu fa-anj*aa*hu **al**l*aa*hu mina **al**nn*aa*ri inna fii *dzaa*lika la*aa*y*aa*tin liqawmi

Maka tidak ada jawaban kaumnya (Ibrahim), selain mengatakan, “Bunuhlah atau bakarlah dia,” lalu Allah menyelamatkannya dari api. Sungguh, pada yang demikian itu pasti terdapat tanda-tanda (kebesaran Allah) bagi orang yang beriman.

29:25

# وَقَالَ اِنَّمَا اتَّخَذْتُمْ مِّنْ دُوْنِ اللّٰهِ اَوْثَانًاۙ مَّوَدَّةَ بَيْنِكُمْ فِى الْحَيٰوةِ الدُّنْيَا ۚ ثُمَّ يَوْمَ الْقِيٰمَةِ يَكْفُرُ بَعْضُكُمْ بِبَعْضٍ وَّيَلْعَنُ بَعْضُكُمْ بَعْضًا ۖوَّمَأْوٰىكُمُ النَّارُ وَمَا لَكُمْ مِّنْ نّٰصِرِيْنَۖ

waq*aa*la innam*aa* ittakha*dz*tum min duuni **al**l*aa*hi awts*aa*nan mawaddata baynikum fii **a**l*h*ay*aa*ti **al**dduny*aa* tsumma yawma **a**lqiy*aa*mati

Dan dia (Ibrahim) berkata, “Sesungguhnya berhala-berhala yang kamu sembah selain Allah, hanya untuk menciptakan perasaan kasih sayang di antara kamu dalam kehidupan di dunia, kemudian pada hari Kiamat sebagian kamu akan saling mengingkari dan saling mengu

29:26

# ۞ فَاٰمَنَ لَهٗ لُوْطٌۘ وَقَالَ اِنِّيْ مُهَاجِرٌ اِلٰى رَبِّيْ ۗاِنَّهٗ هُوَ الْعَزِيْزُ الْحَكِيْمُ

fa*aa*mana lahu luu*th*un waq*aa*la innii muh*aa*jirun il*aa* rabbii innahu huwa **a**l'aziizu **a**l*h*akiim**u**

Maka Lut membenarkan (kenabian Ibrahim). Dan dia (Ibrahim) berkata, “Sesungguhnya aku harus berpindah ke (tempat yang diperintahkan) Tuhanku; sungguh, Dialah Yang Mahaperkasa, Mahabijaksana.”

29:27

# وَوَهَبْنَا لَهٗٓ اِسْحٰقَ وَيَعْقُوْبَ وَجَعَلْنَا فِيْ ذُرِّيَّتِهِ النُّبُوَّةَ وَالْكِتٰبَ وَاٰتَيْنٰهُ اَجْرَهٗ فِى الدُّنْيَا ۚوَاِنَّهٗ فِى الْاٰخِرَةِ لَمِنَ الصّٰلِحِيْنَ

wawahabn*aa* lahu is*haa*qa waya'quuba waja'aln*aa* fii *dz*urriyyatihi **al**nnubuwwata wa**a**lkit*aa*ba wa*aa*tayn*aa*hu ajrahu fii **al**dduny*aa* wa-innahu fii **a**

**Dan Kami anugerahkan kepada Ibrahim, Ishak dan Yakub, dan Kami jadikan kenabian dan kitab kepada keturunannya, dan Kami berikan kepadanya balasannya di dunia; dan sesungguhnya dia di akhirat, termasuk orang yang saleh.**









29:28

# وَلُوْطًا اِذْ قَالَ لِقَوْمِهٖٓ اِنَّكُمْ لَتَأْتُوْنَ الْفَاحِشَةَ ۖمَا سَبَقَكُمْ بِهَا مِنْ اَحَدٍ مِّنَ الْعٰلَمِيْنَ

waluu*th*an i*dz* q*aa*la liqawmihi innakum lata/tuuna **a**lf*aah*isyata m*aa* sabaqakum bih*aa* min a*h*adin mina **a**l'*aa*lamiin**a**

Dan (ingatlah) ketika Lut berkata kepada kaumnya, “Kamu benar-benar melakukan perbuatan yang sangat keji (homoseksual) yang belum pernah dilakukan oleh seorang pun dari umat-umat sebelum kamu.

29:29

# اَىِٕنَّكُمْ لَتَأْتُوْنَ الرِّجَالَ وَتَقْطَعُوْنَ السَّبِيْلَ ەۙ وَتَأْتُوْنَ فِيْ نَادِيْكُمُ الْمُنْكَرَ ۗفَمَا كَانَ جَوَابَ قَوْمِهٖٓ اِلَّآ اَنْ قَالُوا ائْتِنَا بِعَذَابِ اللّٰهِ اِنْ كُنْتَ مِنَ الصّٰدِقِيْنَ

a-innakum lata/tuuna **al**rrij*aa*la wataq*th*a'uuna **al**ssabiila wata/tuuna fii n*aa*diikumu **a**lmunkara fam*aa* k*aa*na jaw*aa*ba qawmihi ill*aa* an q*aa*luu i/tin*aa<*

Apakah pantas kamu mendatangi laki-laki, menyamun dan mengerjakan kemungkaran di tempat-tempat pertemuanmu?” Maka jawaban kaumnya tidak lain hanya mengatakan, “Datangkanlah kepada kami azab Allah, jika engkau termasuk orang-orang yang benar.”







29:30

# قَالَ رَبِّ انْصُرْنِيْ عَلَى الْقَوْمِ الْمُفْسِدِيْنَ ࣖ

q*aa*la rabbi un*sh*urnii 'al*aa* **a**lqawmi **a**lmufsidiin**a**

Dia (Lut) berdoa, “Ya Tuhanku, tolonglah aku (dengan menimpakan azab) atas golongan yang berbuat kerusakan itu.”

29:31

# وَلَمَّا جَاۤءَتْ رُسُلُنَآ اِبْرٰهِيْمَ بِالْبُشْرٰىۙ قَالُوْٓا اِنَّا مُهْلِكُوْٓا اَهْلِ هٰذِهِ الْقَرْيَةِ ۚاِنَّ اَهْلَهَا كَانُوْا ظٰلِمِيْنَ ۚ

walamm*aa* j*aa*-at rusulun*aa* ibr*aa*hiima bi**a**lbusyr*aa* q*aa*luu inn*aa* muhlikuu ahli h*aadz*ihi **a**lqaryati inna ahlah*aa* k*aa*nuu *zhaa*limiin**a**

Dan ketika utusan Kami (para malaikat) datang kepada Ibrahim dengan membawa kabar gembira, mereka mengatakan, “Sungguh, kami akan membinasakan penduduk kota (Sodom) ini karena penduduknya sungguh orang-orang zalim.”

29:32

# قَالَ اِنَّ فِيْهَا لُوْطًا ۗقَالُوْا نَحْنُ اَعْلَمُ بِمَنْ فِيْهَا ۖ لَنُنَجِّيَنَّهٗ وَاَهْلَهٗٓ اِلَّا امْرَاَتَهٗ كَانَتْ مِنَ الْغٰبِرِيْنَ

q*aa*la inna fiih*aa* luu*th*an q*aa*luu na*h*nu a'lamu biman fiih*aa* lanunajjiyannahu wa-ahlahu ill*aa* imra-atahu k*aa*nat mina **a**lgh*aa*biriin**a**

Ibrahim berkata, “Sesungguhnya di kota itu ada Lut.” Mereka (para malaikat) berkata, “Kami lebih mengetahui siapa yang ada di kota itu. Kami pasti akan menyelamatkan dia dan pengikut-pengikutnya kecuali istrinya. Dia termasuk orang-orang yang tertinggal (

29:33

# وَلَمَّآ اَنْ جَاۤءَتْ رُسُلُنَا لُوْطًا سِيْۤءَ بِهِمْ وَضَاقَ بِهِمْ ذَرْعًا وَّقَالُوْا لَا تَخَفْ وَلَا تَحْزَنْ ۗاِنَّا مُنَجُّوْكَ وَاَهْلَكَ اِلَّا امْرَاَتَكَ كَانَتْ مِنَ الْغٰبِرِيْنَ

walamm*aa* an j*aa*-at rusulun*aa* luu*th*an sii-a bihim wa*daa*qa bihim *dz*ar'an waq*aa*luu l*aa* takhaf wal*aa* ta*h*zan inn*aa* munajjuuka wa-ahlaka ill*aa* imra-ataka k*aa*nat mina

Dan ketika para utusan Kami (para malaikat) datang kepada Lut, dia merasa bersedih hati karena (kedatangan) mereka, dan (merasa) tidak mempunyai kekuatan untuk melindungi mereka, dan mereka (para utusan) berkata, “Janganlah engkau takut dan jangan (pula)

29:34

# اِنَّا مُنْزِلُوْنَ عَلٰٓى اَهْلِ هٰذِهِ الْقَرْيَةِ رِجْزًا مِّنَ السَّمَاۤءِ بِمَا كَانُوْا يَفْسُقُوْنَ

inn*aa* munziluuna 'al*aa* ahli h*aadz*ihi **a**lqaryati rijzan mina **al**ssam*aa*-i bim*aa* k*aa*nuu yafsuquun**a**

Sesungguhnya Kami akan menurunkan azab dari langit kepada penduduk kota ini karena mereka berbuat fasik.

29:35

# وَلَقَدْ تَّرَكْنَا مِنْهَآ اٰيَةً ۢ بَيِّنَةً لِّقَوْمٍ يَّعْقِلُوْنَ

walaqad tarakn*aa* minh*aa* *aa*yatan bayyinatan liqawmin ya'qiluun**a**

Dan sungguh, tentang itu telah Kami tinggalkan suatu tanda yang nyata bagi orang-orang yang mengerti.

29:36

# وَاِلٰى مَدْيَنَ اَخَاهُمْ شُعَيْبًاۙ فَقَالَ يٰقَوْمِ اعْبُدُوا اللّٰهَ وَارْجُوا الْيَوْمَ الْاٰخِرَ وَلَا تَعْثَوْا فِى الْاَرْضِ مُفْسِدِيْنَ ۖ

wa-il*aa* madyana akh*aa*hum syu'ayban faq*aa*la y*aa* qawmi u'buduu **al**l*aa*ha wa**u**rjuu **a**lyawma **a**l-*aa*khira wal*aa* ta'tsaw fii **a**l-ar

Dan kepada penduduk Madyan, (Kami telah mengutus) saudara mereka Syuaib, dia berkata, “Wahai kaumku! Sembahlah Allah, harapkanlah (pahala) hari akhir, dan jangan kamu berkeliaran di bumi berbuat kerusakan.”

29:37

# فَكَذَّبُوْهُ فَاَخَذَتْهُمُ الرَّجْفَةُ فَاَصْبَحُوْا فِيْ دَارِهِمْ جٰثِمِيْنَ ۙ

faka*dzdz*abuuhu fa-akha*dz*at-humu **al**rrajfatu fa-a*sh*ba*h*uu fii d*aa*rihim j*aa*tsimiin**a**

Mereka mendustakannya (Syuaib), maka mereka ditimpa gempa yang dahsyat, lalu jadilah mereka mayat-mayat yang bergelimpangan di tempat-tempat tinggal mereka.

29:38

# وَعَادًا وَّثَمُوْدَا۟ وَقَدْ تَّبَيَّنَ لَكُمْ مِّنْ مَّسٰكِنِهِمْۗ وَزَيَّنَ لَهُمُ الشَّيْطٰنُ اَعْمَالَهُمْ فَصَدَّهُمْ عَنِ السَّبِيْلِ وَكَانُوْا مُسْتَبْصِرِيْنَ ۙ

wa'*aa*dan watsamuuda waqad tabayyana lakum min mas*aa*kinihim wazayyana lahumu **al**sysyay*thaa*nu a'm*aa*lahum fa*sh*addahum 'ani **al**ssabiili wak*aa*nuu mustab*sh*iriin**a**

**Juga (ingatlah) kaum ’Ad dan Samud, sungguh telah nyata bagi kamu (kehancuran mereka) dari (puing-puing) tempat tinggal mereka. Setan telah menjadikan terasa indah bagi mereka perbuatan (buruk) mereka, sehingga menghalangi mereka dari jalan (Allah), sedan**









29:39

# وَقَارُوْنَ وَفِرْعَوْنَ وَهَامٰنَۗ وَلَقَدْ جَاۤءَهُمْ مُّوْسٰى بِالْبَيِّنٰتِ فَاسْتَكْبَرُوْا فِى الْاَرْضِ وَمَا كَانُوْا سَابِقِيْنَ ۚ

waq*aa*ruuna wafir'awna wah*aa*m*aa*na walaqad j*aa*-ahum muus*aa* bi**a**lbayyin*aa*ti fa**i**stakbaruu fii **a**l-ar*dh*i wam*aa* k*aa*nuu s*aa*biqiin**a**

**dan (juga) Karun, Fir‘aun dan Haman. Sungguh, telah datang kepada mereka Musa dengan (membawa) keterangan-keterangan yang nyata. Tetapi mereka berlaku sombong di bumi, dan mereka orang-orang yang tidak luput (dari azab Allah).**









29:40

# فَكُلًّا اَخَذْنَا بِذَنْۢبِهٖۙ فَمِنْهُمْ مَّنْ اَرْسَلْنَا عَلَيْهِ حَاصِبًا ۚوَمِنْهُمْ مَّنْ اَخَذَتْهُ الصَّيْحَةُ ۚوَمِنْهُمْ مَّنْ خَسَفْنَا بِهِ الْاَرْضَۚ وَمِنْهُمْ مَّنْ اَغْرَقْنَاۚ وَمَا كَانَ اللّٰهُ لِيَظْلِمَهُمْ وَلٰكِنْ كَانُوْٓا اَنْفُس

fakullan akha*dz*n*aa* bi*dz*anbihi faminhum man arsaln*aa* 'alayhi *has*iban waminhum man akha*dz*at-hu **al***shsh*ay*h*atu waminhum man khasafn*aa* bihi **a**l-ar*dh*a waminhum

Maka masing-masing (mereka itu) Kami azab karena dosa-dosanya, di antara mereka ada yang Kami timpakan kepadanya hujan batu kerikil, ada yang ditimpa suara keras yang mengguntur, ada yang Kami benamkan ke dalam bumi, dan ada pula yang Kami tenggelamkan. A

29:41

# مَثَلُ الَّذِيْنَ اتَّخَذُوْا مِنْ دُوْنِ اللّٰهِ اَوْلِيَاۤءَ كَمَثَلِ الْعَنْكَبُوْتِۚ اِتَّخَذَتْ بَيْتًاۗ وَاِنَّ اَوْهَنَ الْبُيُوْتِ لَبَيْتُ الْعَنْكَبُوْتِۘ لَوْ كَانُوْا يَعْلَمُوْنَ

matsalu **al**la*dz*iina ittakha*dz*uu min duuni **al**l*aa*hi awliy*aa*-a kamatsali **a**l'ankabuuti ittakha*dz*at baytan wa-inna awhana **a**lbuyuuti labaytu **a**

Perumpamaan orang-orang yang mengambil pelindung selain Allah adalah seperti laba-laba yang membuat rumah. Dan sesungguhnya rumah yang paling lemah ialah rumah laba-laba, sekiranya mereka mengetahui.

29:42

# اِنَّ اللّٰهَ يَعْلَمُ مَا يَدْعُوْنَ مِنْ دُوْنِهٖ مِنْ شَيْءٍۗ وَهُوَ الْعَزِيْزُ الْحَكِيْمُ

inna **al**l*aa*ha ya'lamu m*aa* yad'uuna min duunihi min syay-in wahuwa **a**l'aziizu **a**l*h*akiim**u**

Sungguh, Allah mengetahui apa saja yang mereka sembah selain Dia. Dan Dia Mahaperkasa, Mahabijaksana.

29:43

# وَتِلْكَ الْاَمْثَالُ نَضْرِبُهَا لِلنَّاسِۚ وَمَا يَعْقِلُهَآ اِلَّا الْعَالِمُوْنَ

watilka **a**l-amts*aa*lu na*dh*ribuh*aa* li**l**nn*aa*si wam*aa* ya'qiluh*aa* ill*aa* **a**l'*aa*limuun**a**

Dan perumpamaan-perumpamaan ini Kami buat untuk manusia; dan tidak ada yang akan memahaminya kecuali mereka yang berilmu.

29:44

# خَلَقَ اللّٰهُ السَّمٰوٰتِ وَالْاَرْضَ بِالْحَقِّۗ اِنَّ فِيْ ذٰلِكَ لَاٰيَةً لِّلْمُؤْمِنِيْنَ ࣖ ۔

khalaqa **al**l*aa*hu **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*a bi**a**l*h*aqqi inna fii *dzaa*lika la*aa*yatan lilmu-miniin**a**

Allah menciptakan langit dan bumi dengan haq. Sungguh, pada yang demikian itu pasti terdapat tanda-tanda (kebesaran Allah) bagi orang-orang yang beriman.

29:45

# اُتْلُ مَآ اُوْحِيَ اِلَيْكَ مِنَ الْكِتٰبِ وَاَقِمِ الصَّلٰوةَۗ اِنَّ الصَّلٰوةَ تَنْهٰى عَنِ الْفَحْشَاۤءِ وَالْمُنْكَرِ ۗوَلَذِكْرُ اللّٰهِ اَكْبَرُ ۗوَاللّٰهُ يَعْلَمُ مَا تَصْنَعُوْنَ

utlu m*aa* uu*h*iya ilayka mina **a**lkit*aa*bi wa-aqimi **al***shsh*al*aa*ta inna **al***shsh*al*aa*ta tanh*aa* 'ani **a**lfa*h*sy*aa*-i wa**a**

**Bacalah Kitab (Al-Qur'an) yang telah diwahyukan kepadamu (Muhammad) dan laksanakanlah salat. Sesungguhnya salat itu mencegah dari (perbuatan) keji dan mungkar. Dan (ketahuilah) mengingat Allah (salat) itu lebih besar (keutamaannya dari ibadah yang lain).**









29:46

# ۞ وَلَا تُجَادِلُوْٓا اَهْلَ الْكِتٰبِ اِلَّا بِالَّتِيْ هِيَ اَحْسَنُۖ اِلَّا الَّذِيْنَ ظَلَمُوْا مِنْهُمْ وَقُوْلُوْٓا اٰمَنَّا بِالَّذِيْٓ اُنْزِلَ اِلَيْنَا وَاُنْزِلَ اِلَيْكُمْ وَاِلٰهُنَا وَاِلٰهُكُمْ وَاحِدٌ وَّنَحْنُ لَهٗ مُسْلِمُوْنَ

wal*aa* tuj*aa*diluu ahla **a**lkit*aa*bi ill*aa* bi**a**llatii hiya a*h*sanu ill*aa* **al**la*dz*iina *zh*alamuu minhum waquuluu *aa*mann*aa* bi**a**lla

Dan janganlah kamu berdebat dengan Ahli Kitab, melainkan dengan cara yang baik, kecuali dengan orang-orang yang zalim di antara mereka, dan katakanlah, ”Kami telah beriman kepada (kitab-kitab) yang diturunkan kepada kami dan yang diturunkan kepadamu; Tuha

29:47

# وَكَذٰلِكَ اَنْزَلْنَآ اِلَيْكَ الْكِتٰبَۗ فَالَّذِيْنَ اٰتَيْنٰهُمُ الْكِتٰبَ يُؤْمِنُوْنَ بِهٖۚ وَمِنْ هٰٓؤُلَاۤءِ مَنْ يُّؤْمِنُ بِهٖۗ وَمَا يَجْحَدُ بِاٰيٰتِنَآ اِلَّا الْكٰفِرُوْنَ

waka*dzaa*lika anzaln*aa* ilayka **a**lkit*aa*ba fa**a**lla*dz*iina *aa*tayn*aa*humu **a**lkit*aa*ba yu/minuuna bihi wamin h*aa*ul*aa*-i man yu/minu bihi wam*aa* yaj<

Dan demikianlah Kami turunkan Kitab (Al-Qur'an) kepadamu. Adapun orang-orang yang telah Kami berikan Kitab (Taurat dan Injil) mereka beriman kepadanya (Al-Qur'an), dan di antara mereka (orang-orang kafir Mekah) ada yang beriman kepadanya. Dan hanya orang-

29:48

# وَمَا كُنْتَ تَتْلُوْا مِنْ قَبْلِهٖ مِنْ كِتٰبٍ وَّلَا تَخُطُّهٗ بِيَمِيْنِكَ اِذًا لَّارْتَابَ الْمُبْطِلُوْنَ

wam*aa* kunta tatluu min qablihi min kit*aa*bin wal*aa* takhu*ththh*uhu biyamiinika i*dz*an la**i**rt*aa*ba **a**lmub*th*iluun**a**

Dan engkau (Muhammad) tidak pernah membaca sesuatu kitab sebelum (Al-Qur'an) dan engkau tidak (pernah) menulis suatu kitab dengan tangan kananmu; sekiranya (engkau pernah membaca dan menulis), niscaya ragu orang-orang yang mengingkarinya.

29:49

# بَلْ هُوَ اٰيٰتٌۢ بَيِّنٰتٌ فِيْ صُدُوْرِ الَّذِيْنَ اُوْتُوا الْعِلْمَۗ وَمَا يَجْحَدُ بِاٰيٰتِنَآ اِلَّا الظّٰلِمُوْنَ

bal huwa *aa*y*aa*tun bayyin*aa*tun fii *sh*uduuri **al**la*dz*iina uutuu **a**l'ilma wam*aa* yaj*h*adu bi-*aa*y*aa*tin*aa* ill*aa* **al***zhzhaa*limuun

Sebenarnya, (Al-Qur'an) itu adalah ayat-ayat yang jelas di dalam dada orang-orang yang berilmu. Hanya orang-orang yang zalim yang mengingkari ayat-ayat Kami.

29:50

# وَقَالُوْا لَوْلَآ اُنْزِلَ عَلَيْهِ اٰيٰتٌ مِّنْ رَّبِّهٖ ۗ قُلْ اِنَّمَا الْاٰيٰتُ عِنْدَ اللّٰهِ ۗوَاِنَّمَآ اَنَا۠ نَذِيْرٌ مُّبِيْنٌ

waq*aa*luu lawl*aa* unzila 'alayhi *aa*y*aa*tun min rabbihi qul innam*aa* **a**l-*aa*y*aa*tu 'inda **al**l*aa*hi wa-innam*aa* an*aa* na*dz*iirun mubiin**un**

Dan mereka (orang-orang kafir Mekah) berkata, ”Mengapa tidak diturunkan mukjizat-mukjizat dari Tuhannya?” Katakanlah (Muhammad), ”Mukjizat-mukjizat itu terserah kepada Allah. Aku hanya seorang pemberi peringatan yang jelas.”

29:51

# اَوَلَمْ يَكْفِهِمْ اَنَّآ اَنْزَلْنَا عَلَيْكَ الْكِتٰبَ يُتْلٰى عَلَيْهِمْ ۗاِنَّ فِيْ ذٰلِكَ لَرَحْمَةً وَّذِكْرٰى لِقَوْمٍ يُّؤْمِنُوْنَ ࣖ

awa lam yakfihim ann*aa* anzaln*aa* 'alayka **a**lkit*aa*ba yutl*aa* 'alayhim inna fii *dzaa*lika lara*h*matan wa*dz*ikr*aa* liqawmin yu/minuun**a**

Apakah tidak cukup bagi mereka bahwa Kami telah menurunkan kepadamu Kitab (Al-Qur'an) yang dibacakan kepada mereka? Sungguh, dalam (Al-Qur'an) itu terdapat rahmat yang besar dan pelajaran bagi orang-orang yang beriman.

29:52

# قُلْ كَفٰى بِاللّٰهِ بَيْنِيْ وَبَيْنَكُمْ شَهِيْدًاۚ يَعْلَمُ مَا فِى السَّمٰوٰتِ وَالْاَرْضِۗ وَالَّذِيْنَ اٰمَنُوْا بِالْبَاطِلِ وَكَفَرُوْا بِاللّٰهِ اُولٰۤىِٕكَ هُمُ الْخٰسِرُوْنَ

qul kaf*aa* bi**al**l*aa*hi baynii wabaynakum syahiidan ya'lamu m*aa* fii **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i wa**a**lla*dz*iina *aa*manuu bi**a**l

Katakanlah (Muhammad), ”Cukuplah Allah menjadi saksi antara aku dan kamu. Dia mengetahui apa yang di langit dan di bumi. Dan orang yang percaya kepada yang batil dan ingkar kepada Allah, mereka itulah orang-orang yang rugi.”

29:53

# وَيَسْتَعْجِلُوْنَكَ بِالْعَذَابِۗ وَلَوْلَآ اَجَلٌ مُّسَمًّى لَّجَاۤءَهُمُ الْعَذَابُۗ وَلَيَأْتِيَنَّهُمْ بَغْتَةً وَّهُمْ لَا يَشْعُرُوْنَ

wayasta'jiluunaka bi**a**l'a*dzaa*bi walawl*aa* ajalun musamman laj*aa*-ahumu **a**l'a*dzaa*bu walaya/tiyannahum baghtatan wahum l*aa* yasy'uruun**a**

Dan mereka meminta kepadamu agar segera diturunkan azab. Kalau bukan karena waktunya yang telah ditetapkan, niscaya datang azab kepada mereka, dan (azab itu) pasti akan datang kepada mereka dengan tiba-tiba, sedang mereka tidak menyadarinya.

29:54

# يَسْتَعْجِلُوْنَكَ بِالْعَذَابِۗ وَاِنَّ جَهَنَّمَ لَمُحِيْطَةٌ ۢ بِالْكٰفِرِيْنَۙ

wayasta'jiluunaka bi**a**l'a*dzaa*bi walawl*aa* ajalun musamman laj*aa*-ahumu **a**l'a*dzaa*bu walaya/tiyannahum baghtatan wahum l*aa* yasy'uruun**a**

Mereka meminta kepadamu agar segera diturunkan azab. Dan sesungguhnya neraka Jahanam itu pasti meliputi orang-orang kafir,

29:55

# يَوْمَ يَغْشٰىهُمُ الْعَذَابُ مِنْ فَوْقِهِمْ وَمِنْ تَحْتِ اَرْجُلِهِمْ وَيَقُوْلُ ذُوْقُوْا مَا كُنْتُمْ تَعْمَلُوْنَ

yawma yaghsy*aa*humu **a**l'a*dzaa*bu min fawqihim wamin ta*h*ti arjulihim wayaquulu *dz*uuquu m*aa* kuntum ta'maluun**a**

pada hari (ketika) azab menutup mereka dari atas dan dari bawah kaki mereka dan (Allah) berkata (kepada mereka), ”Rasakanlah (balasan dari) apa yang telah kamu kerjakan!”

29:56

# يٰعِبَادِيَ الَّذِيْنَ اٰمَنُوْٓا اِنَّ اَرْضِيْ وَاسِعَةٌ فَاِيَّايَ فَاعْبُدُوْنِ

y*aa* 'ib*aa*diya **al**la*dz*iina *aa*manuu inna ar*dh*ii w*aa*si'atun fa-iyy*aa*ya fa**u**'buduun**i**

Wahai hamba-hamba-Ku yang beriman! Sungguh, bumi-Ku luas, maka sembahlah Aku (saja).

29:57

# كُلُّ نَفْسٍ ذَاۤىِٕقَةُ الْمَوْتِۗ ثُمَّ اِلَيْنَا تُرْجَعُوْنَ

kullu nafsin *dzaa*-iqatu **a**lmawti tsumma ilayn*aa* turja'uun**a**

Setiap yang bernyawa akan merasakan mati. Kemudian hanya kepada Kami kamu dikembalikan.

29:58

# وَالَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِ لَنُبَوِّئَنَّهُمْ مِّنَ الْجَنَّةِ غُرَفًا تَجْرِيْ مِنْ تَحْتِهَا الْاَنْهٰرُ خٰلِدِيْنَ فِيْهَاۗ نِعْمَ اَجْرُ الْعٰمِلِيْنَۖ

wa**a**lla*dz*iina *aa*manuu wa'amiluu **al***shshaa*li*haa*ti lanubawwi-annahum mina **a**ljannati ghurafan tajrii min ta*h*tih*aa* **a**l-anh*aa*ru kh*aa*lidiina

Dan orang-orang yang beriman dan mengerjakan kebajikan, sungguh, mereka akan Kami tempatkan pada tempat-tempat yang tinggi (di dalam surga), yang mengalir di bawahnya sungai-sungai, mereka kekal di dalamnya. Itulah sebaik-baik balasan bagi orang yang berb

29:59

# الَّذِيْنَ صَبَرُوْا وَعَلٰى رَبِّهِمْ يَتَوَكَّلُوْنَ

**al**la*dz*iina *sh*abaruu wa'al*aa* rabbihim yatawakkaluun**a**

(yaitu) orang-orang yang bersabar dan bertawakal kepada Tuhannya.

29:60

# وَكَاَيِّنْ مِّنْ دَاۤبَّةٍ لَّا تَحْمِلُ رِزْقَهَاۖ اللّٰهُ يَرْزُقُهَا وَاِيَّاكُمْ وَهُوَ السَّمِيْعُ الْعَلِيْمُ

waka-ayyin min d*aa*bbatin l*aa* ta*h*milu rizqah*aa* **al**l*aa*hu yarzuquh*aa* wa-iyy*aa*kum wahuwa **al**ssamii'u **a**l'aliim**u**

Dan berapa banyak makhluk bergerak yang bernyawa yang tidak (dapat) membawa (mengurus) rezekinya sendiri. Allah-lah yang memberi rezeki kepadanya dan kepadamu. Dia Maha Mendengar, Maha Mengetahui.

29:61

# وَلَىِٕنْ سَاَلْتَهُمْ مَّنْ خَلَقَ السَّمٰوٰتِ وَالْاَرْضَ وَسَخَّرَ الشَّمْسَ وَالْقَمَرَ لَيَقُوْلُنَّ اللّٰهُ ۗفَاَنّٰى يُؤْفَكُوْنَ

wala-in sa-altahum man khalaqa **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*a wasakhkhara **al**sysyamsa wa**a**lqamara layaquulunna **al**l*aa*hu fa-ann*aa* yu/fakuun

Dan jika engkau bertanya kepada mereka, ”Siapakah yang menciptakan langit dan bumi dan menundukkan matahari dan bulan?” Pasti mereka akan menjawab, ”Allah.” Maka mengapa mereka bisa dipalingkan (dari kebenaran).

29:62

# اَللّٰهُ يَبْسُطُ الرِّزْقَ لِمَنْ يَّشَاۤءُ مِنْ عِبَادِهٖ وَيَقْدِرُ لَهٗ ۗاِنَّ اللّٰهَ بِكُلِّ شَيْءٍ عَلِيْمٌ

**al**l*aa*hu yabsu*th*u **al**rrizqa liman yasy*aa*u min 'ib*aa*dihi wayaqdiru lahu inna **al**l*aa*ha bikulli syay-in 'aliim**un**

Allah melapangkan rezeki bagi orang yang Dia kehendaki di antara hamba-hamba-Nya dan Dia (pula) yang membatasi baginya. Sungguh, Allah Maha Mengetahui segala sesuatu.

29:63

# وَلَىِٕنْ سَاَلْتَهُمْ مَّنْ نَّزَّلَ مِنَ السَّمَاۤءِ مَاۤءً فَاَحْيَا بِهِ الْاَرْضَ مِنْۢ بَعْدِ مَوْتِهَا لَيَقُوْلُنَّ اللّٰهُ ۙقُلِ الْحَمْدُ لِلّٰهِ ۗبَلْ اَكْثَرُهُمْ لَا يَعْقِلُوْنَ ࣖ

wala-in sa-altahum man nazzala mina **al**ssam*aa*-i m*aa*-an fa-a*h*y*aa* bihi **a**l-ar*dh*a min ba'di mawtih*aa* layaquulunna **al**l*aa*hu quli **a**l*h*amdu l

Dan jika kamu bertanya kepada mereka, ”Siapakah yang menurunkan air dari langit lalu dengan (air) itu dihidupkannya bumi yang sudah mati?” Pasti mereka akan menjawab, ”Allah.” Katakanlah, ”Segala puji bagi Allah,” tetapi kebanyakan mereka tidak mengerti.

29:64

# وَمَا هٰذِهِ الْحَيٰوةُ الدُّنْيَآ اِلَّا لَهْوٌ وَّلَعِبٌۗ وَاِنَّ الدَّارَ الْاٰخِرَةَ لَهِيَ الْحَيَوَانُۘ لَوْ كَانُوْا يَعْلَمُوْنَ

wam*aa* h*aadz*ihi **a**l*h*ay*aa*tu **al**dduny*aa* ill*aa* lahwun wala'ibun wa-inna **al**dd*aa*ra **a**l-*aa*khirata lahiya **a**l*h*ayaw*aa*

Dan kehidupan dunia ini hanya senda gurau dan permainan. Dan sesungguhnya negeri akhirat itulah kehidupan yang sebenarnya, sekiranya mereka mengetahui.







29:65

# فَاِذَا رَكِبُوْا فِى الْفُلْكِ دَعَوُا اللّٰهَ مُخْلِصِيْنَ لَهُ الدِّيْنَ ەۚ فَلَمَّا نَجّٰىهُمْ اِلَى الْبَرِّ اِذَا هُمْ يُشْرِكُوْنَۙ

fa-i*dzaa* rakibuu fii **a**lfulki da'awuu **al**l*aa*ha mukhli*sh*iina lahu **al**ddiina falamm*aa* najj*aa*hum il*aa* **a**lbarri i*dzaa* hum yusyrikuun**a**

**Maka apabila mereka naik kapal, mereka berdoa kepada Allah dengan penuh rasa pengabdian (ikhlas) kepada-Nya, tetapi ketika Allah menyelamatkan mereka sampai ke darat, malah mereka (kembali) mempersekutukan (Allah),**









29:66

# لِيَكْفُرُوْا بِمَآ اٰتَيْنٰهُمْۙ وَلِيَتَمَتَّعُوْاۗ فَسَوْفَ يَعْلَمُوْنَ

liyakfuruu bim*aa* *aa*tayn*aa*hum waliyatamatta'uu fasawfa ya'lamuun**a**

biarlah mereka mengingkari nikmat yang telah Kami berikan kepada mereka dan silakan mereka (hidup) bersenang-senang (dalam kekafiran). Maka kelak mereka akan mengetahui (akibat perbuatannya).

29:67

# اَوَلَمْ يَرَوْا اَنَّا جَعَلْنَا حَرَمًا اٰمِنًا وَّيُتَخَطَّفُ النَّاسُ مِنْ حَوْلِهِمْۗ اَفَبِالْبَاطِلِ يُؤْمِنُوْنَ وَبِنِعْمَةِ اللّٰهِ يَكْفُرُوْنَ

awa lam yaraw ann*aa* ja'aln*aa* *h*araman *aa*minan wayutakha*ththh*afu **al**nn*aa*su min *h*awlihim afabi**a**lb*aath*ili yu/minuuna wabini'mati **al**l*aa*hi yakfuruun

Tidakkah mereka memperhatikan, bahwa Kami telah menjadikan (negeri mereka) tanah suci yang aman, padahal manusia di sekitarnya saling merampok. Mengapa (setelah nyata kebenaran) mereka masih percaya kepada yang batil dan ingkar kepada nikmat Allah?

29:68

# وَمَنْ اَظْلَمُ مِمَّنِ افْتَرٰى عَلَى اللّٰهِ كَذِبًا اَوْ كَذَّبَ بِالْحَقِّ لَمَّا جَاۤءَهٗ ۗ اَلَيْسَ فِيْ جَهَنَّمَ مَثْوًى لِّلْكٰفِرِيْنَ

waman a*zh*lamu mimmani iftar*aa* 'al*aa* **al**l*aa*hi ka*dz*iban aw ka*dzdz*aba bi**a**l*h*aqqi lamm*aa* j*aa*-ahu alaysa fii jahannama matswan lilk*aa*firiin**a**

Dan siapakah yang lebih zalim daripada orang yang mengada-adakan kebohongan kepada Allah atau orang yang mendustakan yang hak ketika (yang hak) itu datang kepadanya? Bukankah dalam neraka Jahanam ada tempat bagi orang-orang kafir?

29:69

# وَالَّذِيْنَ جَاهَدُوْا فِيْنَا لَنَهْدِيَنَّهُمْ سُبُلَنَاۗ وَاِنَّ اللّٰهَ لَمَعَ الْمُحْسِنِيْنَ ࣖ

wa**a**lla*dz*iina j*aa*haduu fiin*aa* lanahdiyannahum subulan*aa* wa-inna **al**l*aa*ha lama'a **a**lmu*h*siniin**a**

Dan orang-orang yang berjihad untuk (mencari keridaan) Kami, Kami akan tunjukkan kepada mereka jalan-jalan Kami. Dan sungguh, Allah beserta orang-orang yang berbuat baik.

<!--EndFragment-->