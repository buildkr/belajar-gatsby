---
title: (7) Al-A'raf - الاعراف
date: 2021-10-27T03:23:05.005Z
ayat: 7
description: "Jumlah Ayat: 206 / Arti: Tempat Tertinggi"
---
<!--StartFragment-->

7:1

# الۤمّۤصۤ ۚ

alif-l*aa*m-miim-*shaa*d

Alif Lam Mim shad.

7:2

# كِتٰبٌ اُنْزِلَ اِلَيْكَ فَلَا يَكُنْ فِيْ صَدْرِكَ حَرَجٌ مِّنْهُ لِتُنْذِرَ بِهٖ وَذِكْرٰى لِلْمُؤْمِنِيْنَ

kit*aa*bun unzila ilayka fal*aa* yakun fii *sh*adrika *h*arajun minhu litun*dz*ira bihi wa*dz*ikr*aa* lilmu/miniin**a**

(Inilah) Kitab yang diturunkan kepadamu (Muhammad); maka janganlah engkau sesak dada karenanya, agar engkau memberi peringatan dengan (Kitab) itu dan menjadi pelajaran bagi orang yang beriman.

7:3

# اِتَّبِعُوْا مَآ اُنْزِلَ اِلَيْكُمْ مِّنْ رَّبِّكُمْ وَلَا تَتَّبِعُوْا مِنْ دُوْنِهٖٓ اَوْلِيَاۤءَۗ قَلِيْلًا مَّا تَذَكَّرُوْنَ

ittabi'uu m*aa* unzila ilaykum min rabbikum wal*aa* tattabi'uu min duunihi awliy*aa*-a qaliilan m*aa* ta*dz*akkaruun**a**

Ikutilah apa yang diturunkan kepadamu dari Tuhanmu, dan janganlah kamu ikuti selain Dia sebagai pemimpin. Sedikit sekali kamu mengambil pelajaran.

7:4

# وَكَمْ مِّنْ قَرْيَةٍ اَهْلَكْنٰهَا فَجَاۤءَهَا بَأْسُنَا بَيَاتًا اَوْ هُمْ قَاۤىِٕلُوْنَ

wakam min qaryatin ahlakn*aa*h*aa* faj*aa*-ah*aa* ba/sun*aa* bay*aa*tan aw hum q*aa*-iluun**a**

Betapa banyak negeri yang telah Kami binasakan, siksaan Kami datang (menimpa penduduk)nya pada malam hari, atau pada saat mereka beristirahat pada siang hari.

7:5

# فَمَا كَانَ دَعْوٰىهُمْ اِذْ جَاۤءَهُمْ بَأْسُنَآ اِلَّآ اَنْ قَالُوْٓا اِنَّا كُنَّا ظٰلِمِيْنَ

fam*aa* k*aa*na da'w*aa*hum i*dz* j*aa*-ahum ba/sun*aa* ill*aa* an q*aa*luu inn*aa* kunn*aa* *zhaa*limiin**a**

Maka ketika siksaan Kami datang menimpa mereka, keluhan mereka tidak lain, hanya mengucap, “Sesungguhnya kami adalah orang-orang yang zalim.”

7:6

# فَلَنَسْـَٔلَنَّ الَّذِيْنَ اُرْسِلَ اِلَيْهِمْ وَلَنَسْـَٔلَنَّ الْمُرْسَلِيْنَۙ

falanas-alanna **al**la*dz*iina ursila ilayhim walanas-alanna **a**lmursaliin**a**

Maka pasti akan Kami tanyakan kepada umat yang telah mendapat seruan (dari rasul-rasul) dan Kami akan tanyai (pula) para rasul,

7:7

# فَلَنَقُصَّنَّ عَلَيْهِمْ بِعِلْمٍ وَّمَا كُنَّا غَاۤىِٕبِيْنَ

falanaqu*shsh*anna 'alayhim bi'ilmin wam*aa* kunn*aa* gh*aa*-ibiin**a**

dan pasti akan Kami beritakan kepada mereka dengan ilmu (Kami) dan Kami tidak jauh (dari mereka).

7:8

# وَالْوَزْنُ يَوْمَىِٕذِ ِۨالْحَقُّۚ فَمَنْ ثَقُلَتْ مَوَازِيْنُهٗ فَاُولٰۤىِٕكَ هُمُ الْمُفْلِحُوْنَ

wa**a**lwaznu yawma-i*dz*ini **a**l*h*aqqu faman tsaqulat maw*aa*ziinuhu faul*aa*-ika humu **a**lmufli*h*uun**a**

Timbangan pada hari itu (menjadi ukuran) kebenaran. Maka barangsiapa berat timbangan (kebaikan)nya, mereka itulah orang yang beruntung,

7:9

# وَمَنْ خَفَّتْ مَوَازِيْنُهٗ فَاُولٰۤىِٕكَ الَّذِيْنَ خَسِرُوْٓا اَنْفُسَهُمْ بِمَا كَانُوْا بِاٰيٰتِنَا يَظْلِمُوْنَ

waman khaffat maw*aa*ziinuhu faul*aa*-ika **al**la*dz*iina khasiruu anfusahum bim*aa* k*aa*nuu bi-*aa*y*aa*tin*aa* ya*zh*limuun**a**

dan barangsiapa ringan timbangan (kebaikan)nya, maka mereka itulah orang yang telah merugikan dirinya sendiri, karena mereka mengingkari ayat-ayat Kami.

7:10

# وَلَقَدْ مَكَّنّٰكُمْ فِى الْاَرْضِ وَجَعَلْنَا لَكُمْ فِيْهَا مَعَايِشَۗ قَلِيْلًا مَّا تَشْكُرُوْنَ ࣖ

walaqad makkann*aa*kum fii **a**l-ar*dh*i waja'aln*aa* lakum fiih*aa* ma'*aa*yisya qaliilan m*aa* tasykuruun**a**

Dan sungguh, Kami telah menempatkan kamu di bumi dan di sana Kami sediakan (sumber) penghidupan untukmu. (Tetapi) sedikit sekali kamu bersyukur.

7:11

# وَلَقَدْ خَلَقْنٰكُمْ ثُمَّ صَوَّرْنٰكُمْ ثُمَّ قُلْنَا لِلْمَلٰۤىِٕكَةِ اسْجُدُوْا لِاٰدَمَ فَسَجَدُوْٓا اِلَّآ اِبْلِيْسَۗ لَمْ يَكُنْ مِّنَ السّٰجِدِيْنَ

walaqad khalaqn*aa*kum tsumma *sh*awwarn*aa*kum tsumma quln*aa* lilmal*aa*-ikati usjuduu li-*aa*dama fasajaduu ill*aa* ibliisa lam yakun mina **al**ss*aa*jidiin**a**

Dan sungguh, Kami telah menciptakan kamu, kemudian membentuk (tubuh)mu, kemudian Kami berfirman kepada para malaikat, “Bersujudlah kamu kepada Adam,” maka mereka pun sujud kecuali Iblis. Ia (Iblis) tidak termasuk mereka yang bersujud.

7:12

# قَالَ مَا مَنَعَكَ اَلَّا تَسْجُدَ اِذْ اَمَرْتُكَ ۗقَالَ اَنَا۠ خَيْرٌ مِّنْهُۚ خَلَقْتَنِيْ مِنْ نَّارٍ وَّخَلَقْتَهٗ مِنْ طِيْنٍ

q*aa*la m*aa* mana'aka **al**l*aa* tasjuda i*dz* amartuka q*aa*la an*aa* khayrun minhu khalaqtanii min n*aa*rin wakhalaqtahu min *th*iin**in**

(Allah) berfirman, “Apakah yang menghalangimu (sehingga) kamu tidak bersujud (kepada Adam) ketika Aku menyuruhmu?” (Iblis) menjawab, “Aku lebih baik daripada dia. Engkau ciptakan aku dari api, sedangkan dia Engkau ciptakan dari tanah.”

7:13

# قَالَ فَاهْبِطْ مِنْهَا فَمَا يَكُوْنُ لَكَ اَنْ تَتَكَبَّرَ فِيْهَا فَاخْرُجْ اِنَّكَ مِنَ الصّٰغِرِيْنَ

q*aa*la fa**i**hbi*th* minh*aa* fam*aa* yakuunu laka an tatakabbara fiih*aa* fa**u**khruj innaka mina **al***shshaa*ghiriin**a**

(Allah) berfirman, “Maka turunlah kamu darinya (surga); karena kamu tidak sepatutnya menyombongkan diri di dalamnya. Keluarlah! Sesungguhnya kamu termasuk makhluk yang hina.”

7:14

# قَالَ اَنْظِرْنِيْٓ اِلٰى يَوْمِ يُبْعَثُوْنَ

q*aa*la an*zh*irnii il*aa* yawmi yub'atsuun**a**

(Iblis) menjawab, “Berilah aku penangguhan waktu, sampai hari mereka dibangkitkan.”

7:15

# قَالَ اِنَّكَ مِنَ الْمُنْظَرِيْنَ

q*aa*la innaka mina **a**lmun*zh*ariin**a**

(Allah) berfirman, “Benar, kamu termasuk yang diberi penangguhan waktu.”

7:16

# قَالَ فَبِمَآ اَغْوَيْتَنِيْ لَاَقْعُدَنَّ لَهُمْ صِرَاطَكَ الْمُسْتَقِيْمَۙ

q*aa*la fabim*aa* aghwaytanii la-aq'udanna lahum *sh*ir*aath*aka **a**lmustaqiim**a**

(Iblis) menjawab, “Karena Engkau telah menyesatkan aku, pasti aku akan selalu menghalangi mereka dari jalan-Mu yang lurus,

7:17

# ثُمَّ لَاٰتِيَنَّهُمْ مِّنْۢ بَيْنِ اَيْدِيْهِمْ وَمِنْ خَلْفِهِمْ وَعَنْ اَيْمَانِهِمْ وَعَنْ شَمَاۤىِٕلِهِمْۗ وَلَا تَجِدُ اَكْثَرَهُمْ شٰكِرِيْنَ

tsumma la*aa*tiyannahum min bayni aydiihim wamin khalfihim wa'an aym*aa*nihim wa'an syam*aa*-ilihim wal*aa* tajidu aktsarahum sy*aa*kiriin**a**

kemudian pasti aku akan mendatangi mereka dari depan, dari belakang, dari kanan dan dari kiri mereka. Dan Engkau tidak akan mendapati kebanyakan mereka bersyukur.”

7:18

# قَالَ اخْرُجْ مِنْهَا مَذْءُوْمًا مَّدْحُوْرًا ۗ لَمَنْ تَبِعَكَ مِنْهُمْ لَاَمْلَـَٔنَّ جَهَنَّمَ مِنْكُمْ اَجْمَعِيْنَ

q*aa*la ukhruj minh*aa* ma*dz*uuman mad*h*uuran laman tabi'aka minhum la-amla-anna jahannama minkum ajma'iin**a**

(Allah) berfirman, “Keluarlah kamu dari sana (surga) dalam keadaan terhina dan terusir! Sesungguhnya barangsiapa di antara mereka ada yang mengikutimu, pasti akan Aku isi neraka Jahanam dengan kamu semua.”

7:19

# وَيٰٓاٰدَمُ اسْكُنْ اَنْتَ وَزَوْجُكَ الْجَنَّةَ فَكُلَا مِنْ حَيْثُ شِئْتُمَا وَلَا تَقْرَبَا هٰذِهِ الشَّجَرَةَ فَتَكُوْنَا مِنَ الظّٰلِمِيْنَ

way*aa* *aa*damu uskun anta wazawjuka **a**ljannata fakul*aa* min *h*aytsu syi/tum*aa* wal*aa* taqrab*aa* h*aadz*ihi **al**sysyajarata fatakuun*aa* mina **al***zhzhaa*

*Dan (Allah berfirman), “Wahai Adam! Tinggallah engkau bersama istrimu dalam surga dan makanlah apa saja yang kamu berdua sukai. Tetapi janganlah kamu berdua dekati pohon yang satu ini. (Apabila didekati) kamu berdua termasuk orang-orang yang zalim.”*









7:20

# فَوَسْوَسَ لَهُمَا الشَّيْطٰنُ لِيُبْدِيَ لَهُمَا مَا وٗرِيَ عَنْهُمَا مِنْ سَوْاٰتِهِمَا وَقَالَ مَا نَهٰىكُمَا رَبُّكُمَا عَنْ هٰذِهِ الشَّجَرَةِ ِالَّآ اَنْ تَكُوْنَا مَلَكَيْنِ اَوْ تَكُوْنَا مِنَ الْخٰلِدِيْنَ

fawaswasa lahum*aa* **al**sysyay*thaa*nu liyubdiya lahum*aa* m*aa* wuuriya 'anhum*aa* min saw-*aa*tihim*aa* waq*aa*la m*aa* nah*aa*kum*aa* rabbukum*aa* 'an h*aadz*ihi

Kemudian setan membisikkan pikiran jahat kepada mereka agar menampakkan aurat mereka (yang selama ini) tertutup. Dan (setan) berkata, “Tuhanmu hanya melarang kamu berdua mendekati pohon ini, agar kamu berdua tidak menjadi malaikat atau tidak menjadi orang

7:21

# وَقَاسَمَهُمَآ اِنِّيْ لَكُمَا لَمِنَ النّٰصِحِيْنَۙ

waq*aa*samahum*aa* innii lakum*aa* lamina **al**nn*aas*i*h*iin**a**

Dan dia (setan) bersumpah kepada keduanya, “Sesungguhnya aku ini benar-benar termasuk para penasihatmu,”

7:22

# فَدَلّٰىهُمَا بِغُرُورٍۚ فَلَمَّا ذَاقَا الشَّجَرَةَ بَدَتْ لَهُمَا سَوْاٰتُهُمَا وَطَفِقَا يَخْصِفٰنِ عَلَيْهِمَا مِنْ وَّرَقِ الْجَنَّةِۗ وَنَادٰىهُمَا رَبُّهُمَآ اَلَمْ اَنْهَكُمَا عَنْ تِلْكُمَا الشَّجَرَةِ وَاَقُلْ لَّكُمَآ اِنَّ الشَّيْطٰنَ لَكُمَ

fadall*aa*hum*aa* bighuruurin falamm*aa* *dzaa*q*aa* **al**sysyajarata badat lahum*aa* saw-*aa*tuhum*aa* wa*th*afiq*aa* yakh*sh*if*aa*ni 'alayhim*aa* min waraqi aljannati wan

dia (setan) membujuk mereka dengan tipu daya. Ketika mereka mencicipi (buah) pohon itu, tampaklah oleh mereka auratnya, maka mulailah mereka menutupinya dengan daun-daun surga. Tuhan menyeru mereka, “Bukankah Aku telah melarang kamu dari pohon itu dan Aku







7:23

# قَالَا رَبَّنَا ظَلَمْنَآ اَنْفُسَنَا وَاِنْ لَّمْ تَغْفِرْ لَنَا وَتَرْحَمْنَا لَنَكُوْنَنَّ مِنَ الْخٰسِرِيْنَ

q*aa*l*aa* rabban*aa* *zh*alamn*aa* anfusan*aa* wa-in lam taghfir lan*aa* watar*h*amn*aa* lanakuunanna mina **a**lkh*aa*siriin**a**

Keduanya berkata, “Ya Tuhan kami, kami telah menzalimi diri kami sendiri. Jika Engkau tidak mengampuni kami dan memberi rahmat kepada kami, niscaya kami termasuk orang-orang yang rugi.”

7:24

# قَالَ اهْبِطُوْا بَعْضُكُمْ لِبَعْضٍ عَدُوٌّ ۚوَلَكُمْ فِى الْاَرْضِ مُسْتَقَرٌّ وَّمَتَاعٌ اِلٰى حِيْنٍ

q*aa*la ihbi*th*uu ba'*dh*ukum liba'*dh*in 'aduwwun walakum fii **a**l-ar*dh*i mustaqarrun wamat*aa*'un il*aa* *h*iin**in**

(Allah) berfirman, “Turunlah kamu! Kamu akan saling bermusuhan satu sama lain. Bumi adalah tempat kediaman dan kesenanganmu sampai waktu yang telah ditentukan.”

7:25

# قَالَ فِيْهَا تَحْيَوْنَ وَفِيْهَا تَمُوْتُوْنَ وَمِنْهَا تُخْرَجُوْنَ ࣖ

q*aa*la fiih*aa* ta*h*yawna wafiih*aa* tamuutuuna waminh*aa* tukhrajuun**a**

(Allah) berfirman, “Di sana kamu hidup, di sana kamu mati, dan dari sana (pula) kamu akan dibangkitkan.”

7:26

# يَا بَنِيْٓ اٰدَمَ قَدْ اَنْزَلْنَا عَلَيْكُمْ لِبَاسًا يُّوَارِيْ سَوْاٰتِكُمْ وَرِيْشًاۗ وَلِبَاسُ التَّقْوٰى ذٰلِكَ خَيْرٌۗ ذٰلِكَ مِنْ اٰيٰتِ اللّٰهِ لَعَلَّهُمْ يَذَّكَّرُوْنَ

y*aa* banii *aa*dama qad anzaln*aa* 'alaykum lib*aa*san yuw*aa*rii saw-*aa*tikum wariisyan walib*aa*su **al**ttaqw*aa* *dzaa*lika khayrun *dzaa*lika min *aa*y*aa*ti **al**

**Wahai anak cucu Adam! Sesungguhnya Kami telah menyediakan pakaian untuk menutupi auratmu dan untuk perhiasan bagimu. Tetapi pakaian takwa, itulah yang lebih baik. Demikianlah sebagian tanda-tanda kekuasaan Allah, mudah-mudahan mereka ingat.**









7:27

# يٰبَنِيْٓ اٰدَمَ لَا يَفْتِنَنَّكُمُ الشَّيْطٰنُ كَمَآ اَخْرَجَ اَبَوَيْكُمْ مِّنَ الْجَنَّةِ يَنْزِعُ عَنْهُمَا لِبَاسَهُمَا لِيُرِيَهُمَا سَوْاٰتِهِمَا ۗاِنَّهٗ يَرٰىكُمْ هُوَ وَقَبِيْلُهٗ مِنْ حَيْثُ لَا تَرَوْنَهُمْۗ اِنَّا جَعَلْنَا الشَّيٰطِيْنَ اَ

y*aa* banii *aa*dama l*aa* yaftinannakumu **al**sysyay*thaa*nu kam*aa* akhraja abawaykum mina **a**ljannati yanzi'u 'anhum*aa* lib*aa*sahum*aa* liyuriyahum*aa* saw-*aa*tihi

Wahai anak cucu Adam! Janganlah sampai kamu tertipu oleh setan sebagaimana halnya dia (setan) telah mengeluarkan ibu bapakmu dari surga, dengan menanggalkan pakaian keduanya untuk memperlihatkan aurat keduanya. Sesungguhnya dia dan pengikutnya dapat melih

7:28

# وَاِذَا فَعَلُوْا فَاحِشَةً قَالُوْا وَجَدْنَا عَلَيْهَآ اٰبَاۤءَنَا وَاللّٰهُ اَمَرَنَا بِهَاۗ قُلْ اِنَّ اللّٰهَ لَا يَأْمُرُ بِالْفَحْشَاۤءِۗ اَتَقُوْلُوْنَ عَلَى اللّٰهِ مَا لَا تَعْلَمُوْنَ

wa-i*dzaa* fa'aluu f*aah*isyatan q*aa*luu wajadn*aa* 'alayh*aa* *aa*b*aa*-an*aa* wa**al**l*aa*hu amaran*aa* bih*aa* qul inna **al**l*aa*ha l*aa* ya/muru bi**a**

Dan apabila mereka melakukan perbuatan keji, mereka berkata, “Kami mendapati nenek moyang kami melakukan yang demikian, dan Allah menyuruh kami mengerjakannya.” Katakanlah, “Sesungguhnya Allah tidak pernah menyuruh berbuat keji. Mengapa kamu membicarakan







7:29

# قُلْ اَمَرَ رَبِّيْ بِالْقِسْطِۗ وَاَقِيْمُوْا وُجُوْهَكُمْ عِنْدَ كُلِّ مَسْجِدٍ وَّادْعُوْهُ مُخْلِصِيْنَ لَهُ الدِّيْنَ ەۗ كَمَا بَدَاَكُمْ تَعُوْدُوْنَۗ

qul amara rabbii bi**a**lqis*th*i wa-aqiimuu wujuuhakum 'inda kulli masjidin wa**u**d'uuhu mukhli*sh*iina lahu **al**ddiina kam*aa* bada-akum ta'uuduun**a**

Katakanlah, “Tuhanku menyuruhku berlaku adil. Hadapkanlah wajahmu (kepada Allah) pada setiap salat, dan sembahlah Dia dengan mengikhlaskan ibadah semata-mata hanya kepada-Nya. Kamu akan dikembalikan kepada-Nya sebagaimana kamu diciptakan semula.

7:30

# فَرِيْقًا هَدٰى وَفَرِيْقًا حَقَّ عَلَيْهِمُ الضَّلٰلَةُ ۗاِنَّهُمُ اتَّخَذُوا الشَّيٰطِيْنَ اَوْلِيَاۤءَ مِنْ دُوْنِ اللّٰهِ وَيَحْسَبُوْنَ اَنَّهُمْ مُّهْتَدُوْنَ

fariiqan had*aa* wafariiqan *h*aqqa 'alayhimu **al***dhdh*al*aa*latu innahumu ittakha*dz*uu **al**sysyay*aath*iina awliy*aa*-a min duuni **al**l*aa*hi waya*h*sabuuna annah

Sebagian diberi-Nya petunjuk dan sebagian lagi sepantasnya menjadi sesat. Mereka menjadikan setan-setan sebagai pelindung selain Allah. Mereka mengira bahwa mereka mendapat petunjuk.

7:31

# ۞ يٰبَنِيْٓ اٰدَمَ خُذُوْا زِيْنَتَكُمْ عِنْدَ كُلِّ مَسْجِدٍ وَّكُلُوْا وَاشْرَبُوْا وَلَا تُسْرِفُوْاۚ اِنَّهٗ لَا يُحِبُّ الْمُسْرِفِيْنَ ࣖ

y*aa* banii *aa*dama khu*dz*uu ziinatakum 'inda kulli masjidin wakuluu wa**i**syrabuu wal*aa* tusrifuu innahu l*aa* yu*h*ibbu **a**lmusrifiin**a**

Wahai anak cucu Adam! Pakailah pakaianmu yang bagus pada setiap (memasuki) masjid, makan dan minumlah, tetapi jangan berlebihan. Sungguh, Allah tidak menyukai orang yang berlebih-lebihan.

7:32

# قُلْ مَنْ حَرَّمَ زِيْنَةَ اللّٰهِ الَّتِيْٓ اَخْرَجَ لِعِبَادِهٖ وَالطَّيِّبٰتِ مِنَ الرِّزْقِۗ قُلْ هِيَ لِلَّذِيْنَ اٰمَنُوْا فِى الْحَيٰوةِ الدُّنْيَا خَالِصَةً يَّوْمَ الْقِيٰمَةِۗ كَذٰلِكَ نُفَصِّلُ الْاٰيٰتِ لِقَوْمٍ يَّعْلَمُوْنَ

qul man *h*arrama ziinata **al**l*aa*hi **al**latii akhraja li'ib*aa*dihi wa**al***ththh*ayyib*aa*ti mina **al**rrizqi qul hiya lilla*dz*iina *aa*manuu fii **a**

**Katakanlah (Muhammad), “Siapakah yang mengharamkan perhiasan dari Allah yang telah disediakan untuk hamba-hamba-Nya dan rezeki yang baik-baik? Katakanlah, “Semua itu untuk orang-orang yang beriman dalam kehidupan dunia, dan khusus (untuk mereka saja) pada**









7:33

# قُلْ اِنَّمَا حَرَّمَ رَبِّيَ الْفَوَاحِشَ مَا ظَهَرَ مِنْهَا وَمَا بَطَنَ وَالْاِثْمَ وَالْبَغْيَ بِغَيْرِ الْحَقِّ وَاَنْ تُشْرِكُوْا بِاللّٰهِ مَا لَمْ يُنَزِّلْ بِهٖ سُلْطٰنًا وَّاَنْ تَقُوْلُوْا عَلَى اللّٰهِ مَا لَا تَعْلَمُوْنَ

qul innam*aa* *h*arrama rabbiya **a**lfaw*aah*isya m*aa* *zh*ahara minh*aa* wam*aa* ba*th*ana wa**a**l-itsma wa**a**lbaghya bighayri **a**l*h*aqqi wa-an tusyr

Katakanlah (Muhammad), “Tuhanku hanya mengharamkan segala perbuatan keji yang terlihat dan yang tersembunyi, perbuatan dosa, perbuatan zalim tanpa alasan yang benar, dan (mengharamkan) kamu mempersekutukan Allah dengan sesuatu, sedangkan Dia tidak menurun

7:34

# وَلِكُلِّ اُمَّةٍ اَجَلٌۚ فَاِذَا جَاۤءَ اَجَلُهُمْ لَا يَسْتَأْخِرُوْنَ سَاعَةً وَّلَا يَسْتَقْدِمُوْنَ

walikulli ummatin ajalun fa-i*dzaa* j*aa*-a ajaluhum l*aa* yasta/khiruuna s*aa*'atan wal*aa* yastaqdimuun**a**

Dan setiap umat mempunyai ajal (batas waktu). Apabila ajalnya tiba, mereka tidak dapat meminta penundaan atau percepatan sesaat pun.

7:35

# يٰبَنِيْٓ اٰدَمَ اِمَّا يَأْتِيَنَّكُمْ رُسُلٌ مِّنْكُمْ يَقُصُّوْنَ عَلَيْكُمْ اٰيٰتِيْۙ فَمَنِ اتَّقٰى وَاَصْلَحَ فَلَا خَوْفٌ عَلَيْهِمْ وَلَا هُمْ يَحْزَنُوْنَ

y*aa* banii *aa*dama imm*aa* ya/tiyannakum rusulun minkum yaqu*shsh*uuna 'alaykum *aa*y*aa*tii famani ittaq*aa* wa-a*sh*la*h*a fal*aa* khawfun 'alayhim wal*aa* hum ya*h*zanuun**a**

Wahai anak cucu Adam! Jika datang kepadamu rasul-rasul dari kalanganmu sendiri, yang menceritakan ayat-ayat-Ku kepadamu, maka barangsiapa bertakwa dan mengadakan perbaikan, maka tidak ada rasa takut pada mereka, dan mereka tidak bersedih hati.

7:36

# وَالَّذِيْنَ كَذَّبُوْا بِاٰيٰتِنَا وَاسْتَكْبَرُوْا عَنْهَآ اُولٰۤىِٕكَ اَصْحٰبُ النَّارِۚ هُمْ فِيْهَا خٰلِدُوْنَ

wa**a**lla*dz*iina ka*dzdz*abuu bi-*aa*y*aa*tin*aa* wa**i**stakbaruu 'anh*aa* ul*aa*-ika a*sh*-*haa*bu **al**nn*aa*ri hum fiih*aa* kh*aa*liduun**a**

**Tetapi orang-orang yang mendustakan ayat-ayat Kami dan menyombongkan diri terhadapnya, mereka itulah penghuni neraka; mereka kekal di dalamnya.**









7:37

# فَمَنْ اَظْلَمُ مِمَّنِ افْتَرٰى عَلَى اللّٰهِ كَذِبًا اَوْ كَذَّبَ بِاٰيٰتِهٖۗ اُولٰۤىِٕكَ يَنَالُهُمْ نَصِيْبُهُمْ مِّنَ الْكِتٰبِۗ حَتّٰٓى اِذَا جَاۤءَتْهُمْ رُسُلُنَا يَتَوَفَّوْنَهُمْۙ قَالُوْٓا اَيْنَ مَا كُنْتُمْ تَدْعُوْنَ مِنْ دُوْنِ اللّٰهِ ۗقَ

faman a*zh*lamu mimmani iftar*aa* 'al*aa* **al**l*aa*hi ka*dz*iban aw ka*dzdz*aba bi-*aa*y*aa*tihi ul*aa*-ika yan*aa*luhum na*sh*iibuhum mina **a**lkit*aa*bi *h*att

Siapakah yang lebih zalim daripada orang yang mengada-adakan kebohongan terhadap Allah atau yang mendustakan ayat-ayat-Nya? Mereka itu akan memperoleh bagian yang telah ditentukan dalam Kitab sampai datang para utusan (malaikat) Kami kepada mereka untuk m

7:38

# قَالَ ادْخُلُوْا فِيْٓ اُمَمٍ قَدْ خَلَتْ مِنْ قَبْلِكُمْ مِّنَ الْجِنِّ وَالْاِنْسِ فِى النَّارِۙ كُلَّمَا دَخَلَتْ اُمَّةٌ لَّعَنَتْ اُخْتَهَا ۗحَتّٰٓى اِذَا ادَّارَكُوْا فِيْهَا جَمِيْعًا ۙقَالَتْ اُخْرٰىهُمْ لِاُوْلٰىهُمْ رَبَّنَا هٰٓؤُلَاۤءِ اَضَلُّو

q*aa*la udkhuluu fii umamin qad khalat min qablikum mina **a**ljinni wa**a**l-insi fii **al**nn*aa*ri kullam*aa* dakhalat ummatun la'anat ukhtah*aa* *h*att*aa* i*dzaa* idd*aa*r

Allah berfirman, “Masuklah kamu ke dalam api neraka bersama golongan jin dan manusia yang telah lebih dahulu dari kamu. Setiap kali suatu umat masuk, dia melaknat saudaranya, sehingga apabila mereka telah masuk semuanya, berkatalah orang yang (masuk) bela

7:39

# وَقَالَتْ اُوْلٰىهُمْ لِاُخْرٰىهُمْ فَمَا كَانَ لَكُمْ عَلَيْنَا مِنْ فَضْلٍ فَذُوْقُوا الْعَذَابَ بِمَا كُنْتُمْ تَكْسِبُوْنَ ࣖ

waq*aa*lat uul*aa*hum li-ukhr*aa*hum fam*aa* k*aa*na lakum 'alayn*aa* min fa*dh*lin fa*dz*uuquu **a**l'a*dzaa*ba bim*aa* kuntum taksibuun**a**

Dan orang yang (masuk) terlebih dahulu berkata kepada yang (masuk) belakangan, “Kamu tidak mempunyai kelebihan sedikit pun atas kami. Maka rasakanlah azab itu karena perbuatan yang telah kamu lakukan.”

7:40

# اِنَّ الَّذِيْنَ كَذَّبُوْا بِاٰيٰتِنَا وَاسْتَكْبَرُوْا عَنْهَا لَا تُفَتَّحُ لَهُمْ اَبْوَابُ السَّمَاۤءِ وَلَا يَدْخُلُوْنَ الْجَنَّةَ حَتّٰى يَلِجَ الْجَمَلُ فِيْ سَمِّ الْخِيَاطِ ۗ وَكَذٰلِكَ نَجْزِى الْمُجْرِمِيْنَ

inna **al**la*dz*iina ka*dzdz*abuu bi-*aa*y*aa*tin*aa* wa**i**stakbaruu 'anh*aa* l*aa* tufatta*h*u lahum abw*aa*bu **al**ssam*aa*-i wal*aa* yadkhuluuna **a**

Sesungguhnya orang-orang yang mendustakan ayat-ayat Kami dan menyombongkan diri terhadapnya, tidak akan dibukakan pintu-pintu langit bagi mereka, dan mereka tidak akan masuk surga, sebelum unta masuk ke dalam lubang jarum. Demikianlah Kami memberi balasan







7:41

# لَهُمْ مِّنْ جَهَنَّمَ مِهَادٌ وَّمِنْ فَوْقِهِمْ غَوَاشٍۗ وَكَذٰلِكَ نَجْزِى الظّٰلِمِيْنَ

lahum min jahannama mih*aa*dun wamin fawqihim ghaw*aa*syin waka*dzaa*lika najzii **al***zhzhaa*limiin**a**

Bagi mereka tikar tidur dari api neraka dan di atas mereka ada selimut (api neraka). Demikianlah Kami memberi balasan kepada orang-orang yang zalim.

7:42

# وَالَّذِيْنَ اٰمَنُوْا وَعَمِلُوا الصّٰلِحٰتِ لَا نُكَلِّفُ نَفْسًا اِلَّا وُسْعَهَآ اُولٰۤىِٕكَ اَصْحٰبُ الْجَنَّةِۚ هُمْ فِيْهَا خٰلِدُوْنَ

wa**a**lla*dz*iina *aa*manuu wa'amiluu **al***shshaa*li*haa*ti l*aa* nukallifu nafsan ill*aa* wus'ah*aa* ul*aa*-ika a*sh*-*haa*bu **a**ljannati hum fiih*aa* kh

Dan orang-orang yang beriman serta mengerjakan kebajikan, Kami tidak akan membebani seseorang melainkan menurut kesanggupannya. Mereka itulah penghuni surga; mereka kekal di dalamnya,

7:43

# وَنَزَعْنَا مَا فِيْ صُدُوْرِهِمْ مِّنْ غِلٍّ تَجْرِيْ مِنْ تَحْتِهِمُ الْانْهٰرُۚ وَقَالُوا الْحَمْدُ لِلّٰهِ الَّذِيْ هَدٰىنَا لِهٰذَاۗ وَمَا كُنَّا لِنَهْتَدِيَ لَوْلَآ اَنْ هَدٰىنَا اللّٰهُ ۚ لَقَدْ جَاۤءَتْ رُسُلُ رَبِّنَا بِالْحَقِّۗ وَنُوْدُوْٓا ا

wanaza'n*aa* m*aa* fii *sh*uduurihim min ghillin tajrii min ta*h*tihimu **a**l-anh*aa*ru waq*aa*luu **a**l*h*amdu lill*aa*hi **al**la*dz*ii had*aa*n*aa* lih*aa*

dan Kami mencabut rasa dendam dari dalam dada mereka, di bawahnya mengalir sungai-sungai. Mereka berkata, “Segala puji bagi Allah yang telah menunjukkan kami ke (surga) ini. Kami tidak akan mendapat petunjuk sekiranya Allah tidak menunjukkan kami. Sesungg







7:44

# وَنَادٰٓى اَصْحٰبُ الْجَنَّةِ اَصْحٰبَ النَّارِ اَنْ قَدْ وَجَدْنَا مَا وَعَدَنَا رَبُّنَا حَقًّا فَهَلْ وَجَدْتُّمْ مَّا وَعَدَ رَبُّكُمْ حَقًّا ۗقَالُوْا نَعَمْۚ فَاَذَّنَ مُؤَذِّنٌۢ بَيْنَهُمْ اَنْ لَّعْنَةُ اللّٰهِ عَلَى الظّٰلِمِيْنَ

wan*aa*d*aa* a*sh*-*haa*bu **a**ljannati a*sh*-*haa*ba **al**nn*aa*ri an qad wajadn*aa* m*aa* wa'adan*aa* rabbun*aa* *h*aqqan fahal wajadtum m*aa* wa'ada rabbukum <

Dan para penghuni surga menyeru penghuni-penghuni neraka, “Sungguh, kami telah memperoleh apa yang dijanjikan Tuhan kepada kami itu benar. Apakah kamu telah memperoleh apa yang dijanjikan Tuhan kepadamu itu benar?” Mereka menjawab, “Benar.” Kemudian penye

7:45

# اَلَّذِيْنَ يَصُدُّوْنَ عَنْ سَبِيْلِ اللّٰهِ وَيَبْغُوْنَهَا عِوَجًاۚ وَهُمْ بِالْاٰخِرَةِ كٰفِرُوْنَۘ

**al**la*dz*iina ya*sh*udduuna 'an sabiili **al**l*aa*hi wayabghuunah*aa* 'iwajan wahum bi**a**l-*aa*khirati k*aa*firuun**a**

(yaitu) orang-orang yang menghalang-halangi (orang lain) dari jalan Allah dan ingin membelokkannya. Mereka itulah yang mengingkari kehidupan akhirat.”

7:46

# وَبَيْنَهُمَا حِجَابٌۚ وَعَلَى الْاَعْرَافِ رِجَالٌ يَّعْرِفُوْنَ كُلًّا ۢ بِسِيْمٰىهُمْۚ وَنَادَوْا اَصْحٰبَ الْجَنَّةِ اَنْ سَلٰمٌ عَلَيْكُمْۗ لَمْ يَدْخُلُوْهَا وَهُمْ يَطْمَعُوْنَ

wabaynahum*aa* *h*ij*aa*bun wa'al*aa* **a**l-a'r*aa*fi rij*aa*lun ya'rifuuna kullan bisiim*aa*hum wan*aa*daw a*sh*-*haa*ba **a**ljannati an sal*aa*mun 'alaykum lam yadkhuluuh

Dan di antara keduanya (penghuni surga dan neraka) ada tabir dan di atas A‘raf (tempat yang tertinggi) ada orang-orang yang saling mengenal, masing-masing dengan tanda-tandanya. Mereka menyeru penghuni surga, “Salamun ‘alaikum” (salam sejahtera bagimu). M

7:47

# ۞ وَاِذَا صُرِفَتْ اَبْصَارُهُمْ تِلْقَاۤءَ اَصْحٰبِ النَّارِۙ قَالُوْا رَبَّنَا لَا تَجْعَلْنَا مَعَ الْقَوْمِ الظّٰلِمِيْنَ ࣖ

wa-i*dzaa* *sh*urifat ab*shaa*ruhum tilq*aa*-a a*sh*-*haa*bi **al**nn*aa*ri q*aa*luu rabban*aa* l*aa* taj'aln*aa* ma'a **a**lqawmi **al***zhzhaa*limiin

Dan apabila pandangan mereka dialihkan ke arah penghuni neraka, mereka berkata, “Ya Tuhan kami, janganlah Engkau tempatkan kami bersama-sama orang-orang zalim itu.”

7:48

# وَنَادٰٓى اَصْحٰبُ الْاَعْرَافِ رِجَالًا يَّعْرِفُوْنَهُمْ بِسِيْمٰىهُمْ قَالُوْا مَآ اَغْنٰى عَنْكُمْ جَمْعُكُمْ وَمَا كُنْتُمْ تَسْتَكْبِرُوْنَ

wan*aa*d*aa* a*sh*-*haa*bu **a**l-a'r*aa*fi rij*aa*lan ya'rifuunahum bisiim*aa*hum q*aa*luu m*aa* aghn*aa* 'ankum jam'ukum wam*aa* kuntum tastakbiruun**a**

Dan orang-orang di atas A‘raf (tempat yang tertinggi) menyeru orang-orang yang mereka kenal dengan tanda-tandanya sambil berkata, “Harta yang kamu kumpulkan dan apa yang kamu sombongkan, (ternyata) tidak ada manfaatnya buat kamu.

7:49

# اَهٰٓؤُلَاۤءِ الَّذِيْنَ اَقْسَمْتُمْ لَا يَنَالُهُمُ اللّٰهُ بِرَحْمَةٍۗ اُدْخُلُوا الْجَنَّةَ لَا خَوْفٌ عَلَيْكُمْ وَلَآ اَنْتُمْ تَحْزَنُوْنَ

ah*aa*ul*aa*-i **al**la*dz*iina aqsamtum l*aa* yan*aa*luhumu **al**l*aa*hu bira*h*matin udkhuluu **a**ljannata l*aa* khawfun 'alaykum wal*aa* antum ta*h*zanuun

Itukah orang-orang yang kamu telah bersumpah, bahwa mereka tidak akan mendapat rahmat Allah?” (Allah berfirman), “Masuklah kamu ke dalam surga! Tidak ada rasa takut padamu dan kamu tidak pula akan bersedih hati.”

7:50

# وَنَادٰٓى اَصْحٰبُ النَّارِ اَصْحٰبَ الْجَنَّةِ اَنْ اَفِيْضُوْا عَلَيْنَا مِنَ الْمَاۤءِ اَوْ مِمَّا رَزَقَكُمُ اللّٰهُ ۗقَالُوْٓا اِنَّ اللّٰهَ حَرَّمَهُمَا عَلَى الْكٰفِرِيْنَۙ

wan*aa*d*aa* a*sh*-*haa*bu **al**n*aa*ri a*sh*-*haa*ba **a**ljannati an afii*dh*uu 'alayn*aa* mina **a**lm*aa*-i aw mimm*aa* razaqakumu **al**l*aa*

Para penghuni neraka menyeru para penghuni surga, “Tuangkanlah (sedikit) air kepada kami atau rezeki apa saja yang telah dikaruniakan Allah kepadamu.” Mereka menjawab, “Sungguh, Allah telah mengharamkan keduanya bagi orang-orang kafir,”







7:51

# الَّذِيْنَ اتَّخَذُوْا دِيْنَهُمْ لَهْوًا وَّلَعِبًا وَّغَرَّتْهُمُ الْحَيٰوةُ الدُّنْيَاۚ فَالْيَوْمَ نَنْسٰىهُمْ كَمَا نَسُوْا لِقَاۤءَ يَوْمِهِمْ هٰذَاۙ وَمَا كَانُوْا بِاٰيٰتِنَا يَجْحَدُوْنَ

**al**la*dz*iina ittakha*dz*uu diinahum lahwan wala'iban wagharrat-humu **a**l*h*ay*aa*tu **al**dduny*aa* fa**a**lyawma nans*aa*hum kam*aa* nasuu liq*aa*-a yawmihi

(yaitu) orang-orang yang menjadikan agamanya sebagai permainan dan senda gurau, dan mereka telah tertipu oleh kehidupan dunia. Maka pada hari ini (Kiamat), Kami melupakan mereka sebagaimana mereka dahulu melupakan pertemuan hari ini, dan karena mereka men

7:52

# وَلَقَدْ جِئْنٰهُمْ بِكِتٰبٍ فَصَّلْنٰهُ عَلٰى عِلْمٍ هُدًى وَّرَحْمَةً لِّقَوْمٍ يُّؤْمِنُوْنَ

walaqad ji/n*aa*hum bikit*aa*bin fa*shsh*aln*aa*hu 'al*aa* 'ilmin hudan wara*h*matan liqawmin yu/minuun**a**

Sungguh, Kami telah mendatangkan Kitab (Al-Qur'an) kepada mereka, yang Kami jelaskan atas dasar pengetahuan, sebagai petunjuk dan rahmat bagi orang-orang yang beriman.

7:53

# هَلْ يَنْظُرُوْنَ اِلَّا تَأْوِيْلَهٗۗ يَوْمَ يَأْتِيْ تَأْوِيْلُهٗ يَقُوْلُ الَّذِيْنَ نَسُوْهُ مِنْ قَبْلُ قَدْ جَاۤءَتْ رُسُلُ رَبِّنَا بِالْحَقِّۚ فَهَلْ لَّنَا مِنْ شُفَعَاۤءَ فَيَشْفَعُوْا لَنَآ اَوْ نُرَدُّ فَنَعْمَلَ غَيْرَ الَّذِيْ كُنَّا نَعْمَ

hal yan*zh*uruuna ill*aa* ta/wiilahu yawma ya/tii ta/wiiluhu yaquulu **al**la*dz*iina nasuuhu min qablu qad j*aa*-at rusulu rabbin*aa* bi**a**l*h*aqqi fahal lan*aa* min syufa'*aa*-a fayasyf

Tidakkah mereka hanya menanti-nanti bukti kebenaran (Al-Qur'an) itu. Pada hari bukti kebenaran itu tiba, orang-orang yang sebelum itu mengabaikannya berkata, “Sungguh, rasul-rasul Tuhan kami telah datang membawa kebenaran. Maka adakah pemberi syafaat bagi

7:54

# اِنَّ رَبَّكُمُ اللّٰهُ الَّذِيْ خَلَقَ السَّمٰوٰتِ وَالْاَرْضَ فِيْ سِتَّةِ اَيَّامٍ ثُمَّ اسْتَوٰى عَلَى الْعَرْشِۗ يُغْشِى الَّيْلَ النَّهَارَ يَطْلُبُهٗ حَثِيْثًاۙ وَّالشَّمْسَ وَالْقَمَرَ وَالنُّجُوْمَ مُسَخَّرٰتٍۢ بِاَمْرِهٖٓ ۙاَلَا لَهُ الْخَلْقُ

inna rabbakumu **al**l*aa*hu **al**la*dz*ii khalaqa **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*a fii sittati ayy*aa*min tsumma istaw*aa* 'al*aa* **a**l'arsy

Sungguh, Tuhanmu (adalah) Allah yang menciptakan langit dan bumi dalam enam masa, lalu Dia bersemayam di atas ‘Arsy. Dia menutupkan malam kepada siang yang mengikutinya dengan cepat. (Dia ciptakan) matahari, bulan dan bintang-bintang tunduk kepada perinta

7:55

# اُدْعُوْا رَبَّكُمْ تَضَرُّعًا وَّخُفْيَةً ۗاِنَّهٗ لَا يُحِبُّ الْمُعْتَدِيْنَۚ

ud'uu rabbakum ta*dh*arru'an wakhufyatan innahu l*aa* yu*h*ibbu **a**lmu'tadiin**a**

Berdoalah kepada Tuhanmu dengan rendah hati dan suara yang lembut. Sungguh, Dia tidak menyukai orang-orang yang melampaui batas.

7:56

# وَلَا تُفْسِدُوْا فِى الْاَرْضِ بَعْدَ اِصْلَاحِهَا وَادْعُوْهُ خَوْفًا وَّطَمَعًاۗ اِنَّ رَحْمَتَ اللّٰهِ قَرِيْبٌ مِّنَ الْمُحْسِنِيْنَ

wal*aa* tufsiduu fii **a**l-ar*dh*i ba'da i*sh*l*aah*ih*aa* wa**u**d'uuhu khawfan wa*th*ama'an inna ra*h*mata **al**l*aa*hi qariibun mina **a**lmu*h*siniin

Dan janganlah kamu berbuat kerusakan di bumi setelah (diciptakan) dengan baik. Berdoalah kepada-Nya dengan rasa takut dan penuh harap. Sesungguhnya rahmat Allah sangat dekat kepada orang yang berbuat kebaikan.

7:57

# وَهُوَ الَّذِيْ يُرْسِلُ الرِّيٰحَ بُشْرًاۢ بَيْنَ يَدَيْ رَحْمَتِهٖۗ حَتّٰٓى اِذَآ اَقَلَّتْ سَحَابًا ثِقَالًا سُقْنٰهُ لِبَلَدٍ مَّيِّتٍ فَاَنْزَلْنَا بِهِ الْمَاۤءَ فَاَخْرَجْنَا بِهٖ مِنْ كُلِّ الثَّمَرٰتِۗ كَذٰلِكَ نُخْرِجُ الْمَوْتٰى لَعَلَّكُمْ ت

wahuwa **al**la*dz*ii yursilu **al**rriy*aah*a busyran bayna yaday ra*h*matihi *h*att*aa* i*dzaa* aqallat sa*haa*ban tsiq*aa*lan suqn*aa*hu libaladin mayyitin fa-anzaln*aa* bihi <

Dialah yang meniupkan angin sebagai pembawa kabar gembira, mendahului kedatangan rahmat-Nya (hujan), sehingga apabila angin itu membawa awan mendung, Kami halau ke suatu daerah yang tandus, lalu Kami turunkan hujan di daerah itu. Kemudian Kami tumbuhkan d

7:58

# وَالْبَلَدُ الطَّيِّبُ يَخْرُجُ نَبَاتُهٗ بِاِذْنِ رَبِّهٖۚ وَالَّذِيْ خَبُثَ لَا يَخْرُجُ اِلَّا نَكِدًاۗ كَذٰلِكَ نُصَرِّفُ الْاٰيٰتِ لِقَوْمٍ يَّشْكُرُوْنَ ࣖ

wa**a**lbaladu **al***ththh*ayyibu yakhruju nab*aa*tuhu bi-i*dz*ni rabbihi wa**a**lla*dz*ii khabutsa l*aa* yakhruju ill*aa* nakidan ka*dzaa*lika nu*sh*arrifu **a**

Dan tanah yang baik, tanaman-tanamannya tumbuh subur dengan izin Tuhan; dan tanah yang buruk, tanaman-tanamannya yang tumbuh merana. Demikianlah Kami menjelaskan berulang-ulang tanda-tanda (kebesaran Kami) bagi orang-orang yang bersyukur.

7:59

# لَقَدْ اَرْسَلْنَا نُوْحًا اِلٰى قَوْمِهٖ فَقَالَ يٰقَوْمِ اعْبُدُوا اللّٰهَ مَا لَكُمْ مِّنْ اِلٰهٍ غَيْرُهٗۗ اِنِّيْٓ اَخَافُ عَلَيْكُمْ عَذَابَ يَوْمٍ عَظِيْمٍ

laqad arsaln*aa* nuu*h*an il*aa* qawmihi faq*aa*la y*aa* qawmi u'buduu **al**l*aa*ha m*aa* lakum min il*aa*hin ghayruhu innii akh*aa*fu 'alaykum 'a*dzaa*ba yawmin 'a*zh*iim**in**

**Sungguh, Kami benar-benar telah mengutus Nuh kepada kaumnya, lalu dia berkata, “Wahai kaumku! Sembahlah Allah! Tidak ada tuhan (sembahan) bagimu selain Dia. Sesungguhnya aku takut kamu akan ditimpa azab pada hari yang dahsyat (kiamat).**









7:60

# قَالَ الْمَلَاُ مِنْ قَوْمِهٖٓ اِنَّا لَنَرٰىكَ فِيْ ضَلٰلٍ مُّبِيْنٍ

q*aa*la **a**lmalau min qawmihi inn*aa* lanar*aa*ka fii *dh*al*aa*lin mubiin**in**

Pemuka-pemuka kaumnya berkata, “Sesungguhnya kami memandang kamu benar-benar berada dalam kesesatan yang nyata.”

7:61

# قَالَ يٰقَوْمِ لَيْسَ بِيْ ضَلٰلَةٌ وَّلٰكِنِّيْ رَسُوْلٌ مِّنْ رَّبِّ الْعٰلَمِيْنَ

q*aa*la y*aa* qawmi laysa bii *dh*al*aa*latun wal*aa*kinnii rasuulun min rabbi **a**l'*aa*lamiin**a**

Dia (Nuh) menjawab, “Wahai kaumku! Aku tidak sesat; tetapi aku ini seorang Rasul dari Tuhan seluruh alam.

7:62

# اُبَلِّغُكُمْ رِسٰلٰتِ رَبِّيْ وَاَنْصَحُ لَكُمْ وَاَعْلَمُ مِنَ اللّٰهِ مَا لَا تَعْلَمُوْنَ

uballighukum ris*aa*l*aa*ti rabbii wa-an*sh*a*h*u lakum wa-a'lamu mina **al**l*aa*hi m*aa* l*aa* ta'lamuun**a**

Aku menyampaikan kepadamu amanat Tuhanku, memberi nasihat kepadamu, dan aku mengetahui dari Allah apa yang tidak kamu ketahui.”

7:63

# اَوَعَجِبْتُمْ اَنْ جَاۤءَكُمْ ذِكْرٌ مِّنْ رَّبِّكُمْ عَلٰى رَجُلٍ مِّنْكُمْ لِيُنْذِرَكُمْ وَلِتَتَّقُوْا وَلَعَلَّكُمْ تُرْحَمُوْنَ

awa 'ajibtum an j*aa*-akum *dz*ikrun min rabbikum 'al*aa* rajulin minkum liyun*dz*irakum walitattaquu wala'allakum tur*h*amuun**a**

Dan herankah kamu bahwa ada peringatan yang datang dari Tuhanmu melalui seorang laki-laki dari kalanganmu sendiri, untuk memberi peringatan kepadamu dan agar kamu bertakwa, sehingga kamu mendapat rahmat?

7:64

# فَكَذَّبُوْهُ فَاَنْجَيْنٰهُ وَالَّذِيْنَ مَعَهٗ فِى الْفُلْكِ وَاَغْرَقْنَا الَّذِيْنَ كَذَّبُوْا بِاٰيٰتِنَاۗ اِنَّهُمْ كَانُوْا قَوْمًا عَمِيْنَ ࣖ

faka*dzdz*abuuhu fa-anjayn*aa*hu wa**a**lla*dz*iina ma'ahu fii **a**lfulki wa-aghraqn*aa* **al**la*dz*iina ka*dzdz*abuu bi-*aa*y*aa*tin*aa* innahum k*aa*nuu qawman 'a

Maka mereka mendustakannya (Nuh). Lalu Kami selamatkan dia dan orang-orang yang bersamanya di dalam kapal. Kami tenggelamkan orang-orang yang mendustakan ayat-ayat Kami. Sesungguhnya mereka adalah kaum yang buta (mata hatinya).

7:65

# ۞ وَاِلٰى عَادٍ اَخَاهُمْ هُوْدًاۗ قَالَ يٰقَوْمِ اعْبُدُوا اللّٰهَ مَا لَكُمْ مِّنْ اِلٰهٍ غَيْرُهٗۗ اَفَلَا تَتَّقُوْنَ

wa-il*aa* '*aa*din akh*aa*hum huudan q*aa*la y*aa* qawmi u'buduu **al**l*aa*ha m*aa* lakum min il*aa*hin ghayruhu afal*aa* tattaquun**a**

Dan kepada kaum ‘Ad (Kami utus) Hud, saudara mereka. Dia berkata, “Wahai kaumku! Sembahlah Allah! Tidak ada tuhan (sembahan) bagimu selain Dia. Maka mengapa kamu tidak bertakwa?”

7:66

# قَالَ الْمَلَاُ الَّذِيْنَ كَفَرُوْا مِنْ قَوْمِهٖٓ اِنَّا لَنَرٰىكَ فِيْ سَفَاهَةٍ وَّاِنَّا لَنَظُنُّكَ مِنَ الْكٰذِبِيْنَ

q*aa*la **a**lmalau **al**la*dz*iina kafaruu min qawmihi inn*aa* lanar*aa*ka fii saf*aa*hatin wa-inn*aa* lana*zh*unnuka mina **a**lk*aadz*ibiin**a**

Pemuka-pemuka orang-orang yang kafir dari kaumnya berkata, “Sesungguhnya kami memandang kamu benar-benar kurang waras dan kami kira kamu termasuk orang-orang yang berdusta.”

7:67

# قَالَ يٰقَوْمِ لَيْسَ بِيْ سَفَاهَةٌ وَّلٰكِنِّيْ رَسُوْلٌ مِّنْ رَّبِّ الْعٰلَمِيْنَ

q*aa*la y*aa* qawmi laysa bii saf*aa*hatun wal*aa*kinnii rasuulun min rabbi **a**l'*aa*lamiin**a**

Dia (Hud) menjawab, “Wahai kaumku! Bukan aku kurang waras, tetapi aku ini adalah Rasul dari Tuhan seluruh alam.

7:68

# اُبَلِّغُكُمْ رِسٰلٰتِ رَبِّيْ وَاَنَا۠ لَكُمْ نَاصِحٌ اَمِيْنٌ

uballighukum ris*aa*l*aa*ti rabbii wa-an*aa* lakum n*aas*i*h*un amiin**un**

Aku menyampaikan kepadamu amanat Tuhanku dan pemberi nasihat yang terpercaya kepada kamu.

7:69

# اَوَعَجِبْتُمْ اَنْ جَاۤءَكُمْ ذِكْرٌ مِّنْ رَّبِّكُمْ عَلٰى رَجُلٍ مِّنْكُمْ لِيُنْذِرَكُمْۗ وَاذْكُرُوْٓا اِذْ جَعَلَكُمْ خُلَفَاۤءَ مِنْۢ بَعْدِ قَوْمِ نُوْحٍ وَّزَادَكُمْ فِى الْخَلْقِ بَصْۣطَةً ۚفَاذْكُرُوْٓا اٰلَاۤءَ اللّٰهِ لَعَلَّكُمْ تُفْلِحُوْنَ

awa 'ajibtum an j*aa*-akum *dz*ikrun min rabbikum 'al*aa* rajulin minkum liyun*dz*irakum wa**u***dz*kuruu i*dz* ja'alakum khulaf*aa*-a min ba'di qawmi nuu*h*in waz*aa*dakum fii **a**lk

Dan herankah kamu bahwa ada peringatan yang datang dari Tuhanmu melalui seorang laki-laki dari kalanganmu sendiri, untuk memberi peringatan kepadamu? Ingatlah ketika Dia menjadikan kamu sebagai khalifah-khalifah setelah kaum Nuh, dan Dia lebihkan kamu dal

7:70

# قَالُوْٓا اَجِئْتَنَا لِنَعْبُدَ اللّٰهَ وَحْدَهٗ وَنَذَرَ مَا كَانَ يَعْبُدُ اٰبَاۤؤُنَاۚ فَأْتِنَا بِمَا تَعِدُنَآ اِنْ كُنْتَ مِنَ الصّٰدِقِيْنَ

q*aa*luu aji/tan*aa* lina'buda **al**l*aa*ha wa*h*dahu wana*dz*ara m*aa* k*aa*na ya'budu *aa*b*aa*un*aa* fa/tin*aa* bim*aa* ta'idun*aa* in kunta mina **al***shsha*

Mereka berkata, “Apakah kedatanganmu kepada kami, agar kami hanya menyembah Allah saja dan meninggalkan apa yang biasa disembah oleh nenek moyang kami? Maka buktikanlah ancamanmu kepada kami, jika kamu benar!”







7:71

# قَالَ قَدْ وَقَعَ عَلَيْكُمْ مِّنْ رَّبِّكُمْ رِجْسٌ وَّغَضَبٌۗ اَتُجَادِلُوْنَنِيْ فِيْٓ اَسْمَاۤءٍ سَمَّيْتُمُوْهَآ اَنْتُمْ وَاٰبَاۤؤُكُمْ مَّا نَزَّلَ اللّٰهُ بِهَا مِنْ سُلْطٰنٍۗ فَانْتَظِرُوْٓا اِنِّيْ مَعَكُمْ مِّنَ الْمُنْتَظِرِيْنَ

q*aa*la qad waqa'a 'alaykum min rabbikum rijsun wagha*dh*abun atuj*aa*diluunanii fii asm*aa*-in sammaytumuuh*aa* antum wa*aa*b*aa*ukum m*aa* nazzala **al**l*aa*hu bih*aa* min sul*thaa*

Dia (Hud) menjawab, “Sungguh, kebencian dan kemurkaan dari Tuhan akan menimpa kamu. Apakah kamu hendak berbantah denganku tentang nama-nama (berhala) yang kamu dan nenek moyangmu buat sendiri, padahal Allah tidak menurunkan keterangan untuk itu? Jika demi







7:72

# فَاَنْجَيْنٰهُ وَالَّذِيْنَ مَعَهٗ بِرَحْمَةٍ مِّنَّا وَقَطَعْنَا دَابِرَ الَّذِيْنَ كَذَّبُوْا بِاٰيٰتِنَا وَمَا كَانُوْا مُؤْمِنِيْنَ ࣖ

fa-anjayn*aa*hu wa**a**lla*dz*iina ma'ahu bira*h*matin minn*aa* waqa*th*a'n*aa* d*aa*bira **al**la*dz*iina ka*dzdz*abuu bi-*aa*y*aa*tin*aa* wam*aa* k*aa*nuu mu

Maka Kami selamatkan dia (Hud) dan orang-orang yang bersamanya dengan rahmat Kami dan Kami musnahkan sampai ke akar-akarnya orang-orang yang mendustakan ayat-ayat Kami. Mereka bukanlah orang-orang beriman.

7:73

# وَاِلٰى ثَمُوْدَ اَخَاهُمْ صٰلِحًاۘ قَالَ يٰقَوْمِ اعْبُدُوا اللّٰهَ مَا لَكُمْ مِّنْ اِلٰهٍ غَيْرُهٗۗ قَدْ جَاۤءَتْكُمْ بَيِّنَةٌ مِّنْ رَّبِّكُمْۗ هٰذِهٖ نَاقَةُ اللّٰهِ لَكُمْ اٰيَةً فَذَرُوْهَا تَأْكُلْ فِيْٓ اَرْضِ اللّٰهِ وَلَا تَمَسُّوْهَا بِسُوْۤء

wa-il*aa* tsamuuda akh*aa*hum *shaa*li*h*an q*aa*la y*aa* qawmi u'buduu **al**l*aa*ha m*aa* lakum min il*aa*hin ghayruhu qad j*aa*-atkum bayyinatun min rabbikum h*aadz*ihi n*aa*qatu

Dan kepada kaum Samud (Kami utus) saudara mereka Saleh. Dia berkata, “Wahai kaumku! Sembahlah Allah! Tidak ada tuhan (sembahan) bagimu selain Dia. Sesungguhnya telah datang kepadamu bukti yang nyata dari Tuhanmu. Ini (seekor) unta betina dari Allah sebaga

7:74

# وَاذْكُرُوْٓا اِذْ جَعَلَكُمْ خُلَفَاۤءَ مِنْۢ بَعْدِ عَادٍ وَّبَوَّاَكُمْ فِى الْاَرْضِ تَتَّخِذُوْنَ مِنْ سُهُوْلِهَا قُصُوْرًا وَّتَنْحِتُوْنَ الْجِبَالَ بُيُوْتًا ۚفَاذْكُرُوْٓا اٰلَاۤءَ اللّٰهِ وَلَا تَعْثَوْا فِى الْاَرْضِ مُفْسِدِيْنَ

wa**u***dz*kuruu i*dz* ja'alakum khulaf*aa*-a min ba'di '*aa*din wabawwa-akum fii **a**l-ar*dh*i tattakhi*dz*uuna min suhuulih*aa* qu*sh*uuran watan*h*ituuna **a**ljib*a*

Dan ingatlah ketika Dia menjadikan kamu khalifah-khalifah setelah kaum ‘Ad dan menempatkan kamu di bumi. Di tempat yang datar kamu dirikan istana-istana dan di bukit-bukit kamu pahat menjadi rumah-rumah. Maka ingatlah nikmat-nikmat Allah dan janganlah kam







7:75

# قَالَ الْمَلَاُ الَّذِيْنَ اسْتَكْبَرُوْا مِنْ قَوْمِهٖ لِلَّذِيْنَ اسْتُضْعِفُوْا لِمَنْ اٰمَنَ مِنْهُمْ اَتَعْلَمُوْنَ اَنَّ صٰلِحًا مُّرْسَلٌ مِّنْ رَّبِّهٖۗ قَالُوْٓا اِنَّا بِمَآ اُرْسِلَ بِهٖ مُؤْمِنُوْنَ

q*aa*la **a**lmalau **al**la*dz*iina istakbaruu min qawmihi lilla*dz*iina istu*dh*'ifuu liman *aa*mana minhum ata'lamuuna anna *shaa*li*h*an mursalun min rabbihi q*aa*luu inn*aa* bim

Pemuka-pemuka yang menyombongkan diri berkata kepada orang-orang yang dianggap lemah, yaitu orang-orang yang telah beriman di antara kaumnya, “Tahukah kamu bahwa Saleh adalah seorang rasul dari Tuhannya?” Mereka menjawab, “Sesungguhnya kami percaya kepada

7:76

# قَالَ الَّذِيْنَ اسْتَكْبَرُوْٓا اِنَّا بِالَّذِيْٓ اٰمَنْتُمْ بِهٖ كٰفِرُوْنَ

q*aa*la **al**la*dz*iina istakbaruu inn*aa* bi**a**lla*dz*ii *aa*mantum bihi k*aa*firuun**a**

Orang-orang yang menyombongkan diri berkata, “Sesungguhnya kami mengingkari apa yang kamu percayai.”

7:77

# فَعَقَرُوا النَّاقَةَ وَعَتَوْا عَنْ اَمْرِ رَبِّهِمْ وَقَالُوْا يٰصٰلِحُ ائْتِنَا بِمَا تَعِدُنَآ اِنْ كُنْتَ مِنَ الْمُرْسَلِيْنَ

fa'aqaruu **al**nn*aa*qata wa'ataw 'an amri rabbihim waq*aa*luu y*aa* *shaa*li*h*u i/tin*aa* bim*aa* ta'idun*aa* in kunta mina **a**lmursaliin**a**

Kemudian mereka sembelih unta betina itu, dan berlaku angkuh terhadap perintah Tuhannya. Mereka berkata, “Wahai Saleh! Buktikanlah ancaman kamu kepada kami, jika benar engkau salah seorang rasul.”

7:78

# فَاَخَذَتْهُمُ الرَّجْفَةُ فَاَصْبَحُوْا فِيْ دَارِهِمْ جٰثِمِيْنَ

fa-akha*dz*at-humu **al**rrajfatu fa-a*sh*ba*h*uu fii d*aa*rihim j*aa*tsimiin**a**

Lalu datanglah gempa menimpa mereka, dan mereka pun mati bergelimpangan di dalam reruntuhan rumah mereka.

7:79

# فَتَوَلّٰى عَنْهُمْ وَقَالَ يٰقَوْمِ لَقَدْ اَبْلَغْتُكُمْ رِسَالَةَ رَبِّيْ وَنَصَحْتُ لَكُمْ وَلٰكِنْ لَّا تُحِبُّوْنَ النّٰصِحِيْنَ

fatawall*aa* 'anhum waq*aa*la y*aa* qawmi laqad ablaghtukum ris*aa*lata rabbii wana*sh*a*h*tu lakum wal*aa*kin l*aa* tu*h*ibbuuna **al**nn*aas*i*h*iin**a**

Kemudian dia (Saleh) pergi meninggalkan mereka sambil berkata, “Wahai kaumku! Sungguh, aku telah menyampaikan amanat Tuhanku kepadamu dan aku telah menasihati kamu. Tetapi kamu tidak menyukai orang yang memberi nasihat.”

7:80

# وَلُوْطًا اِذْ قَالَ لِقَوْمِهٖٓ اَتَأْتُوْنَ الْفَاحِشَةَ مَا سَبَقَكُمْ بِهَا مِنْ اَحَدٍ مِّنَ الْعٰلَمِيْنَ

waluu*th*an i*dz* q*aa*la liqawmihi ata/tuuna **a**lf*aah*isyata m*aa* sabaqakum bih*aa* min a*h*adin mina **a**l'*aa*lamiin**a**

Dan (Kami juga telah mengutus) Lut, ketika dia berkata kepada kaumnya, “Mengapa kamu melakukan perbuatan keji, yang belum pernah dilakukan oleh seorang pun sebelum kamu (di dunia ini).

7:81

# اِنَّكُمْ لَتَأْتُوْنَ الرِّجَالَ شَهْوَةً مِّنْ دُوْنِ النِّسَاۤءِۗ بَلْ اَنْتُمْ قَوْمٌ مُّسْرِفُوْنَ

innakum lata/tuuna **al**rrij*aa*la syahwatan min duuni **al**nnis*aa*-i bal antum qawmun musrifuun**a**

Sungguh, kamu telah melampiaskan syahwatmu kepada sesama lelaki bukan kepada perempuan. Kamu benar-benar kaum yang melampaui batas.”

7:82

# وَمَا كَانَ جَوَابَ قَوْمِهٖٓ اِلَّآ اَنْ قَالُوْٓا اَخْرِجُوْهُمْ مِّنْ قَرْيَتِكُمْۚ اِنَّهُمْ اُنَاسٌ يَّتَطَهَّرُوْنَ

wam*aa* k*aa*na jaw*aa*ba qawmihi ill*aa* an q*aa*luu akhrijuuhum min qaryatikum innahum un*aa*sun yata*th*ahharuun**a**

Dan jawaban kaumnya tidak lain hanya berkata, “Usirlah mereka (Lut dan pengikutnya) dari negerimu ini, mereka adalah orang yang menganggap dirinya suci.”

7:83

# فَاَنْجَيْنٰهُ وَاَهْلَهٗٓ اِلَّا امْرَاَتَهٗ كَانَتْ مِنَ الْغٰبِرِيْنَ

fa-anjayn*aa*hu wa-ahlahu ill*aa* imra-atahu k*aa*nat mina **a**lgh*aa*biriin**a**

Kemudian Kami selamatkan dia dan pengikutnya, kecuali istrinya. Dia (istrinya) termasuk orang-orang yang tertinggal.

7:84

# وَاَمْطَرْنَا عَلَيْهِمْ مَّطَرًاۗ فَانْظُرْ كَيْفَ كَانَ عَاقِبَةُ الْمُجْرِمِيْنَ ࣖ

wa-am*th*arn*aa* 'alayhim ma*th*aran fa**u**n*zh*ur kayfa k*aa*na '*aa*qibatu **a**lmujrimiin**a**

Dan Kami hujani mereka dengan hujan (batu). Maka perhatikanlah bagaimana kesudahan orang yang berbuat dosa itu.

7:85

# وَاِلٰى مَدْيَنَ اَخَاهُمْ شُعَيْبًاۗ قَالَ يٰقَوْمِ اعْبُدُوا اللّٰهَ مَا لَكُمْ مِّنْ اِلٰهٍ غَيْرُهٗۗ قَدْ جَاۤءَتْكُمْ بَيِّنَةٌ مِّنْ رَّبِّكُمْ فَاَوْفُوا الْكَيْلَ وَالْمِيْزَانَ وَلَا تَبْخَسُوا النَّاسَ اَشْيَاۤءَهُمْ وَلَا تُفْسِدُوْا فِى الْاَر

wa-il*aa* madyana akh*aa*hum syu'ayban q*aa*la y*aa* qawmi u'buduu **al**l*aa*ha m*aa* lakum min il*aa*hin ghayruhu qad j*aa*-atkum bayyinatun min rabbikum fa-awfuu **a**lkayla wa**a<**

Dan kepada penduduk Madyan, Kami (utus) Syuaib, saudara mereka sendiri. Dia berkata, “Wahai kaumku! Sembahlah Allah. Tidak ada tuhan (sembahan) bagimu selain Dia. Sesungguhnya telah datang kepadamu bukti yang nyata dari Tuhanmu. Sempurnakanlah takaran dan







7:86

# وَلَا تَقْعُدُوْا بِكُلِّ صِرَاطٍ تُوْعِدُوْنَ وَتَصُدُّوْنَ عَنْ سَبِيْلِ اللّٰهِ مَنْ اٰمَنَ بِهٖ وَتَبْغُوْنَهَا عِوَجًاۚ وَاذْكُرُوْٓا اِذْ كُنْتُمْ قَلِيْلًا فَكَثَّرَكُمْۖ وَانْظُرُوْا كَيْفَ كَانَ عَاقِبَةُ الْمُفْسِدِيْنَ

wal*aa* taq'uduu bikulli *sh*ir*aath*in tuu'iduuna wata*sh*udduuna 'an sabiili **al**l*aa*hi man *aa*mana bihi watabghuunah*aa* 'iwajan wa**u***dz*kuruu i*dz* kuntum qaliilan fakatstsa

Dan janganlah kamu duduk di setiap jalan dengan menakut-nakuti dan menghalang-halangi orang-orang yang beriman dari jalan Allah dan ingin membelokkannya. Ingatlah ketika kamu dahulunya sedikit, lalu Allah memperbanyak jumlah kamu. Dan perhatikanlah bagaim

7:87

# وَاِنْ كَانَ طَاۤىِٕفَةٌ مِّنْكُمْ اٰمَنُوْا بِالَّذِيْٓ اُرْسِلْتُ بِهٖ وَطَاۤىِٕفَةٌ لَّمْ يُؤْمِنُوْا فَاصْبِرُوْا حَتّٰى يَحْكُمَ اللّٰهُ بَيْنَنَاۚ وَهُوَ خَيْرُ الْحٰكِمِيْنَ ۔

wa-in k*aa*na *thaa*-ifatun minkum *aa*manuu bi**a**lla*dz*ii ursiltu bihi wa*thaa*-ifatun lam yu/minuu fa**i***sh*biruu *h*att*aa* ya*h*kuma **al**l*aa*

*Jika ada segolongan di antara kamu yang beriman kepada (ajaran) yang aku diutus menyampaikannya, dan ada (pula) segolongan yang tidak beriman, maka bersabarlah sampai Allah menetapkan keputusan di antara kita. Dialah hakim yang terbaik.*









7:88

# قَالَ الْمَلَاُ الَّذِيْنَ اسْتَكْبَرُوْا مِنْ قَوْمِهٖ لَنُخْرِجَنَّكَ يٰشُعَيْبُ وَالَّذِيْنَ اٰمَنُوْا مَعَكَ مِنْ قَرْيَتِنَآ اَوْ لَتَعُوْدُنَّ فِيْ مِلَّتِنَاۗ قَالَ اَوَلَوْ كُنَّا كَارِهِيْنَ

q*aa*la **a**lmalau **al**la*dz*iina istakbaruu min qawmihi lanukhrijannaka y*aa* syu'aybu wa**a**lla*dz*iina *aa*manuu ma'aka min qaryatin*aa* aw lata'uudunna fii millatin*aa* q

Pemuka-pemuka yang menyombongkan diri dari kaum Syuaib berkata, “Wahai Syuaib! Pasti kami usir engkau bersama orang-orang yang beriman dari negeri kami, kecuali engkau kembali kepada agama kami.”Syuaib berkata, “Apakah (kamu akan mengusir kami), kendatipu







7:89

# قَدِ افْتَرَيْنَا عَلَى اللّٰهِ كَذِبًا اِنْ عُدْنَا فِيْ مِلَّتِكُمْ بَعْدَ اِذْ نَجّٰىنَا اللّٰهُ مِنْهَاۗ وَمَا يَكُوْنُ لَنَآ اَنْ نَّعُوْدَ فِيْهَآ اِلَّآ اَنْ يَّشَاۤءَ اللّٰهُ رَبُّنَاۗ وَسِعَ رَبُّنَا كُلَّ شَيْءٍ عِلْمًاۗ عَلَى اللّٰهِ تَوَكَّ

qadi iftarayn*aa* 'al*aa* **al**l*aa*hi ka*dz*iban in 'udn*aa* fii millatikum ba'da i*dz* najj*aa*n*aa* **al**l*aa*hu minh*aa* wam*aa* yakuunu lan*aa* an na'uuda fiih

Sungguh, kami telah mengada-adakan kebohongan yang besar terhadap Allah, jika kami kembali kepada agamamu, setelah Allah melepaskan kami darinya. Dan tidaklah pantas kami kembali kepadanya, kecuali jika Allah, Tuhan kami menghendaki. Pengetahuan Tuhan kam







7:90

# وَقَالَ الْمَلَاُ الَّذِيْنَ كَفَرُوْا مِنْ قَوْمِهٖ لَىِٕنِ اتَّبَعْتُمْ شُعَيْبًا ِانَّكُمْ اِذًا لَّخٰسِرُوْنَ

waq*aa*la **a**lmalau **al**la*dz*iina kafaruu min qawmihi la-ini ittaba'tum syu'ayban innakum i*dz*an lakh*aa*siruun**a**

Dan pemuka-pemuka dari kaumnya (Syuaib) yang kafir berkata (kepada sesamanya), “Sesungguhnya jika kamu mengikuti Syuaib, tentu kamu menjadi orang-orang yang rugi.”

7:91

# فَاَخَذَتْهُمُ الرَّجْفَةُ فَاَصْبَحُوْا فِيْ دَارِهِمْ جٰثِمِيْنَۙ

fa-akha*dz*at-humu **al**rrajfatu fa-a*sh*ba*h*uu fii d*aa*rihim j*aa*tsimiin**a**

Lalu datanglah gempa menimpa mereka, dan mereka pun mati bergelimpangan di dalam reruntuhan rumah mereka.

7:92

# الَّذِيْنَ كَذَّبُوْا شُعَيْبًا كَاَنْ لَّمْ يَغْنَوْا فِيْهَاۚ اَلَّذِيْنَ كَذَّبُوْا شُعَيْبًا كَانُوْا هُمُ الْخٰسِرِيْنَ

**al**la*dz*iina ka*dzdz*abuu syu'ayban ka-an lam yaghnaw fiih*aa* **al**la*dz*iina ka*dzdz*abuu syu'ayban k*aa*nuu humu **a**lkh*aa*siriin**a**

Orang-orang yang mendustakan Syuaib seakan-akan mereka belum pernah tinggal di (negeri) itu. Mereka yang mendustakan Syuaib, itulah orang-orang yang rugi.

7:93

# فَتَوَلّٰى عَنْهُمْ وَقَالَ يٰقَوْمِ لَقَدْ اَبْلَغْتُكُمْ رِسٰلٰتِ رَبِّيْ وَنَصَحْتُ لَكُمْۚ فَكَيْفَ اٰسٰى عَلٰى قَوْمٍ كٰفِرِيْنَ ࣖ

fatawall*aa* 'anhum waq*aa*la y*aa* qawmi laqad ablaghtukum ris*aa*l*aa*ti rabbii wana*sh*a*h*tu lakum fakayfa *aa*s*aa* 'al*aa* qawmin k*aa*firiin**a**

Maka Syuaib meninggalkan mereka seraya berkata, “Wahai kaumku! Sungguh, aku telah menyampaikan amanat Tuhanku kepadamu dan aku telah menasihati kamu. Maka bagaimana aku akan bersedih hati terhadap orang-orang kafir?”

7:94

# وَمَآ اَرْسَلْنَا فِيْ قَرْيَةٍ مِّنْ نَّبِيٍّ اِلَّآ اَخَذْنَآ اَهْلَهَا بِالْبَأْسَاۤءِ وَالضَّرَّاۤءِ لَعَلَّهُمْ يَضَّرَّعُوْنَ

wam*aa* arsaln*aa* fii qaryatin min nabiyyin ill*aa* akha*dz*n*aa* ahlah*aa* bi**a**lba/s*aa*-i wa**al***dhdh*arr*aa*-i la'allahum ya*dhdh*arra'uun**a**

Dan Kami tidak mengutus seorang nabi pun kepada sesuatu negeri, (lalu penduduknya mendustakan nabi itu), melainkan Kami timpakan kepada penduduknya kesempitan dan penderitaan agar mereka (tunduk dengan) merendahkan diri.

7:95

# ثُمَّ بَدَّلْنَا مَكَانَ السَّيِّئَةِ الْحَسَنَةَ حَتّٰى عَفَوْا وَّقَالُوْا قَدْ مَسَّ اٰبَاۤءَنَا الضَّرَّاۤءُ وَالسَّرَّاۤءُ فَاَخَذْنٰهُمْ بَغْتَةً وَّهُمْ لَا يَشْعُرُوْنَ

tsumma baddaln*aa* mak*aa*na **al**ssayyi-ati **a**l*h*asanata *h*att*aa* 'afaw waq*aa*luu qad massa *aa*b*aa*-an*aa* **al***dhdh*arr*aa*u wa**al**s

Kemudian Kami ganti penderitaan itu dengan kesenangan (sehingga keturunan dan harta mereka) bertambah banyak, lalu mereka berkata, “Sungguh, nenek moyang kami telah merasakan penderitaan dan kesenangan,” maka Kami timpakan siksaan atas mereka dengan tiba-

7:96

# وَلَوْ اَنَّ اَهْلَ الْقُرٰٓى اٰمَنُوْا وَاتَّقَوْا لَفَتَحْنَا عَلَيْهِمْ بَرَكٰتٍ مِّنَ السَّمَاۤءِ وَالْاَرْضِ وَلٰكِنْ كَذَّبُوْا فَاَخَذْنٰهُمْ بِمَا كَانُوْا يَكْسِبُوْنَ

walaw anna ahla **a**lqur*aa* *aa*manuu wa**i**ttaqaw lafata*h*n*aa* 'alayhim barak*aa*tin mina **al**ssam*aa*-i wa**a**l-ar*dh*i wal*aa*kin ka*dzdz*abuu fa-a

Dan sekiranya penduduk negeri beriman dan bertakwa, pasti Kami akan melimpahkan kepada mereka berkah dari langit dan bumi, tetapi ternyata mereka mendustakan (ayat-ayat Kami), maka Kami siksa mereka sesuai dengan apa yang telah mereka kerjakan.

7:97

# اَفَاَمِنَ اَهْلُ الْقُرٰٓى اَنْ يَّأْتِيَهُمْ بَأْسُنَا بَيَاتًا وَّهُمْ نَاۤىِٕمُوْنَۗ

afa-amina ahlu **a**lqur*aa* an ya/tiyahum ba/sun*aa* bay*aa*tan wahum n*aa*-imuun**a**

Maka apakah penduduk negeri itu merasa aman dari siksaan Kami yang datang malam hari ketika mereka sedang tidur?

7:98

# اَوَاَمِنَ اَهْلُ الْقُرٰٓى اَنْ يَّأْتِيَهُمْ بَأْسُنَا ضُحًى وَّهُمْ يَلْعَبُوْنَ

awa amina ahlu **a**lqur*aa* an ya/tiyahum ba/sun*aa* *dh*u*h*an wahum yal'abuun**a**

Atau apakah penduduk negeri itu merasa aman dari siksaan Kami yang datang pada pagi hari ketika mereka sedang bermain?

7:99

# اَفَاَمِنُوْا مَكْرَ اللّٰهِۚ فَلَا يَأْمَنُ مَكْرَ اللّٰهِ اِلَّا الْقَوْمُ الْخٰسِرُوْنَ ࣖ

afa-aminuu makra **al**l*aa*hi fal*aa* ya/manu makra **al**l*aa*hi ill*aa* **a**lqawmu **a**lkh*aa*siruun**a**

Atau apakah mereka merasa aman dari siksaan Allah (yang tidak terduga-duga)? Tidak ada yang merasa aman dari siksaan Allah selain orang-orang yang rugi.

7:100

# اَوَلَمْ يَهْدِ لِلَّذِيْنَ يَرِثُوْنَ الْاَرْضَ مِنْۢ بَعْدِ اَهْلِهَآ اَنْ لَّوْ نَشَاۤءُ اَصَبْنٰهُمْ بِذُنُوْبِهِمْۚ وَنَطْبَعُ عَلٰى قُلُوْبِهِمْ فَهُمْ لَا يَسْمَعُوْنَ

awa lam yahdi lilla*dz*iina yaritsuuna **a**l-ar*dh*a min ba'di ahlih*aa* an law nasy*aa*u a*sh*abn*aa*hum bi*dz*unuubihim wana*th*ba'u 'al*aa* quluubihim fahum l*aa* yasma'uun**a**

**Atau apakah belum jelas bagi orang-orang yang mewarisi suatu negeri setelah (lenyap) penduduknya? Bahwa kalau Kami menghendaki pasti Kami siksa mereka karena dosa-dosanya; dan Kami mengunci hati mereka sehingga mereka tidak dapat mendengar (pelajaran).**









7:101

# تِلْكَ الْقُرٰى نَقُصُّ عَلَيْكَ مِنْ اَنْۢبَاۤىِٕهَاۚ وَلَقَدْ جَاۤءَتْهُمْ رُسُلُهُمْ بِالْبَيِّنٰتِۚ فَمَا كَانُوْا لِيُؤْمِنُوْا بِمَا كَذَّبُوْا مِنْ قَبْلُۗ كَذٰلِكَ يَطْبَعُ اللّٰهُ عَلٰى قُلُوْبِ الْكٰفِرِيْنَ

tilka **a**lqur*aa* naqu*shsh*u 'alayka min anb*aa*-ih*aa* walaqad j*aa*-at-hum rusuluhum bi**a**lbayyin*aa*ti fam*aa* k*aa*nuu liyu/minuu bim*aa* ka*dzdz*abuu min qablu ka*dzaa*

Itulah negeri-negeri (yang telah Kami binasakan) itu, Kami ceritakan sebagian kisahnya kepadamu. Rasul-rasul mereka benar-benar telah datang kepada mereka dengan membawa bukti-bukti yang nyata. Tetapi mereka tidak beriman (juga) kepada apa yang telah mere







7:102

# وَمَا وَجَدْنَا لِاَكْثَرِهِمْ مِّنْ عَهْدٍۚ وَاِنْ وَّجَدْنَآ اَكْثَرَهُمْ لَفٰسِقِيْنَ

wam*aa* wajadn*aa* li-aktsarihim min 'ahdin wa-in wajadn*aa* aktsarahum laf*aa*siqiin**a**

Dan Kami tidak mendapati kebanyakan mereka memenuhi janji. Sebaliknya yang Kami dapati kebanyakan mereka adalah orang-orang yang benar-benar fasik.

7:103

# ثُمَّ بَعَثْنَا مِنْۢ بَعْدِهِمْ مُّوْسٰى بِاٰيٰتِنَآ اِلٰى فِرْعَوْنَ وَمَلَا۟ىِٕهٖ فَظَلَمُوْا بِهَاۚ فَانْظُرْ كَيْفَ كَانَ عَاقِبَةُ الْمُفْسِدِيْنَ

tsumma ba'atsn*aa* min ba'dihim muus*aa* bi-*aa*y*aa*tin*aa* il*aa* fir'awna wamala-ihi fa*zh*alamuu bih*aa* fa**u**nu*zh*ur kayfa k*aa*na '*aa*qibatu **a**lmufsidiin

Setelah mereka, kemudian Kami utus Musa dengan membawa bukti-bukti Kami kepada Fir‘aun dan pemuka-pemuka kaumnya, lalu mereka mengingkari bukti-bukti itu. Maka perhatikanlah bagaimana kesudahan orang-orang yang berbuat kerusakan.







7:104

# وَقَالَ مُوْسٰى يٰفِرْعَوْنُ اِنِّيْ رَسُوْلٌ مِّنْ رَّبِّ الْعٰلَمِيْنَۙ

waq*aa*la muus*aa* y*aa* fir'awnu innii rasuulun min rabbi **a**l'*aa*lamiin**a**

Dan Musa berkata, “Wahai Fir‘aun! Sungguh, aku adalah seorang utusan dari Tuhan seluruh alam,

7:105

# حَقِيْقٌ عَلٰٓى اَنْ لَّآ اَقُوْلَ عَلَى اللّٰهِ اِلَّا الْحَقَّۗ قَدْ جِئْتُكُمْ بِبَيِّنَةٍ مِّنْ رَّبِّكُمْ فَاَرْسِلْ مَعِيَ بَنِيْٓ اِسْرَاۤءِيْلَ ۗ

*h*aqiiqun 'al*aa* an l*aa* aquula 'al*aa* **al**l*aa*hi ill*aa* **a**l*h*aqqa qad ji/tukum bibayyinatin min rabbikum fa-arsil ma'iya banii isr*aa*-iil**a**

aku wajib mengatakan yang sebenarnya tentang Allah. Sungguh, aku datang kepadamu dengan membawa bukti yang nyata dari Tuhanmu, maka lepaskanlah Bani Israil (pergi) bersamaku.”

7:106

# قَالَ اِنْ كُنْتَ جِئْتَ بِاٰيَةٍ فَأْتِ بِهَآ اِنْ كُنْتَ مِنَ الصّٰدِقِيْنَ

q*aa*la in kunta ji/ta bi-*aa*yatin fa/ti bih*aa* in kunta mina **al***shshaa*diqiin**a**

Dia (Fir‘aun) menjawab, “Jika benar engkau membawa sesuatu bukti, maka tunjukkanlah, kalau kamu termasuk orang-orang yang benar.”

7:107

# فَاَلْقٰى عَصَاهُ فَاِذَا هِيَ ثُعْبَانٌ مُّبِيْنٌ ۖ

fa-alq*aa* 'a*shaa*hu fa-i*dzaa* hiya tsu'b*aa*nun mubiin**un**

Lalu (Musa) melemparkan tongkatnya, tiba-tiba tongkat itu menjadi ular besar yang sebenarnya.

7:108

# وَّنَزَعَ يَدَهٗ فَاِذَا هِيَ بَيْضَاۤءُ لِلنّٰظِرِيْنَ ࣖ

wanaza'a yadahu fa-i*dzaa* hiya bay*daa*u li**l**nn*aats*iriin**a**

Dan dia mengeluarkan tangannya, tiba-tiba tangan itu menjadi putih (bercahaya) bagi orang-orang yang melihatnya.

7:109

# قَالَ الْمَلَاُ مِنْ قَوْمِ فِرْعَوْنَ اِنَّ هٰذَا لَسٰحِرٌ عَلِيْمٌۙ

q*aa*la **a**lmalau min qawmi fir'awna inna h*aadzaa* las*aah*irun 'aliim**un**

Pemuka-pemuka kaum Fir‘aun berkata, “Orang ini benar-benar pesihir yang pandai,

7:110

# يُّرِيْدُ اَنْ يُّخْرِجَكُمْ مِّنْ اَرْضِكُمْ ۚ فَمَاذَا تَأْمُرُوْنَ

yuriidu an yukhrijakum min ar*dh*ikum fam*aatsaa* ta/muruun**a**

yang hendak mengusir kamu dari negerimu.” (Fir‘aun berkata), “Maka apa saran kamu?”

7:111

# قَالُوْآ اَرْجِهْ وَاَخَاهُ وَاَرْسِلْ فِى الْمَدَاۤىِٕنِ حٰشِرِيْنَۙ

q*aa*luu arjih wa-akh*aa*hu wa-arsil fii **a**lmad*aa*-ini *haa*syiriin**a**

(Pemuka-pemuka) itu menjawab, “Tahanlah (untuk sementara) dia dan saudaranya dan utuslah ke kota-kota beberapa orang untuk mengumpulkan (para pesihir),

7:112

# يَأْتُوْكَ بِكُلِّ سٰحِرٍ عَلِيْمٍ

ya/tuuka bikulli s*aah*irin 'aliim**in**

agar mereka membawa semua pesihir yang pandai kepadamu.”

7:113

# وَجَاۤءَ السَّحَرَةُ فِرْعَوْنَ قَالُوْٓا اِنَّ لَنَا لَاَجْرًا اِنْ كُنَّا نَحْنُ الْغٰلِبِيْنَ

waj*aa*-a **al**ssa*h*aratu fir'awna q*aa*luu inna lan*aa* la-ajran in kunn*aa* na*h*nu **a**lgh*aa*libiin**a**

Dan para pesihir datang kepada Fir‘aun. Mereka berkata, “(Apakah) kami akan mendapat imbalan, jika kami menang?”

7:114

# قَالَ نَعَمْ وَاِنَّكُمْ لَمِنَ الْمُقَرَّبِيْنَ

q*aa*la na'am wa-innakum lamina **a**lmuqarrabiin**a**

Dia (Fir‘aun) menjawab, “Ya, bahkan kamu pasti termasuk orang-orang yang dekat (kepadaku).”

7:115

# قَالُوْا يٰمُوْسٰٓى اِمَّآ اَنْ تُلْقِيَ وَاِمَّآ اَنْ نَّكُوْنَ نَحْنُ الْمُلْقِيْنَ

q*aa*luu y*aa* muus*aa* imm*aa* an tulqiya wa-imm*aa* an nakuuna na*h*nu **a**lmulqiin**a**

Mereka (para pesihir) berkata, “Wahai Musa! Engkaukah yang akan melemparkan lebih dahulu, atau kami yang melemparkan?”

7:116

# قَالَ اَلْقُوْاۚ فَلَمَّآ اَلْقَوْا سَحَرُوْٓا اَعْيُنَ النَّاسِ وَاسْتَرْهَبُوْهُمْ وَجَاۤءُوْ بِسِحْرٍ عَظِيْمٍ

q*aa*la **a**lquu falamm*aa* **a**lqaw sa*h*aruu a'yuna **al**nn*aa*si wa**i**starhabuuhum waj*aa*uu bisi*h*rin 'a*zh*iim**in**

Dia (Musa) menjawab, “Lemparkanlah (lebih dahulu)!” Maka setelah mereka melemparkan, mereka menyihir mata orang banyak dan menjadikan orang banyak itu takut, karena mereka memperlihatkan sihir yang hebat (menakjubkan).

7:117

# ۞ وَاَوْحَيْنَآ اِلٰى مُوْسٰٓى اَنْ اَلْقِ عَصَاكَۚ فَاِذَا هِيَ تَلْقَفُ مَا يَأْفِكُوْنَۚ

wa-aw*h*ayn*aa* il*aa* muus*aa* an **a**lqi 'a*shaa*ka fa-i*dzaa* hiya talqafu m*aa* ya/fikuun**a**

Dan Kami wahyukan kepada Musa, “Lemparkanlah tongkatmu!” Maka tiba-tiba ia menelan (habis) segala kepalsuan mereka.

7:118

# فَوَقَعَ الْحَقُّ وَبَطَلَ مَا كَانُوْا يَعْمَلُوْنَۚ

fawaqa'a **a**l*h*aqqu waba*th*ala m*aa* k*aa*nuu ya'maluun**a**

Maka terbuktilah kebenaran, dan segala yang mereka kerjakan jadi sia-sia.

7:119

# فَغُلِبُوْا هُنَالِكَ وَانْقَلَبُوْا صٰغِرِيْنَۚ

faghulibuu hun*aa*lika wa**i**nqalabuu *shaa*ghiriin**a**

Maka mereka dikalahkan di tempat itu dan jadilah mereka orang-orang yang hina.

7:120

# وَاُلْقِيَ السَّحَرَةُ سٰجِدِيْنَۙ

waulqiya **al**ssa*h*aratu s*aa*jidiin**a**

Dan para pesihir itu serta merta menjatuhkan diri dengan bersujud.

7:121

# قَالُوْٓا اٰمَنَّا بِرَبِّ الْعٰلَمِيْنَۙ

q*aa*luu *aa*mann*aa* birabbi **a**l'*aa*lamiin**a**

Mereka berkata, “Kami beriman kepada Tuhan seluruh alam,

7:122

# رَبِّ مُوْسٰى وَهٰرُوْنَ

rabbi muus*aa* wah*aa*ruun**a**

(yaitu) Tuhannya Musa dan Harun.”

7:123

# قَالَ فِرْعَوْنُ اٰمَنْتُمْ بِهٖ قَبْلَ اَنْ اٰذَنَ لَكُمْۚ اِنَّ هٰذَا لَمَكْرٌ مَّكَرْتُمُوْهُ فِى الْمَدِيْنَةِ لِتُخْرِجُوْا مِنْهَآ اَهْلَهَاۚ فَسَوْفَ تَعْلَمُوْنَ

q*aa*la fir'awnu *aa*mantum bihi qabla an *aadz*ana lakum inna h*aadzaa* lamakrun makartumuuhu fii **a**lmadiinati litukhrijuu minh*aa* ahlah*aa* fasawfa ta'lamuun**a**

Fir‘aun berkata, “Mengapa kamu beriman kepadanya sebelum aku memberi izin kepadamu? Sesungguhnya ini benar-benar tipu muslihat yang telah kamu rencanakan di kota ini, untuk mengusir penduduknya. Kelak kamu akan mengetahui (akibat perbuatanmu ini).

7:124

# لَاُقَطِّعَنَّ اَيْدِيَكُمْ وَاَرْجُلَكُمْ مِّنْ خِلَافٍ ثُمَّ لَاُصَلِّبَنَّكُمْ اَجْمَعِيْنَ

lauqa*th*i'anna aydiyakum wa-arjulakum min khil*aa*fin tsumma lau*sh*allibannakum ajma'iin**a**

Pasti akan aku potong tangan dan kakimu dengan bersilang (tangan kanan dan kaki kiri atau sebaliknya), kemudian aku akan menyalib kamu semua.”

7:125

# قَالُوْٓا اِنَّآ اِلٰى رَبِّنَا مُنْقَلِبُوْنَۙ

q*aa*luu inn*aa* il*aa* rabbin*aa* munqalibuun**a**

Mereka (para pesihir) menjawab, “Sesungguhnya kami akan kembali kepada Tuhan kami.

7:126

# وَمَا تَنْقِمُ مِنَّآ اِلَّآ اَنْ اٰمَنَّا بِاٰيٰتِ رَبِّنَا لَمَّا جَاۤءَتْنَا ۗرَبَّنَآ اَفْرِغْ عَلَيْنَا صَبْرًا وَّتَوَفَّنَا مُسْلِمِيْنَ ࣖ

wam*aa* tanqimu minn*aa* ill*aa* an *aa*mann*aa* bi-*aa*y*aa*ti rabbin*aa* lamm*aa* j*aa*-atn*aa* rabban*aa* afrigh 'alayn*aa* *sh*abran watawaffan*aa* muslimiin**a**

Dan engkau tidak melakukan balas dendam kepada kami, melainkan karena kami beriman kepada ayat-ayat Tuhan kami ketika ayat-ayat itu datang kepada kami.” (Mereka berdoa), “Ya Tuhan kami, limpahkanlah kesabaran kepada kami dan matikanlah kami dalam keadaan

7:127

# وَقَالَ الْمَلَاُ مِنْ قَوْمِ فِرْعَوْنَ اَتَذَرُ مُوْسٰى وَقَوْمَهٗ لِيُفْسِدُوْا فِى الْاَرْضِ وَيَذَرَكَ وَاٰلِهَتَكَۗ قَالَ سَنُقَتِّلُ اَبْنَاۤءَهُمْ وَنَسْتَحْيٖ نِسَاۤءَهُمْۚ وَاِنَّا فَوْقَهُمْ قَاهِرُوْنَ

waq*aa*la **a**lmalau min qawmi fir'awna ata*dz*aru muus*aa* waqawmahu liyufsiduu fii **a**l-ar*dh*i waya*dz*araka wa*aa*lihataka q*aa*la sanuqattilu abn*aa*-ahum wanasta*h*yii nis*a*

Dan para pemuka dari kaum Fir‘aun berkata, “Apakah engkau akan membiarkan Musa dan kaumnya untuk berbuat kerusakan di negeri ini (Mesir) dan meninggalkanmu dan tuhan-tuhanmu?” (Fir‘aun) menjawab, “Akan kita bunuh anak-anak laki-laki mereka dan kita biarka







7:128

# قَالَ مُوْسٰى لِقَوْمِهِ اسْتَعِيْنُوْا بِاللّٰهِ وَاصْبِرُوْاۚ اِنَّ الْاَرْضَ لِلّٰهِ ۗيُوْرِثُهَا مَنْ يَّشَاۤءُ مِنْ عِبَادِهٖۗ وَالْعَاقِبَةُ لِلْمُتَّقِيْنَ

q*aa*la muus*aa* liqawmihi ista'iinuu bi**al**l*aa*hi wa**i***sh*biruu inna **a**l-ar*dh*a lill*aa*hi yuuritsuh*aa* man yasy*aa*u min 'ib*aa*dihi wa**a**l'

Musa berkata kepada kaumnya, “Mohonlah pertolongan kepada Allah dan bersabarlah. Sesungguhnya bumi (ini) milik Allah; diwariskan-Nya kepada siapa saja yang Dia kehendaki di antara hamba-hamba-Nya. Dan kesudahan (yang baik) adalah bagi orang-orang yang ber







7:129

# قَالُوْٓا اُوْذِيْنَا مِنْ قَبْلِ اَنْ تَأْتِيَنَا وَمِنْۢ بَعْدِ مَا جِئْتَنَا ۗقَالَ عَسٰى رَبُّكُمْ اَنْ يُّهْلِكَ عَدُوَّكُمْ وَيَسْتَخْلِفَكُمْ فِى الْاَرْضِ فَيَنْظُرَ كَيْفَ تَعْمَلُوْنَ ࣖ

q*aa*luu uu*dz*iin*aa* min qabli an ta/tiyan*aa* wamin ba'di m*aa* ji/tan*aa* q*aa*la 'as*aa* rabbukum an yuhlika 'aduwwakum wayastakhlifakum fii **a**l-ar*dh*i fayan*zh*ura kayfa ta'maluun

Mereka (kaum Musa) berkata, ”Kami telah ditindas (oleh Fir‘aun) sebelum engkau datang kepada kami dan setelah engkau datang.” (Musa) menjawab, “Mudah-mudahan Tuhanmu membinasakan musuhmu dan menjadikan kamu khalifah di bumi; maka Dia akan melihat bagaima

7:130

# وَلَقَدْ اَخَذْنَآ اٰلَ فِرْعَوْنَ بِالسِّنِيْنَ وَنَقْصٍ مِّنَ الثَّمَرٰتِ لَعَلَّهُمْ يَذَّكَّرُوْنَ

walaqad akha*dz*n*aa* *aa*la fir'awna bi**al**ssiniina wanaq*sh*in mina **al**tstsamar*aa*ti la'allahum ya*dzdz*akkaruun**a**

Dan sungguh, Kami telah menghukum Fir‘aun dan kaumnya dengan (mendatangkan musim kemarau) bertahun-tahun dan kekurangan buah-buahan, agar mereka mengambil pelajaran.

7:131

# فَاِذَا جَاۤءَتْهُمُ الْحَسَنَةُ قَالُوْا لَنَا هٰذِهٖ ۚوَاِنْ تُصِبْهُمْ سَيِّئَةٌ يَّطَّيَّرُوْا بِمُوْسٰى وَمَنْ مَّعَهٗۗ اَلَآ اِنَّمَا طٰۤىِٕرُهُمْ عِنْدَ اللّٰهِ وَلٰكِنَّ اَكْثَرَهُمْ لَا يَعْلَمُوْنَ

fa-i*dzaa* j*aa*-at-humu **a**l*h*asanatu q*aa*luu lan*aa* h*aadz*ihi wa-in tu*sh*ibhum sayyi-atun ya*ththh*ayyaruu bimuus*aa* waman ma'ahu **a**l*aa* innam*aa* *tha*

Kemudian apabila kebaikan (kemakmuran) datang kepada mereka, mereka berkata, “Ini adalah karena (usaha) kami.” Dan jika mereka ditimpa kesusahan, mereka lemparkan sebab kesialan itu kepada Musa dan pengikutnya. Ketahuilah, sesungguhnya nasib mereka di tan







7:132

# وَقَالُوْا مَهْمَا تَأْتِنَا بِهٖ مِنْ اٰيَةٍ لِّتَسْحَرَنَا بِهَاۙ فَمَا نَحْنُ لَكَ بِمُؤْمِنِيْنَ

waq*aa*luu mahm*aa* ta/tin*aa* bihi min *aa*yatin litas*h*aran*aa* bih*aa* fam*aa* na*h*nu laka bimu/miniin**a**

Dan mereka berkata (kepada Musa), “Bukti apa pun yang engkau bawa kepada kami untuk menyihir kami, kami tidak akan beriman kepadamu.”

7:133

# فَاَرْسَلْنَا عَلَيْهِمُ الطُّوْفَانَ وَالْجَرَادَ وَالْقُمَّلَ وَالضَّفَادِعَ وَالدَّمَ اٰيٰتٍ مُّفَصَّلٰتٍۗ فَاسْتَكْبَرُوْا وَكَانُوْا قَوْمًا مُّجْرِمِيْنَ

fa-arsaln*aa* 'alayhimu **al***ththh*uuf*aa*na wa**a**ljar*aa*da wa**a**lqummala wa**al***dhdh*af*aa*di'a wa**al**ddama *aa*y*aa*tin mufa*shsh*al

Maka Kami kirimkan kepada mereka topan, belalang, kutu, katak dan darah (air minum berubah menjadi darah) sebagai bukti-bukti yang jelas, tetapi mereka tetap menyombongkan diri dan mereka adalah kaum yang berdosa.

7:134

# وَلَمَّا وَقَعَ عَلَيْهِمُ الرِّجْزُ قَالُوْا يٰمُوْسَى ادْعُ لَنَا رَبَّكَ بِمَا عَهِدَ عِنْدَكَۚ لَىِٕنْ كَشَفْتَ عَنَّا الرِّجْزَ لَنُؤْمِنَنَّ لَكَ وَلَنُرْسِلَنَّ مَعَكَ بَنِيْٓ اِسْرَاۤءِيْلَ ۚ

walamm*aa* waqa'a 'alayhimu **al**rrijzu q*aa*luu y*aa* muus*aa* ud'u lan*aa* rabbaka bim*aa* 'ahida 'indaka la-in kasyafta 'ann*aa* **al**rrijza lanu/minanna laka walanursilanna ma'aka banii is

Dan ketika mereka ditimpa azab (yang telah diterangkan itu) mereka pun berkata, “Wahai Musa! Mohonkanlah untuk kami kepada Tuhanmu sesuai dengan janji-Nya kepadamu. Jika engkau dapat menghilangkan azab itu dari kami, niscaya kami akan beriman kepadamu dan

7:135

# فَلَمَّا كَشَفْنَا عَنْهُمُ الرِّجْزَ اِلٰٓى اَجَلٍ هُمْ بَالِغُوْهُ اِذَا هُمْ يَنْكُثُوْنَ

falamm*aa* kasyafn*aa* 'anhumu **al**rrijza il*aa* ajalin hum b*aa*lighuuhu i*dzaa* hum yankutsuun**a**

Tetapi setelah Kami hilangkan azab itu dari mereka hingga batas waktu yang harus mereka penuhi ternyata mereka ingkar janji.

7:136

# فَانْتَقَمْنَا مِنْهُمْ فَاَغْرَقْنٰهُمْ فِى الْيَمِّ بِاَنَّهُمْ كَذَّبُوْا بِاٰيٰتِنَا وَكَانُوْا عَنْهَا غٰفِلِيْنَ

fa**i**ntaqamn*aa* minhum fa-aghraqn*aa*hum fii **a**lyammi bi-annahum ka*dzdz*abuu bi-*aa*y*aa*tin*aa* wak*aa*nuu 'anh*aa* gh*aa*filiin**a**

Maka Kami hukum sebagian di antara mereka, lalu Kami tenggelamkan mereka di laut karena mereka telah mendustakan ayat-ayat Kami dan melalaikan ayat-ayat Kami.

7:137

# وَاَوْرَثْنَا الْقَوْمَ الَّذِيْنَ كَانُوْا يُسْتَضْعَفُوْنَ مَشَارِقَ الْاَرْضِ وَمَغَارِبَهَا الَّتِيْ بٰرَكْنَا فِيْهَاۗ وَتَمَّتْ كَلِمَتُ رَبِّكَ الْحُسْنٰى عَلٰى بَنِيْٓ اِسْرَاۤءِيْلَۙ بِمَا صَبَرُوْاۗ وَدَمَّرْنَا مَا كَانَ يَصْنَعُ فِرْعَوْنُ وَق

wa-awratsn*aa* **a**lqawma **al**la*dz*iina k*aa*nuu yusta*dh*'afuuna masy*aa*riqa **a**l-ar*dh*i wamagh*aa*ribah*aa* **al**latii b*aa*rakn*aa* fiih*aa*

Dan Kami wariskan kepada kaum yang tertindas itu, bumi bagian timur dan bagian baratnya yang telah Kami berkahi. Dan telah sempurnalah firman Tuhanmu yang baik itu (sebagai janji) untuk Bani Israil disebabkan kesabaran mereka. Dan Kami hancurkan apa yang







7:138

# وَجَاوَزْنَا بِبَنِيْٓ اِسْرَاۤءِيْلَ الْبَحْرَ فَاَتَوْا عَلٰى قَوْمٍ يَّعْكُفُوْنَ عَلٰٓى اَصْنَامٍ لَّهُمْ ۚقَالُوْا يٰمُوْسَى اجْعَلْ لَّنَآ اِلٰهًا كَمَا لَهُمْ اٰلِهَةٌ ۗقَالَ اِنَّكُمْ قَوْمٌ تَجْهَلُوْنَ

waj*aa*wazn*aa* bibanii isr*aa*-iila **a**lba*h*ra fa-ataw 'al*aa* qawmin ya'kufuuna 'al*aa* a*sh*n*aa*min lahum q*aa*luu y*aa* muus*aa* ij'al lan*aa* il*aa*han kam*aa* lah

Dan Kami selamatkan Bani Israil menyeberangi laut itu (bagian utara dari Laut Merah). Ketika mereka sampai kepada suatu kaum yang tetap menyembah berhala, mereka (Bani Israil) berkata, “Wahai Musa! Buatlah untuk kami sebuah tuhan (berhala) sebagaimana mer

7:139

# اِنَّ هٰٓؤُلَاۤءِ مُتَبَّرٌ مَّا هُمْ فِيْهِ وَبٰطِلٌ مَّا كَانُوْا يَعْمَلُوْنَ

inna h*aa*ul*aa*-i mutabbarun m*aa* hum fiihi wab*aath*ilun m*aa* k*aa*nuu ya'maluun**a**

Sesungguhnya mereka akan dihancurkan (oleh kepercayaan) yang dianutnya dan akan sia-sia apa yang telah mereka kerjakan.

7:140

# قَالَ اَغَيْرَ اللّٰهِ اَبْغِيْكُمْ اِلٰهًا وَّهُوَ فَضَّلَكُمْ عَلَى الْعٰلَمِيْنَ

q*aa*la aghayra **al**l*aa*hi abghiikum il*aa*han wahuwa fa*dhdh*alakum 'al*aa* **a**l'*aa*lamiin**a**

Dia (Musa) berkata, “Pantaskah aku mencari tuhan untukmu selain Allah, padahal Dia yang telah melebihkan kamu atas segala umat (pada masa itu).”

7:141

# وَاِذْ اَنْجَيْنٰكُمْ مِّنْ اٰلِ فِرْعَوْنَ يَسُوْمُوْنَكُمْ سُوْۤءَ الْعَذَابِۚ يُقَتِّلُوْنَ اَبْنَاۤءَكُمْ وَيَسْتَحْيُوْنَ نِسَاۤءَكُمْۗ وَفِيْ ذٰلِكُمْ بَلَاۤءٌ مِّنْ رَّبِّكُمْ عَظِيْمٌ ࣖ

wa-i*dz* anjayn*aa*kum min *aa*li fir'awna yasuumuunakum suu-a **a**l'a*dzaa*bi yuqattiluuna abn*aa*-akum wayasta*h*yuuna nis*aa*-akum wafii *dzaa*likum bal*aa*un min rabbikum 'a*zh*iim

Dan (ingatlah wahai Bani Israil) ketika Kami menyelamatkan kamu dari (Fir‘aun) dan kaumnya, yang menyiksa kamu dengan siksaan yang sangat berat, mereka membunuh anak-anak laki-lakimu dan membiarkan hidup anak-anak perempuanmu. Dan pada yang demikian itu m

7:142

# ۞ وَوٰعَدْنَا مُوْسٰى ثَلٰثِيْنَ لَيْلَةً وَّاَتْمَمْنٰهَا بِعَشْرٍ فَتَمَّ مِيْقَاتُ رَبِّهٖٓ اَرْبَعِيْنَ لَيْلَةً ۚوَقَالَ مُوْسٰى لِاَخِيْهِ هٰرُوْنَ اخْلُفْنِيْ فِيْ قَوْمِيْ وَاَصْلِحْ وَلَا تَتَّبِعْ سَبِيْلَ الْمُفْسِدِيْنَ

waw*aa*'adn*aa* muus*aa* tsal*aa*tsiina laylatan wa-atmamn*aa*h*aa* bi'asyrin fatamma miiq*aa*tu rabbihi arba'iina laylatan waq*aa*la muus*aa* li-akhiihi h*aa*ruuna ukhlufnii fii qawmii wa-a*sh*li*h*

Dan Kami telah menjanjikan kepada Musa (memberikan Taurat) tiga puluh malam, dan Kami sempurnakan jumlah malam itu dengan sepuluh (malam lagi), maka sempurnalah waktu yang telah ditentukan Tuhannya empat puluh malam. Dan Musa berkata kepada saudaranya (ya







7:143

# وَلَمَّا جَاۤءَ مُوْسٰى لِمِيْقَاتِنَا وَكَلَّمَهٗ رَبُّهٗۙ قَالَ رَبِّ اَرِنِيْٓ اَنْظُرْ اِلَيْكَۗ قَالَ لَنْ تَرٰىنِيْ وَلٰكِنِ انْظُرْ اِلَى الْجَبَلِ فَاِنِ اسْتَقَرَّ مَكَانَهٗ فَسَوْفَ تَرٰىنِيْۚ فَلَمَّا تَجَلّٰى رَبُّهٗ لِلْجَبَلِ جَعَلَهٗ دَكًّا

walamm*aa* j*aa*-a muus*aa* limiiq*aa*tin*aa* wakallamahu rabbuhu q*aa*la rabbi arinii an*zh*ur ilayka q*aa*la lan tar*aa*nii wal*aa*kini un*zh*ur il*aa* **a**ljabali fa-ini istaqarr

Dan ketika Musa datang untuk (munajat) pada waktu yang telah Kami tentukan dan Tuhan telah berfirman (langsung) kepadanya, (Musa) berkata, “Ya Tuhanku, tampakkanlah (diri-Mu) kepadaku agar aku dapat melihat Engkau.” (Allah) berfirman, “Engkau tidak akan (

7:144

# قَالَ يٰمُوْسٰٓى اِنِّى اصْطَفَيْتُكَ عَلَى النَّاسِ بِرِسٰلٰتِيْ وَبِكَلَامِيْ ۖفَخُذْ مَآ اٰتَيْتُكَ وَكُنْ مِّنَ الشّٰكِرِيْنَ

q*aa*la y*aa* muus*aa* innii i*sth*afaytuka 'al*aa* **al**nn*aa*si biris*aa*l*aa*tii wabikal*aa*mii fakhu*dz* m*aa* *aa*taytuka wakun mina **al**sysy*aa*kiriin

(Allah) berfirman, “Wahai Musa! Sesungguhnya Aku memilih (melebihkan) engkau dari manusia yang lain (pada masamu) untuk membawa risalah-Ku dan firman-Ku, sebab itu berpegang-teguhlah kepada apa yang Aku berikan kepadamu dan hendaklah engkau termasuk orang

7:145

# وَكَتَبْنَا لَهٗ فِى الْاَلْوَاحِ مِنْ كُلِّ شَيْءٍ مَّوْعِظَةً وَّتَفْصِيْلًا لِّكُلِّ شَيْءٍۚ فَخُذْهَا بِقُوَّةٍ وَّأْمُرْ قَوْمَكَ يَأْخُذُوْا بِاَحْسَنِهَا ۗسَاُورِيْكُمْ دَارَ الْفٰسِقِيْنَ

wakatabn*aa* lahu fii **a**l-alw*aah*i min kulli syay-in maw'i*zh*atan wataf*sh*iilan likulli syay-in fakhu*dz*h*aa* biquwwatin wa/mur qawmaka ya/khu*dz*uu bi-a*h*sanih*aa* sauriikum d*aa*ra <

Dan telah Kami tuliskan untuk Musa pada lauh-lauh (Taurat) segala sesuatu sebagai pelajaran dan penjelasan untuk segala hal; maka (Kami berfirman), “Berpegangteguhlah kepadanya dan suruhlah kaummu berpegang kepadanya dengan sebaik-baiknya, Aku akan memper

7:146

# سَاَصْرِفُ عَنْ اٰيٰتِيَ الَّذِيْنَ يَتَكَبَّرُوْنَ فِى الْاَرْضِ بِغَيْرِ الْحَقِّۗ وَاِنْ يَّرَوْا كُلَّ اٰيَةٍ لَّا يُؤْمِنُوْا بِهَاۚ وَاِنْ يَّرَوْا سَبِيْلَ الرُّشْدِ لَا يَتَّخِذُوْهُ سَبِيْلًاۚ وَاِنْ يَّرَوْا سَبِيْلَ الْغَيِّ يَتَّخِذُوْهُ سَبِي

sa-a*sh*rifu 'an *aa*y*aa*tiya **al**la*dz*iina yatakabbaruuna fii **a**l-ar*dh*i bighayri **a**l*h*aqqi wa-in yaraw kulla *aa*yatin l*aa* yu/minuu bih*aa* wa-in yaraw sab

Akan Aku palingkan dari tanda-tanda (kekuasaan-Ku) orang-orang yang menyombongkan diri di bumi tanpa alasan yang benar. Kalaupun mereka melihat setiap tanda (kekuasaan-Ku) mereka tetap tidak akan beriman kepadanya. Dan jika mereka melihat jalan yang memba

7:147

# وَالَّذِيْنَ كَذَّبُوْا بِاٰيٰتِنَا وَلِقَاۤءِ الْاٰخِرَةِ حَبِطَتْ اَعْمَالُهُمْۗ هَلْ يُجْزَوْنَ اِلَّا مَا كَانُوْا يَعْمَلُوْنَ ࣖ

wa**a**lla*dz*iina ka*dzdz*abuu bi-*aa*y*aa*tin*aa* waliq*aa*-i **a**l-*aa*khirati *h*abi*th*at a'm*aa*luhum hal yujzawna ill*aa* m*aa* k*aa*nuu ya'maluun**a<**

Dan orang-orang yang mendustakan tanda-tanda (kekuasaan) Kami dan (mendustakan) adanya pertemuan akhirat, sia-sialah amal mereka. Mereka diberi balasan sesuai dengan apa yang telah mereka kerjakan.







7:148

# وَاتَّخَذَ قَوْمُ مُوْسٰى مِنْۢ بَعْدِهٖ مِنْ حُلِيِّهِمْ عِجْلًا جَسَدًا لَّهٗ خُوَارٌۗ اَلَمْ يَرَوْا اَنَّهٗ لَا يُكَلِّمُهُمْ وَلَا يَهْدِيْهِمْ سَبِيْلًاۘ اِتَّخَذُوْهُ وَكَانُوْا ظٰلِمِيْنَ

wa**i**ttakha*dz*a qawmu muus*aa* min ba'dihi min *h*uliyyihim 'ijlan jasadan lahu khuw*aa*run alam yaraw annahu l*aa* yukallimuhum wal*aa* yahdiihim sabiilan ittakha*dz*uuhu wak*aa*nuu *zhaa*limii

Dan kaum Musa, setelah kepergian (Musa ke Gunung Sinai) mereka membuat patung anak sapi yang bertubuh dan dapat melenguh (bersuara) dari perhiasan (emas). Apakah mereka tidak mengetahui bahwa (patung) anak sapi itu tidak dapat berbicara dengan mereka dan

7:149

# وَلَمَّا سُقِطَ فِيْٓ اَيْدِيْهِمْ وَرَاَوْا اَنَّهُمْ قَدْ ضَلُّوْاۙ قَالُوْا لَىِٕنْ لَّمْ يَرْحَمْنَا رَبُّنَا وَيَغْفِرْ لَنَا لَنَكُوْنَنَّ مِنَ الْخٰسِرِيْنَ

walamm*aa* suqi*th*a fii aydiihim wara-aw annahum qad *dh*alluu q*aa*luu la-in lam yar*h*amn*aa* rabbun*aa* wayaghfir lan*aa* lanakuunanna mina **a**lkh*aa*siriin**a**

Dan setelah mereka menyesali perbuatannya dan mengetahui bahwa telah sesat, mereka pun berkata, “Sungguh, jika Tuhan kami tidak memberi rahmat kepada kami dan tidak mengampuni kami, pastilah kami menjadi orang-orang yang rugi.”

7:150

# وَلَمَّا رَجَعَ مُوْسٰٓى اِلٰى قَوْمِهٖ غَضْبَانَ اَسِفًاۙ قَالَ بِئْسَمَا خَلَفْتُمُوْنِيْ مِنْۢ بَعْدِيْۚ اَعَجِلْتُمْ اَمْرَ رَبِّكُمْۚ وَاَلْقَى الْاَلْوَاحَ وَاَخَذَ بِرَأْسِ اَخِيْهِ يَجُرُّهٗٓ اِلَيْهِ ۗقَالَ ابْنَ اُمَّ اِنَّ الْقَوْمَ اسْتَضْعَفُ

walamm*aa* raja'a muus*aa* il*aa* qawmihi gha*dh*b*aa*na asifan q*aa*la bi/sam*aa* khalaftumuunii min ba'dii a'ajiltum amra rabbikum wa-alq*aa* al-alw*aah*a wa-akha*dz*a bira/si akhiihi yajurruhu ilayhi q<

Dan ketika Musa telah kembali kepada kaumnya, dengan marah dan sedih hati dia berkata, “Alangkah buruknya perbuatan yang kamu kerjakan selama kepergianku! Apakah kamu hendak mendahului janji Tuhanmu?” Musa pun melemparkan lauh-lauh (Taurat) itu dan memega

7:151

# قَالَ رَبِّ اغْفِرْ لِيْ وَلِاَخِيْ وَاَدْخِلْنَا فِيْ رَحْمَتِكَ ۖوَاَنْتَ اَرْحَمُ الرّٰحِمِيْنَ ࣖ

q*aa*la rabbi ighfir lii wali-akhii wa-adkhiln*aa* fii ra*h*matika wa-anta ar*h*amu **al**rr*aah*imiin**a**

Dia (Musa) berdoa, “Ya Tuhanku, ampunilah aku dan saudaraku dan masukkanlah kami ke dalam rahmat Engkau, dan Engkau adalah Maha Penyayang dari semua penyayang.”

7:152

# اِنَّ الَّذِيْنَ اتَّخَذُوا الْعِجْلَ سَيَنَالُهُمْ غَضَبٌ مِّنْ رَّبِّهِمْ وَذِلَّةٌ فِى الْحَيٰوةِ الدُّنْيَاۗ وَكَذٰلِكَ نَجْزِى الْمُفْتَرِيْنَ

inna **al**la*dz*iina ittakha*dz*uu **a**l'ijla sayan*aa*luhum gha*dh*abun min rabbihim wa*dz*illatun fii **a**l*h*ay*aa*ti **al**dduny*aa* waka*dzaa*lika naj

Sesungguhnya orang-orang yang menjadikan (patung) anak sapi (sebagai sembahannya), kelak akan menerima kemurkaan dari Tuhan mereka dan kehinaan dalam kehidupan di dunia. Demikianlah Kami memberi balasan kepada orang-orang yang berbuat kebohongan.

7:153

# وَالَّذِيْنَ عَمِلُوا السَّيِّاٰتِ ثُمَّ تَابُوْا مِنْۢ بَعْدِهَا وَاٰمَنُوْٓا اِنَّ رَبَّكَ مِنْۢ بَعْدِهَا لَغَفُوْرٌ رَّحِيْمٌ

wa**a**lla*dz*iina 'amiluu **al**ssayyi-*aa*ti tsumma t*aa*buu min ba'dih*aa* wa*aa*manuu inna rabbaka min ba'dih*aa* laghafuurun ra*h*iim**un**

Dan orang-orang yang telah mengerjakan kejahatan, kemudian bertobat dan beriman, niscaya setelah itu Tuhanmu Maha Pengampun, Maha Penyayang.

7:154

# وَلَمَّا سَكَتَ عَنْ مُّوْسَى الْغَضَبُ اَخَذَ الْاَلْوَاحَۖ وَفِيْ نُسْخَتِهَا هُدًى وَّرَحْمَةٌ لِّلَّذِيْنَ هُمْ لِرَبِّهِمْ يَرْهَبُوْنَ

walamm*aa* sakata 'an muus*aa* **a**lgha*dh*abu akha*dz*a **a**l-alw*aah*a wafii nuskhatih*aa* hudan wara*h*matun lilla*dz*iina hum lirabbihim yarhabuun**a**

Dan setelah amarah Musa mereda, diambilnya (kembali) lauh-lauh (Taurat) itu; di dalam tulisannya terdapat petunjuk dan rahmat bagi orang-orang yang takut kepada Tuhannya.

7:155

# وَاخْتَارَ مُوْسٰى قَوْمَهٗ سَبْعِيْنَ رَجُلًا لِّمِيْقَاتِنَا ۚفَلَمَّآ اَخَذَتْهُمُ الرَّجْفَةُ قَالَ رَبِّ لَوْ شِئْتَ اَهْلَكْتَهُمْ مِّنْ قَبْلُ وَاِيَّايَۗ اَتُهْلِكُنَا بِمَا فَعَلَ السُّفَهَاۤءُ مِنَّاۚ اِنْ هِيَ اِلَّا فِتْنَتُكَۗ تُضِلُّ بِهَا

wa**i**kht*aa*ra muus*aa* qawmahu sab'iina rajulan limiiq*aa*tin*aa* falamm*aa* akha*dz*at-humu **al**rrajfatu q*aa*la rabbi law syi/ta ahlaktahum min qablu wa-iyy*aa*ya atuhlikun*aa*

Dan Musa memilih tujuh puluh orang dari kaumnya untuk (memohon tobat kepada Kami) pada waktu yang telah Kami tentukan. Ketika mereka ditimpa gempa bumi, Musa berkata, “Ya Tuhanku, jika Engkau kehendaki, tentulah Engkau binasakan mereka dan aku sebelum ini

7:156

# ۞ وَاكْتُبْ لَنَا فِيْ هٰذِهِ الدُّنْيَا حَسَنَةً وَّفِى الْاٰخِرَةِ اِنَّا هُدْنَآ اِلَيْكَۗ قَالَ عَذَابِيْٓ اُصِيْبُ بِهٖ مَنْ اَشَاۤءُۚ وَرَحْمَتِيْ وَسِعَتْ كُلَّ شَيْءٍۗ فَسَاَكْتُبُهَا لِلَّذِيْنَ يَتَّقُوْنَ وَيُؤْتُوْنَ الزَّكٰوةَ وَالَّذِيْنَ ه

wa**u**ktub lan*aa* fii h*aadz*ihi **al**dduny*aa* *h*asanatan wafii **a**l-*aa*khirati inn*aa* hudn*aa* ilayka q*aa*la 'a*dzaa*bii u*sh*iibu bihi man asy*aa*u w

Dan tetapkanlah untuk kami kebaikan di dunia ini dan di akhirat. Sungguh, kami kembali (bertobat) kepada Engkau. (Allah) berfirman, “Siksa-Ku akan Aku timpakan kepada siapa yang Aku kehendaki dan rahmat-Ku meliputi segala sesuatu. Maka akan Aku tetapkan r

7:157

# اَلَّذِيْنَ يَتَّبِعُوْنَ الرَّسُوْلَ النَّبِيَّ الْاُمِّيَّ الَّذِيْ يَجِدُوْنَهٗ مَكْتُوْبًا عِنْدَهُمْ فِى التَّوْرٰىةِ وَالْاِنْجِيْلِ يَأْمُرُهُمْ بِالْمَعْرُوْفِ وَيَنْهٰىهُمْ عَنِ الْمُنْكَرِ وَيُحِلُّ لَهُمُ الطَّيِّبٰتِ وَيُحَرِّمُ عَلَيْهِمُ الْ

**al**la*dz*iina yattabi'uuna **al**rrasuula **al**nnabiyya **a**l-ummiyya **al**la*dz*ii yajiduunahu maktuuban 'indahum fii **al**ttawr*aa*ti wa**a**

**(Yaitu) orang-orang yang mengikuti Rasul, Nabi yang ummi (tidak bisa baca tulis) yang (namanya) mereka dapati tertulis di dalam Taurat dan Injil yang ada pada mereka, yang menyuruh mereka berbuat yang makruf dan mencegah dari yang mungkar, dan yang mengha**









7:158

# قُلْ يٰٓاَيُّهَا النَّاسُ اِنِّيْ رَسُوْلُ اللّٰهِ اِلَيْكُمْ جَمِيْعًا ۨالَّذِيْ لَهٗ مُلْكُ السَّمٰوٰتِ وَالْاَرْضِۚ لَآ اِلٰهَ اِلَّا هُوَ يُحْيٖ وَيُمِيْتُۖ فَاٰمِنُوْا بِاللّٰهِ وَرَسُوْلِهِ النَّبِيِّ الْاُمِّيِّ الَّذِيْ يُؤْمِنُ بِاللّٰهِ وَكَلِ

qul y*aa* ayyuh*aa* **al**nn*aa*su innii rasuulu **al**l*aa*hi ilaykum jamii'an **al**la*dz*ii lahu mulku **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i l*a*

Katakanlah (Muhammad), “Wahai manusia! Sesungguhnya aku ini utusan Allah bagi kamu semua, Yang memiliki kerajaan langit dan bumi; tidak ada tuhan (yang berhak disembah) selain Dia, Yang menghidupkan dan mematikan, maka berimanlah kamu kepada Allah dan Ras







7:159

# وَمِنْ قَوْمِ مُوْسٰٓى اُمَّةٌ يَّهْدُوْنَ بِالْحَقِّ وَبِهٖ يَعْدِلُوْنَ

wamin qawmi muus*aa* ummatun yahduuna bi**a**l*h*aqqi wabihi ya'diluun**a**

Dan di antara kaum Musa itu terdapat suatu umat yang memberi petunjuk (kepada manusia) dengan (dasar) kebenaran dan dengan itu (pula) mereka berlaku adil menjalankan keadilan.

7:160

# وَقَطَّعْنٰهُمُ اثْنَتَيْ عَشْرَةَ اَسْبَاطًا اُمَمًاۗ وَاَوْحَيْنَآ اِلٰى مُوْسٰٓى اِذِ اسْتَسْقٰىهُ قَوْمُهٗٓ اَنِ اضْرِبْ بِّعَصَاكَ الْحَجَرَۚ فَانْۢبَجَسَتْ مِنْهُ اثْنَتَا عَشْرَةَ عَيْنًاۗ قَدْ عَلِمَ كُلُّ اُنَاسٍ مَّشْرَبَهُمْۗ وَظَلَّلْنَا عَلَ

waqa*ththh*a'n*aa*humu itsnatay 'asyrata asb*aath*an umaman wa-aw*h*ayn*aa* il*aa* muus*aa* i*dz*i istasq*aa*hu qawmuhu ani i*dh*rib bi'a*shaa*ka **a**l*h*ajara fa**i**n

Dan Kami membagi mereka menjadi dua belas suku yang masing-masing berjumlah besar, dan Kami wahyukan kepada Musa ketika kaumnya meminta air kepadanya, “Pukullah batu itu dengan tongkatmu!” Maka memancarlah dari (batu) itu dua belas mata air. Setiap suku t

7:161

# وَاِذْ قِيْلَ لَهُمُ اسْكُنُوْا هٰذِهِ الْقَرْيَةَ وَكُلُوْا مِنْهَا حَيْثُ شِئْتُمْ وَقُوْلُوْا حِطَّةٌ وَّادْخُلُوا الْبَابَ سُجَّدًا نَّغْفِرْ لَكُمْ خَطِيْۤـٰٔتِكُمْۗ سَنَزِيْدُ الْمُحْسِنِيْنَ

wa-i*dz* qiila lahumu uskunuu h*aadz*ihi **a**lqaryata wakuluu minh*aa* *h*aytsu syi/tum waquuluu *h*i*ththh*atun wa**u**dkhuluu **a**lb*aa*ba sujjadan naghfir lakum kha*th*ii-

Dan (ingatlah), ketika dikatakan kepada mereka (Bani Israil), “Diamlah di negeri ini (Baitulmaqdis) dan makanlah dari (hasil bumi)nya di mana saja kamu kehendaki.” Dan katakanlah, “Bebaskanlah kami dari dosa kami dan masukilah pintu gerbangnya sambil memb

7:162

# فَبَدَّلَ الَّذِيْنَ ظَلَمُوْا مِنْهُمْ قَوْلًا غَيْرَ الَّذِيْ قِيْلَ لَهُمْ فَاَرْسَلْنَا عَلَيْهِمْ رِجْزًا مِّنَ السَّمَاۤءِ بِمَا كَانُوْا يَظْلِمُوْنَ ࣖ

fabaddala **al**la*dz*iina *zh*alamuu minhum qawlan ghayra **al**la*dz*ii qiila lahum fa-arsaln*aa* 'alayhim rijzan mina **al**ssam*aa*-i bim*aa* k*aa*nuu ya*zh*limuun**a**

Maka orang-orang yang zalim di antara mereka mengganti (perkataan itu) dengan perkataan yang tidak dikatakan kepada mereka, maka Kami timpakan kepada mereka azab dari langit disebabkan kezaliman mereka.







7:163

# وَسْـَٔلْهُمْ عَنِ الْقَرْيَةِ الَّتِيْ كَانَتْ حَاضِرَةَ الْبَحْرِۘ اِذْ يَعْدُوْنَ فِى السَّبْتِ اِذْ تَأْتِيْهِمْ حِيْتَانُهُمْ يَوْمَ سَبْتِهِمْ شُرَّعًا وَّيَوْمَ لَا يَسْبِتُوْنَۙ لَا تَأْتِيْهِمْ ۛ كَذٰلِكَ ۛنَبْلُوْهُمْ بِمَا كَانُوْا يَفْسُقُوْنَ

wa**i**s-alhum 'ani **a**lqaryati **al**latii k*aa*nat *had*irata **a**lba*h*ri i*dz* ya'duuna fii **al**ssabti i*dz* ta/tiihim *h*iit*aa*nuhum yawma sabti

Dan tanyakanlah kepada Bani Israil tentang negeri yang terletak di dekat laut ketika mereka melanggar aturan pada hari Sabtu, (yaitu) ketika datang kepada mereka ikan-ikan (yang berada di sekitar) mereka terapung-apung di permukaan air, padahal pada hari-

7:164

# وَاِذْ قَالَتْ اُمَّةٌ مِّنْهُمْ لِمَ تَعِظُوْنَ قَوْمًاۙ ۨاللّٰهُ مُهْلِكُهُمْ اَوْ مُعَذِّبُهُمْ عَذَابًا شَدِيْدًاۗ قَالُوْا مَعْذِرَةً اِلٰى رَبِّكُمْ وَلَعَلَّهُمْ يَتَّقُوْنَ

wa-i*dz* q*aa*lat ummatun minhum lima ta'i*zh*uuna qawman **al**l*aa*hu muhlikuhum aw mu'a*dzdz*ibuhum 'a*dzaa*ban syadiidan q*aa*luu ma'*dz*iratan il*aa* rabbikum wala'allahum yattaquun**a**

**Dan (ingatlah) ketika suatu umat di antara mereka berkata, “Mengapa kamu menasihati kaum yang akan dibinasakan atau diazab Allah dengan azab yang sangat keras?” Mereka menjawab, “Agar kami mempunyai alasan (lepas tanggung jawab) kepada Tuhanmu, dan agar m**









7:165

# فَلَمَّا نَسُوْا مَا ذُكِّرُوْا بِهٖٓ اَنْجَيْنَا الَّذِيْنَ يَنْهَوْنَ عَنِ السُّوْۤءِ وَاَخَذْنَا الَّذِيْنَ ظَلَمُوْا بِعَذَابٍۢ بَـِٔيْسٍۢ بِمَا كَانُوْا يَفْسُقُوْنَ

falamm*aa* nasuu m*aa* *dz*ukkiruu bihi anjayn*aa* **al**la*dz*iina yanhawna 'ani **al**ssuu-i wa-akha*dz*n*aa* **al**la*dz*iina *zh*alamuu bi'a*dzaa*bin ba-iisin bim

Maka setelah mereka melupakan apa yang diperingatkan kepada mereka, Kami selamatkan orang-orang yang melarang orang berbuat jahat dan Kami timpakan kepada orang-orang yang zalim siksaan yang keras, disebabkan mereka selalu berbuat fasik.

7:166

# فَلَمَّا عَتَوْا عَنْ مَّا نُهُوْا عَنْهُ قُلْنَا لَهُمْ كُوْنُوْا قِرَدَةً خَاسِـِٕيْنَ

falamm*aa* 'ataw 'an m*aa* nuhuu 'anhu quln*aa* lahum kuunuu qiradatan kh*aa*si-iin**a**

Maka setelah mereka bersikap sombong terhadap segala apa yang dilarang. Kami katakan kepada mereka, “Jadilah kamu kera yang hina.”

7:167

# وَاِذْ تَاَذَّنَ رَبُّكَ لَيَبْعَثَنَّ عَلَيْهِمْ اِلٰى يَوْمِ الْقِيٰمَةِ مَنْ يَّسُوْمُهُمْ سُوْۤءَ الْعَذَابِۗ اِنَّ رَبَّكَ لَسَرِيْعُ الْعِقَابِۖ وَاِنَّهٗ لَغَفُوْرٌ رَّحِيْمٌ

wa-i*dz* ta-a*dzdz*ana rabbuka layab'atsanna 'alayhim il*aa* yawmi **a**lqiy*aa*mati man yasuumuhum suu-a **a**l'a*dzaa*bi inna rabbaka lasarii'u **a**l'iq*aa*bi wa-innahu laghafuurun ra

Dan (ingatlah), ketika Tuhanmu memberitahukan, bahwa sungguh, Dia akan mengirim orang-orang yang akan menimpakan azab yang seburuk-buruknya kepada mereka (orang Yahudi) sampai hari Kiamat. Sesungguhnya Tuhanmu sangat cepat siksa-Nya, dan sesungguhnya Dia

7:168

# وَقَطَّعْنٰهُمْ فِى الْاَرْضِ اُمَمًاۚ مِنْهُمُ الصّٰلِحُوْنَ وَمِنْهُمْ دُوْنَ ذٰلِكَ ۖوَبَلَوْنٰهُمْ بِالْحَسَنٰتِ وَالسَّيِّاٰتِ لَعَلَّهُمْ يَرْجِعُوْنَ

waqa*ththh*a'n*aa*hum fii **a**l-ar*dh*i umaman minhumu **al***shshaa*li*h*uuna waminhum duuna *dzaa*lika wabalawn*aa*hum bi**a**l*h*asan*aa*ti wa**al**ssayyi

Dan Kami pecahkan mereka di dunia ini menjadi beberapa golongan; di antaranya ada orang-orang yang saleh dan ada yang tidak demikian. Dan Kami uji mereka dengan (nikmat) yang baik-baik dan (bencana) yang buruk-buruk, agar mereka kembali (kepada kebenaran)

7:169

# فَخَلَفَ مِنْۢ بَعْدِهِمْ خَلْفٌ وَّرِثُوا الْكِتٰبَ يَأْخُذُوْنَ عَرَضَ هٰذَا الْاَدْنٰى وَيَقُوْلُوْنَ سَيُغْفَرُ لَنَاۚ وَاِنْ يَّأْتِهِمْ عَرَضٌ مِّثْلُهٗ يَأْخُذُوْهُۗ اَلَمْ يُؤْخَذْ عَلَيْهِمْ مِّيْثَاقُ الْكِتٰبِ اَنْ لَّا يَقُوْلُوْا عَلَى اللّٰ

fakhalafa min ba'dihim khalfun waritsuu alkit*aa*ba ya/khu*dz*uuna 'ara*dh*a h*aadzaa* al-adn*aa* wayaquuluuna sayughfaru lan*aa* wa-in ya/tihim 'ara*dh*un mitsluhu ya/khu*dz*uuhu alam yu/kha*dz* 'alayhim miits

Maka setelah mereka, datanglah generasi (yang jahat) yang mewarisi Taurat, yang mengambil harta benda dunia yang rendah ini. Lalu mereka berkata, “Kami akan diberi ampun.” Dan kelak jika harta benda dunia datang kepada mereka sebanyak itu (pula), niscaya

7:170

# وَالَّذِيْنَ يُمَسِّكُوْنَ بِالْكِتٰبِ وَاَقَامُوا الصَّلٰوةَۗ اِنَّا لَا نُضِيْعُ اَجْرَ الْمُصْلِحِيْنَ

wa**a**lla*dz*iina yumassikuuna bi**a**lkit*aa*bi wa-aq*aa*muu **al***shsh*al*aa*ta inn*aa* l*aa* nu*dh*ii'u ajra **a**lmu*sh*li*h*iin**a**

Dan orang-orang yang berpegang teguh pada Kitab (Taurat) serta melaksanakan salat, (akan diberi pahala). Sungguh, Kami tidak akan menghilangkan pahala orang-orang saleh.

7:171

# ۞ وَاِذْ نَتَقْنَا الْجَبَلَ فَوْقَهُمْ كَاَنَّهٗ ظُلَّةٌ وَّظَنُّوْٓا اَنَّهٗ وَاقِعٌۢ بِهِمْۚ خُذُوْا مَآ اٰتَيْنٰكُمْ بِقُوَّةٍ وَّاذْكُرُوْا مَا فِيْهِ لَعَلَّكُمْ تَتَّقُوْنَ ࣖ

wa-i*dz* nataqn*aa* **a**ljabala fawqahum ka-annahu *zh*ullatun wa*zh*annuu annahu w*aa*qi'un bihim khu*dz*uu m*aa* *aa*tayn*aa*kum biquwwatin wa**u***dz*kuruu m*aa* fiihi la

Dan (ingatlah) ketika Kami mengangkat gunung ke atas mereka, seakan-akan (gunung) itu naungan awan dan mereka yakin bahwa (gunung) itu akan jatuh menimpa mereka. (Dan Kami firmankan kepada mereka), “Peganglah dengan teguh apa yang telah Kami berikan kepad

7:172

# وَاِذْ اَخَذَ رَبُّكَ مِنْۢ بَنِيْٓ اٰدَمَ مِنْ ظُهُوْرِهِمْ ذُرِّيَّتَهُمْ وَاَشْهَدَهُمْ عَلٰٓى اَنْفُسِهِمْۚ اَلَسْتُ بِرَبِّكُمْۗ قَالُوْا بَلٰىۛ شَهِدْنَا ۛاَنْ تَقُوْلُوْا يَوْمَ الْقِيٰمَةِ اِنَّا كُنَّا عَنْ هٰذَا غٰفِلِيْنَۙ

wa-i*dz* akha*dz*a rabbuka min banii *aa*dama min *zh*uhuurihim *dz*urriyyatahum wa-asyhadahum 'al*aa* anfusihim alastu birabbikum q*aa*luu bal*aa* syahidn*aa* an taquuluu yawma alqiy*aa*mati inn*aa*

Dan (ingatlah) ketika Tuhanmu mengeluarkan dari sulbi (tulang belakang) anak cucu Adam keturunan mereka dan Allah mengambil kesaksian terhadap roh mereka (seraya berfirman), “Bukankah Aku ini Tuhanmu?” Mereka menjawab, “Betul (Engkau Tuhan kami), kami ber

7:173

# اَوْ تَقُوْلُوْٓا اِنَّمَآ اَشْرَكَ اٰبَاۤؤُنَا مِنْ قَبْلُ وَكُنَّا ذُرِّيَّةً مِّنْۢ بَعْدِهِمْۚ اَفَتُهْلِكُنَا بِمَا فَعَلَ الْمُبْطِلُوْنَ

aw taquuluu innam*aa* asyraka *aa*b*aa*un*aa* min qablu wakunn*aa* *dz*urriyyatan min ba'dihim afatuhlikun*aa* bim*aa* fa'ala **a**lmub*th*iluun**a**

Atau agar kamu tidak mengatakan, “Sesungguhnya nenek moyang kami telah mempersekutukan Tuhan sejak dahulu, sedang kami adalah keturunan yang (datang) setelah mereka. Maka apakah Engkau akan membinasakan kami karena perbuatan orang-orang (dahulu) yang sesa

7:174

# وَكَذٰلِكَ نُفَصِّلُ الْاٰيٰتِ وَلَعَلَّهُمْ يَرْجِعُوْنَ

waka*dzaa*lika nufa*shsh*ilu **a**l-*aa*y*aa*ti wala'allahum yarji'uun**a**

Dan demikianlah Kami menjelaskan ayat-ayat itu, agar mereka kembali (kepada kebenaran).

7:175

# وَاتْلُ عَلَيْهِمْ نَبَاَ الَّذِيْٓ اٰتَيْنٰهُ اٰيٰتِنَا فَانْسَلَخَ مِنْهَا فَاَتْبَعَهُ الشَّيْطٰنُ فَكَانَ مِنَ الْغٰوِيْنَ

wa**u**tlu 'alayhim naba-a **al**la*dz*ii *aa*tayn*aa*hu *aa*y*aa*tin*aa* fa**i**nsalakha minh*aa* fa-atba'ahu **al**sysyay*thaa*nu fak*aa*na mina

Dan bacakanlah (Muhammad) kepada mereka, berita orang yang telah Kami berikan ayat-ayat Kami kepadanya, kemudian dia melepaskan diri dari ayat-ayat itu, lalu dia diikuti oleh setan (sampai dia tergoda), maka jadilah dia termasuk orang yang sesat.

7:176

# وَلَوْ شِئْنَا لَرَفَعْنٰهُ بِهَا وَلٰكِنَّهٗٓ اَخْلَدَ اِلَى الْاَرْضِ وَاتَّبَعَ هَوٰىهُۚ فَمَثَلُهٗ كَمَثَلِ الْكَلْبِۚ اِنْ تَحْمِلْ عَلَيْهِ يَلْهَثْ اَوْ تَتْرُكْهُ يَلْهَثْۗ ذٰلِكَ مَثَلُ الْقَوْمِ الَّذِيْنَ كَذَّبُوْا بِاٰيٰتِنَاۚ فَاقْصُصِ الْقَ

walaw syi/n*aa* larafa'n*aa*hu bih*aa* wal*aa*kinnahu akhlada il*aa* **a**l-ar*dh*i wa**i**ttaba'a haw*aa*hu famatsaluhu kamatsali **a**lkalbi in ta*h*mil 'alayhi yalhats aw ta

Dan sekiranya Kami menghendaki niscaya Kami tinggikan (derajat)nya dengan (ayat-ayat) itu, tetapi dia cenderung kepada dunia dan mengikuti keinginannya (yang rendah), maka perumpamaannya seperti anjing, jika kamu menghalaunya dijulurkan lidahnya dan jika

7:177

# سَاۤءَ مَثَلًا ۨالْقَوْمُ الَّذِيْنَ كَذَّبُوْا بِاٰيٰتِنَا وَاَنْفُسَهُمْ كَانُوْا يَظْلِمُوْنَ

s*aa*-a matsalan **a**lqawmu **al**la*dz*iina ka*dzdz*abuu bi-*aa*y*aa*tin*aa* wa-anfusahum k*aa*nuu ya*zh*limuun**a**

Sangat buruk perumpamaan orang-orang yang mendustakan ayat-ayat Kami; mereka menzalimi diri sendiri.

7:178

# مَنْ يَّهْدِ اللّٰهُ فَهُوَ الْمُهْتَدِيْۚ وَمَنْ يُّضْلِلْ فَاُولٰۤىِٕكَ هُمُ الْخٰسِرُوْنَ

man yahdi **al**l*aa*hu fahuwa **a**lmuhtadii waman yu*dh*lil faul*aa*-ika humu **a**lkh*aa*siruun**a**

Barangsiapa diberi petunjuk oleh Allah, maka dialah yang mendapat petunjuk; dan barangsiapa disesatkan Allah, maka merekalah orang-orang yang rugi.

7:179

# وَلَقَدْ ذَرَأْنَا لِجَهَنَّمَ كَثِيْرًا مِّنَ الْجِنِّ وَالْاِنْسِۖ لَهُمْ قُلُوْبٌ لَّا يَفْقَهُوْنَ بِهَاۖ وَلَهُمْ اَعْيُنٌ لَّا يُبْصِرُوْنَ بِهَاۖ وَلَهُمْ اٰذَانٌ لَّا يَسْمَعُوْنَ بِهَاۗ اُولٰۤىِٕكَ كَالْاَنْعَامِ بَلْ هُمْ اَضَلُّ ۗ اُولٰۤىِٕكَ

walaqad *dz*ara/n*aa* lijahannama katsiiran mina **a**ljinni wa**a**l-insi lahum quluubun l*aa* yafqahuuna bih*aa* walahum a'yunun l*aa* yub*sh*iruuna bih*aa* walahum *aatsaa*nun l*aa*

Dan sungguh, akan Kami isi neraka Jahanam banyak dari kalangan jin dan manusia. Mereka memiliki hati, tetapi tidak dipergunakannya untuk memahami (ayat-ayat Allah) dan mereka memiliki mata (tetapi) tidak dipergunakannya untuk melihat (tanda-tanda kekuasaa

7:180

# وَلِلّٰهِ الْاَسْمَاۤءُ الْحُسْنٰى فَادْعُوْهُ بِهَاۖ وَذَرُوا الَّذِيْنَ يُلْحِدُوْنَ فِيْٓ اَسْمَاۤىِٕهٖۗ سَيُجْزَوْنَ مَا كَانُوْا يَعْمَلُوْنَ ۖ

walill*aa*hi **a**l-asm*aa*u **a**l*h*usn*aa* fa**u**d'uuhu bih*aa* wa*dz*aruu **al**la*dz*iina yul*h*iduuna fii asm*aa*-ihi sayujzawna m*aa* k*aa*n

Dan Allah memiliki Asma'ul-husna (nama-nama yang terbaik), maka bermohonlah kepada-Nya dengan menyebutnya Asma'ul-husna itu dan tinggalkanlah orang-orang yang menyalahartikan nama-nama-Nya. Mereka kelak akan mendapat balasan terhadap apa yang telah mereka

7:181

# وَمِمَّنْ خَلَقْنَآ اُمَّةٌ يَّهْدُوْنَ بِالْحَقِّ وَبِهٖ يَعْدِلُوْنَ ࣖ

wamimman khalaqn*aa* ummatun yahduuna bi**a**l*h*aqqi wabihi ya'diluun**a**

Dan di antara orang-orang yang telah Kami ciptakan ada umat yang memberi petunjuk dengan (dasar) kebenaran, dan dengan itu (pula) mereka berlaku adil.

7:182

# وَالَّذِيْنَ كَذَّبُوْا بِاٰيٰتِنَا سَنَسْتَدْرِجُهُمْ مِّنْ حَيْثُ لَا يَعْلَمُوْنَ

wa**a**lla*dz*iina ka*dzdz*abuu bi-*aa*y*aa*tin*aa* sanastadrijuhum min *h*aytsu l*aa* ya'lamuun**a**

Dan orang-orang yang mendustakan ayat-ayat Kami, akan Kami biarkan mereka berangsur-angsur (ke arah kebinasaan), dengan cara yang tidak mereka ketahui.

7:183

# وَاُمْلِيْ لَهُمْ ۗاِنَّ كَيْدِيْ مَتِيْنٌ

waumlii lahum inna kaydii matiin**un**

Dan Aku akan memberikan tenggang waktu kepada mereka. Sungguh, rencana-Ku sangat teguh.

7:184

# اَوَلَمْ يَتَفَكَّرُوْا مَا بِصَاحِبِهِمْ مِّنْ جِنَّةٍۗ اِنْ هُوَ اِلَّا نَذِيْرٌ مُّبِيْنٌ

awa lam yatafakkaruu m*aa* bi*shaah*ibihim min jinnatin in huwa ill*aa* na*dz*iirun mubiin**un**

Dan apakah mereka tidak merenungkan bahwa teman mereka (Muhammad) tidak gila. Dia (Muhammad) tidak lain hanyalah seorang pemberi peringatan yang jelas.

7:185

# اَوَلَمْ يَنْظُرُوْا فِيْ مَلَكُوْتِ السَّمٰوٰتِ وَالْاَرْضِ وَمَا خَلَقَ اللّٰهُ مِنْ شَيْءٍ وَّاَنْ عَسٰٓى اَنْ يَّكُوْنَ قَدِ اقْتَرَبَ اَجَلُهُمْۖ فَبِاَيِّ حَدِيْثٍۢ بَعْدَهٗ يُؤْمِنُوْنَ

awalam yan*zh*uruu fii malakuuti **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i wam*aa* khalaqa **al**l*aa*hu min syay-in wa-an 'as*aa* an yakuuna qadi iqtaraba ajaluhum fabi-ayyi *h*a

Dan apakah mereka tidak memperhatikan kerajaan langit dan bumi dan segala apa yang diciptakan Allah, dan kemungkinan telah dekatnya waktu (kebinasaan) mereka? Lalu berita mana lagi setelah ini yang akan mereka percayai?

7:186

# مَنْ يُّضْلِلِ اللّٰهُ فَلَا هَادِيَ لَهٗ ۖوَيَذَرُهُمْ فِيْ طُغْيَانِهِمْ يَعْمَهُوْنَ

man yu*dh*lili **al**l*aa*hu fal*aa* h*aa*diya lahu waya*dz*aruhum fii *th*ughy*aa*nihim ya'mahuun**a**

Barangsiapa dibiarkan sesat oleh Allah, maka tidak ada yang mampu memberi petunjuk. Allah membiarkannya terombang-ambing dalam kesesatan.

7:187

# يَسْـَٔلُوْنَكَ عَنِ السَّاعَةِ اَيَّانَ مُرْسٰىهَاۗ قُلْ اِنَّمَا عِلْمُهَا عِنْدَ رَبِّيْۚ لَا يُجَلِّيْهَا لِوَقْتِهَآ اِلَّا هُوَۘ ثَقُلَتْ فِى السَّمٰوٰتِ وَالْاَرْضِۗ لَا تَأْتِيْكُمْ اِلَّا بَغْتَةً ۗيَسْـَٔلُوْنَكَ كَاَنَّكَ حَفِيٌّ عَنْهَاۗ قُ

yas-aluunaka 'ani **al**ss*aa*'ati ayy*aa*na murs*aa*h*aa* qul innam*aa* 'ilmuh*aa* 'inda rabbii l*aa* yujalliih*aa* liwaqtih*aa* ill*aa* huwa tsaqulat fii **al**ssam*aa*w

Mereka menanyakan kepadamu (Muhammad) tentang Kiamat, “Kapan terjadi?” Katakanlah, “Sesungguhnya pengetahuan tentang Kiamat itu ada pada Tuhanku; tidak ada (seorang pun) yang dapat menjelaskan waktu terjadinya selain Dia. (Kiamat) itu sangat berat (huru-h







7:188

# قُلْ لَّآ اَمْلِكُ لِنَفْسِيْ نَفْعًا وَّلَا ضَرًّا اِلَّا مَا شَاۤءَ اللّٰهُ ۗوَلَوْ كُنْتُ اَعْلَمُ الْغَيْبَ لَاسْتَكْثَرْتُ مِنَ الْخَيْرِۛ وَمَا مَسَّنِيَ السُّوْۤءُ ۛاِنْ اَنَا۠ اِلَّا نَذِيْرٌ وَّبَشِيْرٌ لِّقَوْمٍ يُّؤْمِنُوْنَ ࣖ

qul l*aa* amliku linafsii naf'an wal*aa* *dh*arran ill*aa* m*aa* sy*aa*-a **al**l*aa*hu walaw kuntu a'lamu **a**lghayba la**i**staktsartu mina **a**lkhayri wam*aa*

Katakanlah (Muhammad), “Aku tidak kuasa mendatangkan manfaat maupun menolak mudarat bagi diriku kecuali apa yang dikehendaki Allah. Sekiranya aku mengetahui yang gaib, niscaya aku membuat kebajikan sebanyak-banyaknya dan tidak akan ditimpa bahaya. Aku han

7:189

# ۞ هُوَ الَّذِيْ خَلَقَكُمْ مِّنْ نَّفْسٍ وَّاحِدَةٍ وَّجَعَلَ مِنْهَا زَوْجَهَا لِيَسْكُنَ اِلَيْهَاۚ فَلَمَّا تَغَشّٰىهَا حَمَلَتْ حَمْلًا خَفِيْفًا فَمَرَّتْ بِهٖ ۚفَلَمَّآ اَثْقَلَتْ دَّعَوَا اللّٰهَ رَبَّهُمَا لَىِٕنْ اٰتَيْتَنَا صَالِحًا لَّنَكُوْنَ

huwa **al**la*dz*ii khalaqakum min nafsin w*aah*idatin waja'ala minh*aa* zawjah*aa* liyaskuna ilayh*aa* falamm*aa* taghasysy*aa*h*aa* *h*amalat *h*amlan khafiifan famarrat bihi falamm*aa*

Dialah yang menciptakan kamu dari jiwa yang satu (Adam) dan daripadanya Dia menciptakan pasangannya, agar dia merasa senang kepadanya. Maka setelah dicampurinya, (istrinya) mengandung kandungan yang ringan, dan teruslah dia merasa ringan (beberapa waktu).

7:190

# فَلَمَّآ اٰتٰىهُمَا صَالِحًا جَعَلَا لَهٗ شُرَكَاۤءَ فِيْمَآ اٰتٰىهُمَا ۚفَتَعٰلَى اللّٰهُ عَمَّا يُشْرِكُوْنَ

falamm*aa* *aa*t*aa*hum*aa* *shaa*li*h*an ja'al*aa* lahu syurak*aa*-a fiim*aa* *aa*t*aa*hum*aa* fata'*aa*l*aa* **al**l*aa*hu 'amm*aa* yusyrikuun**a**

Maka setelah Dia memberi keduanya seorang anak yang saleh, mereka menjadikan sekutu bagi Allah terhadap anak yang telah dianugerahkan-Nya itu. Maka Mahatinggi Allah dari apa yang mereka persekutukan.

7:191

# اَيُشْرِكُوْنَ مَا لَا يَخْلُقُ شَيْـًٔا وَّهُمْ يُخْلَقُوْنَۖ

ayusyrikuuna m*aa* l*aa* yakhluqu syay-an wahum yukhlaquun**a**

Mengapa mereka mempersekutukan (Allah dengan) sesuatu (berhala) yang tidak dapat menciptakan sesuatu apa pun? Padahal (berhala) itu sendiri diciptakan.

7:192

# وَلَا يَسْتَطِيْعُوْنَ لَهُمْ نَصْرًا وَّلَآ اَنْفُسَهُمْ يَنْصُرُوْنَ

wal*aa* yasta*th*ii'uuna lahum na*sh*ran wal*aa* anfusahum yan*sh*uruun**a**

Dan (berhala) itu tidak dapat memberikan pertolongan kepada penyembahnya, dan kepada dirinya sendiri pun mereka tidak dapat memberi pertolongan.

7:193

# وَاِنْ تَدْعُوْهُمْ اِلَى الْهُدٰى لَا يَتَّبِعُوْكُمْۗ سَوَۤاءٌ عَلَيْكُمْ اَدَعَوْتُمُوْهُمْ اَمْ اَنْتُمْ صَامِتُوْنَ

wa-in tad'uuhum il*aa* **a**lhud*aa* l*aa* yattabi'uukum saw*aa*un 'alaykum ada'awtumuuhum am antum *shaa*mituun**a**

Dan jika kamu (wahai orang-orang musyrik) menyerunya (berhala-berhala) untuk memberi petunjuk kepadamu, tidaklah berhala-berhala itu dapat memperkenankan seruanmu; sama saja (hasilnya) buat kamu menyeru mereka atau berdiam diri.

7:194

# اِنَّ الَّذِيْنَ تَدْعُوْنَ مِنْ دُوْنِ اللّٰهِ عِبَادٌ اَمْثَالُكُمْ فَادْعُوْهُمْ فَلْيَسْتَجِيْبُوْا لَكُمْ اِنْ كُنْتُمْ صٰدِقِيْنَ

inna **al**la*dz*iina tad'uuna min duuni **al**l*aa*hi 'ib*aa*dun amts*aa*lukum fa**u**d'uuhum falyastajiibuu lakum in kuntum *shaa*diqiin**a**

Sesungguhnya mereka (berhala-berhala) yang kamu seru selain Allah adalah makhluk (yang lemah) yang serupa juga dengan kamu. Maka serulah mereka lalu biarkanlah mereka memperkenankan permintaanmu, jika kamu orang yang benar.

7:195

# اَلَهُمْ اَرْجُلٌ يَّمْشُوْنَ بِهَآ ۖ اَمْ لَهُمْ اَيْدٍ يَّبْطِشُوْنَ بِهَآ ۖ اَمْ لَهُمْ اَعْيُنٌ يُّبْصِرُوْنَ بِهَآ ۖ اَمْ لَهُمْ اٰذَانٌ يَّسْمَعُوْنَ بِهَاۗ قُلِ ادْعُوْا شُرَكَاۤءَكُمْ ثُمَّ كِيْدُوْنِ فَلَا تُنْظِرُوْنِ

alahum arjulun yamsyuuna bih*aa* am lahum aydin yab*th*isyuuna bih*aa* am lahum a'yunun yub*sh*iruuna bih*aa* am lahum *aatsaa*nun yasma'uuna bih*aa* quli ud'uu syurak*aa*-akum tsumma kiiduuni fal*aa* tun*zh<*

Apakah mereka (berhala-berhala) mempunyai kaki untuk berjalan, atau mempunyai tangan untuk memegang dengan keras, atau mempunyai mata untuk melihat, atau mempunyai telinga untuk mendengar? Katakanlah (Muhammad), “Panggillah (berhala-berhalamu) yang kamu a







7:196

# اِنَّ وَلِيِّ َۧ اللّٰهُ الَّذِيْ نَزَّلَ الْكِتٰبَۖ وَهُوَ يَتَوَلَّى الصّٰلِحِيْنَ

inna waliyyiya **al**l*aa*hu **al**la*dz*ii nazzala **a**lkit*aa*ba wahuwa yatawall*aa* **al***shshaa*li*h*iin**a**

Sesungguhnya pelindungku adalah Allah yang telah menurunkan Kitab (Al-Qur'an). Dia melindungi orang-orang saleh.

7:197

# وَالَّذِيْنَ تَدْعُوْنَ مِنْ دُوْنِهٖ لَا يَسْتَطِيْعُوْنَ نَصْرَكُمْ وَلَآ اَنْفُسَهُمْ يَنْصُرُوْنَ

wa**a**lla*dz*iina tad'uuna min duunihi l*aa* yasta*th*ii'uuna na*sh*rakum wal*aa* anfusahum yan*sh*uruun**a**

Dan berhala-berhala yang kamu seru selain Allah tidaklah sanggup menolongmu, bahkan tidak dapat menolong dirinya sendiri.”

7:198

# وَاِنْ تَدْعُوْهُمْ اِلَى الْهُدٰى لَا يَسْمَعُوْاۗ وَتَرٰىهُمْ يَنْظُرُوْنَ اِلَيْكَ وَهُمْ لَا يُبْصِرُوْنَ

wa-in tad'uuhum il*aa* **a**lhud*aa* l*aa* yasma'uu watar*aa*hum yan*zh*uruuna ilayka wahum l*aa* yub*sh*iruun**a**

Dan jika kamu menyeru mereka (berhala-berhala) untuk memberi petunjuk, mereka tidak dapat mendengarnya. Dan kamu lihat mereka memandangmu padahal mereka tidak melihat.

7:199

# خُذِ الْعَفْوَ وَأْمُرْ بِالْعُرْفِ وَاَعْرِضْ عَنِ الْجَاهِلِيْنَ

khu*dz*i **a**l'afwa wa/mur bi**a**l'urfi wa-a'ri*dh* 'ani **a**lj*aa*hiliin**a**

Jadilah pemaaf dan suruhlah orang mengerjakan yang makruf, serta jangan pedulikan orang-orang yang bodoh.

7:200

# وَاِمَّا يَنْزَغَنَّكَ مِنَ الشَّيْطٰنِ نَزْغٌ فَاسْتَعِذْ بِاللّٰهِ ۗاِنَّهٗ سَمِيْعٌ عَلِيْمٌ

wa-imm*aa* yanzaghannaka mina **al**sysyay*thaa*ni nazghun fa**i**sta'i*dz* bi**al**l*aa*hi innahu samii'un 'aliim**un**

Dan jika setan datang menggodamu, maka berlindunglah kepada Allah. Sungguh, Dia Maha Mendengar, Maha Mengetahui.

7:201

# اِنَّ الَّذِيْنَ اتَّقَوْا اِذَا مَسَّهُمْ طٰۤىِٕفٌ مِّنَ الشَّيْطٰنِ تَذَكَّرُوْا فَاِذَا هُمْ مُّبْصِرُوْنَۚ

inna **al**la*dz*iina ittaqaw i*dzaa* massahum *thaa*-ifun mina **al**sysyay*thaa*ni ta*dz*akkaruu fa-i*dzaa* hum mub*sh*iruun**a**

Sesungguhnya orang-orang yang bertakwa apabila mereka dibayang-bayangi pikiran jahat (berbuat dosa) dari setan, mereka pun segera ingat kepada Allah, maka ketika itu juga mereka melihat (kesalahan-kesalahannya).

7:202

# وَاِخْوَانُهُمْ يَمُدُّوْنَهُمْ فِى الْغَيِّ ثُمَّ لَا يُقْصِرُوْنَ

wa-ikhw*aa*nuhum yamudduunahum fii **a**lghayyi tsumma l*aa* yuq*sh*iruun**a**

Dan teman-teman mereka (orang kafir dan fasik) membantu setan-setan dalam menyesatkan dan mereka tidak henti-hentinya (menyesatkan).

7:203

# وَاِذَا لَمْ تَأْتِهِمْ بِاٰيَةٍ قَالُوْا لَوْلَا اجْتَبَيْتَهَاۗ قُلْ اِنَّمَآ اَتَّبِعُ مَا يُوْحٰٓى اِلَيَّ مِنْ رَّبِّيْۗ هٰذَا بَصَاۤىِٕرُ مِنْ رَّبِّكُمْ وَهُدًى وَّرَحْمَةٌ لِّقَوْمٍ يُّؤْمِنُوْنَ

wa-i*dzaa* lam ta/tihim bi-*aa*yatin q*aa*luu lawl*aa* ijtabaytah*aa* qul innam*aa* attabi'u m*aa* yuu*haa* ilayya min rabbii h*aadzaa* ba*shaa*-iru min rabbikum wahudan wara*h*matun liqawmin yu/minuu

Dan apabila engkau (Muhammad) tidak membacakan suatu ayat kepada mereka, mereka berkata, “Mengapa tidak engkau buat sendiri ayat itu?” Katakanlah (Muhammad), “Sesungguhnya aku hanya mengikuti apa yang diwahyukan Tuhanku kepadaku. (Al-Qur'an) ini adalah bu

7:204

# وَاِذَا قُرِئَ الْقُرْاٰنُ فَاسْتَمِعُوْا لَهٗ وَاَنْصِتُوْا لَعَلَّكُمْ تُرْحَمُوْنَ

wa-i*dzaa* quri-a **a**lqur-*aa*nu fa**i**stami'uu lahu wa-an*sh*ituu la'allakum tur*h*amuun**a**

Dan apabila dibacakan Al-Qur'an, maka dengarkanlah dan diamlah, agar kamu mendapat rahmat.

7:205

# وَاذْكُرْ رَّبَّكَ فِيْ نَفْسِكَ تَضَرُّعًا وَّخِيْفَةً وَّدُوْنَ الْجَهْرِ مِنَ الْقَوْلِ بِالْغُدُوِّ وَالْاٰصَالِ وَلَا تَكُنْ مِّنَ الْغٰفِلِيْنَ

wa**u***dz*kur rabbaka fii nafsika ta*dh*arru'an wakhiifatan waduuna **a**ljahri mina **a**lqawli bi**a**lghuduwwi wa**a**l-*aasaa*li wal*aa* takun mina **a**

**Dan ingatlah Tuhanmu dalam hatimu dengan rendah hati dan rasa takut, dan dengan tidak mengeraskan suara, pada waktu pagi dan petang, dan janganlah kamu termasuk orang-orang yang lengah.**









7:206

# اِنَّ الَّذِيْنَ عِنْدَ رَبِّكَ لَا يَسْتَكْبِرُوْنَ عَنْ عِبَادَتِهٖ وَيُسَبِّحُوْنَهٗ وَلَهٗ يَسْجُدُوْنَ ࣖ ۩

inna **al**la*dz*iina 'inda rabbika l*aa* yastakbiruuna 'an 'ib*aa*datihi wayusabbi*h*uunahu walahu yasjuduun**a**

Sesungguhnya orang-orang yang ada di sisi Tuhanmu tidak merasa enggan untuk menyembah Allah dan mereka menyucikan-Nya dan hanya kepada-Nya mereka bersujud.

<!--EndFragment-->