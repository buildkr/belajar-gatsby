---
title: (49) Al-Hujurat - الحجرٰت
date: 2021-10-27T04:09:50.907Z
ayat: 49
description: "Jumlah Ayat: 18 / Arti: Kamar-Kamar"
---
<!--StartFragment-->

49:1

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْا لَا تُقَدِّمُوْا بَيْنَ يَدَيِ اللّٰهِ وَرَسُوْلِهٖ وَاتَّقُوا اللّٰهَ ۗاِنَّ اللّٰهَ سَمِيْعٌ عَلِيْمٌ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu l*aa* tuqaddimuu bayna yadayi **al**l*aa*hi warasuulihi wa**i**ttaquu **al**l*aa*ha inna **al**l*aa*ha sami

Wahai orang-orang yang beriman! Janganlah kamu mendahului Allah dan Rasul-Nya dan bertakwalah kepada Allah. Sungguh, Allah Maha Mendengar, Maha Mengetahui.

49:2

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْا لَا تَرْفَعُوْٓا اَصْوَاتَكُمْ فَوْقَ صَوْتِ النَّبِيِّ وَلَا تَجْهَرُوْا لَهٗ بِالْقَوْلِ كَجَهْرِ بَعْضِكُمْ لِبَعْضٍ اَنْ تَحْبَطَ اَعْمَالُكُمْ وَاَنْتُمْ لَا تَشْعُرُوْنَ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu l*aa* tarfa'uu a*sh*w*aa*takum fawqa *sh*awti **al**nnabiyyi wal*aa* tajharuu lahu bi**a**lqawli kajahri ba'*dh*ikum liba'<

Wahai orang-orang yang beriman! Janganlah kamu meninggikan suaramu melebihi suara Nabi, dan janganlah kamu berkata kepadanya dengan suara keras sebagaimana kerasnya (suara) sebagian kamu terhadap yang lain, nanti (pahala) segala amalmu bisa terhapus sedan

49:3

# اِنَّ الَّذِيْنَ يَغُضُّوْنَ اَصْوَاتَهُمْ عِنْدَ رَسُوْلِ اللّٰهِ اُولٰۤىِٕكَ الَّذِيْنَ امْتَحَنَ اللّٰهُ قُلُوْبَهُمْ لِلتَّقْوٰىۗ لَهُمْ مَّغْفِرَةٌ وَّاَجْرٌ عَظِيْمٌ

inna **al**la*dz*iina yaghu*dhdh*uuna a*sh*w*aa*tahum 'inda rasuuli **al**l*aa*hi ul*aa*-ika **al**la*dz*iina imta*h*ana **al**l*aa*hu quluubahum li**l<**

Sesungguhnya orang-orang yang merendahkan suaranya di sisi Rasulullah, mereka itulah orang-orang yang telah diuji hatinya oleh Allah untuk bertakwa. Mereka akan memperoleh ampunan dan pahala yang besar.







49:4

# اِنَّ الَّذِيْنَ يُنَادُوْنَكَ مِنْ وَّرَاۤءِ الْحُجُرٰتِ اَكْثَرُهُمْ لَا يَعْقِلُوْنَ

inna **al**la*dz*iina yun*aa*duunaka min war*aa*-i **a**l*h*ujur*aa*ti aktsaruhum l*aa* ya'qiluun**a**

Sesungguhnya orang-orang yang memanggil engkau (Muhammad) dari luar kamar(mu) kebanyakan mereka tidak mengerti.

49:5

# وَلَوْ اَنَّهُمْ صَبَرُوْا حَتّٰى تَخْرُجَ اِلَيْهِمْ لَكَانَ خَيْرًا لَّهُمْ ۗوَاللّٰهُ غَفُوْرٌ رَّحِيْمٌ

walaw annahum *sh*abaruu *h*att*aa* takhruja ilayhim lak*aa*na khayran lahum wa**al**l*aa*hu ghafuurun ra*h*iim**un**

Dan sekiranya mereka bersabar sampai engkau keluar menemui mereka, tentu akan lebih baik bagi mereka. Dan Allah Maha Pengampun, Maha Penyayang.

49:6

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْٓا اِنْ جَاۤءَكُمْ فَاسِقٌۢ بِنَبَاٍ فَتَبَيَّنُوْٓا اَنْ تُصِيْبُوْا قَوْمًاۢ بِجَهَالَةٍ فَتُصْبِحُوْا عَلٰى مَا فَعَلْتُمْ نٰدِمِيْنَ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu in j*aa*-akum f*aa*siqun binaba-in fatabayyanuu an tu*sh*iibuu qawman bijah*aa*latin fatu*sh*bi*h*uu 'al*aa* m*aa* fa'altum n*aa*dimii

Wahai orang-orang yang beriman! Jika seseorang yang fasik datang kepadamu membawa suatu berita, maka telitilah kebenarannya, agar kamu tidak mencelakakan suatu kaum karena kebodohan (kecerobohan), yang akhirnya kamu menyesali perbuatanmu itu.

49:7

# وَاعْلَمُوْٓا اَنَّ فِيْكُمْ رَسُوْلَ اللّٰهِ ۗ لَوْ يُطِيْعُكُمْ فِيْ كَثِيْرٍ مِّنَ الْاَمْرِ لَعَنِتُّمْ وَلٰكِنَّ اللّٰهَ حَبَّبَ اِلَيْكُمُ الْاِيْمَانَ وَزَيَّنَهٗ فِيْ قُلُوْبِكُمْ وَكَرَّهَ اِلَيْكُمُ الْكُفْرَ وَالْفُسُوْقَ وَالْعِصْيَانَ ۗ اُولٰ

wa**i**'lamuu anna fiikum rasuula **al**l*aa*hi law yu*th*ii'ukum fii katsiirin mina **a**l-amri la'anittum wal*aa*kinna **al**l*aa*ha *h*abbaba ilaykumu **a**l-iim<

Dan ketahuilah olehmu bahwa di tengah-tengah kamu ada Rasulullah. Kalau dia menuruti (kemauan) kamu dalam banyak hal pasti kamu akan mendapatkan kesusahan. Tetapi Allah menjadikan kamu cinta kepada keimanan dan menjadikan (iman) itu indah dalam hatimu ser

49:8

# فَضْلًا مِّنَ اللّٰهِ وَنِعْمَةً ۗوَاللّٰهُ عَلِيْمٌ حَكِيْمٌ

fa*dh*lan mina **al**l*aa*hi wani'matan wa**al**l*aa*hu 'aliimun *h*akiim**un**

sebagai karunia dan nikmat dari Allah. Dan Allah Maha Mengetahui, Mahabijaksana.

49:9

# وَاِنْ طَاۤىِٕفَتٰنِ مِنَ الْمُؤْمِنِيْنَ اقْتَتَلُوْا فَاَصْلِحُوْا بَيْنَهُمَاۚ فَاِنْۢ بَغَتْ اِحْدٰىهُمَا عَلَى الْاُخْرٰى فَقَاتِلُوا الَّتِيْ تَبْغِيْ حَتّٰى تَفِيْۤءَ اِلٰٓى اَمْرِ اللّٰهِ ۖفَاِنْ فَاۤءَتْ فَاَصْلِحُوْا بَيْنَهُمَا بِالْعَدْلِ وَاَ

wa-in *thaa*-ifat*aa*ni mina **a**lmu/miniina iqtataluu fa-a*sh*li*h*uu baynahum*aa* fa-in baghat i*h*d*aa*hum*aa* 'al*aa* **a**l-ukhr*aa* faq*aa*tiluu **al**

**Dan apabila ada dua golongan orang-orang mukmin berperang, maka damaikanlah antara keduanya. Jika salah satu dari keduanya berbuat zalim terhadap (golongan) yang lain, maka perangilah (golongan) yang berbuat zalim itu, sehingga golongan itu kembali kepada**









49:10

# اِنَّمَا الْمُؤْمِنُوْنَ اِخْوَةٌ فَاَصْلِحُوْا بَيْنَ اَخَوَيْكُمْ وَاتَّقُوا اللّٰهَ لَعَلَّكُمْ تُرْحَمُوْنَ ࣖ

innam*aa* **a**lmu/minuuna ikhwatun fa-a*sh*li*h*uu bayna akhawaykum wa**i**ttaquu **al**l*aa*ha la'allakum tur*h*amuun**a**

Sesungguhnya orang-orang mukmin itu bersaudara, karena itu damaikanlah antara kedua saudaramu (yang berselisih) dan bertakwalah kepada Allah agar kamu mendapat rahmat.

49:11

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْا لَا يَسْخَرْ قَوْمٌ مِّنْ قَوْمٍ عَسٰٓى اَنْ يَّكُوْنُوْا خَيْرًا مِّنْهُمْ وَلَا نِسَاۤءٌ مِّنْ نِّسَاۤءٍ عَسٰٓى اَنْ يَّكُنَّ خَيْرًا مِّنْهُنَّۚ وَلَا تَلْمِزُوْٓا اَنْفُسَكُمْ وَلَا تَنَابَزُوْا بِالْاَلْقَابِۗ بِئْسَ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu l*aa* yaskhar qawmun min qawmin 'as*aa* an yakuunuu khayran minhum wal*aa* nis*aa*un min nis*aa*-in 'as*aa* an yakunna khayran minhunna wal*aa*

Wahai orang-orang yang beriman! Janganlah suatu kaum mengolok-olok kaum yang lain (karena) boleh jadi mereka (yang diperolok-olokkan) lebih baik dari mereka (yang mengolok-olok) dan jangan pula perempuan-perempuan (mengolok-olokkan) perempuan lain (karena

49:12

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوا اجْتَنِبُوْا كَثِيْرًا مِّنَ الظَّنِّۖ اِنَّ بَعْضَ الظَّنِّ اِثْمٌ وَّلَا تَجَسَّسُوْا وَلَا يَغْتَبْ بَّعْضُكُمْ بَعْضًاۗ اَيُحِبُّ اَحَدُكُمْ اَنْ يَّأْكُلَ لَحْمَ اَخِيْهِ مَيْتًا فَكَرِهْتُمُوْهُۗ وَاتَّقُوا اللّٰهَ ۗ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu ijtanibuu katsiiran mina **al***zhzh*anni inna ba'*dh*a **al***zhzh*anni itsmun wal*aa* tajassasuu wal*aa* yaghtab ba'*dh*u

Wahai orang-orang yang beriman! Jauhilah banyak dari prasangka, sesungguhnya sebagian prasangka itu dosa dan janganlah kamu mencari-cari kesalahan orang lain dan janganlah ada di antara kamu yang menggunjing sebagian yang lain. Apakah ada di antara kamu y

49:13

# يٰٓاَيُّهَا النَّاسُ اِنَّا خَلَقْنٰكُمْ مِّنْ ذَكَرٍ وَّاُنْثٰى وَجَعَلْنٰكُمْ شُعُوْبًا وَّقَبَاۤىِٕلَ لِتَعَارَفُوْا ۚ اِنَّ اَكْرَمَكُمْ عِنْدَ اللّٰهِ اَتْقٰىكُمْ ۗاِنَّ اللّٰهَ عَلِيْمٌ خَبِيْرٌ

y*aa* ayyuh*aa* **al**nn*aa*su inn*aa* khalaqn*aa*kum min *dz*akarin waunts*aa* waja'aln*aa*kum syu'uuban waqab*aa*-ila lita'*aa*rafuu inna akramakum 'inda **al**l*aa*hi atq<

Wahai manusia! Sungguh, Kami telah menciptakan kamu dari seorang laki-laki dan seorang perempuan, kemudian Kami jadikan kamu berbangsa-bangsa dan bersuku-suku agar kamu saling mengenal. Sesungguhnya yang paling mulia di antara kamu di sisi Allah ialah ora

49:14

# ۞ قَالَتِ الْاَعْرَابُ اٰمَنَّا ۗ قُلْ لَّمْ تُؤْمِنُوْا وَلٰكِنْ قُوْلُوْٓا اَسْلَمْنَا وَلَمَّا يَدْخُلِ الْاِيْمَانُ فِيْ قُلُوْبِكُمْ ۗوَاِنْ تُطِيْعُوا اللّٰهَ وَرَسُوْلَهٗ لَا يَلِتْكُمْ مِّنْ اَعْمَالِكُمْ شَيْـًٔا ۗاِنَّ اللّٰهَ غَفُوْرٌ رَّحِيْمٌ

q*aa*lati **a**l-a'r*aa*bu *aa*mann*aa* qul lam tu/minuu wal*aa*kin quuluu aslamn*aa* walamm*aa* yadkhuli **a**l-iim*aa*nu fii quluubikum wa-in tu*th*ii'uu **al**l*aa*

*Orang-orang Arab Badui berkata, “Kami telah beriman.” Katakanlah (kepada mereka), “Kamu belum beriman, tetapi katakanlah ‘Kami telah tunduk (Islam),’ karena iman belum masuk ke dalam hatimu. Dan jika kamu taat kepada Allah dan Rasul-Nya, Dia tidak akan me*









49:15

# اِنَّمَا الْمُؤْمِنُوْنَ الَّذِيْنَ اٰمَنُوْا بِاللّٰهِ وَرَسُوْلِهٖ ثُمَّ لَمْ يَرْتَابُوْا وَجَاهَدُوْا بِاَمْوَالِهِمْ وَاَنْفُسِهِمْ فِيْ سَبِيْلِ اللّٰهِ ۗ اُولٰۤىِٕكَ هُمُ الصّٰدِقُوْنَ

innam*aa* **a**lmu/minuuna **al**la*dz*iina *aa*manuu bi**al**l*aa*hi warasuulihi tsumma lam yart*aa*buu waj*aa*haduu bi-amw*aa*lihim wa-anfusihim fii sabiili **al**l

Sesungguhnya orang-orang mukmin yang sebenarnya adalah mereka yang beriman kepada Allah dan Rasul-Nya kemudian mereka tidak ragu-ragu dan mereka berjihad dengan harta dan jiwanya di jalan Allah. Mereka itulah orang-orang yang benar.

49:16

# قُلْ اَتُعَلِّمُوْنَ اللّٰهَ بِدِيْنِكُمْۗ وَاللّٰهُ يَعْلَمُ مَا فِى السَّمٰوٰتِ وَمَا فِى الْاَرْضِۗ وَاللّٰهُ بِكُلِّ شَيْءٍ عَلِيْمٌ

qul atu'allimuuna **al**l*aa*ha bidiinikum wa**al**l*aa*hu ya'lamu m*aa* fii **al**ssam*aa*w*aa*ti wam*aa* fii **a**l-ar*dh*i wa**al**l*aa*hu bikulli

Katakanlah (kepada mereka), “Apakah kamu akan memberitahukan kepada Allah tentang agamamu (keyakinanmu), padahal Allah mengetahui apa yang ada di langit dan apa yang ada di bumi dan Allah Maha Mengetahui segala sesuatu.”

49:17

# يَمُنُّوْنَ عَلَيْكَ اَنْ اَسْلَمُوْا ۗ قُلْ لَّا تَمُنُّوْا عَلَيَّ اِسْلَامَكُمْ ۚبَلِ اللّٰهُ يَمُنُّ عَلَيْكُمْ اَنْ هَدٰىكُمْ لِلْاِيْمَانِ اِنْ كُنْتُمْ صٰدِقِيْنَ

yamunnuuna 'alayka an aslamuu qul l*aa* tamunnuu 'alayya isl*aa*makum bali **al**l*aa*hu yamunnu 'alaykum an had*aa*kum lil-iim*aa*ni in kuntum *shaa*diqiin**a**

Mereka merasa berjasa kepadamu dengan keislaman mereka. Katakanlah, “Janganlah kamu merasa berjasa kepadaku dengan keislamanmu, sebenarnya Allah yang melimpahkan nikmat kepadamu dengan menunjukkan kamu kepada keimanan, jika kamu orang yang benar.”

49:18

# اِنَّ اللّٰهَ يَعْلَمُ غَيْبَ السَّمٰوٰتِ وَالْاَرْضِۗ وَاللّٰهُ بَصِيْرٌۢ بِمَا تَعْمَلُوْنَ ࣖ

inna **al**l*aa*ha ya'lamu ghayba **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i wa**al**l*aa*hu ba*sh*iirun bim*aa* ta'maluun**a**

Sungguh, Allah mengetahui apa yang gaib di langit dan di bumi. Dan Allah Maha Melihat apa yang kamu kerjakan.

<!--EndFragment-->