---
title: (55) Ar-Rahman - الرحمن
date: 2021-10-27T04:12:58.367Z
ayat: 55
description: "Jumlah Ayat: 78 / Arti: Maha Pengasih"
---
<!--StartFragment-->

55:1

# اَلرَّحْمٰنُۙ

a**l**rra*h*m*aa*n**u**

(Allah) Yang Maha Pengasih,

55:2

# عَلَّمَ الْقُرْاٰنَۗ

'allama **a**lqur-*aa*n**a**

Yang telah mengajarkan Al-Qur'an.

55:3

# خَلَقَ الْاِنْسَانَۙ

khalaqa **a**l-ins*aa*n**a**

Dia menciptakan manusia,

55:4

# عَلَّمَهُ الْبَيَانَ

'allamahu **a**lbay*aa*n**a**

mengajarnya pandai berbicara.

55:5

# اَلشَّمْسُ وَالْقَمَرُ بِحُسْبَانٍۙ

a**l**sysyamsu wa**a**lqamaru bi*h*usb*aa*n**in**

Matahari dan bulan beredar menurut perhitungan,

55:6

# وَّالنَّجْمُ وَالشَّجَرُ يَسْجُدَانِ

wa**al**nnajmu wa**al**sysyajaru yasjud*aa*n**i**

dan tetumbuhan dan pepohonan, keduanya tunduk (kepada-Nya).

55:7

# وَالسَّمَاۤءَ رَفَعَهَا وَوَضَعَ الْمِيْزَانَۙ

wa**al**ssam*aa*-a rafa'ah*aa* wawa*dh*a'a **a**lmiiz*aa*n**a**

Dan langit telah ditinggikan-Nya dan Dia ciptakan keseimbangan,

55:8

# اَلَّا تَطْغَوْا فِى الْمِيْزَانِ

**al**l*aa* ta*th*ghaw fii **a**lmiiz*aa*n**i**

agar kamu jangan merusak keseimbangan itu,

55:9

# وَاَقِيْمُوا الْوَزْنَ بِالْقِسْطِ وَلَا تُخْسِرُوا الْمِيْزَانَ

wa-aqiimuu **a**lwazna bi**a**lqis*th*i wal*aa* tukhsiruu **a**lmiiz*aa*n**a**

dan tegakkanlah keseimbangan itu dengan adil dan janganlah kamu mengurangi keseimbangan itu.

55:10

# وَالْاَرْضَ وَضَعَهَا لِلْاَنَامِۙ

wa**a**l-ar*dh*a wa*dh*a'ah*aa* lil-an*aa*m**i**

Dan bumi telah dibentangkan-Nya untuk makhluk(-Nya),

55:11

# فِيْهَا فَاكِهَةٌ وَّالنَّخْلُ ذَاتُ الْاَكْمَامِۖ

fiih*aa* f*aa*kihatun wa**al**nnakhlu *dzaa*tu **a**l-akm*aa*m**i**

di dalamnya ada buah-buahan dan pohon kurma yang mempunyai kelopak mayang,

55:12

# وَالْحَبُّ ذُو الْعَصْفِ وَالرَّيْحَانُۚ

wa**a**l*h*abbu *dz*uu **a**l'a*sh*fi wa**al**rray*haa*n**i**

dan biji-bijian yang berkulit dan bunga-bunga yang harum baunya.

55:13

# فَبِاَيِّ اٰلَاۤءِ رَبِّكُمَا تُكَذِّبٰنِ

fabi-ayyi *aa*l*aa*-i rabbikum*aa* tuka*dzdz*ib*aa*n**i**

Maka nikmat Tuhanmu yang manakah yang kamu dustakan?

55:14

# خَلَقَ الْاِنْسَانَ مِنْ صَلْصَالٍ كَالْفَخَّارِ

khalaqa **a**l-ins*aa*na min *sh*al*shaa*lin ka**a**lfakhkh*aa*r**i**

Dia menciptakan manusia dari tanah kering seperti tembikar,

55:15

# وَخَلَقَ الْجَاۤنَّ مِنْ مَّارِجٍ مِّنْ نَّارٍۚ

wakhalaqa **a**lj*aa*nna min m*aa*rijin min n*aa*r**in**

dan Dia menciptakan jin dari nyala api tanpa asap.

55:16

# فَبِاَيِّ اٰلَاۤءِ رَبِّكُمَا تُكَذِّبٰنِ

fabi-ayyi *aa*l*aa*-i rabbikum*aa* tuka*dzdz*ib*aa*n**i**

Maka nikmat Tuhanmu yang manakah yang kamu dustakan?

55:17

# رَبُّ الْمَشْرِقَيْنِ وَرَبُّ الْمَغْرِبَيْنِۚ

rabbu **a**lmasyriqayni warabbu **a**lmaghribayn**i**

Tuhan (yang memelihara) dua timur dan Tuhan (yang memelihara) dua barat.

55:18

# فَبِاَيِّ اٰلَاۤءِ رَبِّكُمَا تُكَذِّبٰنِ

fabi-ayyi *aa*l*aa*-i rabbikum*aa* tuka*dzdz*ib*aa*n**i**

Maka nikmat Tuhanmu yang manakah yang kamu dustakan?

55:19

# مَرَجَ الْبَحْرَيْنِ يَلْتَقِيٰنِۙ

maraja **a**lba*h*rayni yaltaqiy*aa*n**i**

Dia membiarkan dua laut mengalir yang (kemudian) keduanya bertemu,

55:20

# بَيْنَهُمَا بَرْزَخٌ لَّا يَبْغِيٰنِۚ

baynahum*aa* barzakhun l*aa* yabghiy*aa*n**i**

di antara keduanya ada batas yang tidak dilampaui oleh masing-masing.

55:21

# فَبِاَيِّ اٰلَاۤءِ رَبِّكُمَا تُكَذِّبٰنِ

fabi-ayyi *aa*l*aa*-i rabbikum*aa* tuka*dzdz*ib*aa*n**i**

Maka nikmat Tuhanmu yang manakah yang kamu dustakan?

55:22

# يَخْرُجُ مِنْهُمَا اللُّؤْلُؤُ وَالْمَرْجَانُۚ

yakhruju minhum*aa* **al**lu/luu wa**a**lmarj*aa*n**u**

Dari keduanya keluar mutiara dan marjan.

55:23

# فَبِاَيِّ اٰلَاۤءِ رَبِّكُمَا تُكَذِّبٰنِ

fabi-ayyi *aa*l*aa*-i rabbikum*aa* tuka*dzdz*ib*aa*ni

Maka nikmat Tuhanmu yang manakah yang kamu dustakan?

55:24

# وَلَهُ الْجَوَارِ الْمُنْشَاٰتُ فِى الْبَحْرِ كَالْاَعْلَامِۚ

walahu **a**ljaw*aa*ri **a**lmunsya*aa*tu fii **a**lba*h*ri ka**a**l-a'l*aa*m**i**

Milik-Nyalah kapal-kapal yang berlayar di lautan bagaikan gunung-gunung.

55:25

# فَبِاَيِّ اٰلَاۤءِ رَبِّكُمَا تُكَذِّبٰنِ ࣖ

fabi-ayyi *aa*l*aa*-i rabbikum*aa* tuka*dzdz*ib*aa*n**i**

Maka nikmat Tuhanmu yang manakah yang kamu dustakan?

55:26

# كُلُّ مَنْ عَلَيْهَا فَانٍۖ

kullu man 'alayh*aa* f*aa*n**in**

Semua yang ada di bumi itu akan binasa,

55:27

# وَّيَبْقٰى وَجْهُ رَبِّكَ ذُو الْجَلٰلِ وَالْاِكْرَامِۚ

wayabq*aa* wajhu rabbika *dz*uu **a**ljal*aa*li wa**a**l-ikr*aa*m**i**

tetapi wajah Tuhanmu yang memiliki kebesaran dan kemuliaan tetap kekal.

55:28

# فَبِاَيِّ اٰلَاۤءِ رَبِّكُمَا تُكَذِّبٰنِ

fabi-ayyi *aa*l*aa*-i rabbikum*aa* tuka*dzdz*ib*aa*n**i**

Maka nikmat Tuhanmu yang manakah yang kamu dustakan?

55:29

# يَسْـَٔلُهٗ مَنْ فِى السَّمٰوٰتِ وَالْاَرْضِۗ كُلَّ يَوْمٍ هُوَ فِيْ شَأْنٍۚ

yas-aluhu man fii **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i kulla yawmin huwa fii sya/n**in**

Apa yang di langit dan di bumi selalu meminta kepada-Nya. Setiap waktu Dia dalam kesibukan.

55:30

# فَبِاَيِّ اٰلَاۤءِ رَبِّكُمَا تُكَذِّبٰنِ

fabi-ayyi *aa*l*aa*-i rabbikum*aa* tuka*dzdz*ib*aa*n**i**

Maka nikmat Tuhanmu yang manakah yang kamu dustakan?

55:31

# سَنَفْرُغُ لَكُمْ اَيُّهَ الثَّقَلٰنِۚ

sanafrughu lakum ayyuh*aa* **al**tstsaqal*aa*n**i**

Kami akan memberi perhatian sepenuhnya kepadamu wahai (golongan) manusia dan jin!

55:32

# فَبِاَيِّ اٰلَاۤءِ رَبِّكُمَا تُكَذِّبٰنِ

fabi-ayyi *aa*l*aa*-i rabbikum*aa* tuka*dzdz*ib*aa*n**i**

Maka nikmat Tuhanmu yang manakah yang kamu dustakan?

55:33

# يٰمَعْشَرَ الْجِنِّ وَالْاِنْسِ اِنِ اسْتَطَعْتُمْ اَنْ تَنْفُذُوْا مِنْ اَقْطَارِ السَّمٰوٰتِ وَالْاَرْضِ فَانْفُذُوْاۗ لَا تَنْفُذُوْنَ اِلَّا بِسُلْطٰنٍۚ

y*aa* ma'syara **a**ljinni wa**a**l-insi ini ista*th*a'tum an tanfu*dz*uu min aq*thaa*ri **al**ssam*aa*w*aa*ti wa**a**l-ar*dh*i fa**u**nfu*dz*u

Wahai golongan jin dan manusia! Jika kamu sanggup menembus (melintasi) penjuru langit dan bumi, maka tembuslah. Kamu tidak akan mampu menembusnya kecuali dengan kekuatan (dari Allah).

55:34

# فَبِاَيِّ اٰلَاۤءِ رَبِّكُمَا تُكَذِّبٰنِ

fabi-ayyi *aa*l*aa*-i rabbikum*aa* tuka*dzdz*ib*aa*n**i**

Maka nikmat Tuhanmu yang manakah yang kamu dustakan?

55:35

# يُرْسَلُ عَلَيْكُمَا شُوَاظٌ مِّنْ نَّارٍۙ وَّنُحَاسٌ فَلَا تَنْتَصِرَانِۚ

yursalu 'alaykum*aa* syuw*aats*un min n*aa*rin wanu*haa*sun fal*aa* tanta*sh*ir*aa*n**i**

Kepada kamu (jin dan manusia), akan dikirim nyala api dan cairan tembaga (panas) sehingga kamu tidak dapat menyelamatkan diri (darinya).

55:36

# فَبِاَيِّ اٰلَاۤءِ رَبِّكُمَا تُكَذِّبٰنِ

fabi-ayyi *aa*l*aa*-i rabbikum*aa* tuka*dzdz*ib*aa*n**i**

Maka nikmat Tuhanmu yang manakah yang kamu dustakan?

55:37

# فَاِذَا انْشَقَّتِ السَّمَاۤءُ فَكَانَتْ وَرْدَةً كَالدِّهَانِۚ

fa-i*dzaa* insyaqqati **al**ssam*aa*u fak*aa*nat wardatan ka**al**ddih*aa*n**i**

Maka apabila langit telah terbelah dan menjadi merah mawar seperti (kilauan) minyak.

55:38

# فَبِاَيِّ اٰلَاۤءِ رَبِّكُمَا تُكَذِّبٰنِ

fabi-ayyi *aa*l*aa*-i rabbikum*aa* tuka*dzdz*ib*aa*n**i**

Maka nikmat Tuhanmu yang manakah yang kamu dustakan?

55:39

# فَيَوْمَئِذٍ لَّا يُسْـَٔلُ عَنْ ذَنْۢبِهٖٓ اِنْسٌ وَّلَا جَاۤنٌّۚ

fayawma-i*dz*in l*aa* yus-alu 'an *dz*anbihi insun wal*aa* j*aa*n**nun**

Maka pada hari itu manusia dan jin tidak ditanya tentang dosanya.

55:40

# فَبِاَيِّ اٰلَاۤءِ رَبِّكُمَا تُكَذِّبٰنِ

fabi-ayyi *aa*l*aa*-i rabbikum*aa* tuka*dzdz*ib*aa*n**i**

Maka nikmat Tuhanmu yang manakah yang kamu dustakan?

55:41

# يُعْرَفُ الْمُجْرِمُوْنَ بِسِيْمٰهُمْ فَيُؤْخَذُ بِالنَّوَاصِيْ وَالْاَقْدَامِۚ

yu'rafu **a**lmujrimuuna bisiim*aa*hum fayu/kha*dz*u bi**al**nnaw*aas*ii wa**a**l-aqd*aa*m**i**

Orang-orang yang berdosa itu diketahui dengan tanda-tandanya, lalu direnggut ubun-ubun dan kakinya.

55:42

# فَبِاَيِّ اٰلَاۤءِ رَبِّكُمَا تُكَذِّبٰنِ

fabi-ayyi *aa*l*aa*-i rabbikum*aa* tuka*dzdz*ib*aa*n**i**

Maka nikmat Tuhanmu yang manakah yang kamu dustakan?

55:43

# هٰذِهٖ جَهَنَّمُ الَّتِيْ يُكَذِّبُ بِهَا الْمُجْرِمُوْنَۘ

h*aadz*ihi jahannamu **al**latii yuka*dzdz*ibu bih*aa* **a**lmujrimuun**a**

Inilah neraka Jahanam yang didustakan oleh orang-orang yang berdosa.

55:44

# يَطُوْفُوْنَ بَيْنَهَا وَبَيْنَ حَمِيْمٍ اٰنٍۚ

ya*th*uufuuna baynah*aa* wabayna *h*amiimin *aa*n**in**

Mereka berkeliling di sana dan di antara air yang mendidih.

55:45

# فَبِاَيِّ اٰلَاۤءِ رَبِّكُمَا تُكَذِّبٰنِ ࣖ

fabi-ayyi *aa*l*aa*-i rabbikum*aa* tuka*dzdz*ib*aa*n**i**

Maka nikmat Tuhanmu yang manakah yang kamu dustakan?

55:46

# وَلِمَنْ خَافَ مَقَامَ رَبِّهٖ جَنَّتٰنِۚ

waliman kh*aa*fa maq*aa*ma rabbihi jannat*aa*n**i**

Dan bagi siapa yang takut akan saat menghadap Tuhannya ada dua surga.

55:47

# فَبِاَيِّ اٰلَاۤءِ رَبِّكُمَا تُكَذِّبٰنِۙ

fabi-ayyi *aa*l*aa*-i rabbikum*aa* tuka*dzdz*ib*aa*n**i**

Maka nikmat Tuhanmu yang manakah yang kamu dustakan?

55:48

# ذَوَاتَآ اَفْنَانٍۚ

*dz*aw*aa*t*aa* afn*aa*n**in**

kedua surga itu mempunyai aneka pepohonan dan buah-buahan.

55:49

# فَبِاَيِّ اٰلَاۤءِ رَبِّكُمَا تُكَذِّبٰنِ

fabi-ayyi *aa*l*aa*-i rabbikum*aa* tuka*dzdz*ib*aa*n**i**

Maka nikmat Tuhanmu yang manakah yang kamu dustakan?

55:50

# فِيْهِمَا عَيْنٰنِ تَجْرِيٰنِۚ

fiihim*aa* 'ayn*aa*ni tajriy*aa*n**i**

Di dalam kedua surga itu ada dua buah mata air yang memancar.

55:51

# فَبِاَيِّ اٰلَاۤءِ رَبِّكُمَا تُكَذِّبٰنِ

fabi-ayyi *aa*l*aa*-i rabbikum*aa* tuka*dzdz*ib*aa*n**i**

Maka nikmat Tuhanmu yang manakah yang kamu dustakan?

55:52

# فِيْهِمَا مِنْ كُلِّ فَاكِهَةٍ زَوْجٰنِۚ

fiihim*aa* min kulli f*aa*kihatin zawj*aa*n**i**

Di dalam kedua surga itu terdapat aneka buah-buahan yang berpasang-pasangan.

55:53

# فَبِاَيِّ اٰلَاۤءِ رَبِّكُمَا تُكَذِّبٰنِۚ

fabi-ayyi *aa*l*aa*-i rabbikum*aa* tuka*dzdz*ib*aa*n**i**

Maka nikmat Tuhanmu yang manakah yang kamu dustakan?

55:54

# مُتَّكِـِٕيْنَ عَلٰى فُرُشٍۢ بَطَاۤىِٕنُهَا مِنْ اِسْتَبْرَقٍۗ وَجَنَا الْجَنَّتَيْنِ دَانٍۚ

muttaki-iina 'al*aa* furusyin ba*thaa*-inuh*aa* min istabraqin wajan*aa* **a**ljannatayni d*aa*n**in**

Mereka bersandar di atas permadani yang bagian dalamnya dari sutera tebal. Dan buah-buahan di kedua surga itu dapat (dipetik) dari dekat.

55:55

# فَبِاَيِّ اٰلَاۤءِ رَبِّكُمَا تُكَذِّبٰنِ

fabi-ayyi *aa*l*aa*-i rabbikum*aa* tuka*dzdz*ib*aa*n**i**

Maka nikmat Tuhanmu yang manakah yang kamu dustakan?

55:56

# فِيْهِنَّ قٰصِرٰتُ الطَّرْفِۙ لَمْ يَطْمِثْهُنَّ اِنْسٌ قَبْلَهُمْ وَلَا جَاۤنٌّۚ

fiihinna q*aas*ir*aa*tu **al***ththh*arfi lam ya*th*mitshunna insun qablahum wal*aa* j*aa*n**nun**

Di dalam surga itu ada bidadari-bidadari yang membatasi pandangan, yang tidak pernah disentuh oleh manusia maupun jin sebelumnya.

55:57

# فَبِاَيِّ اٰلَاۤءِ رَبِّكُمَا تُكَذِّبٰنِۚ

fabi-ayyi *aa*l*aa*-i rabbikum*aa* tuka*dzdz*ib*aa*n**i**

Maka nikmat Tuhanmu yang manakah yang kamu dustakan?

55:58

# كَاَنَّهُنَّ الْيَاقُوْتُ وَالْمَرْجَانُۚ

ka-annahunna **a**ly*aa*quutu wa**a**lmarj*aa*n**u**

Seakan-akan mereka itu permata yakut dan marjan.

55:59

# فَبِاَيِّ اٰلَاۤءِ رَبِّكُمَا تُكَذِّبٰنِ

fabi-ayyi *aa*l*aa*-i rabbikum*aa* tuka*dzdz*ib*aa*n**i**

Maka nikmat Tuhanmu yang manakah yang kamu dustakan?

55:60

# هَلْ جَزَاۤءُ الْاِحْسَانِ اِلَّا الْاِحْسَانُۚ

hal jaz*aa*u **a**l-i*h*s*aa*ni ill*aa* **a**l-i*h*s*aa*n**u**

Tidak ada balasan untuk kebaikan selain kebaikan (pula).

55:61

# فَبِاَيِّ اٰلَاۤءِ رَبِّكُمَا تُكَذِّبٰنِ

fabi-ayyi *aa*l*aa*-i rabbikum*aa* tuka*dzdz*ib*aa*n**i**

Maka nikmat Tuhanmu yang manakah yang kamu dustakan?

55:62

# وَمِنْ دُوْنِهِمَا جَنَّتٰنِۚ

wamin duunihim*aa* jannat*aa*n**i**

Dan selain dari dua surga itu ada dua surga lagi.

55:63

# فَبِاَيِّ اٰلَاۤءِ رَبِّكُمَا تُكَذِّبٰنِۙ

fabi-ayyi *aa*l*aa*-i rabbikum*aa* tuka*dzdz*ib*aa*n**i**

Maka nikmat Tuhanmu yang manakah yang kamu dustakan,

55:64

# مُدْهَاۤمَّتٰنِۚ

mudh*aa*mmat*aa*n**i**

kedua surga itu (kelihatan) hijau tua warnanya.

55:65

# فَبِاَيِّ اٰلَاۤءِ رَبِّكُمَا تُكَذِّبٰنِ

fabi-ayyi *aa*l*aa*-i rabbikum*aa* tuka*dzdz*ib*aa*n**i**

Maka nikmat Tuhanmu yang manakah yang kamu dustakan?

55:66

# فِيْهِمَا عَيْنٰنِ نَضَّاخَتٰنِۚ

fiihim*aa* 'ayn*aa*ni na*dhdhaa*khat*aa*n**i**

Di dalam keduanya (surga itu) ada dua buah mata air yang memancar.

55:67

# فَبِاَيِّ اٰلَاۤءِ رَبِّكُمَا تُكَذِّبٰنِۚ

fabi-ayyi *aa*l*aa*-i rabbikum*aa* tuka*dzdz*ib*aa*n**i**

Maka nikmat Tuhanmu yang manakah yang kamu dustakan?

55:68

# فِيْهِمَا فَاكِهَةٌ وَّنَخْلٌ وَّرُمَّانٌۚ

fiihim*aa* f*aa*kihatun wanakhlun warumm*aa*n**un**

Di dalam kedua surga itu ada buah-buahan, kurma dan delima.

55:69

# فَبِاَيِّ اٰلَاۤءِ رَبِّكُمَا تُكَذِّبٰنِۚ

fabi-ayyi *aa*l*aa*-i rabbikum*aa* tuka*dzdz*ib*aa*n**i**

Maka nikmat Tuhanmu yang manakah yang kamu dustakan?

55:70

# فِيْهِنَّ خَيْرٰتٌ حِسَانٌۚ

fiihinna khayr*aa*tun *h*is*aa*n**un**

Di dalam surga-surga itu ada bidadari-bidadari yang baik-baik dan jelita.

55:71

# فَبِاَيِّ اٰلَاۤءِ رَبِّكُمَا تُكَذِّبٰنِۚ

fabi-ayyi *aa*l*aa*-i rabbikum*aa* tuka*dzdz*ib*aa*n**i**

Maka nikmat Tuhanmu yang manakah yang kamu dustakan?

55:72

# حُوْرٌ مَّقْصُوْرٰتٌ فِى الْخِيَامِۚ

*h*uurun maq*sh*uur*aa*tun fii **a**lkhiy*aa*m**i**

Bidadari-bidadari yang dipelihara di dalam kemah-kemah.

55:73

# فَبِاَيِّ اٰلَاۤءِ رَبِّكُمَا تُكَذِّبٰنِۚ

fabi-ayyi *aa*l*aa*-i rabbikum*aa* tuka*dzdz*ib*aa*n**i**

Maka nikmat Tuhanmu yang manakah yang kamu dustakan?

55:74

# لَمْ يَطْمِثْهُنَّ اِنْسٌ قَبْلَهُمْ وَلَا جَاۤنٌّۚ

lam ya*th*mitshunna insun qablahum wal*aa* j*aa*n**nun**

Mereka tidak pernah disentuh oleh manusia maupun oleh jin sebelumnya.

55:75

# فَبِاَيِّ اٰلَاۤءِ رَبِّكُمَا تُكَذِّبٰنِۚ

fabi-ayyi *aa*l*aa*-i rabbikum*aa* tuka*dzdz*ib*aa*n**i**

Maka nikmat Tuhanmu yang manakah yang kamu dustakan?

55:76

# مُتَّكِـِٕيْنَ عَلٰى رَفْرَفٍ خُضْرٍ وَّعَبْقَرِيٍّ حِسَانٍۚ

muttaki-iina 'al*aa* rafrafin khu*dh*rin wa'abqariyyin *h*is*aa*n**in**

Mereka bersandar pada bantal-bantal yang hijau dan permadani-permadani yang indah.

55:77

# فَبِاَيِّ اٰلَاۤءِ رَبِّكُمَا تُكَذِّبٰنِۚ

fabi-ayyi *aa*l*aa*-i rabbikum*aa* tuka*dzdz*ib*aa*n**i**

Maka nikmat Tuhanmu yang manakah yang kamu dustakan?

55:78

# تَبٰرَكَ اسْمُ رَبِّكَ ذِى الْجَلٰلِ وَالْاِكْرَامِ ࣖ

tab*aa*raka ismu rabbika *dz*ii **a**ljal*aa*li wa**a**l-ikr*aa*m**i**

Mahasuci nama Tuhanmu Pemilik Keagungan dan Kemuliaan.

<!--EndFragment-->