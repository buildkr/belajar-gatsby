---
title: (58) Al-Mujadalah - المجادلة
date: 2021-10-27T04:14:27.000Z
ayat: 58
description: "Jumlah Ayat: 22 / Arti: Gugatan"
---
<!--StartFragment-->

58:1

# قَدْ سَمِعَ اللّٰهُ قَوْلَ الَّتِيْ تُجَادِلُكَ فِيْ زَوْجِهَا وَتَشْتَكِيْٓ اِلَى اللّٰهِ ۖوَاللّٰهُ يَسْمَعُ تَحَاوُرَكُمَاۗ اِنَّ اللّٰهَ سَمِيْعٌۢ بَصِيْرٌ

qad sami'a **al**l*aa*hu qawla **al**latii tuj*aa*diluka fii zawjih*aa* watasytakii il*aa* **al**l*aa*hi wa**al**l*aa*hu yasma'u ta*haa*wurakum*aa* inna **al**

Sungguh, Allah telah mendengar ucapan perempuan yang mengajukan gugatan kepadamu (Muhammad) tentang suaminya, dan mengadukan (halnya) kepada Allah, dan Allah mendengar percakapan antara kamu berdua. Sesungguhnya Allah Maha Mendengar, Maha Melihat.

58:2

# اَلَّذِيْنَ يُظٰهِرُوْنَ مِنْكُمْ مِّنْ نِّسَاۤىِٕهِمْ مَّا هُنَّ اُمَّهٰتِهِمْۗ اِنْ اُمَّهٰتُهُمْ اِلَّا الّٰۤـِٔيْ وَلَدْنَهُمْۗ وَاِنَّهُمْ لَيَقُوْلُوْنَ مُنْكَرًا مِّنَ الْقَوْلِ وَزُوْرًاۗ وَاِنَّ اللّٰهَ لَعَفُوٌّ غَفُوْرٌ

qad sami'a **al**l*aa*hu qawla **al**latii tuj*aa*diluka fii zawjih*aa* watasytakii il*aa* **al**l*aa*hi wa**al**l*aa*hu yasma'u ta*haa*wurakum*aa* inna **al**

Orang-orang di antara kamu yang menzihar istrinya, (menganggap istrinya sebagai ibunya, padahal) istri mereka itu bukanlah ibunya. Ibu-ibu mereka hanyalah perempuan yang melahirkannya. Dan sesungguhnya mereka benar-benar telah mengucapkan suatu perkataan

58:3

# وَالَّذِيْنَ يُظٰهِرُوْنَ مِنْ نِّسَاۤىِٕهِمْ ثُمَّ يَعُوْدُوْنَ لِمَا قَالُوْا فَتَحْرِيْرُ رَقَبَةٍ مِّنْ قَبْلِ اَنْ يَّتَمَاۤسَّاۗ ذٰلِكُمْ تُوْعَظُوْنَ بِهٖۗ وَاللّٰهُ بِمَا تَعْمَلُوْنَ خَبِيْرٌ

wa**a**lla*dz*iina yu*zhaa*hiruuna min nis*aa*-ihim tsumma ya'uuduuna lim*aa* q*aa*luu fata*h*riiru raqabatin min qabli an yatam*aa*ss*aa* *dzaa*likum tuu'a*zh*uuna bihi wa**al**l

Dan mereka yang menzihar istrinya, kemudian menarik kembali apa yang telah mereka ucapkan, maka (mereka diwajibkan) memerdekakan seorang budak sebelum kedua suami istri itu bercampur. Demikianlah yang diajarkan kepadamu, dan Allah Mahateliti terhadap apa

58:4

# فَمَنْ لَّمْ يَجِدْ فَصِيَامُ شَهْرَيْنِ مُتَتَابِعَيْنِ مِنْ قَبْلِ اَنْ يَّتَمَاۤسَّاۗ فَمَنْ لَّمْ يَسْتَطِعْ فَاِطْعَامُ سِتِّيْنَ مِسْكِيْنًاۗ ذٰلِكَ لِتُؤْمِنُوْا بِاللّٰهِ وَرَسُوْلِهٖۗ وَتِلْكَ حُدُوْدُ اللّٰهِ ۗوَلِلْكٰفِرِيْنَ عَذَابٌ اَلِيْمٌ

faman lam yajid fa*sh*iy*aa*mu syahrayni mutat*aa*bi'ayni min qabli an yatam*aa*ss*aa* faman lam yasta*th*i' fa-i*th*'*aa*mu sittiina miskiinan *dzaa*lika litu/minuu bi**al**l*aa*hi warasuulih

Maka barangsiapa tidak dapat (memerdekakan hamba sahaya), maka (dia wajib) berpuasa dua bulan berturut-turut sebelum keduanya bercampur. Tetapi barangsiapa tidak mampu, maka (wajib) memberi makan enam puluh orang miskin. Demikianlah agar kamu beriman kepa

58:5

# اِنَّ الَّذِيْنَ يُحَاۤدُّوْنَ اللّٰهَ وَرَسُوْلَهٗ كُبِتُوْا كَمَا كُبِتَ الَّذِيْنَ مِنْ قَبْلِهِمْ وَقَدْ اَنْزَلْنَآ اٰيٰتٍۢ بَيِّنٰتٍۗ وَلِلْكٰفِرِيْنَ عَذَابٌ مُّهِيْنٌۚ

inna **al**la*dz*iina yu*haa*dduuna **al**l*aa*ha warasuulahu kubituu kam*aa* kubita **al**la*dz*iina min qablihim waqad anzaln*aa* *aa*y*aa*tin bayyin*aa*tin walilk*aa<*

Sesungguhnya orang-orang yang menentang Allah dan Rasul-Nya pasti mendapat kehinaan sebagaimana kehinaan yang telah didapat oleh orang-orang sebelum mereka. Dan sungguh, Kami telah menurunkan bukti-bukti yang nyata. Dan bagi orang-orang yang mengingkariny

58:6

# يَوْمَ يَبْعَثُهُمُ اللّٰهُ جَمِيْعًا فَيُنَبِّئُهُمْ بِمَا عَمِلُوْاۗ اَحْصٰىهُ اللّٰهُ وَنَسُوْهُۗ وَاللّٰهُ عَلٰى كُلِّ شَيْءٍ شَهِيْدٌ ࣖ

yawma yab'atsuhumu **al**l*aa*hu jamii'an fayunabbi-uhum bim*aa* 'amiluu a*hsaa*hu **al**l*aa*hu wanasuuhu wa**al**l*aa*hu 'al*aa* kulli syay-in syahiid**un**

Pada hari itu mereka semuanya dibangkitkan Allah, lalu diberitakan-Nya kepada mereka apa yang telah mereka kerjakan. Allah menghitungnya (semua amal perbuatan itu), meskipun mereka telah melupakannya. Dan Allah Maha Menyaksikan segala sesuatu.

58:7

# اَلَمْ تَرَ اَنَّ اللّٰهَ يَعْلَمُ مَا فِى السَّمٰوٰتِ وَمَا فِى الْاَرْضِۗ مَا يَكُوْنُ مِنْ نَّجْوٰى ثَلٰثَةٍ اِلَّا هُوَ رَابِعُهُمْ وَلَا خَمْسَةٍ اِلَّا هُوَ سَادِسُهُمْ وَلَآ اَدْنٰى مِنْ ذٰلِكَ وَلَآ اَكْثَرَ اِلَّا هُوَ مَعَهُمْ اَيْنَ مَا كَانُ

alam tara anna **al**l*aa*ha ya'lamu m*aa* fii **al**ssam*aa*w*aa*ti wam*aa* fii **a**l-ar*dh*i m*aa* yakuunu min najw*aa* tsal*aa*tsatin ill*aa* huwa r*aa*bi'uh

Tidakkah engkau perhatikan, bahwa Allah mengetahui apa yang ada di langit dan apa yang ada di bumi? Tidak ada pembicaraan rahasia antara tiga orang, melainkan Dialah yang keempatnya. Dan tidak ada lima orang, melainkan Dialah yang keenamnya. Dan tidak ada

58:8

# اَلَمْ تَرَ اِلَى الَّذِيْنَ نُهُوْا عَنِ النَّجْوٰى ثُمَّ يَعُوْدُوْنَ لِمَا نُهُوْا عَنْهُ وَيَتَنٰجَوْنَ بِالْاِثْمِ وَالْعُدْوَانِ وَمَعْصِيَتِ الرَّسُوْلِۖ وَاِذَا جَاۤءُوْكَ حَيَّوْكَ بِمَا لَمْ يُحَيِّكَ بِهِ اللّٰهُ ۙوَيَقُوْلُوْنَ فِيْٓ اَنْفُسِه

alam tara il*aa* **al**la*dz*iina nuhuu 'ani **al**nnajw*aa* tsumma ya'uuduuna lim*aa* nuhuu 'anhu wayatan*aa*jawna bi**a**l-itsmi wa**a**l'udw*aa*ni wama'*sh*iyati

Tidakkah engkau perhatikan orang-orang yang telah dilarang mengadakan pembicaraan rahasia, kemudian mereka kembali (mengerjakan) larangan itu dan mereka mengadakan pembicaraan rahasia untuk berbuat dosa, permusuhan dan durhaka kepada Rasul. Dan apabila me

58:9

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْٓا اِذَا تَنَاجَيْتُمْ فَلَا تَتَنَاجَوْا بِالْاِثْمِ وَالْعُدْوَانِ وَمَعْصِيَتِ الرَّسُوْلِ وَتَنَاجَوْا بِالْبِرِّ وَالتَّقْوٰىۗ وَاتَّقُوا اللّٰهَ الَّذِيْٓ اِلَيْهِ تُحْشَرُوْنَ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu i*dzaa* tan*aa*jaytum fal*aa* tatan*aa*jaw bi**a**l-itsmi wa**a**l'udw*aa*ni wama'*sh*iyati **al**rrasuuli wat

Wahai orang-orang yang beriman! Apabila kamu mengadakan pembicaraan rahasia, janganlah kamu membicarakan perbuatan dosa, permusuhan dan durhaka kepada Rasul. Tetapi bicarakanlah tentang perbuatan kebajikan dan takwa. Dan bertakwalah kepada Allah yang kepa

58:10

# اِنَّمَا النَّجْوٰى مِنَ الشَّيْطٰنِ لِيَحْزُنَ الَّذِيْنَ اٰمَنُوْا وَلَيْسَ بِضَاۤرِّهِمْ شَيْـًٔا اِلَّا بِاِذْنِ اللّٰهِ ۗوَعَلَى اللّٰهِ فَلْيَتَوَكَّلِ الْمُؤْمِنُوْنَ

innam*aa* **al**nnajw*aa* mina **al**sysyay*thaa*ni liya*h*zuna **al**la*dz*iina *aa*manuu walaysa bi*daa*rrihim syay-an ill*aa* bi-i*dz*ni **al**l*a*

Sesungguhnya pembicaraan rahasia itu termasuk (perbuatan) setan, agar orang-orang yang beriman itu bersedih hati, sedang (pembicaraan) itu tidaklah memberi bencana sedikit pun kepada mereka, kecuali dengan izin Allah. Dan kepada Allah hendaknya orang-oran

58:11

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْٓا اِذَا قِيْلَ لَكُمْ تَفَسَّحُوْا فِى الْمَجٰلِسِ فَافْسَحُوْا يَفْسَحِ اللّٰهُ لَكُمْۚ وَاِذَا قِيْلَ انْشُزُوْا فَانْشُزُوْا يَرْفَعِ اللّٰهُ الَّذِيْنَ اٰمَنُوْا مِنْكُمْۙ وَالَّذِيْنَ اُوْتُوا الْعِلْمَ دَرَجٰتٍۗ وَالل

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu i*dzaa* qiila lakum tafassa*h*uu fii **a**lmaj*aa*lisi fa**i**fsa*h*uu yafsa*h*i **al**l*aa*hu lakum wa-i*dz*

Wahai orang-orang yang beriman! Apabila dikatakan kepadamu, “Berilah kelapangan di dalam majelis-majelis,” maka lapangkanlah, niscaya Allah akan memberi kelapangan untukmu. Dan apabila dikatakan, “Berdirilah kamu,” maka berdirilah, niscaya Allah akan meng

58:12

# يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْٓا اِذَا نَاجَيْتُمُ الرَّسُوْلَ فَقَدِّمُوْا بَيْنَ يَدَيْ نَجْوٰىكُمْ صَدَقَةً ۗذٰلِكَ خَيْرٌ لَّكُمْ وَاَطْهَرُۗ فَاِنْ لَّمْ تَجِدُوْا فَاِنَّ اللّٰهَ غَفُوْرٌ رَّحِيْمٌ

y*aa* ayyuh*aa* **al**la*dz*iina *aa*manuu i*dzaa* n*aa*jaytumu **al**rrasuula faqaddimuu bayna yaday najw*aa*kum *sh*adaqatan *dzaa*lika khayrun lakum wa-a*th*haru fa-in lam taji

Wahai orang-orang yang beriman! Apabila kamu mengadakan pembicaraan khusus dengan Rasul, hendaklah kamu mengeluarkan sedekah (kepada orang miskin) sebelum (melakukan) pembicaraan itu. Yang demikian itu lebih baik bagimu dan lebih bersih. Tetapi jika kamu

58:13

# ءَاَشْفَقْتُمْ اَنْ تُقَدِّمُوْا بَيْنَ يَدَيْ نَجْوٰىكُمْ صَدَقٰتٍۗ فَاِذْ لَمْ تَفْعَلُوْا وَتَابَ اللّٰهُ عَلَيْكُمْ فَاَقِيْمُوا الصَّلٰوةَ وَاٰتُوا الزَّكٰوةَ وَاَطِيْعُوا اللّٰهَ وَرَسُوْلَهٗ ۗوَاللّٰهُ خَبِيْرٌ ۢبِمَا تَعْمَلُوْنَ ࣖ

a-asyfaqtum an tuqaddimuu bayna yaday najw*aa*kum *sh*adaq*aa*tin fa-i*dz* lam taf'aluu wat*aa*ba **al**l*aa*hu 'alaykum fa-aqiimuu **al***shsh*al*aa*ta wa*aa*tuu **al**zz

Apakah kamu takut akan (menjadi miskin) karena kamu memberikan sedekah sebelum (melakukan) pembicaraan dengan Rasul? Tetapi jika kamu tidak melakukannya dan Allah telah memberi ampun kepadamu, maka laksanakanlah salat, dan tunaikanlah zakat serta taatlah

58:14

# ۞ اَلَمْ تَرَ اِلَى الَّذِيْنَ تَوَلَّوْا قَوْمًا غَضِبَ اللّٰهُ عَلَيْهِمْۗ مَا هُمْ مِّنْكُمْ وَلَا مِنْهُمْۙ وَيَحْلِفُوْنَ عَلَى الْكَذِبِ وَهُمْ يَعْلَمُوْنَ

alam tara il*aa* **al**la*dz*iina tawallaw qawman gha*dh*iba **al**l*aa*hu 'alayhim m*aa* hum minkum wal*aa* minhum waya*h*lifuuna 'al*aa* **a**lka*dz*ibi wahum ya'lamuun<

Tidakkah engkau perhatikan orang-orang (munafik) yang menjadikan suatu kaum yang telah dimurkai Allah sebagai sahabat? Orang-orang itu bukan dari (kaum) kamu dan bukan dari (kaum) mereka. Dan mereka bersumpah atas kebohongan, sedang mereka mengetahuinya.

58:15

# اَعَدَّ اللّٰهُ لَهُمْ عَذَابًا شَدِيْدًاۗ اِنَّهُمْ سَاۤءَ مَا كَانُوْا يَعْمَلُوْنَ

a'adda **al**l*aa*hu lahum 'a*dzaa*ban syadiidan innahum s*aa*-a m*aa* k*aa*nuu ya'maluun**a**

Allah telah menyediakan azab yang sangat keras bagi mereka. Sungguh, betapa buruknya apa yang telah mereka kerjakan.

58:16

# اِتَّخَذُوْٓا اَيْمَانَهُمْ جُنَّةً فَصَدُّوْا عَنْ سَبِيْلِ اللّٰهِ فَلَهُمْ عَذَابٌ مُّهِيْنٌ

ittakha*dz*uu aym*aa*nahum junnatan fa*sh*adduu 'an sabiili **al**l*aa*hi falahum 'a*dzaa*bun muhiin**un**

Mereka menjadikan sumpah-sumpah mereka sebagai perisai, lalu mereka menghalang-halangi (manusia) dari jalan Allah; maka bagi mereka azab yang menghinakan.

58:17

# لَنْ تُغْنِيَ عَنْهُمْ اَمْوَالُهُمْ وَلَآ اَوْلَادُهُمْ مِّنَ اللّٰهِ شَيْـًٔاۗ اُولٰۤىِٕكَ اَصْحٰبُ النَّارِۗ هُمْ فِيْهَا خٰلِدُوْنَ

lan tughniya 'anhum amw*aa*luhum wal*aa* awl*aa*duhum mina **al**l*aa*hi syay-an ul*aa*-ika a*sh*-*haa*bu **al**nn*aa*ri hum fiih*aa* kh*aa*liduun**a**

Harta benda dan anak-anak mereka tidak berguna sedikit pun (untuk menolong) mereka dari azab Allah. Mereka itulah penghuni neraka, mereka kekal di dalamnya.

58:18

# يَوْمَ يَبْعَثُهُمُ اللّٰهُ جَمِيْعًا فَيَحْلِفُوْنَ لَهٗ كَمَا يَحْلِفُوْنَ لَكُمْ وَيَحْسَبُوْنَ اَنَّهُمْ عَلٰى شَيْءٍۗ اَلَآ اِنَّهُمْ هُمُ الْكٰذِبُوْنَ

yawma yab'atsuhumu **al**l*aa*hu jamii'an faya*h*lifuuna lahu kam*aa* ya*h*lifuuna lakum waya*h*sabuuna annahum 'al*aa* syay-in **a**l*aa* innahum humu **a**lk*aadz*ibuun

(Ingatlah) pada hari (ketika) mereka semua dibangkitkan Allah, lalu mereka bersumpah kepada-Nya (bahwa mereka bukan orang musyrik) sebagaimana mereka bersumpah kepadamu; dan mereka menyangka bahwa mereka akan memperoleh sesuatu (manfaat). Ketahuilah, bahw

58:19

# اِسْتَحْوَذَ عَلَيْهِمُ الشَّيْطٰنُ فَاَنْسٰىهُمْ ذِكْرَ اللّٰهِ ۗ اُولٰۤىِٕكَ حِزْبُ الشَّيْطٰنِۗ اَلَآ اِنَّ حِزْبَ الشَّيْطٰنِ هُمُ الْخٰسِرُوْنَ

ista*h*wa*dz*a 'alayhimu **al**sysyay*thaa*nu fa-ans*aa*hum *dz*ikra **al**l*aa*hi ul*aa*-ika *h*izbu **al**sysyay*thaa*ni **a**l*aa* inna

Setan telah menguasai mereka, lalu menjadikan mereka lupa mengingat Allah; mereka itulah golongan setan. Ketahuilah, bahwa golongan setan itulah golongan yang rugi.

58:20

# اِنَّ الَّذِيْنَ يُحَاۤدُّوْنَ اللّٰهَ وَرَسُوْلَهٗٓ اُولٰۤىِٕكَ فِى الْاَذَلِّيْنَ

inna **al**la*dz*iina yu*haa*dduuna **al**l*aa*ha warasuulahu ul*aa*-ika fii **a**l-a*dz*alliin**a**

Sesungguhnya orang-orang yang menentang Allah dan Rasul-Nya, mereka termasuk orang-orang yang sangat hina.

58:21

# كَتَبَ اللّٰهُ لَاَغْلِبَنَّ اَنَا۠ وَرُسُلِيْۗ اِنَّ اللّٰهَ قَوِيٌّ عَزِيْزٌ

kataba **al**l*aa*hu la-aghlibanna an*aa* warusulii inna **al**l*aa*ha qawiyyun 'aziiz**un**

Allah telah menetapkan, “Aku dan rasul-rasul-Ku pasti menang.” Sungguh, Allah Mahakuat, Mahaperkasa.

58:22

# لَا تَجِدُ قَوْمًا يُّؤْمِنُوْنَ بِاللّٰهِ وَالْيَوْمِ الْاٰخِرِ يُوَاۤدُّوْنَ مَنْ حَاۤدَّ اللّٰهَ وَرَسُوْلَهٗ وَلَوْ كَانُوْٓا اٰبَاۤءَهُمْ اَوْ اَبْنَاۤءَهُمْ اَوْ اِخْوَانَهُمْ اَوْ عَشِيْرَتَهُمْۗ اُولٰۤىِٕكَ كَتَبَ فِيْ قُلُوْبِهِمُ الْاِيْمَانَ و

lā tajidu qaumay yu\`minụna billāhi wal-yaumil-ākhiri yuwāddụna man ḥāddallāha wa rasụlahụ walau kānū ābā\`ahum au abnā\`ahum au ikhwānahum au ‘asyīratahum, ulā\`ika kataba fī qulụbihimul-īmāna wa ayyadahum birụḥim min-h, wa yudkhiluhum jannātin tajrī min taḥtihal-an-hāru khālidīna fīhā, raḍiyallāhu ‘an-hum wa raḍụ ‘an-h, ulā`ika ḥizbullāh, alā inna ḥizballāhi humul-mufliḥụn 

Kamu tak akan mendapati kaum yang beriman pada Allah dan hari akhirat, saling berkasih-sayang dengan orang-orang yang menentang Allah dan Rasul-Nya, sekalipun orang-orang itu bapak-bapak, atau anak-anak atau saudara-saudara ataupun keluarga mereka. Mereka itulah orang-orang yang telah menanamkan keimanan dalam hati mereka dan menguatkan mereka dengan pertolongan yang datang daripada-Nya. Dan dimasukan-Nya mereka ke dalam surga yang mengalir di bawahnya sungai-sungai, mereka kekal di dalamnya. Allah ridha terhadap mereka, dan merekapun merasa puas terhadap (limpahan rahmat)-Nya. Mereka itulah golongan Allah. Ketahuilah, bahwa sesungguhnya hizbullah itu adalah golongan yang beruntung.

<!--EndFragment-->