---
title: (113) Al-Falaq - الفلق
date: 2021-10-27T04:20:19.683Z
ayat: 113
description: "Jumlah Ayat: 5 / Arti: Subuh"
---
<!--StartFragment-->

113:1

# قُلْ اَعُوْذُ بِرَبِّ الْفَلَقِۙ

qul a'uu*dz*u birabbi **a**lfalaq**i**

Katakanlah, “Aku berlindung kepada Tuhan yang menguasai subuh (fajar),

113:2

# مِنْ شَرِّ مَا خَلَقَۙ

min syarri m*aa* khalaq**a**

dari kejahatan (makhluk yang) Dia ciptakan,

113:3

# وَمِنْ شَرِّ غَاسِقٍ اِذَا وَقَبَۙ

wamin syarri gh*aa*siqin i*dzaa* waqab**a**

dan dari kejahatan malam apabila telah gelap gulita,

113:4

# وَمِنْ شَرِّ النَّفّٰثٰتِ فِى الْعُقَدِۙ

wamin syarri **al**nnaff*aa*ts*aa*ti fii **a**l'uqad**i**

dan dari kejahatan (perempuan-perempuan) penyihir yang meniup pada buhul-buhul (talinya),

113:5

# وَمِنْ شَرِّ حَاسِدٍ اِذَا حَسَدَ ࣖ

wamin syarri *haa*sidin i*dzaa* *h*asad**a**

dan dari kejahatan orang yang dengki apabila dia dengki.”

<!--EndFragment-->