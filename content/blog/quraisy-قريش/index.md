---
title: (106) Quraisy - قريش
date: 2021-10-27T04:17:15.496Z
ayat: 106
description: "Jumlah Ayat: 4 / Arti: Quraisy"
---
<!--StartFragment-->

106:1

# لِاِيْلٰفِ قُرَيْشٍۙ

li-iil*aa*fi quraysy**in**

Karena kebiasaan orang-orang Quraisy,

106:2

# اٖلٰفِهِمْ رِحْلَةَ الشِّتَاۤءِ وَالصَّيْفِۚ

iil*aa*fihim ri*h*lata **al**sysyit*aa*-i wa**al***shsh*ayf**i**

(yaitu) kebiasaan mereka bepergian pada musim dingin dan musim panas.

106:3

# فَلْيَعْبُدُوْا رَبَّ هٰذَا الْبَيْتِۙ

falya'buduu rabba h*aadzaa* **a**lbayt**i**

Maka hendaklah mereka menyembah Tuhan (pemilik) rumah ini (Ka‘bah),

106:4

# الَّذِيْٓ اَطْعَمَهُمْ مِّنْ جُوْعٍ ەۙ وَّاٰمَنَهُمْ مِّنْ خَوْفٍ ࣖ

**al**la*dz*ii a*th*'amahum min juu'in wa*aa*manahum min khawf**in**

yang telah memberi makanan kepada mereka untuk menghilangkan lapar dan mengamankan mereka dari rasa ketakutan.

<!--EndFragment-->