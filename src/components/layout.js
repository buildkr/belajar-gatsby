import * as React from "react"
import { Link } from "gatsby"
import { Layout } from 'antd';


const { Header, Footer, Content } = Layout;

const X = ({ location, title, children }) => {
  const rootPath = `${__PATH_PREFIX__}/`
  const isRootPath = location.pathname === rootPath
  let header

  if (isRootPath) {
    header = (
      <Link className="main-heading" to="/">
          <img src="./logo-quranku.png" alt="logo"  className="logo"/>
          <h1 className="main-heading">
            Qur'an-ku
          </h1>
      </Link>
    )
  } else {
    header = (
      <Link className="main-heading" to="/">
        <img src="../logo-quranku.png" alt="logo"  className="logo"/>
        <h1 className="main-heading">
          Qur'an-ku
        </h1>
      </Link>
    )
  }

  return (
    <div className="global-wrapper" data-is-root-path={isRootPath}>
      <Layout>
        <Header className="global-header">
          <header>{header}</header>
        </Header>
        <Content className='main-content1'>
          <main>{children}</main>
        </Content>
        <Footer style={{backgroundColor: "#eef7f8"}}>
           Created by : Nida, Apri, Handoko, Aris.
        </Footer>
      </Layout>
    </div>
  )
}

export default X
