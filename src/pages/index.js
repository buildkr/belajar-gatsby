import * as React from "react"
import { Link, graphql } from "gatsby"
import 'antd/dist/antd.css'

import { Card, Input, Space } from "antd"

// import Bio from "../components/bio"
import Layout from "../components/layout"
import Seo from "../components/seo"


const BlogIndex = ({ data, location }) => {
  const siteTitle = data.site.siteMetadata?.title || `Title`
  const posts = data.allMarkdownRemark.nodes
  const [searchTerms, setSearchTerm] = React.useState("");
  const { Search } = Input;
  const onSearch = value => console.log(value);

  if (posts.length === 0) {
    return (
      <Layout location={location} title={siteTitle}>
        <Space direction="vertical" align="center" className="space-search">
          <Search placeholder="Cari surah" allowClear onChange={(event) => {setSearchTerm(event.target.value);}} onSearch={onSearch}/>
        </Space>
        <Seo title="All posts" />
        <p>
          No blog posts found. Add markdown posts to "content/blog" (or the
          directory you specified for the "gatsby-source-filesystem" plugin in
          gatsby-config.js).
        </p>
      </Layout>
    )
  }

  return (
    <Layout location={location} title={siteTitle}>
      <Space direction="vertical" align="center" className="space-search">
        <Search placeholder="Cari surah" allowClear onChange={(event) => {setSearchTerm(event.target.value);}} onSearch={onSearch} />
      </Space>
      <Seo title="All posts" />
      <div className="grid-content">
        {posts.filter((val)=>{
          if (searchTerms === "") {
            return val
          } else if (val.frontmatter.title.toLowerCase().includes(searchTerms.toLowerCase())){
            return val
          }
          return false
        }).map(post => {
          const title = post.frontmatter.title || post.fields.slug

          return (
            <Card key={post.fields.slug} style={{ boxShadow: "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)" }}>
              <article
                className="post-list-item"
                itemScope
                itemType="http://schema.org/Article"
              >
                <header>
                  <h2 style={{fontFamily: "Cormorant Garamond"}}>
                    <Link to={post.fields.slug} itemProp="url">
                      <span itemProp="headline" style={{color: "#5eadb9", fontWeight: "bold"}}>{title}</span>
                    </Link>
                  </h2>
                </header>
                <section>
                  <p
                    dangerouslySetInnerHTML={{
                      __html: post.frontmatter.description || post.excerpt,
                    }}
                    itemProp="description"
                    style={{fontFamily: "Proza Libre", color: "#143C42"}}
                  />
                </section>
              </article>
            </Card>
          )
        })}
      </div>
    </Layout>
  )
}

export default BlogIndex

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    allMarkdownRemark(sort: { fields: [frontmatter___ayat], order: ASC }) {
      nodes {
        excerpt
        fields {
          slug
        }
        frontmatter {
          date(formatString: "MMMM DD, YYYY")
          title
          description
        }
      }
    }
  }
`
